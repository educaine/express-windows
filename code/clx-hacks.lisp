;;; -*- Mode: LISP; Syntax: Common-lisp; Package: XLIB; Base: 10; Lowercase: Yes; Patch-File: T -*-

;;; -----------------------------------------------------------------
;;;
;;;	4/26/90	(SLN)
;;;		Changed all calls to (declare-values ...) to
;;;		(declare (values ...)).
;;;
;;; -----------------------------------------------------------------

(cl:in-package 'xlib)

#+(and (not CLX-MIT-R4) lucid)
(defun fast-translate-default (src src-start src-end dst dst-start)
  ;; dst is guaranteed to have room for (- src-end src-start) integer elements,
  ;; starting at dst-start; whether dst holds 8-bit or 16-bit elements depends
  ;; on context.  The function should
  ;; translate as many elements of src as possible into indexes in the current
  ;; font, and store them into dst.
  ;;
  ;; The first return value should be the src index of the first untranslated
  ;; element.  If no further elements need to be translated, the second return
  ;; value should be nil.  If a horizontal motion is required before further
  ;; translation, the second return value should be the delta in x coordinate.
  ;; If a font change is required for further translation, the second return
  ;; value should be the new font.  If known, the pixel width of the translated
  ;; text can be returned as the third value; this can allow for appending of
  ;; subsequent output to the same protocol request, if no overall width has
  ;; been specified at the higher level.
  ;; (returns values: ending-index
  ;;                  (OR null horizontal-motion font)
  ;;                  (OR null translated-width))
  (declare (type sequence src)
	   (type array-index src-start src-end dst-start)
	   ;;(type vector dst)
	   ;; determined by experiment in lucid stuff.
	   (type (simple-array (unsigned-byte 8) (*)) dst)
	   (inline graphic-char-p))
  (declare (values integer)) ;; (or null integer font) (or null integer)
  (multiple-value-bind (src-simple-string offset)
      (sys:underlying-simple-vector src)
    (declare (simple-string src-simple-string) (fixnum offset))
    (incf src-start offset)
    (incf src-end offset)
    (do ((i src-start (index+ i 1))
	 (j dst-start (index+ j 1))
	 (char))
	((index>= i src-end)
	 i)
      (declare (type array-index i j))
      ;;(setq buffer dst)
      (if (< 31 (setq char (the fixnum (char-code (the string-char (aref src-simple-string i)))))
	     256)
	  (setf (aref dst j) (the fixnum char))
	  (return i)))))



#+lucid-ignore
(eval-when (compile)
  (load (make-pathname :NAME
		       #+symbolics "BUFMAC"
		       #-symbolics "bufmac"
		       :DEFAULTS user::*Default-EW-Clx-Pathname*))
  (load (make-pathname :NAME
		       #+symbolics "MACROS"
		       #-symbolics "macros"
		       :DEFAULTS user::*Default-EW-Clx-Pathname*)))

#+(and (not CLX-MIT-R4) lucid)
(defun fast-draw-glyphs8 (drawable gcontext x y sequence start end)
  ;; First result is new start, if end was not reached.  Second result is
  ;; overall width, if known.
  (declare (type drawable drawable)
	   (type gcontext gcontext)
	   (type int32 x y)
	   (type array-index start end)
	   ;;(type sequence sequence)
	   (type string sequence)
	   )
  (declare (values (or null array-index) (or null int32)))
  (let* ((src-start start)
	 (src-end (or end (length sequence)))
	 ;;(next-start nil)
	 (length (index- src-end src-start))
	 (display (gcontext-display gcontext))
	 ;; Should metrics-p be T?  Don't want to pass a NIL font into translate...
	 ;;(font (gcontext-font gcontext t))
	 )
    (declare (type array-index src-start src-end length)
	     (type display display))
    (with-buffer-request (display *x-polytext8* :gc-force gcontext :length length)
      (drawable drawable)
      (gcontext gcontext)
      (int16 x y)
      (progn
	(do* ((boffset (index+ buffer-boffset 16))
	      (src-chunk 0)
	      (dst-chunk 0)
	      ;; (offset 0)
	      (stop-p nil))
	     ((or stop-p (zerop length))
	      ;; Ensure terminated with zero bytes
	      (do ((end (the array-index (lround boffset))))
		  ((index>= boffset end))
		(setf (aref #+lucid (the (simple-array (unsigned-byte 8) (*)) buffer-bbuf)
			    boffset) 0)
		(index-incf boffset))
	      (length-put 2 (index-ash (index- boffset buffer-boffset) -2))
	      (setf (buffer-boffset display) boffset))

	  (declare (type array-index src-chunk dst-chunk) ;; offset
		   (type boolean stop-p))
	  (let ((new-start (fast-translate-default
			     sequence src-start
			     (index+ src-start
				     (setq src-chunk (index-min length *max-string-size*)))
			     buffer-bbuf (index+ boffset 2))))
	    (setq dst-chunk (index- new-start src-start)
		  length (index- length dst-chunk)
		  src-start new-start)
	    (when (index-plusp dst-chunk)
	      (setf (aref (the (simple-array (unsigned-byte 8) (*)) buffer-bbuf) boffset)
		    dst-chunk)
	      (setf (aref (the (simple-array (unsigned-byte 8) (*)) buffer-bbuf)
			  (index+ boffset 1))
		    0)	;offset
	      (incf boffset (index+ dst-chunk 2)))
	    ;; (setq offset 0)
	    ;; Don't stop if translate copied whole chunk
	    (unless (index= src-chunk dst-chunk)
	      (setq stop-p t))
	    ))))))

;; In the functions below, if width is specified, it is assumed to be the pixel
;; width of whatever string of glyphs is actually drawn.  Specifying width will
;; allow for appending the output of subsequent calls to the same protocol
;; request, provided gcontext has not been modified in the interim.  If width
;; is not specified, appending of subsequent output might not occur.
;; Specifying width is simply a hint, for performance.  Note that specifying
;; width may be difficult if transform can return nil.

#+ignore-original-version
(defun translate-default (src src-start src-end font dst dst-start)
  ;; dst is guaranteed to have room for (- src-end src-start) integer elements,
  ;; starting at dst-start; whether dst holds 8-bit or 16-bit elements depends
  ;; on context.  font is the current font, if known.  The function should
  ;; translate as many elements of src as possible into indexes in the current
  ;; font, and store them into dst.
  ;;
  ;; The first return value should be the src index of the first untranslated
  ;; element.  If no further elements need to be translated, the second return
  ;; value should be nil.  If a horizontal motion is required before further
  ;; translation, the second return value should be the delta in x coordinate.
  ;; If a font change is required for further translation, the second return
  ;; value should be the new font.  If known, the pixel width of the translated
  ;; text can be returned as the third value; this can allow for appending of
  ;; subsequent output to the same protocol request, if no overall width has
  ;; been specified at the higher level.
  ;; (returns values: ending-index
  ;;                  (OR null horizontal-motion font)
  ;;                  (OR null translated-width))
  (declare (type sequence src)
	   (type array-index src-start src-end dst-start)
	   (type (or null font) font)
	   (type vector dst)
	   (inline graphic-char-p))
  (declare (values integer (or null integer font) (or null integer)))
  font ;;not used
  (if (stringp src)
      (do ((i src-start (index+ i 1))
	   (j dst-start (index+ j 1))
	   (char))
	  ((index>= i src-end)
	   i)
	(declare (type array-index i j))
	(if (graphic-char-p (setq char (char src i)))
	    (setf (aref dst j) (char->card8 char))
	  (return i)))
      (do ((i src-start (index+ i 1))
	   (j dst-start (index+ j 1))
	   (elt))
	  ((index>= i src-end)
	   i)
	(declare (type array-index i j))
	(setq elt (elt src i))
	(cond ((and (characterp elt) (graphic-char-p elt))
	       (setf (aref dst j) (char->card8 elt)))
	      ((integerp elt)
	       (setf (aref dst j) elt))
	      (t
	       (return i))))))


#+ignore-original-version
(defun draw-glyphs8 (drawable gcontext x y sequence start end translate width)
  ;; First result is new start, if end was not reached.  Second result is
  ;; overall width, if known.
  (declare (type drawable drawable)
	   (type gcontext gcontext)
	   (type int32 x y width)
	   (type array-index start end)
	   (type sequence sequence))
  (declare (values (or null array-index) (or null int32)))
  (declare-funarg translation-function translate) 
  (let* ((src-start start)
	 (src-end (or end (length sequence)))
	 (next-start nil)
	 (length (index- src-end src-start))
	 (display (gcontext-display gcontext))
	 ;; Should metrics-p be T?  Don't want to pass a NIL font into translate...
	 (font (gcontext-font gcontext t)))
    (declare (type array-index src-start src-end length)
	     (type (or null array-index) next-start)
	     (type display display))
    (with-buffer-request (display *x-polytext8* :gc-force gcontext :length length)
      (drawable drawable)
      (gcontext gcontext)
      (int16 x y)
      (progn
	(do* ((boffset (index+ buffer-boffset 16))
	      (src-chunk 0)
	      (dst-chunk 0)
	      (offset 0)
	      (overall-width 0)
	      (stop-p nil))
	     ((or stop-p (zerop length))
	      ;; Ensure terminated with zero bytes
	      (do ((end (the array-index (lround boffset))))
		  ((index>= boffset end))
		(setf (aref buffer-bbuf boffset) 0)
		(index-incf boffset))
	      (length-put 2 (index-ash (index- boffset buffer-boffset) -2))
	      (setf (buffer-boffset display) boffset)
	      (unless (index-zerop length) (setq next-start src-start))
	      (when overall-width (setq width overall-width)))

	  (declare (type array-index src-chunk dst-chunk offset)
		   (type (or null int32) overall-width)
		   (type boolean stop-p))
	  (setq src-chunk (index-min length *max-string-size*))
	  (multiple-value-bind (new-start new-font translated-width)
	      (funcall translate
		       sequence src-start (index+ src-start src-chunk)
		       font buffer-bbuf (index+ boffset 2))
	    (setq dst-chunk (index- new-start src-start)
		  length (index- length dst-chunk)
		  src-start new-start)
	    (if translated-width
		(when overall-width (incf overall-width translated-width))
	      (setq overall-width nil))
	    (when (index-plusp dst-chunk)
	      (setf (aref buffer-bbuf boffset) dst-chunk)
	      (setf (aref buffer-bbuf (index+ boffset 1)) offset)
	      (incf boffset (index+ dst-chunk 2)))
	    (setq offset 0)
	    (cond ((null new-font)
		   ;; Don't stop if translate copied whole chunk
		   (unless (index= src-chunk dst-chunk)
		     (setq stop-p t)))
		  ((integerp new-font) (setq offset new-font))
		  ((type? new-font 'font)
		   (setq font new-font)
		   (let ((font-id (font-id font))
			 (buffer-boffset boffset))
		     (declare (type resource-id font-id)
			      (type array-index buffer-boffset))
		     ;; This changes the gcontext font in the server
		     ;; Update the gcontext cache (both local and server state)
		     (setf (gcontext-internal-font-obj (gcontext-local-state gcontext)) font
			   (gcontext-internal-font-obj (gcontext-server-state gcontext)) font
			   (gcontext-internal-font (gcontext-local-state gcontext)) font-id
			   (gcontext-internal-font (gcontext-server-state gcontext)) font-id)
		     (card8-put 0 #xff)
		     (card8-put 1 (ldb (byte 8 24) font-id))
		     (card8-put 2 (ldb (byte 8 16) font-id))
		     (card8-put 3 (ldb (byte 8 8) font-id))
		     (card8-put 4 (ldb (byte 8 0) font-id)))
		   (index-incf boffset 5)))
	    ))))
    (when *Debug* (format Tv:initial-lisp-listener
			  "~&Draw Glyphs Request ~D ~S" (buffer-request-number display)
			  sequence))
    (values next-start width)))




;;; I don't know if this is a big time waster or not.  But I suspect it is one reason
;;; why tracking the mouse is slow.  Why do gethashs in lookup resource id every time
;;; the mouse moves.


(defvar *Last-CLX-Window-Found* NIL)
(defvar *Last-CLX-Window-ID-Found* NIL)
(defvar *Last-CLX-Window-Display-Found* NIL)

(defun lookup-window (display id)
  (declare (type display display) (type resource-id id))
  (declare (values window))
  ;; hopefully much fast to cache the last value since that is what
  ;; get's accessed the most.
  (if (and (eq id *Last-CLX-Window-ID-Found*)
	   (eq display *Last-CLX-Window-Display-Found*))
      *Last-CLX-Window-Found*
      (let ((window (gethash id (display-resource-id-map display))))
	(cond ((null window)
	       (setq window (make-window :DISPLAY display :ID id))
	       (save-id display id window))
	      (#-lucid (type? window 'DRAWABLE)
	       #+lucid (or (eq (sys:structure-type window) 'window)
			   (eq (sys:structure-type window) 'pixmap))
	       window)
	      (t (x-error 'LOOKUP-ERROR
			  :ID id :DISPLAY display :TYPE 'WINDOW :OBJECT window)))
	(setq *Last-CLX-Window-Display-Found* display
	      *Last-CLX-Window-ID-Found* id)
	(setq *Last-CLX-Window-Found* window))))

;; definition used in above function.
#+ignore
(defun lookup-resource-id (display id)
  ;; Find the object associated with resource ID
  (gethash id (display-resource-id-map display)))




;;; WARNING:
;;;	CLX performance will be severely degraded if your lisp uses
;;;	write-byte to send all data to the X Window System server.
;;;	You are STRONGLY encouraged to write a specialized version
;;;	of buffer-write-default that does block transfers.

;; I probably don't even need this redefinition since I have redefined buffer-write
;; to directly call user:write-array.  Just didn't seem to make sense to have a separate
;; function for LUCID.
#+(and (not CLX-MIT-R4) lucid)
(defun buffer-write-default (vector display start end)
  ;; The default buffer write function for use with common-lisp streams
  (declare (type buffer-bytes vector)
	   (type display display)
	   (type array-index start end))
  #.(declare-buffun)
  (user::write-array (display-output-stream display) vector
		     start end)
  #+ignore
  (with-vector (vector buffer-bytes)
    (do ((stream (display-output-stream display))
	 (index start (index+ index 1)))
	((index>= index end))
      (declare (type stream stream)
	       (type array-index index))
      (write-byte (aref vector index) stream))
    ))


;; used to call buffer-write-default - now open coded.

#+(and (not CLX-MIT-R4) lucid)
(defun buffer-write (vector buffer start end)
  ;; Write out VECTOR from START to END into BUFFER
  ;; Internal function, MUST BE CALLED FROM WITHIN WITH-BUFFER
  (declare (type buffer buffer)
	   (type array-index start end))
  (if (buffer-dead buffer)
      (x-error 'closed-display :display buffer)
      (user::write-array (display-output-stream buffer) vector
			 start end))
  nil)


;; orignal version - not expansion of wrap-buf-output just check buffer-dead - so we
;; were checking it twice.
#+ignore
(defun buffer-write (vector buffer start end)
  ;; Write out VECTOR from START to END into BUFFER
  ;; Internal function, MUST BE CALLED FROM WITHIN WITH-BUFFER
  (declare (type buffer buffer)
	   (type array-index start end))
  (when (buffer-dead buffer)
    (x-error 'closed-display :display buffer))
  (wrap-buf-output buffer
    (user::write-array (display-output-stream buffer) vector
		       start end))
  #+ignore
  (wrap-buf-output buffer
    (funcall (buffer-write-function buffer) vector buffer start end))
  nil)

;;; WARNING:
;;;	CLX performance will suffer if your lisp uses read-byte for
;;;	receiving all data from the X Window System server.
;;;	You are encouraged to write a specialized version of
;;;	buffer-read-default that does block transfers.
#+ignore
(defmacro CL-read-bytes (stream vector start end)
  `(do* ((i ,start (index+ i 1))
	 (c nil))
	((index>= i ,end) nil)
     (declare (type array-index i)
	      (type (or null card8) c))
     (setq c (read-byte ,stream nil nil))
     (if c
	 (setf (aref ,vector i) c)
	 (return t))))

;; We finally figured out how to make this sucker work faster in lucid.
#+(and (not CLX-MIT-R4) lucid)
(defmacro CL-read-bytes (stream vector start end)
  `(if (eq 'eof (user::read-array ,stream ,vector ,start ,end NIL 'eof))
       T NIL))

#+(and (not CLX-MIT-R4) lucid)
(proclaim '(float *buffer-read-polling-time*))

#+(and (not CLX-MIT-R4) lucid)
(defun buffer-input (buffer vector start end &optional timeout)
  ;; Read into VECTOR from the buffer stream
  ;; Timeout, when non-nil, is in seconds
  ;; Returns non-nil if EOF encountered
  ;; Returns :TIMEOUT when timeout exceeded
  (declare (type buffer buffer)
	   (type vector vector)
	   (type array-index start end)
	   (type (or null number) timeout))
  (declare (values eof-p))
  (when (buffer-dead buffer)
    (x-error 'closed-display :display buffer))
  (unless (= start end)
    (let ((stream (display-input-stream buffer)))
      (declare (type stream stream))
      (cond ((or (null timeout)			; timeout = NIL
		 (listen stream))		; OR input waiting
	     (cl-read-bytes stream vector start end))
	    ((zerop timeout)			; timeout = 0 
	     :timeout)				; no input (we listened above)
	    (t					; timeout > 0, so poll until time is up.
	     (multiple-value-bind (npoll fraction)
		 (truncate timeout *buffer-read-polling-time*)
	       (if (or (listen stream)		; listen first
		       (dotimes (i npoll)	; Sleep for a time, then listen again
			 (sleep *buffer-read-polling-time*)
			 (when (listen stream) (return t)))
		       (when (plusp fraction)
			 (sleep fraction)	; Sleep a fraction of a second
			 (listen stream)))	; and listen one last time
		   (cl-read-bytes stream vector start end)
		   :timeout)))))))

;; orignal version -seems a waste to have this indirection when it isn't used.

#+ignore
(defun buffer-input  (buffer vector start end &optional timeout)
  ;; Read into VECTOR from the buffer stream
  ;; Timeout, when non-nil, is in seconds
  ;; Returns non-nil if EOF encountered
  ;; Returns :TIMEOUT when timeout exceeded
  (declare (type buffer buffer)
	   (type vector vector)
	   (type array-index start end)
	   (type (or null number) timeout ()))
  (declare (values eof-p))
  (when (buffer-dead buffer)
    (x-error 'closed-display :display buffer))
  (unless (= start end)
    (funcall (buffer-input-function buffer) buffer vector start end timeout)))


#-CLX-MIT-R4
(defstruct (font (:constructor make-font-internal) (:print-function print-xlib-font))
  (id-internal nil :type (or null resource-id)) ;; NIL when not opened
  (display nil :type (or null display))
  (reference-count 0 :type fixnum)
  (name "" :type (or null string)) ;; NIL when ID is for a GContext
  (font-info-internal nil :type (or null font-info))
  (char-infos-internal nil :type (or null vector))
  (local-only-p t :type boolean) ;; When T, always calculate text extents locally
  (plist nil :type list)			; Extension hook
  )

#-CLX-MIT-R4
(defun print-xlib-font (font stream &REST args)
  (declare (ignore args))
  (format stream "#<Xfont ~D ~D>" (font-name font) (font-id-internal font)))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
