;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************



(cl:in-package 'ew)


(defvar *Days-Of-The-Week*
	'("Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday" "Sunday"))

(defvar *Months* '("January" "February" "March" "April" "May" "June" "July" "August"
		   "September" "October" "November" "December"))

(defvar *Month-Lengths* '(31 28 31 30 31 30 31 31 30 31 30 31))

(defun month-length (month year)
  (declare (fixnum month))
  (if (= month 2)
      (if (leap-year-p year) 29. 28.)
      (nth (%1- month) *Month-Lengths*)))

(defun month-string (month &OPTIONAL (mode :long))
  (let ((month (nth (%1- month) *Months*)))
    (if (eq mode :long) month (subseq month 0 3))))

(defun day-of-the-week-string (day &OPTIONAL (mode :long))
  (let ((day (nth day *Days-Of-The-Week*)))
    (if (eq mode :long) day (subseq day 0 3))))

(defun leap-year-p (year)
  (declare (fixnum year))
  (and (zerop (rem year 4))
       (or (not (zerop (rem year 100)))
	   (zerop (rem year 400)))))

(define-type universal-time (() &KEY base-time past-p must-have timezone long-date european-p
			     brief)
  :PRINTER ((time stream)
	    (multiple-value-bind (second minute hour date month year day-of-week
				  daylight-saving-time-p time-zone)
		(decode-universal-time time)
	      (declare (ignore daylight-saving-time-p time-zone))	;5/1/90 (sln)
	      (declare (fixnum year hour month date minute second))
	      (if long-date
		  (format stream "~A the ~:R of ~A, ~D; ~2,'0D:~2,'0D~:[~*~;:~2,'0D~] ~A"
			  (nth day-of-week *Days-Of-The-Week*) date
			  (nth (the fixnum (1- month)) *Months*)
			  year
			  (the fixnum (mod hour 12.))
			  minute (not brief) second
			  (cond ((%= hour 0) " midnight")
				((%= hour 12) " noon")
				((%> hour 12) " pm")
				((%> hour 12) " am")))
		  (princ (with-output-to-string (string)
			   (if european-p
			       (progn
				 (when (< date 10.) (write-char #\0 string))
				 (princ date string)
				 (write-char #\/ string)
				 (when (< date 10.) (write-char #\0 string))
				 (princ month string))
			       (progn
				 (when (< date 10.) (write-char #\0 string))
				 (princ month string)
				 (write-char #\/ string)
				 (when (< date 10.) (write-char #\0 string))
				 (princ date string)))
			   (write-char #\/ string)
			   (princ (%- year 1900) string)
			   (write-char #\Space string)
			   (when (< hour 10.) (write-char #\0 string))
			   (princ hour string)
			   (write-char #\: string)
			   (when (< minute 10.) (write-char #\0 string))
			   (princ minute string)
			   (unless brief
			     (write-char #\: string)
			     (when (< second 10.) (write-char #\0 string))
			     (princ second string)))
			 stream)
		  #+IGNORE
		  (format stream "~2,'0D/~2,'0D/~D ~2,'0D:~2,'0D~:[~*~;:~2,'0D~]"
			  (if european-p date month)
			  (if european-p month date)
			  (%- year 1900)
			  hour minute (not brief) second)
		  )))
  :PARSER ((stream)
	   (let ((date (read-standard-token stream)))
	     (setq date (parse-universal-time date european-p))
	     (if (integerp date)
		 date
		 (parse-error "Not a Valid Date/Time" 'UNIVERSAL-TIME)))))

(defun parse-universal-time (string &OPTIONAL european-p)
  ;; very simple time parser.
  ;; handles the following cases "mm/dd/yr" "mm-dd-yr" "mm dd, yr"
  (or (parse-standard-time-format string european-p)
      (parse-long-hand-time-format string)))


(defun find-first-char (string index length)
  (declare (fixnum index length) (string string))
  (do () ((%= index length) length)
    (when (not (char-equal (aref string index) #\Space))
      (return index))
    (incf index)))


(defun read-number-of-size (string index length min-size max-size)
  (declare (values number final-index) (fixnum index length min-size max-size))
  ;; look for first non space character
  (setq index (find-first-char string index length))
  (do ()
      ((< max-size min-size) (values NIL index))
    (when (>= (%- length index) max-size)
      (let ((all-digits-p
	      (dotimes (x max-size T)
		(unless (digit-char-p (aref string (%+ index x)))
		  (return NIL)))))
	(if all-digits-p
	    (return (values (read-from-string string NIL NIL
					      :START index :END (%+ index max-size))
			    (%+ index max-size))))))
    (decf max-size)))


(defun read-year (string index length)
  ;; look for first non space character
  (setq index (find-first-char string index length))
  (multiple-value-bind (year new-index)
      (read-number-of-size string index length 4 4)
    (if year
	(values year new-index)
	(multiple-value-bind (year new-index)
	    (read-number-of-size string index length 2 2)
	  (if year
	      (values (%+ 1900 year) new-index)
	      (values NIL index))))))


(defun read-date-time-delimiter (type string index length)
  (declare (fixnum index length))
  (let ((delimiters (ecase type
		      (:DATE '(#\\ #\/ #\-))
		      (:TIME '(#\: #\;)))))
    (setq index (find-first-char string index length))
    (cond ((< index length)
	   (if (member (aref string index) delimiters :TEST #'CHAR-EQUAL)
	       (values T (%1+ index))
	       (values NIL index)))
	  (T (values NIL index)))))

(defun look-for-am-pm (string index length)
  #.(fast)
  (declare (fixnum index length))
  (setq index (find-first-char string index length))
  (flet ((find-string (strings)
	   (some #'(lambda (str)
		     (declare (string str))
		     (and (>= (%- length index) (the fixnum (length str)))
			  (string-equal str string
					:END1 (length str)
					:START2 index
					:END2 (%+ index (length str)))
			  (%+ index (length str))))
		 strings)))
    (let ((am (find-string '("a.m." "a.m" "am." "am"))))
      (if am
	  (values :AM am)
	  (let ((pm (find-string '("p.m." "p.m" "pm." "pm"))))
	    (if pm (values :PM pm)
		(values NIL index)))))))

(defun parse-standard-time-format (string &OPTIONAL european-p)
  #.(fast)
  (declare (string string))
  ;; look for time specified like the following mm/dd/yr hh/mm/ss
  (setq string (string-trim '(#\Space) string))
  (let ((day 0) (month 0) (year 0) (hour 0) (minute 0) (second 0)
	(length (length string)) (index 0) valid-delimiter-p am/pm)
    (flet ((encode-time (seconds minutes hours day month year)
	     (when european-p (psetq day month month day))
	     (encode-universal-time seconds minutes hours day month year)))
      (multiple-value-setq (month index)
	(read-number-of-size string index length 1 2))
      (when month
	(multiple-value-setq (valid-delimiter-p index)
	  (read-date-time-delimiter :DATE string index length))
	(when valid-delimiter-p
	  (multiple-value-setq (day index)
	    (read-number-of-size string index length 1 2))
	  (when day
	    (multiple-value-setq (valid-delimiter-p index)
	      (read-date-time-delimiter :DATE string index length))
	    (when valid-delimiter-p
	      (multiple-value-setq (year index)
		(read-year string index length))
	      (when year
		;; now we don't have to have a time afterwards, but let's look for it.
		;; either there is a proper time or there is nothing.
		(setq index (find-first-char string index length))
		(if (= index length)
		    ;; we are done.
		    (encode-time 0 0 0 day month year)
		    ;; must have some kind of time group as well.
		    (progn
		      (multiple-value-setq (hour index)
			(read-number-of-size string index length 1 2))
		      (when hour
			(multiple-value-setq (valid-delimiter-p index)
			  (read-date-time-delimiter :TIME string index length))
			(when valid-delimiter-p
			  (multiple-value-setq (minute index)
			    (read-number-of-size string index length 2 2))
			  (when minute
			    ;; look for possible end here as well.
			    (setq index (find-first-char string index length))
			    (if (= index length)
				;; the end again.
				(encode-time 0 minute hour day month year)
				;; look for possible am/pm.
				(progn
				  (multiple-value-setq (am/pm index)
				    (look-for-am-pm string index length))
				  (if am/pm
				      (encode-time 0 minute (if (eq am/pm :PM)
								(%+ 12 hour)
								hour)
						   day month year)
				      ;; well look for seconds now.
				      (progn
					(multiple-value-setq (valid-delimiter-p index)
					  (read-date-time-delimiter :TIME string index length))
					(when valid-delimiter-p
					  (multiple-value-setq (second index)
					    (read-number-of-size string index length 2 2))
					  (when second
					    ;; look for am/pm
					    (multiple-value-setq (am/pm index)
					      (look-for-am-pm string index length))
					    (if am/pm
						(encode-time
						  second minute (if (eq am/pm :PM)
								    (%+ 12 hour)
								    hour)
						  day month year)
						(encode-time
						  second minute hour
						  day month year)))))))))))))))))))))



(defun parse-long-hand-time-format (string)
  #.(fast)
  (let ((month (do ((months *Months* (cdr months))
		    (x 1 (%1+ x)))
		   ((null months))
		 (declare (fixnum x))
		 (when (search (first months) string :END1 3)
		   (return x)))))
    (when month
      (let ((first-digit (position-if #'digit-char-p string)))
	(when first-digit
	  (multiple-value-bind (day rest-of-string)
	      (read-from-string string NIL NIL :START first-digit)
	    (when (numberp day)
	      (let ((next-digit (position-if #'digit-char-p string :start rest-of-string)))
		(let ((year (read-from-string string NIL NIL :start next-digit)))
		  (when (numberp year)
		    (encode-universal-time 0 0 0 day month
					   (if (%< year 100) (%+ year 1900) year))))))))))))


(defvar *Time-Words*
	'(("Seconds" . :second)
	  ("Second" . :second)
	  ("Sec" . :second)
	  ("S" . :second)
	  ("Minutes" . :minute)
	  ("Minute" . :minute)
	  ("Mins" . :minute)
	  ("Min" . :minute)
	  ("M" . :minute)
	  ("Hours" . :hour)
	  ("Hour" . :hour)
	  ("H" . :hour)
	  ("Days" . :day)
	  ("Day" . :day)
	  ("D" . :day)
	  ("Months" . :month)
	  ("Month" . :month)
	  ("Years" . :year)
	  ("Year" . :year)
	  ("Yr" . :year)))

(defun parse-interval-or-never (string &OPTIONAL start end)
  #.(fast)
  (declare (string string))
  ;; look for any time frame
  (if (equal string "Never")
      NIL
      (let ((year 0) (month 0) (day 0) (hour 0) (minute 0) (second 0)
	    (index (or start 0))
	    (length (if end (min end (length string)) (length string)))
	    (value 0))
	(declare (fixnum year month day hour minute second index length))
	(do ()
	    (())
	  (when (or (not index) (>= index length)) (return NIL))
	  ;; look for a number first.
	  (multiple-value-setq (value index)
	    (read-number-of-size string index length 1 999))
	  ;; look for next non-space character
	  (setq index (position #\Space string :start index :test-not #'EQL))
	  (if (and index value)
	      ;; look for time units
	      (unless
		(dolist (unit-pair *Time-Words* NIL)
		  (declare (list unit-pair))
		  (when (string-equal (the string (first unit-pair)) string
				      :END1 (length (the string (first unit-pair)))
				      :START2 index
				      :END2 (min (%+ (length (the string (first unit-pair)))
						     index)
						 length))
		    (ecase (cdr unit-pair)
		      (:second (setq second value))
		      (:minute (setq minute value))
		      (:hour (setq hour value))
		      (:day (setq day value))
		      (:month (setq month value))
		      (:year (setq year value)))
		    (incf index (length (first unit-pair)))
		    (return T)))
		(parse-error "Couldn't find a valid time unit" 'time-interval))
	      (if (position #\Space string :test-not #'eql :start index)
		  (parse-error "Couldn't find a number time where expected" 'time-interval)
		  (return NIL))))
	(the integer (+ (the integer (* year #.(* 365. 24. 3600.)))
			(%* month #.(* 30. 24. 3600.))
			(%* day #.(* 24. 3600.))
			(%* hour #.(* 3600.))
			(%* minute 60.)
			second)))))

(defun print-interval-or-never (interval &OPTIONAL (stream *Standard-Output*))
  #.(fast)
  (declare (integer interval))
  (if (not interval)
      (princ "Never" stream)
      (multiple-value-bind (years remainder)
	  (floor interval #.(* 365. 24. 3600.))
	(declare (fixnum years))
	(multiple-value-bind (days remainder)
	    (floor remainder #.(* 24. 3600.))
	  (declare (fixnum days remainder))
	  (multiple-value-bind (hours remainder)
	      (floor remainder #.(* 3600.))
	    (declare (fixnum hours remainder))
	    (multiple-value-bind (minutes seconds)
		(floor remainder #.(* 60.))
	      (declare (fixnum minutes seconds))
	      (format stream "~:[~*~;~D year~:P~]~:[~2*~;~:[~; ~]~D day~:P~]~
				~:[~2*~;~:[~; ~]~D hour~:P~]~
				~:[~2*~;~:[~; ~]~D minute~:P~]~:[~2*~;~:[~; ~]~D second~:P~]"
		      (plusp years) years
		      (plusp days)
		      (plusp years) days
		      (plusp hours)
		      (or (plusp days) (plusp years)) hours
		      (plusp minutes)
		      (or (plusp years) (plusp days) (plusp hours)) minutes
		      (plusp seconds)
		      (or (plusp years) (plusp days) (plusp hours) (plusp minutes))
		      seconds)))))))

(defun read-interval-or-never (&OPTIONAL stream-or-nil)
  (query 'time-interval :stream stream-or-nil :PROMPT NIL :provide-default NIL))

(define-type time-interval (())
  :PARSER ((stream)
	   (let ((token (read-standard-token stream)))
	     (parse-interval-or-never token)))
  :PRINTER ((time stream)
	    (print-interval-or-never time stream)))

(define-type time-interval-60ths (())
  :PARSER ((stream)
	   (let ((value (query 'time-interval :STREAM stream :PROMPT NIL
				:PROVIDE-DEFAULT NIL)))
	     (declare (integer value))
	     (* value 60.)))
  :PRINTER ((time stream)
	    (print-interval-or-never (floor time 60.) stream)))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
