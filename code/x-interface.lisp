;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-Windows; Base: 10; Patch-File:T -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)

(defmethod print-scroll-bar ((window window) stream ignore)
  (format stream "#<Scroll Bar ~A>" (window-margin-type window)))

(defmethod print-label ((window window) stream ignore)
  (format stream "#<Label ~A>" (label-string window)))

(defmethod print-presentation-window ((window window) stream ignore)
  (format stream "#<PW ~A>" (window-real-window window)))

(defun allocate-fake-window (&OPTIONAL (stream-to-copy NIL) (copy-cursorpos-p NIL)
			     (use-full-screen-size-p NIL))
  #.(fast)
  (let ((window (if *Fake-Windows*
		    (prog1 (first *Fake-Windows*)
			   (setf *Fake-Windows*
				 (prog1 (cdr *Fake-Windows*)
					(setf (cdr *Fake-Windows*) *Allocated-Windows*)
					(setq *Allocated-Windows* *Fake-Windows*))))
		    (progn
		      #+meter-consing (lisp:format T "~&Consing3")
		      (make-fake-window)))))
    (setf (presentation-window-presentations window) NIL
	  (window-scroll-x-offset window) 0
	  (window-scroll-y-offset window) 0
	  (window-x-pos window) 0
	  (window-y-pos window) 0)
    (if stream-to-copy
	(progn
	  (setf (window-line-height window) (window-line-height stream-to-copy)
		(window-height window) (window-height stream-to-copy)
		(window-width window) (window-width stream-to-copy)
		(window-line-spacing window) (window-line-spacing stream-to-copy)
		(window-character-style window) (window-character-style stream-to-copy)
		(window-font window) (window-font stream-to-copy))
	  (if copy-cursorpos-p
	      (multiple-value-bind (x y) (read-cursorpos stream-to-copy)
		(setf (window-x-pos window) x
		      (window-y-pos window) y))))
	(setf (window-character-style window) '(:fix :roman :normal)
	      (window-font window) (xfonts:get-font-from-style '(:fix :roman :normal))))
    (when use-full-screen-size-p
      (setf (window-height window) (xlib:screen-height *Screen*)
	    (window-width window) (xlib:screen-width *Screen*)))
    window))

(defun deallocate-fake-window (window)
  (if *Allocated-Windows*
      (setq *Allocated-Windows*
	    (prog1 (cdr *Allocated-Windows*)
		   (setf (cdr *Allocated-Windows*)
			 *Fake-Windows*)
		   (setq *Fake-Windows* *Allocated-Windows*)
		   (setf (first *Fake-Windows*) window)))
      (push window *Fake-Windows*)))

(defun add-windows (&REST windows)
  (setq *Windows* (append windows *Windows*)))



(defun find-window (real-window)
  #.(fast)
  (cond ((eq real-window *Last-Found-Window-Real-Window*)
	 *Last-Found-Window*)
	((null real-window) NIL)
	((window-p real-window) real-window)
	(T (setq *Last-Found-Window-Real-Window* real-window)
	   (setq *Last-Found-Window*
		 (do ((windows *Windows* (cdr windows)))
		     ((null windows) NIL)
		   (let ((w (first windows)))
		     (and (eq (window-real-window w) real-window)
			  (return w))))))))








;; for X windows - ALU is handled by :FUNCTION keyword to create-gcontext.
(defvar *Draw-Alu-Value* 0)
(defvar *Erase-Alu-Value* 0)
(defvar *Flip-Alu-Value* 0)
(defvar *Magic-Alu-Value* 0)

(proclaim '(fixnum *Draw-Alu-Value* *Erase-Alu-Value* *Flip-Alu-Value* *Magic-Alu-Value*))


;;; It seems that on XNEWS that the pixel returned is 0 or 255.
;;; bug fixed Nov 7, 89

(defun init-alu-values (black-color)
  (ecase black-color
    #+excl
    ((255 1) (setq *Draw-Alu-Value* 7
	     *Erase-Alu-Value* 0
	     *Flip-Alu-Value* 8
	     *Magic-Alu-Value* 2))
    #-excl
    ((255 1) (setq *Draw-Alu-Value* 7
	     *Erase-Alu-Value* 2
	     *Flip-Alu-Value* 6
	     *Magic-Alu-Value* 5))
    (0 (setq *Draw-Alu-Value* 1
	     *Erase-Alu-Value* 15
	     *Flip-Alu-Value* 9
	     *Magic-Alu-Value* 10))))

;; for allegro clx
;; 0 seems to set to zero.
;; 1. seems to set to 1.
;; 2 seems to set to pattern.
;; 3. seems to set to destination.
;; 4 seems to set to inverse of pattern.
;; 5 seems to xor a 1 with destination.
;; 6 seems to and destination with source.
;; 7 seems to ior
;; 8 xor
;; 9 xor inverse of source with destination.
;; 10 seems to wierd 
;; 11 seems to and of invers of source with inverse of destination.
;; 12 ands inverse of source with destination.
;; 13 ands inverse of destination with source.
;; 14 ior destination with inverse of source.
;; 15 ??

;; for X windows - ALU is handled by :FUNCTION keyword to create-gcontext.
(defun draw-alu (alu)
  (if (integerp alu) alu
      ;; these are from X really.
      (case alu
	(:DRAW *draw-alu-value*)
	(:FLIP *flip-alu-value*)
	(:ERASE *erase-alu-value*)
	(:magic *magic-alu-value*)
	(T alu))))
;GXclear                 0x0             /* 0 */
;GXand                   0x1             /* src AND dst */
;GXandReverse            0x2             /* src AND NOT dst */
;GXcopy                  0x3             /* src */
;GXandInverted           0x4             /* NOT src AND dst */
;GXnoop                  0x5             /* dst */
;GXxor                   0x6             /* src XOR dst */
;GXor                    0x7             /* src OR dst */
;GXnor                   0x8             /* NOT src AND NOT dst */
;GXequiv                 0x9             /* NOT src XOR dst */
;GXinvert                0xa             /* NOT dst */
;GXorReverse             0xb             /* src OR NOT dst */
;GXcopyInverted          0xc             /* NOT src */
;GXorInverted            0xd             /* NOT src OR dst */
;GXnand                  0xe             /* NOT src OR NOT dst */
;GXset                   0xf             /* 1 */



(defmethod print-window ((window window) stream ignore)
  (format stream "#<Window ~A>" (window-real-window window)))

(defmethod print-fake-window ((window window) stream ignore)
  (format stream "#<Fake Window ~A>" (window-real-window window)))


(defclass cursor ()
  ((state :accessor cursor-state :initarg :state :INITFORM :OFF :TYPE (member :off :on NIL))
   (width :accessor cursor-width :initarg :width :INITFORM 8 :TYPE FIXNUM)
   (height :accessor cursor-height :initarg :height :INITFORM 16 :TYPE FIXNUM)
   (gcontext :accessor cursor-gcontext :initarg :gcontext :INITFORM NIL)
   )
  ;(:accessor-prefix cursor-)
  )


(defmacro prepare-window ((window) &BODY body)
  (let ((cursor-active-p-var (gensym "CURSOR-ACTIVE-P-VAR")))
    `(let ((,cursor-active-p-var (and (window-cursor ,window)
				      (eq :ON (cursor-state (window-cursor ,window))))))
       (when ,cursor-active-p-var (setf (cursor-state (window-cursor ,window)) NIL))
       (unwind-protect
	   (progn
	     (and ,cursor-active-p-var (draw-cursor (window-cursor ,window) ,window))
	     . ,body)
	 (when ,cursor-active-p-var
	   (draw-cursor (window-cursor ,window) ,window)
	   (setf (cursor-state (window-cursor ,window)) :ON))))))


;; This is used in function such as query to ensure that there is an active cursor
;; on the window even if it is normally turned off or doesn't exist.
(defmacro enable-cursor ((window) &BODY body)
  (let ((cursor-active-p-var (gensym "CURSOR-ACTIVE-P-VAR")))
    `(let ((,cursor-active-p-var (and (window-cursor ,window)
				      (eq :ON (cursor-state (window-cursor ,window))))))
       (unless ,cursor-active-p-var
	 ;; make sure we have an active cursor.
	 (unless (window-cursor ,window)
	   (setf (window-cursor ,window)
		 #+EW-CLOS (make-instance 'cursor) #-EW-CLOS (make-cursor))
	   (let ((black (xlib:screen-black-pixel *Screen*))
		 (white (xlib:screen-white-pixel *Screen*)))
	     (setf (cursor-gcontext (window-cursor ,window))
		   (xlib:create-gcontext :DRAWABLE (window-real-window ,window)
					 :BACKGROUND white
					 :FOREGROUND black :FUNCTION (draw-alu :FLIP)))))
	 (setf (cursor-state (window-cursor ,window)) :ON))
       (unwind-protect
	   (progn
	     ;; draw it unless we already had one.
	     (or ,cursor-active-p-var (draw-cursor (window-cursor ,window) ,window))
	     . ,body)
	 (unless ,cursor-active-p-var
	   ;; erase it because it was orignally off.
	   (draw-cursor (window-cursor ,window) ,window)
	   (setf (cursor-state (window-cursor ,window)) NIL))))))

(defmethod recompute-window-margins ((window window))
  #.(fast)
  (when (or (presentation-window-p window)
	    (lisp:typep window 'LABEL))
    (let ((left 0) (top 0) (right 0) (bottom 0))
      (declare (fixnum left top right bottom))
      (when (presentation-window-p window)
	(dolist (margin (presentation-window-margins window))
	  (ecase (window-margin-type margin)
	    (:LEFT (incf left (window-width margin)))
	    (:TOP (incf top (window-height margin)))
	    (:RIGHT (incf right (window-width margin)))
	    (:BOTTOM (incf bottom (window-height margin))))))
      (multiple-value-bind (old-x old-y)
	  (read-cursorpos window)
	(setf (window-left-margin-size window) left
	      (window-top-margin-size window) top
	      (window-right-margin-size window) right
	      (window-bottom-margin-size window) bottom)
	(set-cursorpos window old-x old-y)))))

(defmethod recompute-margin-sizes ((window window))
  #.(fast)
  (when (presentation-window-p window)
    (multiple-value-bind (left top right bottom)
	(window-edges window)
      ;; outside absolute boundaries of window.
      (declare (fixnum left top right bottom))
      (let ((border-width (window-border-margin-width window)))
	(declare (fixnum border-width))
	;; since left,top etc are in global coordinates of the outside
	;; we must first offset them by the size of the X window border edge.
	;; this brings the coordinates to the actual drawing part of the window.
	(incf left border-width)
	(incf top border-width)
	(decf right border-width)
	(decf bottom border-width)
	(flet ((thickness (margin)
		 (typecase margin
		   (SCROLL-BAR *Scroll-Bar-Thickness*)
		   (LABEL (window-height margin)))))
	  (dolist (margin (presentation-window-margins window))
	    ;;(dformat "~%Bottom ~D before margin ~A." bottom margin)
	    (let ((thickness (thickness margin)))
	      (ecase (window-margin-type margin)
		(:LEFT
		  ;; position it on the left side of the window
		  (set-window-edges margin
				    left top (%+ left thickness 2) bottom)
		  (incf left (%+ thickness 2)))
		(:RIGHT
		  ;; position it on the left side of the window
		  (set-window-edges margin
				    (%- right thickness 2)
				    top right bottom)
		  (decf left (%+ thickness 2)))
		(:TOP
		  (set-window-edges margin
				    left top right (%+ top thickness 2))
		  (incf top (%+ thickness 2)))
		(:BOTTOM
		  (set-window-edges margin
				    left (%- bottom (%+ thickness 2))
				    right bottom)
		  (decf bottom (%+ thickness 2)))))))))))

;;; SET-WINDOW-INSIDE-SIZE sets the size of the window, in reality the window may be
;;; bigger because of the margins.
(defmethod set-window-inside-size ((window window) new-inside-width new-inside-height)
  (declare (fixnum new-inside-width new-inside-height))
  (let ((border-width (window-border-margin-width window)))
    (declare (fixnum border-width))
    ;; we are going to limit the windows size to be smaller than the parent
    (unless (fake-window-p window)
      (multiple-value-bind (window-left window-top)
	  (window-edges window)
	(multiple-value-bind (superior-left superior-top)
	    (window-edges (window-superior window))
	  (setf new-inside-width  (min new-inside-width
				       (%- (window-inside-width (window-superior window))
					   (%- window-left superior-left)
					   border-width border-width
					   (window-left-margin-size window)
					   (window-right-margin-size window))))
	  ;;#+ignore
	  (dformat "~%Super Ins Height ~D window-top ~D Sup Top ~D Border-width ~D Top Margin ~D Bottom Margin ~D."
		   (window-inside-height (window-superior window))
		   window-top superior-top
		   border-width
		   (window-top-margin-size window)
		   (window-bottom-margin-size window))
	  (setf new-inside-height (min new-inside-height
				       (%- (window-inside-height (window-superior window))
					   (%- window-top superior-top)
					   border-width border-width
					   (window-top-margin-size window)
					   (window-bottom-margin-size window))))))
      (setf (window-width window)  (%+ (window-left-margin-size window) new-inside-width
				       (window-right-margin-size window)
				       border-width border-width)
	    (window-height window) (%+ (window-top-margin-size window) new-inside-height
				       (window-bottom-margin-size window)
				       border-width border-width))
      #+X
      (let ((xwindow (window-real-window window)))
	(xlib:with-state (xwindow)
	  (setf (xlib:drawable-width xwindow) (%- (window-width window)
						  border-width border-width)
		(xlib:drawable-height xwindow) (%- (window-height window)
						   border-width border-width)))
	(sync))
      (recompute-margin-sizes window)
      #+(and symbolics (not x))
      (scl:send (window-real-window window) :SET-INSIDE-SIZE new-width new-height))))



;;; SET-WINDOW-SIZE sets the size of the window.  This includes the X window borders.
(defmethod set-window-size ((window window) new-width new-height)
  (declare (fixnum new-width new-height))
  (setf (window-width window) new-width
	(window-height window) new-height)
  #+X
  (let ((xwindow (window-real-window window))
	(border-width (window-border-margin-width window)))
    (declare (fixnum border-width))
    (xlib:with-state (xwindow)
      (setf (xlib:drawable-width xwindow) (%- new-width border-width border-width)
	    (xlib:drawable-height xwindow) (%- new-height border-width border-width)))
    (sync))
  ;; we also need to fix scroll bars
  (recompute-margin-sizes window)
  #+(and symbolics (not x))
  (scl:send (window-real-window window) :SET-INSIDE-SIZE new-width new-height))

;; X,Y are relative to its superiors outer edges.  So an X of 0 would mean to align the two
;; edges.
(defmethod set-window-position ((window window) x y &OPTIONAL (keep-x-y-fixed-if-possible-p T))
  #.(fast)
  (declare (fixnum x y))
  (maximize x 0) (maximize y 0)
  (when (window-superior window)
    (multiple-value-bind (sup-left sup-top sup-right sup-bottom)
	(window-edges (window-superior window))
      (declare (fixnum sup-left sup-top sup-right sup-bottom))
      (when (or (>= x (%- sup-right sup-left))
		(>= y (%- sup-bottom sup-top)))
	(setq x 0 y 0))
      (multiple-value-bind (left top right bottom)
	  (window-edges window)
	(declare (fixnum left top right bottom))
	;; we want a new top,left for the window, but we want the width
	;; to stay the same if possible.
	;; set the top,left and clip the width,height if necesary.
	(let* ((new-left (%+ x sup-left))
	       (new-top (%+ y sup-top))
	       (new-right (%+ new-left (%- right left)))
	       (new-bottom (%+ new-top (%- bottom top))))
	  (declare (fixnum new-left new-top new-right new-bottom))
	  ;; now check for window going outside boundaries
	  ;; if so, chop its size.
	  (when (> new-right sup-right)
	    (if keep-x-y-fixed-if-possible-p
		(setq new-right sup-right)
		;; move left over until right fits.
		(let* ((overhang (%- new-right sup-right))
		       (amount-left-can-absorb (min (the fixnum overhang) new-left)))
		  (setq new-left (%- new-left amount-left-can-absorb)
			new-right (min sup-right (%- new-right overhang))))))
	  (when (> new-bottom sup-bottom)
	    (if keep-x-y-fixed-if-possible-p
		(setq new-bottom sup-bottom)
		;; move top over until bottom fits.
		(let* ((overhang (%- new-bottom sup-bottom))
		       (amount-top-can-absorb (min (the fixnum overhang) new-top)))
		  (setq new-top (- new-top amount-top-can-absorb)
			new-bottom (min sup-bottom (%- new-bottom overhang))))))
	  (set-window-edges window new-left new-top new-right new-bottom))))))



(defmethod window-position ((window window))
  #.(fast)
  (declare (values relative-x relative-y))
  (let ((superior (window-superior window)))
    (if (not superior)
	(values 0 0)
	(values (%- (window-left window) (window-left superior))
		(%- (window-top window) (window-top superior))))))

(defmethod center-window-around ((window window) x y)
  (multiple-value-bind (width height)
      (window-size window)
    (set-window-position window
			 (%- x (the (values fixnum number)  (floor width 2)))
			 (%- y (the (values fixnum number)  (floor height 2)))
			 NIL)))


;       (:point x y)
;       (:mouse)
; In addition, if warp-mouse-p is
;             non-nil, the mouse is warped to the center of the window. (The
;             mouse only moves if the window is near an edge of its
;             superior; otherwise the mouse is already at the center of the
;             window.)
;
;       (:rectangle left top right bottom)
;             The four arguments specify a rectangle, in pixels, relative to
;             the superior window. The window is positioned somewhere next
;             to but not overlapping the rectangle. In addition, if
;             warp-mouse-p is non-nil, the mouse is warped to the center of
;             the window.
;
;       (:window window-1 window-2 window-3 ...)
;             Position the window somewhere next to but not overlapping the
;             rectangle that is the bounding box of all the window-ns. You
;             must provide at least one window. Usually you only give one,

(defmethod expose-window-near ((window window) mode &OPTIONAL (warp-mouse-p T))
  (when (consp mode)
    (ecase (first mode)
      (:POINT (if (not (and (numberp (second mode)) (numberp (third mode))))
		  (error "Mode arg ~A not right."))
       (center-window-around window (second mode) (third mode)))
      (:MOUSE
	(multiple-value-bind (left top)
	    (window-edges (window-superior window))
	  (let ((x (%- *Global-Mouse-X* left))
		(y (%- *Global-Mouse-Y* top)))
	    (center-window-around window x y)))
	(when warp-mouse-p
	  (multiple-value-bind (left top right bottom)
	      (window-edges window)
	    (mouse-warp (the (values fixnum number)  (floor (%+ left right) 2))
			(the (values fixnum number)  (floor (%+ top bottom) 2))))))
      ((:WINDOW :RECTANGLE)
       (let ((left 0) (top 0) (right 0) (bottom 0))
	 (declare (fixnum left top right bottom))
	 (if (eq (first mode) :WINDOW)
	     (progn (multiple-value-setq (left top right bottom)
		      (window-edges (second mode)))
		    (dolist (w (cddr mode))
		      (multiple-value-bind (w-left w-top w-right w-bottom)
			  (window-edges w)
			(declare (fixnum w-left w-top w-right w-bottom))
			(setq left   (min left   w-left)
			      top    (min top    w-top)
			      right  (min right  w-right)
			      bottom (min bottom w-bottom)))))
	     (setq left (second mode)
		   top (third mode)
		   right (fourth mode)
		   bottom (fifth mode)))
	 (when (not (and (numberp left) (numberp top) (numberp right) (numberp bottom)))
	   (error "Mode not right ~A" mode))
	 ;; I guess the idea is to find a side that has enough room to put the window within
	 ;; its superior without overlapping the rectangle.
	 (multiple-value-bind (width height)
	     (window-size window)
	   (declare (fixnum width height))
	   (multiple-value-bind (sup-left sup-top sup-right sup-bottom)
	       (window-edges (window-superior window))
	     (declare (fixnum sup-left sup-top sup-right sup-bottom))
	     ;; first try the left side.
	     (let ((left-room (%- left sup-left))
		   (right-room (%- sup-right right))
		   (top-room (%- top sup-top))
		   (bottom-room (%- sup-bottom bottom)))
	       (declare (fixnum left-room right-room top-room bottom-room))
	       (cond ((< width left-room)
		      ;; put it on the left and align it vertically so that the window
		      ;; top is near the top of the rectangle.
		      (if (> (%+ top height) sup-bottom)
			  (set-window-edges window
					    (%- left width)
					    (%- sup-bottom height)
					    left
					    sup-bottom)
			  (set-window-edges window
					    (%- left width)
					    top
					    left
					    (%+ top height))))
		     ((< width right-room)
		      ;; put it on the left and align it vertically so that the window
		      ;; top is near the top of the rectangle.
		      (if (> (%+ top height) sup-bottom)
			  (set-window-edges window
					    right
					    (%- sup-bottom height)
					    (%+ right width)
					    sup-bottom)
			  (set-window-edges window
					    right
					    top
					    (%+ right width)
					    (%+ top height))))
		     ((< height top-room)
		      ;; put it on the top and align it vertically so that the window
		      ;; top is near the top of the rectangle.
		      (if (> (%+ left width) sup-right)
			  (set-window-edges window
					    (%- sup-right width)
					    (%- top height)
					    sup-right
					    top)
			  (set-window-edges window
					    left
					    (%- top height)
					    (%+ left width)
					    top)))
		     ((< height bottom-room)
		      ;; put it on the left and align it vertically so that the window
		      ;; top is near the top of the rectangle.
		      (if (> (%+ left width) sup-right)
			  (set-window-edges window
					    (%- sup-right width)
					    bottom
					    sup-right
					    (%+ bottom height))
			  (set-window-edges window
					    left
					    bottom
					    (%+ left width)
					    (%+ bottom height))))
		     (;; now where do I put it.  I'm not going to try very hard.
		      ;; just where there is minimal overlap on one of the side.
		      (and (<= (%- width left-room) (%- width right-room))
			   (<= (%- width left-room) (%- height bottom-room))
			   (<= (%- width left-room) (%- height top-room)))
		      ;; there is more room on the left
		      (if (> (%+ top height) sup-bottom)
			  (set-window-edges window
					    0 (%- sup-bottom height)
					    width sup-bottom)
			  (set-window-edges window
					    0 top
					    width (%+ top height))))
		     ((and (<= (%- width right-room) (%- height bottom-room))
			   (<= (%- width right-room) (%- height top-room)))
		      ;; put it on the left and align it vertically so that the window
		      ;; top is near the top of the rectangle.
		      (if (> (%+ top height) sup-bottom)
			  (set-window-edges window
					    (%- sup-right width)
					    (%- sup-bottom height)
					    sup-right
					    sup-bottom)
			  (set-window-edges window
					    (%- sup-right width)
					    top
					    sup-right
					    (%+ top height))))
		     ((and (<= (%- height top-room) (%- height bottom-room)))
		      ;; put it on the top and align it vertically so that the window
		      ;; top is near the top of the rectangle.
		      (if (> (%+ left width) sup-right)
			  (set-window-edges window
					    (%- sup-right width)
					    sup-top
					    sup-right
					    (%+ sup-top height))
			  (set-window-edges window
					    left
					    sup-top
					    (%+ left width)
					    (%+ sup-top height))))
		     (T
		      ;; put it on the top and align it vertically so that the window
		      ;; top is near the top of the rectangle.
		      (if (> (%+ left width) sup-right)
			  (set-window-edges window
					    (%- sup-right width)
					    (%- sup-bottom height)
					    sup-right
					    sup-bottom)
			  (set-window-edges window
					    left
					    (%- sup-bottom height)
					    (%+ left width)
					    sup-bottom)))))))))))
  (expose-window window))

;; In absolute coordinates the outside edges of the window.
;;defmethod window-edges ((window window))

(defun window-edges (window)
  (declare (values left top right bottom))
  (values (window-left window) (window-top window)
	  (%+ (window-left window) (window-width window))
	  (%+ (window-top window) (window-height window))))

;; In absolute coordinates the outside edges of the window.
(defmethod set-window-edges ((window window) left top right bottom)
  (declare (fixnum left top right bottom))
  (if (lisp:typep window 'PROGRAM-FRAMEWORK-INSTANCE)
      (set-program-frame-edges window left top right bottom)
      (let ((new-height (the fixnum (- bottom top)))
	    (new-width (the fixnum (- right left))))
	(declare (fixnum new-height new-width))
	(setf (window-height window)  new-height
	      (window-width window) new-width
	      (window-left window) left
	      (window-top window) top)
	#+X
	(let* ((xwindow (window-real-window window))
	       (superior (window-superior window))
	       (sup-border-width (if superior (window-border-margin-width superior) 0)))
	  (let ((sup-left (if (lisp:typep window 'WINDOW-MARGIN)
			      (%+ (window-left superior) sup-border-width)
			      (%+ (window-left superior) sup-border-width
				  (window-inside-left superior))))
		(sup-top  (if (lisp:typep window 'WINDOW-MARGIN)
			      (%+ (window-top  superior) sup-border-width)
			      (%+ (window-top  superior) sup-border-width
				  (window-inside-top superior))))
		(border-width (window-border-margin-width window)))
	    (xlib:with-state (xwindow)
	      (setf (xlib:drawable-width xwindow) (%- new-width border-width border-width)
		    (xlib:drawable-height xwindow) (%- new-height border-width border-width)
		    (xlib:drawable-x xwindow) (%- left sup-left)
		    (xlib:drawable-y xwindow) (%- top sup-top))))
	  (sync))
	(recompute-margin-sizes window)
	#+(and symbolics (not x))
	(scl:send (window-real-window window) :SET-EDGES left top right bottom))))


;defmethod window-inside-edges ((window window))

(defun window-inside-edges (window)
  (declare (values inside-left inside-top inside-right inside-bottom))
  (let ((border-width (window-border-margin-width window)))
    (typecase window
      (WINDOW
	(values (%+ (window-left-margin-size window) border-width)
		(%+ (window-top-margin-size window) border-width)
		(%- (window-width window) (window-right-margin-size window) border-width)
		(%- (window-height window)
		    (window-bottom-margin-size window) border-width))))))


;; This returns the window size, not counting margins.
;defmethod window-size ((window window))

(defun window-size (window)
  (values (window-width window) (window-height window)))


;defmethod window-inside-size ((window window))

(defun window-inside-size (window)
  (values (window-inside-width window)
	  (window-inside-height window)))



(defvar *Flip-Alu* :FLIP)
(defvar *Draw-Alu* :DRAW)
(defvar *Erase-Alu* :ERASE)

#+(and symbolics (not x))
(defmacro process-wait (whostate function &REST arguments)
  `(scl:process-wait ,whostate ,function . ,arguments))


#+x
(defun process-wait (whostate function &REST arguments)
  (declare (ignore whostate))
  (do ()(())
    (when (apply function arguments)
      (return T))))



(defmethod exposed-window-p ((stream window))
  (setq stream (window-real-window stream))
  (and stream
       #+(and symbolics (not x))
       (scl:send stream :EXPOSED-P)))


(defmethod expose-window ((window window) &AUX real-window)
  (setq real-window (window-real-window window))
  (when real-window
    #+CLX
    (prepare-window (window)
      (xlib:map-window real-window)
      (xlib:circulate-window-up real-window)
      (sync))
    #+(and symbolics (not x))
    (scl:send real-window :EXPOSE)))

(defmethod select-window ((window window))
  (when (window-p window)
    (setq window (window-real-window window)))
  #+CLX
  (progn (xlib:map-window window)
	 (xlib:circulate-window-up window)
	 )
  #+(and symbolics (not x))
  (scl:send window :SELECT))

(defmethod deexpose-window ((window window))
  #+CLX
  (when (window-real-window window)
    ;(xlib:circulate-window-down (window-real-window window))
    (xlib:unmap-window (window-real-window window))
    (sync))
  #+(and symbolics (not x))
  (scl:send (window-real-window window) :DEEXPOSE))

(defmethod ummap-window ((window window))
  (setq window (window-real-window window))
  (when window
    #+CLX
    (xlib:unmap-window window)
    NIL))





(defun read-any (stream)
  #+CLX (declare (ignore stream))
  #+CLX
  (if *Unread-Char*
      (prog1 *Unread-Char* (setq *Unread-Char* NIL))
      (read-internal 'read 0)))

(defun unread-any (window character)
  #+x (declare (ignore window))
  #+x (setq *Unread-Char* character))

(defun peek-any (stream)
  (or *Unread-Char*
      (let ((character (read-any stream)))
	(unread-any stream character)
	character)))

(defun listen-any (stream &OPTIONAL (timeout 5))
  #+X (declare (ignore stream))
  #+X (if *Unread-Char* T (read-internal 'listen timeout)))



(defvar *Windows-Exposure-Ignores* NIL)



#+ignore
(defun tt ()
  (do ()(())
    (when (listen-any NIL) (print (read-any NIL)))
    (print #.*Mouse-x*)))

;defmethod clear-window-internal ((window window)

(defun clear-window-internal (window &OPTIONAL (x 0) (y 0) width height (set-cursorpos-p T))
  (unless (fake-window-p window)
    (prepare-window (window)
      #+X (if (or (not (zerop x)) (not (zerop y)) width height)
	      (let ((border-width (window-border-margin-width window)))
		(xlib:clear-area (window-real-window window)
				 :X (%- x border-width) :Y (%- y border-width)
				 :WIDTH (%+ border-width width)
				 :HEIGHT (%+ border-width height)))
	      (xlib:clear-area (window-real-window window)))))
  (when set-cursorpos-p
    (set-cursorpos window
		   (window-scroll-x-offset window)
		   (window-scroll-y-offset window)))
  )


(defmethod home-cursor ((window window))
  (set-cursorpos window
		 (window-scroll-x-offset window)
		 (window-scroll-y-offset window)))

(defmethod home-down ((window window))
  (set-cursorpos window
		 (window-scroll-x-offset window)
		 (%- (%+ (window-inside-height window)
			 (window-scroll-y-offset window))
		     (window-line-height window))))

(defmethod set-window-label ((window window) label)
  #+(and symbolics (not x))
  (scl:send (window-real-window window) :SET-LABEL label)
  #+X
  (setup-window-label window label))



#+ignore
(defun get-font-width (font)
  #+symbolics (zl:font-char-width font))

(defmacro font-ascent (font)
  #+(and symbolics (not x)) `(zl:font-baseline ,font)
  #+X `(xlib:font-ascent ,font))

(defmacro font-descent (font)
  #+(and symbolics (not x)) (error)
  #+X `(xlib:font-descent ,font))

(defun font-height (font)
  #+(and symbolics (not x)) (zl:font-char-height font)
  #+X (+ (font-ascent font) (font-descent font)))

;defmethod window-font-ascent ((window window))

(defun window-font-ascent (window)
  (font-ascent (window-font window)))

(defmacro recompute-line-height (window font)
  `(progn
     (setf (window-line-height ,window)
	   (%+ (font-height ,font)
	       (window-line-spacing ,window)))
     (maximize (window-maximum-line-height ,window)
	       (window-line-height ,window))
     (window-line-height ,window)))

(defmacro get-font-from-style (style)
  #+(and symbolics (not x))
  `(si:get-font si:*b&w-screen* si:*standard-character-set* ,style)
  #+x
  `(xfonts:get-font-from-style ,style))


(defmethod set-window-style ((window window) new-style &OPTIONAL (set-line-height T))
  (setf (window-character-style window) new-style)
  (setf (window-font window) (get-font-from-style new-style))
  (when set-line-height
    (recompute-line-height window (window-font window))
    (when (and (window-cursor window)
	       (not (= (%- (window-line-height window) (window-line-spacing window))
		       (cursor-height (window-cursor window)))))
      (resize-cursor (window-cursor window) window
		     (cursor-width (window-cursor window))
		     (%- (window-line-height window) (window-line-spacing window)))))
  (unless (fake-window-p window)
    #+X
;;    (progn
;;      (xlib:with-gcontext ((window-gcontext window))
	(setf (xlib:gcontext-font (window-gcontext window)) (window-font window)));)
    #+(and symbolics (not x))
    (scl:send (window-real-window window) :SET-DEFAULT-CHARACTER-STYLE new-style))


(defmethod set-window-line-spacing ((window window) new-line-spacing)
  (setf (window-line-spacing window) new-line-spacing))



(defmacro beep ()
  #+(and symbolics (not x)) '(tv:beep)
  #+x
  `(clx-beep))

(defun clx-beep ()
  ;;(xlib:change-keyboard-control *X-Display* :BELL-PERCENT 100 ;:BELL-DURATION 1
  ;; :BELL-PITCH 1)
  (xlib:bell *X-Display* 100))




(defun set-mouse-documentation-string (string &OPTIONAL (force-p NIL))
  #.(fast)
  (when (or (not (equal *Mouse-Documentation-String* string)) force-p)
    (when *Mouse-Documentation-Window*
      (let ((*Record-Presentations-P* NIL)
	    (stream *Mouse-Documentation-Window*))
	#+ignore (clear-window-internal *Mouse-Documentation-Window*)
	;; open code clearing for speed
	(xlib:clear-area (window-real-window *Mouse-Documentation-Window*))
	(unless (equal string "")
	  #+CLX
	  (xlib:with-gcontext ((window-gcontext stream)
			       :FUNCTION (draw-alu *Draw-Alu*))
	    (xlib:draw-glyphs (window-real-window stream)
			      (window-gcontext stream)
			      0 (font-ascent (window-font stream)) string)))))
    (setq *Mouse-Documentation-String* string)))



#+(or symbolics x)
(defun convert-window-coords-to-screen-coords (window x y)
  #.(fast)
  (if (window-p window)
      (values (%+ (%- x (window-scroll-x-offset window))
		  (window-inside-left window))
	      (%+ (%- y (window-scroll-y-offset window))
		  (window-inside-top window)))
      (values x y)))

#+(or symbolics x)
(defun convert-window-x-coord-to-screen-coord (window x)
  #.(fast)
  (if (window-p window)
      (%+ (%- x (window-scroll-x-offset window)) (window-inside-left window))
      x))

#+(or symbolics x)
(defun convert-window-y-coord-to-screen-coord (window y)
  #.(fast)
  (if (window-p window)
      (%+ (%- y (window-scroll-y-offset window)) (window-inside-top window))
      y))

#+(or symbolics x)
(defmacro convert-window-y-coord-to-screen-coord-macro (window y)
  `(and (window-p ,window)
	(setq ,y (%+ (%- ,y (window-scroll-y-offset ,window)) (window-inside-top ,window)))))

#+(or symbolics x)
(defmacro convert-window-coords-to-screen-coords-macro (window x y
							&OPTIONAL (window-for-sure-p NIL))
  (if window-for-sure-p
      `(progn
	 (setf ,x (%- ,x (%- (window-scroll-x-offset ,window) (window-inside-left ,window))))
	 (setf ,y (%- ,y (%- (window-scroll-y-offset ,window) (window-inside-top ,window)))))
      `(when (window-p ,window)
	 (setf ,x (%- ,x (%- (window-scroll-x-offset ,window) (window-inside-left ,window))))
	 (setf ,y (%- ,y (%- (window-scroll-y-offset ,window) (window-inside-top ,window)))))))



(defun convert-screen-coords-to-window-coords (window x y)
  #.(fast)
  (declare (fixnum x y))
  (if (window-p window)
      (values (%- (%+ x (window-scroll-x-offset window)) (window-inside-left window))
	      (%- (%+ y (window-scroll-y-offset window)) (window-inside-top window)))
      (values x y)))

;#+EW-CLOS
;(defun convert-screen-coords-to-window-coords (window x y)
;  #.(fast)
;  (values x y))

;;; read-Cursorpos returns x,y in window coordinates.
(defun read-cursorpos (stream &OPTIONAL (units :PIXEL))
  #.(fast)
  (if (eq units :PIXEL)
      (values (%- (window-x-pos stream) (window-left-margin-size stream))
	      (%- (window-y-pos stream) (window-top-margin-size stream)))
      ;; convert to character widths and line-heights.
      (values (floor (%- (window-x-pos stream) (window-left-margin-size stream))
		     (default-char-width stream))
	      (floor (%- (window-y-pos stream) (window-top-margin-size stream))
		     (window-line-height stream)))))

(defmethod erase-window-cursor ((window window))
  #.(fast)
  (let ((cursor (window-cursor window)))
    (when (and cursor (eq :ON (cursor-state cursor)))
      (draw-cursor cursor window)
      (setf (cursor-state cursor) :OFF))))


(defmethod draw-window-cursor ((window window))
  #.(fast)
  (let ((cursor (window-cursor window)))
    (when (and cursor (eq :OFF (cursor-state cursor)))
      (draw-cursor cursor window)
      (setf (cursor-state cursor) :ON))))

#+IGNORE ;; SEE HACKED VERSION BELOW, BUT KEEP THIS FOR DOCUMENTATION PURPOSES.
(defun draw-cursor (cursor window)
  (let ((left (- (window-x-pos window) (window-left-margin-size window)))
	(top (- (window-y-pos window) (window-top-margin-size window))))
    (convert-window-coords-to-screen-coords-macro window left top T)
    (xlib:with-gcontext ((window-gcontext window) :FUNCTION (draw-alu :FLIP))
      (xlib:draw-rectangle (window-real-window window)
			   (window-gcontext window)
			   left top (cursor-width cursor) (cursor-height cursor)
			   T))))

(defun draw-cursor (cursor window)
  #.(fast)
  (let ((left (%- (window-x-pos window) (window-scroll-x-offset window)))
	(top (%- (window-y-pos window) (window-scroll-y-offset window))))
    (declare (fixnum left top))
    (when (and (<= 0 left (window-inside-width window))
	       (<= 0 top (window-inside-height window)))
;;      (xlib:with-gcontext ((window-gcontext window) :FUNCTION (draw-alu :FLIP))
	(xlib:draw-rectangle (window-real-window window)
;;			     (window-gcontext window)
			     (cursor-gcontext cursor)
			     left top
			     (cursor-width cursor) (cursor-height cursor)
			     T))));;)

(defun resize-cursor (cursor window width height)
  (when cursor
    (erase-window-cursor window)
    (setf (cursor-width cursor) width
	  (cursor-height cursor) height)
    (draw-window-cursor window)))

(defmethod set-cursorpos ((stream window) x y &OPTIONAL (units :pixel))
  #.(fast)
  (when (eq units :CHARACTER)
    (multiple-value-bind (width height)
	(char-size-from-font #\Space (window-font stream))
      (declare (fixnum width height))
      (when x (setq x (%* x width)))
      (when y (setq y (%* y height)))))
  (let ((cursor-active-p (and (window-cursor stream)
			      (eq :ON (cursor-state (window-cursor stream))))))
    (when cursor-active-p
      (erase-window-cursor stream))
    (when x
      (setf (window-x-pos stream) (%+ x (window-left-margin-size stream))))
    (when y
      (setf (window-y-pos stream) (%+ y (window-top-margin-size stream))))
    #+ignore
    (unless (fake-window-p stream)
      (when x (convert-window-x-coord-to-screen-coord stream x))
      (when y (convert-window-y-coord-to-screen-coord stream y))
      (multiple-value-bind (width height)
	  (window-size stream)
	#+X (declare (ignore width height))
	(when #+(and symbolics (not x))
	      (and (<= 0 x width)
		   (<= 0 y (- height (window-line-height stream))))
	      #+(and symbolics (not x))
	      (scl:send (window-real-window stream) :SET-CURSORPOS x y)
	      #+X NIL)))
    (when (or cursor-active-p (and (window-cursor stream)
				   (eq :OFF (cursor-state (window-cursor stream)))))
      (draw-window-cursor stream))))

(defun set-cursorpos-and-size-from-char (x y char window)
  (declare (fixnum x y))
  (let ((cursor (window-cursor window)))
    (when cursor
      (unless (graphic-char-p char) (setq char #\Space))
      (multiple-value-bind (width height)
	   (char-size char (window-character-style window) window)
	 (declare (fixnum width height))
	 (when (not (and (= width (cursor-width cursor))
			 (= height (cursor-height cursor))))
	   (erase-window-cursor window)
	   (setf (cursor-width cursor) width
		 (cursor-height cursor) height))
	 (set-cursorpos window x y)))))

;;	  (resize-cursor (window-cursor window) window width height)

#+(and symbolics (not x))
(defmethod text-position ((window window) string index)
  (progn
    (setq window (window-real-window window))
    (scl:send window :COMPUTE-MOTION string 0 index 0 0)))

(defmethod turn-off-blinkers ((window window))
  (when (window-p window) (setq window (window-real-window window)))
  #+(and symbolics (not x))
  (mapc #'(lambda (blinker)
	      (scl:send blinker :set-visibility :OFF)
	      (scl:send blinker :set-deselected-visibility :OFF))
	  (scl:send window :blinker-list)))

(defmethod set-blinker-status ((window window) status)
  #-(and symbolics (not x))			; Added 4/30/90 (SLN)
  (declare (ignore status))
  (when (window-p window) (setq window (window-real-window window)))
  #+(and symbolics (not x))
  (scl:send window :SET-CURSOR-VISIBILITY status))



(defun prompt-and-read (type &optional format-string &rest format-args)
  (let ((prompt (or (and format-string (apply #'format NIL format-string format-args))
		    :enter-type)))
    (when (consp type) (setq type (first type)))
    (query (ecase type
	     (:SYMBOL 'symbol)
	     (:STRING 'string)
	     (:INTEGER 'integer)
	     (:EXPRESSION 'form)
	     (:PATHNAME 'pathname))
	   :PROMPT prompt)))


(defmacro merge-character-styles (style default-style)
  #+(and symbolics (not x))
  `(si:merge-character-styles ,style ,default-style)
  #+X
  `(xfonts:merge-character-styles ,style ,default-style))

(defmacro with-character-style ((style &OPTIONAL (stream T) &KEY bind-line-height) &BODY body)
  (if (eq stream T) (setq stream '*Standard-Output*))
  #+(and symbolics (not x))
  (let ((window (gensym "WINDOW"))
	(real-window (gensym "REAL-WINDOW"))
	(new-style (gensym "NEW-STYLE"))
	(old-style (gensym "OLD-STYLE"))
	(old-font (gensym "OLD-FONT"))
	(old-line-height (gensym "OLD-LINE-HEIGHT"))
	(fake-window-p (gensym "FAKE-WINDOW-P")))
    `(let ((,window ,stream))
       (let ((,old-style (window-character-style ,window))
	     (,old-font (window-font ,window))
	     (,old-line-height (window-line-height ,window))
	     (,fake-window-p (fake-window-p ,window))
	     (,real-window (window-real-window ,window))
	     (,new-style ,style))
	 (unwind-protect
	     (progn
	       (setf ,new-style (merge-character-styles ,new-style ,old-style))
	       (setf (window-character-style ,window) ,new-style
		     (window-font ,window)
		     (get-font-from-style ,new-style))
	       (when ,bind-line-height
		 (recompute-line-height ,window (window-font ,window)))
	       (unless ,fake-window-p
		 #+symbolics
		 (scl:send ,real-window :SET-DEFAULT-CHARACTER-STYLE ,new-style))
	       . ,body)
	   (setf (window-character-style ,window) ,old-style
		 (window-font ,window) ,old-font
		 (window-line-height ,window) ,old-line-height)
	   (unless ,fake-window-p
		 #+symbolics
		 (scl:send ,real-window :SET-DEFAULT-CHARACTER-STYLE ,old-style))))))
  #+X
  (let ((old-style (gensym "OLD-STYLE"))
	(old-line-height (gensym "OLD-LINE-HEIGHT")))
    `(let ((,old-style (window-character-style ,stream))
	   (,old-line-height (window-line-height ,stream)))
       (set-window-style ,stream (merge-character-styles ,style ,old-style))
       (unwind-protect
	   (progn (when ,bind-line-height
		    (recompute-line-height ,stream (window-font ,stream)))
		  . ,body)
	 (set-window-style ,stream ,old-style)
	 (setf (window-line-height ,stream) ,old-line-height)))))





(defmacro with-character-size ((size &OPTIONAL (stream T) &KEY bind-line-height) &BODY body)
  `(with-character-style ('(NIL NIL ,size) ,stream :bind-line-height ,bind-line-height)
     . ,body))

(defmacro with-character-face ((face &OPTIONAL (stream T) &KEY bind-line-height) &BODY body)
  `(with-character-style ('(NIL ,face NIL) ,stream :bind-line-height ,bind-line-height)
     . ,body))

#+ignore
(defmacro with-bold ((&OPTIONAL (stream T)) &BODY body)
  `(with-character-face (:BOLD ,stream) . ,body))

#+ignore
(defmacro with-italics ((&OPTIONAL (stream T)) &BODY body)
  `(with-character-face (:ITALIC ,stream) . ,body))


#+(and symbolics (not x))
(defun char-size (char style window)
  (declare (values width height))
  (with-character-style (style window)
    (multiple-value-bind (index font)
	(tv:sheet-get-char-index-and-font char nil window)
      (let ((char-widths (tv:font-char-width-table font)))
	(values (if char-widths
		    (aref char-widths index)
		    (tv:font-char-width font))
		(tv:font-char-height font))))))

#+X
(defun char-size (char style &OPTIONAL window)
  #.(fast)
  (let ((index (char-code char))
	(font (get-font-from-style
		(if window
		    (merge-character-styles style (window-character-style window))
		    style))))
    (values (if (%= (xlib:max-char-width font) (xlib:min-char-width font))
		(xlib:max-char-width font)
		(xlib:char-width font index))
	    (%+ (font-ascent font) (font-descent font)))))

(defun default-char-width (window)
  #.(fast)
  (char-size-from-font #\Space (window-font window)))

#+(and symbolics (not x))
(defun char-size-from-font (char font)
  (let ((index (if (numberp char) char (char-code char))))
    (let ((fwt (zl:font-char-width-table font)))
      (values (if fwt
		  (aref fwt index)
		  (zl:font-char-width font))
	      (zl:font-char-height font)))))

#+X
(defun char-size-from-font (char font)
  (declare (values width height))
  (values 
   (if (%= (xlib:max-char-width font) (xlib:min-char-width font))
       (xlib:min-char-width font)
       (let ((index (if (numberp char) char (char-code char))))
	 (xlib:char-width font index)))
   (%+ (font-ascent font) (font-descent font))))

#+X
(defun char-width-from-font (char font)
  (declare (values width height))
  (if (%= (xlib:max-char-width font) (xlib:min-char-width font))
      (xlib:min-char-width font)
      (let ((index (if (numberp char) char (char-code char))))
	(xlib:char-width font index))))

;;; Done to avoid consing during circle arc calculations
;(defconstant pi (coerce #+symbolics cl:pi #-symbolics user::pi 'short-float))
;; 5/1/90 (sln) This is better. (I think)
(defconstant pi (coerce lisp:pi 'short-float))

(defconstant *360-degrees* #+(or x symbolics) 6.2831852)
(defconstant *270-degrees* #+(or x symbolics) (* 1.5 pi))
(defconstant *180-degrees* #+(or x symbolics) pi)
(defconstant *90-degrees*  #+(or x symbolics) (/ pi 2))


(defun sync ()
  #+clx
  (xlib:display-force-output *X-Display*)
  #-clx NIL)

(defun screen-edges (&OPTIONAL (screen #+(and symbolics (not x)) tv:default-screen
				       #+X *Screen*))
  #+(and symbolics (not x))
  (scl:send screen :INSIDE-EDGES)
  #+X
  (values 0 0 (xlib:screen-width screen) (xlib:screen-height screen)))

(defmethod clear-rest-of-line ((stream window))
  (multiple-value-bind (x-pos y-pos)
      (read-cursorpos stream)
    (declare (fixnum x-pos y-pos))
    (multiple-value-bind (width height)
	(window-size stream)
      (declare (fixnum width height))
      (unless (fake-window-p stream)
	(convert-window-coords-to-screen-coords-macro stream x-pos y-pos)
	(when (and (<= 0 x-pos width)
		   (<= 0 y-pos (%- height (window-line-height stream))))
	  #+(and symbolics (not x))
	  (progn
	    (scl:send (window-real-window stream) :clear-rest-of-line))
	  #+clx
	  (let ((line-height (window-line-height stream)))
	    (prepare-window (stream)
	      (xlib:clear-area (window-real-window stream)
			       :X x-pos :Y y-pos
			       :WIDTH (%- width x-pos) :HEIGHT line-height))))))))

(defmethod clear-rest-of-window ((stream window))
  (multiple-value-bind (x-pos y-pos)
      (read-cursorpos stream)
    (clear-rest-of-line stream)
    (multiple-value-bind (width height)
	(window-size stream)
      (unless (fake-window-p stream)
	(convert-window-coords-to-screen-coords-macro stream x-pos y-pos)
	(let ((line-height (window-line-height stream)))
	  (prepare-window (stream)
	    #+CLX
	    (xlib:clear-area (window-real-window stream) :X 0 :Y (%+ y-pos line-height)
			     :WIDTH width :HEIGHT height)))))))

(defun incf-cursorpos (dx dy &OPTIONAL (stream *Standard-Output*))
  (multiple-value-bind (x y)
      (read-cursorpos stream)
    (set-cursorpos stream (%+ x dx) (%+ y dy))))

(defmethod increment-cursorpos ((stream window) dx dy &OPTIONAL (type :PIXEL))
  (when (eq type :CHARACTER)
    (multiple-value-bind (width height)
	(char-size-from-font #\Space (window-font stream))
      (setq dx (%* dx width)
	    dy (%* dy height))))
  (incf-cursorpos dx dy stream))


(defmethod tab-cursorpos ((stream window) x &OPTIONAL (dx 16.) (type :PIXEL))
  (declare (fixnum x dx))
  (if (window-p stream)
      (multiple-value-bind (x-pos y-pos)
	  (read-cursorpos stream)
	(declare (fixnum x-pos y-pos))
	(when (eq type :CHARACTER)
	  (let ((width (char-size-from-font #\Space (window-font stream))))
	    (setq x (%* x width)
		  dx (%* dx width))))
	(if (> x x-pos)
	    (set-cursorpos stream x y-pos)
	    (set-cursorpos stream (%+ x-pos dx) y-pos)))
      (simple-princ "  " stream)))


(defun indent-terpri (&OPTIONAL (stream *Standard-Output*))
  (let ((line-height (window-maximum-line-height stream)))
    (declare (fixnum line-height))
    ;; do carriage return.
    (prepare-window (stream)
      (setf (window-x-pos stream) (window-left-margin-size stream)
	    (window-y-pos stream) (%+ (window-y-pos stream) line-height)))
    ;; put real cursorposition out, and also check for exceptions
    (unless (fake-window-p stream)
      (let ((x (window-x-pos stream))
	    (y (window-y-pos stream)))
	(declare (fixnum x y))
	(convert-window-coords-to-screen-coords-macro stream x y T)
	(when (and (<= 0 x (window-width stream))
		   (<= 0 y (%- (window-inside-height stream)
			       (window-line-height stream))))
	  #+(and symbolics (not x))
	  (scl:send (window-real-window stream) :SET-CURSORPOS x y)
	  #+X NIL
	  (unless *Memo*
	    (clear-rest-of-line stream)))
	;; now check for exceptions
	(when (> (%+ y line-height) (%- (window-height stream)
					(window-bottom-margin-size stream)))
	  (setf (window-end-of-page-flag stream) 1))
	(when (> (%+ y line-height line-height) (%- (window-height stream)
						    (window-bottom-margin-size stream)))
	  (setf (window-more-flag stream) 1))
	(check-end-of-page-mode stream)))
    ;; do indentation.
    (when (window-indentation stream)
      (handle-indentation stream))
    (setf (window-maximum-line-height stream) (window-line-height stream))))

(defun handle-indentation (stream)
  (dolist (indentation-amount (window-indentation stream))
    (cond ((integerp Indentation-amount)
	   (if (%< Indentation-amount 11)
	       (increment-cursorpos stream
				    (%* Indentation-amount
					(default-char-width stream)) 0)
	       (increment-cursorpos stream Indentation-amount 0)))
	  ((and (consp Indentation-amount)
		(integerp (first Indentation-amount)))
	   (let ((amount (first Indentation-amount)))
	     (declare (fixnum amount))
	     (when (eq :character (second Indentation-amount))
	       (setq amount (%* amount (default-char-width stream))))
	     (increment-cursorpos stream amount 0)))
	  ((stringp Indentation-amount)
	   (simple-princ Indentation-amount stream))
	  (Indentation-amount
	   (let ((string
		   (with-output-to-presentation-recording-string (stream)
		     (funcall Indentation-amount stream))))
	     (princ string stream))))))



(defun set-cursorpos-with-checks (x y stream)
  (declare (fixnum x y))
  (set-cursorpos stream x y)
  (unless (fake-window-p stream)
    (convert-window-y-coord-to-screen-coord-macro stream y)
    (when (> y (%- (window-inside-height stream)
		   (window-line-height stream)))
      (setf (window-end-of-page-flag stream) 1))))

#+ignore
(defmacro remember-indentation ((&OPTIONAL (stream '*Standard-Output*)) &BODY body)
  `(let ((*Indentation-Amount* (the fixnum (read-cursorpos ,stream))))
     . ,body))


#+ignore
(defun indent-by (amount &OPTIONAL (stream *Standard-Output*))
  (declare (fixnum amount))
  (multiple-value-bind (x y)
      (read-cursorpos stream)
    (declare (fixnum x y))
    (when (> (the fixnum (%+ amount (the fixnum *Indentation-Amount*))) x)
      (set-cursorpos stream (%+ amount *Indentation-Amount*) y))))





#+ignore
(:ACTIVATE
                                   :ADJUST-BLINKER
                                   :ADJUST-CURSOR-POSITION-AND-VISIBILITY
                                   :ALIAS-FOR-SELECTED-WINDOWS
				   :ARREST
                                   :AUTO-LINE-HEIGHT
                                   :BIT-ARRAY
                                   :BITBLT
                                   :BITBLT-FROM-SHEET
                                   :BITBLT-FROM-SHEET-TO-SHEET
                                   :BITBLT-TO-SHEET
                                   :BITBLT-WITHIN-SHEET
                                   :BLINKER-LIST
                                   :BORDERS
                                   :BUFFER-X
                                   :BUFFER-Y
                                   :CHANGE-OF-DEFAULT-FONT
                                   :CHANGE-OF-SCREEN
                                   :CHANGE-OF-SIZE-OR-MARGINS
                                   :CHAR-ALUF
                                   :CHARACTERS
                                   :CLEAR-CHAR
                                   :CLEAR-EOF
                                   :CLEAR-EOL
                                   :CLEAR-INPUT
                                   :CLEAR-MOUSE-MOTION
                                   :CLEAR-OUTPUT
                                   :CLEAR-REGION
                                   :CLEAR-REGION-STRINGS
                                   :CLEAR-SCREEN
                                   :CLIPPING-REGION
                                   :CURRENT-MORE-POSITION
                                   GRAPHICS:CURRENT-POSITION
                                   :CURSOR-VIEWPORT
                                   :CURSOR-VISIBILITY
                                   :DEACTIVATE
                                   :DEEXPOSED-TYPEIN-ACTION
                                   :DEEXPOSED-TYPEOUT-ACTION
                                   :DEFAULT-CHARACTER-STYLE
                                   :DEFAULTED-END-OF-PAGE-MODE
                                   :DELAYED-SET-LABEL
                                   :DELETE-CHAR
                                   :DELETE-DISPLAYED-PRESENTATION
                                   :DELETE-LINE
                                   :DELETE-STRING
                                   :DESCRIBE
                                   :DESELECT
                                   :DISPLAY-CENTERED-STRING
                                   :DISPLAY-DEVICE-TYPE
                                   :DISPLAY-HELP
                                   :DISPLAY-LINE-INTERNAL
                                   :DISPLAY-LOZENGED-STRING
                                   :DISPLAY-PROMPT
                                   :DISPLAY-X-Y-CENTERED-STRING
                                   :DISPLAYED-GRAPHICS
                                   :DISPLAYED-PRESENTATION-AT-POSITION
                                   :DISPLAYED-PRESENTATIONS
                                   :DISPLAYED-STRINGS
                                   :DRAW-1-BIT-RASTER
                                   :DRAW-ARROW
                                   :DRAW-ARROWS
                                   GRAPHICS:DRAW-BEZIER-CURVE
                                   GRAPHICS:DRAW-BEZIER-CURVE-TO
                                   :DRAW-CHAR
                                   :DRAW-CIRCLE
                                   :DRAW-CIRCLE-INTERNAL
                                   :DRAW-CIRCULAR-ARC
                                   :DRAW-CIRCULAR-ARC-CACHED-INTERNAL
                                   GRAPHICS:DRAW-CIRCULAR-ARC-THROUGH-POINT-TO
                                   GRAPHICS:DRAW-CIRCULAR-ARC-TO
                                   :DRAW-CLOSED-CURVE
                                   GRAPHICS:DRAW-CONIC-SECTION
                                   GRAPHICS:DRAW-CONIC-SECTION-TO
                                   :DRAW-CONVEX-POLYGON
                                   GRAPHICS:DRAW-CUBIC-SPLINE
                                   :DRAW-CUBIC-SPLINE
                                   :DRAW-CURVE
                                   :DRAW-DASHED-ARROW
                                   :DRAW-DASHED-ARROWS
                                   :DRAW-DASHED-LINE
                                   GRAPHICS:DRAW-ELLIPSE
                                   :DRAW-FAT-CIRCLE
                                   :DRAW-FAT-LINE
                                   :DRAW-FAT-LINES
                                   :DRAW-FILLED-IN-CIRCLE
                                   :DRAW-FILLED-IN-CIRCLE-CACHED-INTERNAL
                                   :DRAW-FILLED-IN-SECTOR
                                   :DRAW-FILLED-IN-SIMPLE-ELLIPSE
                                   :DRAW-FILLED-IN-SIMPLE-ELLIPSE-CACHED-INTERNAL
                                   GRAPHICS::DRAW-GENERAL-ELLIPSE
                                   :DRAW-GLYPH
                                   GRAPHICS:DRAW-IMAGE
                                   :DRAW-LINE
                                   GRAPHICS:DRAW-LINE-TO
                                   :DRAW-LINES
                                   GRAPHICS::DRAW-LINES-SCALED
                                   GRAPHICS:DRAW-PATH
                                   :DRAW-PATTERNED-RECTANGLE
                                   :DRAW-PATTERNED-TRIANGLE
                                   :DRAW-POINT
                                   GRAPHICS:DRAW-POINT
                                   GRAPHICS:DRAW-POLYGON
                                   :DRAW-POLYGON
                                   :DRAW-RASTER
                                   :DRAW-RECTANGLE
                                   GRAPHICS:DRAW-RECTANGLE
                                   :DRAW-REGULAR-POLYGON
                                   GRAPHICS:DRAW-REGULAR-POLYGON
                                   :DRAW-RING
                                   GRAPHICS::DRAW-SCALED-CIRCLE
                                   GRAPHICS::DRAW-SCALED-CIRCULAR-ARC
                                   GRAPHICS::DRAW-SCALED-FILLED-IN-CIRCLE
                                   GRAPHICS::DRAW-SCALED-FILLED-IN-SECTOR
                                   GRAPHICS::DRAW-SCALED-FILLED-IN-SIMPLE-ELLIPSE
                                   GRAPHICS::DRAW-SCALED-LINE
                                   GRAPHICS::DRAW-SCALED-RING
                                   GRAPHICS::DRAW-SCALED-SECTOR-RING
                                   GRAPHICS::DRAW-SCALED-SIMPLE-ELLIPSE
                                   :DRAW-SIMPLE-ELLIPSE
                                   GRAPHICS::DRAW-SIMPLY-SCALED-IMAGE
                                   GRAPHICS:DRAW-STRING
                                   :DRAW-STRING
                                   GRAPHICS:DRAW-STRING-IMAGE
                                   :DRAW-STRING-SIZE
                                   GRAPHICS::DRAW-THICK-LINES-SCALED
                                   :DRAW-TILED-RECTANGLE
                                   :DRAW-TILED-TRIANGLE
                                   :DRAW-TRIANGLE
                                   GRAPHICS:DRAW-TRIANGLE
                                   :DRAW-TRIANGULAR-OUTLINE
                                   :DRAW-VIEWPORT-SEPARATOR-LINE
                                   :DRAW-WIDE-CURVE
                                   :END-OF-LINE-EXCEPTION
                                   :END-OF-PAGE-EXCEPTION
                                   :END-OF-PAGE-MODE
                                   :EOF
                                   :ERASE-ALUF
                                   :EXPOSED-INFERIORS
                                   :EXPOSED-P
                                   :FINISH
                                   :FINISH-TYPEOUT
                                   :FLAGS
                                   :FOLLOWING-BLINKER
                                   :FORCE-KBD-INPUT
                                   :FORCE-OUTPUT
                                   :FORCE-RESCAN
                                   :FORWARD-CHAR
                                   :FULL-SCREEN
                                   :HANDLE-EXCEPTIONS
                                   :HANDLE-MOUSE
                                   :HIGHLIGHTING-BLINKER
                                   :INFERIOR-ACTIVATE
                                   :INFERIOR-BURY
                                   :INFERIOR-DEACTIVATE
                                   :INFERIOR-DEEXPOSE
                                   :INFERIOR-EXPOSE
                                   :INFERIOR-LOCK-COUNT
                                   :INFERIOR-SELECT
                                   :INFERIOR-SET-EDGES
                                   :INIT
                                   :INPUT-WAIT
                                   :INSERT-CHAR
                                   :INSERT-INPUT-BLIP
                                   :INSERT-LINE
                                   :INSERT-STRING
                                   :INTERACTIVE
                                   :INTERACTOR-P
                                   :INVISIBLE-TO-MOUSE-P
                                   :IO-BUFFER
                                   :ITEM
                                   :KEEP-MOUSE-VISIBLE
				   :LABEL
                                   :LISP-LISTENER-P
                                   :LIST-TYI
                                   :LISTEN
                                   :LOCATIONS-PER-LINE
                                   :MATCHING-CHARACTER-BLINKER
                                   :MAXIMUM-EXPOSABLE-INSIDE-SIZE
                                   :MORE-EXCEPTION
                                   :MORE-TYI
                                   :MORE-VPOS
                                   :MOUSE-BLINKER-CHARACTER
                                   :MOUSE-BLINKER-TYPE
                                   :MOUSE-BUTTONS
                                   :MOUSE-CLICK
                                   :MOUSE-MOTION-PENDING
                                   :MOUSE-MOVES
                                   :MOUSE-POSITION
                                   :MOUSE-SELECT
                                   :MOUSE-STANDARD-BLINKER
                                   :MOUSE-X-POSITION
                                   :MOUSE-Y-POSITION
                                   :MOVE-REGION
                                   :NAME-FOR-SELECTION
                                   :NEW-MOUSE-POSITION
                                   :NEW-SCROLL-POSITION
                                   :NEWLINE-WITHOUT-ERASING
                                   :NOISE-STRING-OUT
                                   :PANE-SIZE
                                   :PANE-TYPES-ALIST
                                   :POINT
                                   :PRESTART-PROCESS
                                   :PRINT
                                   :PRIORITY
                                   :PROCESS
                                   :RAGGED-BORDERS-P
                                   :READ-UNTIL-EOF
                                   :REAL-SET-CURSORPOS
                                   :REDISPLAY-BETWEEN-CURSORPOSES
                                   :REFRESH-MARGINS
                                   :REFRESH-MORE-PROMPT
                                   :RESET-PROCESS
                                   :RUBOUT-HANDLER
                                   :SAVE-INPUT-BUFFER
                                   :SAVE-RUBOUT-HANDLER-BUFFER
                                   :SCREEN
                                   :SCREEN-ARRAY
                                   :SCROLL-BAR-P
                                   :SCROLL-FACTOR
                                   :SCROLL-FOR-END-OF-PAGE
                                   :SCROLL-ONE-SCREENFUL
                                   :SCROLL-POSITION-FUNCTION
                                   :SCROLL-RELATIVE
                                   :SCROLL-TO-VISIBLE-CURSOR
                                   :SELECT-RELATIVE
                                   :SELECTABLE-WINDOWS
                                   :SEND-IF-HANDLES
                                   :SET-ACTIVATOR
                                   :SET-AUTO-LINE-HEIGHT
                                   :SET-CHAR-ALUF
                                   :SET-CURRENT-FONT
                                   :SET-CURSOR-VISIBILITY
                                   :SET-DEEXPOSED-TYPEIN-ACTION
                                   :SET-DEEXPOSED-TYPEOUT-ACTION
                                   :SET-DEFAULT-CHARACTER-STYLE
                                   :SET-DEFAULT-STYLE
                                   :SET-END-OF-PAGE-MODE
                                   :SET-ERASE-ALUF
                                   :SET-FONT-MAP
                                   :SET-FONT-MAP-AND-VSP
                                   :SET-IO-BUFFER
                                   :SET-LINE-HEIGHT-FROM-STYLES
                                   :SET-LOCATION
                                   :SET-MOUSE-BLINKER-CHARACTER
                                   :SET-MOUSE-BLINKER-TYPE
                                   :SET-MOUSE-CURSORPOS
                                   :SET-MOUSE-POSITION
                                   :SET-MOUSE-X-POSITION
                                   :SET-MOUSE-Y-POSITION
                                   :SET-PRIORITY
                                   :SET-PROCESS
                                   :SET-REVERSE-VIDEO-P
                                   :SET-SCROLL-FACTOR
                                   :SET-SCROLL-POSITION-FUNCTION
                                   :SET-STATUS
                                   :SET-TRUNCATE-LINE-OUT
                                   TV:SHEET-GET-CHAR-FONT-INTERNAL
                                   :SIZE-IN-CHARACTERS
                                   :STATUS
                                   :STRING-OUT-EXPLICIT
                                   :STRING-OUT-EXPLICIT-INTERNAL
                                   :STRING-OUT-STATE
                                   :TRUNCATE-LINE-OUT
                                   :UN-ARREST
                                   :UNDERLINE-BETWEEN-CURSORPOSES
                                   :UPDATE-LABEL
                                   :VERIFY-NEW-EDGES
                                   :WHO-LINE-DOCUMENTATION-STRING
                                   :WINDOW-AND-OFFSETS
                                   :WITH-AUTO-LINE-HEIGHT
                                   :WITH-CHARACTER-STYLE)


(defvar *Possible-Messages* '((:any-tyi read-any)
			      (:ANY-TYI-NO-HANG read-any-char-no-hang)
			      (:baseline window-font-ascent)
			      :BEEP
			      :BITBLT
			      (:BOTTOM-MARGIN-SIZE window-bottom-margin-size)
			      (:BURY deexpose-window)
			      (:CENTER-AROUND center-window-around)
			      :CHARACTER-HEIGHT
			      :CHARACTER-WIDTH
			      :CHARACTER-WIDTH-IN-FONT
			      (:char-width default-char-width)
			      :CLEAR-BETWEEN-CURSORPOSES
			      (:clear-history clear-history)
			      (:clear-rest-of-line clear-rest-of-line)
			      (:clear-rest-of-window clear-rest-of-window)
			      (:clear-window clear-window)
			      (:COLOR-STREAM-P window-color-p)
			      (:COMPUTE-MOTION window-compute-motion)
			      (:configuration configuration)
			      :CONVERT-TO-ABSOLUTE-COORDINATES
			      :CONVERT-TO-INSIDE-COORDINATES
			      :CONVERT-TO-RELATIVE-COORDINATES
			      :CURRENT-BASELINE
			      (:current-font window-font)
			      (:CURRENT-LINE-HEIGHT window-line-height)
			      (:CURRENT-STYLE window-character-style)
			      :CURSOR-X
			      :CURSOR-Y
			      (:deexpose deexpose-window)
			      :DEFAULT-STYLE
			      (:edges window-edges)
			      :ERASE-DISPLAYED-PRESENTATION
			      (:expose expose-window)
			      (:EXPOSE-NEAR expose-window-near)
			      (:FRESH-LINE fresh-line)
			      (:get-pane get-pane)
			      (:height window-height)
			      (:HOME-CURSOR home-cursor)
			      (:HOME-DOWN home-down)
			      (:increment-cursorpos increment-cursorpos)
			      (:INFERIORS window-inferiors)
			      (:inside-edges window-inside-edges)
			      (:INSIDE-HEIGHT window-inside-height)
			      (:inside-size window-inside-size)
			      (:INSIDE-WIDTH window-inside-width)
			      (:KILL window-kill)
			      (:LEFT-MARGIN-SIZE window-left-margin-size)
			      (:LINE-HEIGHT window-line-height)
			      :LINE-IN
			      :LINE-OUT
			      (:MARGIN-COMPONENTS presentation-window-margins)
			      :MARGINS
			      (:MAXIMUM-X-POSITION presentation-window-max-x-position)
			      (:MAXIMUM-Y-POSITION presentation-window-max-y-position)
			      :MERGED-CURRENT-STYLE
			      :MINIMUM-X-POSITION
			      :MINIMUM-Y-POSITION
			      (:more-p window-more-p)
			      (:name window-name)
			      (:OPERATION-HANDLED-P window-operation-handled-p)
			      (:POSITION window-position)
			      :PRINT-SELF
			      (:read-cursorpos read-cursorpos)
			      (:read-location read-location)
			      :REFRESH
			      :REVERSE-VIDEO-P
			      (:RIGHT-MARGIN-SIZE window-right-margin-size)
			      :SAVE-BITS
			      :SELECT
			      :SET-BORDERS
			      (:set-configuration set-configuration)
			      (:set-cursorpos set-cursorpos)
			      (:SET-EDGES set-window-edges)
			      (:SET-INSIDE-SIZE set-window-inside-size)
			      (:set-label set-window-label)
			      (:SET-MAXIMUM-X-POSITION (setf presentation-window-max-x-position))
			      (:SET-MAXIMUM-Y-POSITION (setf presentation-window-max-y-position))
			      :SET-MINIMUM-X-POSITION
			      :SET-MINIMUM-Y-POSITION
			      (:SET-MORE-P (setf window-more-p))
			      (:set-position set-window-position)
			      :SET-SAVE-BITS
			      (:SET-SIZE set-window-size)
			      :SET-SIZE-IN-CHARACTERS
			      (:SET-SUPERIOR (setf window-superior))
			      (:set-viewport-position set-viewport-position)
			      (:set-vsp set-window-line-spacing)
			      (:size window-size)
			      :SIZE-IN-CHARACTERS
			      :STRING-IN
			      (:STRING-LENGTH window-string-length)
			      :STRING-LINE-IN
			      (:STRING-OUT window-string-out)
			      (:superior window-superior)
			      (:TOP-MARGIN-SIZE window-top-margin-size)
			      (:TYI read-char)
			      (:TYI-NO-HANG read-char-no-hang)
			      (:TYIPEEK peek-char)
			      (:TYO (write-char arg window arg))
			      (:UNTYI unread-char)
			      (:VIEWPORT-POSITION viewport-position)
			      (:visible-cursorpos-limits visible-cursorpos-limits)
			      (:vsp window-line-spacing)
			      (:width window-width)
			      (:WHICH-OPERATIONS which-operations)
			      :X-OFFSET
			      :Y-OFFSET
			      (:x-scroll-position x-scroll-position)
			      (:x-scroll-to x-scroll-to)
			      (:y-scroll-position y-scroll-position)
			      (:y-scroll-to y-scroll-to)))

(defvar *Messages-Handled*
	(copy-list (mapcan #'(lambda (item) (and (consp item) (list (first item))))
			   *Possible-Messages*)))

(dolist (pair *Possible-Messages*)
  (when (consp pair)
    (setf (get (first pair) 'real-function) (second pair))))

(defmethod which-operations ((window window))
;  (declare (ignore window))
  *Messages-Handled*)

(defmethod window-operation-handled-p ((window window) operation)
;  (declare (ignore window))
  (not (null (member operation *Messages-Handled*))))

(defmacro send (object message &rest args)
  (setq message (eval message))
  (let ((function (get message 'real-function)))
    (cond ((and (consp function)
		(eq 'setf (first function)))
	   `(setf (,(second function) ,object) . ,args))
	  ((consp function)
	   (let ((window-position (position 'WINDOW function))
		 (first-args NIL))
	     (dotimes (x (%1- window-position))
	       #+symbolics (declare (ignore x))		;6/27/90 (sln)
	       (push (pop args) first-args))
	     `(,(first function) ,@(nreverse first-args) ,object . ,args)))
	  (function
	   `(,function ,object . ,args))
	  (T
	   (format T "No Translation for message ~A." message)
	   `(format T "No Translation for message ~A." ,message)))))

#+symbolics
(defun handle-invalid-function (condition)
  (when (lisp:typep (scl:send condition :FUNCTION) 'WINDOW)
    (sys:proceed condition
		 :NEW-FUNCTION
		 #'(lambda (&rest args)
		     (let* ((message (first args))
			    (function (get message 'real-function)))
		       (cond ((and (consp function)
				   (eq 'setf (first function)))
			      (eval `(setf (,(second function)
					    ,(scl:send condition :FUNCTION))
					   . ,(cdr args))))
			     ((consp function)
			      (let ((window-position (position 'WINDOW function))
				    (first-args NIL))
				(pop args)
				(dotimes (x (%1- window-position))
				  (declare (ignore x))	;6/27/90 (sln)
				  (push (pop args) first-args))
				(eval `(,(first function)
					,@first-args
					,(scl:send condition :FUNCTION)
					. ,args))))
			     (function
			      (apply (get message 'real-function)
				     (scl:send condition :FUNCTION)
				     (cdr args)))
			     (T (apply 'print-error (scl:send condition :FUNCTION)
				       args))))))))

(defmacro with-safe-windows (&BODY body)
  #+lispm
  `(scl:condition-bind ((si:invalid-function 'handle-invalid-function))
     . ,body)
  
  #-lispm
  `(progn . ,body))


#+symbolics
(defun print-error (&Rest args)
  (print args tv:initial-lisp-listener))


;; Returns two values describing the final position of the cursor so that the cursor is after
;; the character in the string at string-index.  Handles #\Newlines by moving down one line
;; and over to the left side of the window.  Also handles reaching a maximum x-position

#+(and symbolics (not x))
(defun compute-cursor-position-in-string (font string string-index x-position y-position
					  line-height max-x
					  &OPTIONAL (start-x 0) (end-x (length string)))
  (declare (values final-x-position final-y-position) (string string))
  (let ((fwt (zl:font-char-width-table font)))
    (if fwt
	(do ((x start-x (1+ x)))
	    ((or (= x string-index) (= end-x x)) (values x-position y-position))
	  (declare (fixnum x))
	  (let ((char (aref string x)))
	    (if (char-equal char #\Newline)
		(setq x-position 0
		      y-position (+ y-position line-height))
		(let ((char-width (aref fwt (char-code char))))
		  (if (> (+ x-position char-width) max-x)
		      ;; we have reached the maximum right side of the display.
		      ;; wrap around to next line.
		      (progn (setq x-position char-width)
			     (incf y-position line-height))
		      (incf x-position char-width))))))
	(do ((x start-x (1+ x))
	     (char-width (zl:font-char-width font)))
	    ((or (= x string-index) (= end-x x)) (values x-position y-position))
	  (declare (fixnum x))
	  (let ((char (aref string x)))
	    (if (char-equal char #\Newline)
		(progn
		  (setq x-position 0)
		  (incf y-position line-height))
		(if (> (+ x-position char-width) max-x)
		    (progn (setq x-position char-width)
			   (incf y-position line-height))
		    (incf x-position char-width))))))))

#+X
(defun compute-cursor-position-in-string (font string string-index x-position y-position
					  line-height max-x
					  &OPTIONAL
					  (start-x 0)
					  (end-x (length (the string string))))
  #.(fast)
  (declare (fixnum start-x end-x line-height max-x x-position y-position string-index)
	   (string string))
  (declare (values final-x-position final-y-position))
  (setq end-x (min end-x string-index)) ;; minimizing checking in end-test.
  (do ((x start-x (the fixnum (1+ (the fixnum x)))))
      ((= end-x x) (values x-position y-position))
    (declare (fixnum x))
    (let ((char (aref string x)))
      (case char
	(#\Newline
	 (setq x-position 0
	       y-position (%+ y-position line-height)))
	(#\TAB
	 (setq x-position (max x-position (get-next-tab-position x-position font)))
	 (if (> x-position max-x)
	     (progn (setq x-position 0)
		    (incf y-position line-height))))
	(T (let ((char-width (char-size-from-font char font)))
	     (if (> (%+ x-position char-width) max-x)
		 ;; we have reached the maximum right side of the display.
		 ;; wrap around to next line.
		 (progn (setq x-position char-width)
			(incf y-position line-height))
		 (incf x-position char-width))))))))




(defmethod check-end-of-page-mode ((stream window))
  #.(fast)
  (unless *Recursive-Check-End-Of-Page-Mode*
    (let ((*Recursive-Check-End-Of-Page-Mode* T))
      (when (= 1 (window-end-of-page-flag stream))
	#+ignore(dformat "MORE")
	;; print out more and stop for now.
	;; now that we have done that, let's scroll the screen.
	(let ((mode (window-end-of-page-mode stream)))
	  (when (eq mode :DEFAULT) (setq mode *Default-End-Of-Page-Mode*))
	  (setf (window-end-of-page-flag stream) 0)
	  (unless (eq mode :TRUNCATE)
	    (multiple-value-bind (x y)
		(read-cursorpos stream)
	      (when (window-more-p stream)
		(let ((*Record-Presentations-P* NIL))
		  (simple-princ "**More**" stream))
		;; (read-any stream)
		#+(and symbolics (not x))
		(tv:process-sleep 30)
		;; erase the more
		(set-cursorpos stream x y)
		(unless *Memo*
		  (clear-rest-of-line stream)))
	      (case mode
		(:SCROLL
		  (scroll-window-down stream
				      :LINES
				      (let ((line-height (window-line-height stream))
					    (height (window-inside-height stream)))
					(declare (fixnum line-height height))
					(max 15
					     (%- (the (values fixnum number)
						      (floor height line-height))
						 5)))))
		(:WRAP
		  ;; wrapping is nothing more than scrolling an entire screen,
		  ;; moving the cursor to the beginning of the screen, and
		  ;; of course probably not erasing the current contents.
		  ;; I'm not sure how to deal with making them correctly mouse sensitive.
		  ;; I guess we could hack it with some magic constant that is part of the window.
		  ;; screw it for now.
		  (scroll-window-down stream :PIXEL
					       (convert-window-y-coord-to-screen-coord stream y))
		  (multiple-value-bind (x y)
		      (visible-cursorpos-limits stream)
		    (set-cursorpos stream x y)))))))))))



(defparameter *Default-Host*
	      #+unix "unix"
	      #-unix (machine-instance))

(defun initialize-window-system (&OPTIONAL (host *Default-Host*) &KEY protocol (display 0))
  (setq *Menu-Choose-Resource* NIL
	*mouse-window* NIL)
  (when *X-Display*
    ;; remove pixmaps from stipples so they can be GC'd.
    (dolist (stipple *stipple-arrays*)
      (setf (stipple-pattern-pixmaps stipple)
	    (delete (xlib:screen-root *screen*)
		    (stipple-pattern-pixmaps stipple)
		    :KEY #'FIRST)))
    (xlib:close-display *X-display*))
  (setq *X-Display* (xlib:open-display host :DISPLAY display :PROTOCOL protocol)
	*Screen*  (xlib:display-default-screen *X-Display*))
  (init-alu-values (xlib:screen-black-pixel *Screen*))
  (xfonts::init-fonts)
  (let ((screen-width (xlib:screen-width *Screen*))
	(screen-height (xlib:screen-height *Screen*))
	(mouse-documentation-window-height 36))
    (setq *Ultimate-Root-Window*
	  (make-presentation-window :REAL-WINDOW
				    (xlib:screen-root *Screen*)
				    :LEFT 0 :TOP 0 :border-margin-width 0
				    :WIDTH screen-width
				    :HEIGHT screen-height))
    (init-mouse-cursors)
    (setq *Mouse-Documentation-Window*
	  (make-window 'WINDOW :MORE-P NIL :EXPOSE-P NIL :BLINKER-P NIL
		       :LABEL NIL
		       :BORDERS 1
		       :LEFT 0 :TOP (%- screen-height mouse-documentation-window-height)
		       :HEIGHT mouse-documentation-window-height
		       :WIDTH screen-width
		       :SUPERIOR *Ultimate-Root-Window*))
    (setq *Root-Window*
	  (make-window 'WINDOW :MORE-P NIL :EXPOSE-P NIL :BLINKER-P NIL
		       :LABEL NIL
		       :LEFT 0 :TOP 0
		       :WIDTH screen-width
		       :HEIGHT (%- screen-height mouse-documentation-window-height)
		       :BORDERS 0
		       :SUPERIOR *Ultimate-Root-Window*))
    (xlib:set-standard-properties
      (window-real-window *Mouse-Documentation-Window*)
      :ICON-NAME (concatenate 'string (or #+symbolics "Symbolics" #+ti "Explorer" #+lucid "Lucid" #+allegro "Allegro" "Lisp") " Mouse Pane")
      :name (concatenate 'string "Mouse Pane for " (lisp-implementation-type) " on " #-excl (machine-instance) #+excl (short-site-name))
      :INPUT :OFF
      :NORMAL-HINTS
      (xlib:make-wm-size-hints :WIDTH screen-width :min-WIDTH screen-width
			       :height mouse-documentation-window-height :min-height mouse-documentation-window-height
			       :X 0 :Y (%- screen-height mouse-documentation-window-height)))
    (xlib:set-standard-properties
      (window-real-window *Root-Window*)
      :ICON-NAME (lisp-implementation-type)
      :name (concatenate 'string (lisp-implementation-type) " on " #-excl (machine-instance) #+excl (short-site-name))
      :INPUT :ON
      :NORMAL-HINTS
      (xlib:make-wm-size-hints :WIDTH screen-width :min-WIDTH screen-width
			       :height (%- screen-height mouse-documentation-window-height)
			       :min-height (%- screen-height mouse-documentation-window-height)
			       :X 0 :Y 0))
    (expose-window *Mouse-Documentation-Window*)
    (expose-window *Root-Window*)))


#+ignore
(:ACTIVATE-P
  :ACTIVATOR
 :ASYNCHRONOUS-CHARACTERS
 :AUTO-LINE-HEIGHT
 :BACKSPACE-NOT-OVERPRINTING-FLAG	:CR-NOT-NEWLINE-FLAG
 :BIT-ARRAY
 :BLINKER-ALU  :BLINKER-DESELECTED-VISIBILITY  :BLINKER-FLAVOR  :BLINKER-P
 :BORDER-MARGIN-WIDTH	:BORDERS
 :CHAR-ALUF 		:ERASE-ALUF
 :CHARACTER-HEIGHT 		:CHARACTER-WIDTH
 :DEEXPOSED-TYPEIN-ACTION 	:DEEXPOSED-TYPEOUT-ACTION
 :DEFAULT-CHARACTER-STYLE 	:DEFAULT-STYLE
 :DISPLAY-DEVICE-TYPE
 :EDGES-FROM
			 :INSIDE-HEIGHT		 :MINIMUM-HEIGHT   :INSIDE-SIZE
 			 :INSIDE-WIDTH		 :MINIMUM-WIDTH
 :INTEGRAL-P
 :IO-BUFFER
 :LEFT-MARGIN-SIZE  :TOP-MARGIN-SIZE  :RIGHT-MARGIN-SIZE  :BOTTOM-MARGIN-SIZE
 :NOTIFICATION-MODE
 :PRIORITY
 :REVERSE-VIDEO-P
 :RIGHT-MARGIN-CHARACTER-FLAG
 :SAVE-BITS
 :SCREEN
 :SIZE
 :TAB-NCHARS
 :TRUNCATE-LINE-OUT		:TRUNCATE-LINE-OUT-FLAG	 :TRUNCATE-OUTPUT
 )

;;; X, Y, Left, Top, Right, Bottom are all in terms of absolute coordinates.

(defvar *Default-Window-Event-Mask* '(:exposure
				      :key-press :key-release
				      :exposure
				      :button-press :button-release
				      :pointer-motion
				      :pointer-motion-hint
				      ))

#+X
(defun make-window (window-type &REST other-keys
		    &KEY (superior *Root-Window*)
		    x y ;; x,y are relative coordinates to outside edges.
		    width height size inside-width inside-height inside-size
		    (blinker-p T)
		    name default-name
		    (borders 1)
		    left top right bottom edges (more-p NIL)
		    (end-of-page-mode :DEFAULT)
		    (end-of-line-mode :DEFAULT)
		    (default-style '(:fix :roman :normal))
		    (style '(:fix :roman :normal))
		    (character-style '(:fix :roman :normal))
		    (default-character-style '(:fix :roman :normal))
		    expose-p
		    activate-p integral-p
		    (save-under NIL) (label T)
		    (margin-components NIL)
		    (gcontext-function (draw-alu :DRAW))
		    (gcontext-tile NIL)
		    (bit-gravity :north-west)
		    (gcontext-fill-style :SOLID)
		    (event-mask *Default-Window-Event-Mask*)
		    (position NIL)
		    (vsp NIL) (line-spacing 2)
		    &ALLOW-OTHER-KEYS
		    &AUX (real-left 0) (real-top 0) (real-right 0) (real-bottom 0)
		    sup-left sup-top sup-right sup-bottom)
  (declare (fixnum real-left real-top real-right real-bottom))
  (declare (ignore inside-size inside-height inside-width size other-keys)) ;; 4/30/90 (SLN)
  (warn-unimplemented-args activate-p NIL)
  (unless (equal style '(:fix :roman :normal)) (setq default-style style))
  (unless (equal default-character-style '(:fix :roman :normal))
    (setq default-style default-character-style))
  (unless (equal character-style '(:fix :roman :normal))
    (setq default-style character-style))
  (unless superior (setq superior *Root-Window*))
  ;; first find window edges.
  ;; inside-width has priority over width.
  ;; inside-height has priority over height.
  ;; size has priority over inside-size and inside-width and inside-height.  
  ;; x,y have priority over left, top.
  ;; left, top, right, bottom priority over edges.
  (case save-under
    ((T) (setq save-under :ON))
    ((NIL) (setq save-under :OFF)))
  (multiple-value-setq (sup-left sup-top sup-right sup-bottom)
    (window-edges superior))
  (multiple-value-setq (real-left real-top real-right real-bottom)
    (window-edges superior))
  (when left (setq real-left left))
  (when top (setq real-top top))
  (when right (setq real-right right))
  (when bottom (setq real-bottom bottom))
  (when (consp position)
    (setq x (first position)
	  x (second position)))
  ;; x and y a relative to the superiors outside edges.
  (when x
    (psetq real-left (%+ x sup-left)
	   real-right (%- real-right real-left)))
  (when y
    (psetq real-top (%+ y sup-top)
	   real-bottom (%- real-bottom real-top)))
  (when (numberp width)
    (setq real-right (%+ real-left width)))
  (when (numberp height)
    (setq real-bottom (%+ real-top height)))
  (when (consp edges)
    (setq real-left (first edges) real-top (second edges)
	  real-right (third edges) real-bottom (fourth edges)))
  (when (< real-left sup-left) (setq real-left sup-left))
  (when (< real-top sup-top) (setq real-top sup-top))
  (when (> real-right sup-right) (setq real-right sup-right))
  (when (> real-bottom sup-bottom) (setq real-bottom sup-bottom))
  (unless (numberp borders) (setq borders 1))
  (let ((black (xlib:screen-black-pixel *Screen*))
	(white (xlib:screen-white-pixel *Screen*)))
    (let ((xwindow (xlib:create-window
		     :PARENT (window-real-window superior)
		     :BIT-GRAVITY bit-gravity
		     :SAVE-UNDER save-under
		     :BORDER-WIDTH borders
		     :COLORMAP (xlib:screen-default-colormap *Screen*)
		     :EVENT-MASK event-mask
		     :X (%- real-left sup-left)
		     :Y (%- real-top sup-top)
		     :BACKGROUND white
		     :BORDER black
		     :WIDTH (%- real-right real-left borders borders)
		     :HEIGHT (%- real-bottom real-top borders borders)))
	  (window (make-window-of-type window-type)))
      (setf (window-real-window window) xwindow
	    (window-superior window) superior
	    (window-more-p window) more-p
	    (window-end-of-page-mode window) end-of-page-mode
	    (window-end-of-line-mode window) end-of-line-mode
	    (window-border-margin-width window) borders
	    (window-line-spacing window) (or vsp line-spacing)
	    (window-left window) real-left
	    (window-top window) real-top
	    (window-width window) (%- real-right real-left)
	    (window-height window) (%- real-bottom real-top))
      (when (or name default-name)
	(setf (window-name window) (or name default-name)))
      (when blinker-p
	(setf (window-cursor window)
	      #-EW-CLOS (make-cursor) #+EW-CLOS (make-instance 'CURSOR))
	(setf (cursor-gcontext (window-cursor window))
	      (xlib:create-gcontext :DRAWABLE xwindow
				    :BACKGROUND white
				    :FOREGROUND black :FUNCTION (draw-alu :FLIP))))
      (let ((black (xlib:screen-black-pixel *Screen*))
	    (white (xlib:screen-white-pixel *Screen*))
	    (font (xlib:open-font *X-Display* "fixed")))
	(setf (window-gcontext window)
	      (xlib:create-gcontext :DRAWABLE xwindow
				    :BACKGROUND white
				    :FOREGROUND black
				    :FONT font
				    :FUNCTION gcontext-function
				    :STIPPLE gcontext-tile
				    :FILL-STYLE gcontext-fill-style))
	(setf (xlib:window-cursor xwindow) mouse-nw-arrow)
	(when expose-p (xlib:map-window xwindow))
	(set-window-style window default-style)
	(recompute-window-margins window)
	(when label (setup-window-label window label))
	(process-margin-components window margin-components)
	(sync)
	(add-windows window)
	(when integral-p
	  ;; make the inside an integral number of characters
	  (multiple-value-bind (inside-width inside-height)
	      (window-inside-size window)
	    (let ((char-width (default-char-width window))
		  (line-height (window-line-height window)))
	      (set-window-inside-size
		window
		(%* char-width
		    (%1+ (the (values fixnum number)  (floor (%1- inside-width) char-width))))
		(%* line-height
		   (%1+ (the (values fixnum number)
			     (floor (%1- inside-height) line-height))))))))
	window))))


(defun make-gc-context (window)
  (let ((black (xlib:screen-black-pixel *Screen*))
	(white (xlib:screen-white-pixel *Screen*))
	(font (xlib:open-font *X-Display* "fixed")))
    (xlib:create-gcontext :DRAWABLE (window-real-window window)
			  :BACKGROUND white
			  :FOREGROUND black
			  :FONT font)))

#+X
(defun make-window-of-type (type)
  (let ((name (and (symbolp type)
		   (lisp-format NIL "~A ~D" (make-pretty-name (symbol-name type))
				(if (numberp (get type :WINDOW-COUNT))
				    (incf (get type :WINDOW-COUNT))
				    (setf (get type :WINDOW-COUNT) 1))))))
    #-EW-CLOS
    (case type
      ((DYNAMIC-WINDOW WINDOW) (make-presentation-window :NAME name))
      (SCROLL-BAR (make-scroll-bar :NAME name))
      (LABEL (make-label :NAME name))
      (program-frame (make-program-frame :NAME name))
      (program-pane (make-program-pane :NAME name))
      (program-command-menu (make-program-command-menu :NAME name))
      (program-title-pane (make-program-title-pane :NAME name))
      (program-display-pane (make-program-display-pane :NAME name))
      (program-query-values-pane (make-program-query-values-pane :NAME name))
      (T type))
    #+EW-CLOS
    (if (symbolp type)
	(make-instance (if (member type '(dynamic-window window))
			   'presentation-window type) :NAME name)
	type)
    ))



(defmethod draw-margins ((window window))
  (draw-scroll-bars window)
  (dolist (margin (presentation-window-margins window))
    (typecase margin
      (label (draw-window-label window margin)))))

(defmacro update-vertical-scroll-bars (window bottom)
  `(progn
     (setf (presentation-window-max-y-position ,window) ,bottom)
     (or *Inhibit-Scroll-Bar-P*
	 (draw-scroll-bars ,window '(:left :right) NIL))))

(defmacro update-horizontal-scroll-bars (window right)
  `(progn
     (setf (presentation-window-max-x-position ,window) ,right)
     (or *Inhibit-Scroll-Bar-P*
	 (draw-scroll-bars ,window '(:top :bottom) NIL))))

(defmethod draw-scroll-bars ((window window)
			     &OPTIONAL (bar-types '(:left :right :top :bottom))
			     (redisplay-all-p T))
  #.(fast)
  (dolist (bar (presentation-window-margins window))
    (when (and (lisp:typep bar 'SCROLL-BAR)
	       (member (window-margin-type bar) bar-types))
      (let ((visibility (scroll-bar-visibility bar))
	    (current-visibility (scroll-bar-current-visibility bar)))
	(if (case visibility
	      (:NORMAL T)
	      (:IF-NEEDED
		(if (member (window-margin-type bar) '(:LEFT :RIGHT))
		    (if (or (> (presentation-window-max-y-position window)
			       (window-inside-height window))
			    (not (zerop (window-scroll-y-offset window))))
			(progn
			  (when (eq current-visibility :OFF)
			    (setf (xlib:window-border (window-real-window bar))
				  (xlib:screen-black-pixel *Screen*))
			    (setf (scroll-bar-current-visibility bar) :ON)
			    (setq redisplay-all-p T))
			  T)
			(progn
			  (when (eq current-visibility :ON)
			    (setf (xlib:window-border (window-real-window bar))
				  (xlib:screen-white-pixel *Screen*))
			    (xlib:clear-area (window-real-window bar))
			    (setf (scroll-bar-current-visibility bar) :OFF))
			  NIL))
		    (if (or (> (presentation-window-max-x-position window)
			   (window-inside-width window))
			    (not (zerop (window-scroll-x-offset window))))
			(progn
			  (when (eq current-visibility :OFF)
			    (setf (xlib:window-border (window-real-window bar))
				  (xlib:screen-black-pixel *Screen*))
			    (setf (scroll-bar-current-visibility bar) :ON)
			    (setq redisplay-all-p T))
			  T)
			(progn
			  (when (eq current-visibility :ON)
			    (setf (xlib:window-border (window-real-window bar))
				  (xlib:screen-white-pixel *Screen*))
			    (xlib:clear-area (window-real-window bar))
			    (setf (scroll-bar-current-visibility bar) :OFF))
			  NIL))))
	      (T NIL))
	    (draw-scroll-bars-internal bar window redisplay-all-p)
	    )))))

(defun draw-scroll-bars-internal (bar window redisplay-all-p)
  (let ((real-window (window-real-window bar))
	(gcontext (window-gcontext bar))
	(old-bar-start (scroll-bar-bar-start bar))
	(old-bar-length (scroll-bar-bar-length bar)))
    (declare (fixnum old-bar-start old-bar-length))
    (when (or redisplay-all-p (member (window-margin-type bar) '(:top :bottom)))
      (xlib:clear-area real-window))
    (ecase (window-margin-type bar)
      ((:LEFT :RIGHT)
       (draw-left-right-scroll-bar window bar redisplay-all-p real-window gcontext
				   old-bar-start old-bar-length))
      ((:BOTTOM :TOP)
       (draw-top-bottom-scroll-bar window bar redisplay-all-p real-window gcontext
				   old-bar-start old-bar-length)
       ))))

(defun draw-left-right-scroll-bar (window bar redisplay-all-p real-window gcontext
				   old-bar-start old-bar-length)
  (let* ((height (window-inside-height window))
	 (y-offset (window-scroll-y-offset window))
	 (max-y (presentation-window-max-y-position window))
	 (bar-start
	   (if (zerop max-y)
	       1
	       (max 0
		    (min (the (values fixnum number)
			      (floor (the float
					  (* (the float
						  (/ (the float (float y-offset))
						     (the float (float max-y))))
					     (the float (float height))))))
			 (%- height 10)))))
	 ;; ratio of bar-height to window-height = ratio of
	 ;; text in window to total text avail.
	 (bar-height
	   (the fixnum
		(max 10
		     (if (zerop (the fixnum max-y))
			 height
			 (the (values fixnum number) 
			      (floor (* (the float (float height))
					(the float
					     (/ (float
						  (max 0
						       (min (%- max-y y-offset)
							    height)))
						(the float (float max-y))))))))))))
    (declare (fixnum bar-start bar-height height y-offset max-y))
    (if redisplay-all-p
	(xlib:draw-rectangle real-window gcontext
			     *Scroll-Bar-Quarter-Thickness* bar-start
			     8 bar-height T)
	;; now figure how much to erase or draw at the ends of the bar.
	(cond ((= bar-start old-bar-start)
	       ;; top end stays constant.
	       ;; take some away.
	       (cond ((< old-bar-length bar-height)
		      (xlib:draw-rectangle
			real-window gcontext
			*Scroll-Bar-Quarter-Thickness*
			bar-start
			8 (%- bar-height old-bar-length) T))
		     ((< bar-height old-bar-length)
		      (xlib:draw-rectangle
			real-window gcontext
			*Scroll-Bar-Quarter-Thickness*
			(%- bar-start (%- old-bar-length bar-height))
			8 (%- old-bar-length bar-height) T))))
	      (T ;;(> bar-start old-bar-start)
	       (xlib:draw-rectangle
		 real-window gcontext
		 *Scroll-Bar-Quarter-Thickness*
		 (the fixnum (min old-bar-start bar-start))
		 8 (abs (%- bar-start old-bar-start)) T)
	       (let ((end-difference (%+ (%- bar-height old-bar-length)
					 (%- bar-start old-bar-start))))
		 (declare (fixnum end-difference))
		 (cond ((> end-difference 0)
			(xlib:draw-rectangle
			  real-window gcontext
			  *Scroll-Bar-Quarter-Thickness*
			  (%+ old-bar-start old-bar-length)
			  8 end-difference T))
		       ((< end-difference 0)
			(xlib:draw-rectangle
			  real-window gcontext
			  *Scroll-Bar-Quarter-Thickness*
			  (%+ (%+ old-bar-start old-bar-length) end-difference)
			  8 (%- end-difference) T)))))))
    (setf (scroll-bar-bar-start bar) bar-start
	  (scroll-bar-bar-length bar) bar-height)))

(defun draw-top-bottom-scroll-bar (window bar redisplay-all-p real-window gcontext
				   old-bar-start old-bar-length)
  (declare (ignore old-bar-length old-bar-start redisplay-all-p bar))	; 4/30/90 (SLN)
  (let* ((width (window-inside-width window))
	 (x-offset (window-scroll-x-offset window))
	 (max-x (presentation-window-max-x-position window))
	 (horizontal-scroll-percentage
	   (if (zerop (the fixnum max-x))
	       0.0
	       (min 1.0 (the float
			     (/ (the float (float x-offset))
				(the float (float max-x)))))))
	 (bar-start
	   (max 0 (min (the (values fixnum number)
			    (floor (the float
					(* horizontal-scroll-percentage
					   (the float (float width))))))
		       (%- width 10))))
	 (graphics-width-used (float (max 0 (min (%- max-x x-offset) (the fixnum width)))))
	 (bar-width
	   (max 10
		(if (zerop max-x)
		    width
		    (the (values fixnum number) 
			 (floor
			   (the float
				(* (the float (float width))
				   (the float
					(/ graphics-width-used
					   (the float (float max-x))))))))))))
    (declare (float horizontal-scroll-percentage graphics-width-used)
	     (fixnum max-x width x-offset bar-width bar-start))
    (xlib:draw-rectangle real-window gcontext
			 bar-start *Scroll-Bar-Quarter-Thickness*
			 bar-width 8 T)))

#+X
(defmethod setup-scroll-bar ((window window) component)
  (when (and (consp component) (eq (first component) 'margin-scroll-bar))
    (let ((type (getf (cdr component) :MARGIN :LEFT)))
      (unless (member type '(:left :top :right :bottom))
	(error "Scroll Bar type must be one of ~D." '(:left :top :right :bottom)))
      (let ((w (make-window 'SCROLL-BAR :BLINKER-P NIL :MORE-P NIL :LABEL NIL
			    :SUPERIOR window :EXPOSE-P NIL
			    :GCONTEXT-FUNCTION (draw-alu :FLIP)
			    :GCONTEXT-FILL-STYLE :STIPPLED
			    :GCONTEXT-TILE
			    (get-pattern-pixmap 25%-gray))))
	(setf (xlib:window-cursor (window-real-window w))
	      (if (member type '(:LEFT :RIGHT))
		  mouse-vertical-double-arrow
		  mouse-horizontal-double-arrow)
	      (scroll-bar-window-to-scroll w) window
	      (window-margin-type w) type
	      (scroll-bar-history-noun w) (getf (cdr component) :HISTORY-NOUN)
	      (scroll-bar-visibility w) (getf (cdr component) :VISIBILITY :NORMAL))
	(setf (presentation-window-margins window) (nconc (presentation-window-margins window)
							  (list w)))
	(if (eq (scroll-bar-visibility w) :IF-NEEDED)
	    (setf (scroll-bar-current-visibility w) :OFF
		  (xlib:window-border (window-real-window w))
		  (xlib:screen-white-pixel *Screen*))
	    (setf (scroll-bar-current-visibility w) :ON
		  (xlib:window-border (window-real-window w))
		  (xlib:screen-black-pixel *Screen*)))
	(xlib:map-window (window-real-window w))
	)))
  (recompute-margin-sizes window)
  (recompute-window-margins window))



(defmethod window-kill ((window window))
  (deexpose-window window)
  (when (window-superior window)
    (setf (window-inferiors (window-superior window))
	  (delete window (window-inferiors (window-superior window)))))
  (setq *Windows* (delete window *Windows*))
  (mapc #'window-kill (window-inferiors window))
  (when (presentation-window-p window)
    (mapc #'window-kill (presentation-window-margins window))))

;; Mike Young at Visual Technology 508-459-4903 Brian Croxen Visual-640



;; graphics-textlength textlength
;; compute-cursor-position-in-string
;; char-size char-size-from-font
;; compute-motion-for-string-internal
;; character-size-from-font



(defvar xlib::*Debug* NIL)



#+symbolics
(DEFUN tv:KBD-UNIMPORTANT-MESSAGE (STRING STREAM)
  (COND ((EQ STREAM tv:DEFAULT-BACKGROUND-STREAM))
	((AND (tv:TYPEP STREAM 'tv:SHEET)
	      (OR (tv:SHEET-OUTPUT-HELD-P STREAM) (NOT (tv:SHEET-CAN-GET-LOCK STREAM)))))
	((scl:instancep stream)
	 (FLET ((PRINT-IT (STREAM)
		  (DECLARE (sys:DOWNWARD-FUNCTION))
		  (tv:SEND-IF-HANDLES STREAM :PREPARE-FOR-MORE-TYPEOUT)
		  (tv:SEND STREAM :CLEAR-REST-OF-LINE)
		  (tv:SEND STREAM :STRING-OUT STRING)
		  T))
	   (OR (tv:SEND-IF-HANDLES STREAM :WITH-OUTPUT-RECORDING-ENABLED #'PRINT-IT STREAM T)
	       (PRINT-IT STREAM))))))


(defun bitblt (alu width height from-window from-x from-y to-window to-x to-y)
  (let ((gcontext (window-gcontext to-window)))
    (xlib:with-gcontext (gcontext :FUNCTION (draw-alu alu))
      (xlib:copy-area (window-real-window from-window)
		      gcontext
		      from-x from-y width height
		      (window-real-window to-window)
		      to-x to-y))))





;; should really use xlib:warp-pointer, but that seems to have a bug in it.
(defun mouse-warp (x y &OPTIONAL mouse)
  (declare (ignore mouse))
  (xlib:warp-pointer-relative *X-Display*
			      (%- x *Global-Mouse-X*)
			      (%- y *Global-Mouse-Y*))
  (setq *Global-Mouse-X* x
	*Global-Mouse-Y* x))



(defmacro window-call-relative ((window &OPTIONAL final-action &REST final-action-args)
				&BODY body)
  (declare (ignore final-action final-action-args))	; 4/30/90 (SLN)
  `(window-call-relative-internal ,window #'(lambda ()
					      (progn . ,body))))

(defun window-call-relative-internal (window continuation)
  (unwind-protect
      (progn
	(expose-window window)
	(funcall continuation))
    (deexpose-window window)))

(defun window-string-out (window string &OPTIONAL start end)
  (write-string string window :START (or start 0) :END end))


(defmethod process-margin-components ((window window) margin-components)
  (dolist (component margin-components)
    (case (first component)
      (MARGIN-LABEL
	;; :BACKGROUND-GRAY binary array for background
	;; :BOX enclose label in box. default is NIL :inside :outside
	;; :BOX-THICKNESS, :CENTERED-P default is NIL
	;; :EXTEND-BOX-P default T extends box full length of margin
	;; :MARGIN, :STRING label string
	;; :STYLE, :CHARACTER-STYLE
	(setup-window-label window component))
      (MARGIN-SCROLL-BAR
	;; :elevator-thickness, :history-noun, :margin, :shaft-whitespace-thickness
	;; :visibility
	(setup-scroll-bar window component))
      (MARGIN-BORDERS) ;; :THICKNESS default 1 pixels of black
      (MARGIN-WHITE-BORDERS ;:THICKNESS default 1 pixels of white.
	)
      (MARGIN-WHITESPACE
	;; :MARGIN, :THICKNESS white space.
	))))



#+X
(defmethod setup-window-label ((window window) component)
  (let ((string NIL)
	(margin NIL)
	(style NIL)
	(box T)
	(box-thickness 1))
    (cond ((consp component)
	   (when (eq (first component) 'MARGIN-LABEL)
	     (setq component (cdr component)))
	   (setf string (getf component :STRING)
		 style (or (getf component :STYLE)
			   (getf component :CHARACTER-STYLE))
		 margin (getf component :MARGIN)
		 box-thickness (getf component :BOX-THICKNESS box-thickness)
		 box (getf component :BOX box)))
	  ((stringp component) (setq string component)))
    ;; now all the values are found, go ahead and create the label.
    ;; first look for an existing label that should just be modified.
    (let ((w (if margin
		 (find margin (presentation-window-margins window)
		       :KEY #'window-margin-type)
		 (find-if #'(lambda (label)
			      (lisp:typep label 'label))
			  (presentation-window-margins window)))))
      (unless w
	(multiple-value-bind (left top right bottom)
	    (window-edges window)
	  ;; start edges of window somewhere that should be OK.
	  (setq w (make-window 'LABEL :BLINKER-P NIL :MORE-P NIL :LABEL NIL
			       :LEFT left :TOP top :RIGHT right :BOTTOM bottom
			       :SUPERIOR window :EXPOSE-P NIL
			       :BORDERS (if box box-thickness 0))))
	(setf (window-end-of-page-mode w) :TRUNCATE)
	(setf (presentation-window-margins window)
	      (nconc (presentation-window-margins window) (list w)))
	;; don't do some of the defaults unless we are creating the label the
	;; first time.
	(unless margin (setq margin :BOTTOM))
	(unless string (setq string (window-name window)))
	(unless style
	  (setq style (xfonts::merge-character-styles '(NIL :ITALICS NIL)
						      (window-character-style window)))))
      (when margin (setf (window-margin-type w) margin))
      (when string (setf (label-string w) string))
      (when style (set-window-style w style))
      ;; there seems to be a problem setting the size if the margin has gotten hosed.
      ;; therefore make sure it's at a valid location first.
      ;; need to make sure label is correct height for starters.
      (multiple-value-bind (left top right bottom)
	  (window-edges window)
	(declare (ignore top))
	(let ((number-of-lines (if (stringp (label-string w))
				   (1+ (count #+symbolics #\CR
					      #-symbolics #\Newline
					      (label-string w) :TEST #'EQUAL))
				   1)))
	  (set-window-edges w left
			    (- bottom (%+ 1 (* number-of-lines (window-line-height w))))
			    right bottom)))
      #+ignore
      (set-window-inside-size w 100 (%+ 1 (window-line-height w)))
      (xlib:map-window (window-real-window w))
      (recompute-window-margins w)))
  (recompute-margin-sizes window)
  (recompute-window-margins window))


(defmethod draw-window-label ((window window) margin)
  (let ((string (label-string margin)))
    ;(xlib:clear-area (window-real-window margin))
    (with-presentations-disabled
      #+ignore
      (draw-string string 0 (%+ 2 (font-ascent (window-font margin)))
		   :CHARACTER-STYLE (window-character-style margin)
		   :STREAM margin)
      (clear-window-internal margin)
      (presentation-print-string string margin)
      )))



(defmethod window-color-p ((window window))
;  (declare (ignore window))
  NIL)

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
