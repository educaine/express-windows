;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************



(cl:in-package 'ew)




;;; Given a string and a possible completion and the delimiters - check if the completion
;;; is valid, and if so, return T.
(defun compare-string-with-possible-completion (string completion delims)
  #.(fast)
  (declare (string string completion))
  (if (equal completion "")
      (equal string "")
      (do ((string-index 0)
	   (completion-index 0)
	   (string-length (length string))
	   (completion-length (length completion)))
	  ((or (%= string-length string-index) (%= completion-length completion-index))
	   ;; only return T if string had reached end, return NIL if completion has reached END
	   (%= string-length string-index))
	(declare (fixnum string-index completion-index string-length completion-length))
	(let ((string-character (aref string string-index)))
	  (cond ((member string-character delims :TEST #'CHAR-EQUAL)
		 ;; given a delimiter in the string, we need to search down the completion string
		 ;; until we find a delimiter that matches.
		 (unless (do ((index completion-index (%1+ index)))
			     ((%= index completion-length) (return NIL))
			   (declare (fixnum index))
			   (when (char-equal string-character (aref completion index))
			     (incf string-index)
			     (setq completion-index (%1+ index))
			     (return T)))
		   (return NIL)))
		((not (char-equal string-character (aref completion completion-index)))
		 (return NIL))
		(T (incf string-index)
		   (incf completion-index)))))))

;; expects alist of (string . value) pairs.

(defun complete-string (string alist delims complete-limited-p)
  #.(fast)
  (declare (values possibilities completion-string possible-completion-pair))
  (let ((possibilities NIL))
    (dolist (pair alist)
      (when (compare-string-with-possible-completion string (first pair) delims)
	(push pair possibilities)))
    (if (not possibilities)
	NIL
	(let ((possible-completion (find-gcd-of-completions
				     string possibilities delims complete-limited-p)))
	  (values possibilities possible-completion
		  (assoc possible-completion alist :TEST #'EQUALP))))))

(defun find-gcd-of-completions (input-string possibilities delims complete-limited-p)
  ;;#.(fast)
  ;; GD compiler seems to screwup this function.
  (declare (optimize (speed 0)(compilation-speed 3)))
  (declare (string input-string))
  (when possibilities
    (let ((chunk-lengths NIL)
	  (string-to-compare-to (first (first possibilities))))
      (declare (string string-to-compare-to))
      (declare (list chunk-lengths))
      ;; find length of chunks in first string.
      (let ((string (first (first possibilities))))
	(declare (string string))
	(do ((index 0 (1+ index))
	     (chunk-length 0)
	     (max (length string)))
	    ((= index max) (push chunk-length chunk-lengths))
	  (declare (fixnum index chunk-length max))
	  (let ((character (aref string index)))
	    (if (member character delims :TEST #'CHAR-EQUAL)
		(progn (push chunk-length chunk-lengths)
		       (setq chunk-length 0))
		(incf chunk-length)))))
      (setq chunk-lengths (nreverse chunk-lengths))
      ;; if we are limited to the same number of chunks then it easy to just make the length
      ;; of chunk-lengths the same.
      (when complete-limited-p
;;	let ((number-of-chunks (%1+ (count-if #'(lambda (char)
;;						   (member char delims :TEST #'CHAR-EQUAL))
;;					       input-string))))
	(let ((number-of-chunks 1)
	      (length (length input-string)))
	  (declare (fixnum number-of-chunks))
	  (do ((i 0 (1+ i))) ((= i length))
	      (when (member (aref input-string i) delims :TEST #'CHAR-EQUAL)
		    (setq number-of-chunks (1+ number-of-chunks))))
	  (let ((number-of-chunks-so-far (length chunk-lengths)))
	    (declare (fixnum number-of-chunks-so-far))
	    (when (< number-of-chunks number-of-chunks-so-far)
	      (setq chunk-lengths (butlast chunk-lengths
					   (%- number-of-chunks-so-far number-of-chunks)))))))
      ;; now we have an approximate answer in terms of indexes into each word.
      (dolist (p (cdr possibilities))
	;; now do the intersection by decreasing string chunk indexes as necessary
	(let* ((string (first p))
	       (chunks chunk-lengths)
	       (max (length string))
	       (compare-max (length string-to-compare-to)))
	  (do ((index 0)
	       (compare-index 0)
	       (chunk-length 0))
	      ((or (%= index max) (%= compare-index compare-max))
	       ;; when done, set current chunk
	       (setf (first chunks) (min (first chunks) chunk-length))
	       ;; also get rid of any extra chunks.
	       (setf (cdr chunks) NIL))
	    (declare (fixnum index compare-index chunk-length))
	    (let ((character (aref string index)))
	      (cond ((member character delims :TEST #'CHAR-EQUAL)
		     ;; we are about to start a new chunk, so change this last chunk
		     ;; and then bump it up.
		     (setf (first chunks) (min (first chunks) chunk-length))
		     (pop chunks)
		     ;; if there are now more chunks left, than discontinue looking at this
		     ;; string
		     (when (null chunks) (return NIL))
		     (setq chunk-length 0)
		     (incf index)
		     ;; look for same character in compare string.
		     (setq compare-index (position character string-to-compare-to
						   :TEST #'CHAR-EQUAL :START compare-index))
		     ;; if there are no more chunks in string-to-compare-to
		     ;; then we need to cut off compare here.
		     (if (integerp compare-index)
			 (incf compare-index)
			 ;; cutoff search
			 (return (setf (cdr chunks) NIL))))
		    ((char-equal (aref string-to-compare-to compare-index)
				 (aref string index))
		     (incf chunk-length)
		     (incf index)
		     (incf compare-index))
		    (T ;; strings don't compare. setup chunk-length now and move to next
		     ;; chunk
		     (setf (first chunks) (min (first chunks) chunk-length))
		     (setq chunk-length 0)
		     (setq index (position-if #'(lambda (char)
						  (member char delims :TEST #'CHAR-EQUAL))
					      string :START index))
		     (setq compare-index (position-if
					   #'(lambda (char)
					       (member char delims :TEST #'CHAR-EQUAL))
					      string-to-compare-to :START compare-index))
		     (unless (and (integerp index) (integerp compare-index))
		       ;; terminate compare
		       (return (setf (cdr chunks) NIL)))
		     (pop chunks)
		     (when (null chunks) (return NIL))
		     (incf index)
		     (incf compare-index)))))))
      ;(print chunk-lengths)
      ;; now process string-to-compare using chunks and build up a final response.
      (do ((chunks chunk-lengths (cdr chunks))
	   (index 0)
	   (first-p T NIL) ;; to decide whether to include the delimiter
	   (result (make-string (%+ (%1- (length chunk-lengths))
				    (apply #'+ chunk-lengths))))
	   (result-index 0))
	  ((null chunks) result)
	(declare (fixnum index))
	(dotimes (x (if first-p (first chunks) (%1+ (first chunks))))
	  #+symbolics (declare (ignore x))			;6/27/90 (sln)
	  (setf (aref result result-index)
		(aref string-to-compare-to index))
	  (incf result-index)
	  (incf index))
	;; reset index to next chunk beginning
	(setq index (position-if #'(lambda (char)
				     (member char delims :TEST #'CHAR-EQUAL))
				 string-to-compare-to :START index))))))


(defun complete-pathname (pathname-string complete-limited-p default-version)
  (declare (values possibilities completion-string possible-completion-pair))
  (let* ((pathname (pathname pathname-string))
	 (version (pathname-version pathname))
	 (type (pathname-type pathname))
	 (name (pathname-name pathname))
	 ;(directory (pathname-directory pathname))
	 )
    (when (not version) (setq version default-version))
    (when (or (not type) (equal type "")) (setq type :WILD))
    #+symbolics
    (if (and (member version '(:NEWEST :WILD)) (eq type :WILD))
	;; then add wild character onto name even if if is there.
	(setq name (if (or (eq :WILD name) (null name)) :WILD (concatenate 'STRING name "*")))
	(when (or (not name) (equal name ""))
	  (setq name :WILD)))
    (setq pathname (make-pathname :NAME name :TYPE type :VERSION version :DEFAULTS pathname))
    ;; on lucid at least, DIRECTORY does not work well with partial file names.
    (let ((possibilities (directory #+symbolics pathname
				    #-symbolics
				    (make-pathname :DIRECTORY (pathname-directory pathname)
						   :DEVICE (pathname-device pathname)
						   :NAME :WILD))))
      #-symbolics
      (setq possibilities
	    (mapcar #'second
		    (complete-string (file-namestring pathname)
				     (mapcar #'(lambda (pathname)
						 (list (file-namestring pathname) pathname))
					     possibilities)
				     '(#\. #\- #\_) ())))
      (if (not possibilities)
	  NIL
	  (let ((alist (mapcar #'(lambda (x)
				   (let ((y (namestring x)))
				     (list y x)))
			       possibilities)))
	    (let ((possible-completion (find-gcd-of-completions
					 ;;(namestring pathname)
					 pathname-string
					 alist
					 '(#\Space #\. #\\ #\< #\>)
					 complete-limited-p)))
	      (values alist possible-completion
		      (assoc possible-completion alist :TEST #'EQUALP))))))))


(defvar *Completion-Table* NIL)

(defmacro completing-from-suggestions ((stream
					 &KEY (allow-any-input T)
					 (delimiters *standard-completion-delimiters*)
					 (enable-forced-return NIL)
					 (force-complete NIL)
					 (partial-completers NIL)
					 (type NIL) (parser NIL) (complete-activates NIL)
					 (compress-choices NIL) (compression-delimiter NIL)
					 (initially-display-possibilities NIL))
				       &BODY body)
  `(completing-from-suggestions-internal
     ,stream
     ,allow-any-input ',delimiters
     ,enable-forced-return ,partial-completers ,type ,parser ,complete-activates
     ,compress-choices ,compression-delimiter ,initially-display-possibilities ,force-complete
     #'(lambda () . ,body)))

(defun completing-from-suggestions-internal
       (stream
	allow-any-input delimiters
	enable-forced-return partial-completers type parser complete-activates
	compress-choices compression-delimiter initially-display-possibilities force-complete
	body)
  (declare (ignore complete-activates))		;4/30/90 (SLN)
  (let ((*Completion-Table* NIL))
    (funcall body)
    (complete-input stream
		    #'(lambda (input-string operation)
			(default-complete-function input-string operation
			  *Completion-Table* delimiters))
		    :ALLOW-ANY-INPUT allow-any-input
		    :ENABLE-FORCED-RETURN enable-forced-return
		    :FORCE-COMPLETE force-complete
		    :partial-completers partial-completers
		    :TYPE type
		    :parser parser :compress-choices compress-choices
		    :compression-delimiter compression-delimiter
		    :initially-display-possibilities initially-display-possibilities)))

(defun suggest (completion-string object)
  (push (list completion-string object) *Completion-Table*))


(defun complete-from-sequence (sequence stream &REST other-args
			       &KEY (name-key #'string)
			       (value-key #'identity)
			       (delimiters *standard-completion-delimiters*)
			       (allow-any-input T)
			       (enable-forced-return NIL) (partial-completers NIL)
			       (type NIL) (parser NIL) (complete-activates NIL)
			       (compress-choices NIL) (compression-delimiter NIL)
			       (initially-display-possibilities NIL))
  (declare (values object success string)
	   (ignore allow-any-input enable-forced-return partial-completers
		   type parser complete-activates compress-choices compression-delimiter
		   initially-display-possibilities))
  (let ((completion-list (mapcar #'(lambda (item)
				     (list (funcall name-key item)
					   (funcall value-key item)))
				 sequence)))
    (apply #'complete-input stream
	   #'(lambda (input-string operation)
	       (default-complete-function input-string operation
		 completion-list
		 delimiters))
	   other-args)))

(defun default-complete-pathname (input-string operation alist delims)
  (declare (ignore delims alist))
  (ecase operation
    ((:COMPLETE :COMPLETE-LIMITED :COMPLETE-MAXIMAL)
     ;;complete and return as much as possible based on input so far.
     ;; return 5 values.
     ;; 1. completed string
     ;; 2. Flag T if completion is a complete name.
     ;; 3. Object associated with completion if Flag is T.
     ;; 4. Ambiguity index
     ;; 5. Number of possible completions.
     (multiple-value-bind (possibilities completion-string possible-completion-pair)
	 (complete-pathname input-string (eq operation :COMPLETE-LIMITED) :NEWEST)
       (declare (list possibilities))
       (if possibilities
	   (values completion-string
		   (not (null possible-completion-pair))
		   (second possible-completion-pair)
		   NIL
		   (length possibilities))
	   (values input-string NIL NIL NIL 0))))
    (:POSSIBILITIES
     (multiple-value-bind (possibilities completion-string possible-completion-pair)
	 (complete-pathname input-string (eq operation :COMPLETE-LIMITED) :NEWEST)
       (declare (ignore completion-string possible-completion-pair))
       possibilities))))
      

(defun default-complete-function (input-string operation alist delims)
  (ecase operation
    ((:COMPLETE :COMPLETE-LIMITED :COMPLETE-MAXIMAL)
     ;;complete and return as much as possible based on input so far.
     ;; return 5 values.
     ;; 1. completed string
     ;; 2. Flag T if completion is a complete name.
     ;; 3. Object associated with completion if Flag is T.
     ;; 4. Ambiguity index
     ;; 5. Number of possible completions.
     (multiple-value-bind (possibilities completion-string possible-completion-pair)
	 (complete-string input-string alist delims (eq operation :COMPLETE-LIMITED))
       (declare (list possibilities))
       (if possibilities
	   (values completion-string
		   (not (null possible-completion-pair))
		   (second possible-completion-pair)
		   NIL
		   (length possibilities))
	   (values input-string NIL NIL NIL 0))))
    (:POSSIBILITIES
      (multiple-value-bind (possibilities completion-string possible-completion-pair)
	  (complete-string input-string alist delims (eq operation :COMPLETE-LIMITED))
	(declare (ignore completion-string possible-completion-pair))
	possibilities))))

(defun show-possibilities (stream type initial-location function)
  #.(fast)
  (let* ((state (window-input-editor stream))
	 (total-string (input-editor-state-string state))
	 ;(current-location (read-location stream))
	 (string (subseq total-string initial-location )))
    (let ((possible-completions (funcall function string :POSSIBILITIES)))
      (declare (list possible-completions))
      (case (the fixnum (length possible-completions))
	(0 (terpri stream)
	   (format stream "There are no possible completions for the string ~S" string))
	(1 (terpri stream)
	   (format stream "The only possible completion for the string ~S is : ~%  " string)
	   (display-as (:stream stream :TYPE (or type 'string)
						 :object (second (first possible-completions)))
	     (princ (first (first possible-completions)) stream)))
	(T (terpri stream)
	   (format stream "The completions for the string ~S are as follows:" string)
	   (terpri stream)
	   (setq possible-completions (sort (copy-list possible-completions) #'string-lessp
					    :KEY #'first))
	   #+ignore
	   (make-table-from-sequence possible-completions :STREAM stream
				     :TYPE type :KEY #'SECOND
				     :OPTIMAL-NUMBER-OF-ROWS 1)
	   (with-output-filling (stream)
	     (dolist (item possible-completions)
	       (display-as (:stream stream :TYPE (or type 'string)
				    :object (second item))
		 (princ (first item) stream))
	       (simple-princ "    " stream)))
	   )))))

(defun complete-input (stream function &KEY (allow-any-input NIL) enable-forced-return
		       (allow-empty-input NIL)
		       (force-complete T)
		       partial-completers (type NIL) parser (compress-choices 20)
		       (compression-delimiter) (help-offers-possibilities T)
		       (initially-display-possibilities NIL) (complete-activates NIL)
		       (documenter NIL) (document)
		       &ALLOW-OTHER-KEYS)
  (declare (ignore force-complete))
  (warn-unimplemented-args documenter NIL document NIL
			   parser NIL compress-choices 20 compression-delimiter NIL
			   help-offers-possibilities t initially-display-possibilities NIL
			   complete-activates NIL
			   enable-forced-return NIL)
  (let ((initial-location (read-location stream))
	(state (window-input-editor stream)))
    (with-token-delimiters (partial-completers)
    (with-token-delimiters (#+symbolics #\complete #-symbolics #\ESC)
      ;; read tokens until we win.
      (do ()
	  (())
	(flet ((possibilities (stream)
		 (show-possibilities stream type initial-location function)))
	  (with-input-editing-options (((:COMPLETE-HELP :override) #'possibilities))
	    (read-standard-token stream)))
	(let* ((delimiter (peek-char-for-query stream))
	       (total-string (input-editor-state-string state))
	       (current-location (read-location stream))
	       (string (subseq total-string initial-location current-location)))
	  (unless (listp delimiter)
	    (error "Shouldn't the delimiter be a list."))
	  (cond ((and allow-empty-input (equal "" string))
		 (return NIL))
		((member (second delimiter) partial-completers)
		 ;; complete upto token
		 (multiple-value-bind (completed-string completion-valid object
				       ambiguity number-of-completions)
		     (funcall function string :COMPLETE-LIMITED)
		   (declare (ignore ambiguity) (fixnum number-of-completions))
		   (cond ((zerop number-of-completions)
			  #+ignore
			  (replace-input-editor-string
			      stream (read-location stream)
			      (string (second delimiter))
			      :DONT-QUOTE t)
			  (parse-error string NIL "Not a Valid Possibility")
			  (beep)
			  (read-char-for-query stream)
			  #+ignore ;; 3/25/89
			  (if allow-any-input
			      (replace-input-editor-string
				stream (read-location stream)
				(string (second delimiter))
				:DONT-QUOTE t)))
			 ((and (= 1 number-of-completions)
			       completion-valid)
			  #+ignore(when *Debug*
			    (dformat "~&One Completion ~A." completed-string))
			  (replace-input-editor-string
			    stream initial-location completed-string)
			  (return (values object T completed-string)))
			 (T (replace-input-editor-string
			      stream initial-location completed-string)
			    (read-char-for-query stream)
			    #+ignore ;; 3/25/89 spaces insert now.
			    (replace-input-editor-string
			      stream (read-location stream)
			      (string (second delimiter))
			      :DONT-QUOTE t)
			    ))))
		(T (multiple-value-bind (completed-string completion-valid object
					 ambiguity number-of-completions)
		       (funcall function string :COMPLETE-MAXIMAL)
		     (declare (ignore ambiguity) (fixnum number-of-completions))
		     (cond ((zerop number-of-completions)
			    (if allow-any-input
				(return string)
				(parse-error string NIL "Not a Valid Possibility")))
			   ((= number-of-completions 1)
			    (replace-input-editor-string
			      stream initial-location completed-string)
			    (return (values object T completed-string)))
			   (completion-valid
			    (replace-input-editor-string
			      stream initial-location completed-string)
			    (return (values object T completed-string)))
			   (T
			    (replace-input-editor-string
			      stream initial-location completed-string)
			    (parse-error string NIL "Not a Valid Possibility"))))))))))))

#+ignore
(trace read-char-for-query unread-char-for-query peek-char-for-query
       replace-input-editor-string query read-location
       editor complete-input default-complete-function read-standard-token)

#+ignore
(defun test (&AUX (test-window test-window))
  (select-window test-window)
  (clear-history test-window)
  (query '((keyword-argument (("First Christine" :first)
					("Second comes Andrew" :second)
					("Second comes Everyone" :second)
					))) 
	  :stream test-window))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
