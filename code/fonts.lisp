;;; -*- Mode: LISP; Syntax: Common-lisp; Package: XFONTS; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'xfonts)


(export '(init-fonts get-font-from-style merge-character-styles))


;family :FIX :COURIER :NEW-CENTURY-SCHOOLBOOK :HELVETICA :CHARTER :SYMBOL
;				 --- :swiss :dutch :helvetica
;face :roman :bold :bold-italic :italic -- :bold-extended 
;size :very-small :small :normal :large :very-large :huge

(defvar *Cached-Font* NIL)
(defvar *Cached-Font-Style* NIL)

(defvar *Font-Table*)

(setq
  *Font-Table*
  '((:FIX ;;:COURIER
      (:ROMAN
	(:SMALL 	 -adobe-courier-medium-r-normal--10-100-75-75-m-60-iso8859-1)
	(:NORMAL 	 -adobe-courier-medium-r-normal--12-120-75-75-m-70-iso8859-1)
	(:LARGE 	 -adobe-courier-medium-r-normal--14-140-75-75-m-90-iso8859-1)
	(:VERY-LARGE -adobe-courier-medium-r-normal--18-180-75-75-m-110-iso8859-1)
	(:HUGE 	 -adobe-courier-medium-r-normal--24-240-75-75-m-150-iso8859-1)
	(:VERY-SMALL -adobe-courier-medium-r-normal--8-80-75-75-m-50-iso8859-1))
      (:ITALIC
	(:SMALL      -adobe-courier-medium-o-normal--10-100-75-75-m-60-iso8859-1)
	(:NORMAL     -adobe-courier-medium-o-normal--12-120-75-75-m-70-iso8859-1)
	(:large      -adobe-courier-medium-o-normal--14-140-75-75-m-90-iso8859-1)
	(:very-large -adobe-courier-medium-o-normal--18-180-75-75-m-110-iso8859-1)
	(:huge       -adobe-courier-medium-o-normal--24-240-75-75-m-150-iso8859-1)
	(:very-small -adobe-courier-medium-o-normal--8-80-75-75-m-50-iso8859-1))
      (:BOLD
	(:small      -adobe-courier-bold-r-normal--10-100-75-75-m-60-iso8859-1)
	(:normal     -adobe-courier-bold-r-normal--12-120-75-75-m-70-iso8859-1)
	(:large      -adobe-courier-bold-r-normal--14-140-75-75-m-90-iso8859-1)
	(:very-large -adobe-courier-bold-r-normal--18-180-75-75-m-110-iso8859-1)
	(:huge       -adobe-courier-bold-r-normal--24-240-75-75-m-150-iso8859-1)
	(:very-small -adobe-courier-bold-r-normal--8-80-75-75-m-50-iso8859-1))
      (:bold-italic
	(:small      -adobe-courier-bold-o-normal--10-100-75-75-m-60-iso8859-1)
	(:normal     -adobe-courier-bold-o-normal--12-120-75-75-m-70-iso8859-1)
	(:large      -adobe-courier-bold-o-normal--14-140-75-75-m-90-iso8859-1)
	(:very-large -adobe-courier-bold-o-normal--18-180-75-75-m-110-iso8859-1)
	(:huge       -adobe-courier-bold-o-normal--24-240-75-75-m-150-iso8859-1)
	(:very-small -adobe-courier-bold-o-normal--8-80-75-75-m-50-iso8859-1)))

    (:HELVETICA
      (:ROMAN
	(:small      -adobe-helvetica-medium-r-normal--10-100-75-75-p-56-iso8859-1)
	(:normal     -adobe-helvetica-medium-r-normal--12-120-75-75-p-67-iso8859-1)
	(:large      -adobe-helvetica-medium-r-normal--14-140-75-75-p-77-iso8859-1)
	(:very-large -adobe-helvetica-medium-r-normal--18-180-75-75-p-98-iso8859-1)
	(:huge       -adobe-helvetica-medium-r-normal--24-240-75-75-p-130-iso8859-1)
	(:very-small -adobe-helvetica-medium-r-normal--8-80-75-75-p-46-iso8859-1))
      (:italic
	(:small      -adobe-helvetica-medium-o-normal--10-100-75-75-p-57-iso8859-1)
	(:normal     -adobe-helvetica-medium-o-normal--12-120-75-75-p-67-iso8859-1)
	(:large      -adobe-helvetica-medium-o-normal--14-140-75-75-p-78-iso8859-1)
	(:very-large -adobe-helvetica-medium-o-normal--18-180-75-75-p-98-iso8859-1)
	(:huge       -adobe-helvetica-medium-o-normal--24-240-75-75-p-130-iso8859-1)
	(:very-small -adobe-helvetica-medium-o-normal--8-80-75-75-p-47-iso8859-1))
      (:bold
	(:small      -adobe-helvetica-bold-r-normal--10-100-75-75-p-60-iso8859-1)
	(:normal     -adobe-helvetica-bold-r-normal--12-120-75-75-p-70-iso8859-1)
	(:large      -adobe-helvetica-bold-r-normal--14-140-75-75-p-82-iso8859-1)
	(:very-large -adobe-helvetica-bold-r-normal--18-180-75-75-p-103-iso8859-1)
	(:huge       -adobe-helvetica-bold-r-normal--24-240-75-75-p-138-iso8859-1)
	(:very-small -adobe-helvetica-bold-r-normal--8-80-75-75-p-50-iso8859-1))
      (:bold-italic
	(:small      -adobe-helvetica-bold-o-normal--10-100-75-75-p-60-iso8859-1)
	(:normal     -adobe-helvetica-bold-o-normal--12-120-75-75-p-69-iso8859-1)
	(:large      -adobe-helvetica-bold-o-normal--14-140-75-75-p-82-iso8859-1)
	(:very-large -adobe-helvetica-bold-o-normal--18-180-75-75-p-104-iso8859-1)
	(:huge       -adobe-helvetica-bold-o-normal--24-240-75-75-p-138-iso8859-1)
	(:very-small -adobe-helvetica-bold-o-normal--8-80-75-75-p-50-iso8859-1)))

    (:NEW-CENTURY-SCHOOLBOOK
      (:roman
	(:small      |-adobe-new century schoolbook-medium-r-normal--10-100-75-75-p-60-iso8859-1|)
	(:normal     |-adobe-new century schoolbook-medium-r-normal--12-120-75-75-p-70-iso8859-1|)
	(:large      |-adobe-new century schoolbook-medium-r-normal--14-140-75-75-p-82-iso8859-1|)
	(:very-large |-adobe-new century schoolbook-medium-r-normal--18-180-75-75-p-103-iso8859-1|)
	(:huge       |-adobe-new century schoolbook-medium-r-normal--24-240-75-75-p-137-iso8859-1|)
	(:very-small |-adobe-new century schoolbook-medium-r-normal--8-80-75-75-p-50-iso8859-1|))
      (:italic
	(:small      |-adobe-new century schoolbook-medium-i-normal--10-100-75-75-p-60-iso8859-1|)
	(:normal     |-adobe-new century schoolbook-medium-i-normal--12-120-75-75-p-70-iso8859-1|)
	(:large      |-adobe-new century schoolbook-medium-i-normal--14-140-75-75-p-81-iso8859-1|)
	(:very-large |-adobe-new century schoolbook-medium-i-normal--18-180-75-75-p-104-iso8859-1|)
	(:huge       |-adobe-new century schoolbook-medium-i-normal--24-240-75-75-p-136-iso8859-1|)
	(:very-small |-adobe-new century schoolbook-medium-i-normal--8-80-75-75-p-50-iso8859-1|))
      (:bold
	(:small      |-adobe-new century schoolbook-bold-r-normal--10-100-75-75-p-66-iso8859-1|)
	(:normal     |-adobe-new century schoolbook-bold-r-normal--12-120-75-75-p-77-iso8859-1|)
	(:large      |-adobe-new century schoolbook-bold-r-normal--14-140-75-75-p-87-iso8859-1|)
	(:very-large |-adobe-new century schoolbook-bold-r-normal--18-180-75-75-p-113-iso8859-1|)
	(:huge       |-adobe-new century schoolbook-bold-r-normal--24-240-75-75-p-149-iso8859-1|)
	(:very-small |-adobe-new century schoolbook-bold-r-normal--8-80-75-75-p-56-iso8859-1|))
      (:bold-italic
	(:small      |-adobe-new century schoolbook-bold-i-normal--10-100-75-75-p-66-iso8859-1|)
	(:normal     |-adobe-new century schoolbook-bold-i-normal--12-120-75-75-p-76-iso8859-1|)
	(:large      |-adobe-new century schoolbook-bold-i-normal--14-140-75-75-p-88-iso8859-1|)
	(:very-large |-adobe-new century schoolbook-bold-i-normal--18-180-75-75-p-111-iso8859-1|)
	(:huge       |-adobe-new century schoolbook-bold-i-normal--24-240-75-75-p-148-iso8859-1|)
	(:very-small |-adobe-new century schoolbook-bold-i-normal--8-80-75-75-p-56-iso8859-1|)))

    (:TIMES
      (:roman
	(:normal     -adobe-times-medium-r-normal--12-120-75-75-p-64-iso8859-1)
	(:large      -adobe-times-medium-r-normal--14-140-75-75-p-74-iso8859-1)
	(:very-large -adobe-times-medium-r-normal--18-180-75-75-p-94-iso8859-1)
	(:huge       -adobe-times-medium-r-normal--24-240-75-75-p-124-iso8859-1)
	(:very-small -adobe-times-medium-r-normal--8-80-75-75-p-44-iso8859-1))
      (:italic
	(:small      -adobe-times-medium-i-normal--10-100-75-75-p-52-iso8859-1)
	(:normal     -adobe-times-medium-i-normal--12-120-75-75-p-63-iso8859-1)
	(:large      -adobe-times-medium-i-normal--14-140-75-75-p-73-iso8859-1)
	(:very-large -adobe-times-medium-i-normal--18-180-75-75-p-94-iso8859-1)
	(:huge       -adobe-times-medium-i-normal--24-240-75-75-p-125-iso8859-1)
	(:very-small -adobe-times-medium-i-normal--8-80-75-75-p-42-iso8859-1)
	(:small      -adobe-times-medium-r-normal--10-100-75-75-p-54-iso8859-1))
      (:bold
	(:small      -adobe-times-bold-r-normal--10-100-75-75-p-57-iso8859-1)
	(:normal     -adobe-times-bold-r-normal--12-120-75-75-p-67-iso8859-1)
	(:large      -adobe-times-bold-r-normal--14-140-75-75-p-77-iso8859-1)
	(:very-large -adobe-times-bold-r-normal--18-180-75-75-p-99-iso8859-1)
	(:huge       -adobe-times-bold-r-normal--24-240-75-75-p-132-iso8859-1)
	(:very-small -adobe-times-bold-r-normal--8-80-75-75-p-47-iso8859-1))
      (:bold-italic
	(:small      -adobe-times-bold-i-normal--10-100-75-75-p-57-iso8859-1)
	(:normal     -adobe-times-bold-i-normal--12-120-75-75-p-68-iso8859-1)
	(:large      -adobe-times-bold-i-normal--14-140-75-75-p-77-iso8859-1)
	(:very-large -adobe-times-bold-i-normal--18-180-75-75-p-98-iso8859-1)
	(:huge       -adobe-times-bold-i-normal--24-240-75-75-p-128-iso8859-1)
	(:very-small -adobe-times-bold-i-normal--8-80-75-75-p-47-iso8859-1)))

    (:CHARTER
      (:roman
	(:small      -bitstream-charter-medium-r-normal--10-100-75-75-p-56-iso8859-1)
	(:normal     -bitstream-charter-medium-r-normal--12-120-75-75-p-67-iso8859-1)
	(:large      -bitstream-charter-medium-r-normal--15-140-75-75-p-84-iso8859-1)
	(:very-large -bitstream-charter-medium-r-normal--19-180-75-75-p-106-iso8859-1)
	(:huge       -bitstream-charter-medium-r-normal--25-240-75-75-p-139-iso8859-1)
	(:very-small -bitstream-charter-medium-r-normal--8-80-75-75-p-45-iso8859-1))
      (:italic
	(:small      -bitstream-charter-medium-i-normal--10-100-75-75-p-55-iso8859-1)
	(:normal     -bitstream-charter-medium-i-normal--12-120-75-75-p-65-iso8859-1)
	(:large      -bitstream-charter-medium-i-normal--15-140-75-75-p-82-iso8859-1)
	(:very-large -bitstream-charter-medium-i-normal--19-180-75-75-p-103-iso8859-1)
	(:huge      -bitstream-charter-medium-i-normal--25-240-75-75-p-136-iso8859-1)
	(:very-small -bitstream-charter-medium-i-normal--8-80-75-75-p-44-iso8859-1))
      (:bold
	(:small      -bitstream-charter-bold-r-normal--10-100-75-75-p-63-iso8859-1)
	(:normal     -bitstream-charter-bold-r-normal--12-120-75-75-p-75-iso8859-1)
	(:large      -bitstream-charter-bold-r-normal--15-140-75-75-p-94-iso8859-1)
	(:very-large -bitstream-charter-bold-r-normal--19-180-75-75-p-119-iso8859-1)
	(:huge       -bitstream-charter-bold-r-normal--25-240-75-75-p-157-iso8859-1)
	(:very-small -bitstream-charter-bold-r-normal--8-80-75-75-p-50-iso8859-1))
      (:bold-italic
	(:small      -bitstream-charter-bold-i-normal--10-100-75-75-p-62-iso8859-1)
	(:normal     -bitstream-charter-bold-i-normal--12-120-75-75-p-74-iso8859-1)
	(:large      -bitstream-charter-bold-i-normal--15-140-75-75-p-93-iso8859-1)
	(:very-large -bitstream-charter-bold-i-normal--19-180-75-75-p-117-iso8859-1)
	(:huge       -bitstream-charter-bold-i-normal--25-240-75-75-p-154-iso8859-1)
	(:very-small -bitstream-charter-bold-i-normal--8-80-75-75-p-50-iso8859-1)))

    (:SYMBOL				
      (:roman
	(:small      -adobe-symbol-medium-r-normal--10-100-75-75-p-61-adobe-fontspecific)
	(:normal     -adobe-symbol-medium-r-normal--12-120-75-75-p-74-adobe-fontspecific)
	(:large      -adobe-symbol-medium-r-normal--14-140-75-75-p-85-adobe-fontspecific)
	(:very-large -adobe-symbol-medium-r-normal--18-180-75-75-p-107-adobe-fontspecific)
	(:huge       -adobe-symbol-medium-r-normal--24-240-75-75-p-142-adobe-fontspecific)
	(:very-small -adobe-symbol-medium-r-normal--8-80-75-75-p-51-adobe-fontspecific)))))


(dolist (family *Font-Table*)
  (dolist (face (cdr family))
    (dolist (size (cdr face))
      (let ((font-name (second size)))
	(when font-name
	  (eval `(defvar ,font-name))
	  (export font-name 'XFONTS))))))

(defun get-defined-character-families ()
  (mapcar #'first *Font-Table*))

(defun get-defined-character-faces-for-family (family)
  (mapcar #'first (cdr (assoc family *Font-Table*))))

(defun get-defined-character-sizes-for-family-face (family face)
  (mapcar #'first
	  (cdr (assoc face (cdr (assoc family *Font-Table*))))))

(defun merge-character-styles (style default-style)
  (cond ((equal default-style style)
	 default-style)
	((and (first style) (second style) (third style)
	      (not (member (third style) '(:smaller :larger))))
	 ;; minimize the consing
	 style)
	(T (list (or (first style) (first default-style))
		 (or (second style) (second default-style))
		 (case (third style)
		   (:SMALLER
		     (case (third default-style)
		       (:NORMAL :SMALL)
		       (:large :NORMAL)
		       (:very-large :large)
		       (:HUGE :VERY-LARGE)
		       (T :VERY-SMALL)))
		   (:LARGER
		     (case (third default-style)
		       (:NORMAL :large)
		       (:very-small :small)
		       (:SMALL :normal)
		       (:VERY-LARGE :HUGE)
		       (T :very-large)))
		   (T (or (third style) (third default-style))))))))

(defun get-font-from-style (style)
  (if (equal style *Cached-Font-Style*)
      *Cached-Font*
      (let ((font (symbol-value
		    (or (second (assoc (third style)
				       (cdr (assoc (second style)
						   (cdr (assoc (first style)
							       *Font-Table*))))))
			(second (assoc :NORMAL
				       (cdr (assoc :ROMAN
						   (cdr (assoc :FIX *Font-Table*))))))
			(error "~A is an unknown character style." style)))))
	(setq *Cached-Font-Style* style
	      *Cached-Font* font))))


;; 5/17/90 (sln) Added this.  Looks okay.  Whaddya think?
(defun backtranslate-font (font &rest ignore)
  (if (equal font *Cached-Font*) *Cached-Font-Style*
      (loop named find-the-font
	    for family in *font-table*
	    do (loop for faces in (rest family)
		     do (loop for (size xfont) in (rest faces)
			      when (equal xfont font)
				do (return-from find-the-font (list (first family) (first faces) size)))))))

#|
(loop for family in *font-table*
	    do (format t "~&XXX ~s" family))
(loop for family in *font-table*
	    do (loop for face in (rest family)
		     do (format t "~&XXX ~s ~s" (first family) (first face))))
(loop for family in *font-table*
	    do (loop for face in (rest family)
		     do (loop for (size font) in (rest face)
			      do (format t "~&~s ~s ~s ~s" (first family) (first face) size font))))
|#

(defun init-fonts ()
  (setq *Cached-Font-Style* NIL
	*Cached-Font* NIL)
  (dolist (family *Font-Table*)
    (dolist (face (cdr family))
      (dolist (size (cdr face))
	(let ((font-name (second size)))
	  (when font-name
	    ;(eval `(defvar ,font-name)) ; 7/1/90 [sln] not needed?  It's done at load time.
	    (setf (symbol-value font-name)
		  ;#+CLX ; 7/1/90 [sln] not needed.  We only using CLX.
		  (xlib::open-font express-windows::*X-Display*
				  (string-downcase (symbol-name font-name))))
	    ;(export font-name 'XFONTS) ; 7/1/90 [sln] not needed?  It's done at load-time.
	    ))))))



(defun fontp (font)
  #+CLX (lisp:typep font 'xlib::font)
  #-CLX (error "Not Implemented."))

(defun font-name (font)
  #+CLX (xlib::font-name font)
  #-CLX (error "Not Implemented."))




#+ignore
(:family family
 (:size size
  (:face face target-font
	 :face face target-font
	 :face face target-font)
  :size size
  (:face face target-font
	 :face                )))

#+ignore
(defun define-character-style-families (device character-set &REST plists)
  (declare (ignore device character-set))
  (dolist (plist1 plists)
    (let ((characteristic1 (first plist1)))
      (do ((sub-values1 (cdr plist1) (cddr sub-values1)))
	  ((null sub-values1))
	(let ((value1 (first sub-values1))
	      (plist2 (second sub-values1)))
	  (let ((characteristic2 (first plist2)))
	    (do ((sub-values2 (cdr plist2) (cddr sub-values2)))
		((null sub-values2))
	      (let ((value2 (first sub-values2))
		    (plist3 (second sub-values2)))
		(let ((characteristic3 (first plist3)))
		  (do ((sub-values3 (cdr plist3) (cddr sub-values3)))
		      ((null sub-values3))
		    (let ((value3 (first sub-values3))
			  (font (second sub-values3)))
		      (let ((family (cond ((eq :FAMILY characteristic1)
					   value1)
					  ((eq :FAMILY characteristic2)
					   value2)
					  ((eq :FAMILY characteristic3)
					   value3)))
			    (face (cond ((eq :FACE characteristic1)
					   value1)
					  ((eq :FACE characteristic2)
					   value2)
					  ((eq :FACE characteristic3)
					   value3)))
			    (size (cond ((eq :SIZE characteristic1)
					   value1)
					  ((eq :SIZE characteristic2)
					   value2)
					  ((eq :SIZE characteristic3)
					   value3))))
			;; now define the mapping.
			(labels ((define-family ()
				   (if (eq family '*)
				       (error "Do you really do that?")
				       (progn
					 (let ((family-entry (assoc family *Font-Table*)))
					   (unless family-entry
					     (setq family-entry (list family))
					     (push family-entry *Font-Table*)))
					 ;; now look for the face.
					 (define-face family-entry))))
				 (define-face (family-entry)
				   (if (eq face '*)
				       (dolist (face-entry (cdr family-entry))
					 (define-size face-entry))
				       (let ((face-entry (assoc face family-entry)))
					 (unless face-entry
					   (setq face-entry
						 (list face))
					   (push face-entry (cdr family-entry)))
					 (define-size face-entry))))
				 (define-size (face-entry)
				   (if (eq size '*)
				       (dolist (size-entry (cdr face-entry)))))))))))))))))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
