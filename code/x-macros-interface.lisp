;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)


(defvar *Mouse-Character-Names* NIL)

(defmacro create-mouse-characters (&AUX result names)
  (dolist (bits '(0 1 2 3 16 17 18 19)) ;; until we have a hyper or super key to worry about.
    (dotimes (button 3)
      (let ((button-name
	      (concatenate 'STRING
			   (if (zerop (ldb '#.(byte 1 4) bits)) "" "Shift-")
			   (if (zerop (ldb '#.(byte 1 3) bits)) "" "Hyper-")
			   (if (zerop (ldb '#.(byte 1 2) bits)) "" "Super-")
			   (if (zerop (ldb '#.(byte 1 1) bits)) "" "Meta-")
			   (if (zerop (ldb '#.(byte 1 0) bits)) "" "Control-")
			   "Mouse-"
			   (case button
			     (0 "Left")
			     (1 "Middle")
			     (2 "Right"))))
	    (gestures NIL))
	(push (intern (concatenate 'STRING
				   (if (zerop (ldb '#.(byte 1 4) bits)) "" "SHIFT-")
				   (if (zerop (ldb '#.(byte 1 3) bits)) "" "HYPER-")
				   (if (zerop (ldb '#.(byte 1 2) bits)) "" "SUPER-")
				   (if (zerop (ldb '#.(byte 1 1) bits)) "" "META-")
				   (if (zerop (ldb '#.(byte 1 0) bits)) "" "CONTROL-")
				   (case button
				     (0 "LEFT")
				     (1 "MIDDLE")
				     (2 "RIGHT")))
		      (find-package "KEYWORD"))
	      gestures)
	(pushnew (intern (concatenate 'STRING
				      (if (zerop (ldb '#.(byte 1 4) bits)) "" "SHIFT-")
				      (if (zerop (ldb '#.(byte 1 3) bits)) "" "HYPER-")
				      (if (zerop (ldb '#.(byte 1 2) bits)) "" "SUPER-")
				      (if (zerop (ldb '#.(byte 1 0) bits)) "" "CONTROL-")
				      (if (zerop (ldb '#.(byte 1 1) bits)) "" "META-")
				      (case button
					(0 "LEFT")
					(1 "MIDDLE")
					(2 "RIGHT")))
			 (find-package "KEYWORD"))
	      gestures)
	(pushnew (intern (concatenate 'STRING
				      (if (zerop (ldb '#.(byte 1 3) bits)) "" "HYPER-")
				      (if (zerop (ldb '#.(byte 1 2) bits)) "" "SUPER-")
				      (if (zerop (ldb '#.(byte 1 1) bits)) "" "META-")
				      (if (zerop (ldb '#.(byte 1 0) bits)) "" "CONTROL-")
				      (if (zerop (ldb '#.(byte 1 4) bits)) "" "SHIFT-")
				      (case button
					(0 "LEFT")
					(1 "MIDDLE")
					(2 "RIGHT")))
			 (find-package "KEYWORD"))
	      gestures)
	(pushnew (intern (concatenate 'STRING
				      (if (zerop (ldb '#.(byte 1 3) bits)) "" "HYPER-")
				      (if (zerop (ldb '#.(byte 1 2) bits)) "" "SUPER-")
				      (if (zerop (ldb '#.(byte 1 0) bits)) "" "CONTROL-")
				      (if (zerop (ldb '#.(byte 1 1) bits)) "" "META-")
				      (if (zerop (ldb '#.(byte 1 4) bits)) "" "SHIFT-")
				      (case button
					(0 "LEFT")
					(1 "MIDDLE")
					(2 "RIGHT")))
			 (find-package "KEYWORD"))
	      gestures)
	(dolist (gesture gestures)
	  (push (cons
		  (intern (string-upcase (concatenate 'string "*" button-name "*"))
			  (find-package "EXPRESS-WINDOWS"))
		  gesture)
	      names))
	(push `(defvar ,(first (first names))
		       #+(and symbolics (not x))
		       (read-from-string (concatenate 'string "#\\" button-name))
		       #+X
		       ,(+ (%* 8 bits) (expt 2 button)))
	      result))))
  (setq names (nreverse names))
  `(progn (setq *Mouse-Character-Names* ',names)
	  ,(list 'export `',(mapcar #'first names))
	  . ,result))

(create-mouse-characters)

(deftype mouse-char () '(unsigned-byte 8))
(deftype mouse-button () '(unsigned-byte 3))

(defvar *mouse-x*
	#+(and symbolics (not x)) 'tv:mouse-x
	#+X '*Local-Mouse-X*)
(defvar *mouse-y*
	#+(and symbolics (not x)) 'tv:mouse-y
	#+X '*Local-Mouse-Y*)


(defvar *Global-Mouse-X* 0)
(defvar *Global-Mouse-Y* 0)
(proclaim '(fixnum *Global-Mouse-X* *Global-Mouse-Y*))



(defvar *mouse-buttons*
	#+(and symbolics (not x)) 'tv:mouse-buttons
	#+X '*Local-Mouse-Buttons*)


(defvar *Mouse-Window* NIL)


;; We have duplicated values here from whatever the underlying window system is.
;; Be sure that if the underlying window manager changes something, that we know about it.

(defclass window ()
  ((name :accessor window-name :initarg :name :initform NIL)
    (real-window :accessor window-real-window :initarg :real-window :initform NIL)
    (superior :accessor window-superior :initarg :superior :initform NIL)
    (inferiors :accessor window-inferiors :initarg :inferiors :INITFORM NIL :TYPE list)
    ;; sizes of the margins for doing things like scroll bars in.
    ;; this size does not include the size of the X window Border - that must be taken
    ;; into account separately for anything that does window sizes.
    ;; note window-size will include it.
    (top-margin-size :accessor window-top-margin-size :initarg :top-margin-size :INITFORM 0 :type fixnum)
    (left-margin-size :accessor window-left-margin-size :initarg :left-margin-size :INITFORM 0 :type fixnum)
    (right-margin-size :accessor window-right-margin-size :initarg :right-margin-size :INITFORM 0 :type fixnum)
    (bottom-margin-size :accessor window-bottom-margin-size :initarg :bottom-margin-size :INITFORM 0 :type fixnum)
    ;; offsets for scrolling the virtual window.
    (scroll-x-offset :accessor window-scroll-x-offset :initarg :scroll-x-offset :INITFORM 0 :type fixnum)
    (scroll-y-offset :accessor window-scroll-y-offset :initarg :scroll-y-offset :INITFORM 0 :type fixnum)
  
    (transform :accessor window-transform :initarg :transform :INITFORM NIL)
    ;; things associated with character output.
    ;; the current style to use.
    (character-style :accessor window-character-style :initarg :character-style :INITFORM '(:FIX :ROMAN :NORMAL))
    (font :accessor window-font :initarg :font :INITFORM (xfonts::get-font-from-style '(:fix :roman :normal))) ;; cached font
    ;; the amount the y position is incremented in a terpri.
    (line-height :accessor window-line-height :initarg :line-height :INITFORM 16 :type fixnum)
    ;; the line-spacing plus the height of the font associated with the current style
    ;; is = to the line-height.
    (maximum-line-height :accessor window-maximum-line-height :initarg :maximum-line-height :INITFORM 16 :type fixnum)
    (line-spacing :accessor window-line-spacing :initarg :line-spacing :INITFORM 2 :type fixnum)
    ;; the current position of output for functions like print, princ, prin1, and format.
    ;; note this position includes margins. so 0,0 is at top left of full window
    ;; area, top of real drawing area is at left-margin-size,top-margin-size.
    ;; Also remember cursor position is top left of where text is drawn on symbolics.
    (x-pos :accessor window-x-pos :initarg :x-pos :INITFORM 0 :type fixnum)
    (y-pos :accessor window-y-pos :initarg :y-pos :INITFORM 0 :type fixnum)
    ;; The top left corner of the window. in terms of absolute positioning.  Note this should be
    ;; within the superior window to be compatible with Symbolics.
    (top :accessor window-top :initarg :top  :INITFORM 0 :TYPE fixnum)
    (left :accessor window-left :initarg :left :INITFORM 0 :TYPE fixnum)
    ;; the total size of the window. including and borders.
    (width :accessor window-width :initarg :width :INITFORM 800. :type fixnum)
    (height :accessor window-height :initarg :height :INITFORM 99999. :type fixnum)

    (input-editor :accessor window-input-editor :initarg :input-editor :INITFORM NIL)

    (flags :accessor window-flags :initarg :flags :INITFORM 0 :TYPE fixnum)

    (scroll-factor :accessor window-scroll-factor :initarg :scroll-factor :INITFORM NIL)
    (end-of-page-mode :accessor window-end-of-page-mode :initarg :end-of-page-mode :INITFORM :DEFAULT)
    (end-of-line-mode :accessor window-end-of-line-mode :initarg :end-of-line-mode :INITFORM :DEFAULT)
    (fill :accessor window-fill :initarg :fill :INITFORM NIL)
    (indentation :accessor window-indentation :initarg :indentation :INITFORM NIL)
    (more-p :accessor window-more-p :initarg :more-p :INITFORM T)

    #+X
    (gcontext :accessor window-gcontext :initarg :gcontext :INITFORM NIL)
    (cursor :accessor window-cursor :initarg :cursor :INITFORM NIL)

    ;; the width of the X window Border
    (border-margin-width :accessor window-border-margin-width :initarg :border-margin-width :INITFORM 1 :TYPE fixnum)
    )

;;  (:constructor make-interface-window)
;;  (:accessor-prefix window-)
  )


(defconstant *Quad-Table-Array-Size* 200.)
;;(proclaim '(fixnum *Quad-Table-Array-Size*))

(defstruct (quad-table (:TYPE LIST))
  (array (make-array *Quad-Table-Array-Size*) :type simple-vector)
  (table (make-hash-table)))

(defstruct (quad-node (:TYPE list))
  (last-cons NIL)
  (objects NIL :type list)
  (top 0 :type fixnum)
  (bottom 0 :type fixnum))


(Defclass presentation-window (window)
  ((presentations :accessor presentation-window-presentations :initarg :presentations :INITFORM NIL)
   (record-presentations-p :accessor presentation-window-record-presentations-p :initarg :record-presentations-p :INITFORM T
			   :TYPE (member T NIL))
   (quad-table :accessor presentation-window-quad-table :initarg :quad-table :INITFORM (make-quad-table))
   (max-x-position :accessor presentation-window-max-x-position :initarg :max-x-position :INITFORM 0 :TYPE FIXNUM)
   (max-y-position :accessor presentation-window-max-y-position :initarg :max-y-position :INITFORM 0 :TYPE FIXNUM)
   (margins :accessor presentation-window-margins :initarg :margins :INITFORM NIL)
   )
;  (:accessor-prefix presentation-window-)
  )

(defclass window-margin (window)
  ((type :accessor window-margin-type :initarg :type :INITFORM :LEFT :TYPE (member :left :right :top :bottom)))
;  (:accessor-prefix window-margin-)
  )

(defclass scroll-bar (window-margin)
  ((window-to-scroll :accessor scroll-bar-window-to-scroll :initarg :window-to-scroll :INITFORM NIL)
   (visibility :accessor scroll-bar-visibility :initarg :visibility :INITFORM :NORMAL
	       :TYPE (member :NORMAL :IF-REQUESTED :IF-NEEDED))
   (current-visibility :accessor scroll-bar-current-visibility :initarg :current-visibility :INITFORM :OFF
		       :TYPE (member :OFF :ON))
   (history-noun :accessor scroll-bar-history-noun :initarg :history-noun :INITFORM NIL)
   (bar-start :accessor scroll-bar-bar-start :initarg :bar-start  :INITFORM 0 :TYPE fixnum)
   (bar-length :accessor scroll-bar-bar-length :initarg :bar-length :INITFORM 0 :TYPE fixnum))
;  (:accessor-prefix scroll-bar-)
  )

(defclass label (window-margin)
  ((string :accessor label-string :initarg :string :INITFORM  "" :TYPE string)
   (labelled-window :accessor label-labelled-window :initarg :labelled-window :INITFORM NIL))
;  (:accessor-prefix label-)
  )

(defvar *Fake-Windows* NIL)
(defvar *Allocated-Windows* NIL)

(defclass fake-window (presentation-window)
  NIL)

#+(and (not EW-CLOS) (or LCL3.0 (and APOLLO DOMAIN/OS)))
(defstruct-simple-predicate fake-window fake-window-p)

#+(and (not EW-CLOS) (not (or LCL3.0 (and APOLLO DOMAIN/OS))))
(defmacro fake-window-p (stream)
  `(eq (lisp:type-of ,stream) 'FAKE-WINDOW))

#+EW-CLOS
(defmacro fake-window-p (stream)
  `(typep ,stream 'FAKE-WINDOW))

;; 6/14/90 (SLN) Remove explicit references to pcl
#+EW-CLOS
(defmacro make-fake-window (&REST args)
  `(make-instance 'FAKE-WINDOW . ,args))

;; 6/14/90 (SLN) Remove explicit references to pcl
#+EW-CLOS
(defmacro make-presentation-window (&REST args)
  `(make-instance 'PRESENTATION-WINDOW . ,args))

#+(and (not ew-clos) lucid)
(defmacro window-p (x)
  `(and (sys::structurep ,x)
	(member (sys:structure-type ,x)
		'(window presentation-window label
			 scroll-bar fake-window
			 program-pane
			 program-title-pane
			 program-display-pane program-command-menu program-frame))))

#+(and (not ew-clos) (not lucid))
(defmacro window-p (x)
  `(member (lisp::type-of ,x)
	   '(window presentation-window label
		    scroll-bar fake-window
		    program-pane
		    program-title-pane
		    program-display-pane program-command-menu program-frame)))

#+ew-clos
(defmacro window-p (x)
  `(lisp:typep ,x 'window))

#+(and (not ew-clos) lucid)
(defmacro presentation-window-p (x)
  `(and (sys::structurep ,x)
	(member (sys:structure-type ,x)
		'(presentation-window
		   fake-window
		   program-pane
		   program-title-pane
		   program-display-pane program-command-menu program-frame))))

#+(and (not ew-clos) (not lucid))
(defmacro presentation-window-p (x)
  `(member (lisp::type-of ,x)
	   '(presentation-window
	      fake-window
	      program-pane
	      program-title-pane
	      program-display-pane program-command-menu program-frame)))


#+ew-clos
(defmacro presentation-window-p (x)
  `(lisp:typep ,x 'presentation-window))


(defvar *Windows* NIL)

(defvar *Last-Found-Window* NIL)
(defvar *Last-Found-Window-Real-Window* NIL)

#+clx
(defvar *Unread-Char* NIL)
#+x
(progn
(defvar *Key-Control-State* 0)
(proclaim '(type (member 0 1) *Key-Control-State*))

(defvar *Key-Meta-State* 0)
(proclaim '(type (member 0 2) *Key-Meta-State*))

(defvar *Key-Super-State* 0)
(proclaim '(type (member 0 4) *Key-Super-State*))

(defvar *Key-Hyper-State* 0)
(proclaim '(type (member 0 8) *Key-Hyper-State*))

(defvar *Key-Shift-State* 0)
(proclaim '(type (member 0 16) *Key-Shift-State*))
)

#+X
(progn
(defvar *Local-Mouse-X* 0)
(proclaim '(fixnum *Local-Mouse-X*))

(defvar *Local-Mouse-Y* 0)
(proclaim '(fixnum *Local-Mouse-Y*))

(defvar *Local-Mouse-Buttons* 0)
(proclaim '(fixnum *Local-Mouse-Buttons*))
)

(defvar *Mouse-Documentation-String* NIL)


(defvar *Mouse-Documentation-Window* NIL)

(defvar *Default-End-Of-Page-Mode* :SCROLL)

(defvar *Recursive-Check-End-Of-Page-Mode* NIL)


#+X
(defvar *X-Display* NIL)
#+X
(defvar *Screen*)
(defvar *Root-Window*)
(defvar *Ultimate-Root-Window*)

(defvar *Chord-Shifts* 0)
(proclaim '(fixnum *Chord-Shifts*))

;; currently relying on read-internal to keep this up to date at all times.
(defmacro mouse-chord-shifts ()
  #+CLX
  '*Chord-Shifts*)

#+ignore
(defmacro mouse-chord-shifts ()
  #+(and symbolics (not x))
  `(tv:mouse-chord-shifts)
  #+CLX
  '(%+ *Key-Control-State* *Key-Meta-State*
       ;; *Key-Super-State* *Key-Hyper-State* currently don't have a super or hyper key to worry about.
       *Key-Shift-State*))

(defmacro window-inside-left (window &OPTIONAL (include-border-width-p T))
  (declare (ignore include-border-width-p))
  `(window-left-margin-size ,window))

(defmacro window-inside-top (window)
  `(window-top-margin-size ,window))

(defmacro window-inside-bottom (window)
  `(- (window-height ,window) (window-bottom-margin-size ,window)))

(defmacro window-inside-right (window)
  `(- (window-width ,window) (window-right-margin-size ,window)))


(defmacro window-inside-width (window)
  `(%- (window-width ,window) (%+ (window-left-margin-size ,window)
				  (window-right-margin-size ,window)
				  (window-border-margin-width ,window)
				  (window-border-margin-width ,window))))

(defmacro window-inside-height (window)
  `(%- (window-height ,window) (%+ (window-top-margin-size ,window)
				   (window-bottom-margin-size ,window)
				   (window-border-margin-width ,window)
				   (window-border-margin-width ,window))))


(defmacro mouse-char-bits (mouse-char)
  #+(and symbolics (not x))
  `(si:mouse-char-bits ,mouse-char)
  #+x
  `(the fixnum (ldb '#.(byte 5 3) (the fixnum ,mouse-char))))


(defmacro mouse-char-button (mouse-char)
  #+(and symbolics (not x)) `(si:mouse-char-button ,mouse-char)
  #+x `(the (values fixnum number)
	    (floor (the fixnum (ldb '#.(byte 3 0) (the fixnum ,mouse-char))) 2)))


(defvar *Consume-Extra-Mouse-Motion-Events-P* T)


(defmacro with-mouse-documentation-string ((documentation) &BODY body)
;  #+symbolics
;  (let ((old-documentation-string (gensym "OLD-DOCUMENTATION-STRING")))
;    `(let ((,old-documentation-string *Mouse-Documentation-String*))
;       (unwind-protect
;	   (progn
;	     (setq *Mouse-Documentation-String* ,documentation)
;	     . ,body)
;	 (setq *Mouse-Documentation-String* ,old-documentation-string))))
  #+X
  (let ((old-documentation-string (gensym "OLD-DOCUMENTATION-STRING")))
    `(let ((,old-documentation-string *Mouse-Documentation-String*))
       (unwind-protect
	   (progn
	     (when ,documentation (set-mouse-documentation-string ,documentation))
	     . ,body)
	 (when ,documentation
	   (set-mouse-documentation-string ,old-documentation-string))))))







(defvar 25%-gray)
(defvar mouse-vertical-double-arrow)
(defvar mouse-nw-arrow)
(defvar mouse-horizontal-double-arrow)


(defconstant *Scroll-Bar-Thickness* 16.)
;(proclaim '(fixnum *Scroll-Bar-Thickness*))

(defconstant *Scroll-Bar-Half-Thickness* 8.)
;(proclaim '(fixnum *Scroll-Bar-Half-Thickness*))

(defconstant *Scroll-Bar-Quarter-Thickness* 4.)
;(proclaim '(fixnum *Scroll-Bar-Quarter-Thickness*))

(defconstant *Scroll-Bar-Three-Quarters-Thickness* 12.)
;(proclaim '(fixnum *Scroll-Bar-Three-Quarters-Thickness*))


(defvar *Highlighted-Presentation* NIL)
(defvar *Highlighted-Presentation-Window* NIL)


(defmacro get-next-tab-position (x-pos font &OPTIONAL char-width)
  `(the fixnum
	,(if char-width
	     `(%* 8 ,char-width
		  (%1+ (the (values fixnum number)
			    (floor (the (values fixnum number)
					(floor (the fixnum ,x-pos)
					       (the fixnum ,char-width)))))
		       8))
	     `(let ((char-width (char-width-from-font #\Space ,font)))
		(declare (fixnum char-width))
		(the fixnum
		     (%* 8 char-width
			 (%1+ (the (values fixnum number)
				   (floor (the (values fixnum number)
					       (floor (the fixnum ,x-pos)
						      char-width))
					  8)))))))))


;; bind to T around code that doesn't need to have a scroll bar actively updating.
(defvar *Inhibit-Scroll-Bar-P* NIL)



(defmacro create-window-flag-macros (&REST flag-descriptions)
  (let ((bit-position 0))
    `(progn
       . ,(mapcar #'(lambda (flag)
		      (let ((flag-name (first flag))
			    (flag-width (second flag)))
			(prog1
			  `(defmacro ,(intern (concatenate 'string "WINDOW-" (symbol-name flag-name)))
				     (window)
			     (list 'LDB '',(byte flag-width bit-position)
				   (list 'WINDOW-FLAGS window)))
			  (incf bit-position flag-width))))
		  flag-descriptions))))

(create-window-flag-macros
  (more-flag 1)
  (end-of-page-flag 1)
  (end-of-line-flag 1))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
