;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)



;; returns a list of presentations that represent the output.
(defmacro with-output-to-fake-window ((&OPTIONAL (stream '*Standard-Output*)
						 stream-to-copy (set-cursorpos-p T)
						 (use-full-screen-size-p NIL))
				      &BODY body)
  (declare (values presentations final-x final-y))
  `(with-output-to-fake-window-internal #'(lambda (,stream) . ,body)
					,stream-to-copy ,set-cursorpos-p
					,use-full-screen-size-p))

(defun with-output-to-fake-window-internal (body stream-to-copy set-cursorpos-p
					    use-full-screen-size-p)
  (let ((fake-window (allocate-fake-window stream-to-copy
					     set-cursorpos-p use-full-screen-size-p))
	(presentations NIL)
	final-x final-y)
    (unwind-protect
	(progn
	  ;; make it look top-level
	  (let ((*Presentation-Records* NIL)
		(*Presentation-Records-Last-Cons* NIL)
		(*Inside-Record-Presentations-P* T)
		(*Immediately-Inside-Woap* NIL))
	    (funcall body fake-window)
	    (setq presentations *Presentation-Records*))
	  (multiple-value-setq (final-x final-y) (read-cursorpos fake-window)))
      (deallocate-fake-window fake-window))
    (values presentations final-x final-y)))


(defmacro with-centered-display ((&OPTIONAL (stream '*Standard-Output*)
					    &KEY (center-x-p T) (center-y-p NIL)
					    width height)
				 &BODY body)
  `(with-centered-display-internal
     ,stream ,width ,height
     ,center-x-p ,center-y-p
     #'(lambda (,stream) . ,body)))

(defun with-centered-display-internal (stream width height center-x-p center-y-p body)
  (multiple-value-bind (x y)
      (read-cursorpos stream)
    (multiple-value-bind (presentations final-x final-y)
	(with-output-to-fake-window (stream stream)
	  (funcall body stream))
      (multiple-value-bind (left top right bottom)
	  (get-presentations-boundaries presentations)
	;; center that thing.
	(multiple-value-bind (stream-width stream-height)
	    (window-size stream)
	  (unless width (setq width stream-width))
	  (unless height (setq height stream-height)))
	(insert-presentations-with-offset
	  stream presentations
	  (if center-x-p (the (values fixnum number) (floor (%- width (%- right left)) 2)) x)
	  (if center-y-p (the (values fixnum number) (floor (%- height (%- bottom top)) 2)) y)
	  T))
      (set-cursorpos stream final-x final-y))))



(defun draw-oval (center-x center-y x-radius y-radius &KEY (stream *Standard-Output*)
		  (filled T) (thickness 1))
  ;; draw lines for size and connect with semi circles etc.
  (if (< y-radius x-radius)
      ;; horizontal radius
      (let ((delta (- x-radius y-radius)))
	(if filled
	    (draw-rectangle (- center-x delta) (- center-y y-radius)
			    (+ center-x delta) (+ center-y y-radius)
			    :stream stream :filled filled :THICKNESS thickness)
	    (progn
	      (draw-line (- center-x delta) (+ center-y y-radius)
			 (+ center-x delta) (+ center-y y-radius) :stream stream
			 :THICKNESS thickness)
	      (draw-line (- center-x delta) (- center-y y-radius)
			 (+ center-x delta) (- center-y y-radius) :stream stream
			 :THICKNESS thickness)))
	(draw-circle (- center-x delta) center-y y-radius :stream stream :filled filled
		     :START-ANGLE *90-degrees* :end-angle *270-degrees*
		     :THICKNESS thickness)
	(draw-circle (+ center-x delta) center-y y-radius :stream stream :filled filled
		     :END-ANGLE *90-degrees*
		     :START-ANGLE *270-degrees*
		     :THICKNESS thickness))
      (let ((delta (- y-radius x-radius)))
	(if filled
	    (draw-rectangle (- center-x x-radius) (- center-y delta)
			    (+ center-x x-radius) (+ center-y delta)
			    :stream stream :filled filled :THICKNESS thickness)
	    (progn
	      (draw-line (+ center-x x-radius) (- center-y delta)
			 (+ center-x x-radius) (+ center-y delta) :stream stream
			 :THICKNESS thickness)
	      (draw-line (- center-x x-radius) (- center-y delta)
			 (- center-x x-radius) (+ center-y delta) :stream stream
			 :THICKNESS thickness)))
	(draw-circle center-x (- center-y delta) x-radius :stream stream :filled filled
		     :end-ANGLE 0 :start-angle *180-degrees* :THICKNESS thickness)
	(draw-circle center-x (+ center-y delta) x-radius :stream stream :filled filled
		     :start-ANGLE 0
		     :end-ANGLE *180-degrees* :THICKNESS thickness))))

;;#+ignore
;(defun with-border-internal (stream filled alu thickness shape move-cursor body)
;  (multiple-value-bind (presentations final-x final-y)
;      (with-output-to-fake-window (stream stream)
;	(funcall body stream))
;    (multiple-value-bind (left top right bottom)
;	(get-presentations-boundaries presentations)
;      ;;;mjk - the magic numbers 2, 3, 6 should be vars descriptive of their function.
;      ;;;This may help to explain why we incf left by 3 and then immediately subtract 2 from it
;      (insert-presentations-with-offset stream presentations
;					(case shape
;					  (:OVAL 2)
;					  (:RECTANGLE 2))
;					(case shape
;					  (:OVAL 2)
;					  (:RECTANGLE 2))
;					T)
;      ;;;mjk - when :filled t, the filling obliterates the output.  Symbolics doesn't.
;      (memoize (:stream stream)
;	(case shape
;	  (:OVAL
;	    (draw-oval (the fixnum (floor (%+ left right) 2))
;		       (the fixnum (floor (%+ top bottom) 2))
;		       (ceiling (%+ (%- bottom top) (%- right left)) 2)
;		       (ceiling (%+ 4 (%- bottom top)) 2)
;		       :FILLED filled :THICKNESS thickness
;		       :STREAM stream))
;	  (T (draw-rectangle (%- left 2) (%- top 2)
;			     (%+ right 2) (%+ bottom 2)
;			     :THICKNESS thickness :FILLED filled
;			     :ALU alu :STREAM stream))))
;      (if move-cursor
;	  (terpri stream)
;	  (set-cursorpos stream final-x (%+ final-y 4))))))


(defun with-border-internal (stream filled alu thickness shape move-cursor body)
  (let ((presentation
	  (display-as (:STREAM stream :TYPE 'ignore-presentation)
	    (funcall body stream))))
    (let ((left (boxed-presentation-left presentation))
	  (top (boxed-presentation-top presentation))
	  (right (boxed-presentation-right presentation))
	  (bottom (boxed-presentation-bottom presentation)))
      ;;;mjk - when :filled t, the filling obliterates the output.  Symbolics doesn't.
      (memoize (:stream stream)
	(case shape
	  (:OVAL
	    (draw-oval (the (values fixnum number) (floor (%+ left right) 2))
		       (the (values fixnum number) (floor (%+ top bottom) 2))
		       (ceiling (%+ (%- bottom top) (%- right left)) 2)
		       (ceiling (%+ 4 (%- bottom top)) 2)
		       :FILLED filled :THICKNESS thickness
		       :STREAM stream))
	  (T (draw-rectangle (%- left 2) (%- top 2)
			     (%+ right 2) (%+ bottom 2)
			     :THICKNESS thickness :FILLED filled
			     :ALU alu :STREAM stream))))
      (if move-cursor
	  (terpri stream)))))

(defmacro with-room-for-graphics ((&optional STREAM Height
					     &key (fresh-line t) (move-cursor t))
				  &body body)
  (setup-standard-output-stream-arg stream)
  (when (eq stream t)(setq stream '*standard-output*))
  `(with-room-for-graphics-internal ,STREAM ,height ,fresh-line ,move-cursor
				    #'(lambda (,stream) . ,body)))

(defun with-room-for-graphics-internal (stream height fresh-line move-cursor
					continuation)
  (when fresh-line (fresh-line stream))
  (multiple-value-bind (x y)
      (read-cursorpos stream)
    (declare (ignore x))
    (let ((presentations
	    (with-output-to-fake-window (stream)
	      (funcall continuation stream))))
      (multiple-value-bind (left top right bottom)
	  (get-presentations-boundaries presentations)
	(declare (ignore right left))
	(when (not height) (setq height (%- bottom top)))
	(insert-presentations-for-room-for-graphics
	  stream presentations bottom))
      (when move-cursor
	(set-cursorpos stream 0
		       (%+ y
			   (%* (window-line-height stream)
			       (values
				 (ceiling height (window-line-height stream))))))))))

(defmacro with-underlining ((&OPTIONAL stream &KEY (underline-whitespace T)) &BODY body)
  (declare (ignore underline-whitespace stream))
  `(progn . ,body))


(defun my-string (object)
  (if (presentation-recording-string-p object)
      (presentation-recording-string-string object)
      (string object)))


(defun make-pretty-name (name &OPTIONAL (replace-hypens T))
  (declare (type (or string symbol) name))
  (when (symbolp name) (setq name (symbol-name (the symbol name))))
  (when (and (%> (length (the string name)) 3)
	     (string-equal name "COM-" :END1 4 :END2 4))
    (setq name (subseq (the string name) 4)))
  (when replace-hypens (substitute #\Space #\- (string-capitalize name))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
