;;; -*- Mode: LISP; Syntax: Common-lisp; Package: CL-USER; Base: 10 -*-

;;; ------------------------------
;;;	IN-PACKAGE
;;; ------------------------------

(cl:in-package :cl-user)

(eval-when (eval load compile)
  (defvar *EW-Common-Lisp-Symbols-To-Shadow*
	  (append
	    '(;;lisp:clear-input ;; this isn't defined anywhere so comment it out (SLN) 4/30/90
	      lisp:format
	      lisp:fresh-line
	      lisp:listen
	      lisp:peek-char
	      lisp:pi
	      lisp:prin1 lisp:princ lisp:print
	      lisp:read lisp:read-char lisp:read-char-no-hang
	      lisp:read-line lisp:read-preserving-whitespace
	      lisp:terpri
	      lisp:unread-char
	      lisp:write lisp:write-char lisp:write-line lisp:write-string
	      lisp:y-or-n-p lisp:yes-or-no-p
	      lisp:streamp
	      lisp:input-stream-p lisp:output-stream-p
	      lisp:stream-element-type
	      lisp:close
	      lisp:force-output
	      lisp:typep)
	    (when (and #+symbolics (= 7 (sct:get-release-version))) '(lisp:type-of)))))

;;; ------------------------------
;;;	DEFPACKAGE
;;; ------------------------------

(defpackage:defpackage XFONTS
  (:use cl)
  (:export 9x15 get-font-from-style merge-character-styles))

#+PCL
(defpackage:defpackage FPCL
  (:use cl))

(defpackage:defpackage EW
  (:use cl)
  (:nicknames Express-Windows)
  (:shadow . #.*EW-Common-Lisp-Symbols-To-Shadow*)
  #+lucid
  (:import-from lucid DEFSTRUCT-SIMPLE-PREDICATE  ;; We need this "internal", undocumented Lucid goody
		;;  We import the following symbols, because in 2.1
		;;  Lucid Lisps they have to be accessed as
		;;  LUCID::<foo>, whereas in 3.0 lisps, they have to be
		;;  accessed as SYS:<foo>
		NEW-STRUCTURE STRUCTURE-REF PROCEDUREP PROCEDURE-SYMBOL PROCEDURE-ARGLIST PROCEDURE-REF SET-PROCEDURE-REF)
  (:import-from #+symbolics scl #+lucid lcl #+excl excl arglist)
  (:import-from ew-clos with-slots defclass defmethod make-instance print-object find-class standard-class describe-object)
  (:export
    ;; symbols that replace common lisp functions.
    clear-input format fresh-line listen peek-char prin1 princ print read
    read-char read-char-no-hang read-line read-preserving-whitespace
    terpri unread-char write write-char write-line write-string y-or-n-p
    yes-or-no-p 

    ;; io functions
    peek-any-char prompt-and-read read-any-char read-any-char-no-hang
    unread-any-char with-input-editing with-input-editing-options

    pi *90-degrees* *180-degrees* *270-degrees* *360-degrees*

    ;; symbols having to do with command processor and commands.
    build-command command command-in-command-table-p *Command-Table*
    *Default-Blank-Line-Mode* define-command delete-command-table
    execute-command find-command-table *Full-Command-Prompt*
    make-command-table undefine-command 

    ;; basic symbols
    query query-from-string query-values query-values-choose-from-sequence
    query-values-command-button memo-write-string query-values-into-list
    querying-values compare-char-for-query describe-type
    define-mouse-action define-mouse-command define-type-transform define-type 
    display-as erase-displayed-presentation menu-choose menu-choose-from-set

    peek-char-for-query parse-error display presentation-blip-case
	  
    presentation-type-name prompt-and-query read-char-for-query
    read-location read-standard-token standard-query-values-displayer
    unread-char-for-query 

    replace-input-editor-string

    with-output-recording-disabled
    with-output-to-presentation-recording-string
    with-presentations-disabled with-presentations-enabled
    with-presentation-input-context with-type-decoded 

    *Activation-Chars* *Token-Delimiter-Chars* with-activation-chars
    with-token-delimiters 

    ;; completion stuff
    complete-from-sequence complete-input completing-from-suggestions
    default-complete-function *Standard-Completion-Delimiters* suggest 

    ;; presentation type symbols
    alist-member alist-subset boolean

    ;; buffer what is a lucid buffer
    class

    ;; character-face-or-style
    character-family character-style expression font form host

    ;; integer
    inverted-boolean keyword member-sequence no-type
    ;; number
    ;; null-or-type
    ;; sequence
    sequence-enumerated subset time-interval time-interval-60ths
    token-or-type universal-time 

    ;; presentation semi-internal stuff.
    call-presentation-menu get-presentations-boundaries
    highlight-presentation insert-presentations-with-offset
    insert-presentations 

    ;; incremental redisplay
    run-memo independently-redisplayable-format program-redisplay
    memo-format memo-display memo memoize 

    ;; framework stuff.
    clear-all command-error configuration command-loop-eval-function
    command-loop-print-function default-command-top-level
    define-command-menu-handler define-program-command
    define-program-framework *Dispatch-Mode* exit-program
    find-program-window get-pane get-program-pane margin-borders
    margin-label margin-scroll-bar margin-white-borders margin-whitespace
    margin-ragged-borders *Program* *Program-Frame* program-frame
    program-name read-accelerated-command read-program-command run-program
    standard-command-menu-handler set-configuration
    set-program-frame-configuration 

    ;; graphic transformation symbols
    build-graphics-transform compose-transforms make-graphics-transform
    make-identity-transform transform-distance transform-point
    with-rotation with-scaling with-translation with-transform
    with-identity-transform 

    ;; fancy display macros.
    make-table-from-sequence make-table-from-generated-sequence

    make-table table-row row table-entry entry table-column-headings

    format-list formatting-list formatting-list-element
    format-sequence-as-table-rows 

    with-indenting-output with-border with-centered-display
    with-room-for-graphics with-output-truncation with-output-filling
    with-underlining with-output-abbreviating 

    ;; basic graphic primitives.
    bitblt defstipple draw-arrow draw-circle draw-ellipse draw-line
    draw-line-to draw-glyph draw-point draw-polygon draw-rectangle
    draw-regular-polygon draw-string draw-triangle drawing-path 

    *Default-Arrow-Width* *Default-Arrow-Length*

    ;; alu operations
    draw-alu *Draw-Alu* *Erase-Alu* *Flip-Alu* with-proper-alu

    ;; basic window operations
    clear-history clear-window
	  
    set-viewport-position set-viewport-position-after
    visible-cursorpos-limits x-scroll-to x-scroll-to-after
    x-scroll-position y-scroll-position y-scroll-to y-scroll-to-after 

    ;; mouse variables.
    *Global-Mouse-X* *Global-Mouse-Y* *Mouse-Buttons* *Mouse-Window*
    *Mouse-X* *Mouse-Y* mouse-char-bits mouse-char-button mouse-chord-shifts tracking-mouse

    mouse-char-for-gesture mouse-char-gesture mouse-char-gestures

    ;; character style stuff
    char-size char-size-from-font font-ascent font-descent font-height
    font-width get-font-from-style merge-character-styles
    with-character-face with-character-size with-character-style 

    ;; window attributes
    window-inside-bottom window-inside-left window-inside-right
    window-inside-top window-bottom-margin-size window-character-style
    window-edges window-end-of-line-mode window-end-of-page-mode
    window-font window-font-ascent window-gcontext window-height
    window-inferiors window-inside-edges window-inside-height
    window-inside-size window-inside-width window-left
    window-left-margin-size window-line-height window-line-spacing
    window-more-p window-name window-real-window window-right-margin-size
    window-size window-scroll-x-offset window-scroll-y-offset
    window-superior window-top window-top-margin-size window-transform
    window-width window-x-pos window-y-pos 

    center-window-around kill-window set-window-edges
    set-window-inside-size set-window-label set-window-line-spacing
    set-window-position set-window-size set-window-style 

    dynamic-window make-window window dynamic-window-pane
    window-call-relative 

    home-cursor increment-cursorpos read-cursorpos set-cursorpos
    set-cursorpos-and-size-from-char tab-cursorpos 

    deexpose-window expose-window expose-window-near exposed-window-p
    select-window

    make-presentation-window presentation-window
    presentation-window-presentations presentation-window-quad-table 

    allocate-fake-window deallocate-fake-window fake-window
    make-fake-window 

    compute-motion text-position textlength
    compute-cursor-position-in-string clear-rest-of-line clear-rest-of-window 

    parse-interval-or-never parse-universal-time print-interval-or-never
    read-interval-or-never 

    beep *Default-Sync* describe-object enable-cursor find-window
    initialize-window-system lisp-window mouse-warp prepare-window
    process-wait send sync which-operations 

    *Mouse-Documentation-String* set-mouse-documentation-string
    with-mouse-documentation-string *Mouse-Window* 


    turn-off-blinkers set-blinker-status

    convert-global-mouse-to-local-mouse
    convert-window-coords-to-screen-coords
    convert-screen-coords-to-window-coords
    convert-window-coords-to-screen-coords-macro 

    type-of typep self

    screen-edges indent-terpri with-safe-windows

    alphalessp string-length with-lock-held
    ))

#-symbolics
(progn
  (defpackage:defpackage dw
    (:use ew)
    (:export menu-choose menu-choose-from-set

	     ;; formatting table stuff.
	     formatting-cell
	     formatting-column-headings
	     formatting-row
	     formatting-table

	     present
	     ;; incremental redisplay
	     do-redisplay
	     independently-redisplayable-format
	     program-redisplay
	     redisplayable-format
	     redisplayable-present
	     redisplayer
	     with-redisplayable-output
	     with-output-as-presentation
	     format-item-list
	     formatting-item-list

	     format-textual-list
	     formatting-textual-list
	     formatting-textual-list-element

	     describe-presentation-type

	     accept
	     accept-from-string
	     accept-values
	     accept-values-choose-from-sequence
	     accept-values-command-button
	     accept-values-fixed-line memo-write-string
	     accept-values-into-list
	     accepting-values
	     compare-char-for-accept
	     peek-char-for-accept
	     prompt-and-accept
	     read-char-for-accept
	     standard-accept-values-displayer
	     unread-char-for-accept

	     with-accept-activation-chars
	     with-accept-blip-chars

	     filling-output
	     abbreviating-output
	     surrounding-output-with-border

	     define-presentation-action
	     define-presentation-to-command-translator
	     define-presentation-translator
	     define-presentation-type

	     presentation-type-name

	     presentation-replace-input

	     format-output-macro-default-stream
	     named-value-snapshot-continuation
	     presentation-blip-case
	  
	     presentation-type-name
	     read-standard-token

	     with-output-recording-disabled
	     with-output-to-presentation-recording-string
	     with-presentation-input-context
	     with-type-decoded

	     ;; completion stuff
	     complete-from-sequence
	     complete-input
	     completing-from-suggestions
	     suggest
	     tracking-mouse

	     ;; presentation semi-internal stuff.
	     call-presentation-menu

	     ;; framework stuff.
	     default-command-top-level
	     define-command-menu-handler
	     define-program-command
	     define-program-framework

	     find-program-window
	     get-program-pane
	     margin-borders
	     margin-label
	     margin-scroll-bar
	     margin-white-borders
	     margin-whitespace
	     margin-ragged-borders
	     *Program*
	     *Program-Frame*

	     standard-command-menu-handler
	     set-program-frame-configuration

	     inverted-boolean
	     keyword
	     member-sequence
	     no-type
	     sequence-enumerated
	     subset
	     token-or-type
	     alist-member
	     alist-subset))
  (defpackage:defpackage graphics
    (:use ew)
    (:export with-graphics-rotation with-graphics-scale
	     with-graphics-translation
	     with-graphics-transform
	     with-graphics-identity-transform
	     draw-arrow
	     draw-circle
	     draw-ellipse
	     draw-line
	     draw-line-to
	     draw-glyph
	     draw-point
	     draw-polygon
	     draw-rectangle
	     draw-regular-polygon
	     draw-string
	     draw-triangle
	     drawing-path
	     ;; graphic transformation symbols
	     build-graphics-transform
	     compose-transforms
	     make-graphics-transform
	     make-identity-transform
	     transform-distance
	     transform-point
	     with-rotation
	     with-scaling
	     with-translation
	     with-transform
	     with-identity-transform
	     defstipple))
  (defpackage:defpackage tv
    (:use ew)
    (:export sheet-inside-left sheet-inside-top s
	     heet-inside-bottom
	     sheet-inside-right
	     prepare-sheet))
  #-excl
  (defpackage:defpackage si
    (:use ew)
    (:export prompt-and-read merge-character-styles))
  (defpackage:defpackage scl
    (:use ew)
    (:export with-input-editing with-input-editing-options
	     beep
	     with-character-face
	     with-character-size
	     with-character-style
	     bitblt
	     ;; character-face-or-style
	     character-family
	     character-style))
  (defpackage:defpackage cp
    (:use ew)
    (:export build-command command
	     command-in-command-table-p
	     *Command-Table*
	     *Default-Blank-Line-Mode*
	     define-command
	     delete-command-table
	     execute-command
	     find-command-table
	     *Full-Command-Prompt*
	     make-command-table
	     undefine-command
	     *Dispatch-Mode*
	     read-accelerated-command
	     read-program-command))
  (defpackage:defpackage time
    (:use ew)
    (:export parse-interval-or-never
	   parse-universal-time
	   print-interval-or-never
	   read-interval-or-never
	   time-interval
	   time-interval-60ths
	   universal-time)))

;; 4/27/90 (SLN) Let's get rid of those annoying warnings about
;; "Variable: IGNORE bound but not referenced."

(proclaim (list 'special (intern "IGNORE" :ew)))

(defpackage:defpackage File-Manager
  (:use lisp)
  (:shadowing-import-from ew . #.*EW-Common-Lisp-Symbols-To-Shadow*)
  (:import-from ew-clos defmethod make-instance with-slots)
  (:import-from ew self query
		display boolean
		alist-member querying-values
		define-type
		define-mouse-command
		querying-values))

#-symbolics
(defpackage:defpackage Process-Manager
  (:use lisp)
  (:shadowing-import-from ew . #.*EW-Common-Lisp-Symbols-To-Shadow*)
  (:import-from ew-clos defmethod make-instance with-slots)
  (:import-from ew self query
		display boolean
		alist-member querying-values
		define-type define-mouse-command
		memoize entry table-row make-table
		table-column-headings
		with-character-face display-as)
  #+lucid (:import-from lcl process-name process-whostate)
  #+excl (:import-from mp process-name process-whostate))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
