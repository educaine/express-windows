;;; -*- Mode: LISP; Syntax: Common-Lisp; Base: 10; Package: Express-windows -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)


(defvar *Absolute-Table-X-Offset* 0)
(defvar *Absolute-Table-Y-Offset* 0)


;;; Definitions for the following functions.

; make-table
; table-row
; entry

; memo
; run-memo



(defun entry-internal (stream align-x align-y continuation
		       &OPTIONAL (value 'NO-CACHE-VALUE)
		       id (value-test #'EQL)
		       copy-value (id-test #'EQL))
  (let () ;(width height)
    (home-cursor stream)	      
    (let ((memo (memoize (:stream stream :memo-type :cell
				  :value value
				  :id id
				  :value-test value-test
				  :copy-value copy-value
				  :id-test id-test)
		  (funcall continuation stream))))
      #+ignore
      (let ((presentation (memo-presentation memo)))
	(setf width (%- (boxed-presentation-right presentation)
			(boxed-presentation-left presentation))
	      height (%- (boxed-presentation-bottom presentation)
			 (boxed-presentation-top presentation))))
      (setf (cell-memo-align-x memo) align-x
	    (cell-memo-align-y memo) align-y))))




(defvar *Displaying-Table-First-Time-P* NIL)

(defun make-table-internal (stream equalize-column-widths inter-row-spacing
				  extend-width extend-height inter-column-spacing
				  multiple-columns multiple-column-inter-column-spacing
				  unique-id
				  continuation
				  &AUX
				  ;;5/1/90 (sln) since we aren't using
				  ;;these, out with 'em (see note below)
;				  old-total-width old-total-height old-row-heights
;				  old-column-widths old-row-cell-data
				  new-total-width new-total-height new-row-heights
				  new-column-widths new-row-cell-data
				  )
  (flet ((internal (stream)
	   (multiple-value-bind (start-x start-y)
	       (read-cursorpos stream)
	     (unless inter-column-spacing
	       (setq inter-column-spacing 16))
	     (unless multiple-column-inter-column-spacing
	       (setq multiple-column-inter-column-spacing inter-column-spacing))
	     (multiple-value-bind (width height) (window-size stream)
	       ;; first make the low level display on the fake window.
	       (let ((old-table-memo
		       (find-matching-memo unique-id #'eql :TABLE NIL)))
;		 (when old-table-memo
;		   ;; we have previously displayed this table.
;		   ;; find its previous cell positions.
;		   (multiple-value-setq (old-total-width old-total-height old-row-heights
;							 old-column-widths old-row-cell-data)
;		     (calculate-table-sizes
;		       old-table-memo
;		       equalize-column-widths multiple-columns
;		       multiple-column-inter-column-spacing
;		       extend-width extend-height width height inter-column-spacing)))
		 ;; 5/1/90 (sln) See that code commented out above.
		 ;; Pretty funky, huh!?  It setqs those five variables,
		 ;; but doesn't use 'em.  Well, I'm sick of the
		 ;; compilation warnings so I'm removing the call to
		 ;; multiple-value-setq.  On the offchance that
		 ;; calculate-table-sizes has side-effects, I'll leave
		 ;; it in.  I suppose I could look it up, huh?  Oh well,
		 ;; next time I'm in the code.
		 (when old-table-memo
		   (calculate-table-sizes
		     old-table-memo
		     equalize-column-widths multiple-columns
		     multiple-column-inter-column-spacing
		     extend-width extend-height width height inter-column-spacing))
		 (let* ((new-memo NIL)
			(presentations
			  (with-output-to-fake-window (stream stream)
			    (setq new-memo
				  (memoize
				    (:MEMO-TYPE :TABLE :ID unique-id :STREAM stream)
				    (funcall continuation stream)))
			    ;;new-memo
			    ;;(first (memo-inferiors *Memo*))
			    )))
		   ;;  we can part ways here maybe, if this is the first time
		   ;; do it easily
		   (multiple-value-setq (new-total-width new-total-height new-row-heights
							 new-column-widths new-row-cell-data)
		     (calculate-table-sizes
		       new-memo
		       equalize-column-widths multiple-columns
		       multiple-column-inter-column-spacing
		       extend-width extend-height width height inter-column-spacing))
		   (if (not old-table-memo)
		       (let ((*Displaying-Table-First-Time-P* T))
			 ;; since this is the first time we have displayed this thing
			 ;; we can just fix up the presentations below and then insert them
			 ;; into the table
			 (fix-up-presentations stream new-memo
					       new-row-heights new-column-widths
					       new-row-cell-data
					       start-x start-y
					       inter-row-spacing)
			 (insert-presentations stream presentations T)
			 )
		       (let ((*Absolute-Table-X-Offset*
			       (table-memo-x-offset new-memo))
			     (*Absolute-Table-Y-Offset*
			       (table-memo-y-offset new-memo)))
			 #+ignore
			 (format tv:selected-window "~&Absolute X ~D Y ~D"
				 *Absolute-Table-X-Offset* *Absolute-Table-Y-Offset*)
			 (fix-up-presentations stream new-memo
					       new-row-heights new-column-widths
					       new-row-cell-data
					       start-x start-y
					       inter-row-spacing)
			 (insert-presentations stream presentations T))
		       )
		   (set-cursorpos stream
				    (%+ start-x new-total-width)
				    (%+ start-y new-total-height
					(window-line-spacing stream)))))))))
    (if (not *Memo*)
	(run-memo (memo (stream)
		    (internal stream)) stream)
	(internal stream)))
  NIL)


#+failed-attempt-for-speed
(defun make-table-internal (stream equalize-column-widths inter-row-spacing
				  extend-width extend-height inter-column-spacing
				  multiple-columns multiple-column-inter-column-spacing
				  unique-id
				  continuation
				  &AUX
				  old-total-width old-total-height old-row-heights
				  old-column-widths
				  new-total-width new-total-height new-row-heights
				  new-column-widths)
  (flet ((internal (stream)
	   (multiple-value-bind (start-x start-y)
	       (read-cursorpos stream)
	     (unless inter-column-spacing
	       (setq inter-column-spacing 16))
	     (unless multiple-column-inter-column-spacing
	       (setq multiple-column-inter-column-spacing inter-column-spacing))
	     (multiple-value-bind (width height) (window-size stream)
	       ;; first make the low level display on the fake window.
	       (let ((old-table-memo
		       (find-matching-memo unique-id #'eql :TABLE NIL)))
		 (when old-table-memo
		   ;; we have previously displayed this table.
		   ;; find its previous cell positions.
		   (multiple-value-setq (old-total-width old-total-height old-row-heights
							 old-column-widths)
		     (calculate-table-sizes
		       old-table-memo
		       equalize-column-widths multiple-columns
		       multiple-column-inter-column-spacing
		       extend-width extend-height width height inter-column-spacing)))
		 (let* ((new-memo NIL)
			(presentations
			  (with-output-to-fake-window (stream stream)
			    (setq new-memo
				  (memoize
				    (:MEMO-TYPE :TABLE :ID unique-id :STREAM stream)
				    (funcall continuation stream)))
			    ;;new-memo
			    ;;(first (memo-inferiors *Memo*))
			    )))
		   ;;  we can part ways here maybe, if this is the first time
		   ;; do it easily
		   (multiple-value-setq (new-total-width new-total-height new-row-heights
							 new-column-widths)
		     (calculate-table-sizes
		       new-memo
		       equalize-column-widths multiple-columns
		       multiple-column-inter-column-spacing
		       extend-width extend-height width height inter-column-spacing))
		   (if (not old-table-memo)
		       (let ((*Displaying-Table-First-Time-P* T))
			 ;; since this is the first time we have displayed this thing
			 ;; we can just fix up the presentations below and then insert them
			 ;; into the table
			 (fix-up-presentations stream new-memo
					       new-row-heights new-column-widths
					       start-x start-y
					       inter-row-spacing)
			 (insert-presentations stream presentations T)
			 )
		       (let ((*Absolute-Table-X-Offset*
			       (table-memo-x-offset new-memo))
			     (*Absolute-Table-Y-Offset*
			       (table-memo-y-offset new-memo)))
			 #+ignore
			 (format tv:selected-window "~&Absolute X ~D Y ~D"
				 *Absolute-Table-X-Offset* *Absolute-Table-Y-Offset*)
			 (fix-up-presentations stream new-memo
					       new-row-heights new-column-widths
					       start-x start-y
					       inter-row-spacing)
			 (insert-presentations stream presentations T))
		       )
		   (set-cursorpos stream
				    (%+ start-x new-total-width)
				    (%+ start-y new-total-height))))))))
    (if (not *Memo*)
	(run-memo (memo (stream)
		    (internal stream)) stream)
	(internal stream)))
  NIL)

#+ignore
(defun find-table-cell-data (table-memo)
  #.(fast)
  ;; do a depth first search for all rows and columns
  (let ((table-row-memos (find-table-rows table-memo)))
    (let ((row-cell-data
	    (mapcar #'(lambda (row-memo)
			(cons row-memo (find-table-cells row-memo)))
		    table-row-memos)))
      row-cell-data)))

#+ignore
(defun find-table-rows (inferior)
  #.(fast)
  (if (row-memo-p inferior)
      (list inferior)
      (mapcan #'(lambda (inferior)
		      (find-table-rows inferior))
	      (memo-inferiors inferior))))


;; replaces the above two functions by combining them, should be faster as well.
;; reduces consing the intermediate list of table-row-memos.
;; Also open coded one call of find-table-cells because we could remove a type check
;; since we already know that the inferior is a ROW-MEMO not a CELL-MEMO.
(defun find-table-cell-data (inferior)
  #.(fast)
  (if (row-memo-p inferior)
      (list (cons inferior (mapcan #'(lambda (inferior)
				       (find-table-cells inferior))
				   (memo-inferiors inferior))))
      (mapcan #'(lambda (inferior)
		  (find-table-cell-data inferior))
	      (memo-inferiors inferior))))

(defun find-table-cells (inferior)
  #.(fast)
  (if (cell-memo-p inferior)
      (list inferior)
      (mapcan #'(lambda (inferior)
		  (find-table-cells inferior))
	      (memo-inferiors inferior))))


;;; This takes the presentations as displayed within each cell and adds offsets
;;; so that they are properly placed within the table.  NOTE: what do we do
;;; when redisplaying?  Some of the presentations have already been given offsets.
;;; We don't want to offset them twice.  ALSO, what if their new offset were different
;;; then their old?

(defun fix-up-presentations (stream memo row-heights
			     column-widths row-cell-data start-x start-y
			     inter-row-spacing)
  ;; go through the data in row-cell-data and clean up the presentation's coordinates.
  (let ((y-offset start-y))
    ;; loop through each row.
    (do* ((rows row-cell-data (cdr rows))
	  (row-info (first rows) (first rows))
	  (sub-row-heights row-heights (cdr sub-row-heights))
	  (row-height (first sub-row-heights) (first sub-row-heights)))
	 ((null rows))
      (let* ((row-memo (first row-info))
	     (*Original-Y-Offset* (row-memo-y-offset row-memo))
	     (x-offset start-x))
	(declare (special *Original-Y-Offset*))
	;; loop through each cell in a particular row.
	(do* ((cells (cdr row-info) (cdr cells))
	      (cell-memo (first cells) (first cells))
	      (sub-column-widths column-widths (cdr sub-column-widths))
	      (column-width (first sub-column-widths) (first sub-column-widths)))
	     ((null cells))
	  ;; tell the cell it has been offset by x-offset and y-offset.
	  (let ((final-x-offset
		  (if (not (cell-memo-align-x cell-memo))
		      x-offset
		      (+ x-offset
			 (ecase (cell-memo-align-x cell-memo)
			   (:LEFT 0)
			   ((T :CENTER) (floor (- column-width (cell-memo-width cell-memo)) 2))
			   (:RIGHT (- column-width (cell-memo-width cell-memo)))))))
		(final-y-offset
		  (if (not (cell-memo-align-y cell-memo))
		      y-offset
		      (+ y-offset
			 (ecase (cell-memo-align-y cell-memo)
			   (:TOP 0)
			   ((T :CENTER) (floor (- row-height (cell-memo-height cell-memo)) 2))
			   (:BOTTOM (- row-height (cell-memo-height cell-memo))))))))
	    (let ((*Original-X-Offset* (cell-memo-x-offset cell-memo)))
	      (declare (special *Original-X-Offset*))
	      (offset-memo stream cell-memo final-x-offset final-y-offset))
	    (setf (cell-memo-x-offset cell-memo) final-x-offset))
	  (incf x-offset column-width))
	(setf (memo-offset-p row-memo) T
	      (row-memo-y-offset row-memo) y-offset))
      (incf y-offset (if inter-row-spacing (%+ inter-row-spacing row-height) row-height))))
  ;; now go through the hierarchy from the top, making sure any presentations
  ;; above the cell presentations have their correct coordinates.
  (calculate-presentation-boundaries-of-tree (memo-presentation memo))
  )

#+failed-attempt-for-speed
(defun fix-up-presentations (stream memo *Row-Heights*
			     *Column-Widths* *Start-X* start-y
			     inter-row-spacing)
  ;; go through the data in row-cell-data and clean up the presentation's coordinates.
  (declare (special *Row-Heights* *Column-Widths* *Start-X* inter-row-spacing stream))
  (let ((*Y-Offset* start-y))
    (declare (special *Y-Offset*))
    (dolist (inferior (memo-inferiors memo))
      (fix-up-row-presentations inferior)))
  ;; now go through the hierarchy from the top, making sure any presentations
  ;; above the cell presentations have their correct coordinates.
  (calculate-presentation-boundaries-of-tree (memo-presentation memo)))
#+failed-attempt-for-speed
(defun fix-up-row-presentations (row-memo)
  (declare (special *Y-Offset* *Column-Widths* *Start-X* *Row-Heights*
		    inter-row-spacing))
  (if (row-memo-p row-memo)
      ;; loop through each row.
      (let* ((*Original-Y-Offset* (row-memo-y-offset row-memo))
	     (*Column-Widths* *Column-Widths*)
	     (*Row-Height* (pop *Row-Heights*))
	     (*X-Offset* *Start-X*))
	(declare (special *Row-Height* *Original-Y-Offset* *X-Offset* *Column-Widths*))
	(dolist (inferior (memo-inferiors row-memo))
	  (fix-up-entry-presentations inferior))
	(setf (memo-offset-p row-memo) T
	      (row-memo-y-offset row-memo) *Y-Offset*)
	(incf *Y-Offset* (if inter-row-spacing
			     (%+ inter-row-spacing *Row-Height*)
			     *Row-Height*)))
      (dolist (inferior (memo-inferiors row-memo))
	(fix-up-row-presentations inferior))))
#+failed-attempt-for-speed
(defun fix-up-entry-presentations (cell-memo)
  (declare (special *X-offset* *Y-offset* *Column-Widths* *Row-Height* stream))
  (if (cell-memo-p cell-memo)
      (let ((column-width (pop *Column-Widths*)))
	;; tell the cell it has been offset by x-offset and y-offset.
	(let ((final-x-offset
		(if (not (cell-memo-align-x cell-memo))
		    *X-Offset*
		    (+ *X-Offset*
		       (ecase (cell-memo-align-x cell-memo)
			 (:LEFT 0)
			 ((T :CENTER) (floor (- column-width (cell-memo-width cell-memo)) 2))
			 (:RIGHT (- column-width (cell-memo-width cell-memo)))))))
	      (final-y-offset
		(if (not (cell-memo-align-y cell-memo))
		    *Y-Offset*
		    (+ *Y-Offset*
		       (ecase (cell-memo-align-y cell-memo)
			 (:TOP 0)
			 ((T :CENTER) (floor (- *Row-Height* (cell-memo-height cell-memo)) 2))
			 (:BOTTOM (- *Row-Height* (cell-memo-height cell-memo))))))))
	  (let ((*Original-X-Offset* (cell-memo-x-offset cell-memo)))
	    (declare (special *Original-X-Offset*))
	    (offset-memo stream cell-memo final-x-offset final-y-offset))
	  (setf (cell-memo-x-offset cell-memo) final-x-offset)
	  (incf *X-Offset* column-width)))
      (dolist (inferior (memo-inferiors cell-memo))
	(fix-up-entry-presentations inferior))))

;;; This tells a cell memo that it must be offset.  This should go through each
;;; of its presentations and make sure they have the right offset.

(defvar *Remove-Presentation-from-window-during-table-offset* NIL)


(defun offset-memo (stream memo x-offset y-offset)
  (declare (special *Original-Y-Offset* *Original-X-Offset*))
  (if (table-memo-p memo)
      (offset-table-memo stream memo x-offset y-offset)
      (if (memo-offset-p memo)
	  (let ((previous-x-offset *Original-X-Offset*)
		(previous-y-offset *Original-Y-Offset*))
	    (unless (and (%= x-offset previous-x-offset)
			 (%= y-offset previous-y-offset))
;	  (dformat "~%Offseting ~D,~D for ~D"
;		        (- x-offset previous-x-offset)
;			(- y-offset previous-y-offset) memo)
	      ;; memo has moved, let's fix its offsets.
	      (let ((inferiors (memo-inferiors memo))
		    (new-x-offset (%- x-offset previous-x-offset))
		    (new-y-offset (%- y-offset previous-y-offset))
		    (*Remove-Presentation-from-window-during-table-offset* T))
		(dolist (inferior inferiors)
		  (offset-memo stream inferior new-x-offset new-y-offset))
		(dolist (p (presentation-records (memo-presentation memo)))
		  (offset-memo-internal stream p
					inferiors new-x-offset new-y-offset)))))
	  ;; otherwise calculate it by checking inferior memos
	  ;; and inferior presentations that are not part of any memos.
	  ;; now check all inferior memos
	  (progn
	    (let ((inferiors (memo-inferiors memo)))
	      (dolist (inferior inferiors)
		(offset-memo stream inferior x-offset y-offset))
	      (let ((x-offset x-offset)
		    (y-offset y-offset))
		(when (cell-memo-p memo)
		  (incf x-offset *Absolute-Table-X-Offset*)
		  (incf y-offset *Absolute-Table-Y-Offset*))
		(dolist (p (presentation-records (memo-presentation memo)))
		  (offset-memo-internal stream p
					inferiors x-offset y-offset))))
	    (when (cell-memo-p memo)
	      (incf (cell-memo-x-offset memo) x-offset)
	      (incf (cell-memo-y-offset memo) y-offset))
	    (setf (memo-offset-p memo) T)))))


(defun offset-memo-internal (stream presentation inferiors x-offset y-offset)
  (if (and (presentation-p presentation)
	   (eq 'MEMO (presentation-type presentation)))
      NIL
      ;; check its values.
      (if (not (presentation-p presentation))
	  (progn
	    (when *Remove-Presentation-from-window-during-table-offset*
	      (unless (presentation-p presentation)
		(let ((*Erase-p* T))
		  (redraw-presentation presentation *Real-Stream* NIL NIL NIL NIL))
		(push presentation *Deferred-Redraw-Presentations*)))
	    (offset-presentation presentation x-offset y-offset))
	  (dolist (record (presentation-records presentation))
	    (offset-memo-internal stream  record inferiors x-offset y-offset)))))

(defun offset-table-memo (stream memo x-offset y-offset)
  (let ((new-x-offset x-offset)
	(new-y-offset y-offset))
    (when (memo-offset-p memo)
      (let ((previous-x-offset (table-memo-x-offset memo))
	    (previous-y-offset (table-memo-y-offset memo)))
	(setq new-x-offset (%- x-offset previous-x-offset)
	      new-y-offset (%- y-offset previous-y-offset))))
    ;; table memo has moved, let's fix its offsets.
    (let ((inferiors (memo-inferiors memo))
	  (*Remove-Presentation-from-window-during-table-offset*
	    (and (memo-offset-p memo)
		 (not *Displaying-Table-First-Time-P*))))
      (unless (and (zerop new-x-offset) (zerop new-y-offset))
	(dolist (inferior inferiors)
	  (offset-table-memo-inferiors stream inferior new-x-offset new-y-offset))
	(dolist (p (presentation-records (memo-presentation memo)))
	  (offset-memo-internal stream p
				       inferiors new-x-offset new-y-offset))))
    (setf (memo-offset-p memo) T
	  (table-memo-x-offset memo) x-offset
	  (table-memo-y-offset memo) y-offset)))


(defun offset-table-memo-inferiors (stream memo delta-x delta-y)
  (let ((inferiors (memo-inferiors memo)))
    (when (table-memo-p memo)
      (incf (table-memo-x-offset memo) delta-x)
      (incf (table-memo-y-offset memo) delta-y))
    (dolist (inferior inferiors)
      (offset-table-memo-inferiors stream inferior delta-x delta-y))
    (dolist (p (presentation-records (memo-presentation memo)))
      (offset-memo-internal stream p
				   inferiors delta-x delta-y))))



(defun find-cell-boundaries (memo &OPTIONAL (width 0) (height 0))
  (declare (fixnum width height))
  (if (memo-offset-p memo)
      (values (memo-width memo) (memo-height memo))
      ;; otherwise calculate it by checking inferior memos
      ;; and inferior presentations that are not part of any memos.
      (progn
	(let ((inferiors (memo-inferiors memo)))
	  (dolist (presentation (presentation-records (memo-presentation memo)))
	    (if (not (presentation-p presentation))
		(progn (maximize width (boxed-presentation-right presentation))
		       (maximize height (boxed-presentation-bottom presentation)))
		(dolist (record (presentation-records presentation))
		  (multiple-value-setq (width height)
		    (find-cell-boundaries-internal record inferiors width height)))))
	  ;; now check all inferior memos
	  (dolist (inferior inferiors)
	    (multiple-value-setq (width height)
	      (find-cell-boundaries inferior width height))))
	(setf (memo-width memo) width
	      (memo-height memo) height)
	(values width height))))

(defun find-cell-boundaries-internal (presentation inferiors width height)
  (declare (fixnum width height))
  (if (and (presentation-p presentation)
	   (eq 'memo (presentation-type presentation)))
      (values width height)
      ;; check its values.
      (progn
	(if (not (presentation-p presentation))
	    (progn (maximize width (boxed-presentation-right presentation))
		   (maximize height (boxed-presentation-bottom presentation)))
	    (dolist (record (presentation-records presentation))
	      (multiple-value-setq (width height)
		(find-cell-boundaries-internal record inferiors width height))))
	(values width height))))



(defun calculate-table-sizes (table-memo equalize-column-widths multiple-columns
			      multiple-column-inter-column-spacing
			      extend-width extend-height width height inter-column-spacing)
  (declare (values total-table-width total-table-height row-heights column-widths
		   row-cell-data)
	   (fixnum width height))
  (when (numberp extend-width) (make-fixnum extend-width))
  (let ((data (find-table-cell-data table-memo)))
    (declare (list data))
    (let* ((number-of-multiple-columns 1)
	   (total-table-width 0)
	   (total-table-height 0)
	   (number-of-columns (let ((max 0))
				(declare (fixnum max))
				(dolist (row-info data)
				  (declare (list row-info))
				  (setq max
					(max max
					     (the fixnum (length (the list (cdr row-info)))))))
				max))
	   (max-column-width 0)
	   ;; now parse apart and find maximum column widths.
	   (column-widths (make-list number-of-columns :INITIAL-ELEMENT 0))
	   (number-of-rows (length data))
	   (row-heights  (make-list number-of-rows :INITIAL-ELEMENT 0)))
      (declare (fixnum max-column-width number-of-rows number-of-multiple-columns)
	       (list column-widths row-heights))
      (do* ((rows data (cdr rows))
	    (row-info (first rows)(first rows))
	    (row-w row-heights (cdr row-w)))
	   ((null rows))
	(let ((row-height 0))
	  (declare (fixnum row-height))
	  (do* ((columns column-widths (cdr columns))
		(sub-list (cdr row-info) (cdr sub-list))
		(cell-memo (first sub-list)(first sub-list)))
	       ((null sub-list))
	    (multiple-value-bind (width height)
		(find-cell-boundaries cell-memo)
	      (declare (fixnum width height))
	      (when (numberp inter-column-spacing)
		(incf width (the fixnum inter-column-spacing)))
	      (maximize max-column-width width)
	      (maximize (first columns) width)
	      (maximize row-height height)
	      #+ignore
	      (setf max-column-width (max max-column-width width)
		    (first columns) (max (the fixnum (first columns)) width)
		    row-height (max row-height height))))
	  (setf (first row-w) row-height)))
      (setq total-table-width (if equalize-column-widths
				  (%* number-of-columns max-column-width)
				  (apply #'+ column-widths)))
      (when multiple-columns
	(setq number-of-multiple-columns
	      (the (values fixnum number)
		   (floor width (%+ total-table-width multiple-column-inter-column-spacing)))))
      (let ((width (the (values fixnum number) (floor width number-of-multiple-columns))))
	(declare (fixnum width))
	(when (numberp extend-width)
	  (setq extend-width (the (values fixnum number)
				  (floor (the fixnum extend-width)
					 number-of-multiple-columns))))
	(when (and extend-width (%> (if (numberp extend-width) extend-width width)
				    total-table-width))
	  (let ((extra-width-per-column (the (values fixnum number)
					     (floor (%- (if (numberp extend-width)
							    extend-width width)
							total-table-width)
						    number-of-columns))))
	    ;; need to change typical width
	    (if equalize-column-widths
		(incf max-column-width extra-width-per-column)
		(do ((sub-list column-widths (cdr sub-list)))
		    ((null sub-list))
		  (incf (first sub-list) extra-width-per-column))))))
      ;; recompute table width to include any extras.
      (setq total-table-width (if equalize-column-widths
				  (%* number-of-columns max-column-width)
				  (apply #'+ column-widths)))
      ;; fixup list of table column widths if necessary.
      (when equalize-column-widths
	(do ((columns column-widths (cdr columns)))
	    ((null columns))
	  (setf (first columns) max-column-width)))
      (setq total-table-height (apply #'+ row-heights))
      (when (and extend-height (%> (if (numberp extend-height) extend-height height)
				   total-table-height))
	(let ((extra-height-per-row (floor (- (if (numberp extend-height)
						  extend-height height)
					      total-table-height)
					   number-of-rows)))
	  ;; need to change typical height
	  (do ((sub-list row-heights (cdr sub-list)))
	      ((null sub-list))
	    (incf (first sub-list) extra-height-per-row))))
      (setq total-table-height (apply #'+ row-heights))
      (setf (memo-width table-memo) total-table-width
	    (memo-height table-memo) total-table-height)
      (setf (memo-offset-p table-memo) T)
      (values total-table-width total-table-height row-heights column-widths data))))



#+failed-attempt-for-speed
(defun calculate-table-sizes (table-memo equalize-column-widths multiple-columns
			      multiple-column-inter-column-spacing
			      extend-width extend-height width height inter-column-spacing)
  (declare (values total-table-width total-table-height row-heights column-widths)
	   (fixnum width height))
  (when (numberp extend-width) (make-fixnum extend-width))
  (let* ((number-of-multiple-columns 1)
	 (total-table-width 0)
	 (total-table-height 0)
	 (number-of-columns 0)
	 (max-column-width 0)
	 ;; now parse apart and find maximum column widths.
	 (column-widths NIL)
	 (number-of-rows 0)
	 (row-heights  NIL))
    (declare (fixnum max-column-width number-of-rows number-of-multiple-columns)
	     (list column-widths row-heights))
    (multiple-value-setq (row-heights column-widths)
      (calculating-row-entry-sizes table-memo))
    (when (numberp inter-column-spacing)
      (do ((list column-widths (cdr list)))
	  ((null list))
	(incf (first list) (the fixnum inter-column-spacing))))
    (setq number-of-columns (length column-widths)
	  number-of-rows (length row-heights))
    (setq total-table-width (if equalize-column-widths
				(%* number-of-columns max-column-width)
				(apply #'+ column-widths)))
    (when multiple-columns
      (setq number-of-multiple-columns
	    (the (values fixnum number)
		 (floor width (%+ total-table-width multiple-column-inter-column-spacing)))))
    (let ((width (the (values fixnum number) (floor width number-of-multiple-columns))))
      (declare (fixnum width))
      (when (numberp extend-width)
	(setq extend-width (the (values fixnum number)
				(floor (the fixnum extend-width)
				       number-of-multiple-columns))))
      (when (and extend-width (%> (if (numberp extend-width) extend-width width)
				  total-table-width))
	(let ((extra-width-per-column (the (values fixnum number)
					   (floor (%- (if (numberp extend-width)
							  extend-width width)
						      total-table-width)
						  number-of-columns))))
	  ;; need to change typical width
	  (if equalize-column-widths
	      (incf max-column-width extra-width-per-column)
	      (do ((sub-list column-widths (cdr sub-list)))
		  ((null sub-list))
		(incf (first sub-list) extra-width-per-column))))))
    ;; recompute table width to include any extras.
    (setq total-table-width (if equalize-column-widths
				(%* number-of-columns max-column-width)
				(apply #'+ column-widths)))
    ;; fixup list of table column widths if necessary.
    (when equalize-column-widths
      (do ((columns column-widths (cdr columns)))
	  ((null columns))
	(setf (first columns) max-column-width)))
    (setq total-table-height (apply #'+ row-heights))
    (when (and extend-height (%> (if (numberp extend-height) extend-height height)
				 total-table-height))
      (let ((extra-height-per-row (floor (- (if (numberp extend-height)
						extend-height height)
					    total-table-height)
					 number-of-rows)))
	;; need to change typical height
	(do ((sub-list row-heights (cdr sub-list)))
	    ((null sub-list))
	  (incf (first sub-list) extra-height-per-row))))
    (setq total-table-height (apply #'+ row-heights))
    (setf (memo-width table-memo) total-table-width
	  (memo-height table-memo) total-table-height)
    (setf (memo-offset-p table-memo) T)
    (values total-table-width total-table-height row-heights column-widths)))

;; goal is to get a list of row-heights and a list of column widths.
;; we do a depth-first walk of the tree.
;; we walk down to a cell and then across the cells of the same row.
;; as we walk across this row we need to build a list of column widths
;; and a maximum height for the row.
#+failed-attempt-for-speed
(defun calculating-row-entry-sizes (inferior)
  (let ((*Column-Widths* NIL)
	(*Row-Heights* NIL))
    (declare (special *Column-Widths* *Row-Heights*))
    (dolist (inferior (memo-inferiors inferior))
      (calculating-row-sizes inferior))
    (values (nreverse *Row-Heights*) *Column-Widths*)))
#+failed-attempt-for-speed
(defun calculating-row-sizes (inferior)
  (declare (special *Row-Heights* *Column-Widths*))
  ;; first look for a row entry.
  (cond ((row-memo-p inferior)
	 ;; now search for its cells
	 (let ((*Row-Height* 0)
	       (*Previous-Column-Widths* *Column-Widths*)
	       (*Current-Column-Widths* *Column-Widths*))
	   (declare (special *Row-Height* *Previous-Column-Widths* *Current-Column-Widths*))
	   (dolist (inferior (memo-inferiors inferior))
	     (calculating-entry-sizes inferior))
	   (push *Row-Height* *Row-Heights*)))
	(T (dolist (inferior (memo-inferiors inferior))
	     (calculating-row-entry-sizes inferior)))))
#+failed-attempt-for-speed
(defun calculating-entry-sizes (inferior)
  (declare (special *Row-Height* *Row-Heights* *Column-Widths*
		    *Previous-Column-Widths* *Current-Column-Widths*))
  (if (cell-memo-p inferior)
      ;; we have found an entry.
      ;; figure out its size.
      (progn
	(multiple-value-bind (width height)
	    (find-cell-boundaries inferior)
	  (maximize *Row-Height* height)
	  (if (not *Current-Column-Widths*)
	      (if (not *Previous-Column-Widths*)
		  ;; must be the very first one.
		  (setq *Previous-Column-Widths* (setq *Column-Widths* (list width)))
		  ;; must need to add one more.
		  (progn
		    (nconc *Previous-Column-Widths* (list width))
		    (setq *Previous-Column-Widths* (cdr *Previous-Column-Widths*))))
	      ;; just change the current one.
	      (maximize (first *Current-Column-Widths*) width)
	      (if (eq *Current-Column-Widths* *Previous-Column-Widths*)
		  (setq *Current-Column-Widths* (cdr *Current-Column-Widths*))
		  (setq *Current-Column-Widths* (cdr *Current-Column-Widths*)
			*Previous-Column-Widths* (cdr *Previous-Column-Widths*))))))
      (mapcan #'(lambda (inferior)
		  (calculating-entry-sizes inferior))
	      (memo-inferiors inferior))))


(defun formatting-list-element-internal (stream continuation)
  (declare (special *Formatting-List-Elements*) (ignore stream))
  (push (with-output-to-presentation-recording-string (stream)
	  (funcall continuation stream))
	*Formatting-List-Elements*))

;; there are several ways to implement this function
;; 1. run entire body while saving output in a temporary location.  THen when all done
;;    compute how to present it to real stream knowing what separators and the like
;;    to use.
;; 2. Run the body, but as soon as you can output something correctly. Do It.
;;    Puts more burden on formatting-list-element-internal

;; For now, let's just collect it all together, then worry about it.

(defun formatting-list-internal (stream separator finally if-two
				 filled after-line-break conjunction
				 continuation)
  (warn-unimplemented-args filled NIL after-line-break NIL)
  (let ((*Formatting-List-Elements* NIL))
    (declare (special *Formatting-List-Elements*))
    (funcall continuation stream)
    (setq *Formatting-List-Elements* (nreverse *Formatting-List-Elements*))
    (cond ((null *Formatting-List-Elements*))
	  ((null (cdr *Formatting-List-Elements*))
	   (princ (first *Formatting-List-Elements*) stream))
	  ((null (cddr *Formatting-List-Elements*))
	   (princ (first *Formatting-List-Elements*) stream)
	   (cond (if-two (princ if-two stream))
		 (conjunction (princ " " stream) (princ conjunction stream) (princ " " stream))
		 (T (princ separator stream)))
	   (princ (second *Formatting-List-Elements*) stream))
	  (T (princ (first *Formatting-List-Elements*) stream)
	     (do ((sub-seq (cdr *Formatting-List-Elements*) (cdr sub-seq)))
		 ((null sub-seq))
	       (if (null (cdr sub-seq))
		   (cond (finally (princ finally stream))
			 (conjunction (princ " " stream)(princ conjunction stream)
				      (princ " " stream))
			 (T (princ separator stream)))
		   (princ separator stream))
	       (princ (first sub-seq) stream))))))

(defun format-list (sequence function
		    &KEY (separator ",") finally if-two
		    filled after-line-break conjunction (stream *Standard-Output*))
  (flet ((internal ()
	   (formatting-list (stream :separator separator :finally finally
				    :if-two if-two :filled filled
				    :after-line-break after-line-break
				    :conjunction conjunction)
			    (dolist (element sequence)
			      (formatting-list-element (stream)
						       (funcall function element stream))))))
    (if filled
	(with-output-filling (stream :after-line-break after-line-break)
	  (internal))
	(internal))))




(defun trace-table ()
  (setq *debug* t)
  (trace make-table-internal entry-internal
	 calculate-table-sizes find-cell-boundaries-internal find-cell-boundaries
	 offset-memo-internal offset-memo
	 fix-up-presentations
	 find-matching-memo))



(defun make-table-from-generated-sequence-internal
       (stream inter-row-spacing inter-column-spacing
	row-wise output-row-wise n-rows
	n-columns equalize-column-widths inside-width
	inside-height max-width max-height unique-id
	continuation
	&AUX
	;;5/1/90 (sln) since we aren't using
	;;these, out with 'em (see note below)
;	old-total-width old-total-height old-row-heights
;	old-column-widths old-cell-data
	new-total-width new-total-height new-row-heights
	new-column-widths new-cell-data)
  (declare (ignore inter-row-spacing output-row-wise inside-width inside-height))
  (unless inter-column-spacing (setq inter-column-spacing (send stream :CHAR-WIDTH)))
  (flet ((internal (stream)
	   (multiple-value-bind (start-x start-y)
	       (read-cursorpos stream)
	     (multiple-value-bind (width height) (window-size stream)
	       ;; first make the low level display on the fake window.
	       (let ((old-table-memo
		       (find-matching-memo unique-id #'EQL :TABLE NIL)))
;		 (when old-table-memo
;		   ;; we have previously displayed this table.
;		   ;; find its previous cell positions.
;		   (multiple-value-setq (old-total-width old-total-height old-row-heights
;							 old-column-widths old-cell-data)
;		     (calculate-format-item-table-sizes
;		       old-table-memo n-rows n-columns row-wise
;		       max-width max-height
;		       equalize-column-widths width height
;		 inter-column-spacing)))
		 ;; 5/1/90 (sln) See that code commented out above.
		 ;; Pretty funky, huh!?  It setqs those five variables,
		 ;; but doesn't use 'em.  Well, I'm sick of the
		 ;; compilation warnings so I'm removing the call to
		 ;; multiple-value-setq.  On the offchance that
		 ;; calculate-table-sizes has side-effects, I'll leave
		 ;; it in.  I suppose I could look it up, huh?  Oh well,
		 ;; next time I'm in the code.
		 (when old-table-memo
		   (calculate-format-item-table-sizes
		       old-table-memo n-rows n-columns row-wise
		       max-width max-height
		       equalize-column-widths width height inter-column-spacing))
		 (let* ((new-memo NIL)
			(presentations
			  (with-output-to-fake-window (stream stream)
			    (setq new-memo
				  (memoize
				    (:MEMO-TYPE :TABLE :ID unique-id :STREAM stream)
				    (funcall continuation stream)))
			    ;;new-memo (first (memo-inferiors *Memo*))
			    )))
		   ;; we can part ways here maybe, if this is the first time
		   ;; do it easily
		   (multiple-value-setq (new-total-width new-total-height new-row-heights
							 new-column-widths new-cell-data)
		     (calculate-format-item-table-sizes
		       new-memo n-rows n-columns row-wise
		       max-width max-height
		       equalize-column-widths width height inter-column-spacing))
		   (if (not old-table-memo)
		       (let ((*Displaying-Table-First-Time-P* T))
			 ;; since this is the first time we have displayed this thing
			 ;; we can just fix up the presentations below and then insert them
			 ;; into the table
			 (fix-up-format-item-presentations stream new-memo
					       new-row-heights new-column-widths
					       new-cell-data
					       start-x start-y)
			 (insert-presentations stream presentations T)
			 )
		       (let ((*Absolute-Table-X-Offset*
			       (table-memo-x-offset new-memo))
			     (*Absolute-Table-Y-Offset*
			       (table-memo-y-offset new-memo)))
			 #+ignore
			 (dformat "~&Absolute X ~D Y ~D"
				  *Absolute-Table-X-Offset* *Absolute-Table-Y-Offset*)
			 (fix-up-format-item-presentations stream new-memo
					       new-row-heights new-column-widths
					       new-cell-data
					       start-x start-y)
			 (insert-presentations stream presentations T))
		       )
		   (set-cursorpos stream
				  (%+ start-x new-total-width)
				  (%+ start-y new-total-height))))))))
    (if (not *Memo*)
	(run-memo (memo (stream)
		    (internal stream)) stream)
	(internal stream))))



;;; Here is the strategy for making table
;;; 1. Use given value for N-Columns
;;;    a. Put appropriate number of elements in each column based on number-of-elements
;;;    b. Figure out there respective sizes and place them.
;;; 2. Use Given value for N-Rows
;;; 3. If neither given, then compute maximal size of an element
;;;    Try to make table as square as possible taking into consideration the limits
;;;    on the table's width and height.  NOTE: How do we account for the fact that
;;;    the column widths are dependent on how many are in the column which will affect the
;;;    limit.  For now, just assume that we make it fit in the width direction first if given.

(defun transform-matrix (matrix)
  (cond ((not (some #'identity matrix)) NIL)
	(T (cons (mapcan #'(lambda (x) (and (first x) (list (first x))))
			 matrix)
		 (transform-matrix
		   (mapcar #'cdr matrix))))))

(defun calculate-format-item-table-sizes (table-memo n-rows n-columns row-wise
					  max-width max-height
					  equalize-column-widths
					  width height
					  inter-column-spacing)
  (declare (values total-table-width total-table-height row-heights column-widths
		   cell-data) (ignore row-wise equalize-column-widths)
	   (fixnum width height))
  (flet ((sort-cells-by-rows (cells number-per-row)
	   (let ((row NIL)
		 (rows NIL))
	     (do ((cs cells (cdr cs))
		  (index 0 (1+ index)))
		 ((null cs) (when row (setq rows (nconc rows (list row)))))
	       (declare (fixnum index))
	       (when (%= number-per-row index)
		 (when row (setq rows (nconc rows (list row))))
		 (setq row NIL)
		 (setq index 0))
	       (setq row (nconc row (list (first cs))))))))
    (let ((cells (find-table-cells table-memo)))
      (declare (list cells))
      (when (and (not (numberp n-columns)) (not (numberp n-rows)))
	(let ((max-cell-width 0) (max-cell-height 0))
	  (declare (fixnum max-cell-width max-cell-height))
	  (dolist (cell cells)
	    (multiple-value-bind (width height)
		(find-cell-boundaries cell)
	      (declare (fixnum width height))
	      #+ignore
	      (setq max-cell-width (max max-cell-width width)
		    max-cell-height (max max-cell-height height))
	      (maximize max-cell-width width)
	      (maximize max-cell-height height)))
	  ;; now figure out how many can fit into a row base on row and height.
	  (incf max-cell-width (the fixnum inter-column-spacing))
	  (cond (max-width
		 (setq n-columns (the (values fixnum number) (floor width max-cell-width)))
		 #+ignore(when *Debug*
		   (format tv:selected-window "~&Selecting ~D columns due to max-width ~D."
			   n-columns max-width)))
		(max-height
		 (setq n-rows (the (values fixnum number) (floor height max-cell-height)))
		 #+ignore(when *Debug*
		   (format tv:selected-window "~&Selecting ~D rows due to max-height ~D."
			   n-rows max-height)))
		(T ;; so guess, to make it a square
		 (setq n-rows
		       (round (the float (sqrt (the float
						    (/ (the float
							    (* (the fixnum (length cells))
							       (float max-cell-width)))
						       max-cell-height))))))
		 #+ignore(when *Debug*
		   (format tv:selected-window
			   "~&Selecting ~D rows due to square Cell Width ~D Height ~D # ~D"
			   n-rows max-cell-width max-cell-height (length cells)))))))
      (let* ((rows (if (numberp n-rows)
		       (sort-cells-by-rows cells
					   (floor (length cells) (the fixnum n-rows)))
		       (sort-cells-by-rows cells
					   (floor (length cells)
						  (the fixnum
						       (ceiling (the fixnum (length cells))
								(the fixnum n-columns)))))))
	     (row-heights (make-list (length rows) :INITIAL-ELEMENT 0))
	     (column-widths (make-list (apply #'max (mapcar #'length rows))
				       :INITIAL-ELEMENT 0)))
	(declare (list rows))
	(do ((row-cells rows (cdr row-cells))
	     (heights row-heights (cdr heights)))
	    ((null row-cells))
	  (do ((cells (first row-cells) (cdr cells))
	       (widths column-widths (cdr widths)))
	      ((null cells))
	    (multiple-value-bind (width height)
		(find-cell-boundaries (first cells))
	      (declare (fixnum width height))
	      (setf (first widths) (the fixnum (max (%+ inter-column-spacing width)
						    (the fixnum (first widths))))
		    (first heights) (the fixnum (max (the fixnum (first heights)) height))))))
	;; at this point we have figured out the row heights and the column widths
	;; so we should return
	(values (apply #'+ column-widths)
		(apply #'+ row-heights)
		row-heights column-widths
		rows)))))

;;; This takes the presentations as displayed within each cell and adds offsets
;;; so that they are properly placed within the table.  NOTE: what do we do
;;; when redisplaying?  Some of the presentations have already been given offsets.
;;; We don't want to offset them twice.  ALSO, what if their new offset were different
;;; then their old?

(defun fix-up-format-item-presentations (stream memo row-heights
					 column-widths cell-data start-x start-y)
  ;; go through the data in cell-data and clean up the presentation's coordinates.
  (let ((y-offset start-y))
    ;; loop through each row.
    (do* ((rows cell-data (cdr rows))
	  (row-info (first rows) (first rows))
	  (sub-row-heights row-heights (cdr sub-row-heights))
	  (row-height (first sub-row-heights) (first sub-row-heights)))
	 ((null rows))
      (let ((x-offset start-x))
	;; loop through each cell in a particular row.
	(do* ((cells row-info (cdr cells))
	      (cell-memo (first cells) (first cells))
	      (sub-column-widths column-widths (cdr sub-column-widths))
	      (column-width (first sub-column-widths) (first sub-column-widths)))
	     ((null cells))
	  ;; tell the cell it has been offset by x-offset and y-offset.
	  (let ((*Original-X-Offset* (cell-memo-x-offset cell-memo))
		(*Original-Y-Offset* (cell-memo-y-offset cell-memo)))
	    (declare (special *Original-X-Offset* *Original-Y-Offset*))
	    (offset-memo stream cell-memo x-offset y-offset))
	  (setf (cell-memo-x-offset cell-memo) x-offset)
	  (setf (cell-memo-y-offset cell-memo) y-offset)
	  (incf x-offset column-width)))
      (incf y-offset row-height)))
  ;; now go through the hierarchy from the top, making sure any presentations
  ;; above the cell presentations have their correct coordinates.
  (calculate-presentation-boundaries-of-tree (memo-presentation memo))
  )



(defvar *Optimal-Number-Of-Rows* NIL)

(defun make-table-from-sequence
       (elements &KEY (stream *Standard-Output*) printer type
	(key #'identity) (fresh-line t) (return-at-end t)
	(order-columnwise t)
	(optimal-number-of-rows *Optimal-Number-Of-Rows*)
	(additional-indentation 2) equalize-column-widths max-width
	row-spacing column-spacing
	max-height)
  (when fresh-line (fresh-line stream))
  (when (numberp additional-indentation)
    (increment-cursorpos stream additional-indentation 0 :CHARACTER))
  ;; elements can be either a vector of 1,2 dimensions or a list.
  (typecase elements
    (list
      (make-table-from-generated-sequence
	(stream :ROW-WISE (not order-columnwise)
		:N-ROWS optimal-number-of-rows
		:EQUALIZE-COLUMN-WIDTHS equalize-column-widths
		:MAX-WIDTH max-width :MAX-HEIGHT max-height)
	(dolist (item elements)
	  (setq item (funcall key item))
	  (entry (stream)
	    (if type
		(display item type :STREAM stream)
		(funcall (or printer #'princ) item stream))))))
    ((array T (*))
     (make-table-from-generated-sequence
       (stream :ROW-WISE (not order-columnwise)
	       :N-ROWS optimal-number-of-rows
	       :EQUALIZE-COLUMN-WIDTHS equalize-column-widths
	       :MAX-WIDTH max-width :MAX-HEIGHT max-height)
       (dotimes (x (length (the array elements)))
	 (let ((item (funcall key (aref elements x))))
	   (entry (stream)
	     (if type
		 (display item type :STREAM stream)
		 (funcall (or printer #'princ) item stream)))))))
    ((array T (* *))
     (make-table (stream :EQUALIZE-COLUMN-WIDTHS equalize-column-widths
			 :ROW-SPACING row-spacing :COLUMN-SPACING column-spacing)
       (dotimes (x (array-dimension elements 0))
	 (table-row (stream)
	   (dotimes (y (array-dimension elements 1))
	     (let ((item (funcall key (aref elements x y))))
	       (entry (stream)
		 (if type
		     (display item type :STREAM stream)
		     (funcall (or printer #'(lambda (object stream &REST args)
					      (declare (ignore args))
					      (princ object stream)))
			      item stream
			      x y))))))))))
  (when return-at-end
    (terpri stream)))




(defun filling-output-internal (stream fill-column fill-characters after-line-break
				initially-too body)
  (let ((old-fill (window-fill stream))
	(new-fill NIL))
    (unwind-protect
	(progn
	  (setq new-fill (pop *Fill-Resource*))
	  (if new-fill
	      (setf (fill-object-column new-fill) fill-column
		    (fill-object-characters new-fill) fill-characters
		    (fill-object-line-break new-fill) after-line-break
		    (fill-object-initially new-fill) initially-too)
	      (setq new-fill (make-fill-object :COLUMN fill-column :CHARACTERS fill-characters
					       :line-break after-line-break
					       :initially initially-too)))
	  (setf (window-fill stream) new-fill)
	  (funcall body stream))
      (when (window-fill stream)
	(push (window-fill stream) *Fill-Resource*))
      (setf (window-fill stream) old-fill))))




(defun format-sequence-as-table-rows (sequence printer &KEY
				      (stream *Standard-Output*)
				      equalize-column-widths extend-width
				      extend-height
				      (inter-row-spacing 0)
				      (inter-column-spacing (ew:send stream :CHAR-WIDTH))
				      multiple-columns
				      (multiple-column-inter-column-spacing
					inter-column-spacing)
				      equalize-multiple-column-widths
				      output-multiple-columns-row-wise)
  ;; 4/30/90 (SLN)
  (declare (ignore output-multiple-columns-row-wise equalize-multiple-column-widths multiple-column-inter-column-spacing))
  (make-table (stream
	       :equalize-column-widths equalize-column-widths
	       :row-spacing inter-row-spacing
	       :extend-width extend-width
	       :extend-height extend-height
	       :column-spacing inter-column-spacing
	       :multiple-columns multiple-columns
	       :multiple-column-inter-column-spacing
	       multiple-column-inter-column-spacing)
    (dotimes (x (length sequence))
      (row (stream)
	(funcall printer (elt sequence x) stream)))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
