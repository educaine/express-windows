;;; -*- Mode: LISP; Syntax: Common-lisp; Package: USER; Base: 10 -*-

;;; ------------------------------
;;;
;;;	#+/-<feature>s used in this file:
;;;
;;;	#+excl
;;;	#+lucid
;;;	#+symbolics
;;;	#+ti
;;;	#+lispm
;;;	#+editor
;;;	#+pcl
;;;
;;; ------------------------------

;;; ------------------------------
;;;	IN-PACKAGE
;;; ------------------------------

(in-package :user)

;;; -----------------------------------------------------------------
;;;
;;;	MISCELLANEOUS	MISCELLANEOUS	MISCELLANEOUS	MISCELLANEOUS
;;;
;;; -----------------------------------------------------------------

(unless (find-package :common-lisp)
     (rename-package :lisp :lisp (union '("CL" "COMMON-LISP") (package-nicknames (find-package :lisp)) :test #'string=)))
(unless (find-package :common-lisp-user)
     (rename-package :user :user (union '("CL-USER" "COMMON-LISP-USER") (package-nicknames (find-package :user)) :test #'string=)))

(defparameter *EW-SYSTEM-DATE* "8/3/90 Goodwill Express Windows")

#-lispm
(progn
  (defvar *Loaded-Files* (make-hash-table :test 'equal))

  (defvar *Binary-File-Type* #+Excl "fasl" #+lucid "lbin" #+symbolics "BIN" #+ti "XLD")

  (defun get-pathname-properties (pathname)
    (let ((name (namestring pathname))) (gethash name *Loaded-Files*)))

  (defun set-pathname-properties (pathname write-date)
    (let ((name (namestring pathname))) (setf (gethash name *Loaded-Files*) write-date)))

  (defun load-file (file)
    (setq file (make-pathname :type *Binary-File-Type* :defaults (parse-namestring file)))
    (let ((results (multiple-value-list (load file))))
      (set-pathname-properties file (file-write-date file))
      (values-list results)))

  (defun load-file-maybe (file)
    (setq file (make-pathname :type *Binary-File-Type*
			      :defaults (parse-namestring file)))
    (let ((write-date (and (probe-file file)
			   (file-write-date file))))
      (cond ((numberp write-date)
	     (let ((previous-load (get-pathname-properties file)))
	       (if (numberp previous-load)
		   (when (> write-date previous-load)
		     (load file)
		     (set-pathname-properties file write-date))
		   (progn (load file)
			  (set-pathname-properties file write-date)))))
	    (T (format T "~&File ~A does not exist to load.~%" file)))))

  (defun compile-file-maybe (file &key output-file)
    (let ((lisp-version (make-pathname :defaults file :type #-lispm "lisp" #+lispm "LISP"))
	  (binary-version (merge-pathnames (or output-file file )
					   (make-pathname :defaults file :type (or (and output-file (pathname-type output-file))
										   *Binary-File-Type*)))))
      (let ((lisp-date (and (probe-file lisp-version)
			    (file-write-date lisp-version)))
	    (binary-date (and (probe-file binary-version)
			      (file-write-date binary-version))))
	(if (not (numberp lisp-date))
	    (format T "~&File ~A does not exist to compile.~%" file)
	    (when (or (and (numberp binary-date) (> lisp-date binary-date))
		      (not (numberp binary-date)))
	      #+symbolics (format T "~&Compiling File ~A." lisp-version)
	      (compile-file lisp-version :output-file binary-version))))))

  (defmacro with-compilation-unit ((&rest options &key override (verbose :default) &allow-other-keys) &body body)
    #-excl (declare (ignore options))
    (setq override (case override
		     #+lispm (:warn :just-warn)
		     #-lucid (:query nil)
		     (t override)))
    `(#+lucid lucid::with-deferred-warnings
      #+symbolics compiler:compiler-warnings-context-bind
      #-(or lucid symbolics) progn
      (let (#+lispm (SYS:INHIBIT-FDEFINE-WARNINGS ,override)
	    #+lucid (lcl::*redefinition-action* ,override)
	    #+excl (excl::*redefinition-warnings* ,override)
	    #+excl (compiler::*compile-verbose* (if (eq ':default ,verbose) compiler::*compile-verbose* ,verbose))
	    #+lucid (lucid::*compiler-options* (apply #'lucid::process-compiler-options
						      (unless (eq ':default ,verbose) (list ':messages ,verbose))))
	    #+symbolics (compiler::compiler-verbose (if (eq ':default ,verbose) compiler::compiler-verbose ,verbose)))
	#+(or symbolics excl lucid)
	(declare (special #+symbolics compiler::compiler-verbose
			  #+excl compiler::*compile-verbose*
			  #+lucid lucid::*compiler-options*))
	.,body)))
  )

;;; ------------------------------
;;;	DEFSYSTEM
;;; ------------------------------

#-lispm
(progn
  (defparameter *EW-TOP-LEVEL-DIRECTORY* "~/ew/"
    "Top Level Directory of Express Windows.")

  (defparameter *EW-SOURCE-CODE-DIRECTORY* "~/ew/code/"
    "Directory for the Source Code of Express Windows.")

  (defparameter *EW-BINARY-CODE-DIRECTORY*
		(format nil "~~/ew/binary/~a/~a/"
			#+lucid "lcl" #+excl "excl" #+symbolics "scl" #+ti "ticl"
			(format nil "rel~a"
				(or #+(or excl lucid) (aref (lisp-implementation-version) 0)
				    (lisp-implementation-version))))
    "Directory for the Binary Code of Express Windows.")
  )

(defparameter *EW-Files*
	      '("require"
		"defpackage"
		"clx-hacks"
		"meter"
		"cl-clos-macros"
		"fixnum-macros"
		"x-macros-interface"
		"fonts"
		"macros"
		"x-interface"
		"read-internal"
		"quad-trees"
		"base-presentations"
		"type"
		"scrolling"
		"graphic-primitives"
		"io-functions"
		"command-processor"
		"misc"
		"memo"
		"table"
		"query"
		"completion"
		"frames"
		"input-editor"
		"proprietary-presentation-types"
		"presentation-types"
		"lisp-window"))

(defparameter *Optional-EW-Files*
	      '("demos"
		"sym-comp"
		"time"
		"lisp-machine-lisp-compatible"
		"file-manager"
		#+(and lucid editor) "lucid-editor-interface"
		"menu-definer"
		#-symbolics "processes"))

#-lispm
(progn  
  (defun load-ew (&OPTIONAL reset)
    #+lucid (proclaim '(optimize (speed 3) (safety 0)(space 0) (compilation-speed 0)))
    #+excl (unless (excl::scheduler-running-p) (mp:start-scheduler))
    (let ((load-fcn (if reset #'load-file #'load-file-maybe)))
      (dolist (file (append *Ew-Files* *Optional-Ew-Files*))
	(funcall load-fcn (merge-pathnames file *EW-BINARY-CODE-DIRECTORY*))))
    (provide :ew-goodwill)(pushnew :ew-goodwill *features*)
    (provide :ew) (pushnew :EW *Features*))

  (defun compile-ew (&key recompile (verbose :default))
    #+lucid (proclaim '(optimize (speed 3) (safety 0)(space 0) (compilation-speed 0)))
    #+excl (unless (excl::scheduler-running-p) (mp:start-scheduler))
    (let ((compile-fcn (if recompile #'compile-file #'compile-file-maybe))
	  (load-fcn (if recompile #'load-file #'load-file-maybe)))
      (with-compilation-unit (:verbose verbose)
	(dolist (file (append *Ew-Files* *Optional-Ew-Files*))
	  (funcall compile-fcn (merge-pathnames file *EW-SOURCE-CODE-DIRECTORY*)
		   :output-file (merge-pathnames *EW-BINARY-CODE-DIRECTORY*
						 (make-pathname :name file :type *Binary-File-Type*)))
	  (funcall load-fcn (merge-pathnames file *EW-BINARY-CODE-DIRECTORY*)))))
    (provide :ew-goodwill)(pushnew :ew-goodwill *features*)
    (provide :ew) (pushnew :EW *Features*))
  )

#+lispm
(progn
  (unless (fs:get-logical-pathname-host "EW" t)
    (fs:make-logical-pathname-host "EW"))

  #+symbolics
  (defsystem ew
      (:pretty-name "Express Windows"
       :patchable NIL :maintain-journals nil
       :default-destination-pathname #.(format nil "EW:BINARY; SCL; REL~s;" (sct:get-release-version))
       :default-pathname "EW:SOURCE;")
    (:module clx (clx) (:type :system))
    (:module defpackage ("define-defpackage"))
    (:serial clx defpackage . #.(append *ew-files* *optional-ew-files*))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
