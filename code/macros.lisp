;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-


;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************



(cl:in-package 'ew)

;;; ****************************************************************************************
;;; Start Definition of PRESENTATION OBJECTS.
;;; ****************************************************************************************


(defstruct (superior-object (:CONC-NAME NIL))
  superior)

(defstruct (boxed-presentation (:INCLUDE superior-object))
  (left 0 :type fixnum)
  (top 0 :type fixnum)
  (right 0 :type fixnum)
  (bottom 0 :type fixnum))


(defun print-presentation (presentation stream ignore)
  (format stream "<~A ~A ~D,~D  ~D,~D>" (type-of presentation)
	  (presentation-type presentation)
	  (boxed-presentation-left presentation)
	  (boxed-presentation-top presentation)
	  (boxed-presentation-right presentation)
	  (boxed-presentation-bottom presentation)))



(defstruct (presentation (:INCLUDE boxed-presentation) (:print-function print-presentation))
  type
  records
  single-box
  object)


#+(or LCL3.0 (and APOLLO DOMAIN/OS))
(defstruct-simple-predicate presentation presentation-p)
;; 4/30/90 (SLN) Added Symbolics since defstruct automatically creates
;; one.  
;; 6/27/90 (SLN) so does Allegro; so should every other lisp, so
;; I'm commenting this out.
;#-(or SYMBOLICS LCL3.0 (and APOLLO DOMAIN/OS))
;(defmacro presentation-p (stream)
;  `(eq (lisp:type-of ,stream) 'PRESENTATION))


(defstruct (graphics-presentation (:include boxed-presentation))
  (alu 0 :TYPE (integer 0 15)))

(defstruct (string-presentation (:include graphics-presentation)
				(:print-function print-string-presentation))
  ;;printed-object
  string
  font)

;; a text-presentation is used by printing functions.
(defstruct (text-presentation (:include string-presentation)
			      (:print-function print-string-presentation)))

(defstruct (complex-string-presentation (:include string-presentation))
  pattern STIPPLE TILE
  COLOR GRAY-LEVEL OPAQUE
  ;; MASK MASK-X MASK-Y
  )

(defun print-string-presentation (presentation stream ignore)
  (format stream "<~A ~D,~D  ~D,~D " (type-of presentation)
	  (boxed-presentation-left presentation)
	  (boxed-presentation-top presentation)
	  (boxed-presentation-right presentation)
	  (boxed-presentation-bottom presentation))
  (prin1 (string-presentation-string presentation) stream)
  (simple-princ ">" stream))


(defstruct (glyph-presentation (:include graphics-presentation)
			       (:print-function print-string-presentation))
  pattern
  (index 0 :type fixnum)
  font)

(defstruct (complex-glyph-presentation (:include glyph-presentation)
				       (:print-function print-presentation))
  STIPPLE TILE
  COLOR GRAY-LEVEL OPAQUE
  ;; MASK MASK-X MASK-Y
  )

(defstruct (rectangle-presentation (:include graphics-presentation)
				   (:print-function print-presentation))
  pattern
  filled
  (thickness 0 :TYPE fixnum))

(defstruct (complex-rectangle-presentation (:include rectangle-presentation)
					   (:print-function print-presentation))
  STIPPLE TILE
  COLOR GRAY-LEVEL OPAQUE
  ;; MASK MASK-X MASK-Y
  LINE-END-SHAPE LINE-JOINT-SHAPE
  DASHED DASH-PATTERN INITIAL-DASH-PHASE DRAW-PARTIAL-DASHES)


(defstruct (point-presentation (:include graphics-presentation)
			       (:print-function print-presentation))
  pattern)

(defstruct (complex-point-presentation (:include point-presentation)
				       (:print-function print-presentation))
  STIPPLE TILE
  COLOR GRAY-LEVEL OPAQUE
  ;; MASK MASK-X MASK-Y
  )


(defstruct (triangle-presentation (:include graphics-presentation)
				   (:print-function print-presentation))
  filled
  (thickness 0 :TYPE fixnum)
  pattern
  (x1 0 :type fixnum) (y1 0 :type fixnum)
  (x2 0 :type fixnum) (y2 0 :type fixnum)
  (x3 0 :type fixnum) (y3 0 :type fixnum))


(defstruct (arrow-presentation (:include graphics-presentation)
				   (:print-function print-presentation))
  draw-shaft head-length head-width
  filled
  (thickness 0 :TYPE fixnum)
  pattern
  (x1 0 :type fixnum) (y1 0 :type fixnum)
  (x2 0 :type fixnum) (y2 0 :type fixnum))


(defstruct (circle-presentation (:include graphics-presentation)
				(:print-function print-presentation))
  (center-x 0 :type fixnum) (center-y 0 :type fixnum) (radius 0 :type fixnum)
  inner-radius start-angle end-angle
  ;; clockwise join-to-path - don't worry about for now.
  PATTERN FILLED
  (thickness 0 :TYPE fixnum))


(defstruct (complex-circle-presentation (:include circle-presentation)
					(:print-function print-presentation))
  ;; STIPPLE TILE - not used in circle
  ;; COLOR GRAY-LEVEL - not used in circle
  OPAQUE
  ;; MASK MASK-X MASK-Y
  LINE-END-SHAPE LINE-JOINT-SHAPE
  DASHED DASH-PATTERN INITIAL-DASH-PHASE DRAW-PARTIAL-DASHES)


(defstruct (ellipse-presentation (:include graphics-presentation)
				 (:print-function print-presentation))
  (center-x 0 :type fixnum) (center-y 0 :type fixnum)
  (x-radius 0 :type fixnum) (y-radius 0 :type fixnum)
  inner-x-radius inner-y-radius
  start-angle end-angle
  ;; clockwise join-to-path - don't worry about for now.
  PATTERN FILLED
  (thickness 0 :TYPE fixnum))


(defstruct (complex-ellipse-presentation (:include ellipse-presentation)
					 (:print-function print-presentation))
  ;; STIPPLE TILE - not used in ellipse
  ;; COLOR GRAY-LEVEL - not used in ellipse
  OPAQUE
  ;; MASK MASK-X MASK-Y
  LINE-END-SHAPE LINE-JOINT-SHAPE
  DASHED DASH-PATTERN INITIAL-DASH-PHASE DRAW-PARTIAL-DASHES)



(defstruct (line-presentation (:include graphics-presentation)
			      (:print-function print-presentation))
  (start-x 0 :type fixnum) (start-y 0 :type fixnum)
  (end-x 0 :type fixnum) (end-y 0 :type fixnum)
  PATTERN
  ;; FILLED - lines do not take filled arguments.
  (thickness 0 :TYPE fixnum))

(defstruct (complex-line-presentation (:include line-presentation)
				      (:print-function print-presentation))
  STIPPLE TILE
  COLOR GRAY-LEVEL OPAQUE
  ;; MASK MASK-X MASK-Y
  LINE-END-SHAPE LINE-JOINT-SHAPE
  DASHED DASH-PATTERN INITIAL-DASH-PHASE DRAW-PARTIAL-DASHES)



(defstruct (polygon-presentation (:include graphics-presentation)
				 (:print-function print-presentation))
  (points NIL)
  pattern filled
  (thickness 0 :TYPE fixnum))

(defstruct (complex-polygon-presentation (:include polygon-presentation)
					 (:print-function print-presentation))
  STIPPLE TILE
  COLOR GRAY-LEVEL OPAQUE
  ;; MASK MASK-X MASK-Y
  LINE-END-SHAPE LINE-JOINT-SHAPE
  DASHED DASH-PATTERN INITIAL-DASH-PHASE DRAW-PARTIAL-DASHES)

#+ignore
(defstruct (regular-polygon-presentation (:include graphics-presentation)
					 (:print-function print-presentation))
  (start-x 0 :type fixnum)
  (start-y 0 :type fixnum)
  (end-x 0 :type fixnum)
  (end-y 0 :type fixnum)
  (number-of-sides 0 :type fixnum)
  (handedness :LEFT :TYPE (member :LEFT :RIGHT))
  pattern filled
  (thickness 0 :TYPE fixnum))

#+ignore
(defstruct (complex-regular-polygon-presentation (:include polygon-presentation)
						 (:print-function print-presentation))
  STIPPLE TILE
  COLOR GRAY-LEVEL OPAQUE
  ;; MASK MASK-X MASK-Y
  LINE-END-SHAPE LINE-JOINT-SHAPE
  DASHED DASH-PATTERN INITIAL-DASH-PHASE DRAW-PARTIAL-DASHES)

(defstruct (icon-presentation (:include graphics-presentation)
			      (:print-function print-presentation))
  (icon-pattern NIL))


(defstruct (complex-icon-presentation (:include icon-presentation)
				      (:print-function print-presentation))
  COLOR GRAY-LEVEL OPAQUE)

(defstruct (recording-string-presentation (:TYPE LIST))
  (start-index 0 :type fixnum)
  (end-index 0 :type fixnum)
  object
  type)

#+ignore
(defstruct presentation-recording-string
  (string (make-array '(100) :ADJUSTABLE T :ELEMENT-TYPE 'string-char :FILL-POINTER 0)
	  :TYPE string)
  (presentations NIL) ;; presentations of type recording-string-presentation
  )



(defstruct presentation-recording-string
  (string (make-array '(100) :ADJUSTABLE T :ELEMENT-TYPE 'string-char :FILL-POINTER 0))
  (presentations NIL) ;; presentations of type recording-string-presentation
  )

#+(or LCL3.0 (and APOLLO DOMAIN/OS))
(defstruct-simple-predicate presentation-recording-string presentation-recording-string-p)

;; 4/30/90 (SLN) Added Symbolics since defstruct automatically creates one.
;; 6/27/90 (SLN) so does Allegro; so should every other lisp, so
;; I'm commenting this out.
;#-(or Symbolics LCL3.0 (and APOLLO DOMAIN/OS))
;(defmacro presentation-recording-string-p (stream)
;  `(eq (lisp:type-of ,stream) 'PRESENTATION-RECORDING-STRING))


(defstruct (handler (:PRINT-FUNCTION print-handler))
  name
  from-type to-type
  test
  documentation
  gestures
  menu
  context-independent
  blank-area
  (priority 0 :TYPE fixnum)
  exclude-other-handlers
  include-body-in-test
  (suppress-highlighting NIL)
  body)

(defun print-handler (handler stream ignore)
  (format stream "#<Handler ~A>" (handler-name handler)))

(defstruct command-menu-handler
  command-name
  menu-levels
  gestures
  documentation
  function
  test)


(defstruct program-command-menu-template
  name
  level
  items)

(defclass program-pane (presentation-window)
     ((redisplay-after-commands :accessor program-pane-redisplay-after-commands :initarg :redisplay-after-commands :initform NIL)
      (redisplay-function :accessor program-pane-redisplay-function :initarg :redisplay-function :initform NIL)
      (redisplay-string :accessor program-pane-redisplay-string :initarg :redisplay-string :initform NIL)
      (incremental-redisplayer :accessor program-pane-incremental-redisplayer :initarg :incremental-redisplayer :initform NIL)
      (incremental-redisplay :accessor program-pane-incremental-redisplay :initarg :incremental-redisplay :initform NIL)
      (height-in-lines :accessor program-pane-height-in-lines :initarg :height-in-lines :initform NIL))
  ;(:accessor-prefix program-pane-)
  )

(defclass program-query-values-pane (program-pane)
     ((query-values-function :accessor program-query-values-pane-query-values-function :initarg :query-values-function
			     :INITFORM NIL)
      (program-memo :accessor program-query-values-pane-program-memo :initarg :program-memo :INITFORM NIL)
      (query-entry-table :accessor program-query-values-pane-query-entry-table :initarg :query-entry-table :INITFORM NIL))
  ;(:accessor-prefix program-query-values-pane-)
  )

(defclass program-title-pane (program-pane)
     ((redisplay-string :accessor program-title-pane-redisplay-string :initarg :redisplay-string :initform NIL)))

(defmethod print-program-title-pane ((pane program-title-pane) stream &REST ignore)
  (declare (ignore ignore))
  (format stream "#<Title Pane ~S>" (program-title-pane-redisplay-string pane)))

(defclass program-display-pane (program-pane) ())

(defclass program-command-menu (program-pane)
  ((level :accessor program-command-menu-level :initarg :level :initform NIL)
   (items :accessor program-command-menu-items :initarg :items :initform NIL)
   (orientation :accessor program-command-menu-orientation :initarg :orientation :initform NIL))
  ;(:accessor-prefix program-command-menu-)
  )


(defclass program-frame (presentation-window)
  ((program-framework-instance :accessor program-frame-program-framework-instance :initarg :program-framework-instance
			       :initform NIL))
  ;(:accessor-prefix program-frame-)
  )

(defmethod print-program-pane ((pane program-pane) stream &REST ignore)
;  (declare (ignore pane ignore))
  (declare (ignore ignore))
  (format stream "#<Program Pane>"))


(defmethod print-display-pane ((pane program-display-pane) stream &REST ignore)
  (declare (ignore ignore))
  (format stream "#<Display Pane ~A>" (window-name pane)))

(defmethod print-program-command-menu ((pane program-command-menu) stream &REST ignore)
  (declare (ignore ignore))
  (format stream "#<Command Menu Pane ~S>" (program-command-menu-level pane)))

(defmethod print-program-frame ((frame program-frame) stream &rest args)
  (declare (ignore args))
  (format stream "#<Program Frame ~A>" (program-frame-program-framework-instance frame)))


(defstruct (memo (:print-function print-memo))
  stream cache-value unique-id id-test continuation
  (displayed-p NIL)
  start-cursor-x start-cursor-y end-cursor-x end-cursor-y presentation
  inferiors offset-p width height)

(defstruct (table-memo (:include memo) (:print-function print-table-memo))
  (x-offset 0 :type fixnum)
  (y-offset 0 :type fixnum))

(defstruct (row-memo (:include memo) (:print-function print-row-memo))
  (y-offset 0 :type fixnum))

(defstruct (cell-memo (:include memo) (:print-function print-cell-memo))
  (x-offset 0 :type fixnum)
  (y-offset 0 :type fixnum)
  align-x align-y
  )

#+(or LCL3.0 (and APOLLO DOMAIN/OS))
(defstruct-simple-predicate table-memo table-memo-p)

;; 4/30/90 (SLN) Added Symbolics since defstruct automatically creates one.
;; 6/27/90 (SLN) so does Allegro; so should every other lisp, so
;; I'm commenting this out.
;#-(or Symbolics LCL3.0 (and APOLLO DOMAIN/OS))
;(defmacro table-memo-p (stream)
;  `(eq (lisp:type-of ,stream) 'TABLE-MEMO))

#+(or LCL3.0 (and APOLLO DOMAIN/OS))
(defstruct-simple-predicate row-memo row-memo-p)

;; 4/30/90 (SLN) Added Symbolics since defstruct automatically creates one.
;; 6/27/90 (SLN) so does Allegro; so should every other lisp, so
;; I'm commenting this out.
;#-(or Symbolics LCL3.0 (and APOLLO DOMAIN/OS))
;(defmacro row-memo-p (stream)
;  `(eq (lisp:type-of ,stream) 'ROW-MEMO))


#+(or LCL3.0 (and APOLLO DOMAIN/OS))
(defstruct-simple-predicate cell-memo cell-memo-p)

;; 4/30/90 (SLN) Added Symbolics since defstruct automatically creates one.
;; 6/27/90 (SLN) so does Allegro; so should every other lisp, so
;; I'm commenting this out.
;#-(or Symbolics LCL3.0 (and APOLLO DOMAIN/OS))
;(defmacro cell-memo-p (stream)
;  `(eq (lisp:type-of ,stream) 'CELL-MEMO))


(defun print-memo (memo stream &REST ignore)
  (format stream "#<Memo ~A ~A>"
	  (memo-unique-id memo)
	  (memo-cache-value memo)))

(defun print-cell-memo (memo stream &REST ignore)
  (format stream "#<Cell Memo ~A ~A>"
	  (memo-unique-id memo)
	  (memo-cache-value memo)))

(defun print-row-memo (memo stream &REST ignore)
  (format stream "#<Row Memo ~A ~A>"
	  (memo-unique-id memo)
	  (memo-cache-value memo)))

(defun print-table-memo (memo stream &REST ignore)
  (format stream "#<Table Memo ~A ~A>"
	  (memo-unique-id memo)
	  (memo-cache-value memo)))




(defstruct (presentation-blip (:predicate presentation-blip-p))
  presentation
  object
  options
  type
  mouse-char x y)




(defclass program-framework
	  ()
     ((name :accessor program-framework-name :initarg :name :INITFORM NIL)
      (pretty-name :accessor program-framework-pretty-name :initarg :pretty-name :INITFORM NIL)
      (command-table :accessor program-framework-command-table :initarg :command-table :INITFORM NIL)
      (command-evaluator :accessor program-framework-command-evaluator :initarg :command-evaluator :INITFORM NIL)
      (top-level :accessor program-framework-top-level :initarg :top-level :INITFORM NIL)
      (select-key :accessor program-framework-select-key :initarg :select-key :INITFORM NIL)
      (panes :accessor program-framework-panes :initarg :panes :INITFORM NIL)
      (configurations :accessor program-framework-configurations :initarg :configurations :INITFORM NIL)
      (state-variables :accessor program-framework-state-variables :initarg :state-variables :INITFORM NIL)
      (selected-pane :accessor program-framework-selected-pane :initarg :selected-pane :INITFORM NIL)
      (query-io-pane :accessor program-framework-query-io-pane :initarg :query-io-pane :INITFORM NIL)
      (terminal-io-pane :accessor program-framework-terminal-io-pane :initarg :terminal-io-pane :INITFORM NIL)
      (label-pane :accessor program-framework-label-pane :initarg :label-pane :INITFORM NIL)
      ;(menu-templates :accessor program-framework-menu-templates :initarg :menu-templates :INITFORM NIL)
      (program-instances :accessor program-framework-program-instances :initarg :program-instances :INITFORM NIL)
      (number-created :accessor program-framework-number-created :initarg :number-created :INITFORM 0))
  ;(:accessor-prefix program-framework-)
  )
  

;Loading DUCKABUSH:/usr/ducky2/pub-lisp/ew/code/macros.bin.~newest~ into package EXPRESS-WINDOWS
;Error: the method and generic function differ in whether they accept
;       rest or keyword arguments.
;
;PCL:ADD-ARG-INFO
;   Arg 0 (PCL:GENERIC-FUNCTION): #<Standard-Generic-Function PRINT-OBJECT (8) 360245334>
;   Arg 1 (PCL:METHOD): #<Standard-Method NIL (PROGRAM-FRAMEWORK T) 61125134>
;   Arg 2 (PCL:ARG-INFO): #<ART-Q-6 344545212>
;(defmethod print-object ((self program-framework) stream &REST ignore)
(defmethod print-object ((self program-framework) stream)
;  (declare (ignore ignore))
  (format stream "#<Framework ~A>" (program-framework-pretty-name self)))

(defclass basic-frame-program
	  ()
     ((frame :accessor program-frame :initarg :frame :INITFORM NIL))
  ;(:accessor-prefix program-)
  )

(defclass program-framework-instance
	  ()
     ((program-number :accessor program-framework-instance-program-number :initarg :program-number :INITFORM NIL)
       (program-framework :accessor program-framework-instance-program-framework :initarg :program-framework :INITFORM NIL)
       (configuration :accessor program-framework-instance-configuration :initarg :configuration :INITFORM NIL)
       (root-window :accessor program-framework-instance-root-window :initarg :root-window :INITFORM NIL)
       (current-panes :accessor program-framework-instance-current-panes :initarg :current-panes :INITFORM NIL)
       (windows :accessor program-framework-instance-windows :initarg :windows :INITFORM NIL)
       (process :accessor program-framework-instance-process :initarg :process :INITFORM
		(or #+lucid lcl::*current-process* #+EXCL MP::*CURRENT-PROCESS* nil))
       )
  ;(:accessor-prefix program-framework-instance-)
  )

;Loading DUCKABUSH:/usr/ducky2/pub-lisp/ew/code/macros.bin.~newest~ into package EXPRESS-WINDOWS
;Error: the method and generic function differ in whether they accept
;       rest or keyword arguments.
;
;PCL:ADD-ARG-INFO
;   Arg 0 (PCL:GENERIC-FUNCTION): #<Standard-Generic-Function PRINT-OBJECT (8) 360245334>
;   Arg 1 (PCL:METHOD): #<Standard-Method NIL (PROGRAM-FRAMEWORK T) 61125134>
;   Arg 2 (PCL:ARG-INFO): #<ART-Q-6 344545212>
;(defmethod print-object ((self program-framework-instance) stream &REST ignore)
(defmethod print-object ((self program-framework-instance) stream)
;  (declare (ignore ignore))
  (format stream "#<Framework Instance ~A>"
	  (program-framework-pretty-name
	    (program-framework-instance-program-framework self))))



(defstruct (input-editor-state (:print-function print-input-editor-state))
  (cursorpos 0 :TYPE FIXNUM)
  (scan-pointer 0 :TYPE FIXNUM)
  (string (make-array '(100) :ADJUSTABLE T :ELEMENT-TYPE 'string-char :FILL-POINTER 0)
	  :TYPE string)
  (modified-p NIL)
  (input-x 0 :type fixnum)
  (input-y 0 :type fixnum)
  (prompt-x 0 :type fixnum)
  (prompt-y 0 :type fixnum)
  (unread-characters NIL)
  (noise-strings NIL)
  (presentation-blips NIL)
  (input-history NIL)
  (input-history-yank-pointer NIL)
  )

(defvar *Rescanning* NIL)

(defun print-input-editor-state (state stream &REST args)
  (declare (ignore args))
  (format stream "#<Input Editor State ~S Cursorpos ~D Scan ~D ~:[not~;Currently~] scanning.>"
	  (input-editor-state-string state)
	  (input-editor-state-cursorpos state)
	  (input-editor-state-scan-pointer state)
	  *Rescanning*))

(defstruct input-editor-history-item
  string noise-strings presentation-blips)

(defstruct query-history-item
  type
  object
  input ;; input is an input-editor-history-item
  )

(defvar *Query-History* NIL)

(defmacro with-input-editing ((&OPTIONAL stream keyword) &BODY body)
  (declare (ignore keyword))
  (if (member stream '(NIL T)) (setq stream '*Standard-Input*))
  `(with-input-editing-internal ,stream #'(lambda () . ,body)))


(defmacro with-input-editing-options (options &BODY body)
  (let ((result NIL))
    (dolist (option options)
      (let ((option-type (first option)))
	(let ((option-name (if (consp option-type) (first option-type) option-type))
	      (override (and (consp option-type) (member :override (cdr option-type)))))
	  (if override
	      (setq result
		    (cons `(push (list ,option-name . ,(cdr option)) *Input-Editing-Options*)
			  result))
	      (setq result
		    (cons `(unless (assoc ,option-name *Input-Editing-Options*)
			     (push (list ,option-name . ,(cdr option))
				   *Input-Editing-Options*)) result))))))
    `(let ((*Input-Editing-Options* *Input-Editing-Options*))
       ,@result
       . ,body)))






;;; ****************************************************************************************
;;; Start Definition of PRESENTATION VARIABLES.
;;; ****************************************************************************************

(defvar *Record-Presentations-P* T)

(defvar *Inside-Record-Presentations-P* NIL)

(defvar *Immediately-Inside-Woap* NIL "Dynamically Bound to T when inside a 
display-as.  Used to determine if a memo is
inside of a display-as.")

(defvar *Presentation-Records* NIL)
(defvar *Presentation-Records-Last-Cons* NIL)

(defvar *Record-Inferior-Presentations-P* T)

(defvar *Presentations* NIL)


(defvar *Defer-Redraw-Presentations* NIL)
(defvar *Deferred-Redraw-Presentations* NIL)



(defvar *Handlers* NIL)

(defvar *Handlers-Table* NIL)

(defvar *No-Type-Handlers* NIL)

(defvar *No-Type-Handlers-Table* NIL)

(defvar *No-Type*)


(defvar *Memos* NIL)

(defvar *Memo* NIL)

(defvar *Inferior-Memo-P* NIL)


(defvar *Redisplaying-Memo-P* NIL)


(defvar *Memo-Presentation-Records* NIL)
(defvar *Memo-Presentation-Records-Previous-Cons* NIL)

(defvar *Memo-Presentation* NIL)

#+ignore
(pushnew ':METER-CONSING *features*)


(defvar *Stipple-Arrays* NIL)

(defmacro defstipple (name (height width) (&key pretty-name gray-level tv-gray) patterns)
  `(defvar ,name (defstipple-internal ',name ,height ,width
		   ,pretty-name ,gray-level ,tv-gray ',patterns)))


(defstruct stipple-pattern
  name width height
  black-image ;; normal image
  white-image ;; used on displays with black color pixel value of 0.
  pixmaps)


(defvar *Warn-Unimplemented-Args* NIL)

(defmacro warn-unimplemented-args (&rest arg-default-values)
  `(let ((error-ctrl-string "~&Unimplemented Arg ~A is used."))
     (when *Warn-Unimplemented-Args*
       . ,(do ((sub-list arg-default-values (cddr sub-list))
	       (clauses NIL))
	      ((null sub-list) clauses)
	    (push `(unless (equal ,(first sub-list) ,(second sub-list))
		     (format T error-ctrl-string ',(first sub-list)))
		  clauses)))))


(defmacro non-rotating-transform-p (transform)
  `(or (not ,transform)
       (zerop (second ,transform))
       (zerop (third ,transform))))




(defmacro setup-standard-output-stream-arg (stream)
  `(when (member ,stream '(T NIL))
     (setq ,stream '*standard-output*)))

(defvar *Fill-Resource* NIL)

(defstruct (fill-object (:TYPE LIST))
  column characters line-break initially)


(defmacro with-output-filling ((&OPTIONAL stream &KEY fill-column (fill-characters ''(#\Space))
					 after-line-break initially-too
					 &ALLOW-OTHER-KEYS)
			      &BODY body)
  (setup-standard-output-stream-arg stream)
  `(filling-output-internal ,stream ,fill-column ,fill-characters ,after-line-break
			    ,initially-too
			    #'(lambda (,stream)
				;; 6/27/90 (sln) Added the ",stream" to
				;; suppress warnings about stream not being
				;; used.
				,stream
				. ,body)))



(defmacro formatting-list-element ((&OPTIONAL stream) &BODY body)
  (setup-standard-output-stream-arg stream)
  `(formatting-list-element-internal
     ,stream #'(lambda (,stream)  . ,body)))

(defmacro formatting-list ((&OPTIONAL stream
				      &KEY (separator ", ") finally if-two
				      filled after-line-break conjunction
				      dont-snapshot-variables)
			   &BODY body)
  (declare (ignore dont-snapshot-variables))
  (setup-standard-output-stream-arg stream)
  `(formatting-list-internal ,stream ,separator ,finally ,if-two
			     ,filled ,after-line-break ,conjunction
			     #'(lambda (,stream) . ,body)))

(defmacro char-unit-width (window)
  `(char-size-from-font #\Space (window-font ,window)))

(defmacro char-unit-height (window)
  `(multiple-value-bind (width height)
       (char-size-from-font #\Space (window-font ,window))
     (declare (ignore width))
     height))




(defvar *Parsing-Or-Type-P* NIL "Bound to T during parsing of an OR so that
we don't get real parsing errors on individual clauses parsed.")


(defmacro make-table-from-generated-sequence
	  ((&OPTIONAL stream &KEY (inter-row-spacing 0)
		      inter-column-spacing
		      (row-wise t) output-row-wise n-rows
		      n-columns equalize-column-widths inside-width
		      inside-height max-width max-height
		      dont-snapshot-variables) &BODY body)
  (warn-unimplemented-args dont-snapshot-variables NIL
			   inside-width NIL inside-height NIL)
  (setup-standard-output-stream-arg stream)
  `(make-table-from-generated-sequence-internal
     ,stream ,inter-row-spacing ,inter-column-spacing
     ,row-wise ,output-row-wise ,n-rows
     ,n-columns ,equalize-column-widths ,inside-width
     ,inside-height ,max-width ,max-height
     ',(gensym)
     #'(lambda (,stream)
	 . ,body)))




(defun my-read-from-string (string &OPTIONAL (eof-errorp T) eof-value
			    &KEY (start 0) end preserve-whitespace)
  (if eof-errorp
      (read-from-string string eof-errorp eof-value :start start :end end
			:preserve-whitespace preserve-whitespace)
      (if (not (lisp-expression-p string start end))
	  eof-value
	  (let (result (success NIL))
	    (#+symbolics scl:catch-error
	     #-symbolics progn ;;user::ignore-errors
	     (progn
	       (setq result
		     (multiple-value-list
		       (read-from-string
			 string eof-errorp eof-value :start start :end end
			 :preserve-whitespace preserve-whitespace)))
	       (setq success T))
	     NIL)
	    (if success
		(values-list result)
		eof-value)))))

(defun lisp-expression-p (string &OPTIONAL (start 0) end)
  #.(fast)
  (declare (fixnum start) (string string))
  (setq end (length string))
  ;; simple version that only handles parens and double-quotes.
  (let ((parens-to-balance 0)
	(in-quote-p NIL))
    (declare (fixnum parens-to-balance))
    (do ((x start (%1+ x)))
	((%= x end) (and (not in-quote-p) (zerop parens-to-balance)))
      (declare (fixnum x))
      (let ((char (aref string x)))
	(if in-quote-p
	    (when (eql char #\")
	      (setq in-quote-p NIL))
	    (case (aref string x)
	      (#\" (setq in-quote-p T))
	      (#\( (incf parens-to-balance))
	      (#\) (decf parens-to-balance)
	       (when (< parens-to-balance 0)
		 (return NIL)))
	      ((#\CR #\Space #\TAB)
	       (when (%= parens-to-balance 0) (return T)))
	      (T NIL)))))))


(defmacro add-to-end-of-list (item list last-cons-of-list)
  `(if ,last-cons-of-list
      (progn (nconc ,last-cons-of-list (list ,item))
	     (setq ,last-cons-of-list (cdr ,last-cons-of-list)))
      (progn
	(setq ,list (list ,item))
	(setq ,last-cons-of-list ,list))))

(defmacro add-items-to-end-of-list (items list last-cons-of-list)
  `(if ,last-cons-of-list
      (progn (nconc ,last-cons-of-list (copy-list ,items))
	     (setq ,last-cons-of-list (last ,last-cons-of-list)))
      (progn
	(setq ,list (copy-list ,items))
	(setq ,last-cons-of-list (last ,list)))))



(defmacro with-type-decoded ((type-name-var &OPTIONAL data-args-var display-args-var)
			     type &BODY body)
  `(multiple-value-bind (,type-name-var . ,(if data-args-var
					       `(,data-args-var
						 . ,(if display-args-var
							`(,display-args-var)))))
       (decode-type ,type)
     . ,body))

(defun type-name (type &OPTIONAL ignore)
  #.(fast)
  (cond ((symbolp type) type)
	((and (consp type)
	      (consp (first type)))
	 (first (first type)))
	((consp type)
	 (first type))))

(defmacro querying-values ((&OPTIONAL (stream '*Query-Io*)
				      &REST other-args
				       &KEY own-window label (near-mode ''(:mouse))
				       (display-exit-boxes T)
				       temporary-p resynchronize-every-pass
				       initially-select-query-identifier
				       (character-style ''(:fix :roman :large))
				       &ALLOW-OTHER-KEYS)
				&BODY body)
  (when (member stream '(T NIL)) (setq stream '*query-io*))
  `(querying-values-internal ,stream ,own-window ,label ,near-mode ,display-exit-boxes
			      ,temporary-p ,initially-select-query-identifier
			      #'(lambda (,stream)
				,@body)
			      ,(getf other-args :MENU-DEFINER-P)
			      ,character-style
			      ,resynchronize-every-pass))







(defvar *Standard-Completion-Delimiters* '(#\Space #\-))


(defvar *Token-Delimiter-Chars* NIL)
(defmacro with-token-delimiters ((additional-characters &KEY override) &BODY body)
  (let ((chars (gensym "CHARS")))
    `(let ((,chars ,additional-characters))
       (unless (listp ,chars) (setq ,chars (list ,chars)))
       (let ((*Token-Delimiter-Chars*
	       ,(if (not override)
		    `(cons ,chars *Token-Delimiter-Chars*)
		    `(if ,override (list ,chars) (cons ,chars *Token-Delimiter-Chars*)))))
	 . ,body))))

(defvar *Activation-Chars* '((#\Newline #\CR #+symbolics #\END)))
(defmacro with-activation-chars ((additional-characters &KEY override) &BODY body)
  (let ((chars (gensym "CHARS")))
    `(let ((,chars ,additional-characters))
       (unless (listp ,chars) (setq ,chars (list ,chars)))
       (let ((*Activation-Chars*
	       ,(if (not override)
		    `(cons ,chars *Activation-Chars*)
		    `(if ,override (list ,chars) (cons ,chars *Activation-Chars*)))))
	 . ,body))))



;;; Bound to the Presentation that has the printing of an error message during a parse-error
;;; in the command processor.  It is up to the editor function to erase the message
;;; when another character is typed.
(defvar *Parse-Error-Presentation* NIL)

(defvar *Debug* NIL)



(defmacro convert-screen-x-coord-to-window-coord-macro (window x)
  `(%- (%+ ,x (window-scroll-x-offset ,window)) (window-inside-left ,window)))

(defmacro convert-screen-y-coord-to-window-coord-macro (window y)
  `(%- (%+ ,y (window-scroll-y-offset ,window)) (window-inside-top ,window)))


;;; tracking mouse should only call mouse motion function when mouse is in stream input.


(defmacro tracking-mouse ((&OPTIONAL stream
				     &KEY (whostate "Track Mouse")
				     (start-x '#.*mouse-x*)
				     (start-y '#.*mouse-y*)
				     (who-line-documentation-string NIL)
				     (consume-extra-motion-events-p NIL))
			  &BODY clauses &AUX (compiler-bug-variable (gensym)))
  (setup-standard-output-stream-arg stream)
  (let ((local-functions
	  (mapcar #'(lambda (clause)
		      (let ((type (first clause))
			    (arglist (second clause))
			    (body (cddr clause))
			    (name (gensym)))
			(list type name
			      `#'(lambda ,arglist
				   ;; 5/1/90 (sln) suppress compiler warnings about unused vars
				   ,@(remove t arglist :test #'(lambda (ignore a2) (member a2 lambda-list-keywords))) nil
				   (incf ,compiler-bug-variable)
				   #+ignore (dformat "Entering ~A." ,type)
				   . ,body))))
		  clauses)))
    (let ((old-mouse-x (gensym "OLD-MOUSE-X"))
	  (old-mouse-y (gensym "OLD-MOUSE-Y"))
	  (old-window-mouse-x (gensym "OLD-WINDOW-MOUSE-X"))
	  (old-window-mouse-y (gensym "OLD-WINDOW-MOUSE-Y"))
	  (old-mouse-buttons (gensym "OLD-MOUSE-BUTTONS"))
	  (new-mouse-x (gensym "NEW-MOUSE-X"))        (new-mouse-y (gensym "NEW-MOUSE-Y"))
	  (new-window-mouse-x (gensym "NEW-WINDOW-MOUSE-X"))
	  (new-window-mouse-y (gensym "NEW-WINDOW-MOUSE-Y"))
	  (new-mouse-buttons (gensym "NEW-MOUSE-BUTTONS"))
	  (character (gensym "CHARACTER"))
	  (documentation (gensym "DOCUMENTATION")))
      ;`(flet ,(mapcar #'third local-functions)
      `(let ((*Consume-Extra-Mouse-Motion-Events-P* ,consume-extra-motion-events-p)
	     (,old-mouse-x ,start-x)
	     (,old-mouse-y ,start-y)
	     (,old-window-mouse-x 0) ;; value just so my declare is accurate.
	     (,old-window-mouse-y 0) ;; value just so my declare is accurate.
	     (,new-window-mouse-x 0) ;; value just so my declare is accurate.
	     (,new-window-mouse-y 0) ;; value just so my declare is accurate.
	     (,old-mouse-buttons #.*Mouse-Buttons*)
	     (,documentation *Mouse-Documentation-String*)
	     (,compiler-bug-variable 0))
	 (declare (fixnum ,old-mouse-x ,old-mouse-y ,old-window-mouse-x ,old-window-mouse-y
			  ,new-window-mouse-x ,new-window-mouse-y ,old-mouse-buttons
			  ,compiler-bug-variable))
	 #+X
	 (multiple-value-setq (,old-window-mouse-x ,old-window-mouse-y)
	   (convert-screen-coords-to-window-coords ,stream ,old-mouse-x ,old-mouse-y))
	 (unwind-protect
	     (do ()(())
	       (set-mouse-documentation-string
		 ,(if (assoc :who-line-documentation-string local-functions)
		      `(funcall ,(third (assoc :who-line-documentation-string
					       local-functions)))
		      who-line-documentation-string))
	       #+ignore
	       (dformat "Mouse Window ~A Stream ~A." *Mouse-Window* ,stream)
	       (process-wait ,whostate
			    #'(lambda (stream old-x old-y old-buttons)
				(declare (fixnum old-x old-y))
				(or ;;initial-p
				    (not (%= old-x #.*Mouse-X*))
				    (not (%= old-y #.*Mouse-Y*))
				    (not (%= old-buttons #.*Mouse-Buttons*))
				    (listen-any stream)
				    ;; (not (= old-bits (the fixnum (mouse-chord-shifts))))
				    (not (%= old-x #.*Mouse-X*))
				    (not (%= old-y #.*Mouse-Y*))
				    (not (%= old-buttons #.*Mouse-Buttons*))))
			    ,stream ,old-mouse-x ,old-mouse-y ,old-mouse-buttons)
	       ;; (princ "foo" *Interaction-Window*)
	       (let ((,new-mouse-x #.*mouse-x*)
		     (,new-mouse-y #.*mouse-y*)
		     (,new-mouse-buttons #.*mouse-buttons*))
		 (declare (fixnum ,new-mouse-x ,new-mouse-y ,new-mouse-buttons))
		 #+ignore
		 (dformat "X ~D Y ~D B ~D~%" ,new-mouse-x ,new-mouse-y ,new-mouse-buttons)
		 #+CLX
		 (multiple-value-setq (,new-window-mouse-x ,new-window-mouse-y)
		   (convert-screen-coords-to-window-coords
		     ,stream ,new-mouse-x ,new-mouse-y))
		 (cond ((listen-any ,stream 0)
			(let ((,character (read-any ,stream)))
			  (if (and (listp ,character)
				   (eq (first ,character) :MOUSE-CLICK))
			      (if (eq *mouse-window* ,stream)
				  ,(cond
				     ((assoc :PRESENTATION-CLICK local-functions)
				      `(funcall
					 ,(third (assoc :PRESENTATION-CLICK
							local-functions))
					 (find-presentation
					   ,stream
					   (convert-screen-x-coord-to-window-coord-macro
					     ,stream (fourth ,character))
					   (convert-screen-y-coord-to-window-coord-macro
					     ,stream (fifth ,character)))
					 (second ,character)))
				     ((assoc :MOUSE-CLICK local-functions)
				      `(funcall
					 ,(third (assoc :MOUSE-CLICK
							local-functions))
					 (second ,character)
					 (convert-screen-x-coord-to-window-coord-macro
					   ,stream (fourth ,character))
					 (convert-screen-y-coord-to-window-coord-macro
					   ,stream (fifth ,character))))
				     (T NIL))
				  ;; check for scrolling too guy.
				  (if (lisp:typep (third ,character) 'scroll-bar)
				      (handle-scrolling ,character (third ,character))))
			      ,(if (assoc :KEYBOARD local-functions)
				   `(funcall ,(third (assoc :KEYBOARD
							    local-functions))
					     ,character)
				   NIL))))
		       ((and (eq *Mouse-Window* ,stream)
			     (or (not (%= ,old-window-mouse-x ,new-window-mouse-x))
				 (not (%= ,old-window-mouse-y ,new-window-mouse-y))))
			(if (zerop (the fixnum ,new-mouse-buttons))
			    ,(cond ((assoc :PRESENTATION local-functions)
				    `(funcall ,(third (assoc :PRESENTATION
							     local-functions))
					      (find-presentation
						,new-window-mouse-x ,new-window-mouse-y)))
				   ((assoc :MOUSE-MOTION local-functions)
				    `(funcall ,(third (assoc :MOUSE-MOTION
							     local-functions))
					      ,new-window-mouse-x ,new-window-mouse-y))
				   (T NIL))
			    ,(cond ((assoc :PRESENTATION-HOLD local-functions)
				    `(funcall ,(third (assoc :PRESENTATION-HOLD
							     local-functions))
					      (find-presentation
						,new-window-mouse-x ,new-window-mouse-y)))
				   ((assoc :mouse-motion-hold local-functions)
				    `(funcall ,(third (assoc :MOUSE-MOTION-HOLD
							     local-functions))
					      ,new-window-mouse-x ,new-window-mouse-y))
				   (T NIL))))
		       ((and (eq *Mouse-Window* ,stream)
			     (and (zerop (the fixnum ,new-mouse-buttons))
				  (not (zerop (the fixnum ,old-mouse-buttons)))))
			;; (print "released" *Interaction-Window*)
			,(if (assoc :release-mouse local-functions)
			     `(funcall ,(third (assoc :RELEASE-MOUSE
						      local-functions)))
			     NIL)))
		 (setq ,old-mouse-x ,new-mouse-x
		       ,old-mouse-y ,new-mouse-y
		       ,old-mouse-buttons ,new-mouse-buttons
		       ,old-window-mouse-x ,new-window-mouse-x
		       ,old-window-mouse-y ,new-window-mouse-y)))
	   (set-mouse-documentation-string ,documentation))))))


(defvar *Disable-Expression-Presentation-P* NIL)




(defmacro with-presentations-disabled (&BODY body)
  `(let ((*Record-Presentations-P* NIL))
     . ,body))

(defmacro with-presentations-enabled (&BODY body)
  `(let ((*Record-Presentations-P* T))
     . ,body))

(defmacro with-output-recording-disabled ((stream) &BODY body)
  (declare (ignore stream))
  `(let ((*Record-Presentations-P* NIL))
     . ,body))

(defmacro with-output-to-presentation-recording-string ((stream &OPTIONAL string) &BODY body)
  (declare (ignore string))
  `(let ((,stream (make-presentation-recording-string)))
     ,@body
     ,stream))



(defvar *Input-Editing-Options* NIL)


(defvar *Command-Tables* NIL)
(defvar *Last-Command-Values* NIL)
(defvar *Command-Table* NIL)
(defvar *Full-Command-Prompt* "> ")
(defvar *Default-Blank-Line-Mode* :REPROMT)


(defvar *Parse-Type* NIL)


(defvar *Query-Menu-Windows* NIL)


(defvar *Menu-Choose-Resource* NIL)

(defvar *Inside-Input-Editor-P* NIL)


;;; Bound to a Menu if it is a temporary window. i.e. it should disappear if the mouse moves
;;; off of it.
(defvar *Momentary-Menu* NIL)

(defvar *Momentary-Menu-Hysteresis-Pixels* 16.)


(defmacro inside-window-p (window x1 y1 &OPTIONAL x2 y2)
  (if x2 ;; left, top , right, and bottom
      `(not (or (%< ,x2 0)
		(%< ,y2 ,0)
		(%> ,x1 (window-inside-right ,window))
		(%> ,y1 (window-inside-bottom ,window))))
      `(and (%<= 0 ,x1 (window-inside-right ,window))
	    (%<= 0 ,y1 (window-inside-bottom ,window)))))




;(defmacro with-merged-transform (transform-variable
;				 (scale-x scale-y rotation translate transform)
;				 &BODY body)
;  `(multiple-value-bind (transform)))


(defmacro with-output-truncation ((&OPTIONAL stream &REST options) &BODY body)
  (setup-standard-output-stream-arg stream)
  (let ((old-eop (gensym "OLD-EOP"))
	(old-eol (gensym "OLD-EOL"))
	(new-vertical (gensym "NEW-VERTICAL"))
	(new-horizontal (gensym "NEW-HORIZONTAL"))
	(vertical (getf options :VERTICAL))
	(horizontal (getf options :HORIZONTAL)))
    (when (and (not vertical) (not horizontal))
      (setq vertical T horizontal T))
    `(let ((,old-eop (window-end-of-page-mode ,stream))
	   (,old-eol (window-end-of-line-mode ,stream))
	   (,new-vertical ,vertical)
	   (,new-horizontal ,horizontal))
       (unwind-protect
	   (progn
	     ;; setup new truncation
	     (when ,new-vertical
	       (setf (window-end-of-page-mode ,stream) :TRUNCATE))
	     (when ,new-horizontal
	       (setf (window-end-of-line-mode ,stream) :TRUNCATE))
	     . ,body)
	 (setf (window-end-of-page-mode ,stream) ,old-eop
	       (window-end-of-line-mode ,stream) ,old-eol)))))


(defmacro with-indenting-output ((stream indentation) &BODY body)
;  Compiling WITH-INDENTING-OUTPUT
;  Warning: variable STREAM is used yet it was declared ignored
;  (declare (ignore stream))
  (setup-standard-output-stream-arg stream)
  (let ((values (gensym "VALUES")))
    `(let ((,values NIL))
       (unwind-protect
	   (progn
	     (push ,indentation (window-indentation ,stream))
	     (setq ,values (multiple-value-list (progn . ,body))))
	 (pop (window-indentation ,stream)))
       (values-list ,values))))

(defmacro with-border ((&OPTIONAL
			 stream
			 &KEY
			 (shape :rectangle)
			 (thickness 1) (margin 1) (pattern t)
			 (gray-level 1) opaque filled
			 (alu :DRAW) (move-cursor T)
			 width height label (label-position :bottom)
			 label-separator-line
			 (label-separator-line-thickness 1)
			 (label-alignment :left))
		       &BODY body)
  (warn-unimplemented-args label NIL label-position :bottom width NIL height NIL
			   gray-level 1 opaque NIL filled NIL pattern t margin 1
			   label-separator-line NIL
			   label-separator-line-thickness 1
			   label-alignment :left)
  (setup-standard-output-stream-arg stream)
  `(with-border-internal
     ,stream ,filled ,alu ,thickness
     ,shape ,move-cursor
     #'(lambda (,stream) . ,body)))




(defun replace-strings-with-princs (body stream)
  (setq body (mapcar #'(lambda (item)
			 (if (stringp item)
			     `(princ ,item ,stream)
			     item))
		     body)))




(defmacro structure-type (object)
  #+lucid `(sys:structure-type ,object)
  #+symbolics `(let ((object ,object))
		 (IF (scl:ARRAY-HAS-LEADER-P object)
		     (scl:ARRAY-LEADER object 1)
		     (AREF object 0)))
  #-(or symbolics lucid) `(type-of ,object))


(defvar *Running-Menu-Definer-P* NIL)


(defmacro inhibit-scrolling ((window) &BODY body)
  `(progn
     (let ((*Inhibit-Scroll-Bar-P* T))
       . ,body)
     (draw-margins ,window)))


(defvar *Command-Menu-Test-Phase* NIL)


(defmacro memoize ((&KEY value (stream '*Standard-Output*)
			 (value-test '#'EQL) copy-value id (id-test '#'EQL) memo-type
			 once-only
			 &ALLOW-OTHER-KEYS)
		   &BODY body)
  (when (member stream '(NIL T)) (setq stream '*Standard-Output*))
  (if (null id-test) (setq id-test '#'EQL))
  (if (null value-test) (setq value-test '#'EQL))
  `(memoize-internal ,stream ,memo-type
		     ,(if once-only T
			  (if (null value) ''NO-CACHE-VALUE
			      value))
		     ,id
		     ,copy-value
		     ,id-test ,value-test
		     #'(lambda (,stream) . ,body)))





(defvar *Optimizing-Memo* T) ;; bind to NIL when you don't want optimization of bitblts in the
;; y direction.

(defmacro memo ((&OPTIONAL (stream '*standard-output*) (optimize T)) &BODY body)
  `(memoizing-internal #'(lambda (,stream)
			   (let ((*Optimizing-Memo* (and *Optimizing-Memo* ,optimize)))
			     . ,body))))


(defmacro independently-redisplayable-format (stream format-string &REST format-args)
  `(memoize (:stream ,stream)
     (format ,stream ,format-string . ,format-args)))



(defmacro make-table ((&OPTIONAL (stream '*Standard-Output*)
				 &KEY equalize-column-widths extend-width
				 extend-height (row-spacing 0)
				 column-spacing	;(scl:send stream char-width)
				 multiple-columns
				 (multiple-column-column-spacing)
				 equalize-multiple-column-widths
				 output-multiple-columns-row-wise
				 &ALLOW-OTHER-KEYS)
		      &BODY body)
  (warn-unimplemented-args equalize-multiple-column-widths NIL
			   output-multiple-columns-row-wise NIL)
  (setup-standard-output-stream-arg stream)
  `(make-table-internal ,stream ,equalize-column-widths ,row-spacing
			      ,extend-width ,extend-height ,column-spacing
			      ,multiple-columns ,multiple-column-column-spacing
			      ',(gensym)
			      #'(lambda (,stream) . ,body)))


(defvar *Fake-Window* NIL)
(defvar *Table-Presentations*)
(defvar *Row-Presentations*)


(defmacro replace-strings-with-entries (body)
  `(setq body (mapcar #'(lambda (item)
			  (if (stringp item)
			      `(entry (,stream) ,item)
			      item))
		      ,body)))

(defmacro table-row ((&OPTIONAL (stream '*Standard-Output*)
				&KEY single-column id (id-test '#'eql) (once-only NIL)
				&ALLOW-OTHER-KEYS)
		     &BODY body)
  (warn-unimplemented-args single-column NIL)
  (setup-standard-output-stream-arg stream)
  (let ((args NIL))
    (when (and (consp body)
	       (consp (first body))
	       (eq 'MEMOIZE (first (first body)))
	       (null (cdr body)))
      (setq args (second (first body))
	    body (cddr (first body))))
    ;; now replace any strings in body with appropriate write-strings around them
    (replace-strings-with-entries body)
    (if id
	`(memoize (:STREAM ,stream :ID ,id :ID-TEST ,id-test ,@(and once-only '(:VALUE T)))
	   (memoize (:STREAM ,stream :MEMO-TYPE :row . ,args)
	      . ,body))
	`(memoize (:STREAM ,stream :MEMO-TYPE :row ,@(and once-only '(:VALUE T)) . ,args)
	   . ,body))))

(defmacro row ((&OPTIONAL (stream '*Standard-Output*)
			  &KEY single-column id (id-test '#'eql) (once-only NIL)
			  &ALLOW-OTHER-KEYS)
	       &BODY body)
  `(table-row (,stream :single-column ,single-column :id ,id :id-test ,id-test
	       :once-only ,once-only)
     . ,body))


(defmacro table-column-headings ((&OPTIONAL stream
					    &KEY underline-p
					    (id ''headings)
					    (once-only T)
					    &ALLOW-OTHER-KEYS)
				 &BODY body)
  (warn-unimplemented-args underline-p NIL)
  `(table-row (,stream :ID ,id :ONCE-ONLY ,once-only) . ,body))




(defmacro entry ((&OPTIONAL (stream '*Standard-Output*)
			    &KEY align-x align-y align
			    value (value-test '#'EQL)
			    &ALLOW-OTHER-KEYS)
		 &BODY body)
  (unless align-x (setq align-x align))
  (setup-standard-output-stream-arg stream)
  (let ((result (gensym "VALUE")))
    (if (and (consp body)
	     (consp (first body))
	     (eq 'MEMOIZE (first (first body)))
	     (null (cdr body)))
	(let ((args (second (first body))))
	  (let ((cache-value (getf args :VALUE 'NO-CACHE-VALUE))
		(unique-id (getf args :ID))
		(cache-test (getf args :VALUE-TEST '#'EQL))
		(copy-value (getf args :COPY-VALUE))
		(id-test (getf args :ID-TEST '#'EQL)))
	    (setq body (cddr (first body)))
	    (setq body (replace-strings-with-princs body stream))
	    `(entry-internal
	       ,stream ,align-x ,align-y
	       #'(lambda (,stream)
		   (let ((,result (progn . ,body)))
		     (when (and ,result
				(if *Redisplaying-Memo-P*
				    (not (presentation-records *Memo-Presentation*))
				    (not *Presentation-Records*)))
		       (princ ,result ,stream))))
	       ,cache-value ,unique-id ,cache-test
	       ,copy-value ,id-test)))
	(progn
	  (setq body (replace-strings-with-princs body stream))
	  (if value
	      (if (not body)
		  `(entry-internal
		     ,stream ,align-x ,align-y
		     #'(lambda (,stream)
			 (princ ,value))
		     ,value NIL ,value-test)
		  `(entry-internal
		     ,stream ,align-x ,align-y
		     #'(lambda (,stream)
			 (let ((,result			    
				(progn . ,body)))
			   (when (and ,result
				      (if *Redisplaying-Memo-P*
					  (not (presentation-records *Memo-Presentation*))
					  (not *Presentation-Records*)))
			     (princ ,result ,stream))))
		     ,value NIL ,value-test))
	      `(entry-internal
		 ,stream ,align-x ,align-y
		 #'(lambda (,stream)
		     (let ((,result  (progn . ,body)))
		       (when (and ,result
				  (if *Redisplaying-Memo-P*
				      (not (presentation-records *Memo-Presentation*))
				      (not *Presentation-Records*)))
			 (princ ,result ,stream))))))))))


(defmacro table-entry ((&OPTIONAL (stream '*Standard-Output*)
				  &KEY align-x align-y align
				  value (value-test '#'EQL)
				  &ALLOW-OTHER-KEYS)
		       &BODY body)
  `(entry (,stream :align-x ,align-x :align-y ,align-y :align ,align
	   :value ,value :value-test ,value-test)
     . ,body))


(defvar *Subtypep-Cache-Table* (make-hash-table :size 200))


(defmacro maximize (variable number)
  `(when (%< ,variable ,number)
     (setf ,variable ,number)))

(defmacro minimize (variable number)
  `(when (%> ,variable ,number)
     (setf ,variable ,number)))


(defvar *Querying-Values-Stream* NIL)

(defvar *Query-Active* NIL)
(defvar *Query-Default* NIL)
(defvar *Query-Location* NIL)
(defvar *Query-Stream* NIL)
(defvar *Query-X* 0)
(defvar *Query-Y* 0)

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
