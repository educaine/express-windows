;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)


(eval-when (compile load eval)
(define-program-framework menu-definer
  :SELECT-KEY
  #\M
  :COMMAND-DEFINER
  T
  :COMMAND-TABLE
  (:INHERIT-FROM '("colon full command" "standard arguments" "standard scrolling")
   :KBD-ACCELERATOR-P NIL)
  :STATE-VARIABLES ((menu-items) (label NIL) (own-window-p T)
		    (stream-symbol '*Query-Io*)
		    (character-style NIL)
		    (redisplay-code-p T)
		    (output-object NIL)
		    (near-mode (list ':mouse))
		    )
  :PANES
  ((TITLE :TITLE
          :REDISPLAY-STRING "Menu Definer Program"
          :HEIGHT-IN-LINES 1
          :REDISPLAY-AFTER-COMMANDS NIL)
   (MENU  :COMMAND-MENU :MENU-LEVEL :TOP-LEVEL)
   (display :display :REDISPLAY-FUNCTION 'REDISPLAY-MENU-DEFINER
	    :INCREMENTAL-REDISPLAY T
	    :MARGIN-COMPONENTS
	    '((margin-scroll-bar :margin :LEFT :visibility :if-needed)
	      (margin-scroll-bar :margin :BOTTOM :visibility :if-needed)
	      ))
   (code-display :display :REDISPLAY-FUNCTION 'REDISPLAY-MENU-DEFINER-CODE
		 :INCREMENTAL-REDISPLAY T
		 :MARGIN-COMPONENTS
		 '((margin-scroll-bar :margin :LEFT :visibility :if-needed)
		   (margin-scroll-bar :margin :BOTTOM :visibility :if-needed)
		   ))
   (INTERACTOR :INTERACTOR :MARGIN-COMPONENTS
	       '((margin-scroll-bar :margin :LEFT :visibility :if-needed))))
  :CONFIGURATIONS
  '((main
      (:LAYOUT (main :COLUMN TITLE MENU DISPLAY CODE-DISPLAY INTERACTOR))
      (:SIZES
	(MAIN (TITLE 1 :LINES) (MENU :ASK-WINDOW) (interactor 6 :lines)
	      (code-display .3)
	      :THEN (display :EVEN)))))))

(defvar *Menu-Position* '(:POINT 300 100))


(defvar *Combiner-Types* '(or and sequence))

(defstruct (moveable-command)
  presentation x y)


(defstruct (query-command (:PRINT-FUNCTION print-query-command) (:include moveable-command))
  (superior)
  (display-type)
  (prompt "Prompt" :type string)
  (default)
  (inferior-query-command-types NIL))

(defun print-query-command (command stream &REST args)
  (declare (ignore args))
  (format stream "#<Query ~D>" (query-command-prompt command)))

(defstruct (query-command-button (:print-function print-query-command-button)
				 (:include moveable-command))
  (prompt)
  (body))

(defun print-query-command-button (command stream &REST args)
  (declare (ignore args))
  (format stream "#<Command Button ~D>" (query-command-button-prompt command)))

(defstruct alist-member-item
  (string "Item" :type string)
  (style)
  (selectable-p T)
  (value))


(defmacro menu-definer-query-prompt-and-default ()
  `(unless inferior-command-p
     (setq prompt
	   (query 'STRING :PROMPT "Prompt for Query" :DEFAULT prompt)
	   default
	   (query 'form :PROMPT "Default Value" :DEFAULT default))
     (if (query 'boolean :PROMPT "Make Absolute Positioning of Query"
		:DEFAULT (numberp x))
	 (setq x (query 'INTEGER :PROMPT "X Position of Query" :default (or x 0))
	       y (query 'INTEGER :PROMPT "Y Position of Query" :default (or y 0)))
	 (setq x NIL y NIL))
     ))

(defmacro set-menu-definer-query-prompt-and-default ()
  `(unless inferior-command-p
     (setf (query-command-prompt query-command) prompt
	   (query-command-default query-command) default
	   (moveable-command-x query-command) x
	   (moveable-command-y query-command) y)))



(setf (get 'ALIST-MEMBER :CALL-DEFINER) 'call-definer-alist-member)
(setf (get 'ALIST-SUBSET :CALL-DEFINER) 'call-definer-alist-member)
(defun call-definer-alist-member (query-command inferior-command-p)
  (let ((alist-member-item (query-command-display-type query-command))
	(prompt (or (query-command-prompt query-command) "Prompt"))
	(default (or (query-command-default query-command) :DEFAULT-VALUE))
	(provide-minimum-p NIL)
	(provide-maximum-p NIL)
	(minimum NIL)
	(maximum NIL)
	(minimum-inclusive-p T)
	(maximum-inclusive-p T)
	(x (moveable-command-x query-command))
	(y (moveable-command-y query-command))
	(button-type :radio-button)
	(items NIL))
    (with-type-decoded (name data-args display-args) alist-member-item
      (setq button-type (getf display-args :BUTTON-TYPE button-type))
      (when (eq name 'alist-subset)
	(when (eq default :DEFAULT-VALUE)
	  (setq default NIL))
	(setq minimum (third data-args)
	      maximum (fourth data-args))
	(when (consp minimum) (setq minimum-inclusive-p NIL
				    minimum (first minimum)))
	(when (consp maximum) (setq maximum-inclusive-p NIL
				    maximum (first maximum))))
      (setq items
	    (mapcar #'(lambda (item)
			(make-alist-member-item
			  :STRING (get-menu-item-string item)
			  :style (get-menu-item-character-style item)
			  :selectable-p (get-menu-item-selectable-p item)
			  :value (get-menu-item-value item)))
		    (first data-args)))
      (querying-values (*Query-Io* :OWN-WINDOW T :NEAR-MODE *Menu-Position*
				   :label (format NIL  "Options for ~D" name))
	(menu-definer-query-prompt-and-default)
	(setq button-type
	      (query '((alist-member (("Radio" :radio-button) ("Check Box" :check-box)
				      ("Bold Text" :bold-text)))
		       :button-type :radio-button)
		     :DEFAULT button-type
		     :PROMPT "Graphical Style of each Item"))
	(when (eq name 'alist-subset)
	  (setq provide-minimum-p
		(query 'boolean :PROMPT "Include Minimum Number of Values for Query? "
		       :DEFAULT (not (null minimum))))
	  (when provide-minimum-p
	    (setq minimum (query 'INTEGER :PROMPT "Minimum Number Allowed as Input: "
				 :DEFAULT (or minimum 0)))
	    (setq minimum-inclusive-p
		  (query 'boolean :PROMPT "Make Minimum Value Inclusive? "
			 :default minimum-inclusive-p)))
	  (setq provide-maximum-p
		(query 'boolean :PROMPT "Include Maximum Number of Values for Query? "
		       :DEFAULT (not (null maximum))))
	  (when provide-maximum-p
	    (setq maximum (query 'integer :PROMPT "Maximum Number Allowed as Input: "
				 :DEFAULT (or maximum 100)))
	    (setq maximum-inclusive-p
		  (query 'boolean :PROMPT "Make Maximum Value Inclusive? "
			 :default maximum-inclusive-p))))
	(command-button (*Query-Io*)
			"Add Alist Item"
	  (push (make-alist-member-item) items))
	(if items
	    (memo-format *Query-Io* "Alist Member Items Below")
	    (memo-format *Query-Io* "No Alist Member Items entered."))
	(do ((items items (cdr items))
	     (count 0 (%1+ count)))
	    ((null items))
	  (let ((item (first items)))
	    (terpri *Query-Io*)
	    (setf (alist-member-item-string item)
		  (query 'string :PROMPT "Item Display String"
			 :QUERY-IDENTIFIER (intern (format NIL "~D-PROMPT" count))
			 :DEFAULT (alist-member-item-string item))
		  (alist-member-item-style item)
		  (query 'character-style :PROMPT "Item Character Style"
			 :QUERY-IDENTIFIER (intern (format NIL "~D-STYLE" count))
			 :DEFAULT (alist-member-item-style item))
		  (alist-member-item-selectable-p item)
		  (query 'BOOLEAN :PROMPT "Make Item Selectable"
			 :QUERY-IDENTIFIER (intern (format NIL "~D-SELECT" count))
			 :DEFAULT (alist-member-item-selectable-p item))
		  (alist-member-item-value item)
		  (query 'form :PROMPT "Associated Value"
			 :QUERY-IDENTIFIER (intern (format NIL "~D-VALUE" count))
			 :DEFAULT (alist-member-item-value item))))))
	(set-menu-definer-query-prompt-and-default)
	(setf (query-command-display-type query-command)
	      (let ((items (if items
			       (mapcar #'(lambda (item)
					   (if (or (alist-member-item-style item)
						   (alist-member-item-selectable-p item)
						   (consp (alist-member-item-value item)))
					       `(,(alist-member-item-string item)
						 :VALUE ,(alist-member-item-value item)
						 ,@(if (alist-member-item-selectable-p item)
						       NIL '(:no-select T))
						 ,@(if (alist-member-item-style item)
						       `(:STYLE ,(alist-member-item-style item)) NIL))
					       `(,(alist-member-item-string item)
						 . ,(alist-member-item-value item))))
				       items)
			       '(*Slot-For-Alist-Items*))))
		(let ((basic-type `(,name ,items ,@(if (and provide-minimum-p (integerp minimum))
						       (if minimum-inclusive-p
							   `(,minimum) `((,minimum)))
						       (if provide-maximum-p '(NIL)))
				    ,@(if (and provide-maximum-p (integerp maximum))
					  (if maximum-inclusive-p
					      `(,maximum) `((,maximum)))))))
		  (if (eq button-type :RADIO-BUTTON)
		      basic-type
		      `(,basic-type :button-type ,button-type))))))
  query-command))




;; used to recognize common cases of alist-member so that other queries
;; can benefit from all the properties in an alist-member without the user
;; having to care.
;(defun translate-from-alist-member)

(defun find-menu-query (unique-identifier menu-items)
  (let ((count 1))
    (dolist (menu-item menu-items (error "Should never get here"))
      (when (lisp:typep menu-item 'QUERY-COMMAND)
	(when (eq count unique-identifier)
	  ;; this is the correct menu item to modify.
	  (return menu-item))
	(incf count)))))

(define-mouse-command move-alist-item
		      (query-values-choice
			:GESTURE :CONTROL-MIDDLE
			:DOCUMENTATION "Move Alist Item"
			:TEST
			((choice)
			 (and (lisp:typep *Program* 'MENU-DEFINER)
			      (let ((query
				      (find-menu-query (query-values-choices-query-identifier
							 (query-values-choice-choices choice))
						       (menu-definer-menu-items *Program*))))
				(and query
				     (with-type-decoded (name)
							(query-command-display-type query)
				       (eq name 'ALIST-MEMBER)))))))
		      (object &KEY mouse-x mouse-y window)
  `(com-move-alist-item-screw ,object ,mouse-x ,mouse-y ,window))

(define-menu-definer-command (com-move-alist-item-screw) ((object 'query-values-choice)
							  (mouse-x 'integer)
							  (mouse-y 'integer)
							  (*Standard-Output* 'window))
  (when object
    (let ((item (query-values-choice-value object))
	  (presentation (query-values-choice-presentation object))
	  (choices (query-values-choice-choices object)))
      (let ((identifier (query-values-choices-query-identifier choices)))
	;; the identifier was created on the fly such that every call to query
	;; had a unique integer in ascending order from 1 just so that we
	;; could correctly identify it here.
	(let ((found-menu-item (find-menu-query identifier menu-items)))
	  (let ((old-mouse-x mouse-x) (old-mouse-y mouse-y)
		final-x final-y original-x original-y)
	    (multiple-value-setq (original-x original-y)
	      (get-menu-item-position item))
	    ;; if the item has no position yet, calculate it based on its presentation
	  ;; now we need to pick apart the type and correctly
	    (unless original-x
	      (setq original-x (- (boxed-presentation-left presentation)
				  (query-command-x found-menu-item))))
	    (unless original-y
	      (setq original-y (- (boxed-presentation-top presentation)
				  (query-command-y found-menu-item))))
	    #+debugging-menu
	    (dformat "~%Original-X ~D Original-Y ~D." original-x original-y)
	    ;; change it after the move.
	    (tracking-mouse (*Standard-Output*
			      :who-line-documentation-string "Move Item to proper position")
	      (:MOUSE-MOTION-HOLD (new-x new-y)
	       ;; erase presentation from screen, but leave in window.
	       (let ((*Erase-P* T))
		 (redraw-presentation presentation *Standard-Output* NIL NIL NIL NIL T))
	       (offset-presentation-tree presentation (- new-x mouse-x) (- new-y mouse-y))
	       ;; redraw on screen.
	       (redraw-presentation presentation *Standard-Output* NIL NIL NIL NIL T)
	       (setf mouse-x new-x mouse-y new-y))
	      (:MOUSE-MOTION (new-x new-y)
	       (return NIL))
	      (:MOUSE-CLICK (character new-x new-y)
	       (return NIL))
	      (:RELEASE-MOUSE ()
			      (return NIL)))
	    #+debugging-menu
	    (dformat "~%Original-X ~D Original-Y ~D." original-x original-y)
	    #+debugging-menu
	    (dformat "~%Mouse-X ~D Mouse-Y ~D." mouse-x mouse-y)
	    #+debugging-menu
	    (dformat "~%Old-Mouse-X ~D Old-Mouse-Y ~D." old-mouse-x old-mouse-y)
	    ;; setup x,y in object
	    ;; we must set this relative to the query's x and y values.
	    (setq final-x (+ original-x (- mouse-x old-mouse-x))
		  final-y (+ original-y (- mouse-y old-mouse-y)))
	    #+debugging-menu
	    (dformat "~%Final-X ~D Final-Y ~D." final-x final-y)
	    ;; we are done tracking - now fix up the item type.
	    ;; first break apart the item into the original values.
	    (let ((item-string (get-menu-item-string item))
		  (item-value (get-menu-item-value item))
		  (item-style (get-menu-item-character-style item))
		  (item-select (get-menu-item-selectable-p item)))
	      ;; we are definitely going to need it in full list option.
	      (let ((new-item
		      `(,item-string
			,@(if item-select
			      `(:VALUE ,item-value)
			      `(:NO-SELECT T))
			,@(if item-style `(:STYLE ,item-style))
			:X ,final-x :Y ,final-y)))
		;;(dformat "~%New Item ~D" new-item)
		;;(dformat "~%Old Item ~D" item)
		;; replace it in type definition.
		;; this is a terrible hack, but I will copy-tree the previous display-type
		;; so that the new will not be equal to the old so that redisplay will work
		;; without having to make major changes elsewhere.
		(let ((type (copy-tree (query-command-display-type found-menu-item))))
		  ;;(setq type (translate-type-if-necessary type))
		  (with-type-decoded (name data-args) type
		    (declare (ignore name))
		    ;;#+debugging-qv
		    (dformat "~%Old Item List ~D" (first data-args))
		    ;; side effect the item list - it should work.
		    (setf (first data-args)
			  (nsubstitute new-item item (first data-args) :TEST #'EQUAL))
		    ;;#+debugging-qv
		    (dformat "~%New Item List ~D" (first data-args))
		    )
		  #+debugging-qv
		  (dformat "~%New Type ~D" type)
		  ;; now reset the type in the query-command
		  (setf (query-command-display-type found-menu-item) type))))))))))


;; most types do not directly support the individual manipulation of the args
;; such as boolean.. So if such happens translate this to the internal representation.
;(defun translate-type-if-necessary (type)
;  (with-type-decoded (name data-args) type
;    (case name
;      ((boolean inverted-boolean)))))
;       ;; look for alist-items that have been moved.



(setf (get 'integer :CALL-DEFINER) 'call-definer-number)
(setf (get 'number :CALL-DEFINER) 'call-definer-number)
(defun call-definer-number (query-command inferior-command-p)
  (let ((type (query-command-display-type query-command))
	(prompt (or (query-command-prompt query-command) "Prompt"))
	(default (or (query-command-default query-command) 10))
	(x (moveable-command-x query-command))
	(y (moveable-command-y query-command))
	(provide-minimum-p NIL)
	(provide-maximum-p NIL)
	(minimum NIL)
	(maximum NIL)
	(minimum-inclusive-p T)
	(maximum-inclusive-p T))
    ;; a little error checking here.
    (unless type (setq type (setf (query-command-display-type query-command) 'integer)))
    (with-type-decoded (type-name data-args display-args) type
      (declare (ignore display-args))		;4/30/90 (SLN)
      (setq minimum (second data-args)
	    maximum (third data-args))
      (when (consp minimum) (setq minimum-inclusive-p NIL
				  minimum (first minimum)))
      (when (consp maximum) (setq maximum-inclusive-p NIL
				  maximum (first maximum)))
      (querying-values (*Query-Io* :OWN-WINDOW T  :NEAR-MODE *Menu-Position*
				   :label (format NIL "Options for ~A type"
						  type-name))
	(menu-definer-query-prompt-and-default)
	(setq provide-minimum-p
	      (query 'boolean :PROMPT "Include Minimum Value for Query? "
		     :DEFAULT (not (null minimum))))
	(when provide-minimum-p
	  (setq minimum (query type-name :PROMPT "Minimum Value Allowed as Input: "
			       :DEFAULT (or minimum 0)))
	  (setq minimum-inclusive-p
		(query 'boolean :PROMPT "Make Minimum Value Inclusive? "
		       :default minimum-inclusive-p)))
	(setq provide-maximum-p
	      (query 'boolean :PROMPT "Include Maximum Value for Query? "
		     :DEFAULT (not (null maximum))))
	(when provide-maximum-p
	  (setq maximum (query type-name :PROMPT "Maximum Value Allowed as Input: "
			       :DEFAULT (or maximum 100)))
	  (setq maximum-inclusive-p
		(query 'boolean :PROMPT "Make Maximum Value Inclusive? "
		       :default maximum-inclusive-p))))
      (set-menu-definer-query-prompt-and-default)
      (setf (query-command-display-type query-command)
	    (if (not (or provide-minimum-p provide-maximum-p))
		type-name
		`(,type-name ,(if provide-minimum-p
				 (if minimum-inclusive-p
				     `(,minimum) minimum))
			    . ,(if provide-minimum-p
				   `(,(if minimum-inclusive-p
					  `(,minimum) minimum))))))))
  query-command)




(setf (get 'string :CALL-DEFINER) 'call-definer-string)
(defun call-definer-string (query-command inferior-command-p)
  (let ((type (query-command-display-type query-command))
	(prompt (or (query-command-prompt query-command) "Prompt"))
	(default (or (query-command-default query-command) "Default Value"))
	(x (moveable-command-x query-command))
	(y (moveable-command-y query-command))
	(provide-exact-size-p NIL)
	(exact-size NIL)
	(provide-minimum-p NIL)
	(provide-maximum-p NIL)
	(minimum NIL)
	(maximum NIL)
	(minimum-inclusive-p NIL)
	(maximum-inclusive-p NIL)
	(case NIL))
    ;; a little error checking here.
    (unless type (setq type (setf (query-command-display-type query-command) 'integer)))
    (with-type-decoded (type-name data-args display-args) type
      (setq exact-size (first data-args)
	    minimum (second data-args)
	    maximum (third data-args))
      (when (consp minimum) (setq minimum-inclusive-p T
				  minimum (first minimum)))
      (when (consp maximum) (setq maximum-inclusive-p T
				  maximum (first maximum)))
      (setq case (getf display-args :CASE :UNCHANGED))
      (querying-values (*Query-Io* :OWN-WINDOW T :NEAR-MODE *Menu-Position*
				   :label (format NIL "Options for ~A type"
						  type-name))
	(menu-definer-query-prompt-and-default)
	(setq case
	      (query '(alist-member (("Unchanged" :unchanged)
				     ("Upper Case" :uppercase)
				     ("Lower Case" :lowercase)
				     ("Capitalize" :capitalize)))
		     :PROMPT "String Case For the Input"
		     :DEFAULT case))
	(setq provide-exact-size-p
	      (query 'boolean :PROMPT "Include Exact Size of Input String? "
		     :DEFAULT (not (null exact-size))))
	(if provide-exact-size-p
	    (setq exact-size
		  (query 'integer :PROMPT "Exact Size of Input String? "
			 :DEFAULT exact-size))
	    (progn
	      (setq provide-minimum-p
		    (query 'boolean :PROMPT "Include Minimum Size for Query? "
			   :DEFAULT (not (null minimum))))
	      (when provide-minimum-p
		(setq minimum (query type-name :PROMPT "Minimum Size Allowed as Input: "
				     :DEFAULT (or minimum 0)))
		(setq minimum-inclusive-p
		      (query 'boolean :PROMPT "Make Minimum Size Inclusive? "
			     :default minimum-inclusive-p)))
	      (setq provide-maximum-p
		    (query 'boolean :PROMPT "Include Maximum Size for Query? "
			   :DEFAULT (not (null maximum))))
	      (when provide-maximum-p
		(setq maximum (query type-name :PROMPT "Maximum Size Allowed as Input: "
				     :DEFAULT (or maximum 100)))
		(setq maximum-inclusive-p
		      (query 'boolean :PROMPT "Make Maximum Size Inclusive? "
			     :default maximum-inclusive-p))))))
      (set-menu-definer-query-prompt-and-default)
      (setf (query-command-display-type query-command)
	    (let ((basic-type
		    (cond ((not (or provide-minimum-p provide-maximum-p provide-exact-size-p))
			   type-name)
			  (provide-exact-size-p
			   `(,type-name ,exact-size))
			  (T `(,type-name NIL ,(if provide-minimum-p
						   (if minimum-inclusive-p
						       `(,minimum) minimum))
			       . ,(if provide-minimum-p
				      `(,(if minimum-inclusive-p
					     `(,minimum) minimum))))))))
	      (cond ((eq case :unchanged)
		     basic-type)
		    ((consp basic-type)
		     `(,basic-type :CASE ,case))
		    (T
		     `((,basic-type) :CASE ,case)))))))
  query-command)

(define-menu-definer-command (menu-definer-exit :menu-accelerator "Exit") ()
  (exit-program))

(define-menu-definer-command (initialize :menu-accelerator T) ()
  (setq menu-items NIL))


;;; 6/27/90 (sln)
;;; For Type QUERY-COMMAND
;;;   Type QUERY-COMMAND is defined twice in the file EW:SOURCE;MENU-DEFINER.
;;; For Function (:PROPERTY QUERY-COMMAND DEFTYPE)
;;;   Function (:PROPERTY QUERY-COMMAND DEFTYPE) is defined twice in the file EW:SOURCE;MENU-DEFINER.
;(define-type query-command (())
;  )

(define-type define-query-menu-label (())
  :PRINTER ((label stream)
	    (let ((string label)
		  (style '(NIL :BOLD :LARGE)))
	      (when (consp label)
		(setq string (getf label :STRING "Querying Values Menu")
		      style (getf label :STYLE (getf label :CHARACTER-STYLE style))))
	      (with-character-style (style stream)
		(if (or (stringp string) (symbolp string) (numberp string))
		    (simple-princ string stream)
		    (funcall label stream))))))

(defmethod redisplay-menu-definer ((self menu-definer) *Standard-Output*)
  (with-slots (menu-items label character-style) self
    ;; pretend you are running query-values, but don't create normal presentations.
    (let ((*Running-Menu-Definer-P* T)
	  (query-identifier 0)) ;; generate a fake query identifier for now.
      (with-character-style ((or character-style '(:fix :roman :large)) *Standard-Output*)
	(querying-values-display
	  *Standard-Output*
	  #'(lambda (stream)
	      (display (or label "No Defined Label") 'DEFINE-QUERY-MENU-LABEL :STREAM stream))
	  #'(lambda (stream)
	      (dolist (command menu-items)
		(typecase command
		  (query-command
		    (setf (query-command-presentation command)
			  (display-as (:TYPE 'QUERY-COMMAND :OBJECT command :STREAM stream
					     :SINGLE-BOX T)
			    (if (query-command-display-type command)
				(query (query-command-display-type command)
				       :PROMPT (query-command-prompt command)
				       :DEFAULT (query-command-default command)
				       :STREAM stream
				       :QUERY-IDENTIFIER (incf query-identifier)
				       :X (moveable-command-x command)
				       :Y (moveable-command-y command))
				(memo-format stream "Undefined Query Command")))))
		  (query-command-button
		    (setf (query-command-presentation command)
			  (display-as (:TYPE 'query-command-button :object command :stream stream
					     :single-box T)
			    (command-button (stream)
					    (simple-princ (or (query-command-button-prompt command)
							      "Command Button Prompt")
							  stream)
			      (query-command-button-body command))))))))
	  NIL
	  T)))))



(define-mouse-command edit-menu-definer-menu-label
		       (define-query-menu-label
			 :DOCUMENTATION "Edit Label")
		       (label)
  `(com-edit-menu-definer-menu-label ,label))


(define-menu-definer-command (com-add-label :MENU-ACCELERATOR T) ()
  (if label
      (progn (format *Query-Io* "~&The Menu already has a Label.~%")
	     (beep))
      (com-edit-menu-definer-menu-label "New Label")))


(define-menu-definer-command (com-edit-menu-definer-menu-label) ((new-label '(or string list)))
  (let ((label-string new-label)
	(label-p (not (null new-label)))
	(style '(NIL :BOLD :LARGE)))
    (when (consp new-label)
      (setq style (getf new-label :STYLE '(NIL :BOLD :LARGE))
	    label-string (getf new-label :STRING "Querying Values Menu")))
    (querying-values (*Query-Io* :LABEL "Label Properties"  :NEAR-MODE *Menu-Position*
				 :own-window T)
      (setq label-p (query 'boolean :prompt "Include Label in Query Menu."
			   :DEFAULT label-p))
      (when label-p
	(setq label-string
	      (query 'STRING :PROMPT "Label String" :DEFAULT label-string)
	      style
	      (query 'character-style :PROMPT "Character Style for the Label"
		     :default style))))
    (setq label
	  (if label-p
	      (if (equal style '(NIL :BOLD :LARGE))
		  label-string
		  `(:STRING ,label-string :STYLE ,style))
	      NIL))))


(defvar *Query-Types*
	'(("Alist Member" alist-member)
	  ("Alist Subset" alist-subset)
	  ("Boolean" boolean)
	  ("Character Style" character-style)
	  ;; 6/14/90 (sln) for non-pcls
	  #+ew-clos ("CLOS Class" class)
	  ("Font" font)
	  ("Integer" integer)
	  ("Keyword" keyword)
	  ("Number" number)
	  ("Package" package)
	  ("String" string)
	  (" ---- " :no-select T)
	  ("Complex Types" :no-select T)
	  ("OR" or)
	  ("AND" and)
	  ("SEQUENCE" sequence)))

(define-menu-definer-command (add-query-command :menu-accelerator T) ()
  (let ((command (make-query-command)))
    (let ((type (menu-choose *Query-Types* :PROMPT "Choose Type for Query")))
      (when type
	(setf (query-command-display-type command) type)
	(when (fboundp (get type :CALL-DEFINER))
	  (funcall (get type :CALL-DEFINER) command NIL))
	(setq menu-items (nconc menu-items (list command)))))))



(define-mouse-command delete-query-command
		      (query-command :gesture :control-Left
				     :Documentation "Delete Query")
		      (command)
  `(com-delete-query-command ,command))

(define-menu-definer-command (com-delete-query-command) ((command 'QUERY-COMMAND))
  (setq menu-items
	(delete command menu-items)))


(define-mouse-command edit-query-command
		      (query-command :Documentation "Edit Query")
		      (command)
  `(com-edit-query-command ,command))

(define-menu-definer-command (com-edit-query-command) ((command 'QUERY-COMMAND))
  (edit-command command NIL))

(defun edit-command (command inferior-p)
  (when (menu-choose '(("Yes" T)("No" NIL))
		     :PROMPT
		     (format NIL "Change the Type from ~A"
			     (query-command-display-type command)))
    (let ((new-type (menu-choose *Query-Types* :prompt "Choose Type for Query")))
      (when new-type
	(setf (query-command-display-type command) new-type))))
  (let ((type (query-command-display-type command)))
    (with-type-decoded (name) type
      (when (fboundp (get name :CALL-DEFINER))
	(funcall (get name :CALL-DEFINER) command inferior-p)))))

(defmethod redisplay-menu-definer-code ((self menu-definer) *Standard-Output*)
  (with-slots (menu-items label own-window-p stream-symbol redisplay-code-p) self
    (with-output-truncation (*Standard-Output*)
      (memoize (:VALUE redisplay-code-p :VALUE-TEST #'(lambda (x y) (declare (ignore y)) x))	;4/30/90 (sln) added ignore on y
	(let ((code (generate-code self)))
	  (let ((code-string
		  (with-output-to-string (stream)
		    (write code :stream stream :pretty :code))))
	    (simple-princ code-string)))
	(setq redisplay-code-p NIL)))))

(defmethod generate-code ((self menu-definer))
  (with-slots (stream-symbol menu-items label own-window-p character-style) self
    `(querying-values (,stream-symbol ,@(if own-window-p '(:own-window T))
		       ,@(if label `(:label ,label))
		       ,@(if character-style `(:character-style ',character-style)))
       ,@(mapcar #'(lambda (command)
		     (typecase command
		       (query-command
			 `(query ',(query-command-display-type command)
				 ,@(if (not (eq stream-symbol '*Query-IO*))
				       `(:stream ,stream-symbol))
				 :PROMPT ',(query-command-prompt command)
				 :DEFAULT ,(query-command-default command)
				 ,@(if (moveable-command-x command)
				       `(:X ,(moveable-command-x command)))
				 ,@(if (moveable-command-y command)
				       `(:Y ,(moveable-command-y command)))))
		       (query-command-button
			 `(command-button (,stream-symbol)
					  ,(or (query-command-button-prompt command)
					       "Command Button Prompt")
			    ,(query-command-button-body command)))))
		 menu-items))))




(setf (get 'sequence :CALL-DEFINER) 'call-definer-sequence)
(defun call-definer-sequence (query-command inferior-command-p)
  (let ((type (query-command-display-type query-command))
	(prompt (or (query-command-prompt query-command) "Prompt"))
	(default (or (query-command-default query-command) ()))
	(x (moveable-command-x query-command))
	(y (moveable-command-y query-command))
	(sequence-delimiter #\,)
	(echo-space T)
	(inferior-query-command
	  (first (query-command-inferior-query-command-types query-command))))
    (unless inferior-query-command
      (setq inferior-query-command (make-query-command :DISPLAY-TYPE 'INTEGER
							:SUPERIOR query-command))
      (setf (query-command-inferior-query-command-types query-command)
	    (list inferior-query-command)))
    ;; a little error checking here.
    (unless type (setq type (setf (query-command-display-type query-command) 'integer)))
    (with-type-decoded (type-name data-args display-args) type
      (declare (ignore data-args))		;4/30/90 (sln)
      (setq sequence-delimiter (getf display-args :sequence-delimiter #\,)
	    echo-space (getf display-args :echo-space T))
      (querying-values (*Query-Io* :OWN-WINDOW T  :NEAR-MODE *Menu-Position*
				   :label (format NIL "Options for ~A type"
						  type-name))
	(menu-definer-query-prompt-and-default)
	(setq sequence-delimiter
	      (query 'character :PROMPT "Sequence Delimiter Character: "
		     :Default sequence-delimiter)
	      echo-space
	      (query 'boolean :PROMPT "Echo Space? "
		     :DEFAULT echo-space)))
      (set-menu-definer-query-prompt-and-default)
      (flet ((recompute-type ()
	       (setf (query-command-display-type query-command)
		     (let ((basic-type
			     (query-command-display-type inferior-query-command)))
		       (if (and (eq sequence-delimiter #\,)
				(eq T echo-space))
			   `(sequence ,basic-type)
			   `((sequence ,basic-type) ,@(and (not (eq sequence-delimiter #\,))
						  `(:sequence-delimiter ,sequence-delimiter))
			     ,@(and (not (eq T echo-space))
				    `(:echo-space ,echo-space))))))))
	(recompute-type)
	;; now edit inferior
	(edit-command inferior-query-command T)
	(recompute-type))))
  query-command)




(define-menu-definer-command (save-program :menu-accelerator T) ()
  (when output-object
    ;; query about saving to same place as before.
    (unless (query 'BOOLEAN
		   :prompt (format NIL "Save to ~:[File~;Buffer~] again?"
				   (not (pathnamep output-object))))
      (setq output-object NIL)))
  (unless output-object
    (let ((file-p :BUFFER)
	  (file-or-buffer NIL))
      (if (member :LUCID-EDITOR *Features*)
	  (querying-values (*Query-Io* :OWN-WINDOW T :NEAR-MODE *Menu-Position*
				       :LABEL "Save Options")
	    (setq file-p (query '(alist-member (("File" . :FILE)("Buffer" . :BUFFER)))
				:PROMPT "Save code to a "
				:DEFAULT file-p))
	    (setq file-or-buffer
		  (if (eq :FILE file-p)
		      (query 'pathname :PROMPT "Choose Pathname")
		      (query '((editor-buffer) :any-input T) :PROMPT "Choose Buffer"))))
	  (querying-values (*Query-Io* :OWN-WINDOW T :NEAR-MODE *Menu-Position*
				       :LABEL "Save Options")
	    (setq file-or-buffer
		  (query 'pathname :PROMPT "Choose Pathname"))))
      (setq output-object file-or-buffer)))
  (flet ((get-code ()
	   (let ((code (generate-code self)))
	     (let ((code-string
		     (with-output-to-string (stream)
		       (write code :stream stream :pretty :code))))
	       code-string))))
    (cond ((not output-object)
	   (format *Query-Io* "Aborted Saving Code."))
	  ((pathnamep output-object)
	   (with-open-file (output output-object :DIRECTION :OUTPUT)
	     (princ (get-code) output)))
	  #+lucid  ; 6/28/90 (sln) write-string-to-buffer is defined in lucid-editor-interface.lisp
	  (T ;; hopefully a editor buffer
	   (write-string-to-buffer (get-code) output-object)))))





(setf (get 'and :CALL-DEFINER) 'call-definer-and)
(setf (get 'or :CALL-DEFINER) 'call-definer-and)
(defun call-definer-and (query-command inferior-command-p)
  (let ((type (query-command-display-type query-command))
	(prompt (or (query-command-prompt query-command) "Prompt"))
	(default (or (query-command-default query-command) ()))
	(x (moveable-command-x query-command))
	(y (moveable-command-y query-command))
	(inferior-query-commands (query-command-inferior-query-command-types query-command)))
    (with-type-decoded (type-name) type
      (declare (ignore type-name))		;4/30/90 (SLN)
      (unless inferior-query-commands
	(setq inferior-query-commands
	      (list (make-query-command :DISPLAY-TYPE 'INTEGER
					:SUPERIOR query-command)))
	(setf (query-command-inferior-query-command-types query-command)
	      inferior-query-commands))
      ;; a little error checking here.
      (unless type (setq type (setf (query-command-display-type query-command) 'INTEGER)))
      (with-type-decoded (type-name data-args display-args) type
	(declare (ignore data-args display-args))	;4/30/90 (SLN)
	(let ((keep-types NIL))
	  (querying-values (*Query-Io* :OWN-WINDOW T :NEAR-MODE *Menu-Position*
				       :label (format NIL "Options for ~A type"
						      type-name))
	    (menu-definer-query-prompt-and-default)
	    (let ((count 0))
	      (setq keep-types
		    (mapcar #'(lambda (command)
				(declare (ignore command))	;5/1/90 (sln)
				(incf count)
				(query 'boolean :DEFAULT T
				       :PROMPT
				       (format NIL "Keep the ~:R inferior Type." count)))
			    (query-command-inferior-query-command-types query-command)))))
	  (set-menu-definer-query-prompt-and-default)
	  (do ((keeps keep-types (cdr keeps))
	       (commands (query-command-inferior-query-command-types query-command)
			 (cdr commands)))
	      ((null keeps))
	    (when (and (not (first keeps))
		       (cdr (query-command-inferior-query-command-types query-command)))
	      (setf (query-command-inferior-query-command-types query-command)
		    (delete (first commands)
			    (query-command-inferior-query-command-types query-command))))))
	(flet ((recompute-type ()
		 (setf (query-command-display-type query-command)
		       `(,type-name 
			 . ,(mapcar #'(lambda (command)
					(query-command-display-type command))
				    (query-command-inferior-query-command-types
				      query-command))))))
	  (recompute-type)
	  ;; now edit inferiors
	  (dolist (inferior (query-command-inferior-query-command-types query-command))
	    (edit-command inferior T))
	  (recompute-type)))))
  query-command)




(setf (get 'character-style :CALL-DEFINER) 'call-definer-character-style)
(setf (get 'package :CALL-DEFINER) 'call-definer-character-style)
(setf (get 'keyword :CALL-DEFINER) 'call-definer-character-style)
(setf (get 'class :CALL-DEFINER) 'call-definer-character-style)
(setf (get 'font :CALL-DEFINER) 'call-definer-character-style)
(defun call-definer-character-style (query-command inferior-command-p)
  (let ((type (query-command-display-type query-command))
	(prompt (or (query-command-prompt query-command) "Prompt"))
	(default (or (query-command-default query-command) "Default Value"))
	(x (moveable-command-x query-command))
	(y (moveable-command-y query-command)))
    (with-type-decoded (type-name data-args display-args) type
      (declare (ignore display-args data-args))	;4/30/90 (SLN)
      (querying-values (*Query-Io* :OWN-WINDOW T :NEAR-MODE *Menu-Position*
				   :label (format NIL "Options for ~A type"
						  type-name))
	(menu-definer-query-prompt-and-default))
      (set-menu-definer-query-prompt-and-default)
      (setf (query-command-display-type query-command)
	    type-name)))
  query-command)




(setf (get 'BOOLEAN :CALL-DEFINER) 'call-definer-boolean)
(setf (get 'INVERTED-BOOLEAN :CALL-DEFINER) 'call-definer-boolean)
(defun call-definer-boolean (query-command inferior-command-p)
  (let ((boolean-item (query-command-display-type query-command))
	(prompt (or (query-command-prompt query-command) "Prompt"))
	(x (moveable-command-x query-command))
	(y (moveable-command-y query-command))
	(default (or (query-command-default query-command) :DEFAULT-VALUE))
	(response-type :YES-OR-NO)
	(button-type :RADIO-BUTTON))
    (with-type-decoded (name data-args display-args) boolean-item
      (declare (ignore data-args))
      (setq button-type (getf display-args :BUTTON-TYPE button-type))
      (setq response-type (getf display-args :RESPONSE-TYPE response-type))
      (querying-values (*Query-Io* :OWN-WINDOW T :NEAR-MODE *Menu-Position*
				   :label (format NIL "Options for ~A" name))
	(menu-definer-query-prompt-and-default)
	(setq button-type
	      (query '((alist-member (("Radio" :radio-button) ("Check Box" :check-box)
				      ("Bold Text" :bold-text)))
		       :button-type :radio-button)
		     :DEFAULT button-type
		     :PROMPT "Graphical Style of each Item"))
	(setq response-type
	      (query '((alist-member (("Yes-or-No" :yes-or-no)
				      ("True-or-False" :true-or-false)
				      ("On-or-Off" :on-or-off)))
		       :button-type :radio-button)
		     :DEFAULT response-type
		     :PROMPT "Choose Response Type")))
      (set-menu-definer-query-prompt-and-default)
      (setf (query-command-display-type query-command)
	    (if (and (eq button-type :RADIO-BUTTON)
		     (eq response-type :yes-or-no))
		name
		`((,name) ,@(if (not (eq button-type :radio-button))
				  `(:button-type ,button-type))
		  ,@(if (not (eq response-type :yes-or-no))
			`(:response-type ,response-type))))))
  query-command))



(define-mouse-command move-query-command
		      (query-command :GESTURE :CONTROL-MIDDLE
				     :Documentation "Move Query")
		      (command &KEY mouse-x mouse-y window)
  `(com-move-command ,command ,mouse-x ,mouse-y ,window))

(define-menu-definer-command (com-move-command) ((command 'QUERY-COMMAND)
						 (mouse-x 'integer)
						 (mouse-y 'integer)
						 (*Standard-Output* 'window))
  (let ((presentation (moveable-command-presentation command)))
    (unless presentation
      (error "Where is the presentation for this command, or how did it get selected."))
    (let ((old-mouse-x mouse-x)
	  (old-mouse-y mouse-y)
	  (original-x (query-command-x command))
	  (original-y (query-command-y command)))
      (unless original-x
	(setq original-x (boxed-presentation-left presentation)))
      (unless original-y
	(setq original-y (boxed-presentation-top presentation)))
      (tracking-mouse (*Standard-Output*
			:WHO-LINE-DOCUMENTATION-STRING "Move Query to proper position")
	(:mouse-motion-hold (new-x new-y)
	 ;; erase presentation from screen, but leave in window.
	 (let ((*Erase-P* T))
	   (redraw-presentation presentation *Standard-Output* NIL NIL NIL NIL T))
	 (offset-presentation-tree presentation (- new-x mouse-x) (- new-y mouse-y))
	 ;; redraw on screen.
	 (redraw-presentation presentation *Standard-Output* NIL NIL NIL NIL T)
	 (setf mouse-x new-x mouse-y new-y))
	(:MOUSE-MOTION (new-x new-y)
	 (return NIL))
	(:MOUSE-CLICK (character new-x new-y)
	 (return NIL))
	(:RELEASE-MOUSE ()
			(return NIL)))
      ;; setup x,y in object
      (setf (moveable-command-x command) (+ original-x (- mouse-x old-mouse-x))
	    (moveable-command-y command) (+ original-y (- mouse-y old-mouse-y))))))



(define-menu-definer-command (com-refresh-menu-definer :menu-accelerator "Refresh") ()
  (redraw-window (get-program-pane 'DISPLAY)))

(define-menu-definer-command (com-edit-menu-options :menu-accelerator T) ()
  (with-slots (menu-items character-style own-window-p near-mode) self
    (let ((near-mode-type (and (consp near-mode)
			       (first near-mode))))
      (querying-values (*Query-Io* :OWN-WINDOW T :NEAR-MODE *Menu-Position*)
	(setq character-style
	      (query 'character-style :PROMPT "Default Character Style for Menu"
		     :DEFAULT character-style))
	(setq own-window-p (query 'boolean :PROMPT "Make Menu have its own pop up window"))
	(when own-window-p
	  (setq near-mode-type
		(query '(alist-member '(("Near Mouse" . :mouse)
					("Near Window" . :window)
					("Centered on Point" . :point)
					("Near Rectangle" . :rectangle)))
		       :PROMPT "Choose Where to Place Menu"
		       :DEFAULT near-mode-type))
	  (setq near-mode
		(if (eq near-mode-type :MOUSE)
		    (list near-mode-type)
		    (list near-mode-type (case near-mode
					   (:window 'window)
					   (:point 'point-location)
					   (:rectangle 'rectangle))))))))))







(setf (get 'pathname :CALL-DEFINER) 'call-definer-pathname)
(defun call-definer-pathname (query-command inferior-command-p)
  (let ((type (query-command-display-type query-command))
	(prompt (or (query-command-prompt query-command) "Prompt"))
	(default (or (query-command-default query-command) "Default Value"))
	(x (moveable-command-x query-command))
	(y (moveable-command-y query-command))
	default-type default-version default-name dont-merge-default direction format
	)
    ;; a little error checking here.
    (unless type (setq type (setf (query-command-display-type query-command) 'integer)))
    (with-type-decoded (type-name data-args display-args) type
      (declare (ignore data-args))		;4/30/90 (sln)
      (setq default-type (getf display-args :DEFAULT-TYPE NIL)
	    default-version (getf display-args :DEFAULT-VERSION :NEWEST)
	    default-name (getf display-args :DEFAULT-NAME NIL)
	    dont-merge-default (getf display-args :DONT-MERGE-DEFAULT NIL)
	    direction (getf display-args :DIRECTION :READ)
	    format (getf display-args :FORMAT :NORMAL))
      (querying-values (*Query-Io* :OWN-WINDOW T :NEAR-MODE *Menu-Position*
				   :label (format NIL "Options for ~A type"
						  type-name))
	(menu-definer-query-prompt-and-default)
	(setq default-name (query 'string
				  :DEFAULT default-name :PROMPT "Default Name")
	      default-type
	      (query 'form :DEFAULT default-type :PROMPT "Default Type")
	      default-version (query 'form :default default-version :PROMPT "Default Version")
	      dont-merge-default (query 'inverted-boolean :default dont-merge-default
					:PROMPT "Merge Default?")
	      direction (query '(alist (("Read" :READ)("Write" :WRITE)))
			       :DEFAULT direction :PROMPT "Direction for operation"))
	(terpri *Query-Io*)
	(if (eq direction :READ)
	    (memo-write-string "Read implies that only existing files are allowable.")
	    (memo-write-string "Write implies that any pathname is allowable."))
	(setq format (query '(alist (("Normal" :normal)("Directory" :directory)))
			    :DEFAULT format :PROMPT "Printing Format")))
      (set-menu-definer-query-prompt-and-default)
      (setf (query-command-display-type query-command)
	    (if (and (eq default-version :NEWEST)
		     (eq default-type NIL)
		     (eq default-name NIL)
		     (eq dont-merge-default NIL)
		     (eq direction :READ)
		     (eq format :NORMAL))
		'PATHNAME
		`((PATHNAME)
		  ,@(if (eq default-version :NEWEST) NIL `(:default-version ,default-version))
		  ,@(if (eq default-type NIL) NIL `(:default-type ,default-type))
		  ,@(if (eq default-name NIL) NIL `(:default-name ,default-name))
		  ,@(if (eq dont-merge-default NIL)
			NIL `(:dont-merge-default ,dont-merge-default))
		  ,@(if (eq direction :READ) NIL `(:direction ,direction))
		  ,@(if (eq format :NORMAL) NIL `(:format ,format)))))))
  query-command)




#+ignore
(define-mouse-command menu-definer-imbed-in-expression
		      (query-command
			:GESTURE NIL))

(define-mouse-command menu-definer-add-inferior-type
		      (query-command
			:GESTURE :MIDDLE
			:DOCUMENTATION "Add Inferior Type"
			:TEST
			((command)
			 (member (query-command-display-type command)
				 '(OR AND))))
		      (command)
  `(com-menu-definer-add-inferior-type ,command))

(defun com-menu-definer-add-inferior-type (command)
  ;; pop up menu to prompt for where to add it.
  (let ((inferior-types (query-command-display-type command)))
    (if inferior-types
	(multiple-value-bind (type item gesture)
	    (menu-choose
	      (cons '("Left Button for Before - Middle for After"
		      :NO-SELECT T)
		    (mapcar #'(lambda (type)
				(list (format NIL "~D" type)
				      :VALUE type))
			    inferior-types))
	      :LABEL "Choose Type to Place Type Before or After")
	  (declare (ignore item gesture))	;4/30/90 (sln)
	  (when (member type inferior-types)
	    ;; create a new type and go with it.
	    )))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
