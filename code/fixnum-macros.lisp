;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)

;; 5/14/90 (sln) - X3J13 doesn't allow macros to expand into declares,
;; so I've replace every call to (fast) with (declare (optimize (speed
;; 3) (safety 0) (compilation-speed 0) (space 0))).
;; 8/2/90 (sln) - We'll use <sharp-sign><period> for this when it's used.
(defmacro fast ()
  `'(declare (optimize (speed 3) (safety 0) (compilation-speed 0) (space 0))))

(defmacro %= (&REST fixnums)
  `(= . ,(mapcar #'(lambda (x) `(the fixnum ,x)) fixnums)))

(defmacro %> (&REST fixnums)
  `(> . ,(mapcar #'(lambda (x) `(the fixnum ,x)) fixnums)))

(defmacro %< (&REST fixnums)
  `(< . ,(mapcar #'(lambda (x) `(the fixnum ,x)) fixnums)))

(defmacro %>= (&REST fixnums)
  `(>= . ,(mapcar #'(lambda (x) `(the fixnum ,x)) fixnums)))

(defmacro %<= (&REST fixnums)
  `(<= . ,(mapcar #'(lambda (x) `(the fixnum ,x)) fixnums)))

(defmacro %+ (&REST fixnums)
  (declare (list fixnums))
  (case (length fixnums)
    (0 0)
    (1 (first fixnums))
    (2 `(the fixnum (+ (the fixnum ,(first fixnums))
		       (the fixnum ,(second fixnums)))))
    (T `(the fixnum (+ (the fixnum ,(first fixnums))
		       (%+ . ,(rest fixnums)))))))

(defmacro %1+ (fixnum)
  `(the fixnum (1+ (the fixnum ,fixnum))))

(defmacro %1- (fixnum)
  `(the fixnum (1- (the fixnum ,fixnum))))


(defmacro %- (&REST fixnums)
  (declare (list fixnums))
  (case (length fixnums)
    (0 0)
    (1 `(the fixnum (- (the fixnum ,(first fixnums)))))
    (2 `(the fixnum (- (the fixnum ,(first fixnums))
		       (the fixnum ,(second fixnums)))))
    (T `(the fixnum (- (the fixnum ,(first fixnums))
		       (%+ . ,(rest fixnums)))))))

(defmacro %* (&REST fixnums)
  (declare (list fixnums))
  (case (length fixnums)
    (0 0)
    (1 (first fixnums))
    (2 `(the fixnum (* (the fixnum ,(first fixnums))
		       (the fixnum ,(second fixnums)))))
    (T `(the fixnum (* (the fixnum ,(first fixnums))
		       (%* . ,(rest fixnums)))))))


(defmacro make-fixnum (&REST variables)
  `(progn
     . ,(mapcar #'(lambda (x)
		    `(setq ,x (the (values fixnum number) (floor ,x))))
		variables)))

#+ignore
(defmacro make-fixnum (&REST variables)
  `(progn
     . ,(mapcar #'(lambda (x)
		    `(setq ,x (the (values fixnum number) (floor ,x))))
		variables)))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
