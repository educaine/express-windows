;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)

(defun decode-arglist (arglist)
  (declare (values required-args optional-args keyword-args aux-args))
  (let ((required-args NIL)
	(optional-args NIL)
	(keyword-args NIL)
	(aux-args NIL))
    (do ((args arglist (cdr args))
	 (arg-type :REQUIRED))
	((null args))
      (let ((arg (first args)))
	(case arg-type
	  (:REQUIRED
	    (case arg
	      (&KEY (setq arg-type :KEY))
	      (&AUX (setq arg-type :AUX))
	      (&OPTIONAL (setq arg-type :OPTIONAL))
	      (T (push arg required-args))))
	  (:OPTIONAL
	    (case arg
	      (&KEY (setq arg-type :KEY))
	      (&AUX (setq arg-type :AUX))
	      (&OPTIONAL (error "Extra &OPTIONAL in arglist"))
	      (T (push arg optional-args))))
	  (:KEY
	    (case arg
	      (&KEY (error "Extra &KEY in arglist."))
	      (&AUX (setq arg-type :AUX))
	      (&OPTIONAL (error "&OPTIONAL in wrong place in arglist"))
	      (T (push arg keyword-args))))
	  (:AUX
	    (case arg
	      ((&KEY &AUX &OPTIONAL) (error "Extra ~A in arglist" arg))
	      (T (push arg aux-args))))
	  )))
    (values (nreverse required-args) (nreverse optional-args) (nreverse keyword-args)
	    (nreverse aux-args))))



(defun parse-presentation-function-args (valid-args args number-required error-message)
  (multiple-value-bind (required optional keyword aux)
      (decode-arglist args)
    (declare (list required optional keyword aux))
    (when optional (error "Parsers have no optional keywords."))
    (unless (%= number-required (length required))
      (error error-message required))
    (when (set-difference keyword valid-args :KEY #'SYMBOL-NAME :TEST #'EQUALP)
      (error "Keyword Args ~A not allowed in presentation."
	     (set-difference keyword valid-args :KEY #'SYMBOL-NAME :TEST #'EQUALP)))
    ;; I'm assuming default values for the args are not allowed.
    (let ((args NIL)
	  (ignore-args NIL))
      (dolist (valid-arg valid-args)
	(let ((arg (find (symbol-name valid-arg) keyword :TEST #'EQUALP :KEY #'SYMBOL-NAME)))
	  (if arg
	      (push arg args)
	      (progn (push valid-arg args)
		     (push valid-arg ignore-args)))))
      (values (append required (nreverse args) aux)
	      ignore-args))))


(defvar *Valid-Presentation-Query-values-displayer-Keyword-Args*
	'(type original-type provide-default))

(defun parse-presentation-query-values-displayer-args (args)
  (parse-presentation-function-args
    *Valid-Presentation-Query-values-displayer-Keyword-Args* args
    3 "Query-values-displayers take three required arguments."))


(defvar *Valid-Presentation-Highlight-Keyword-Args*
	'(type original-type presentation x y))

(defun parse-presentation-highlight-args (args)
  (parse-presentation-function-args
    *Valid-Presentation-Highlight-Keyword-Args* args
    1 "Highlighting Box Function take one required argument."))

(defvar *Valid-Presentation-Parser-Keyword-Args*
	'(type original-type initially-display-possibilities ;;predicate 
	       default default-supplied default-type))

(defun parse-presentation-parser-args (args)
  (parse-presentation-function-args
    *Valid-Presentation-Parser-Keyword-Args* args
    1 "Parsers take only one required argument (a stream argument). ~A"))

(defvar *Valid-Presentation-Printer-Keyword-Args*
	'(type queryably original-type)) ;; for-context-type

(defun parse-presentation-printer-args (args)
  (parse-presentation-function-args
    *Valid-Presentation-Printer-Keyword-Args* args
    2 "Printers take exactly two required argument (an object and a stream argument). ~A"))

(defvar *Valid-Presentation-Preprocessor-Keyword-Args*
	'(type original-type default-type))

(defun parse-presentation-preprocessor-args (args)
  (parse-presentation-function-args
    *Valid-Presentation-Preprocessor-Keyword-Args* args
    1 "Preprocessors take exactly one required argument (an object). ~A"))

(defvar *Valid-Presentation-Describer-Keyword-Args*
	'(type plural-count original-type))

(defun parse-presentation-describer-args (args)
  (parse-presentation-function-args
    *Valid-Presentation-Describer-Keyword-Args* args
    1 "Describers take only one required argument (a stream argument). ~A"))

(defmacro define-type (type-name arglist &REST args)
  (let ((data-arglist (car arglist))
	(pr-arglist (cdr arglist))
	(printer (getf args :PRINTER))
	(preprocessor (getf args :DEFAULT-PREPROCESSOR))
	(typep (getf args :TYPEP))
	(describer (getf args :DESCRIBER))
	(query-values-displayer (getf args :QUERY-VALUES-DISPLAYER))
	(parser (getf args :PARSER))
	(no-deftype (getf args :NO-DEFTYPE))
	(description (getf args :DESCRIPTION))
	(expander (getf args :EXPANDER))
	(highlighting-box-function (getf args :HIGHLIGHTING-BOX-FUNCTION)))
    data-arglist pr-arglist printer
    (let ((satisfies-function-name
	    (and (not no-deftype) (intern (lisp:format NIL "satisfies-~A" type-name))))
	  (printer-function-name (intern (lisp:format NIL "printer-~A" type-name)))
	  (preprocessor-function-name (intern (lisp:format NIL "preprocessor-~A" type-name)))
	  (typep-function-name (intern (lisp:format NIL "typep-~A" type-name)))
	  (expander-function-name (intern (lisp:format NIL "expander-~A" type-name)))
	  (describer-function-name (intern (lisp:format NIL "describer-~A" type-name)))
	  (parser-function-name (intern (lisp:format NIL "parser-~A" type-name)))
	  (highlighting-box-function-name (intern (lisp:format NIL "highlight-~A"
							       type-name)))
	  (query-values-displayer-function-name
	    (intern (lisp:format NIL "avd-~A" type-name))))
      `(progn
	 ,@(if highlighting-box-function
	       (let ((highlight-args (first highlighting-box-function)))
		 (multiple-value-bind (args arg-ignores)
		     (parse-presentation-highlight-args highlight-args)
		   (when (or data-arglist pr-arglist)
		     (setq arg-ignores
			   (delete "TYPE" arg-ignores :TEST #'EQUALP :KEY #'SYMBOL-NAME)))
		   `((defun ,highlighting-box-function-name ,args
		       #.(fast)
		       (declare (ignore . ,arg-ignores))
		       ,(add-lexical-bindings-for-args
			  (third args) data-arglist pr-arglist
			  (cdr highlighting-box-function)))
		     (setf (get ',type-name :TYPE-HIGHLIGHTING-BOX-FUNCTION)
			   ',highlighting-box-function-name))))
	       `((remprop ',type-name :TYPE-HIGHLIGHTING-BOX-FUNCTION)))
	 ,@(if description
	       `((setf (get ',type-name :TYPE-DESCRIPTION)
		       ,description))
	       `((remprop ',type-name :TYPE-DESCRIPTION)))
	 ,@(if typep
	       `((setf (get ',type-name :type-typep) T)
		 ,(let ((type (gensym "TYPE")))
		    `(defun ,typep-function-name ,(list (caar typep) type)
		       ,type
		       ,(add-lexical-bindings-for-args
			  type data-arglist NIL (cdr typep)))))
	       `((remprop ',type-name :type-typep)))
	 ,@(if expander
	       `((setf (get ',type-name :EXPANDER) ',expander-function-name)
		 ,(let ((type (gensym "TYPE")))
		    `(defun ,expander-function-name (,type)
		       ,(add-lexical-bindings-for-args
			  type data-arglist pr-arglist `(',(car (cdr expander)))))))
	       `((remprop ',type-name :EXPANDER)))
	 ,@(if no-deftype
	       `((remprop ',type-name :type-type))
	       `((setf (get ',type-name :type-type) T)
		 (defun ,satisfies-function-name (arg)
		   . ,(if typep `((funcall ',typep-function-name arg))
			`((declare (ignore arg))
			  T)))
		 (deftype ,type-name (&rest args) ;; ,data-arglist
		   (declare (ignore args)) ;; . ,data-arglist
		   '(satisfies ,satisfies-function-name))))
	 ,@(if printer
	       (let ((printer-args (first printer)))
		 (multiple-value-bind (args arg-ignores)
		     (parse-presentation-printer-args printer-args)
		   (when (or data-arglist pr-arglist)
		     (setq arg-ignores
			   (delete "TYPE" arg-ignores :TEST #'EQUALP :KEY #'SYMBOL-NAME)))
		   `((defun ,printer-function-name ,args
		       #.(fast)
		       (declare (ignore . ,arg-ignores))
		       ;; 5/1/90 (sln) suppress compiler warnings about unused vars
		       ,@(remove t (set-difference args arg-ignores)
				 :test #'(lambda (a1 a2) (declare (ignore a1)) (member a2 lambda-list-keywords)))
		       ,(add-lexical-bindings-for-args
			  (third args) data-arglist pr-arglist (cdr printer)))
		     (setf (get ',type-name :TYPE-PRINTER) ',printer-function-name))))
	       `((remprop ',type-name :TYPE-PRINTER)))
	 ,@(if preprocessor
	       (let ((preprocessor-args (first preprocessor)))
		 (multiple-value-bind (args arg-ignores)
		     (parse-presentation-preprocessor-args preprocessor-args)
		   (when (or data-arglist pr-arglist)
		     (setq arg-ignores
			   (delete "TYPE" arg-ignores :TEST #'EQUALP :KEY #'SYMBOL-NAME)))
		   `((defun ,preprocessor-function-name ,args
		       (declare (ignore . ,arg-ignores))
		       ;; 5/1/90 (sln) suppress compiler warnings about unused vars
		       ,@(remove t (set-difference args arg-ignores)
				 :test #'(lambda (a1 a2) (declare (ignore a1)) (member a2 lambda-list-keywords)))
		       ,(add-lexical-bindings-for-args
			  (third args) data-arglist pr-arglist (cdr preprocessor)))
		     (setf (get ',type-name :TYPE-PREPROCESSOR)
			   ',preprocessor-function-name))))
	       `((remprop ',type-name :TYPE-PREPROCESSOR)))
	 ,@(if describer
	       (let ((describer-args (first describer)))
		 (multiple-value-bind (args arg-ignores)
		     (parse-presentation-describer-args describer-args)
		   (when (or data-arglist pr-arglist)
		     (setq arg-ignores
			   (delete "TYPE" arg-ignores :TEST #'EQUALP :KEY #'SYMBOL-NAME)))
		   `((defun ,describer-function-name ,args
		       (declare (ignore . ,arg-ignores))
		       ;; 5/1/90 (sln) suppress compiler warnings about unused vars
		       ,@(remove t (set-difference args arg-ignores)
				 :test #'(lambda (a1 a2) (declare (ignore a1)) (member a2 lambda-list-keywords)))
		       ,(add-lexical-bindings-for-args
			  (second args) data-arglist pr-arglist (cdr describer)))
		     (setf (get ',type-name :TYPE-DESCRIBER)
			   ',describer-function-name))))
	       `((remprop ',type-name :TYPE-DESCRIBER)))
	 ,@(if parser
	       (let ((parser-args (first parser)))
		 (multiple-value-bind (args arg-ignores)
		     (parse-presentation-parser-args parser-args)
		   (when (or data-arglist pr-arglist)
		     (setq arg-ignores
			   (delete "TYPE" arg-ignores :TEST #'EQUALP :KEY #'SYMBOL-NAME)))
		   `((defun ,parser-function-name ,args
		       #.(fast)
		       (declare (ignore . ,arg-ignores))
		       ;; 5/1/90 (sln) suppress compiler warnings about unused vars
		       ,@(remove t (set-difference args arg-ignores)
				 :test #'(lambda (a1 a2) (declare (ignore a1)) (member a2 lambda-list-keywords)))
		       ,(add-lexical-bindings-for-args
			  (second args) data-arglist pr-arglist (cdr parser)))
		     (setf (get ',type-name :TYPE-PARSER) ',parser-function-name))))
	       `((remprop ',type-name :TYPE-PARSER)))
	 ,@(if query-values-displayer
	       (let ((query-values-displayer-args (first query-values-displayer)))
		 (multiple-value-bind (args arg-ignores)
		     (parse-presentation-query-values-displayer-args
		       query-values-displayer-args)
		   (when (or data-arglist pr-arglist)
		     (setq arg-ignores
			   (delete "TYPE" arg-ignores :TEST #'EQUALP :KEY #'SYMBOL-NAME)))
		   `((defun ,query-values-displayer-function-name ,args
		       (declare (ignore . ,arg-ignores))
		       ;; 5/1/90 (sln) suppress compiler warnings about unused vars
		       ,@(remove t (set-difference args arg-ignores)
				 :test #'(lambda (a1 a2) (declare (ignore a1)) (member a2 lambda-list-keywords)))
		       ,(add-lexical-bindings-for-args
			  (fourth args) data-arglist pr-arglist (cdr query-values-displayer)))
		     (setf (get ',type-name :TYPE-QUERY-VALUES-DISPLAYER)
			   ',query-values-displayer-function-name))))
	       `((remprop ',type-name :TYPE-QUERY-VALUES-DISPLAYER)))))))



(defun call-type-preprocessor (default type original-type default-type)
  (with-type-decoded (type-name) type
    (cond ((get type-name :TYPE-PREPROCESSOR)
	   (funcall (get type-name :TYPE-PREPROCESSOR)
		    default type original-type default-type))
	  ((get type-name :EXPANDER)
	   (call-type-preprocessor
	     default (funcall (get type-name :EXPANDER) type) original-type default-type))
	  (T default))))

(defun get-type-parser (type)
  (declare (values type parser-function))
  (with-type-decoded (type-name) type
    (if (get type-name :TYPE-PARSER)
	(values type (get type-name :TYPE-PARSER))
	(if (get type-name :EXPANDER)
	    (get-type-parser (funcall (get type-name :EXPANDER) type))
	    (values type #'QUERY-DEFAULT-PARSER)))))

(defun get-type-describer (type)
  (declare (values type describer-function))
  (with-type-decoded (type-name) type
    (if (get type-name :TYPE-DESCRIBER)
	(values type (get type-name :TYPE-describer))
	(if (get type-name :EXPANDER)
	    (get-type-describer (funcall (get type-name :EXPANDER) type))
	    (values type NIL)))))

(defun describe-type (type &OPTIONAL (stream *standard-output*)
		      plural-count reason)
  (with-type-decoded (type-name) type
    (let ((describer (get type-name :TYPE-DESCRIBER))
	  (description (get type-name :TYPE-DESCRIPTION)))
      (when (or describer description)
	(cond (description
	       (simple-princ description stream))
	      (describer
	       (funcall describer stream type plural-count NIL))
	      ((get type-name :EXPANDER)
	       (describe-type (funcall (get type-name :EXPANDER) type)
			      stream plural-count reason)))))))

(defun get-type-printer (type)
  (declare (values type printer-function))
  (with-type-decoded (type-name) type
    (if (get type-name :TYPE-PRINTER)
	(values type (get type-name :TYPE-PRINTER))
	(if (get type-name :EXPANDER)
	    (get-type-printer (funcall (get type-name :EXPANDER) type))
	    (values type #'DEFAULT-DISPLAY-PRINTER)))))

(defun get-type-query-values-displayer (type)
  (declare (values type query-values-displayer-function))
  (with-type-decoded (type-name) type
    (if (get type-name :TYPE-QUERY-VALUES-DISPLAYER)
	(values type (get type-name :TYPE-QUERY-VALUES-DISPLAYER))
	(if (get type-name :EXPANDER)
	    (get-type-query-values-displayer (funcall (get type-name :EXPANDER) type))
	    (values type #'DEFAULT-STANDARD-QUERY-VALUES-DISPLAYER)))))

(defun decode-argument (arg)
  #.(fast)
  (if (consp arg)
      (values (first arg) (second arg) (third arg))
      arg))

(defun keyword-present-p (keyword list)
  #.(fast)
  (do ((sub-list list (cddr sub-list)))
      ((null sub-list) NIL)
    (when (eq keyword (first sub-list)) (return T))))

(defun add-lexical-bindings-for-args (type-arg data-arglist display-arglist body)
  (let ((type-name (gensym "TYPE-NAME"))
	(data-args (gensym "DATA-ARGS"))
	(display-args (gensym "DISPLAY-ARGS")))
    (let ((data-args-code
	    (do ((args data-arglist (cdr args))
		 (result NIL)
		 (arg-type :REQUIRED))
		((null args) (nreverse result))
	      (let ((arg (first args)))
		(case arg-type
		  (:REQUIRED
		    (case arg
		      (&KEY (setq arg-type :KEY))
		      (&AUX (return (nreverse result)))
		      (&OPTIONAL (setq arg-type :OPTIONAL))
		      (&REST (setq arg-type :REST))
		      (T (push `(,arg (pop ,data-args)) result))))
		  (:OPTIONAL
		    (case arg
		      (&KEY (setq arg-type :KEY))
		      (&AUX (return (nreverse result)))
		      (&OPTIONAL (error "Arglist has two &OPTIONAL ~A" data-args))
		      (&REST (setq arg-type :REST))
		      (T (multiple-value-bind (arg-name value supplied-argument)
			     (decode-argument arg)
			   (push `(,arg-name (if ,data-args (pop ,data-args) ,value)) result)
			   (if supplied-argument
			       (push `(,supplied-argument (not (null ,data-args))) result))))))
		  (:REST
		    (unless (member arg lambda-list-keywords)
		      (push `(,arg ,data-args) result)))
		  (:KEY
		    (case arg
		      ((&KEY &REST &OPTIONAL) (error "Arglist has two &OPTIONAL ~A" data-args))
		      (&AUX (return (nreverse result)))
		      (T (multiple-value-bind (arg-name value supplied-argument)
			     (decode-argument arg)
			   (let ((keyword (intern (symbol-name arg-name)
						  (find-package "KEYWORD"))))
			     (push `(,arg-name (getf ,data-args ,keyword ,value)) result)
			     (if supplied-argument
				 (push `(,supplied-argument
					 (keyword-present-p keyword ,data-args))
				       result)))))))))))
	  (display-args-code
	    (do ((args (cdr display-arglist) (cdr args))
		 (result NIL))
		((null args) (nreverse result))
	      (let ((arg (first args)))
		(multiple-value-bind (arg-name value supplied-argument)
		    (decode-argument arg)
		  (let ((keyword (intern (symbol-name arg-name)
					 (find-package "KEYWORD"))))
		    (push `(,arg-name (getf ,display-args ,keyword ,value)) result)
		    (if supplied-argument
			(push `(,supplied-argument
				(keyword-present-p keyword ,display-args))
			      result)))))))
	  ;; 4/30/90 (sln) This collects the arguments in a list for use
	  ;; down below.  This code does not handle the case where a
	  ;; keyword parameter is specified with a different variable
	  ;; name (e.g. &key ((:my-key my-var)) <default-value>
	  ;; <s-var>).  But then again, I don't think the other code in
	  ;; the function does either.
	  (ignored-args
	    (loop for arg in (append data-arglist display-arglist)
		  if (consp arg) collect (first arg)
		  else unless (member arg lambda-list-keywords)
			 collect arg))
	  )
      (if (or display-args-code data-args-code)
	  `(with-type-decoded ,(cond (display-args-code
				      `(,type-name ,data-args ,display-args))
				     (data-args-code
				      `(,type-name ,data-args))
				     (T `(,type-name)))
			      ,type-arg
	     (declare (ignore ,type-name))
	     ;; 5/1/90 (sln) Again, let's reference them at least once
	     ;; to suppress compilation warnings.
	     ,@(cond (display-args-code `(,data-args ,display-args))
		     (data-args-code `(,data-args)))
	     (let* ,data-args-code
	       (let* ,display-args-code
		 ;; 4/30/90 (sln) by using all the args here, we keep
		 ;; the compiler from complaining about the ones that we
		 ;; don't use in the body.  Neeto hack, huh?!
		 ,@ignored-args nil		; we put nil here in case body is nil,
						; so we don't return the last arg in ignored-args
		 . ,body)))
	  `(progn . ,body)))))


;;; ****************************************************************
;;; ****************************************************************
;;; *************** TYPE Matching stuff ****************************
;;; ****************************************************************
;;; ****************************************************************

(defun match-type-to-no-type-context (context-type handler)
  #.(fast)
  (let ((to-type (handler-to-type handler)))
    (when (and (consp to-type) (consp (first to-type))) (setq to-type (first to-type)))
    (when (and (consp context-type) (consp (first context-type)))
      (setq context-type (first context-type)))
    (fast-subtypep to-type context-type)))




;;; before We spent almost all the time in doing subtypep checks during mouse tracking
;;; checking for sensitive presentations.  This is much faster, since we cache previous
;;; calls to subtypep.  Eventually, though the table of types will get big and may have
;;; pointers to data the user wish was garbage collected.  I'm not sure yet where we should
;;; make this automatically flush the table every once in awhile.  Maybe at the top level
;;; command loop it should clear the cache between commands.

(defun fast-subtypep (type1 type2)
  (let ((previous-subtypeps (gethash type2 *Subtypep-Cache-Table*)))
    (if previous-subtypeps
	(let ((entry (assoc type1 previous-subtypeps)))
	  (if entry
	      (cdr entry)
	      (let ((result (my-subtypep type1 type2)))
		(push (cons type1 result) (cdr previous-subtypeps))
		result)))
	(let ((result (my-subtypep type1 type2)))
	  (setf (gethash type2 *Subtypep-Cache-Table*) (list (cons type1 result)))
	  result))))

(defun my-subtypep (type1 type2)
  #.(fast)
  (when (and (consp type1) (consp (first type1))) (setq type1 (car type1)))
  (when (and (consp type2) (consp (first type2))) (setq type2 (car type2)))
  (cond #+excl
	((equal type1 type2) T)
	((and (consp type2) (member (first type2) '(or sequence pathname string)))
	 (case (first type2)
	   (PATHNAME (my-subtypep type1 'PATHNAME))
	   (STRING
	     (if (> (length type2) 2)
		 (if (null (second type2))
		     (my-subtypep type1 'string)
		     (my-subtypep type1 (list 'string (second type2))))
		 (my-subtypep type1 type2)))
	   (OR (dolist (type (cdr type2) NIL)
		 (when (my-subtypep type1 type) (return T))))
	   (SEQUENCE (subtypep type1 'sequence))
	   (T (subtypep type1 type2))))
	((and (consp type1) (member (first type1) '(or sequence pathname string)))
	 (case (first type1)
	   (PATHNAME (my-subtypep type2 'PATHNAME))
	   (STRING
	     (if (> (length type1) 2)
		 (if (null (second type1))
		     (my-subtypep 'string type2)
		     (my-subtypep (list 'string (second type1)) type2))
		 (my-subtypep type2 type1)))
	   (OR (dolist (type (cdr type1) NIL)
		 (when (my-subtypep type type2) (return T))))
	   (SEQUENCE (subtypep 'sequence type2))
	   (T (subtypep type1 type2))))
	;; lucid has this screw, that a pathname is this of type lucid::%pathname, but
	;; it is not recognized by subtypep.
	#+lucid
	((eq type1 'lucid::%pathname)
	 (if (eq type2 'lucid::%pathname) T (my-subtypep 'pathname type2)))
	#+lucid
	((eq type2 'lucid::%pathname)
	 (my-subtypep type1 'pathname))
	(T (or (subtypep type1 type2)
	       ;; check for case where one is a cons and is system defined.
	       ;; disgusting hack for now.
	       #+excl
	       (and (symbolp type1)
		    (get type1 :type-type)
		    (consp type2)
		    (symbolp (car type2))
		    (eq type1 (first type2)))
	       #+excl
	       (and (symbolp type2)
		    (get type2 :type-type)
		    (consp type1)
		    (symbolp (car type1))
		    (eq type2 (first type1)))
	       ;; disgusting hack that should be fixed with a :TYPE-SUBTYEP option
	       ;; to command or command-or-form someday.
	       (and (member type1 '(command command-form))
		    (or (eq type2 'command-or-form)
			(and (consp type2)
			     (eq (car type2) 'command-or-form))))
	       (and (member type2 '(command command-form))
		    (or (eq type1 'command-or-form)
			(and (consp type1)
			     (eq (car type1) 'command-or-form))))
	       ;; examine definition of pcl::find-class to figure it out.
	       ;; 6/14/90 (sln) put #+pcl.  I hope this is okay.
	       #+pcl
	       (and (symbolp type1)
		    (symbolp type2)
		    (let ((class1 (gethash type1 pcl::*find-class*)))
		      (and class1
			   (let ((class2 (gethash type2 pcl::*find-class*)))
			     (and class2
				  (member class2 (pcl::class-precedence-list class1)))))))))))

;; 6/14/90 (sln) only pcl needs this.
#+pcl
(unless (or #+symbolics (>= 8 (sct:get-release-version)))
  (defun type-of (object)
    #.(fast)
    (if #+pcl-victoria (pcl::iwmc-class-p object) 
	#+pcl-may (pcl::standard-class-p object)
	#+pcl-victoria (typep object 'pcl::iwmc-class)
	#+pcl-may (pcl::class-name (pcl::class-of object))
	(lisp-type-of object))))



;;; has to handle presentation types.
(defun typep (object type)
  #.(fast)
  (cond ((consp type)
	 (case (first type)
	   (OR (dolist (type-items (cdr type) NIL)
		 (when (typep object type-items) (return T))))
	   (AND (dolist (type-items (cdr type) T)
		  (unless (typep object type-items) (return NIL))))
	   (NOT (not (typep object (second type))))
	   (STRING ;; special case since we have extended the meaning of string.
	     (and (stringp object)
		  (if (integerp (second type))
		      (= (length (the string object))
			 (the fixnum (second type)))
		      T)
		  (if (third type)
		      (> (length (the string object))
			 (the fixnum (if (integerp (third type))
					 (third type) (%1- (first (third type))))))
		      T)
		  (if (fourth type)
		      (< (length (the string object))
			 (the fixnum (if (integerp (fourth type))
					 (fourth type) (%1+ (first (fourth type))))))
		      T)))
	   (NUMBER ;; extend number to be like INTEGER
	     (and (numberp object)
		  ;; check bounds
		  (if (second type)
		      (if (integerp (second type))
			  (< object (second type))
			  (<= object (first (second type))))
		      T)
		  (if (third type)
		      (if (integerp (third type))
			  (< object (third type))
			  (<= object (first (third type))))
		      T)))
	   (SEQUENCE ;; extend sequence to handle a type
	     (and (or (listp object)
		      (arrayp object))
		  (every #'(lambda (item)
			     (typep item (second type)))
			 object)
		  (or (not (cddr type))
		      (let ((length (length object)))
			(declare (fixnum length))
			(and (cond ((integerp (third type))
				    (<= (the fixnum (third type)) length))
				   ((and (consp (third type))
					 (integerp (first (third type))))
				    (< (the fixnum (first (third type))) length))
				   (T T))
			     (cond ((integerp (fourth type))
				    (<= length (the fixnum (fourth type))))
				   ((and (consp (fourth type))
					 (integerp (first (fourth type))))
				    (< length (the fixnum (first (fourth type)))))
				   (T T)))))))
	   (PATHNAME
	     (pathnamep object))
	   (T (cond ((consp (first type))
		     (typep object (first type)))
		    ((get (first type) :type-typep)
		     (funcall (get (first type) :type-typep) object type))
		    ((get (first type) :EXPANDER)
		     (typep object (funcall (get (first type) :EXPANDER) type)))
		    (T (lisp-typep object type))))))
	((get type :type-typep)
	 (funcall (get type :type-typep) object type))
	((get type :EXPANDER)
	 (typep object (funcall (get type :EXPANDER) type)))
	(T (lisp-typep object type))))

(defun match-type-to-context (context-type handler display-type presentation)
  #.(fast)
  #+debug-type
  (dformat "Match Type Context Type ~A handler ~A Display-type ~S Presentation ~S")
  (meter match-type
  (when (and (consp context-type) (consp (first context-type)))
    (setq context-type (first context-type)))
  (cond ((and (consp context-type)
	      (member (first context-type) '(OR AND SATISFIES)))
	 (case (first context-type)
	   (OR
	     (dolist (type (cdr context-type) NIL)
	       (when (match-type-to-context type handler display-type presentation)
		 (return T))))
	   (AND
	     (dolist (type (cdr context-type) T)
	       (unless (match-type-to-context type handler display-type presentation)
		 (return NIL))))
	   (SATISFIES
	     (funcall (second context-type) (presentation-object presentation)))))
	(T (if (eq (handler-name handler) 'EQUALITY-TRANSLATOR)
	       (if (eq display-type 'EXPRESSION)
		   ;; we do it differently because we actually look at the object instead
		   ;; of just the type.
		   (progn
		     #+debug-type
		     (dformat "MTYPE CASE I.")
		     (or (fast-subtypep (type-of (presentation-object presentation))
					context-type)
			 (with-type-decoded (from-type-name) context-type
			   (and (or (not (get from-type-name :TYPE-TYPE))
				    (get from-type-name :TYPE-TYPEP))
				(typep (presentation-object presentation) context-type)))))
		   (progn
		     #+debug-type
		     (dformat "MTYPE CASE II.")
		     ;; the type we are looking for must be a subtype of what is displayed.
		     (or (eq context-type display-type)
			 (fast-subtypep display-type context-type)
			 (and (eq (if (consp context-type)
				      (first context-type) context-type)
				  (if (consp display-type)
				      (first display-type) display-type))
			      (typep (presentation-object presentation) context-type))
			 )))
	       ;; not an equality translator, so we need to actually look at the translator's
	       ;; to and from types.
	       (let ((to-type (handler-to-type handler))
		     (from-type (handler-from-type handler)))
		 (when (and (consp to-type) (consp (first to-type)))
		   (setq to-type (first to-type)))
		 (when (and (consp from-type) (consp (first from-type)))
		   (setq from-type (first from-type)))
		 (if (eq display-type 'EXPRESSION)
		     ;; again we treat expression objects specially by looking at the object's type.
		     (with-type-decoded (from-type-name) from-type
		       #+debug-type
		       (dformat "MTYPE CASE III.")
		       (and (or (not (get from-type-name :TYPE-TYPE))
				(get from-type-name :TYPE-TYPEP))
			    ;; due to kludge of EXPRESSIONs matching on every bloody
			    ;; presentation type I have defined, only match an EXPRESSION
			    ;; object against either a system defined type, or
			    ;; a presentation type that include a TYPEP predicate.
			
			    (typep (presentation-object presentation) from-type)
			    (or (eq to-type T)
				(fast-subtypep to-type context-type))))
		     #+ignore
		     (or (fast-subtypep (type-of (presentation-object presentation))
				      from-type)
			 (fast-subtypep from-type
				      (type-of (presentation-object presentation))))
		     ;; not and equality and not an expression.
		     (progn
		       #+debug-type
		       (dformat "MTYPE CASE IV.")
		       (and (not (eq display-type 'NO-TYPE))
			    (not (eq from-type 'no-type))
			    (or (eq to-type T)
				(fast-subtypep to-type context-type))
			    (fast-subtypep display-type from-type))))))))))


(defun match-handler-to-context (context-type handler)
  #.(fast)
  (meter match-handler-type
  (when (and (consp context-type) (consp (first context-type)))
    (setq context-type (first context-type)))
  (cond ((and (consp context-type)
	      (member (first context-type) '(OR AND SATISFIES)))
	 (case (first context-type)
	   (OR
	     (dolist (type (cdr context-type) NIL)
	       (when (match-handler-to-context type handler)
		 (return T))))
	   (AND
	     (dolist (type (cdr context-type) T)
	       (unless (match-handler-to-context type handler)
		 (return NIL))))
	   (SATISFIES T)))
	(T (if (eq (handler-name handler) 'EQUALITY-TRANSLATOR)
	       T
	       ;; not an equality translator, so we need to actually look at the translator's
	       ;; to and from types.
	       (let ((to-type (handler-to-type handler))
		     (from-type (handler-from-type handler)))
		 (when (and (consp to-type) (consp (first to-type)))
		   (setq to-type (first to-type)))
		 ;; not and equality and not an expression.
		 (and (not (eq from-type 'no-type))
		      (or (eq to-type T)
			  (fast-subtypep to-type context-type)))))))))



(defvar *Cached-Handlers-for-context* NIL)
(defvar *Cached-Handlers-Context* NIL)
(defvar *Cached-Handlers-Mouse-Shifts* NIL)

(defun get-handlers-for-context ()
  (if (and (eq *cached-handlers-context* *input-context*)
	   (eq (mouse-chord-shifts) *Cached-Handlers-Mouse-Shifts*))
      *Cached-Handlers-for-Context*
      (setq *Cached-Handlers-Mouse-Shifts* (mouse-chord-shifts)
	    *cached-handlers-context* *input-context*
	    *Cached-Handlers-for-context*
	    (mapcan #'(lambda (handler)
			(when (dolist (context *Input-Context* NIL)
				(when (match-handler-to-context context handler)
				  (return T)))
			  (list handler)))
		    (get-handlers-by-mouse-char *Cached-Handlers-Mouse-Shifts*)))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
