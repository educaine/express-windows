;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */



;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************



(cl:in-package 'ew)

(defmacro with-redisplayable-output ((&KEY (stream '*standard-output*) cache-value
					   unique-id (cache-test '#'eql)
					   copy-cache-value (id-test '#'eql) &ALLOW-OTHER-KEYS)
				     &BODY body)
  `(memoize (:VALUE ,cache-value :STREAM ,stream :VALUE-TEST ,cache-test
	     :copy-value ,copy-cache-value :id ,unique-id :id-test ,id-test)
	    . ,body))

(defmacro formatting-table ((&OPTIONAL (stream '*Standard-Output*)
				       &KEY equalize-column-widths extend-width
				       extend-height (inter-row-spacing 0)
				       inter-column-spacing
				       multiple-columns
				       (multiple-column-inter-column-spacing)
				       equalize-multiple-column-widths
				       output-multiple-columns-row-wise
				       &ALLOW-OTHER-KEYS)
			    &BODY body)
  `(make-table (,stream
		:EQUALIZE-COLUMN-WIDTHS ,equalize-column-widths
		:EXTEND-WIDTH ,extend-width
		:EXTEND-HEIGHT ,extend-height
		:ROW-SPACING ,inter-row-spacing
		:COLUMN-SPACING ,inter-column-spacing
		:MULTIPLE-COLUMNS ,multiple-columns
		:MULTIPLE-COLUMN-COLUMN-SPACING ,multiple-column-inter-column-spacing
		:EQUALIZE-MULTIPLE-COLUMN-WIDTHS ,equalize-multiple-column-widths
		:OUTPUT-MULTIPLE-COLUMNS-ROW-WISE ,output-multiple-columns-row-wise)
     . ,body))

(defmacro formatting-row ((&OPTIONAL (stream '*Standard-Output*)
				     &KEY single-column
				     &ALLOW-OTHER-KEYS)
			  &BODY body)
  `(table-row (,stream :SINGLE-COLUMN ,single-column)
     . ,body))

(defmacro formatting-column-headings ((&OPTIONAL (stream '*Standard-Output*)
				     &KEY underline
				     &ALLOW-OTHER-KEYS)
			  &BODY body)
  `(table-column-headings (,stream :UNDERLINE ,underline)
     . ,body))

(defmacro formatting-cell ((&OPTIONAL (stream '*Standard-Output*)
			    &KEY align-x align-y align
			    &ALLOW-OTHER-KEYS)
			   &BODY body)
  `(entry (,stream :ALIGN-X ,align-x :ALIGN-Y ,align-y :ALIGN ,align)
     . ,body))

(defmacro redisplayer ((&OPTIONAL (stream '*standard-output*)) &BODY body)
  `(memo (,stream)
     . ,body))

(defmacro do-redisplay (redisplay-piece &OPTIONAL (stream '*Standard-Output*)
			&KEY fill-set-cursorpos truncate-p once-only
			save-cursor-position limit-to-viewport)
  `(run-memo ,redisplay-piece ,stream :fill-set-cursorpos ,fill-set-cursorpos
	     :truncate-p ,truncate-p
	     :once-only ,once-only
	     :save-cursor-position ,save-cursor-position
	     :limit-to-viewport ,limit-to-viewport))

(defmacro with-output-as-presentation ((&KEY object type
					     (single-box NIL)
					     (stream '*Standard-Output*)
					     (allow-sensitive-inferiors T)
					     &ALLOW-OTHER-KEYS)
				       &BODY body)
  `(display-as (:STREAM ,stream :object ,object :type ,type
			:single-box ,single-box
			:allow-sensitive-inferiors ,allow-sensitive-inferiors)
	       . ,body))

(defmacro redisplayable-present (object &OPTIONAL (type (type-of object))
				 &REST options &KEY (stream '*standard-output*)
				 unique-id &ALLOW-OTHER-KEYS)
  `(memo-display ,object ,type :id ,unique-id :stream ,stream . ,options))


(defmacro define-presentation-translator
	  (name
	   (from-presentation-type
	     to-presentation-type
	     &KEY tester (gesture :SELECT)
	     documentation
	     suppress-highlighting
	     (menu T)
	     (context-independent NIL)
	     priority
	     exclude-other-handlers
	     blank-area do-not-compose)
	   arglist &BODY body)
  `(define-type-transform ,name
			  (,from-presentation-type
			   ,to-presentation-type
			   :test ,tester
			   :gesture ,gesture
			   :documentation ,documentation
			   :suppress-highlighting ,suppress-highlighting
			   :menu ,menu
			   :context-independent ,context-independent
			   :priority ,priority
			   :exclude-other-handlers ,exclude-other-handlers
			   :blank-area ,blank-area
			   :include-body-in-test (not ,do-not-compose))
     ,arglist
     . ,body))

(defmacro define-presentation-to-command-translator
	  (name
	   (type
	     &key tester (gesture :SELECT)
	     documentation
	     suppress-highlighting
	     (menu T)
	     priority
	     do-not-compose
	     blank-area)
	   arglist &BODY body)
  `(define-mouse-command ,name
			 (,type
			  :test ,tester
			  :gesture ,gesture
			  :documentation ,documentation
			  :suppress-highlighting ,suppress-highlighting
			  :menu ,menu
			  :priority ,priority
			  :blank-area ,blank-area
			  :include-body-in-test (not ,do-not-compose))
			 ,arglist
     . ,body))

(defmacro define-presentation-action (name
				      (from-presentation-type
					to-presentation-type
					&KEY tester (gesture :SELECT)
					documentation
					suppress-highlighting
					(menu T)
					(context-independent NIL)
					priority
					exclude-other-handlers
					blank-area defines-menu)
				      arglist &BODY body)
  `(define-mouse-action ,name
			(,from-presentation-type
			 ,to-presentation-type
			 :test ,tester
			 :gesture ,gesture
			 :documentation ,documentation
			 :suppress-highlighting ,suppress-highlighting
			 :menu ,menu
			 :context-independent ,context-independent
			 :priority ,priority
			 :exclude-other-handlers ,exclude-other-handlers
			 :blank-area ,blank-area
			 :defines-menu ,defines-menu)
			,arglist
     . ,body))

(dolist (pair '((present display)
		(presentation-replace-input replace-input-editor-string)
		(presentation-type-name type-name)
		(format-item-list make-table-from-sequence)
		(format-textual-list format-list)
		(accept-from-string query-from-string)
		(accept-values query-values)
		(accept-values-choose-from-sequence query-values-choose-from-sequence)
		(accept-values-fixed-line memo-write-string)
		(redisplayable-format memo-format)
		(accept-values-into-list query-values-into-list)
		(compare-char-for-accept compare-char)
		(peek-char-for-accept peek-char-for-query)
		(read-char-for-accept read-char-for-query)
		(unread-char-for-accept unread-char-for-query)
		(prompt-and-accept prompt-and-query)))
  (setf (symbol-function (first pair))(symbol-function (second pair))))

;; simple macro conversions
(defmacro generate-macro-conversions (pairs)
  `(progn . ,(mapcar #'(lambda (pair)
			 `(defmacro ,(first pair) (&rest args)
			    `(,',(second pair) . ,args)))
		     pairs)))

(defmacro named-value-snapshot-continuation (name var-list &BODY body)
  (declare (ignore name))			;4/30/90 (sln)
  `(function (lambda ,var-list . ,body)))

(generate-macro-conversions
  ((accept-values-command-button command-button)
   (describe-presentation-type describe-type)
   (define-presentation-type define-type)

   (format-output-macro-default-stream setup-standard-output-stream-arg)

   (sheet-inside-left window-inside-left)
   (sheet-inside-top window-inside-top)
   (sheet-inside-right window-inside-right)
   (sheet-inside-bottom window-inside-bottom)
   (prepare-sheet prepare-window)

   (with-graphics-identity-transform with-identity-transform)
   (with-graphics-rotation with-rotation)
   (with-graphics-scale with-scaling)
   (with-graphics-translation with-translation)
   (with-graphics-transform with-transform)
   (formatting-item-list make-table-from-generated-sequence)
   (formatting-textual-list formatting-list)
   (formatting-textual-list-element formatting-list-element)
   (accepting-values querying-values)
   (standard-accept-values-displayer standard-query-values-displayer)
   (surrounding-output-with-border with-border)
   (with-accept-activation-chars with-activation-chars)
   (with-accept-blip-chars with-token-delimiters)))

(defun accept (type &REST other-args
	       &KEY blip-chars additional-blip-chars &ALLOW-OTHER-KEYS)
  (apply #'query type :token-delimiter-chars blip-chars
	 :additional-token-delimiter-chars additional-blip-chars
	 other-args))

;; :TESTER keywords changes to :TEST
;; :DO-NOT-COMPOSE changes to :INCLUDE-BODY-IN-TEST
;; command-button - takes expressions or string for prompt, also takes simple commands
;;	for with-border


;; improvements to presentation-types
;; string, symbol, alist-member, boolean, inverted-boolean, universal-time
;; LABEL arg for menu-choose.

(defmacro filling-output ((stream &KEY fill-column fill-characters
				  after-line-break
				  after-line-break-initially-too &Allow-other-keys)
			  &BODY body)
  `(with-output-filling (,stream :fill-column ,fill-column :fill-characters ,fill-characters
			 :after-line-break ,after-line-break
			 :initially-too ,after-line-break-initially-too)
     . ,body))

(defun tv-menu-choose (item-list &OPTIONAL label (near-mode '(:mouse)) default-item
		       superior)
  (declare (ignore superior default-item))
  (menu-choose item-list :LABEL label :NEAR-MODE near-mode))

(defmacro abbreviating-output ((&OPTIONAL stream &KEY width height newline-subsitute
					  show-abbreviation abbreviate-initial-whitespace)
			       &BODY body)
  (declare (ignore abbreviate-initial-whitespace show-abbreviation newline-subsitute height width stream))	;4/30/90 (sln)
  `(progn . ,body))

(defclass dynamic-window-pane
	  (program-display-pane)
     ())

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
