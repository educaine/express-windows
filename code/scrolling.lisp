;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)


(defvar *scroll-percentage* .30)
(proclaim '(float *scroll-percentage*))

(defmethod scroll-window-down ((window window) &OPTIONAL (type :PIXEL) amount)
  (let* ((inside-height (window-inside-height window))
	 (scroll-amount
	   (ecase type
	     (:PIXEL (or amount
			 (the (values fixnum number)
			      (floor (the float
					  (* *scroll-percentage* inside-height))))))
	     (:LINES (the (values fixnum number)
			  (floor (* amount (window-line-height window)))))))
	 (new-y-offset (min (max 0 (%- (presentation-window-max-y-position window)
				       (window-line-height window)))
			    (%+ (window-scroll-y-offset window) scroll-amount))))
    (declare (fixnum inside-height scroll-amount new-y-offset))
    (prepare-window (window)
      (setf (window-scroll-y-offset window) new-y-offset)
      ;; try to be clever if we have only scrolled the window partially
      ;; i.e. bitblt the parts of the window that have just moved, but have not disappeared.
      (if (>= (the fixnum (abs scroll-amount)) inside-height)
	  (redraw-window window)
	  (let ((inside-width (window-inside-width window))
		(x-offset (window-scroll-x-offset window)))
	    (declare (fixnum x-offset inside-width))
	    ;; since we are scrolling down, that means the bottom of the window moves
	    ;; to the top of the window.
	    (bitblt (draw-alu :magic)
		    ;; 10 is a magic number, I think there is a bug in ALU for bitblt
		       inside-width (%- inside-height scroll-amount)
		       window 0 scroll-amount
		       window 0 0)
	    (redraw-window window
			   x-offset
			   (%+ new-y-offset (%- inside-height scroll-amount))
			   (%+ x-offset inside-width)
			   (%+ new-y-offset inside-height))))
      (display-input-editor window))))

(defmethod scroll-window-up ((window window) &OPTIONAL (type :PIXEL) amount)
  (unless (zerop (window-scroll-y-offset window))
    (let* ((inside-height (window-inside-height window))
	   (scroll-amount
	     (min
	       (window-scroll-y-offset window)
	       (ecase type
		 (:PIXEL (or amount (floor (the float (* *scroll-percentage* inside-height)))))
		 (:LINES (the (values fixnum number)
			      (floor (* amount (window-line-height window))))))))
	   (new-y-offset (max 0 (%- (window-scroll-y-offset window) scroll-amount))))
      (declare (fixnum inside-height scroll-amount new-y-offset))
      (prepare-window (window)
	(setf (window-scroll-y-offset window) new-y-offset)
	(if (>= (the fixnum (abs scroll-amount)) inside-height)
	    (redraw-window window)
	    (let ((inside-width (window-inside-width window))
		  (x-offset (window-scroll-x-offset window)))
	      ;; since we are scrolling down, that means the bottom of the window moves
	      ;; to the top of the window.
	      (bitblt (draw-alu :magic) ;; 10 is a magic number, I think there is a bug in ALU for bitblt
			 inside-width (%- inside-height scroll-amount)
			 window 0 0
			 window 0 scroll-amount)
	      (redraw-window window
			     x-offset
			     new-y-offset
			     (%+ x-offset inside-width)
			     (%+ new-y-offset scroll-amount))))
	(display-input-editor window)))))

(defmethod viewport-position ((window window))
  (values (window-scroll-x-offset window) (window-scroll-y-offset window)))

(defmethod set-viewport-position ((window window) left top)
  #.(fast)
  (declare (fixnum left top))
  (let ((old-x-offset (window-scroll-x-offset window))
	(old-y-offset (window-scroll-y-offset window)))
    (declare (fixnum old-x-offset old-y-offset))
    (unless (and (= left old-x-offset)
		 (= top  old-y-offset))
      (prepare-window (window)
	(setf (window-scroll-x-offset window) left
	      (window-scroll-y-offset window) top)
	(let ((width (window-inside-width window))
	      (height (window-inside-height window))
	      (x-scroll-amount (%- left old-x-offset))
	      (y-scroll-amount (%- top  old-y-offset)))
	  (declare (fixnum width height x-scroll-amount y-scroll-amount))
	  (if (or (>= (the fixnum (abs x-scroll-amount)) width)
		  (>= (the fixnum (abs y-scroll-amount)) height))
	      (redraw-window window)
	      ;; we can optimize the redrawing with bitblt.
	      (let ((left-margin (window-inside-left window))
		    (top-margin (window-inside-top window)))
		(declare (fixnum left-margin top-margin))
		(bitblt (draw-alu :magic)
			   (%- width  (abs x-scroll-amount))
			   (%- height (abs y-scroll-amount))
			   window
			   (if (>= x-scroll-amount 0)
			       (%+ left-margin x-scroll-amount) left-margin)
			   (if (>= y-scroll-amount 0)
			       (%+ top-margin y-scroll-amount) top-margin)
			   window
			   (if (>= x-scroll-amount 0)
			       left-margin (%- left-margin x-scroll-amount))
			   (if (>= y-scroll-amount 0)
			       top-margin  (%- top-margin y-scroll-amount)))
		;; now we need to redraw portions of the window
		;; we either need to draw one, or two sections of the window.
		;; first draw the largest rectangle that spans the full width of the window.
		(cond ((> y-scroll-amount 0)
		       (redraw-window window
				      left
				      (%+ top (%- height y-scroll-amount))
				      (%+ left width)
				      (%+ top height))
		       ;; now if there is another chunk to add from a
		       ;; horizontal scroll add it.  but realize, you only
		       ;; need to draw stuff that hasn't already been draw
		       ;; from immediately preceeding redraw.
		       (cond ((> x-scroll-amount 0)
			      (redraw-window window
					     (%+ left (%- width x-scroll-amount))
					     top
					     (%+ left width)
					     (%+ top (%- height y-scroll-amount))))
			     ((< x-scroll-amount 0)
			      (redraw-window window
					     left
					     top
					     (%+ left (%- x-scroll-amount))
					     (%+ top (%- height y-scroll-amount))))))
		      ((< y-scroll-amount 0)
		       (redraw-window window
				      left
				      top
				      (%+ left width)
				      (%+ top (%- y-scroll-amount)))
		       ;; now if there is another chunk to add from a
		       ;; horizontal scroll add it.  but realize, you only
		       ;; need to draw stuff that hasn't already been draw
		       ;; from immediately preceeding redraw.
		       (cond ((> x-scroll-amount 0)
			      (redraw-window window
					     (%+ left (%- width x-scroll-amount))
					     (%+ top (%- y-scroll-amount))
					     (%+ left width)
					     (%+ top height)))
			     ((< x-scroll-amount 0)
			      (redraw-window window
					     left
					     (%+ top (%- y-scroll-amount))
					     (%+ left (%- x-scroll-amount))
					     (%+ top height)))))
		      (T (cond ((> x-scroll-amount 0)
				(redraw-window window
					       (%+ left (%- width x-scroll-amount))
					       top
					       (%+ left width)
					       (%+ top height)))
			       ((< x-scroll-amount 0)
				(redraw-window window
					       left
					       top
					       (%+ left (%- x-scroll-amount))
					       (%+ top height)))))))))
	(display-input-editor window))
      (let ((fcn 'set-viewport-position-after))
	(when (fboundp fcn)
	  (funcall fcn window left top))))))

(defmethod x-scroll-to ((window window) position type)
  (declare (fixnum position))
  (ecase type
    (:ABSOLUTE ())
    (:RELATIVE (incf position (window-scroll-x-offset window)))
    (:RELATIVE-JUMP (setq position (%+ (window-scroll-x-offset window)
				       (%* position (char-unit-width window))))))
  (unless (= (window-scroll-x-offset window) position)
    (setf (window-scroll-x-offset window) position)
    (redraw-window window)
    (let ((fcn 'x-scroll-to-after))
      (when (fboundp fcn)
	(funcall fcn window position)))))

(defmethod y-scroll-to ((window window) position type)
  (declare (fixnum position))
  (ecase type
    (:ABSOLUTE ())
    (:RELATIVE (incf position (window-scroll-y-offset window)))
    (:RELATIVE-JUMP (setq position (%+ (window-scroll-y-offset window)
				       (%* position (char-unit-height window))))))
  (unless (= (window-scroll-y-offset window) position)
    (setf (window-scroll-y-offset window) position)
    (redraw-window window)
    (let ((fcn 'y-scroll-to-after))
      (when (fboundp fcn)
	(funcall fcn window position)))))

(defmethod x-scroll-position ((window window))
  (values (window-scroll-x-offset window)
	  (window-size window)
	  0
	  (%+ (window-scroll-x-offset window)
	      (window-size window))))

(defmethod y-scroll-position ((window window))
  (values (window-scroll-y-offset window)
	  (window-height window)
	  0
	  (%+ (window-scroll-y-offset window)
	      (window-height window))))


(defmethod visible-cursorpos-limits ((window window) &OPTIONAL (units :PIXEL))
  (multiple-value-bind (width height)
      (window-inside-size window)
    (if (eq units :PIXEL)
	(values (window-scroll-x-offset window)
		(window-scroll-y-offset window)
		(%+ (window-scroll-x-offset window) width)
		(%+ (window-scroll-y-offset window) height))
	(multiple-value-bind (char-width char-height)
	    (char-size-from-font #\Space (window-font window))
	  (declare (fixnum char-width char-height))
	  (values (the (values fixnum number)
		       (floor (window-scroll-x-offset window) char-width))
		  (the (values fixnum number)
		       (floor (window-scroll-y-offset window) char-height))
		  (the (values fixnum number)
		       (floor (%+ (window-scroll-x-offset window) width) char-width))
		  (the (values fixnum number)
		       (floor (%+ (window-scroll-y-offset window) height) char-height)))))))

;; three ways to scroll with mouse.
;; LEFT means Line at mouse to TOP
;; RIGHT means  Top line to Mouse
;; MIDDLE means move to percentage in history.

(defun handle-scrolling (command window)
  (cond ((and (lisp:typep window 'SCROLL-BAR) (consp command))
	 ;; handle this by checking percentages.
	 (multiple-value-bind (width height)
	     (window-size window)
	   (declare (fixnum width height))
	   (let* ((main-window (scroll-bar-window-to-scroll window))
		  (line-height (window-line-height main-window)))
	     (declare (fixnum line-height))
	     (flet ((fix-to-grid (amount)
		      (declare (fixnum amount))
		      (%* line-height (the (values fixnum number)
					   (floor amount line-height)))))
	       (ecase (window-margin-type window)
		 ((:LEFT :RIGHT)
		  (let ((y (fifth command))
			(max-y (presentation-window-max-y-position main-window)))
		    (declare (fixnum y max-y))
		    (let ((percentage (the float (/ y (float height)))))
		      (declare (float percentage))
		      (when (< percentage .03) (setq percentage 0.0))
		      (case (second command)
			(#.*Mouse-Left*
			 (set-viewport-position
			   main-window
			   (window-scroll-x-offset main-window)
			   (fix-to-grid (min max-y (%+ (window-scroll-y-offset main-window)
						       y)))))
			(#.*Mouse-Middle*
			 (set-viewport-position
			   main-window
			   (window-scroll-x-offset main-window)
			   (fix-to-grid (floor (the float (* percentage max-y))))))
			(#.*Mouse-Right*
			 (set-viewport-position
			   main-window
			   (window-scroll-x-offset main-window)
			   (fix-to-grid (max 0 (%- (window-scroll-y-offset main-window)
						   y)))))))))
		 ((:BOTTOM :TOP)
		  (let ((x (fourth command))
			(max-x (presentation-window-max-x-position main-window)))
		    (declare (fixnum x max-x))
		    (let ((percentage (the float (/ x (float width)))))
		      (declare (float percentage))
		      (when (< percentage .03) (setq percentage 0.0))
		      (case (second command)
			(#.*Mouse-Left*
			 (set-viewport-position
			   main-window
			   (min max-x (%+ (window-scroll-x-offset main-window) x))
			   (window-scroll-y-offset main-window)))
			(#.*Mouse-Middle*
			 (set-viewport-position
			   main-window
			   (floor (* percentage max-x))
			   (window-scroll-y-offset main-window)))
			(#.*Mouse-Right*
			 (set-viewport-position
			   main-window
			   (max 0 (%- (window-scroll-x-offset main-window) x))
			   (window-scroll-y-offset main-window)))
			)))))))))))


(defvar *Mouse-Scroll-Bar-Documentation-String*
	(make-array 200 :element-type 'string-char :FILL-POINTER T))

(defvar *Last-Scroll-Bar-Window* NIL)
(defvar *Last-Scroll-Bar-Percentage* NIL)

(defmethod show-scroll-bar-documentation ((window window) mouse-x mouse-y &AUX (percentage 0))
  #.(fast)
  (declare (fixnum mouse-x mouse-y percentage))
  ;; handle this by checking percentages.
  (setf (fill-pointer *Mouse-Scroll-Bar-Documentation-String*) 0)
  (multiple-value-bind (width height)
      (window-size window)
    (declare (fixnum width height))
    (ecase (window-margin-type window)
      ((:LEFT :RIGHT)
       (setq percentage (the (values fixnum number)
			     (floor (the float (* 100 (/ mouse-y (float height)))))))
       (when (< percentage 3) (setq percentage 0))
       (with-output-to-string (stream *Mouse-Scroll-Bar-Documentation-String*)
	 (lisp-format stream "Left: Line to Top;   Middle: Move to ~D%;  ~
			Right: Top Line to Mouse."
		      percentage)))
      ((:BOTTOM :TOP)
       (setq percentage (the (values fixnum number)
			     (floor (the float (* 100 (/ mouse-x (float width)))))))
       (when (< percentage 3) (setq percentage 0))
       (with-output-to-string (stream *Mouse-Scroll-Bar-Documentation-String*)
	 (lisp-format stream "Left: Column to Left;   Middle: Move to ~D%;  ~
			Right: Right Column to Mouse."
		      percentage)))))
  (set-mouse-documentation-string
    *Mouse-Scroll-Bar-Documentation-String*
    (or (not (eq *Last-Scroll-Bar-Window* window))
	(not (equal *Last-Scroll-Bar-Percentage* percentage))))
  (setq *Last-Scroll-Bar-Window* window
	*Last-Scroll-Bar-Percentage* percentage))

(defvar *Scroll-Commands* #+symbolics '(#\Triangle #\Ctrl-Triangle) #-symbolics NIL)

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
