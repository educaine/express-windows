;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Process-manager; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(in-package 'process-manager)

(defvar *Display-Lisp-Processes* NIL)

(eval-when (compile load eval)
(express-windows::define-program-framework process-manager
  :SELECT-KEY
  #\P
  :COMMAND-DEFINER
  T
  :COMMAND-TABLE
  (:INHERIT-FROM '("colon full command" "standard arguments" "standard scrolling")
   :KBD-ACCELERATOR-P NIL)
  :STATE-VARIABLES NIL
  :PANES
  ((TITLE :TITLE
          :REDISPLAY-STRING "Process Manager"
          :HEIGHT-IN-LINES 1
          :REDISPLAY-AFTER-COMMANDS NIL)
   (MENU :COMMAND-MENU :MENU-LEVEL :TOP-LEVEL)
   (DISPLAY :DISPLAY :REDISPLAY-FUNCTION 'REDISPLAY-PROCESS-MANAGER
	    :INCREMENTAL-REDISPLAY T
	    :MARGIN-COMPONENTS
	    '((express-windows::margin-scroll-bar :margin :LEFT :visibility :if-needed)))
   (INTERACTOR :INTERACTOR :MARGIN-COMPONENTS
	       '((express-windows::margin-scroll-bar :margin :LEFT :visibility :if-needed))))
  :CONFIGURATIONS
  '((main
      (:LAYOUT (main :COLUMN TITLE MENU DISPLAY INTERACTOR))
      (:SIZES
	(MAIN (TITLE 1 :LINES) (MENU :ASK-WINDOW) (INTERACTOR 5 :LINES)
	      :THEN (display :even)))))))

(define-type lisp-process (())
  :PRINTER ((process stream)
	    (princ (process-name process) stream)))

(define-type unix-process (())
  :PRINTER ((process stream)
	    (princ process stream)))


(define-process-manager-command (com-display-lisp-processes :MENU-ACCELERATOR T) ()
  (setq *Display-Lisp-Processes* T))

(define-process-manager-command (com-display-unix-processes :MENU-ACCELERATOR T) ()
  (setq *Display-Lisp-Processes* NIL))

(defmethod redisplay-process-manager ((self process-manager) *Standard-Output*)
  (if *Display-Lisp-Processes*
      (memoize (:id :lisp)
	(display-lisp-processes))
      (memoize (:id :unix)
	(display-unix-processes))))



(defun display-lisp-processes (&AUX (processes #+lucid lcl:*All-Processes*
					       #+excl mp:*All-Processes*))
  (ew:with-character-style ('(:fix :roman :huge))
    (memoize (:once-only T)
	     (ew::simple-princ #+lucid "Lucid LISP PROCESSES"
			       #+excl "Allegro LISP PROCESSES"))
    (terpri)(terpri)
    #+lucid
    (memoize (:value lcl:*Scheduling-Quantum*)
	     (format T "Scheduling Quantum ~D" lcl:*Scheduling-Quantum*))
    (terpri)
    (terpri)
    (make-table ()
      (with-character-face (:italic)
	(table-column-headings (t :underline-p t :once-only T)
	  "Process Name" "WhoState"
	  #+lucid "State" #+lucid "Debugger-P"
	  #+excl "Runnable"
	  ))
      (dolist (process processes)
	(let ((name (process-name process))
	      (whostate (process-whostate process))
	      #+excl (runnable (mp:process-runnable-p process))
	      #+lucid (state (lcl:process-state process))
	      #+lucid (dbg-p (lcl:process-in-the-debugger-p process)))
	  (table-row (() :ID process)
	     (entry (() :value name) 		(display process 'LISP-PROCESS))
	     (entry (() :value whostate) 	whostate)
	     #+excl
	     (entry (() :value runnable) runnable)
	     #+lucid
	     (entry (() :value state) 	(ew::simple-princ (case state
							    (:active "Active")
							    (:inactive "Inactive")
							    (:killed "Killed"))))
	     #+lucid
	     (entry (() :value dbg-p) 	(display dbg-p 'express-windows:boolean))
	     ))))))






(define-process-manager-command (exit :menu-accelerator T) ()
  (express-windows:exit-program))

(define-mouse-command kill-lisp-process
		      (lisp-process
			:gesture NIL
			:documentation "Kill Process")
		      (process)
  `(com-kill-lisp-process ,process))

(define-process-manager-command (com-kill-lisp-process)
				((process 'lisp-process))
  #+lucid (lcl:kill-process process)
  #+excl (mp:process-kill process))


#+lucid
(define-mouse-command restart-lisp-process
		      (lisp-process
			:gesture NIL
			:documentation "Restart Process")
		      (process)
  `(com-restart-lisp-process ,process))

#+lucid
(define-process-manager-command (com-restart-lisp-process)
				((process 'lisp-process))
  (lcl:restart-process process))

(define-process-manager-command (com-refresh :menu-accelerator T) ()
  NIL)


(define-mouse-command describe-lisp-process
		      (lisp-process
			:gesture :LEFT
			:documentation "Describe Process")
		      (process)
  `(com-describe-lisp-process ,process))

(define-process-manager-command (com-describe-lisp-process)
				((process 'lisp-process))
  (format T "~&Describing Process ~A." process))





(defstruct unix-process
  (name "" :TYPE string)
  (id 0 :TYPE fixnum)
  ;;(owner "" :TYPE string)
  (state :runnable :TYPE (member :runnable ;R
				 :stopped  ;T
				 :page-wait ; P
				 :disk-wait ; D
				 :sleep ; S
				 :idle ; I
				 :terminated ; Z
				 ))
  (swapped-out :not-swapped :type (member :swapped :not-swapped :exceeded-limit))
  (altered-scheduling-priority :not-special :TYPE (member :reduced :raised :not-special))
  (time "" :TYPE string)
  )

(defvar *PS-Arguments* NIL)

(defvar *Display-Processes-Owned-By-Others-P* T)
(defvar *Display-Processes-No-Terminal-P* T)

(defvar *Unix-Processes* NIL)

(defun get-unix-process-info ()
  (if *Display-Processes-No-Terminal-P*
      (if *Display-Processes-Owned-By-Others-P*
	  (setq *PS-Arguments*"axww")
	  (setq *PS-Arguments*"xww"))
      (if *Display-Processes-Owned-By-Others-P*
	  (setq *PS-Arguments*"aww")
	  (setq *PS-Arguments*"ww")))
  (let ((*Processes-Previous-Cons* *Unix-Processes*)
	(*Processes* *Unix-Processes*))
    (declare (special *Processes-Previous-Cons* *Processes*))
    (with-open-stream (input #+lucid (user::run-program "ps" :ARGUMENTS *PS-Arguments* :OUTPUT :STREAM :WAIT NIL)
			     #+excl (user::run-shell-command (concatenate 'string "ps -" *PS-Arguments*)
							     :OUTPUT :STREAM :WAIT NIL))
      (express-windows::lisp-read-line input NIL 'eof)
      (do ()(())
	(multiple-value-bind (line eof)
	    (express-windows::lisp-read-line input NIL 'eof)
	  (if (or (null line) (eq line 'eof) eof) (return NIL))
	  (parse-ps-listing-line line))))
    (when (and *Processes* *Processes-Previous-Cons*)
      (if (eq *Processes* *Processes-Previous-Cons*)
	  (setq *Unix-Processes* NIL)
	  (setf (cdr *Processes-Previous-Cons*) ()))))
  *Unix-Processes*)

;; the general state is that the first element of *Processes* is the process you are hoping
;; to match against the current line.  If you fail to match you look to see if it is later
;; in the list.  If so you eliminate the inbetween processes.  If you don't find it anywhere
;; put it on the list in front of the first one on the list *Processes*.

(defun parse-ps-listing-line (line)
  (declare (optimize (speed 3) (safety 0) (compilation-speed 0) (space 0)))
  #+lucid (declare (simple-string line))
  #-lucid (declare (string line))
  (declare (special *Processes-Previous-Cons* *Processes*))
  (multiple-value-bind (pid index)
      (read-from-string line)
    ;; look for a process already around with this ID.
    (let (current-process (new-p NIL))
      (if (and *Processes* (eq pid (unix-process-id (first *Processes*))))
	  ;; easy just use the first one.
	  (progn (setq current-process (first *Processes*))
		 (unless (eq *Processes-Previous-Cons* *Processes*)
		   (setq *Processes-Previous-Cons* (cdr *Processes-Previous-Cons*)))
		 (setq *Processes* (cdr *Processes*)))
	  ;; otherwise look farther down the list until you find it.
	  (let ((process-list
		  (do ((processes *Processes* (cdr processes)))
		      ((null processes) NIL)
		    (when (eq pid (unix-process-id (first processes)))
		      (return processes)))))
	    (if process-list
		;; there is a current process, eliminate all those between the beginning
		;; and this process.
		(progn
		  (setq current-process (first process-list))
		  (if (eq *Processes-Previous-Cons* *Processes*)
		      ;; still at beginning of list. eliminate from beginning up to here.
		      (setq *Unix-Processes* (cdr process-list)
			    *Processes* (cdr process-list)
			    *Processes-Previous-Cons* (cdr process-list))
		      ;; not at beginning anymore.
		      (progn
			;; eliminate interveening
			(setf (cdr *Processes-Previous-Cons*)
			      process-list)
			;; now reset the process-list
			(setf *Processes* (cdr process-list))
			(setf *Processes-Previous-Cons*
			      (cdr *Processes-Previous-Cons*)))))
		;; no current process
		;; create a new one.
		(progn
		  (setq new-p T
			current-process (make-unix-process :ID pid))
		  ;; don't forget to save it onto the list of processes.
		  (cond ((not *Processes*)
			 ;; must be the very first one of the list so far.
			 (let ((entry (list current-process)))
			   (setq *Unix-Processes* (nconc *Unix-Processes* entry))
			   (setq ;; *Processes* NIL it is already NIL.
			     *Processes-previous-cons* entry)))
			 ;; we must make it appear to be the first one though all by itself.
			;; add onto list.
			((eq *Processes-previous-cons* *Processes*)
			 ;; still at beginning of list.  Add onto these
			 (setq *Unix-Processes* (cons current-process *Unix-Processes*))
			 (setq *Processes* (cdr *Unix-Processes*)
			       *Processes-previous-cons* *Unix-Processes*))
			(T ;; splice in the list.
			 (setf (cdr *Processes-previous-cons*)
			       (cons current-process (cdr *Processes-previous-cons*)))
			 (setf *Processes-previous-cons* (cdr *Processes-previous-cons*))
			 (setf *Processes* (cdr *Processes-previous-cons*))))))))
		      
      ;; now if we already have a process we can be more efficient about consing.
      ;;(format "~%New-P ~D" new-p)(ew::sync)
      (if new-p
	  (let (;;(tt (subseq line index (express-windows::%+ 2 index)))
		(stat-1 (aref line (express-windows::%+ index 3)))
		(stat-2 (aref line (express-windows::%+ index 4)))
		(stat-3 (aref line (express-windows::%+ index 5)))
		(stat-4 (aref line (express-windows::%+ index 6)))
		(process-time (subseq line (express-windows::%+ index 7)
				      (express-windows::%+ index 13)))
		(process (subseq line (express-windows::%+ index 14))))
	    (declare (ignore stat-4))		;5/1/90 (sln)
	    (setf (unix-process-name current-process) process
		  (unix-process-state current-process)
		  (ecase stat-1
		    (#\I :Idle) (#\S :Sleep) (#\D :Disk-Wait)
		    (#\R :Runnable)
		    (#\T :Stopped) (#\P :Page-Wait)
		    (#\Z :Terminated))
		  (unix-process-swapped-out current-process)
		  (case stat-2
		    (#\W :Swapped) (#\> :Exceeded-Limit)
		    (T :Not-Swapped))
		  (unix-process-altered-scheduling-priority current-process)
		  (case stat-3
		    (#\N :Reduced)
		    (#\< :Raised)
		    (T "Not-Special"))
		  (unix-process-time current-process) process-time))
	  ;; old process
	  (let (;;(tt (subseq line index (express-windows::%+ 2 index)))
		(stat-1 (aref line (express-windows::%+ index 3)))
		(stat-2 (aref line (express-windows::%+ index 4)))
		(stat-3 (aref line (express-windows::%+ index 5)))
		(stat-4 (aref line (express-windows::%+ index 6))))
	    (declare (ignore stat-4))		;5/1/90 (sln)
	    (setf (unix-process-state current-process)
		  (ecase stat-1
		    (#\I :Idle) (#\S :Sleep) (#\D :Disk-Wait)
		    (#\R :Runnable)
		    (#\T :Stopped) (#\P :Page-Wait)
		    (#\Z :Terminated))
		  (unix-process-swapped-out current-process)
		  (case stat-2
		    (#\W :Swapped) (#\> :Exceeded-Limit)
		    (T :Not-Swapped))
		  (unix-process-altered-scheduling-priority current-process)
		  (case stat-3
		    (#\N :Reduced)
		    (#\< :Raised)
		    (T :Not-Special)))
	    (unless (string-equal
		      (unix-process-time current-process)
		      line :START2 (express-windows::%+ index 7) :END2 (express-windows::%+ index 13))
	      (setf (unix-process-time current-process)
		    (subseq line (express-windows::%+ index 7) (express-windows::%+ index 13))))
	    (unless (string-equal
		      (unix-process-name current-process)
		      line :START2 (express-windows::%+ index 14))
	      (setf (unix-process-name current-process)
		    (subseq line (express-windows::%+ index 14)))))))))


(defun display-unix-processes (&AUX (processes (get-unix-process-info)))
  (memoize (:once-only T) (ew::simple-princ "UNIX PROCESSES"))
  (fresh-line)
  (terpri)
  (make-table ()
    (with-character-face (:italic)
      (table-column-headings (t :underline-p t :once-only T)
	 "PID" "State" "Swapped Out" "Altered Priority" "Time" "Command"))
    (dolist (process processes)
      (display-as (:TYPE 'UNIX-PROCESS :OBJECT process)
	(let ((id (unix-process-id process))
	      (state (unix-process-state process))
	      (swapped-out (unix-process-swapped-out process))
	      (priority (unix-process-altered-scheduling-priority process))
	      (time (unix-process-time process))
	      (name (unix-process-name process)))
	(table-row (() :id id)
	  (entry (() :value id :value-test #'eq))
	  (entry (() :value state :value-test #'eq)
	    (case state
	      (:runnable "Runnable") (:stopped "Stopped") (:Page-wait "Page")
	      (:Disk-wait "Disk") (:Sleep "Sleep") (:idle "Idle")))
	  (entry (() :value swapped-out :value-test #'eq)
	    (case swapped-out
	      (:swapped "Swapped") (:exceeded-limit "Exceeed") (t "")))
	  (entry (() :value priority :value-test #'eq)
	    (case priority
	      (:reduced "Reduced") (:raised "Raised") (T "")))
	  (entry (() :value time :value-test #'eq))
	  (entry (() :value name :value-test #'eq))))))))

(define-mouse-command kill-unix-process
		      (unix-process
			:gesture :LEFT
			:documentation "Kill Process")
		      (process)
  `(com-kill-unix-process ,process))

(define-process-manager-command (com-kill-unix-process)
				((process 'unix-process))
  (format *Query-Io* "You didn't really expect me to let you kill a process during a demo did you?")
  (ew:beep))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
