;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************

(cl:in-package 'ew)

;; (push :DEBUGGING-IE *Features*)
;; (setq *Features* (delete :DEBUGGING-IE *Features*))


(defvar *Input-Editor-Kill-Ring* NIL)

(defvar *Last-Input-Editor-Command* NIL)

(defun get-window-input-editor-state (stream)
  #.(fast)
  (or (window-input-editor stream)
      (setf (window-input-editor stream)
	    (make-input-editor-state :CURSORPOS 0))))

(defun get-editing-option (option)
  #.(fast)
  (assoc option *Input-Editing-Options*))

(defvar *Editor-Command-Table*
	'((#\Rubout editor-rubout-character)
	  (#\Meta-Rubout editor-rubout-word)
	  (#\Control-A editor-start-line)
	  (#+symbolics #\Control-Shift-a
	   #+lucid #\Control-\a
	   editor-arglist)
	  (#\Control-B Editor-backward-character)
	  (#\Meta-B Editor-backward-word)
	  ;;(#\Meta-Control-B Editor-backward-expression)
	  (#\Meta-C editor-capitalize-word)
	  (#\Control-D editor-delete-character)
	  (#\Meta-D editor-delete-word)
	  (#\Control-E editor-end-line)
	  (#\Control-F Editor-forward-character)
	  (#\Meta-F Editor-forward-word)
	  ;;(#\Meta-Control-F Editor-forward-expression)
	  (#\Control-K editor-kill-line)
	  (#\Control-L editor-refresh-window)
	  (#\Meta-L editor-lowercase-word)
	  (#\Control-N editor-next-line)
	  ;;(#\Control-O editor-make-room)
	  (#\Control-P editor-previous-line)
	  (#\Control-T editor-transpose-character)
	  ;;(#\Meta-T editor-transpose-word)
	  ;(#\Meta-Control-T editor-transpose-expression)
	  (#\Meta-U editor-uppercase-word)
	  (#\Control-V editor-scroll-down)
	  (#\Meta-V editor-scroll-up)
	  (#\Control-Meta-W editor-put-marked-text-on-kill-ring)
	  (#\Control-Y editor-yank)
	  (#\Meta-Y editor-yank-pop)
	  (#\Meta-Control-Y editor-yank-input)
	  (#\Meta-Control-Z editor-abort)
	  (#\control-? editor-help)
	  #+symbolics
	  (#\Clear-Input editor-clear-input)
	  #-symbolics
	  (#\Meta-K editor-clear-input)
	  (#\Meta-Control-> editor-scroll-end)
	  (#\Meta-Control-< editor-scroll-beginning)
	  (#\Meta-> editor-buffer-begin)
	  (#\Meta-< editor-buffer-end)
	  (#\Meta-Control-? editor-editor-help)
	  ))

(defun initialize-state-string (state string)
  #.(fast)
  (declare (string string))
  (let* ((state-string (input-editor-state-string state))
	 (filled-length (length state-string)))
    (declare (fixnum filled-length) (string state-string))
    ;; make sure state string is big enough.
    (when (%> (%+ (length string) filled-length) (array-dimension state-string 0))
      (setf (input-editor-state-string state)
	    (setq state-string (adjust-array string
					     (%+ 20 (length string) filled-length)))))
    (dotimes (x (length string))
      (setf (aref state-string (%+ filled-length x)) (aref string x)))
    (incf (fill-pointer state-string) (the fixnum (length string)))))

(defmacro with-input-editor-redisplay ((state stream &OPTIONAL (start 0)) &BODY body)
  (let ((font (gensym "FONT"))
	(baseline (gensym "BASELINE"))
	(start-variable (gensym "START")))
    `(let* ((,font (window-font stream))
	    (,baseline (font-ascent ,font))
	    (,start-variable ,start))
       (display-input-editor-internal ,state ,stream 
				      (input-editor-state-input-x ,state)
				      (input-editor-state-input-y ,state)
				      :ERASE ,baseline ,start-variable)
       ,@body
       (display-input-editor-internal ,state ,stream 
				      (input-editor-state-input-x ,state)
				      (input-editor-state-input-y ,state)
				      :DRAW ,baseline ,start-variable))))

(defun redisplay-input-editor-after-body (state stream body)
  (with-input-editor-redisplay (state stream)
    (with-presentations-enabled
      (funcall body stream)
      (fresh-line stream)
      (let ((prompt (get-editing-option :PROMPT)))
	(multiple-value-bind (prompt-x prompt-y)
	    (read-cursorpos stream)
	  (let ((input-x prompt-x) (input-y prompt-y))
	    (setf (input-editor-state-prompt-x state) prompt-x
		  (input-editor-state-prompt-y state) prompt-y)
	    (when (second prompt)
	      (set-cursorpos stream prompt-x prompt-y)
	      (simple-princ (second prompt) stream)
	      (multiple-value-setq (input-x input-y) (read-cursorpos stream)))
	    (setf (input-editor-state-input-x state) input-x
		  (input-editor-state-input-y state) input-y)))))))

#+ignore
(defun display-string (string stream begin-x begin-y alu baseline)
  #.(fast)
  (when (lisp:typep string 'PRESENTATION-RECORDING-STRING)
    (setq string (presentation-recording-string-string string)))
  (let ((string-list (compute-motion string begin-x 0 stream)))
    (multiple-value-bind (width height)
	(window-inside-size stream)
      (dolist (item string-list)
	(let ((string (first item))
	      (x-pos (second item))
	      (y-pos (third item))
	      (end-x (fourth item))
	      (font (window-font stream)))
	  (declare (fixnum x-pos y-pos end-x))
	  (incf y-pos begin-y)
	  (multiple-value-bind (screen-x-pos screen-y-pos)
	      (convert-window-coords-to-screen-coords stream x-pos y-pos)
	    (setq end-x (convert-window-x-coord-to-screen-coord stream end-x))
	    (when (and (<= 0 screen-x-pos width)
		       (<= 0 screen-y-pos (the fixnum
					       (- height (window-line-height stream)))))
	      (if *Record-Presentations-P*
		  (let ((presentation
			  (make-string-presentation
			    ;;:PRINTED-OBJECT string
			    :STRING string
			    :FONT (window-font stream)
			    :LEFT x-pos
			    :TOP y-pos
			    :RIGHT end-x
			    :BOTTOM (the fixnum
					 (%+ y-pos (font-height font))))))
		    (add-to-end-of-list presentation
					*Presentation-Records*
					*Presentation-Records-Last-Cons*)))
	      (unless (fake-window-p stream)
		#+CLX
		(progn
		  (if (eq alu :ERASE)
		      (xlib:with-gcontext ((window-gcontext stream)
					   :FUNCTION (draw-alu :ERASE))
			(xlib:draw-rectangle (window-real-window stream)
					     (window-gcontext stream)
					     screen-x-pos screen-y-pos
					     (- end-x screen-x-pos)
					     (window-line-height stream) T))
		      (xlib:draw-glyphs (window-real-window stream)
					(window-gcontext stream)
					screen-x-pos (+ screen-y-pos baseline) string))
		  (sync))))))))
    (values (fourth (first (last string-list)))
	    (+ begin-y (the fixnum (third (first (last string-list))))))))


(defun display-string (string stream begin-x begin-y alu baseline)
  #.(fast)
  (declare (string string) (ignore baseline))
  (when (presentation-recording-string-p string)
    (setq string (presentation-recording-string-string string)))
  (let ((*Erase-p* (eq alu :ERASE)))
    (multiple-value-bind (width height)
	(window-size stream)
      (presentation-print-function-internal
	string stream
	begin-x begin-y 0 (length string)
	NIL (window-font stream) width height))))

(defvar *Rescan-Redisplay-Delayed* NIL)


(defun display-input-editor (stream)
  #.(fast)
  (when (eq stream *Inside-Input-Editor-P*)
    (let ((state (window-input-editor stream)))
      (when state
	(let ((x (input-editor-state-input-x state))
	      (y (input-editor-state-input-y state)))
	  (declare (fixnum x y))
	  (convert-window-coords-to-screen-coords-macro stream x y)
	  (let ((width (window-width stream))
		(height (window-height stream)))
	    (declare (fixnum width height))
	    (when (and (%< (the fixnum (- width)) x width)
		       (%< (the fixnum (- height)) y height))
	      (let* ((font (window-font stream))
		     (baseline (font-ascent font)))
		(display-input-editor-internal state stream
					       (input-editor-state-input-x state)
					       (input-editor-state-input-y state)
					       :DRAW baseline 0)))))))))

(defun display-input-editor-internal (state stream x y alu baseline start-index)
  #.(fast)
  (declare (fixnum x y start-index))
  ;;(dformat "~&Rescn ~D Delayed ~D" *Rescanning* *Rescan-Redisplay-Delayed*)
  (when (or (not *Rescanning*)
	    (not *Rescan-Redisplay-Delayed*))
    (setq *Rescan-Redisplay-Delayed* *Rescanning*)
    (let* ((string (input-editor-state-string state))
	   (noise-strings (input-editor-state-noise-strings state))
	   (length (length string)))
      (declare (string string) (fixnum length))
      (cond ((= length start-index)
	     ;; do nothing we are at end of the string.
	     NIL)
	    ((and (= (%1- length) start-index)
		  (not (assoc length noise-strings)))
	     ;; we have just one character at the end of the string to display
	     ;; be efficient about it.
	     (multiple-value-setq (x y)
	       (compute-cursor-position-in-input-editor stream state x y
							(window-line-height stream)
							(window-font stream)
							start-index))
	     (multiple-value-setq (x y)
	       (display-string (subseq string (1- length)) stream x y alu baseline)))
	    (T
	     (when (not (zerop start-index))
	       (multiple-value-setq (x y)
		 (compute-cursor-position-in-input-editor stream state x y
							  (window-line-height stream)
							  (window-font stream)
							  start-index))
	       (setq string (subseq string start-index)
		     noise-strings
		     (mapcan #'(lambda (noise-string)
				 (when (%> (first noise-string) start-index)
				   (list (cons (%- (first noise-string) start-index)
					       (cdr noise-string)))))
			     noise-strings)))
	     (prepare-window (stream)
	       (if (not noise-strings)
		   (progn
		     (convert-window-coords-to-screen-coords stream x y)
		     ;;(dformat "~&Drawing Simple String in Input Editor.")
		     (multiple-value-setq (x y)
		       (display-string (if *Record-Presentations-P*
					   (copy-seq string)
					   string)
				       stream x y alu baseline)))
		   (let ((start 0))
		     (dolist (noise-string noise-strings)
		       (let ((end (first noise-string)))
			 (let ((string (subseq string start end)))
			   (multiple-value-setq (x y)
			     (display-string string stream x y alu baseline))
			   (multiple-value-setq (x y)
			     (display-string (second noise-string) stream x y alu baseline)))
			 (setq start end)))
		     (when (%< start (length string))
		       (multiple-value-setq (x y)
			 (display-string (subseq string start) stream x y alu baseline))))))))
      ))
  (values x y))


;;; How does cursor position of a particular cursorpos relate to what it would take
;;; to incrementally display that string starting at that cursorpos.
;;; Start out assuming the same.

(defun compute-cursor-position-in-input-editor (stream state x y line-height font
						&OPTIONAL
						(cursorpos
						  (input-editor-state-cursorpos state)))
  #.(fast)
  (declare (fixnum cursorpos x y line-height))
  (let ((string (input-editor-state-string state))
	(max-x (window-inside-width stream))
	(noise-strings (input-editor-state-noise-strings state)))
    (declare (fixnum max-x))
    (if (not noise-strings)
	(compute-cursor-position-in-string font (input-editor-state-string state)
					     cursorpos x y line-height max-x)
	(let ((start 0))
	  (declare (fixnum start))
	  (dolist (noise-string
		    noise-strings
		    (multiple-value-setq (x y)
		      (compute-cursor-position-in-string font string
							   cursorpos x y line-height max-x
							   start)))
	    (let ((end (first noise-string)))
	      (declare (fixnum end))
	      (multiple-value-setq (x y)
		(compute-cursor-position-in-string
		  font string cursorpos x y line-height max-x start end))
	      (when (%< cursorpos end)
		(return NIL))
	      (let ((str (if (presentation-recording-string-p (second noise-string))
			     (presentation-recording-string-string (second noise-string))
			     (second noise-string))))
		(declare (string str))
		(multiple-value-setq (x y)
		  (compute-cursor-position-in-string font str
						       (length str)
						       x y line-height max-x)))
	      (when (<= cursorpos end)
		(return NIL))
	      (setq start end)))
	  (values x y)))))


(defun describe-editor-state (state string)
  (dformat "~%~A Cursorpos ~D Scanp ~D String ~S. Unread Chars ~S Scannering ~D"
	   string
	   (input-editor-state-cursorpos state)
	   (input-editor-state-scan-pointer state)
	   (input-editor-state-string state)
	   (input-editor-state-unread-characters state)
	   *rescanning*))

#+debugging-ie
(defun off ()
  (setq *Untraced* (untrace))
  (setq *Debug* NIL))

#+debugging-ie
(defun on ()
  (eval (cons 'trace *Untraced*))
  (setq *Debug* T))



(defun editor-set-cursorpos (stream state input-x input-y line-height font)
  ;; position the cursor
  (when (or (not *Rescanning*)
	    (%= (input-editor-state-scan-pointer state)
		(length (input-editor-state-string state))))
    (multiple-value-bind (cursor-x cursor-y)
	(compute-cursor-position-in-input-editor
	  stream state input-x input-y line-height font)
      (declare (fixnum cursor-x cursor-y))
      (set-cursorpos-and-size-from-char
	cursor-x cursor-y
	(if (%= (length (input-editor-state-string state))
		(input-editor-state-cursorpos state)) #\Space
	    (aref (input-editor-state-string state)
		  (input-editor-state-cursorpos state)))
	stream))))


#+ignore
(dformat "~%At Start of editor Cursor ~D Scanp ~D string ~S. Unread Chars ~S Scannering ~D"
	 (input-editor-state-cursorpos state)
	 (input-editor-state-scan-pointer state)
	 (input-editor-state-string state)
	 (input-editor-state-unread-characters state)
	 *rescanning*)


(defun editor (stream
	       &AUX
	       (font (window-font stream))
	       ;;(baseline (font-ascent font))
	       (line-height (window-line-height stream))
	       (non-empty-string NIL)
	       (input-x 0) (input-y 0)
	       (state (get-window-input-editor-state stream))
	       (rescanning-just-finished-p NIL))
  #.(fast)
  (declare (fixnum input-x input-y))
  #+debugging-ie
  (describe-editor-state state "Beginning of Editor.")
  (setq input-x (input-editor-state-input-x state)
	input-y (input-editor-state-input-y state))
  (with-presentations-disabled
    ;;(display-input-editor state stream input-x input-y :DRAW baseline)
    ;; editor is assuming beginning string is already on the window.
    (do ()(())
      (if non-empty-string
	  (when (equal (input-editor-state-string state) "")
	    (let ((full-rubout (get-editing-option :FULL-RUBOUT)))
	      (when full-rubout
		(return (values NIL (second full-rubout))))))
	  (when (not (equal (input-editor-state-string state) ""))
	    (setq non-empty-string T)))
      (editor-set-cursorpos stream state input-x input-y line-height font)
      (let ((command (if *Rescanning*
			 (let ((scan-pointer (input-editor-state-scan-pointer state))
			       (string (input-editor-state-string state)))
			   (declare (string string))
			   #+debugging-ie
			   (dformat "~&Rescanning pointer ~D." scan-pointer)
			   (if (%= scan-pointer (length string))
			       (progn
				 ;;(error "I've forgotten why we get here anymore")
				 #+debugging-ie
				 (dformat "~&Rescan Pointer Reached End.")
				 (setq *Rescanning* NIL)
				 (when *Rescan-Redisplay-Delayed*
				   ;; update the display, its been awhile
				   (setq *Rescan-Redisplay-Delayed* NIL)
				   (let* ((font (window-font stream))
					  (baseline (font-ascent font)))
				     (display-input-editor-internal
				       state stream
				       (input-editor-state-input-x state)
				       (input-editor-state-input-y state)
				       :DRAW baseline 0)))
				 #+debugging-ie
				 (dformat "~&Done Rescanning.")
				 (setf (input-editor-state-modified-p state) NIL)
				 #+debugging-ie
				 (if (input-editor-state-unread-characters state)
				     (dformat "~&Reading Char ~D using unread character list."
					      (first (input-editor-state-unread-characters
						       state)))
				     (dformat "~%Reading Char from read-char-Query-Internal."))
				 (or (pop (input-editor-state-unread-characters state))
				     (read-char-for-query-internal stream)))
			       (let ((char (aref (input-editor-state-string state)
						 scan-pointer)))
				  #+debugging-ie
				 (dformat "~&Read Char ~D using scan pointer ~D."
					  char scan-pointer)
				 ;; update scan pointer.
				 (incf (input-editor-state-scan-pointer state))
				 (when (%= (input-editor-state-scan-pointer state)
					   (length string))
				   ;; if we are done reading all characters, then immediately
				   ;; tell scanning it is over so that other functions
				   ;; such as peek-char-for-query don't get confused.
				   (setq *Rescanning* NIL
					 rescanning-just-finished-p T)
				   (when *Rescan-Redisplay-Delayed*
				     ;; update the display, its been awhile
				     (setq *Rescan-Redisplay-Delayed* NIL)
				     (let* ((font (window-font stream))
					    (baseline (font-ascent font)))
				       (display-input-editor-internal
					 state stream
					 (input-editor-state-input-x state)
					 (input-editor-state-input-y state)
					 :DRAW baseline 0)))
				   #+debugging-ie
				   (dformat "~&Done Rescanning.")
				   (setf (input-editor-state-modified-p state) NIL))
				 char)))
			 ;; not rescanning, first look for unread characters.
			 (progn
			   #+debugging-ie
			   (if (input-editor-state-unread-characters state)
			       (dformat "~&Reading Char ~D using unread character list."
					(first (input-editor-state-unread-characters state)))
			       (dformat "~%Reading Char from read-char-Query-Internal."))
			   (with-presentations-enabled
			    (or (pop (input-editor-state-unread-characters state))
				;; otherwise ask for new input from user.
				(read-char-for-query-internal stream)))))))
	(when *Parse-Error-Presentation*
	  (remove-presentation *Query-Stream* *Parse-Error-Presentation* T)
	  (setq *Parse-Error-Presentation* NIL))
	(if (presentation-blip-p command)
	    (progn
	      (throw 'QUERY-BLIP (presentation-blip-object command))
	      (setq *Last-Input-Editor-Command* NIL))
	    (progn
	      ;; get rid of calls to SOME - it is slow.
	      (cond ((dolist (item *Activation-Chars* NIL)
			(when (member command item)
			  (setq command (list :ACTIVATION command))
			  (return T))))
		    (T (dolist (item *Token-Delimiter-Chars*)
			(when (member command item)
			  (setq command (list :QUERY command))
			  (return T)))))
	      #+ignore
	      (cond ((some #'(lambda (item) (member command item)) *Activation-Chars*)
		     (setq command (list :ACTIVATION command)))
		    ((some #'(lambda (item) (member command item)) *Token-Delimiter-Chars*)
		     (setq command (list :QUERY command))))
	      #+debugging-ie
	      (dformat "~&Command ~S" command)
	      (cond ((and (listp command)
			  (not (member (first command) '(:query :activation))))
		     (beep))
		    ((listp command)
		     #+debugging-ie
		     (dformat "~%List Command ~D" command)
		     (setq *Last-Input-Editor-Command* NIL)
		     (if (or *Rescanning* rescanning-just-finished-p)
			 (progn
			   #+debugging-ie (dformat "~%Reading List Char during Rescan.")
			   (ecase (first command)
			     (:QUERY 
			       (incf (input-editor-state-cursorpos state))
			       (return command))
			     (:ACTIVATION	;(error "SHould I ever get here?")
			       (incf (input-editor-state-cursorpos state))
			       (return command))))
			 (ecase (first command)
			   (:QUERY
			     #+debugging-ie
			     (dformat "~%Cursorpos ~D String-Length ~D."
				      (input-editor-state-cursorpos state)
				      (length (input-editor-state-string state)))
			     (if (%= (input-editor-state-cursorpos state)
				     (length (input-editor-state-string state)))
				 (if (input-editor-state-modified-p state)
				     (progn
				       ;;(unread-char-for-query command stream)
				       (editor-insert-character stream (second command))
				       (error "I didn't expect to get here.")
				       #+debugging-ie
				       (dformat "~%Throwing from Editor in modified state.")
				       (throw 'RESCAN 'RESCAN))
				     ;; if we are in the middle of a line and type a delimiter, treat
				     ;; as a regular character.
				     (progn
				       (editor-insert-character stream (second command))
				       (return command)))
				 ;; since we are in the middle of command we can rely
				 ;; on the query characters currently defined.
				 ;; return it like a normal character
				 (progn
				   #+debugging-ie
				   (dformat "~%Treating :Query char ~D as a normal char."
					    command)
				   (setq command (second command))
				   (setq *Last-Input-Editor-Command* NIL)
				   (editor-insert-character stream command)
				   (return command))))
			   (:ACTIVATION
			     ;; first make the activate occur at the end
			     (setf (input-editor-state-cursorpos state)
				   (length (input-editor-state-string state)))
			     (if (input-editor-state-modified-p state)
				 (progn
				   (editor-insert-character stream (second command))
				   (error "I didn't expect to get here.")
				   (unread-char-for-query command stream)
				   #+debugging-ie
				   (dformat "~%Throwing from Editor.")
				   (throw 'RESCAN 'RESCAN))
				 (progn (editor-insert-character stream (second command))
					(return command)))))))
		    ((and (characterp command)
			  (or *Rescanning* rescanning-just-finished-p)
			  (standard-char-p command))
		     (incf (input-editor-state-cursorpos state))
		     #+debugging-ie
		     (dformat "~&Incrementing Cursorpos to ~D rescanning."
			      (input-editor-state-cursorpos state))
		     (setq *Last-Input-Editor-Command* NIL)
		     (return command))
		    ((and (characterp command)
			  (not (or rescanning-just-finished-p *Rescanning*)))
		     (let ((editor-command (second (assoc command *Editor-Command-Table*))))
		       (cond (editor-command
			      (funcall editor-command stream)
			      (setq *Last-Input-Editor-Command* editor-command)
			      #+debugging-ie
			      (dformat "~%Cursorpos after command ~D."
				       (input-editor-state-cursorpos state))
			      (setf input-x (input-editor-state-input-x state)
				    input-y (input-editor-state-input-y state)
				    ))
			     ((not (standard-char-p command))
			      #+ignore
			      (warn-format "~&The Editor Command ~:C is not implemented."
					   command)
			      (beep)
			      (setq *Last-Input-Editor-Command* NIL))
			     (T
			      (setq *Last-Input-Editor-Command* NIL)
			      (editor-insert-character stream command)
			      (return command))))))))))))



(defun insert-space-internal (amount cursorpos filled-length total-length string)
  #.(fast)
  (declare (fixnum amount cursorpos filled-length total-length) (string string))
  (when (> (%+ amount filled-length) total-length)
    (setq string (adjust-array string (%+ 20 filled-length amount))))
  (cond ((= filled-length cursorpos)
	 ;; add space at end.
	 ;; do nothing, space is there.
	 )
	(T
	 ;; copy over characters to make room.
	 (unless (zerop filled-length)
	   (do ((char-index (%1- filled-length) (%1- char-index))
		(count 0 (%1+ count)))
	       ((= count (%- filled-length cursorpos)))
	     (declare (fixnum char-index count))
	     (setf (aref string (%+ char-index amount))
		   (aref string char-index))))))
  (incf (fill-pointer string) amount)
  string)

(defun insert-space-in-state (state amount &OPTIONAL starting-index)
  #.(fast)
  (declare (fixnum amount))
  (let* ((string (input-editor-state-string state))
	 (filled-length (fill-pointer string))
	 (total-length (array-dimension string 0))
	 (cursorpos (or starting-index (input-editor-state-cursorpos state))))
    (declare (string string) (fixnum filled-length total-length cursorpos))
    #+debugging-ie
    (dformat "~&Inserting Space into ~S, filled ~D total ~D cursorpos ~D amount ~D."
	     string filled-length total-length cursorpos amount)
    (setf (input-editor-state-string state)
	  (insert-space-internal amount cursorpos filled-length total-length string))
    ;; don't forget to push over any noise-strings from here
    (dolist (pair (input-editor-state-noise-strings state))
      (when (%> (first pair) cursorpos)
	(setf (first pair) (%+ (first pair) amount))))
    #+debugging-ie
    (dformat "~&Done Inserting Space into ~S, filled ~D."
	     string (fill-pointer string))))

(defun delete-space-in-state (state amount kill-ring-position)
  #.(fast)
  (declare (fixnum amount))
  (let* ((string (input-editor-state-string state))
	 (filled-length (fill-pointer string))
	 (cursorpos (input-editor-state-cursorpos state)))
    (declare (fixnum filled-length cursorpos) (string string))
    #+ignore
    (dformat "~&Deleting Space ~S, filled ~D cursorpos ~D amount ~D."
	     string filled-length cursorpos amount)
    (cond ((> (%+ amount cursorpos) filled-length)
	   (error "Shouldn't happen."))
	  (T
	   (unless (eq kill-ring-position :IGNORE)
	     ;; first copy character onto kill ring.
	     (copy-into-kill-ring string cursorpos amount kill-ring-position))
	   ;; copy over characters to make room.
	   (do ((char-index cursorpos (%1+ char-index))
		(count 0 (%1+ count)))
	       ((= count (%- filled-length (%+ cursorpos amount))))
	     (declare (fixnum char-index count))
	     (setf (aref string char-index)
		   (aref string (%+ char-index amount))))))
    ;; delete noise-strings within this interval, and push over to left and past this point.
    (dolist (pair (input-editor-state-noise-strings state))
      #+ignore
      (dformat "~&Checking noise with cursorpos ~D position ~D amount ~D."
	       cursorpos (first pair) amount)
      (when (%> (first pair) cursorpos)
	(if (%> (first pair) (%+ amount cursorpos))
	    (decf (first pair) amount)
	    (setf  (input-editor-state-noise-strings state)
		   (delete pair  (input-editor-state-noise-strings state))))))
    (decf (fill-pointer string) amount)))


(defun copy-into-kill-ring (string starting-location amount kill-ring-position
			    &AUX kill-string)
  #.(fast)
  (declare (string string) (fixnum starting-location amount))
  ;; check to make sure the string to add to has a fill pointer - error checking
  ;; for other possible lossage in system.
  (if (and kill-ring-position (array-has-fill-pointer-p (first *Input-Editor-Kill-Ring*)))
      (setq kill-string (first *Input-Editor-Kill-Ring*))
      (setq kill-ring-position :START))
  (unless (stringp kill-string)
    (push (setq kill-string (make-array 20 :element-type 'STRING-CHAR :FILL-POINTER 0
					:ADJUSTABLE T))
	  *Input-Editor-Kill-Ring*))
  (setq kill-ring-position (if (eq kill-ring-position :START) 0
			       (fill-pointer kill-string)))
  ;; make the room
  (setq kill-string (insert-space-internal amount kill-ring-position
					   (fill-pointer kill-string)
					   (array-dimension kill-string 0)
					   kill-string))
  ;; just in case we side effected kill-string.
  (setf (first *Input-Editor-Kill-Ring*) kill-string)
  ;; now copy over the characters
  (do ((start kill-ring-position (%1+ start))
       (count 0 (%1+ count))
       (from-index starting-location (%1+ from-index)))
      ((%= count amount))
    (setf (aref (the string kill-string) start) (aref string from-index))))

(defun editor-insert-character (stream character
				&AUX
				(state (window-input-editor stream))
				(cursorpos (input-editor-state-cursorpos state)))
  #.(fast)
  (with-input-editor-redisplay (state stream cursorpos)
    (insert-space-in-state state 1)
    (setf (aref (input-editor-state-string state) cursorpos)
	  character))
  (setf (input-editor-state-cursorpos state) (%1+ cursorpos))
  #+debugging-ie
  (dformat "~&Insert Incrementing Cursorpos to ~D."
	   (input-editor-state-cursorpos state))
  (if (%= (input-editor-state-cursorpos state)
	  (length (input-editor-state-string state)))
      (when (input-editor-state-modified-p state)
	#+debugging-ie
	(dformat "~%Throwing from Insert Character")
	(throw 'RESCAN 'RESCAN))
      (progn
	#+debugging-ie
	(dformat "~%Setting Modified-P to T")
	(setf (input-editor-state-modified-p state) T)))
  #+debugging-ie
  (dformat "~&String is ~S." (input-editor-state-string state)))


(defvar *Editor-Yank-Cursorpos* NIL)
(defun editor-yank (stream
		    &AUX (state (window-input-editor stream))
		    (kill-string (first *Input-Editor-Kill-Ring*))
		    (cursorpos (input-editor-state-cursorpos state)))
  (declare (fixnum cursorpos) (string kill-string))
  (if (or (not (stringp kill-string)) (%= 0 (length kill-string)))
      (beep)
      (let ((amount (length kill-string)))
	(with-input-editor-redisplay (state stream cursorpos)
	  (insert-space-in-state state amount)
	  ;; copy in new characters
	  (let ((string (input-editor-state-string state)))
	    (do ((to-index cursorpos (%1+ to-index))
		 (from-index 0 (%1+ from-index)))
		((%= from-index amount))
	      (declare (fixnum to-index from-index))
	      (setf (aref string to-index) (aref kill-string from-index)))))
	(setq *Editor-Yank-Cursorpos* cursorpos)
	(setf (input-editor-state-cursorpos state) (%+ cursorpos amount))
	(setf (input-editor-state-modified-p state) T)
	)))

(defun editor-yank-input (stream
			  &AUX (state (window-input-editor stream))
			  (item (first (input-editor-state-input-history state)))
			  (cursorpos (input-editor-state-cursorpos state)))
  (if (not item)
      (beep)
      (let* ((kill-string (input-editor-history-item-string item))
	     (noise-strings (input-editor-history-item-noise-strings item))
	     (amount (length kill-string)))
	(declare (fixnum amount) (string kill-string))
	(with-input-editor-redisplay (state stream cursorpos)
	  (insert-space-in-state state amount)
	  ;; copy in new characters
	  (let ((string (input-editor-state-string state)))
	    (do ((to-index cursorpos (%1+ to-index))
		 (from-index 0 (%1+ from-index)))
		((%= from-index amount))
	      (declare (fixnum to-index from-index))
	      (setf (aref string to-index) (aref kill-string from-index))))
	  ;; copy over noise-strings from before
	  (dolist (noise noise-strings)
	    (let ((noise-string (cons (%+ (first noise) cursorpos) (cdr noise))))
	      (setf (input-editor-state-noise-strings state)
		    (nconc (input-editor-state-noise-strings state)
			   (list noise-string)))))
	  (setf (input-editor-state-noise-strings state)
		(sort (input-editor-state-noise-strings state) #'< :KEY #'FIRST)))
	(setq *Editor-Yank-Cursorpos* cursorpos)
	(setf (input-editor-state-cursorpos state) (%+ cursorpos amount))
	(setf (input-editor-state-modified-p state) T)
	)))

(defvar *Yank-Pop-Kill-Ring* NIL)

(defun editor-yank-pop (stream &AUX (state (window-input-editor stream)))
  (case *Last-Input-Editor-Command*
    (EDITOR-YANK (setq *Yank-Pop-Kill-Ring* *Input-Editor-Kill-Ring*))
    (EDITOR-YANK-INPUT
      (setf *Yank-Pop-Kill-Ring* (input-editor-state-input-history state))))
  (if (or (not (member *Last-Input-Editor-Command*
		       '(editor-yank editor-yank-input editor-yank-pop)))
	  (not (cdr *Yank-Pop-Kill-Ring*)))
      (beep)
      (let ((previous-kill-string (pop *Yank-Pop-Kill-Ring*)))
	(setf (input-editor-state-cursorpos state)
	      *Editor-Yank-Cursorpos*)
	;; we have to get rid of the last string.
	(with-input-editor-redisplay (state stream (input-editor-state-cursorpos state))
	  (delete-space-in-state
	    state
	    (length (the string
			 (if (lisp:typep previous-kill-string 'INPUT-EDITOR-HISTORY-ITEM)
			     (input-editor-history-item-string previous-kill-string)
			     previous-kill-string)))
	    :IGNORE)
	  ;; now add in new string
	  (let ((kill-string (first *Yank-Pop-Kill-Ring*))
		(noise-strings NIL))
	    (declare (string kill-string))
	    (when (lisp:typep kill-string 'INPUT-EDITOR-HISTORY-ITEM)
	      (psetq noise-strings (input-editor-history-item-noise-strings kill-string)
		     kill-string (input-editor-history-item-string kill-string)))
	    (let ((amount (length kill-string)))
	      (declare (fixnum amount))
	      (insert-space-in-state state amount)
	      ;; copy in new characters
	      (let ((string (input-editor-state-string state)))
		(do ((to-index (input-editor-state-cursorpos state) (%1+ to-index))
		     (from-index 0 (%1+ from-index)))
		    ((= from-index amount))
		  (declare (fixnum to-index from-index))
		  (setf (aref string to-index) (aref kill-string from-index))))
	      ;; copy over noise-strings from before
	      (dolist (noise noise-strings)
		(let ((noise-string (cons (%+ (first noise) *Editor-Yank-Cursorpos*)
					  (cdr noise))))
		  (setf (input-editor-state-noise-strings state)
			(nconc (input-editor-state-noise-strings state)
			       (list noise-string)))))
	      (setf (input-editor-state-noise-strings state)
		    (sort (input-editor-state-noise-strings state) #'< :KEY #'FIRST))
	      (incf (input-editor-state-cursorpos state) amount)
	      (setf (input-editor-state-modified-p state) T)))))))



(defvar *Add-Single-Characters-To-Kill-Ring* NIL)

(defun add-to-kill-ring-position ()
  (if *Add-Single-Characters-To-Kill-Ring*
      (case *Last-Input-Editor-Command*
	((editor-rubout-character editor-rubout-word) :START)
	((editor-delete-character editor-delete-word) :END))
      (case *Last-Input-Editor-Command*
	((editor-rubout-word) :START)
	((editor-delete-word) :END))))

(defun check-full-rubout (state)
  #.(fast)
  (let ((rubout (get-editing-option :FULL-RUBOUT)))
    (when (and rubout
	       (second rubout)
	       (zerop (input-editor-state-cursorpos state))
	       (equal "" (input-editor-state-string state)))
      (throw 'INPUT-EDITOR (values NIL (second rubout))))))

(defun editor-rubout-character (stream
				&AUX (state (window-input-editor stream))
				(cursorpos (input-editor-state-cursorpos state)))
  #.(fast)
  (declare (fixnum cursorpos))
  (cond ((zerop cursorpos)
	 (check-full-rubout state)
	 (beep))
	(T (decf (input-editor-state-cursorpos state))
	   #+ignore (dformat "~&Delete Decrementing Cursorpos to ~D."
			     (input-editor-state-cursorpos state))
	   (with-input-editor-redisplay (state stream (input-editor-state-cursorpos state))
	     (delete-space-in-state state 1 (if *Add-Single-Characters-To-Kill-Ring*
						(add-to-kill-ring-position)
						:IGNORE)))
	   (setf (input-editor-state-modified-p state) T)
	   (check-full-rubout state))))

(defun editor-delete-character (stream
				&AUX
				(state (window-input-editor stream))
				(string (input-editor-state-string state))
				(cursorpos (input-editor-state-cursorpos state))
				(length (length string)))
  #.(fast)
  (declare (fixnum cursorpos length) (string string))
  (cond ((= length cursorpos)
	 (check-full-rubout state)
	 (beep))
	(T (with-input-editor-redisplay (state stream cursorpos)
	     (delete-space-in-state state 1 (if *Add-Single-Characters-To-Kill-Ring*
						(add-to-kill-ring-position)
						:IGNORE)))
	   (setf (input-editor-state-modified-p state) T)
	   (check-full-rubout state))))


(defun editor-forward-character (stream &AUX (state (window-input-editor stream)))
  #.(fast)
  (setf (input-editor-state-cursorpos state)
	(min (%1+ (input-editor-state-cursorpos state))
	     (the fixnum (length (input-editor-state-string state))))))

(defun editor-backward-character (stream
				  &AUX (state (window-input-editor stream)))
  #.(fast)
  (setf (input-editor-state-cursorpos state)
	(max (%1- (input-editor-state-cursorpos state)) 0)))

(defun editor-previous-line (stream &AUX (state (window-input-editor stream))
			     (string (input-editor-state-string state))
			     (cursorpos (input-editor-state-cursorpos state)))
  (declare (string string) (fixnum cursorpos))
  (let ((previous-cr-position (position #\CR string
					:FROM-END T :END cursorpos)))
    (if (not previous-cr-position)
	(beep)
	(let ((pp-cr-position (position #\CR string
					:FROM-END T :END previous-cr-position)))
	  (let ((chars-to-position-from-line-start
		  (%- cursorpos previous-cr-position)))
	    (setf (input-editor-state-cursorpos state)
		  (if pp-cr-position
		      (%+ (min (%- previous-cr-position pp-cr-position)
			       (the fixnum chars-to-position-from-line-start))
			 pp-cr-position)
		      (min (the fixnum previous-cr-position)
			   (the fixnum chars-to-position-from-line-start)))))))))

(defun editor-next-line (stream &AUX (state (window-input-editor stream))
			 (string (input-editor-state-string state))
			 (cursorpos (input-editor-state-cursorpos state)))
  (declare (string string) (fixnum cursorpos))
  (let ((next-cr-position (position #\CR string :START cursorpos))
	(previous-cr-position (or (position #\CR string :FROM-END T :END cursorpos) 0)))
    (if (not next-cr-position)
	(beep)
	(let ((chars-to-position-from-line-start (%- cursorpos previous-cr-position))
	      (nn-cr-position (position #\CR string :START next-cr-position)))
	  (setf (input-editor-state-cursorpos state)
		(if nn-cr-position
		    (min (the fixnum nn-cr-position)
			 (%+ 1 next-cr-position chars-to-position-from-line-start))
		    (min (the fixnum (length string))
			 (%+ 1 next-cr-position chars-to-position-from-line-start))))))))

(defun editor-start-line (stream &AUX (state (window-input-editor stream))
				(string (input-editor-state-string state)))
  #.(fast)
  (let ((cr-position (position #\CR string :TEST #'CHAR-EQUAL :FROM-END T
			       :END (input-editor-state-cursorpos state))))
    (setf (input-editor-state-cursorpos state)
	  (or (and cr-position (%1+ cr-position))
	      0))))


(defun editor-end-line (stream
			&AUX (state (window-input-editor stream))
			(string (input-editor-state-string state)))
  #.(fast)
  (declare (string string))
  (let ((cr-position (position #\CR string :TEST #'CHAR-EQUAL
			       :START (input-editor-state-cursorpos state))))
    (setf (input-editor-state-cursorpos state)
	  (or cr-position (length string)))))


(defvar *Word-Delimiters* '(#\Space #\TAB #\CR  #\/ #\\ #\. #\- #\( #\)))

(defun editor-forward-word (stream
			    &AUX (state (window-input-editor stream))
			    (string (input-editor-state-string state))
			    (cursorpos (input-editor-state-cursorpos state)))
  #.(fast)
  (declare (fixnum cursorpos) (string string))
  (let ((first-non-space
	  (position-if #'(lambda (char)
			   (not (member char *Word-Delimiters*)))
		       string :START cursorpos)))
    (if (numberp first-non-space)
	(let ((first-space-after (position-if #'(lambda (char)
						  (member char *Word-Delimiters*))
					      string :START first-non-space)))
	  (setf (input-editor-state-cursorpos state)
		(if first-space-after first-space-after (length string)))))))

(defun editor-backward-word (stream
			     &AUX (state (window-input-editor stream))
			     (string (input-editor-state-string state))
			     (cursorpos (input-editor-state-cursorpos state)))
  #.(fast)
  (declare (fixnum cursorpos) (string string))
  (let ((first-non-space
	  (position-if #'(lambda (char)
			   (not (member char *Word-Delimiters*)))
		       string :END cursorpos :FROM-END T)))
    (if (numberp first-non-space)
	(let ((first-space-after (position-if #'(lambda (char)
						  (member char *Word-Delimiters*))
					      string :END first-non-space :FROM-END T)))
	  (setf (input-editor-state-cursorpos state)
		(if first-space-after
		    (the fixnum (min (%1+ first-space-after)
				     (the fixnum (length string))))
		    0))))))

;; 5/3/90 (sln) added this read-time conditional.  symbols.lisp now
;; imports the arglist function for symbolics, lucid and allegro.
#-(or symbolics lucid excl)
(defun arglist (function-name)
  #+symbolics (scl:arglist function-name)
  #+lucid (procedure-ref (symbol-function function-name)
			 procedure-arglist)
  #-(or symbolics lucid) NIL)

#+(or symbolics lucid)
(defun editor-arglist (stream &AUX (state (window-input-editor stream))
		       (string (input-editor-state-string state))
		       (cursorpos (input-editor-state-cursorpos state)))
  #.(fast)
  (declare (fixnum cursorpos) (string string))
  ;; search backward for first unbalanced left paren
  (do ((index cursorpos (%1- index))
       (unbalanced-parens 0)
       (in-quote-p NIL))
      ((%< index 0) NIL)
    (declare (fixnum unbalanced-parens index))
    (case (aref string index)
      (#\( (when (and (not in-quote-p) (zerop unbalanced-parens))
	     ;; we have found it, do the arglist stuff.
	     (incf index)
	     (let ((end-of-word (position-if #'(lambda (char)
						 (member char '(#\Space #\Newline #\" #\()))
					     string :START index)))
	       (let ((function-name (read-from-string string NIL NIL
						      :START index :END end-of-word)))
		 (if (not (fboundp function-name))
		     (beep)
		     (let ((arglist (arglist function-name)))
		       (redisplay-input-editor-after-body
			 state stream
			 #'(lambda (stream)
			     (format stream "Arglist or ~S: ~S" function-name arglist)))))))
	     (return NIL))
       (decf unbalanced-parens))
      (\" (setq in-quote-p (not in-quote-p)))
      (#\) (when (not in-quote-p) (incf unbalanced-parens))))))

(defun editor-case-word (stream case
			 &AUX (state (window-input-editor stream))
			 (string (input-editor-state-string state))
			 (cursorpos (input-editor-state-cursorpos state)))
  (declare (fixnum cursorpos) (string string))
  (let ((first-non-space
	  (position-if #'(lambda (char)
			   (not (member char *Word-Delimiters*)))
		       string :START cursorpos)))
    (if (numberp first-non-space)
	(let ((first-space-after (position-if #'(lambda (char)
						  (member char *Word-Delimiters*))
					      string :START first-non-space)))
	  (let ((new-cursorpos (if first-space-after first-space-after (length string))))
	    (declare (fixnum new-cursorpos))
	    (setf (input-editor-state-cursorpos state) new-cursorpos)
	    (with-input-editor-redisplay (state stream cursorpos)
	      (do ((start first-non-space (%1+ start)))
		  ((= start new-cursorpos))
		(declare (fixnum start))
		(setf (aref string start)
		      (case case
			(:UPPER (char-upcase (aref string start)))
			(:LOWER (char-downcase (aref string start)))
			(:CAPITALIZE
			  (if (%= start first-non-space)
			      (char-upcase (aref string start))
			      (char-downcase (aref string start))))))))))
	(beep))))

(defun editor-uppercase-word (stream)
  #.(fast)
  (editor-case-word stream :UPPER))

(defun editor-capitalize-word (stream)
  #.(fast)
  (editor-case-word stream :CAPITALIZE))

(defun editor-lowercase-word (stream)
  #.(fast)
  (editor-case-word stream :LOWER))

(defun editor-clear-input (stream
			   &AUX (state (window-input-editor stream))
				(string (input-editor-state-string state)))
  #.(fast)
  (with-input-editor-redisplay (state stream)
    (setf (fill-pointer string) 0
	  (input-editor-state-unread-characters state) NIL
	  (input-editor-state-scan-pointer state) 0
	  (input-editor-state-cursorpos state) 0)
    (setf (input-editor-state-modified-p state) T)
	   (check-full-rubout state)))

(defun editor-rubout-word (stream
			   &AUX (state (window-input-editor stream))
				(cursorpos (input-editor-state-cursorpos state)))
  #.(fast)
  (declare (fixnum cursorpos))
  (editor-backward-word stream)
  (let ((new-cursorpos (input-editor-state-cursorpos state)))
    (declare (fixnum new-cursorpos))
    (cond ((= cursorpos new-cursorpos)
	   (check-full-rubout state)
	   (beep))
	  (T (with-input-editor-redisplay (state stream new-cursorpos)
	       (delete-space-in-state state (the fixnum (- cursorpos new-cursorpos))
				      (add-to-kill-ring-position))
	       (setf (input-editor-state-modified-p state) T))
	     (check-full-rubout state)))))



(defun editor-delete-word (stream
			   &AUX (state (window-input-editor stream))
			   (cursorpos (input-editor-state-cursorpos state)))
  #.(fast)
  (declare (fixnum cursorpos))
  (editor-forward-word stream)
  (let ((new-cursorpos (input-editor-state-cursorpos state)))
    (declare (fixnum new-cursorpos))
    (cond ((= cursorpos new-cursorpos)
	   (check-full-rubout state)
	   (beep))
	  (T
	   (with-input-editor-redisplay (state stream cursorpos)
	     (setf (input-editor-state-cursorpos state) cursorpos)
	     (delete-space-in-state state (the fixnum (- new-cursorpos cursorpos))
				    (add-to-kill-ring-position))
	     (setf (input-editor-state-modified-p state) T))
	   (check-full-rubout state)))))


(defun editor-kill-line (stream
			 &AUX (state (window-input-editor stream))
			 (string (input-editor-state-string state))
			 (cursorpos (input-editor-state-cursorpos state)))
  (declare (fixnum cursorpos) (string string))
  (let ((newline-position (position #\Newline string :START cursorpos)))
    (unless newline-position (setq newline-position (length string)))
    (if (%= cursorpos newline-position)
	(beep)
	(with-input-editor-redisplay (state stream cursorpos)
	  (delete-space-in-state state (%- newline-position cursorpos)
				 (add-to-kill-ring-position))
	  (setf (input-editor-state-modified-p state) T)))
    (check-full-rubout state)))

(defun editor-refresh-window (stream)
  #.(fast)
  (with-input-editor-redisplay ((window-input-editor stream) stream)
    (redraw-window stream)))

(defun editor-transpose-character (stream &AUX (state (window-input-editor stream))
				    (string (input-editor-state-string state))
				    (cursorpos (input-editor-state-cursorpos state))
				    (length (length string)))
  (declare (fixnum length cursorpos) (string string))
  (cond ((or (< length 2) (zerop cursorpos))
	 (setf (input-editor-state-cursorpos state) 0)
	 (beep))
	((or (= length cursorpos)
	     (char-equal #\Newline (aref string cursorpos)))
	 ;; reverse the two previous characters.
	 (with-input-editor-redisplay (state stream)
	   (psetf (aref string (%1- cursorpos)) (aref string (%- cursorpos 2))
		  (aref string (%- cursorpos 2)) (aref string (%1- cursorpos)))))
	(T (with-input-editor-redisplay (state stream (%1- cursorpos))
	     (psetf (aref string (%1- cursorpos)) (aref string cursorpos)
		    (aref string cursorpos) (aref string (%1- cursorpos)))
	     (setf (input-editor-state-cursorpos state) (%1+ cursorpos))))))



(defun editor-help (stream &AUX (state (window-input-editor stream)))
  (if (input-editor-state-modified-p state)
      (progn
	#+debugging-ie (dformat "~%Throwing")
	(if (%= (input-editor-state-cursorpos state)
		(length (input-editor-state-string state)))
	    (progn
	      (push #\Control-? (input-editor-state-unread-characters state))
	      ;; we should be able to do this you think??
	      (throw 'RESCAN 'RESCAN))
	    (beep)))
      (redisplay-input-editor-after-body
	state stream
	#'(lambda (stream)
	    (let ((help-option (second (get-editing-option :COMPLETE-HELP)))
		  (brief-help-option (second (get-editing-option :BRIEF-HELP))))
	      (declare (ignore brief-help-option))
	      (cond ((stringp help-option)
		     (format stream help-option stream))
		    ((consp help-option)
		     (if (stringp (first help-option))
			 (apply #'format stream help-option)
			 (apply (first help-option) stream (cdr help-option))))
		    (help-option
		     (funcall help-option stream))
		    (T (format stream "No Help Is Available in current context.")
		       (format stream "~%Type Meta-Control-? for help on Input Editor"))))))))


(defun editor-editor-help (stream &AUX (state (window-input-editor stream)))
  (redisplay-input-editor-after-body
    state stream
    #'(lambda (stream)
	(format stream "Available commands in Input Editor.~%")
	(make-table (stream)
	  (dolist (pair *Editor-Command-Table*)
	    (table-row (stream)
	      (entry (stream)
		(format NIL "~:C" (first pair)))
	      (entry (stream)
		(let ((name (make-pretty-name (second pair))))
		  (if (and (> (length name) #.(length "Editor "))
			   (string-equal name "Editor "
					 :END1 #.(length "Editor ")
					 :END2 #.(length "Editor ")))
		      (subseq name #.(length "Editor "))
		      name)))))))))

(defun editor-buffer-begin (stream)
  (setf (input-editor-state-cursorpos (window-input-editor stream)) 0))

(defun editor-buffer-end (stream &AUX (state (window-input-editor stream)))
  (setf (input-editor-state-cursorpos state)
	(length (input-editor-state-string state))))


(defun editor-scroll-up (stream)
  #.(fast)
  (declare (ignore stream))
  (scroll-window-up *Standard-Output* :PIXEL (%- (window-inside-height *Standard-Output*)
						 (window-line-height *Standard-Output*)
						 (window-line-height *Standard-Output*))))

(defun editor-scroll-down (stream)
  #.(fast)
  (declare (ignore stream))
  (scroll-window-down *Standard-Output* :PIXEL (%- (window-inside-height *Standard-Output*)
						   (window-line-height *Standard-Output*)
						   (window-line-height *Standard-Output*))))

(defun editor-scroll-end (stream)
  (declare (ignore stream))
  ;; scroll so end is in middle of screen.
  (multiple-value-bind (left top)
      (viewport-position *Standard-Output*)
    (declare (ignore top))			;4/30/90 (SLN)
    (set-viewport-position
      *Standard-Output*
      left
      (max 0 (- (presentation-window-max-y-position *Standard-Output*)
		(floor (window-inside-height *Standard-Output*) 2))))))

(defun editor-scroll-beginning (stream)
  (declare (ignore stream))
  (set-viewport-position *Standard-Output* 0 0))
  

(defun editor-abort (stream)
  (declare (ignore stream))
  (throw 'top-level-command-loop NIL))

;; the purpose of the input editor is to take care of querying data as well as allowing
;; editing commands on input line.  INPUT-EDITOR will call the continuation, and then
;; it will if it gets a call to rescan the input, it will recall the continuation while setting
;; up the necessary state for rescanning

(defmethod input-editor ((stream window) continuation
			 &AUX (*Rescanning* NIL) (*Inside-Input-Editor-P* stream))
  #.(fast)
  (catch 'INPUT-EDITOR
    (enable-cursor (stream)
      (let ((state (get-window-input-editor-state stream))
	    (initial-input (get-editing-option :initial-input)))
	(initialize-input-editor stream state)
	(do ()
	    (())
	  (initialize-input-editor-prompt stream state)
	  (let ((values (multiple-value-list
			  (catch 'RESCAN
			    (unwind-protect
				(initialize-input-editor-initial-input
				  initial-input state stream)
			      (setq initial-input NIL))
			    (input-editor-internal stream state continuation)
			    ))))
	    (if (equal (first values) 'RESCAN)
		(let ((state (window-input-editor stream)))
		  #+ignore
		  (dformat "~&Rescanning resetting scan pointers. String Length ~D."
			   (length (input-editor-state-string state)))
		  (setf (input-editor-state-scan-pointer state) 0
			(input-editor-state-cursorpos state) 0)
		  (setq *Rescanning* T))
		(return (return-from-successful-input values stream state)))))))))

(defun input-editor-internal (stream state continuation)
  (let ((success NIL)
	(values NIL))
    (let ((parse-error-info
	    (catch 'PARSE-ERROR
	      (setq values (multiple-value-list (funcall continuation)))
	      (setq success T))))
      (if success
	  (values-list values)
	  ;; hopefully we only get here if we have just gotten a parsing error.
	  (apply #'handle-parsing-error stream state parse-error-info)))))

(defun initialize-input-editor-initial-input (initial-input state stream)
  (when initial-input
    ;; make this look like it had been typed in then a rescan
    ;; had happened.
    (let ((string (second initial-input))
	  (state-string (input-editor-state-string state)))
      (declare (string string state-string))
      (with-input-editor-redisplay (state stream)
	(insert-space-in-state state (length string))
	(dotimes (i (length string))
	  (setf (aref state-string i) (aref string i)))
	(setf (input-editor-state-cursorpos state) (length string)
	      (input-editor-state-modified-p state) NIL)))
    (throw 'rescan 'rescan)))


(defun initialize-input-editor (stream state)
  (multiple-value-bind (x y)
      (read-cursorpos stream)
    (setf (input-editor-state-cursorpos state) 0
	  (input-editor-state-modified-p state) NIL
	  (input-editor-state-noise-strings state) NIL
	  (input-editor-state-unread-characters state) NIL
	  (input-editor-state-prompt-x state) x
	  (input-editor-state-prompt-y state) y
	  ;; by default the input position is the same as the prompt
	  ;; in case there will be no prompt
	  (input-editor-state-input-x state) x
	  (input-editor-state-input-y state) y
	  (fill-pointer (input-editor-state-string state)) 0)))


(defun initialize-input-editor-prompt (stream state)
  (let ((prompt (get-editing-option :prompt))
	;;(reprompt (get-editing-option :reprompt))
	prompt-x prompt-y)
    (setq prompt-x (input-editor-state-prompt-x state)
	  prompt-y (input-editor-state-prompt-y state))
    (when (second prompt)
      #+ignore
      (dformat "~&Editor Prompt ~A" prompt)
      (set-cursorpos stream prompt-x prompt-y)
      (simple-princ (second prompt) stream)
      (multiple-value-bind (input-x input-y)
	  (read-cursorpos stream)
	(setf (input-editor-state-input-x state) input-x
	      (input-editor-state-input-y state) input-y)))))


(defun return-from-successful-input (values stream state)
  ;; throw away last character.
  #+ignore
  (unless (get-editing-option :PRESERVE-WHITESPACE)
    (and (peek-char-for-query stream)
	 (delete-char-for-query (read-char-for-query stream) stream)))
  
  (when (peek-char-for-query stream)
    (let ((char (read-char-for-query stream)))
      (delete-char-for-query char stream)
      (when (get-editing-option :PRESERVE-WHITESPACE)
	(unread-any stream (if (consp char) (second char) char)	))))
  ;; its possible that on a rescan we succeeded in getting all the necessary
  ;; input so that we arrive here with *Rescanning* still T.
  (setq *Rescanning* NIL)
  ;; wipe out old display and setup for proper display
						;(dbg:dbg)
  (with-presentations-disabled
    (let* ((font (window-font stream))
	   (baseline (font-ascent font)))
      (display-input-editor-internal state stream
				     (input-editor-state-input-x state)
				     (input-editor-state-input-y state)
				     :ERASE baseline
				     0)))
  (unless *Inside-Querying-Window-Edit*
    (with-presentations-enabled
      (let ((records NIL))
	(let (					;(*Make-Presentation-Records-Only-P* T)
	      (*Presentation-Records* NIL)
	      (*Presentation-Records-Last-Cons* NIL)
	      (*Inside-Record-Presentations-P* T))
	  (let* ((font (window-font stream))
		 (baseline (font-ascent font)))
	    ;;(dbg:dbg)
	    (multiple-value-bind (last-x last-y)
		(display-input-editor-internal
		  state stream
		  (input-editor-state-input-x state)
		  (input-editor-state-input-y state)
		  :DRAW baseline 0)
	      (set-cursorpos stream last-x last-y)))
	  (setq records *Presentation-Records*))
	(make-top-level-presentation stream records (first values)
				     ;; use type returned from parser
				     ;; if there is one.
				     (or (second values)
					 (first *Input-Context*))))))
  (sync) ;; so that user gets immediate feedback that the command is done.
  ;; remember data on history
  (push (make-input-editor-history-item
	  :STRING (let* ((string (input-editor-state-string state))
			 (length (length string)))
		    (declare (string string) (fixnum length))
		    (if (and (> length 0)
			     (member (aref string (1- length))
				     '(#\Newline #\Space) :TEST #'CHAR-EQUAL))
			;; might as well remove the last character.
			(subseq string 0 (1- length))
			(copy-seq string)))
	  :NOISE-STRINGS (input-editor-state-noise-strings state)
	  :PRESENTATION-BLIPS (input-editor-state-presentation-blips state))
	(input-editor-state-input-history state))
  ;; remember query data if we are in a top level call to query
  ;; not implemented yet.
  ;; finally return values.
  (values-list values))


(defun with-input-editing-internal (stream continuation)
  #.(fast)
  (if *Inside-Input-Editor-P*
      (funcall continuation)
      (input-editor stream continuation)))

(defun final-display-of-input-editor (stream value &OPTIONAL (type (first *Input-Context*)))
  (let ((state (get-window-input-editor-state stream)))
    (unless *Inside-Querying-Window-Edit*
      (with-presentations-enabled
	(let ((records NIL))
	  (let (				;(*Make-Presentation-Records-Only-P* T)
		(*Presentation-Records* NIL)
		(*Presentation-Records-Last-Cons* NIL)
		(*Inside-Record-Presentations-P* T))
	    (let* ((font (window-font stream))
		   (baseline (font-ascent font)))
	      ;;(dbg:dbg)
	      (multiple-value-bind (last-x last-y)
		  (display-input-editor-internal
		    state stream
		    (input-editor-state-input-x state)
		    (input-editor-state-input-y state)
		    :DRAW baseline 0)
		(set-cursorpos stream last-x last-y)))
	    (setq records *Presentation-Records*))
	  (make-top-level-presentation stream records value type))))))

(defun read-location (stream)
  #.(fast)
  (when (window-p stream)
    (input-editor-state-cursorpos (get-window-input-editor-state stream))))


(defun replace-input-editor-string (stream location new &KEY dont-quote)
  (declare (ignore dont-quote) (fixnum location) (string new))
  #+symbolics (setq new (scl:string-thin new))
  (with-presentations-disabled
    (let* ((state (get-window-input-editor-state stream))
	   (string (input-editor-state-string state))
	   (length (length new)))
      (declare (fixnum length) (string string))
      (with-input-editor-redisplay (state stream location)
	(if *Rescanning*
	    ;; if we are rescanning, then we replace the input up to the scan pointer
	    ;; from location.  Note; this means we probably need to push characters
	    ;; that are beyond the scan-pointer but before the fill pointer.
	    (let ((scan-pointer (input-editor-state-scan-pointer state)))
	      (declare (fixnum scan-pointer))
	      #+ignore
	      (dformat "~&Scan Pointer ~D, Location ~D, Length ~D."
		       scan-pointer location length)
	      (cond ((= (%- scan-pointer location) length)
		     ;; we already have enough space here.
		     ;; leave alone.
		     )
		    ((> (%- scan-pointer location) length)
		     ;; more room than we need.  Does this really happen?
		     #+ignore
		     (dformat "Does this really happen, extra room during replace input."))
		    (T (let ((needed-space (%- length (%- scan-pointer location))))
			 (declare (fixnum needed-space))
			 #+ignore
			 (dformat "~&Need Space ~D." needed-space)
			 (insert-space-in-state state needed-space)
			 ;; reset string since insert could have changed its value.
			 (setq string (input-editor-state-string state))
			 ;; set the scan pointer.
			 (incf (input-editor-state-scan-pointer state) needed-space)
			 (incf (input-editor-state-cursorpos state) needed-space))))
	      ;; now copy characters into input line.
	      (do ((index 0 (%1+ index))
		   (state-index location (%1+ state-index)))
		  ((= index length))
		(declare (fixnum index state-index))
		(setf (aref string state-index) (aref new index)))
	      (setf (input-editor-state-cursorpos state) scan-pointer))
	    ;; if not scanning easier.
	    ;; assumption is that we are replacing to end of line.
	    ;; so just reset pointer to current location and add text into line.
	    (progn
	      #+ignore
	      (dformat "~&Replacing Input Fill-pointer ~D location ~D length ~D new ~D"
		       (fill-pointer string) location length new)
	      (setf (fill-pointer string) location
		    (input-editor-state-cursorpos state) location)
	      ;; get rid of any noise-strings within the spot
	      (setf (input-editor-state-noise-strings state)
		    (delete-if #'(lambda (noise-string)
				   (> (first noise-string) location))
			       (input-editor-state-noise-strings state)))
	      (insert-space-in-state state length)
	      ;; reset string since insert could have changed its value.
	      (setq string (input-editor-state-string state))
	      (do ((index 0 (%1+ index))
		   (state-index location (%1+ state-index)))
		  ((= index length))
		(declare (fixnum index state-index))
		(setf (aref string state-index) (aref new index)))
	      (setf (fill-pointer string) (%+ location length)
		    (input-editor-state-cursorpos state) (%+ location length)))))
      #+ignore
      (dformat "~&Cursorpos ~D" (input-editor-state-cursorpos state)))
    ))



(defun noise-string-out (stream string &OPTIONAL (rescan-mode :IGNORE))
  (declare (ignore rescan-mode))
  (when (or (stringp string) (presentation-recording-string-p string))
    (let ((state (get-window-input-editor-state stream))
	  (location (read-location stream)))
      ;; add it in at location
      (unless (assoc location (input-editor-state-noise-strings state))
	(with-input-editor-redisplay (state stream)
	  (setf (input-editor-state-noise-strings state)
		(nconc (input-editor-state-noise-strings state)
		       (list (list location string))))
	  (setf (input-editor-state-noise-strings state)
		(sort (input-editor-state-noise-strings state) #'< :KEY #'FIRST)))))))










;;;; marking text.


;; to handle yank from presentations we need to be able to figure out which presentations
;; are within two mouse coordinates, sort them in order and pull out their text.


(defvar *Marked-Presentations* NIL)


(defun find-presentations-between-coordinates (stream left top right bottom)
  (declare (fixnum left top right bottom))
  (when (%> top bottom) (psetq left right right left top bottom bottom top))
  (let ((top-quad-number (the (values fixnum number) (floor top *Quad-Height*)))
	(bottom-quad-number (floor bottom *Quad-Height*))
	(found-presentations NIL)
	(found-partial-presentations NIL)
	(table (presentation-window-quad-table stream)))
    (declare (fixnum top-quad-number bottom-quad-number)
	     (list found-presentations found-partial-presentations))
    (labels ((search-for-string-presentation (presentation)
	       (case (structure-type presentation)
		 ((STRING-PRESENTATION TEXT-PRESENTATION)
		  ;;(format a "~%Looking at String ~D" presentation)
		   (when (and (%> (presentation-bottom presentation) top)
			      (%< (presentation-top presentation) bottom))
		     ;; it is in there somewhere as far as the y coord goes.
		     ;; there are now four cases to decide
		     (cond ((and (%> (presentation-top presentation) top)
				 (%< (presentation-bottom presentation) bottom))
			    ;; case I. no overlap with either border
			    ;; now it's easy we automatically win.
			    (pushnew presentation found-presentations)
			    T)
			   ;; case II. overlap with both borders
			   ((and (<= (presentation-top presentation)
				     (the fixnum top))
				 (>= (presentation-bottom presentation)
				     (the fixnum bottom)))
			    ;; it all depends on left and right
			    (let ((left left)
				  (right right))
			      (if (%> left right) (psetq right left left right))
			      (cond ((and (%< left (presentation-left presentation))
					  (%> right (presentation-right presentation)))
				     ;; completely
				     (pushnew presentation found-presentations))
				    ((or (%< right (presentation-left presentation))
					 (%> left (presentation-right presentation)))
				     ;; not there
				     NIL)
				    (T ;; partial
				     (pushnew presentation found-partial-presentations)))))
			   ;; case III. overlap with top border
			   ((<= (presentation-top presentation) (the fixnum top))
			    ;; check that the presentation is to the right of the
			    ;; the mouse.
			    (cond ((%< left (presentation-left presentation))
				   (pushnew presentation found-presentations))
				  ((%> left (presentation-right presentation))
				   NIL)
				  (T (pushnew presentation found-partial-presentations))))
			   ;; case IV. overlap with top border
			   ((>= (presentation-bottom presentation)
				(the fixnum bottom))
			    ;; check that the presentation is to the left of the
			    ;; the mouse.
			    (cond ((%> right (presentation-right presentation))
				   (pushnew presentation found-presentations))
				  ((%< right (presentation-left presentation))
				   NIL)
				  (T (pushnew presentation found-partial-presentations))))
			   (T (error "Shouldn't happen.")))))
		 
		 (PRESENTATION
		  ;;(format a "~%Looking at Presentation II ~D" presentation)
		   (dolist (p (presentation-records presentation))
		     ;;(format a "~%Looking at Presentation III ~D" p)
		     (search-for-string-presentation p))))))
    (do ((index top-quad-number (%1+ index))
	 (top (%* top-quad-number *Quad-Height*) (%+ top *Quad-Height*)))
	((%> index bottom-quad-number))
      (let ((quad (get-quad-node table index)))
	(when quad
	  ;;(format a "~%Looking at Quad ~D" quad)
	  (let ((presentations (quad-node-objects quad)))
	    (dolist (p presentations)
	      ;;(format a "~%Looking at Presentation ~D" p)
	      (search-for-string-presentation p)))))))
    (values found-presentations found-partial-presentations)))


#+ignore
(defun test ()
  (print "First")
  (princ " Second")
  (print "Third")
  (princ " Fourth")
  (print "Fifth")
  (princ " Sixth")
  (print "Seventh")
  (princ " Eighth"))

(defun track-string-presentations-under-mouse (stream start-mouse-x start-mouse-y)
  (declare (optimize (speed 3)(safety 0)))
  (let ((last-presentations NIL)
	(new-presentations NIL)
	(last-partial-presentations NIL)
	(new-partial-presentations NIL)
	(line-height (window-line-height stream))
	(end-x start-mouse-x)
	(end-y start-mouse-y)
	(last-end-x start-mouse-x)
	(last-end-y start-mouse-y)
	)
;;    (format a "~%Start X ~D Start Y ~D" start-mouse-x start-mouse-y)
    (flet ((highlight ()
	     ;; first get rid of partial presentations from before.
	     (dformat "~&Erasing partial presentations")
	     (let ((start-x start-mouse-x)
		   (start-y start-mouse-y)
		   (end-x last-end-x)
		   (end-y last-end-y))
	       (when (%> start-y end-y)
		 (psetq start-x end-x end-x start-x
			start-y end-y end-y start-y))
	       ;; erase all old partial-presentations because you don't know how they may
	       ;; have changed.
	       (dolist (p last-partial-presentations)
		 (underline-presentation stream line-height p
					 start-x start-y end-x end-y))
	       (dformat "~&Erasing presentations")
	       ;; erase old presentations that are not going to be around this time
	       (dolist (p last-presentations)
		 (unless (member p new-presentations)
		   (underline-presentation stream line-height p
					   start-x start-y end-x end-y))))
	     ;; not get on with business of new ones.
	     (let ((start-x start-mouse-x)
		   (start-y start-mouse-y)
		   (end-x end-x)
		   (end-y end-y))
	       (when (%> start-y end-y)
		 (psetq start-x end-x end-x start-x
			start-y end-y end-y start-y))
	       (dformat "~&Drawing Presentations")
	       ;; draw new presentation underlines.
	       ;; draw it if it is not a member of the previous presentations
	       (dolist (p new-presentations)
		 (when (or (not (member p last-presentations)))
		   (underline-presentation stream line-height p
					   start-x start-y end-x end-y)))
	       (dolist (p new-partial-presentations)
		 (underline-presentation stream line-height p
					 start-x start-y end-x end-y))
	       (setq last-end-x end-x last-end-y end-y)
	       (setq last-presentations new-presentations
		     last-partial-presentations new-partial-presentations
		     new-presentations NIL
		     new-partial-presentations NIL))))
      (tracking-mouse (stream :CONSUME-EXTRA-MOTION-EVENTS-P T)
	(:WHO-LINE-DOCUMENTATION-STRING (&rest ignore)
	 "Left: Finish Marking Text,  Right: Abort")
	(:MOUSE-MOTION (x y)
;;		       (format a "~% X ~D Y ~D" x y)
	 (multiple-value-setq (new-presentations new-partial-presentations)
	   (find-presentations-between-coordinates stream start-mouse-x start-mouse-y
						   x y))
	 (setq end-x x end-y y)
	 (highlight))
	(:MOUSE-CLICK (button x y)
	 (case button
	   (#.*Mouse-Left*
	    (return NIL))
	   (#.*Mouse-Right*
	    (highlight)
	    (setq last-presentations NIL last-partial-presentations NIL)
	    (return NIL))))))
    (list (append last-presentations last-partial-presentations)
	  start-mouse-x start-mouse-y end-x end-y stream)))

(defun mark-text-with-mouse (window mouse-x mouse-y)
  (when *Marked-Presentations*
    (let ((line-height (window-line-height window))
	  (start-x (second *Marked-Presentations*))
	  (start-y (third *Marked-Presentations*))
	  (end-x (fourth *Marked-Presentations*))
	  (end-y (fifth *Marked-Presentations*))
	  (stream (sixth *Marked-Presentations*)))
      (dolist (p (first *Marked-Presentations*))
	(underline-presentation stream line-height p start-x start-y end-x end-y)))
    (setq *Marked-Presentations* NIL))
  (setq *Marked-Presentations*
	(track-string-presentations-under-mouse window mouse-x mouse-y)))


(defun sort-string-presentations-into-real-stuff (presentations)
  #.(fast)
  (setq presentations
	(sort presentations
	      #'(lambda (p1 p2)
		  ;; first sort by y coordinate
		  (cond ((< (boxed-presentation-top p1) (boxed-presentation-top p2))
			 T)
			((> (boxed-presentation-top p1) (boxed-presentation-top p2))
			 NIL)
			(T (if (<= (boxed-presentation-left p1) (boxed-presentation-left p2))
			       T NIL)))))))


(defvar *Temporary-Marked-Text*
	(make-array 500 :ELEMENT-TYPE 'STRING-CHAR :ADJUSTABLE T :FILL-POINTER 0))

(defun turn-sorted-presentations-into-text (presentations stream
					    line-height
					    start-x start-y end-x end-y)
  (declare (fixnum start-x start-y end-x end-y line-height))
  (setf (fill-pointer *Temporary-Marked-Text*) 0)
  (let ((last-presentation NIL)
	(char-width (default-char-width stream)))
    (declare (fixnum char-width))
    (when presentations
      (flet ((copy-presentation-string (p)
	       (let ((string (string-presentation-string p)))
		 (declare (string string))
;;		 (format A "~%Copying portion of string ~S" string)
		 (cond ((and (<= (presentation-left p) start-x (presentation-right p))
			     (<= (presentation-top p) start-y (presentation-bottom p))
			     (<= (presentation-left p) end-x (presentation-right p))
			     (<= (presentation-top p) end-y (presentation-bottom p)))
			(let ((index1 (find-text-index-for-position
					(string-presentation-font p) string
					(presentation-left p)
					(presentation-top p)
					start-x start-y line-height))
			      (index2 (find-text-index-for-position
					(string-presentation-font p) string
					(presentation-left p)
					(presentation-top p)
					end-x end-y line-height)))
			  (declare (fixnum index1 index2))
			  ;;(format A "~%Case I.")
			  (do ((x index1 (%1+ x)))
			      ((= x index2))
			    (declare (fixnum x))
			    (vector-push-extend (aref string x)
						*Temporary-Marked-Text* 100))))
		       ((and (<= (presentation-left p) start-x (presentation-right p))
			     (<= (presentation-top p) start-y (presentation-bottom p)))
			;;(dformat "~%Case I start")
			;;(format A "~%Case II.")
			(let ((index (find-text-index-for-position
				       (string-presentation-font p) string
				       (presentation-left p)
				       (presentation-top p)
				       start-x start-y line-height)))
			  (do ((x index (%1+ x)))
			      ((= x (length string)))
			    (vector-push-extend (aref string x)
						*Temporary-Marked-Text* 100))))
		       ((and (<= (presentation-left p) end-x (presentation-right p))
			     (<= (presentation-top p) end-y (presentation-bottom p)))
			;;(dformat "~%Case II End")
			(let ((index (find-text-index-for-position
				       (string-presentation-font p) string
				       (presentation-left p)
				       (presentation-top p)
				       end-x end-y line-height)))
			  (declare (fixnum index))
			  (do ((x 0 (%1+ x)))
			      ((= x index))
			    (declare (fixnum x))
			    (vector-push-extend (aref string x)
						*Temporary-Marked-Text* 100))))
		       (T ;;(format A "~%Case III.")
			  (dotimes (x (length string))
			    (vector-push-extend (aref string x)
						*Temporary-Marked-Text* 100)))))
	       (setq last-presentation p)))
	(setq last-presentation (first presentations))
	(copy-presentation-string last-presentation)
	(dolist (p (cdr presentations))
	  ;; now we want to be clever and stick in CR's when the y coordinate changes.
	  (if (= (presentation-top p) (presentation-top last-presentation))
	      ;; now look for spacing
	      (let ((distance (max 0 (%- (presentation-left p)
					 (presentation-right last-presentation)))))
		(declare (fixnum distance))
		(when (and (> distance 0)
			   (> (setq distance (the (values fixnum number)
						  (floor distance char-width))) 0))
		  (dotimes (x distance)
		    #+symbolics (declare (ignore x))	; 6/27/90 (sln)
		    (vector-push-extend #\Space *Temporary-Marked-Text* 100)))
		(copy-presentation-string p))
	      (let ((distance (max 0 (%- (presentation-top p)
					 (presentation-top last-presentation)))))
		(declare (fixnum distance))
		(let ((number-crs (max 1 (the (values fixnum number)
					      (floor distance line-height)))))
		  (declare (fixnum number-crs))
		  (dotimes (x number-crs)
		    #+symbolics (declare (ignore x))	; 6/27/90 (sln)
		    (vector-push-extend #\Newline *Temporary-Marked-Text* 100)))
		;; now look for spacing
		(let ((distance (presentation-left p)))
		  (declare (fixnum distance))
		  (when (and (> distance 0)
			     (> (setq distance (floor distance char-width)) 0))
		    (dotimes (x distance)
		      #+symbolics (declare (ignore x))	; 6/27/90 (sln)
		      (vector-push-extend #\Space *Temporary-Marked-Text* 100)))
		  (copy-presentation-string p))))))))
  (copy-seq *Temporary-Marked-Text*))


(defun editor-put-marked-text-on-kill-ring (stream)
  (if (not *Marked-Presentations*)
      (beep)
      ;; dehighlight marked presentations.
      (let ((line-height (window-line-height stream))
	    (presentations (first *Marked-Presentations*))
	    (start-x (second *Marked-Presentations*))
	    (start-y (third *Marked-Presentations*))
	    (end-x (fourth *Marked-Presentations*))
	    (end-y (fifth *Marked-Presentations*))
	    (text-stream (sixth *Marked-Presentations*)))
	(dolist (p presentations)
	  (underline-presentation text-stream line-height p start-x start-y end-x end-y))
	(let ((sorted-presentations
	       (sort-string-presentations-into-real-stuff presentations)))
	  (let ((text (turn-sorted-presentations-into-text sorted-presentations text-stream
							   line-height
							   start-x start-y end-x end-y)))
	    (unless (equal text "")
	      (push text *Input-Editor-Kill-Ring*))))))
  (setq *Marked-Presentations* NIL))


;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
