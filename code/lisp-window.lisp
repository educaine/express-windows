;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


#||
Apropos
Clear Output History
Close All Files
Compile File
Edit Definition
Edit File
Load File
Rename File
Set Default Character Style
Set Package
Show Character Style
Show Compiled Code
Show Expanded Lisp Code
Show File
Show Font
Show Environment
Show Room
Start Gc
||#

#|| to be implemented soon.
Create Link
Delete Directory
Delete File
Find String
Find Symbol
Hardcopy File
Set Base
Set Command Processor
Set Input Base
Show Class Components
Show Class Dependents
Show Class Differences
Show Class Functions
Show Class Handler
Show Class Initializations
Show Class Instance Variables
Show Class Methods
Show Class Operations
Show Users
Who Am I
||#


(cl:in-package 'ew)


(eval-when (compile load eval)
(define-program-framework lisp-window
  :SELECT-KEY
  #\L
  :COMMAND-DEFINER
  T
  :COMMAND-TABLE
  (:INHERIT-FROM '("colon full command" "standard arguments" "standard scrolling")
   :KBD-ACCELERATOR-P NIL)
  :STATE-VARIABLES ()
  :PANES
  ((TITLE :TITLE
          :REDISPLAY-STRING
          "Lisp Interactor Window"
          :HEIGHT-IN-LINES
          1
          :REDISPLAY-AFTER-COMMANDS
          NIL)
   (INTERACTOR :INTERACTOR :MARGIN-COMPONENTS
	       '((margin-scroll-bar :margin :LEFT :visibility :if-needed)
		 ;(margin-scroll-bar :margin :RIGHT)
		 ;(margin-scroll-bar :margin :TOP)
		 (margin-scroll-bar :margin :BOTTOM :visibility :if-needed)
		 )))
  :CONFIGURATIONS
  '((main
      (:LAYOUT (main :COLUMN TITLE INTERACTOR))
      (:SIZES
	(MAIN (TITLE 1 :LINES) :THEN (interactor :even)))))))



(defvar *Default-Pathname* (make-pathname))

(define-mouse-command edit-file-translator
		      (pathname
			:Documentation "Edit File")
		      (pathname)
  `(com-edit-file ,pathname))

(define-lisp-window-command (com-edit-file)
			    ((pathname 'PATHNAME :PROMPT "Choose Pathname"
				       :DEFAULT *Default-Pathname*))
  (and pathname
       (ed pathname)))

(define-mouse-command edit-definition-translator
		      ((or symbol function-spec)
			:Documentation "Edit Definition"
			:TEST ((object) (and (symbolp object)
					     (fboundp object))))
		      (function-spec)
  `(com-edit-definition ,function-spec))

(define-lisp-window-command (com-edit-definition)
			    ((function 'function-spec :PROMPT "Function Name"
				       :DEFAULT *Default-Pathname*))
  (and function
       (ed function)))

(define-mouse-command compile-file-translator
					   (pathname
					     :Documentation "Compile File")
					   (pathname)
  `(com-compile-file ,pathname))

(define-lisp-window-command (com-compile-file)
			    ((pathname 'PATHNAME :PROMPT "Choose Pathname"
				       :DEFAULT *Default-Pathname*)
			     &KEY
			     (output-file '((PATHNAME) :DIRECTION :WRITE)
					  :DEFAULT
					  (make-pathname :DEFAULTS pathname
							 :TYPE
							 #+symbolics "bin"
							 #+lucid "lbin"
							 #+excl "fasl")))
  (and pathname
       (compile-file pathname :OUTPUT-FILE output-file)))


#+(or symbolics lucid)
(define-lisp-window-command (com-close-all-files) ()
  (if (yes-or-no-p "Really Close all Open File connections?")
      #+lucid (user::close-all-files)
      #+symbolics (fs::close-all-files)))


(define-mouse-command show-file-translator
					   (pathname
					     :Documentation "Show File")
					   (pathname)
  `(com-show-file ,pathname))

(define-lisp-window-command (com-show-file)
			    ((pathname 'PATHNAME :PROMPT "Choose Pathname"
				       :DEFAULT *Default-Pathname*))
  (if (probe-file pathname)
      (with-open-file (input-stream pathname :DIRECTION :INPUT)
	(setq *Default-Pathname* pathname)
	(fresh-line)
	(terpri)
	(format T "    *** ")
	(display (truename input-stream) 'PATHNAME)
	(format T " ***")
	(terpri) (terpri)
	(let (;x y
	      (left-margin (window-left-margin-size *Standard-Output*))
	      (top-margin (window-top-margin-size *Standard-Output*))
	      (font (window-font *Standard-Output*)))
	  (inhibit-scrolling (*Standard-Output*)
	    (prepare-window (*Standard-Output*)
	      (multiple-value-bind (width height)
		  (window-size *Standard-Output*)
		(loop
		  (multiple-value-bind (string eof)
		      (read-line input-stream NIL input-stream)
		    (declare (string string))
		    (when (eq string input-stream)
		      (return NIL))
		    ;; (princ string) open code version of princ for this operation.
		    ;; (multiple-value-setq (x y) (read-cursorpos *Standard-Output*))
		    ;; open coding of read-cursorpos
		    ;; (setq x (%- (window-x-pos *Standard-Output*) left-margin)
		    ;;       y (%- (window-y-pos *Standard-Output*) top-margin))
		    (presentation-print-function-internal
		      string
		      *Standard-Output*
		      (%- (window-x-pos *Standard-Output*) left-margin)
		      (%- (window-y-pos *Standard-Output*) top-margin)
		      0 (length string) T font width height)
		    (indent-terpri *Standard-Output*)
		    (when (eq eof input-stream)
		      (return NIL)))))))
	  (sync)))
      (format T "~&The File ~A was not found." pathname)))


;(define-lisp-window-command (com-copy-file)
;			    ((from-pathname 'PATHNAME :PROMPT "Choose File to Copy"
;					    :DEFAULT *Default-Pathname*)
;			     (to-pathname 'PATHNAME :PROMPT "New Filename"
;					  :DEFAULT from-pathname :CONFIRM T))
;  (if (probe-file from-pathname)
;      (with-open-file (input-stream from-pathname :DIRECTION :INPUT)
;	(with-open-file (output-stream to-pathname :DIRECTION :OUTPUT)
;	  (


(define-lisp-window-command (com-rename-file)
			    ((from-pathname 'PATHNAME :PROMPT "Choose File to Rename"
					    :DEFAULT *Default-Pathname*)
			     (to-pathname '((PATHNAME) :DIRECTION :WRITE)
					  :PROMPT "New Name"
					  :DEFAULT from-pathname :CONFIRM T))
  (multiple-value-bind (new-name old-truename new-truename)
      (rename-file from-pathname to-pathname #+symbolics NIL)
    (declare (ignore new-name))
    (when (and old-truename new-truename)
      (format T "~&Renamed File ~A to ~A." old-truename new-truename)
      (setq *Default-Pathname* from-pathname))))

(define-mouse-command load-file-translator
		      (pathname
			:Documentation "Load File")
		      (pathname)
  `(com-load-file ,pathname))

(define-lisp-window-command (com-load-file)
			    ((pathname '((PATHNAME) :DEFAULT-TYPE
					 #+symbolics "bin"
					 #-symbolics "lbin")
				       :PROMPT "File to Load"
				       :DEFAULT *Default-Pathname*))
  (setq *Default-Pathname* pathname)
  (when (probe-file pathname)
    (load pathname)))


#+ignore
(define-lisp-window-command (com-show-stream)
			    ()
  (print *Standard-Output* #+symbolics tv:selected-window
	      #-symbolics selected-window))

(define-lisp-window-command (com-clear-output-history)
			    ()
  (clear-history *Standard-Output*))


(defvar *Run-Programs* NIL)

(defun run-programs (program)
  (let ((*Run-Programs* T))
    (loop
      (setq program
	    (catch 'program-top-level (run-program program))))))

(define-lisp-window-command (com-exit-program) () (ew::exit-program))

(define-mouse-command top-system-menu
		      (no-type :GESTURE :shift-right
			       :TEST ((object)
					(declare (ignore object))
						;*Run-Programs*
					T
					)
			       :DOCUMENTATION "Select Program")
		      (command-name &KEY gesture presentation-type
				    window)
  (declare (ignore command-name gesture presentation-type window))
  `(com-system-menu))

(define-lisp-window-command (com-system-menu) ()
  (let ((program (menu-choose (mapcar #'(lambda (program)
					  (let ((program-framework-name
						  (program-framework-name program)))
					    (cons (make-pretty-name program-framework-name)
						  program-framework-name)))
				      *Program-Frameworks*)
			      :PROMPT "Choose Program to Run")))
    (when program
      (let ((*Program-Frame* NIL)
	    (*Program* NIL))
	(if *Run-Programs* (throw 'program-top-level program)
	    (catch 'PROGRAM-TOP-LEVEL
	      (run-program program)))))))

(define-mouse-command describe-object
		      (T :GESTURE :SHIFT-CONTROL-MIDDLE
			 :DOCUMENTATION "Describe")
		      (object)
  `(com-describe-object ,object))

(define-lisp-window-command (com-describe-object)
			    ((object T :PROMPT "Describe Object"))
  (describe-object-internal object))


#+ignore
(defclass person
	  ()
     ((name :initform "John Doe")
      (age :initform 32)
      (hair-color :initform 'brown)))

(defvar *Objects-Being-Described* NIL)


(defun describe-object-internal (object)
  (if (member object *Objects-Being-Described*)
      (print object)
      (let ((*Objects-Being-Described* (cons object *Objects-Being-Described*)))
	(typecase object
	  (SYMBOL (describe-symbol object))
	  (NUMBER (describe-number object))
	  (STRING (describe-string object))
	  (CONS (describe-list object))
	  #+pcl-victoria (PCL::IWMC-CLASS (describe-pcl-object object))
	  ;; 6/14/90 (sln) mod for genera 8.0
	  #+(and ew-clos (not pcl-victoria))
	  (STANDARD-CLASS (describe-object object))
	  (HASH-TABLE (describe-hash-table object))
	  (PATHNAME (describe-pathname object))
	  (ARRAY (describe-array object))
	  (T (format T "~%~D is some kind of object." object)))))
  object)


(defun describe-list (object)
  (format T "~%")
  (princ object)
  (format T "  is a list."))


(defun describe-hash-table (table)
  (format T "~%~A is a hash table with ~D elements." table (hash-table-count table))
  (when (and (plusp (hash-table-count table))
	     (let ((*Query-Io* *Standard-Output*))
	       (y-or-n-p "Do you want to see the contents? ")))
    (maphash #'(lambda (key value)
		 (terpri)
		 (princ key)
		 (simple-princ ":  ")
		 (princ value))
	     table))
  (fresh-line))

(defun describe-array (array)
  (declare (array array))
  (format T "~&~D is an array of type ~D with dimensions ~D."
	  array (type-of array) (array-dimensions array))
  (when (%= 1 (length (array-dimensions array)))
    (if (array-has-fill-pointer-p array)
	(format T "~%It has a fill-pointer of ~D." (fill-pointer array)))
    (format T "~%Elements of array are as follows:~%")
    (dotimes (x (length array))
      (terpri)
      (princ x)
      (simple-princ ":  ")
      (princ (aref array x)))))



(defun describe-pathname (pathname)
  (format T "~%~D is a Pathname with the following properties.~%" pathname)
  (increment-cursorpos *Standard-Output* 10 0)
  (make-table ()
    (table-row () "Property" "Value")
    (table-row () "Host"
		    (entry () (display (pathname-host pathname) 'host)))
    (table-row () "Device"
		    (entry () (pathname-device pathname)))
    (table-row () "Directory"
		    (entry () (princ (pathname-directory pathname))))
    (table-row () "Name" (entry () (pathname-name pathname)))
    (table-row () "Type" (entry () (pathname-type pathname)))
    (table-row () "Version" (entry () (pathname-version pathname))))
  (format T "~%It has a namestring of ~D." (namestring pathname))
  )

(defun describe-string (string)
  (terpri)
  (princ string)
  (simple-princ "is a string")
  (format T "~%It is ~D elements long." (array-dimension string 0))
  (if (array-has-fill-pointer-p string)
      (format T "~%Is has a fill-pointer of ~D." (fill-pointer string))))

(defun describe-symbol (object)
  (when (fboundp object)
    (format T "~%~D is the function ~D.~%" object (symbol-function object)))
  (format T "~%~D is in the package ." object)
  (princ (symbol-package object))
  (simple-princ ".")
  (when (boundp object)
    (format T "~2%~D has the value " object)
    (princ (symbol-value object))
    (simple-princ ".")
    (terpri)
    (describe-object-internal object))
  (when (symbol-plist object)
    (format T "~%~D has the property-list " object)
    (princ (symbol-plist object))
    (terpri)))



(defun describe-number (object)
  (typecase object
    (FIXNUM (format T "~&~D is an ~:[odd~;even~] fixnum."
		    object (evenp object)))
    (BIGNUM (format T "~&~D is an ~:[odd~;even~] bignum."
		    object (evenp object)))
    (INTEGER (format T "~&~D is an ~:[odd~;even~] integer."
		     object (evenp object)))
    (SHORT-FLOAT (format T "~&~D is a single precision floating-point number."
			 object))
    (DOUBLE-FLOAT (format T "~&~D is a double precision floating-point number."
			object))
    #-symbolics
    (LONG-FLOAT (format T "~&~D is a long precision floating-point number."
			object))
    ;;; 6/27/90 (sln) symbolics won't get to this point.
    ;;;For Function DESCRIBE-NUMBER
    ;;;  The TYPECASE clause for FLOAT can never be reached and has been deleted.
    ;;;  The form (FORMAT T "~&~D is a floating-point number." OBJECT) will never be executed.
    #-symbolics
    (FLOAT (format T "~&~D is a floating-point number."
		   object))
    (T (format T "~&~D is a number."))))



;; 6/14/90 (sln) don't need in non-pcl code 
;; 6/28/90 (sln) actually, May Day pcl has describe object, so let's
;; restrict this to Victoria Day.
#+pcl
(unless (fboundp 'describe-object)
  (defun describe-pcl-object (object &optional (stream t))
    (let* ((class (pcl::class-of object))
	   (slotds (pcl::slots-to-inspect class object))
	   (max-slot-name-length 0)
	   (instance-slotds ())
	   (class-slotds ())
	   (other-slotds ()))
      (flet ((adjust-slot-name-length (name)
	       (setq max-slot-name-length
		     (max max-slot-name-length
			  (length (the string (symbol-name name))))))
	     (describe-slot (name value &optional (allocation () alloc-p))
	       (table-row ()
		 (progn
		   (entry () name)
		   (entry () (if alloc-p (princ allocation) ""))
		   (entry () (prin1 value))))
	       #+ignore
	       (if alloc-p
		   (progn
		     (format stream
			     "~% ~A ~S ~VT  "
			     name allocation (%+ max-slot-name-length 7))
		     (princ value stream))
		   (progn
		     (format stream
			     "~% ~A~VT  "
			     name max-slot-name-length)
		     (princ value stream)))))
	;; Figure out a good width for the slot-name column.
	(dolist (slotd slotds)
	  (adjust-slot-name-length (pcl::slotd-name slotd))
	  (case (pcl::slotd-allocation slotd)
	    (:instance (push slotd instance-slotds))
	    (:class  (push slotd class-slotds))
	    (otherwise (push slotd other-slotds))))
	(setq max-slot-name-length  (min (+ max-slot-name-length 3) 30))
	(format stream "~%~S is an instance of class ~S:" object class)
	(when instance-slotds
	  (format stream "~% The following slots have :INSTANCE allocation:~%")
	  (make-table ()
	    (dolist (slotd (nreverse instance-slotds))
	      (describe-slot (pcl::slotd-name slotd)
			     (pcl::slot-value object (pcl::slotd-name slotd))))))

	(when class-slotds
	  (format stream "~% The following slots have :CLASS allocation:~%")
	  (make-table ()
	    (dolist (slotd (nreverse class-slotds))
	      (describe-slot (pcl::slotd-name slotd)
			     (pcl::slot-value object (pcl::slotd-name slotd))))))

	(when other-slotds 
	  (format stream "~% The following slots have allocation as shown:~%")
	  (make-table ()
	    (dolist (slotd (nreverse other-slotds))
	      (describe-slot (pcl::slotd-name slotd)
			     (pcl::slot-value object (pcl::slotd-name slotd))
			     (pcl::slotd-allocation slotd)))))
	(values))))
  )

(define-lisp-window-command (com-set-package)
			    ((Package 'Package :PROMPT "Package"
				      :DEFAULT (find-package "USER")))
  (when package
    (setq *Package* package)
    (format T "~&Package set to ")
    (display package 'PACKAGE)
    (simple-princ ".")))


(define-lisp-window-command (com-show-expanded-lisp-code)
			    ((expression 'FORM :PROMPT "Lisp-Code" :CONFIRM T)
			     &KEY
			     (repeat 'boolean :DEFAULT NIL)
			     (everything 'boolean :DEFAULT NIL))
  (print (macroexpand expression)))

(define-lisp-window-command (com-show-compiled-code)
			    ((function 'FUNCTION-SPEC :PROMPT "Function Name" :CONFIRM T))
  (disassemble function))

(define-mouse-action mark-text
		     (no-type NIL :GESTURE	;:meta-control-right
			      :control-right
			      :DOCUMENTATION "Mark Text")
		     (object &KEY window mouse-x mouse-y)
  (mark-text-with-mouse window mouse-x mouse-y))


#||
Interesting Commands to implement:

Append
Clean File
Close File
Compare Directories
Compile System
Copy File
Copy Output History
Create Directory
Debug Process
Delete Printer Request
Disable Network
Disable Services
Edit Directory
Edit System
Enable Network
Enable Services
Expunge Directory
Flush Process
Halt GC
Halt Machine
Halt Printer
Halt Process
Hardcopy System
Help
Initialize Mouse
Initialize Time
Inspect
Kill Process
Load System
Login
Logout
Reset Network
Reset Printer
Restart Printer Request
Restart Process
Save File Buffers
Select Activity
Send Mail
Send Message
Set Alarm
Set Calendar Clock
Set File Properties
Set GC Options
Set Lisp Context
Set Output Base
Set Printer
Set Process Priority
Set Screen Options
Set Time
Set Window Options
Show Callers
Show Differences
Show Directory
Show Expanded Mailing List
Show Function Arguments
Show GC Status
Show Generic Function
Show Handlers For Types
Show Hosts
Show Legal Notice
Show Lisp Context
Show Machine Configuration
Show Mail
Show Mouse Handlers
Show Presentation Handlers From Type
Show Presentation Type
Show Printer Defaults
Show Printer Status
Show Processes
Show Source Code
Show System Components
Show System Definition
Show System Modifications
Show System Plan
Start GC
Start Printer
Start Process
Undelete File


Commands I probably won't implement

Add HackSaw
Add Paging File
Add Service To Hosts
Clear All Breakpoints
Clear Breakpoint
Copy Flod Files
Copy Microcode
Copy World
Create FEP File
Create Namespace Object
Create Netboot Core
Create Spell Dictionary From Namespace
Define Site
Delete Namespace Object
Delete Timer Queue Entry
Disable Capabilities
Disable Condition Tracing
Distribute Systems
Edit Acl
Edit Documentation Example
Edit Namespace Object
Enable Capabilities
Enable Condition Tracing
Find HackSaw
Format File
Format Topic
Initialize Mail
Install System
Install Systems
Load HackSaw File
Load Patches
Monitor Variable
Optimize World
Read Carry Tape
Remove Service From Hosts
Report Bug
Restart Process
Restore Distribution
Run Documentation Example
Save Compiler Warnings
Save Mail Buffers
Save World
Set Boot File World
Set Breakpoint
Set Mode Lock State Controller
Set Site
Set Stack Size
Show Additional Patches
Show Breakpoints
Show Carry Tape
Show Command Processor Status
Show Compiler Warnings
Show Crash Data
Show Definition Documentation
Show Directory
Show Disabled Services
Show Distribution Directory
Show Documentation
Show ECOs
Show FEP Directory
Show HackSaw
Show Hosts
Show IDS Children
Show IDS Files
Show IDS Parents
Show Initialization List
Show Login History
Show Monitored Locations
Show Namespace Object
Show Notifications
Show Object
Show Overview
Show Standard Value Warnings
Show Timer Queue
Unmonitor Variable
Write Carry Tape

||#


(define-mouse-command show-font-translator
		      (font
			:Documentation "Show Font")
		      (font)
  `(com-show-font ,font))

;;(SCL:FUNDEFINE '|COM-SHOW-FONT-internal|)

(define-lisp-window-command (com-show-font)
			    ((font 'font :PROMPT "Choose Font" :CONFIRM T)
			     &KEY
			     (show-char-code 'boolean :PROMPT "Show Char Code"
					     :DEFAULT T))
  (when (xfonts::fontp font)
    (fresh-line)
    (terpri)
    (format T "Description of Font ")
    (display font 'FONT)
    (terpri)(terpri)
    (flet ((show-chars (start end)
	     (declare (fixnum start end))
	     (make-table ()
	       (when show-char-code
		 (table-row ()
		   (do ((x start (%1+ x)))
		       ((= x end))
		     (declare (integer x))
		     (entry () (display x 'integer)))))
	       (table-row ()
		 (do ((x start (%1+ x)))
		     ((= x end))
		   (declare (fixnum x))
		   (entry ()
		     (format T "~:C" (code-char x)))))
	       (table-row ()
		 (do ((x start (%1+ x)))
		     ((= x end))
		   (declare (fixnum x))
		   (entry ()
		     (draw-glyph x font 0 (font-ascent font))))))))
      (show-chars 0 16.)
      (terpri)
      (show-chars 16 32.)
      (terpri)
      (show-chars 32. 64.)
      (terpri)
      (show-chars 64. 96.)
      (terpri)
      (show-chars 96. 128.))
    (fresh-line)))
#+ignore
(defun |COM-SHOW-FONT-parser| (STREAM)
  (LET ((*PARSE-TYPE* :NORMAL))
    (LET (FONT)
      (LET (KEYWORDS-ENTERED)
	(SETQ FONT (READ-COMMAND-ARGUMENT 'FONT STREAM :PROMPT "Choose Font"))
	(LET ((KEYWORD-ALIST '((":Show-Char-Code" :SHOW-CHAR-CODE)))
	      (SHOW-CHAR-CODE NIL))
	  (PROG ((KEYWORD-ALIST KEYWORD-ALIST)
		 (FIRST-KEYWORD T))
		(WHEN (= (LENGTH KEYWORDS-ENTERED) (LENGTH KEYWORD-ALIST))
		  (GO #:G8863))
		#:G8861
		(READ-DELIMITER-CHAR STREAM)
		(LET ((KEYWORD-INPUT
			(QUERY (LIST (LIST 'KEYWORD-ARGUMENT
					   (LET ((RESULT NIL))
					     (LET ((#:VALUE24150 KEYWORD-ALIST)
						   (PAIR NIL))
					       (TAGBODY
						 (GO #:VALUE24149)
						 #:VALUE24148
						 (setq pair (pop #:VALUE24150))
						 (when (not (member (nth 1 pair) keywords-entered))
						   (setq result (cons pair result)))
						 #:VALUE24149
						 (unless (endp #:VALUE24150)
						   (go #:VALUE24148))
						 (return-from nil nil)))
					     result)))
			       :STREAM STREAM
			       :PROMPT-AFTER NIL
			       :PROMPT (AND FIRST-KEYWORD "keywords")
			       :PROVIDE-DEFAULT NIL)))
		  (READ-DELIMITER-CHAR STREAM)
		  (COND ((EQL KEYWORD-INPUT ':SHOW-CHAR-CODE)
			 (SETQ SHOW-CHAR-CODE
			       (QUERY 'BOOLEAN :DEFAULT NIL
				      :STREAM STREAM :PROMPT "Show Char Code"))
			 (SETQ KEYWORDS-ENTERED
			       (CONS :SHOW-CHAR-CODE KEYWORDS-ENTERED)))
			(T
			 (PARSE-ERROR KEYWORD-INPUT NIL "Not a Valid Keyword."))))
		(SETQ FIRST-KEYWORD NIL)
		#:G8862 (UNLESS (= (LENGTH KEYWORDS-ENTERED) (LENGTH KEYWORD-ALIST))
			  (GO #:G8861))
		#:G8863 (RETURN-FROM NIL NIL))
	  (LIST FONT :SHOW-CHAR-CODE SHOW-CHAR-CODE))))))

(define-lisp-window-command (com-show-character-style)
			    ((style 'character-style :PROMPT "Choose Style"
				    :default '(:fix :roman :large))
			     &KEY
			     (show-char-code 'boolean :PROMPT "Show Char Code"))
  (let ((font (get-font-from-style style)))
    (when font
      (com-show-font font :show-char-code show-char-code))))

(define-lisp-window-command (com-set-default-character-style)
			    ((style 'character-style :PROMPT "Choose Style"
				    :DEFAULT '(:fix :roman :large)))
  (when style
    (set-window-style *Standard-Output* style)))

#+lucid
(define-lisp-window-command (com-start-gc :NAME "Start GC") ()
  (user::gc))

(define-lisp-window-command (com-show-room) ((detailed 'BOOLEAN :DEFAULT T
						       :PROMPT "Detailed (Yes or No)"))
  (terpri)
  (room detailed))

(define-lisp-window-command (com-apropos)
			    ((strings '(sequence STRING) :Prompt "String to Search")
			     &KEY
			     (packages '(or (sequence PACKAGE) (member :all)) :DEFAULT :all)
			     (type '(sequence (member :variable :function-name
						      ;; 6/14/90 (SLN) for genera 8.0
						      #+ew-clos :class
						      :all))
				   :PROMPT "Symbol Types"
				   :DEFAULT :all)
			     (external 'boolean :DEFAULT T)
			     (internal 'boolean :DEFAULT T)
			     ;(return-list 'boolean :DEFAULT NIL)
			     )
  (let ((result NIL) (return-list NIL))
    (catch 'done
      (flet ((print-result (symbol)
	       (when (and (dolist (string strings NIL)
			    (when (search string (symbol-name symbol) :TEST #'char-equal)
			      (return T)))
			  (or (eq type :ALL)
			      (and (member :VARIABLE type)
				   (boundp symbol))
			      (and (member :function-name type)
				   (fboundp symbol))
			      ;; 6/14/90 (sln) for non pcls
			      (and (member :class type) (find-class symbol NIL)))
			  (multiple-value-bind (symbol external-or-internal)
			      (intern (symbol-name symbol))
			    (declare (ignore symbol))
			    (or (and external
				     (eq :EXTERNAL external-or-internal))
				(and internal
				     (eq :INTERNAL external-or-internal)))))
		 (if return-list
		     (push symbol result)
		     (let ((need-comma NIL))
		       (terpri)
		       (let (#+symbolics (*Package* NIL)) ;; so package prints out nicely.
			 (prin1 symbol))
		       (when (fboundp symbol)
			 (simple-princ " - Function ")
			 (prin1 (arglist symbol))
			 (setq need-comma T))
		       (when (boundp symbol)
			 (simple-princ (if need-comma ", Bound" " - Bound"))
			 (setq need-comma T))
		       #+symbolics
		       (when (get symbol 'SI:FLAVOR)
			 (simple-princ (if need-comma ", Flavor" " - Flavor"))
			 (setq need-comma T))
		       (when (find-class symbol NIL)
			 (simple-princ (if need-comma ", Class" " - Class"))))))
	       (when (and (listen-any *Standard-Input* 0)
			  (read-any-char *Standard-Input*))
		 (throw 'done NIL))))
	(if (or (null packages) (eq packages :ALL))
	    (do-all-symbols (symbol)
	      (print-result symbol))
	    (dolist (package packages)
	      (do-symbols (symbol package)
		(print-result symbol))))))
    (if return-list
	(values result)
	NIL)))


(define-lisp-window-command (com-show-environment)
			    ()
  (fresh-line)
  (make-table ()
    (when (fboundp 'month-length) ;; time package is loaded.
      (table-row ()
	(entry () "Time")
	(entry () (display (get-universal-time) 'universal-time))))
    (table-row ()
      (entry ()	"Lisp Implementation Type")
      (entry () (simple-princ (lisp-implementation-type))))
    (table-row ()
      (entry ()	"Lisp Implementation Version")
      (entry () (simple-princ (lisp-implementation-version))))
    (table-row ()
      (entry ()	"Machine Type")
      (entry () (simple-princ (machine-type))))
    (table-row ()
      (entry ()	"Machine Version")
      (entry () (simple-princ (machine-version))))
    (table-row ()
      (entry ()	"Machine Instance")
      (entry () (simple-princ (machine-instance))))
    (table-row ()
      (entry ()	"Software Type")
      (entry () (simple-princ (software-type))))
    (table-row ()
      (entry ()	"Software Version")
      (entry () (simple-princ (software-version))))
    (table-row ()
      (entry ()	"Short Site Name")
      (entry () (simple-princ (short-site-name))))
    (table-row ()
      (entry ()	"Long Site Name")
      (entry () (simple-princ (long-site-name))))
    (table-row ()
      (entry ()	"Current Working Directory")
      (entry () (display (make-pathname :name nil :type nil :defaults *default-pathname-defaults* :version nil)
			 'pathname)))
    #+lucid
    (table-row ()
      (entry ()	"Lisp Image Name")
      (entry () (display (user::lisp-image-name) 'pathname)))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
