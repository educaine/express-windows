;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************



(cl:in-package 'ew)



(defun allocate-query-menu ()
  #.(fast)
  (or (pop *Query-Menu-Windows*)
      (let ((w (make-window 'WINDOW :MORE-P NIL :END-OF-PAGE-MODE :TRUNCATE
			    :BLINKER-P NIL
			    :MARGIN-COMPONENTS '((margin-scroll-bar
						   :margin :left
						   :visibility :if-needed))
			    :LABEL NIL
			    :SAVE-UNDER T :BORDERS 4)))
	(xlib::set-window-priority :ABOVE (window-real-window w) NIL)
	(setf (xlib:window-save-under (window-real-window w)) :ON)
	w)))
    

(defun deallocate-query-menu (menu)
  #.(fast)
  (clear-history menu)
  (push menu *Query-Menu-Windows*))

(defvar *Query-Command-Table* (make-command-table "Query-Values"))



(defvar *Query-Entry-Table* (make-hash-table :TEST 'equal))


(defvar *Inside-Querying-Window* NIL)
(defvar *Inside-Querying-Window-Edit* NIL)

(defun decode-type (type)
  #.(fast)
  (declare (values type data-arguments display-arguments))
  (if (consp type)
      ;; some args at least.
      (if (consp (first type))
	  (values (first (first type)) (cdr (first type)) (cdr type))
	  (values (first type) (cdr type) NIL))
      (values type NIL NIL)))


(defun querying-values-internal (stream own-window label near-mode display-exit-boxes
				  temporary-p initially-select-query-identifier
				  body &OPTIONAL menu-definer-p
				  (character-style '(:fix :roman :large))
				  resynchronize-every-pass
				  &AUX
				  (*Command-Table* *Query-Command-Table*))
  (warn-unimplemented-args temporary-p NIL initially-select-query-identifier NIL)
  (let ((*HIGHLIGHTED-PRESENTATION* NIL)
	(*Input-Editing-Options* NIL)
	(*RECORD-PRESENTATIONS-P* T)
	(*INPUT-CONTEXT* NIL)
	(*QUERY-LOCATION* NIL)
	(*QUERY-ACTIVE* NIL)
	(*INSIDE-INPUT-EDITOR-P* NIL)
	(*RESCANNING* NIL)
	(*ACTIVATION-CHARS* '((#\Newline #\CR #+symbolics #\END)))
	(*QUERY-STREAM* NIL)
	(*QUERY-DEFAULT* NIL)
	(*QUERY-Y* 0)
	(*QUERY-X* 0))
    (let ((stream (if own-window (allocate-query-menu) stream))
	  (returning-values NIL))
      (unwind-protect
	  (let (program (*Querying-Values-Stream* stream))
	    (clrhash *Query-Entry-Table*)
	    (when own-window
		  ;; make it as big as possible within constraints of superior.
		  ;; remember that set-window-inside-size will keep the window within
		  ;; its superior.
		  (set-window-inside-size *Querying-Values-Stream* 2000 2000))
	    (with-character-style (character-style stream)
				  (multiple-value-setq (returning-values program)
						       (initial-draw-querying-values-menu
							stream returning-values own-window
							label body display-exit-boxes near-mode))
				  (unless menu-definer-p
					  (catch 'done
					    (do ()(())
						(let ((*Inside-Querying-Window-Edit* T))
						  (read-accelerated-command :STREAM stream))
						(setq returning-values
						      (draw-querying-values-menu stream returning-values program
										 own-window resynchronize-every-pass)))))))
	(when own-window
	      (deallocate-query-menu stream)
	      (deexpose-window stream)))
      (unless own-window (fresh-line stream))
      (values-list returning-values))))


(defun initial-draw-querying-values-menu (stream returning-values own-window
					  label body display-exit-boxes near-mode
					  &AUX program)
  (setq program
	(memo (stream)
	  (querying-values-display stream label body own-window
				   display-exit-boxes)))
  (setq returning-values
	(multiple-value-list (run-memo program stream)))
  (when own-window
    (let* ((p (memo-presentation program))
	   (right (boxed-presentation-right p))
	   (bottom (boxed-presentation-bottom p)))
      (declare (fixnum right bottom))
      (incf right 30)
      (incf bottom (window-line-spacing stream))
      (set-window-inside-size stream (%+ 2 right) (%+ 2 bottom))
      (xlib::set-window-priority :ABOVE
				 (window-real-window stream) NIL)
      (expose-window-near stream near-mode)
      ;;necessary to redraw window since it was drawn on in
      ;; the deexposed state.
      (redraw-window stream)
      (push stream *Windows-Exposure-Ignores*)
      ;;(expose-window stream)
      ))
  (values returning-values program))

(defun draw-querying-values-menu (stream returning-values program
				  own-window resynchronize-every-pass)
  (let ((old-max-x (presentation-window-max-x-position stream))
	(old-max-y (presentation-window-max-y-position stream)))
    (declare (fixnum old-max-x old-max-y))
    (inhibit-scrolling (stream)
      (setq returning-values
	    (multiple-value-list (run-memo program stream)))
      (when resynchronize-every-pass
	(setq returning-values
	      (multiple-value-list (run-memo program stream)))))
    (when own-window
      (multiple-value-bind (width height)
	  (window-inside-size stream)
	(declare (fixnum width height))
	(when (and (or (> (presentation-window-max-x-position stream)
			  old-max-x)
		       (> (presentation-window-max-y-position stream)
			  old-max-y))
		   (or (%> (presentation-window-max-x-position stream)
			   (%- width 2))
		       (%> (presentation-window-max-y-position stream)
			   (%- height 2))))
	  (let ((new-width (if (> (presentation-window-max-x-position stream)
				  old-max-x)
			       (%+ 2 (max width
					  (presentation-window-max-x-position stream)))
			       width))
		(new-height (if (> (presentation-window-max-y-position stream)
				   old-max-y)
				(%+ 2 (max height
					   (presentation-window-max-y-position stream)))
				height)))
	    (set-window-inside-size
	      stream new-width new-height)
	    (push stream *Windows-Exposure-Ignores*)
	    ;; first draw the new part of the window on the bottom.
	    (redraw-window stream
			   0 height new-width new-height))))))
  returning-values)

(defun querying-values-display (stream label body own-window display-exit-boxes
				&AUX (*Inside-Querying-Window* T))
  (declare (ignore own-window))			;4/30/90 (SLN)
  (let ((returning-values NIL))
    (when label
      (memoize (:STREAM stream :VALUE label)
	(let ((string label)
	      (style '(NIL :BOLD :LARGE)))
	  (when (consp label)
	    (setq string (getf label :STRING "Querying Values Menu")
		  style (getf label :STYLE (getf label :CHARACTER-STYLE style))))
	  (with-character-style (style stream)
	    (if (or (stringp string) (symbolp string) (numberp string))
		(simple-princ string stream)
		(funcall label stream)))))
      (let ((label-end (read-cursorpos stream)))
	(fresh-line stream)
	(incf-cursorpos 0 2 stream)
	(let ((line-length label-end))
	  (memoize (:STREAM stream :VALUE line-length)
	    (multiple-value-bind (x y)
		(read-cursorpos stream)
	      (declare (ignore x))
	      (draw-line 0 y (%- line-length 4) y :stream stream
			 :thickness 1))
	    (incf-cursorpos 0 2 stream)))))
    (setq returning-values (multiple-value-list (funcall body stream)))
    (when display-exit-boxes
      (terpri stream)
      (memoize (:STREAM stream :VALUE T :ID 'EXIT-BOXES)
	(display-as
	  (:OBJECT 'DONE :TYPE 'EXIT-MENU-CHOICE :STREAM stream)
	  (with-character-face (:BOLD stream)
	    (simple-princ "Done" stream)))
	(simple-princ "   " stream)
	(display-as
	  (:OBJECT 'ABORT :TYPE 'EXIT-MENU-CHOICE :STREAM stream)
	  (with-character-face (:BOLD stream)
	    (simple-princ "Abort" stream)))))
    (values-list returning-values)))

#+ignore
(defun test1 ()
  (let ((value 0))
    (querying-values (*query-io* :own-window t :label "Foobar Prompt")
      (setq value (query 'integer :stream *query-io* :default value
			  :PROMPT "Enter An Integer")))
    (list value value)))


(defmacro command-button ((&OPTIONAL (stream '*standard-output*)
				     &KEY
				     (surround-p T)
				     (shape :rectangle)
				     (thickness 1))
			  prompt
			  &BODY conditional-forms)
  (if (stringp prompt) (setq prompt `(simple-princ ,prompt ,stream)))
  (if surround-p
      `(with-border (,stream :shape ,shape :thickness ,thickness)
	 (command-button-internal ,stream
				  #'(lambda (,stream)
				      ,prompt)
				  #'(lambda ()
				      . ,conditional-forms)))
      `(command-button-internal ,stream #'(lambda (,stream) ,prompt)
				#'(lambda ()
				    . ,conditional-forms))))

(defun command-button-internal (stream prompt conditional-forms)
  (memoize (:stream stream :value T)
    (display-as (:OBJECT conditional-forms :TYPE 'COMMAND-BUTTON :STREAM stream)
      (funcall prompt stream))))







(defun default-display-printer (object stream type queryably original-type)
  (declare (ignore type queryably original-type))
  (simple-princ object stream))


(defun display (object &OPTIONAL (type (type-of object)) &REST other-args
		&KEY (stream *Standard-Output*) queryably (sensitive t)
		form single-box
		(allow-sensitive-inferiors t) (allow-sensitive-raw-text t)
		original-type &ALLOW-OTHER-KEYS)
  (declare (ignore form allow-sensitive-raw-text sensitive other-args))
  (display-as (:STREAM stream :SINGLE-BOX single-box
		       :TYPE type
		       :OBJECT object
		       :ALLOW-SENSITIVE-INFERIORS
		       allow-sensitive-inferiors)
    (multiple-value-bind (type printer-function)
	(get-type-printer type)
      (let ((*Disable-Expression-Presentation-P* T))
	(funcall printer-function object stream type queryably original-type)))))





(defun allocate-menu-choose-menu (superior)
  #.(fast)
  (let ((w (find superior *Menu-Choose-Resource* :KEY #'window-superior)))
    (if w
	(setf *Menu-Choose-Resource* (delete w *Menu-Choose-Resource* :COUNT 1))
	(setq w (make-window 'window :SAVE-UNDER T :MORE-P NIL :BLINKER-P NIL
			     :SUPERIOR superior
			     :LABEL NIL
			     :MARGIN-COMPONENTS '((margin-scroll-bar
						    :margin :left
						    :visibility :if-needed))
			     :BORDERS 4)))
    (xlib::set-window-priority :ABOVE (window-real-window w) NIL)
    (setf (xlib:window-save-under (window-real-window w)) :ON)
    w))

(defun deallocate-menu-choose-menu (window)
  #.(fast)
  (push window *Menu-Choose-Resource*))


(defun get-menu-item-string (menu-item)
  #.(fast)
  (if (not (consp menu-item))
      menu-item
      (car menu-item)))

(defun get-menu-item-position (menu-item)
  (declare (values x y))
  #.(fast)
  (cond ((or (not (consp menu-item))
	     (not (consp (cdr menu-item)))
	     (null (cddr menu-item)))
	 NIL)
	(T (setq menu-item (cdr menu-item))
	   (values (getf menu-item :X) (getf menu-item :Y)))))

(defun get-menu-item-value (menu-item &OPTIONAL menu)
  #.(fast)
  (cond ((not (consp menu-item))
	 menu-item)
	((not (consp (cdr menu-item)))
	 (cdr menu-item))
	((null (cddr menu-item))
	 (second menu-item))
	(T (setq menu-item (cdr menu-item))
	   (cond ((getf menu-item :VALUE))
		 ((getf menu-item :EVAL)
		  (eval (getf menu-item :EVAL)))
		 ((getf menu-item :FUNCALL)
		  (funcall (getf menu-item :FUNCALL)))
		 ((getf menu-item :FUNCALL-WITH-SELF)
		  (funcall (getf menu-item :FUNCALL-WITH-SELF) menu))))))

(defun get-menu-item-selectable-p (menu-item)
  #.(fast)
  (or (not (consp menu-item))
      (not (consp (cdr menu-item)))
      (null (cddr menu-item))
      (not (getf (cdr menu-item) :NO-SELECT))))

(defun get-menu-item-character-style (menu-item)
  #.(fast)
  (if (or (not (consp menu-item))
	  (not (consp (cdr menu-item)))
	  (null (cddr menu-item)))
      NIL
      (or (getf (cdr menu-item) :CHARACTER-STYLE)
	  (getf (cdr menu-item) :STYLE))))

(defun default-menu-choose-printer (item stream type)
  (setq item (get-menu-item-string item))
  (if type
      (display item type :STREAM stream)
      (simple-princ item stream)))

(defun menu-choose (item-list &REST other-args
		    &KEY label default type (printer 'default-menu-choose-printer)
		    (near-mode '(:mouse)) alias-for-selected-windows (superior) (row-wise t)
		    (center-p NIL) (columns 1)
		    (character-style '(:fix :roman :very-large))
		    (momentary-p t) (temporary-p NIL)
		    minimum-width minimum-height
		    &ALLOW-OTHER-KEYS
		    &AUX (*Command-Table* *Query-Command-Table*))
  (declare (values selected-value selected-item mouse-char))
  (declare (fixnum columns))
  (warn-unimplemented-args alias-for-selected-windows NIL row-wise T temporary-p NIL)
  (setq label (getf other-args :PROMPT label))
  (with-presentations-enabled
    ;; if we have a printer than we need to make a fake window and pass it along
    ;; to each item and check its size.
    (let (menu-window fake-stream
	  (mouse-x *Global-Mouse-X*)
	  (mouse-y *Global-Mouse-Y*)
	  (warp-mouse-p NIL))
      (catch 'done
	(unwind-protect
	    (progn
	      (setq fake-stream (allocate-fake-window NIL NIL T)
		    menu-window (allocate-menu-choose-menu superior))
	      (let ((new-item-list NIL))
		(declare (list new-item-list))
		(with-character-style (character-style fake-stream)
		  (setq new-item-list
			(mapcar #'(lambda (item)
				    (let ((style (get-menu-item-character-style item)))
				      (clear-fake-window fake-stream)
				      (if style
					  (with-character-style (style fake-stream)
					    (if (get-menu-item-selectable-p item)
						(display-as (:OBJECT item
							     :TYPE 'POPUP-MENU-CHOICE
							     :STREAM fake-stream)
						  (funcall printer item fake-stream type))
						(funcall printer item fake-stream type)))
					  (if (get-menu-item-selectable-p item)
					      (display-as (:OBJECT item
							   :TYPE 'POPUP-MENU-CHOICE
							   :STREAM fake-stream)
						(funcall printer item fake-stream type))
					      (funcall printer item fake-stream type))))
				    (cons (presentation-window-presentations fake-stream)
					  item))
			      item-list))
		  ;; how many columns
		  (let (max-width max-height (prompt-right 0) (prompt-bottom 0) ;unused1 unused2 ;4/30/90 (SLN)
			prompt-presentations)
		    (dolist (item new-item-list)
		      (multiple-value-bind (left top right bottom)
			  (get-presentations-boundaries (first item))
			(declare (ignore left top) (fixnum right bottom))
			(if (null max-width)
			    (setq max-width right max-height bottom)
			    (progn
			      (maximize max-width right)
			      (maximize max-height bottom)))))
		    (incf (the fixnum max-width) 5) (incf (the fixnum max-height))
		    ;; don't forget to figure out the prompt size.
		    (when label
		      (clear-fake-window fake-stream)
		      (simple-princ label fake-stream)
		      (setq prompt-presentations
			    (presentation-window-presentations fake-stream))
;		      (multiple-value-setq (unused1 unused2 prompt-right prompt-bottom)
		      ;; 4/30/90 (sln)
		      (multiple-value-setq (ignore ignore prompt-right prompt-bottom)
			(get-presentations-boundaries prompt-presentations)))
		    ;; for now, make the menu two columns
		    (let ((window-width (%+ 2 (max (%* columns max-width)
						   (the fixnum prompt-right))))
			  (window-height (%+ (if (zerop prompt-bottom) 0
						 (+ 2 prompt-bottom))
					     20
					     (%* max-height
						 (the fixnum (ceiling (length new-item-list)
								      columns)))))
			  default-x default-y)
		      (declare (fixnum window-width window-height))
		      (set-window-inside-size menu-window
					      (setq window-width
						    (max (the fixnum (or minimum-width 0))
							 window-width))
					      (max (the fixnum (or minimum-height 0))
						   window-height))
		      (clear-history menu-window)
		      (insert-presentations-with-offset menu-window prompt-presentations 0 0)
		      (do ((items new-item-list (cdr items))
			   (y (%+ 4 prompt-bottom) (%+ y max-height))
			   (number-in-y-direction
			     (values (ceiling (length new-item-list) columns)))
			   (x 2)
			   (number 0 (%1+ number)))
			  ((null items))
			(when (%= number number-in-y-direction)
			  (setq y (%+ 2 prompt-bottom) number 0)
			  (incf x (the fixnum max-width)))
			(if center-p
			    (multiple-value-bind (left top right)
				(get-presentations-boundaries (caar items))
			      (declare (ignore top left) (fixnum right))
			      (insert-presentations-with-offset
				menu-window
				(caar items)
				(%+ x (%- (the (values fixnum number) (floor window-width 2))
					  (the (values fixnum number) (floor right 2))))
				y))
			    (insert-presentations-with-offset menu-window (caar items) x y))
			(when (and default
				   (eq default (cdar items)))
			  (multiple-value-bind (left top right bottom)
			      (get-presentations-boundaries (caar items))
			    (setq default-x (the (values fixnum number) (floor (%+ left right) 2))
				  default-y (the (values fixnum number) (floor (%+ top bottom) 2))))))
		      (expose-window-near menu-window near-mode)
		      (push menu-window *Windows-Exposure-Ignores*)
		      ;;(expose-window menu-window)
		      (redraw-window menu-window)
		      (unless (zerop prompt-bottom)
			(draw-line 0 (%+ 2 prompt-bottom) window-width (%+ 2 prompt-bottom)
				   :THICKNESS 2
				   :STREAM menu-window))
		      (when default-x
			(convert-window-coords-to-screen-coords-macro
			  menu-window default-x default-y T)
			(mouse-warp default-x default-y))
		      (let ((*Momentary-Menu* (and momentary-p menu-window)))
			(catch 'EXIT-TEMPORARY-WINDOW
			  (read-accelerated-command :STREAM menu-window)
			  (setq warp-mouse-p T)))
		      NIL)))))
	  (deallocate-fake-window fake-stream)
	  (deexpose-window menu-window)
	  ;; if the menu was exited by choosing something, than warp mouse.
	  (when warp-mouse-p (mouse-warp mouse-x mouse-y))
	  (deallocate-menu-choose-menu menu-window)
	  NIL)))))

(defun menu-choose-from-set (list type &REST other-args
			     &KEY label default printer
			     (near-mode '(:mouse)) alias-for-selected-windows (superior)
			     (center-p NIL) (columns 1)
			     (character-style '(:jess :roman :large))
			     (momentary-p t) (temporary-p NIL)
			     minimum-width minimum-height
			     &ALLOW-OTHER-KEYS
			     &AUX (*Command-Table* *Query-Command-Table*))
  (declare (ignore label default printer
		   near-mode alias-for-selected-windows superior
		   center-p columns
		   character-style
		   momentary-p temporary-p
		   minimum-width minimum-height))
  (apply #'menu-choose (mapcar #'(lambda (x) (cons x x)) list)
	 :type type other-args))

(defmacro presentation-blip-case (blip &REST clauses)
  (let* ((variable (if (symbolp blip) blip (gensym "VARIABLE")))
	 (body (cons 'cond (mapcar #'(lambda (clause)
				       (if (eq (first clause) T)
					   clause
					   (cons `(presentation-blip-typep
						    ,variable
						    ',(first clause))
						 (cdr clause))))
				   clauses))))
    (if (symbolp blip)
	body
	`(let ((,variable ,blip))
	   ,body))))


(defmacro with-presentation-input-context ((type &REST options)
					   (&OPTIONAL (blip-var '.blip.))
					   non-blip-form &BODY blip-cases)
  (let ((inherit (getf options :INHERIT T)))
    `(let ((*Input-Context* (cons ,type
				  (if ,inherit *Input-Context* NIL))))
       (let ((,blip-var (catch *Input-Context*
			  ,non-blip-form)))
	 (if (not (presentation-blip-p ,blip-var))
	     ,blip-var
	     (presentation-blip-case ,blip-var
				     . ,blip-cases))))))



;; This is used when the type has no parser.  I assume that they are hoping
;; there is something mouse sensitive to query instead.
(defun query-default-parser (stream &REST args)
  (declare (ignore args))
  (do ()(())
    (unless (member (read-char-for-query stream) '(#\Space #\CR))
      (parse-error NIL NIL "Not A Valid Input."))))


(defun query (type &REST other-args
	      &KEY (stream *query-io*)
	      (prompt :enter-type)
	      (prompt-mode :normal)
	      (active-p T)
	      (original-type type)
	      activation-chars additional-activation-chars
	      token-delimiter-chars additional-token-delimiter-chars
	      (inherit-context t)
	      (default NIL default-provided)
	      (provide-default 'unless-default-is-nil) (default-type original-type)
	      (display-default prompt) history
	      (prompts-in-line *Query-Active*) initially-display-possibilities
	      query-identifier separate-inferior-queries confirm
	      x y
	      &ALLOW-OTHER-KEYS &AUX (*Query-X* x)(*Query-Y* y))
  (declare (values object type) (ignore query-identifier confirm))
  (declare (ignore active-p))			;4/30/90 (SLN)
  ;; query-identifier is important for passing on to query-values
  ;; present-default, input-sensitizer, handler-type are not important
  (warn-unimplemented-args display-default prompt history NIL
			   separate-inferior-queries NIL)
  (with-type-decoded (type-name) type
    (when default
      (setq default (call-type-preprocessor default type original-type default-type)))
    (let ((*Input-Context* (if inherit-context (cons type *Input-Context*)
			       (list type)))
	  (*Query-Default* (generate-query-default default default-provided provide-default
						     type-name type))
	  (*Query-Stream* stream)
	  location)
      (if (not *Inside-Querying-Window*)
	  (with-activation-chars (activation-chars :override activation-chars)
	    (with-activation-chars (additional-activation-chars)
	      (with-token-delimiters (token-delimiter-chars :override token-delimiter-chars)
		(with-token-delimiters (additional-token-delimiter-chars)
		  (catch 'QUERY-DEFAULT
		    (multiple-value-bind (expanded-type parser)
			(get-type-parser type)
		      (let ((prompt-string (generate-prompt-string
					     prompt type prompts-in-line prompt-mode)))
			(multiple-value-bind (result result-type)
			    (catch *Input-Context*
			      (flet ((query-2 ()
				       (let ((*Query-Active* T)
					     (*Query-Location*
					       (read-location stream)))
					 (setq location *Query-Location*)
					 (if (eq *Parse-Type* :DEFAULT-UNCONFIRMED-ARGS)
					     (apply #'handle-query-default
						    type other-args)
					     (funcall parser stream expanded-type
						      original-type
						      initially-display-possibilities
						      default NIL default-type)))))
				(if (window-p stream)
				    (if (or *Query-Active*
					    *Inside-Input-Editor-P*)
					(progn
					  (when prompt-string
					    (noise-string-out stream prompt-string))
					  (query-2))
					(progn
					  (if prompt-string
					      (progn
						(when prompt (fresh-line stream))
						(with-input-editing-options
						  (((:PROMPT :OVERRIDE) prompt-string))
						  (input-editor stream #'QUERY-2)))
					      (with-input-editing-options
						((:PROMPT NIL))
						(input-editor stream #'QUERY-2)))))
				    (query-2))))
			  (values (setf (get type-name :QUERY-DEFAULT)
					(if (presentation-blip-p result)
					    (let ((string
						    (make-printed-representation-of-presentation-blip
						      (presentation-blip-type result)
						      (presentation-blip-object result))))
					      (replace-input-editor-string stream location string)
					      (when (not *Query-Active*)
						;; we are no long in the input editor, but it
						;; still needs to be updated with a presentation
						(final-display-of-input-editor
						  stream (presentation-blip-object result)
						  (presentation-blip-type result)))
					      (presentation-blip-object result))
					    result))
				  (let ((real-type (if (presentation-blip-p result)
						       (presentation-blip-type result)
						       (or result-type type))))
				    (if (eq type 'EXPRESSION)
					type
					real-type)))))))))))
	  (apply #'QUERY-FOR-QUERY-MENU
		 type type-name
		 :DEFAULT default
		 other-args)))))


(defun handle-query-default (type &REST other-args &KEY confirm
			      &ALLOW-OTHER-KEYS)
  (declare (ignore other-args))
  (if confirm
      (progn (setq *Parse-Type* :NORMAL)
	     (parse-error NIL NIL "Must be filled in."))
      (progn
	(replace-input-editor-string
	  *Query-Stream* *Query-Location*
	  (multiple-value-bind (type printer-function)
	      (get-type-printer type)
	    (with-output-to-string (stream)
	      (funcall printer-function *Query-Default* stream type
		       NIL type))))
	(throw 'QUERY-DEFAULT *Query-Default*))))



(defun query-for-query-menu (type type-name
			     &REST other-args
			     &KEY (stream *query-io*) (prompt :enter-type)
			     (prompt-mode :normal) (original-type type)
			     (active-p T)
			     (default NIL default-provided)
			     (provide-default 'UNLESS-DEFAULT-IS-NIL)
			     query-identifier x y
			     &ALLOW-OTHER-KEYS)
  (declare (ignore default-provided))
  (declare (ignore other-args))			;4/30/90 (SLN)
  (unless (symbolp type-name)
    (error "Cannot handle type ~A for query." type))
  (let ((prompt-string (generate-prompt-string prompt type NIL prompt-mode))
	(exact-place-p (or x y)))
    (when prompt-string (setq prompt-string (my-string prompt-string)))
    (when (and (not query-identifier) prompt-string)
      (setq query-identifier (intern prompt-string)))
    (multiple-value-bind (expanded-type query-displayer)
	(get-type-query-values-displayer type)
      (unless exact-place-p
	(when prompt (fresh-line stream)))
      ;; make sure *Query-X* has a value now.
      (unless (and *Query-X* *Query-Y*)
	(multiple-value-bind (x y)
	    (read-cursorpos stream)
	  (setq *Query-X* (or *Query-X* x)
		*Query-Y* (or *Query-Y* y))))
      (flet ((body ()
	       (when exact-place-p
		 (set-cursorpos stream x y))
	       (when prompt-string
		 (memoize (:STREAM stream :value prompt-string :value-test #'Equal)
		   (simple-princ prompt-string stream)))
	       (let ((table-entry (gethash query-identifier *Query-Entry-Table*)))
		 (let ((value (if table-entry (first table-entry) default)))
		   (setf (gethash query-identifier *Query-Entry-Table*) (list value))
		   #+debugging-qv
		   (dformat "~%Entering Memo for Display with value ~D type ~D."
			    value type)
		   (memoize (:STREAM stream
			     :VALUE (if *Running-Menu-Definer-P*
					(list value query-identifier type)
					value)
			     :VALUE-TEST
			     #-debugging-qv
			     (if *Running-Menu-Definer-P* #'equalp #'eql)
			     #+debugging-qv
			     #'(lambda (x y)
				 (dformat "~%Testing X ~D Y ~D" x y)
				 (equalp x y)))
		     #+debugging-qv
		     (dformat "~%Inside Entering Memo for Display with.")
		     (funcall query-displayer
			      stream value query-identifier expanded-type original-type
			      provide-default))
		   (unless exact-place-p (terpri stream))
		   ;;(increment-cursorpos stream 0 2)
		   value))))
	(when active-p
	    (body))
;	(if active-p
;	    (body)
;	    (let ((context (make-gc-context *Query-Stream*))
;		  (*Draw-Alu* *Gray-Gcontext*))
;	      (setf (xlib:gcontext-tile context) (get-pattern-pixmap pattern)
;		    (xlib:gcontext-fill-style context) :tiled)
;	      (body)))
	))))


(defun generate-prompt-string (prompt type prompts-in-line prompt-mode)
  #.(fast)
  (let ((*Disable-Expression-Presentation-P* T))
    (cond ((not prompt) NIL)
	  ((eq :RAW prompt-mode) prompt)
	  (T (with-output-to-presentation-recording-string (stream)
	       (cond (prompts-in-line (simple-princ "(" stream))
		     ((eq prompt :enter-type) (simple-princ "Enter " stream)))
	       (cond ((stringp prompt) (simple-princ prompt stream))
		     ((eq prompt :ENTER-TYPE)
		      (describe-type type stream NIL NIL))
		     (T (format T "Function Prompts are not implemented.") NIL))
	       (unless (or (eq *Query-Default* 'no-query-default)
			   *Inside-Querying-Window*)
		 (simple-princ " [" stream)
		 (display *Query-Default* type :STREAM stream)
		 (simple-princ "]" stream))
	       (simple-princ (if prompts-in-line
				 ") " ": ") stream))))))


(defun generate-query-default (default default-provided provide-default
				 type-name type)
  #.(fast)
  (if (eq type-name 'OR)
      ;; get default from first element instead
      (with-type-decoded (type-name) (second type)
	(generate-query-default default default-provided provide-default
				 type-name
				 (second type)))
      (if (not provide-default)
	  'no-query-default
	  (let ((result
		  (if default-provided
		      default
		      (get type-name :QUERY-DEFAULT 'no-query-default))))
	    (if (or (and (not result)
			 (eq provide-default 'unless-default-is-nil))
		    (not (typep result type)))
		'no-query-default
		result)))))



(defun peek-char-for-query (stream &OPTIONAL hang)
  #.(fast)
  #+debugging-ie
  (dformat "~&Peek Hang ~S  Rescan ~S  Unread Chars ~S Listen ~A"
	   hang
	   *Rescanning*
	   (input-editor-state-unread-characters (window-input-editor stream))
	   (listen-any stream 0))
  (if (or hang
	  *Rescanning*
	  (input-editor-state-unread-characters (window-input-editor stream))
	  (listen-any stream 0))
      (let ((character (read-char-for-query stream)))
	(unread-char-for-query character stream)
	character)
      NIL))

(defun read-char-for-query (stream)
  #.(fast)
  (if (window-p stream)
      (editor stream)
      (read-char stream NIL :EOF)))


(defun unread-char-for-query (char stream &AUX (original-char char))
  #.(fast)
  (if (listp char) (setq char (second char)))
  (if (window-p stream)
      (let* ((state (window-input-editor stream)))
	#+debugging-ie
	(describe-editor-state state "Start of Unread")
	(if *Rescanning*
	    ;; better check that we are unreading same character that was there last time.
	    (let ((scan-pointer (input-editor-state-scan-pointer state)))
	      #+debugging-ie
	      (dformat "~&Rescanning pointer ~D." scan-pointer)
	      (unless (char-equal (aref (input-editor-state-string state)
					(%1- scan-pointer))
				  char)
		(error "Unreading different character than read."))
	      (decf (input-editor-state-scan-pointer state))
	      ;;(unless (consp original-char)
		(decf (input-editor-state-cursorpos state))
		#+debugging-ie
		(dformat "~&Decrementing cursorpos to ~D"
			 (input-editor-state-cursorpos state)));)
	    (if (and (not (consp original-char))
		     (not (equal char
				 (aref (input-editor-state-string state)
				       (%1- (input-editor-state-cursorpos state)))))
		     (not (%= (input-editor-state-cursorpos state)
			      (fill-pointer (input-editor-state-string state)))))
		(error "Should this be different.")
		(progn
		  #+debugging-ie
		  (dformat "~%Pushing Unread Char ~D on List ~D."
			   char (input-editor-state-unread-characters state))
		  (push char (input-editor-state-unread-characters state))
		  ;;(unless (consp original-char)
		    (decf (input-editor-state-cursorpos state))
		    #+debugging-ie (dformat "~&Decrementing cursorpos to ~D"
					   (input-editor-state-cursorpos state))
		    (delete-space-in-state state 1 :IGNORE))));)
	#+debugging-ie
	(describe-editor-state state "End of Unread"))
      (unread-char char stream)))

(defun delete-char-for-query (char stream &AUX (original-char char))
  ;; since the character has already been read we need to find it and just not put it
  ;; back on an unread list.
  (if (listp char) (setq char (second char)))
  (let* ((state (window-input-editor stream)))
    #+debugging-ie
    (describe-editor-state state "Start of Delete Char")
    (if *Rescanning*
	;; better check that we are unreading same character that was there last time.
	(let ((scan-pointer (input-editor-state-scan-pointer state)))
	  (error "I don't handle this case yet.")
	  #+debugging-ie
	  (dformat "~&Rescanning pointer ~D." scan-pointer)
	  (unless (char-equal (aref (input-editor-state-string state)
				    (%1- scan-pointer))
			      char)
	    (error "Unreading different character than read."))
	  (decf (input-editor-state-scan-pointer state))
	  ;;(unless (consp original-char)
	  (decf (input-editor-state-cursorpos state))
	  #+debugging-ie
	  (dformat "~&Decrementing cursorpos to ~D"
		   (input-editor-state-cursorpos state)))	;)
	(if (and (not (consp original-char))
		 (not (equal char
			     (aref (input-editor-state-string state)
				   (%1- (input-editor-state-cursorpos state)))))
		 (not (%= (input-editor-state-cursorpos state)
			  (fill-pointer (input-editor-state-string state)))))
	    (error "Should this be different.")
	    (progn
	      #+debugging-ie
	      (dformat "~%Pushing Unread Char ~D on List ~D."
		       char (input-editor-state-unread-characters state))
	      ;;(push char (input-editor-state-unread-characters state))
	      (decf (input-editor-state-cursorpos state))
	      #+debugging-ie (dformat "~&Decrementing cursorpos to ~D"
				      (input-editor-state-cursorpos state))
	      (delete-space-in-state state 1 :IGNORE))))
    #+debugging-ie
    (describe-editor-state state "End of Delete Char")))


(defun read-delimiter-for-query (stream)
  #.(fast)
  (let ((char (read-char-for-query stream)))
    (if (not (member char '(#\Space #\-))) (beep))
    (replace-input-editor-string stream
				(read-location stream)
				" ")))

(defun insert-delimiter-into-stream (stream delimiter)
  #.(fast)
  (with-presentations-disabled
    (replace-input-editor-string stream
				(read-location stream)
				(string delimiter))))

(defun read-char-for-query-internal (stream &AUX (initial-p T))
  ;; initial-p starts at T to make sure that when we first enter this function
  ;; it will get a chance to highlight a presentation right away if there is no key
  ;; input.
  #.(fast)
  (let ((old-mouse-x #.*mouse-x*) (old-mouse-y #.*Mouse-Y*) x y
	(old-bits (mouse-chord-shifts))
	(*Highlighted-Presentation* NIL)
	(*Highlighted-Presentation-Window* NIL)
	(old-mouse-window (or *mouse-window* stream))
	(mouse-over-window-p NIL))
    (declare (fixnum old-mouse-x old-mouse-y old-bits))
    (unwind-protect
	(do ()(())
	  ;; wait in this loop until something interesting happens.
	  ;; i.e. Mouse motion, keyboard input, mouse-button click
	  ;; or the first time we enter it.
	  (process-wait NIL
			#'(lambda (stream old-mouse-window old-x old-y)
			    (declare (fixnum old-x old-y))
			    (or initial-p
				(not (%= old-x #.*Mouse-X*))
				(not (%= old-y #.*Mouse-Y*))
				#+x
				(listen-any stream)
				(not (%= old-bits (mouse-chord-shifts)))
				(not (%= old-x #.*Mouse-X*))
				(not (%= old-y #.*Mouse-Y*))
				(not (eq old-mouse-window *Mouse-Window*))))
			stream old-mouse-window old-mouse-x old-mouse-y)
	  (when *Momentary-Menu*
	    ;; check to see if we have moved off of a momentary menu, if so exit it!
	    (cond ((and (not initial-p)
			(eq *Momentary-Menu* *Mouse-Window*))
		   (setq mouse-over-window-p T))
		  (mouse-over-window-p
		   ;; figure out how far away the mouse is from the window.
		   (multiple-value-bind (left top right bottom)
		       (window-edges *Momentary-Menu*)
		     (unless (and (< (%- left *Momentary-Menu-Hysteresis-Pixels*)
				     *Global-Mouse-X*
				     (%+ right *Momentary-Menu-Hysteresis-Pixels*))
				  (< (%- top *Momentary-Menu-Hysteresis-Pixels*)
				     *Global-Mouse-Y*
				     (%+ bottom *Momentary-Menu-Hysteresis-Pixels*)))
		       (throw 'EXIT-TEMPORARY-WINDOW :ABORT))))))
	  (setq initial-p NIL)
	  (when (and *Highlighted-Presentation*
		     (listen-any stream 0)
		     (%= old-mouse-x #.*Mouse-X*)
		     (%= old-mouse-y #.*Mouse-Y*))
	    ;; we haven't moved since we last found a presentation, use it immediately.
	    (return NIL))
	  (setq old-mouse-x #.*Mouse-X* old-mouse-y #.*Mouse-Y*
		old-bits (mouse-chord-shifts))
	  ;; If there is a character already waiting, then don't bother doing
	  ;; mouse highlighting stuff.  Return Immediately.
	  (when (and (listen-any stream 0) (characterp (peek-any stream)))
	    (return NIL))
	  #+x
	  (multiple-value-setq (x y)
	    (convert-screen-coords-to-window-coords *Mouse-Window*
						    old-mouse-x old-mouse-y))
	  (let ((object
		  (find-closest-presentation-with-valid-handler *Mouse-Window* x y)))
	    (show-documentation-for-handlers object *Mouse-Window* x y)
	    (if object
		(when (not (eq object *Highlighted-Presentation*))
		  (when *Highlighted-Presentation*
		    (highlight-presentation *Highlighted-Presentation*
					    *Highlighted-Presentation-Window*)
		    (setq *Highlighted-Presentation* NIL))
		  (highlight-presentation object *Mouse-Window*)
		  (setq *Highlighted-Presentation* object
			*Highlighted-Presentation-Window* *Mouse-Window*))
		(when *Highlighted-Presentation*
		  (highlight-presentation *Highlighted-Presentation*
					  *Highlighted-Presentation-Window*)
		  (setq *Highlighted-Presentation* NIL))))
	  (when (listen-any stream 0)
	    (return NIL)))
      (when *Highlighted-Presentation*
	(highlight-presentation *Highlighted-Presentation*
				*Highlighted-Presentation-Window*))
      (set-mouse-documentation-string ""))
    (handle-query-input x y *Highlighted-Presentation* *Mouse-Window* stream)))




(defun handle-query-input (x y highlighted-object stream original-stream)
  #.(fast)
  (let ((command (read-any stream)))
    (cond ((and (listp command)
		(eq (first command) :MOUSE-CLICK))
	   (cond (highlighted-object
		  (multiple-value-bind (handler contexts)
		      (find-selected-closest-presentation-with-valid-handler
			stream
			highlighted-object (second command) x y)
		    (if handler
			(let ((body-values
			       (multiple-value-list
				(funcall (handler-body handler)
					 (presentation-object highlighted-object)
					 :WINDOW stream :MOUSE-X x :MOUSE-Y y
					 :PRESENTATION highlighted-object
					 :GESTURE (second command)
					 :PRESENTATION-TYPE
					 (presentation-type highlighted-object)))))
			  (if (not (first body-values))
			      (read-char-for-query-internal original-stream)
			      (let ((object (first body-values))
				    (type (or (second body-values) (first contexts)))
				    (options (cddr body-values)))
				;; (dformat "~%Body Values ~S" body-values)
				;; (dformat "~&Throwing to context ~S." contexts)
				(if (not (getf options :activate 'not-null))
				    ;; setup for rescanning, don't activate.
				    (let ((string
					    (make-printed-representation-of-presentation-blip
					      type object)))
				      (replace-input-editor-string
					stream *Query-Location* string)
				      (throw 'rescan 'rescan))
				    (throw contexts
				      (make-presentation-blip
					:OBJECT object :TYPE type
					:MOUSE-CHAR (second command)
					:OPTIONS options))))))
			(values command (second command) x y))))
		 ((lisp:typep stream 'scroll-bar)
		  (handle-scrolling command stream)
		  (read-char-for-query-internal original-stream))
		 (T (beep)
		    (read-char-for-query-internal original-stream))))
	  ((member command *Scroll-Commands* :TEST #'EQUALP)
	   (handle-scrolling command stream)
	   (read-char-for-query-internal original-stream))
	  (T command))))




;; reading a double quote as the first character will cause this to read up to the next
;; double quote.
;;; experimental version for handling QUOTES.  Haven't decided all ramifications yet.
#+ignore 
(defun read-standard-token (stream)
  (if (window-p stream)
      (let* ((state (window-input-editor stream))
	     (cursorpos (input-editor-state-cursorpos state)))
	(when *Debug* (dformat "~%Cursorpos ~D." cursorpos))
	(let* ((first-char (read-char-for-query stream))
	       (quote-p (compare-char first-char #\"))
	       (value
		 (if (or (consp character)
			 (presentation-blip-p character))
		     (progn
		       (when (consp character)
			 (unread-char-for-query character stream))
		       character)
		     (do ()(())
		       (let ((character (read-char-for-query stream)))
			 (when (or (consp character)
				   (presentation-blip-p character)
				   (and quote-p
					(compare-char character #\")))
			   (when (consp character)
			     (unread-char-for-query character stream))
			   (return character)))))))
	  (if (presentation-blip-p value)
	      (presentation-blip-object value)
	      (subseq (input-editor-state-string state)
		      cursorpos
		      (input-editor-state-cursorpos state)))))
      ;; read a token from a standard stream that has no input editor.
      (with-output-to-string (output-stream)
	(do ()(())
	  (let ((char (read-char-for-query stream)))
	    (cond ((eq char :EOF)
		   (return NIL))
		  ((consp char)
		   (unread-char-for-query char stream)
		   (return NIL))
		  (T (write-char char output-stream))))))))

(defun read-standard-token (stream)
  (if (window-p stream)
      (let* ((state (window-input-editor stream))
	     (cursorpos (input-editor-state-cursorpos state)))
	#+debugging-ie
	(dformat "~%Cursorpos ~D." cursorpos)
	(let ((value
		(do ()(())
		  (let ((character (read-char-for-query stream)))
		    (when (or (consp character)
			      (presentation-blip-p character))
		      (when (consp character)
			(unread-char-for-query character stream))
		      (return character))))))
	  (if (presentation-blip-p value)
	      (presentation-blip-object value)
	      (subseq (input-editor-state-string state)
		      cursorpos
		      (input-editor-state-cursorpos state)))))
      ;; read a token from a standard stream that has no input editor.
      (with-output-to-string (output-stream)
	(do ()(())
	  (let ((char (read-char-for-query stream)))
	    (cond ((eq char :EOF)
		   (return NIL))
		  ((consp char)
		   (unread-char-for-query char stream)
		   (return NIL))
		  (T (write-char char output-stream))))))))

(defun make-printed-representation-of-presentation-blip (type object)
  (multiple-value-bind (expanded-type printer-function)
      (get-type-printer type)
    (with-output-to-string (stream)
      (funcall printer-function object stream expanded-type NIL type))))



(defun parse-error (input-string type &OPTIONAL (ctrl-string NIL) &REST args)
  (cond (*Parsing-Or-Type-P*
	 (throw 'or-parser (apply #'lisp-format NIL ctrl-string args)))
	((and (not (eq *Query-Default* 'no-query-default))
	      (or (equal input-string "") (equal input-string " ")))
	 (replace-input-editor-string
	   *Query-Stream* *Query-Location*
	   (multiple-value-bind (expanded-type printer-function)
	       (get-type-printer type)
	     (with-output-to-string (stream)
	       (funcall printer-function *Query-Default* stream expanded-type NIL type))))
	 (throw 'QUERY-DEFAULT *Query-Default*))
	(T
	 ;; if we have a parse error we need to print the warning message and wait for the user
	 ;; to keep trying.
	 ;; throw out to the input editor where we can try to be clever.
	 (throw 'PARSE-ERROR (list type ctrl-string (copy-list args)))
	 )))


;;; A parse error can not be handled in the context of where the error occured since you no
;;; longer have a good idea about what delimiters and activators are valid.  They only
;;; things you can be sure of is that unless some character in the string is modifed
;;; you cannot ever expect a valid input.  Therefore you must read in a special manner
;;; until some part of the string is modified except for the last part.  Adding onto the
;;; end of a string can never make it valid.

;;; This function gets called at the top of the input editor after throwing out past
;;; all the interveening calls to accept.
;;; We should call a simple editor here to look at keystrokes until we notice
;;; a modification.  If there is a modification of interest than rescan.
;;; Of course only rescan when a character is typed at the end of the line
;;; after the modification.

;; This has problems if we get a parsing error during rescan.  Not sure what to do yet.

;; Parsing errors are usually discovered after reading some object which usually implies that there is
;; some delimiter character left in the stream.  We check for that character and throw it away
;; if it is still hanging there.

(defun handle-parsing-error (stream state type ctrl-string args)
  ;; make sure we don't think there are any modifications.
  ;; then this flag should only be set to T when a modification has been
  ;; made to the string somewhere besides the end.
  (setf (input-editor-state-modified-p state) NIL)
  (with-token-delimiters (NIL :OVERRIDE T)
    (let ((presentation
	    (display-as (:type 'string :object "Error" :stream *Query-Stream*)
	      (terpri *Query-Stream*)
	      (if ctrl-string
		  (apply 'format *Query-Stream* ctrl-string args)
		  (format *Query-Stream* "Parse Error ~A." type)))))
      (unless *Rescanning*
	;; read character that caused error now.
	(and (peek-char-for-query *Query-Stream*)
	     (read-char-for-query *Query-Stream*)))
      (let ((*Parse-Error-Presentation* presentation))
	(let (;(next-char (peek-char-for-query *Query-Stream* T)) ;4/30/90 (SLN)
	      )
	  ;; put here just to make sure.  It should actually be deleted inside the EDITOR.
	  (when *Parse-Error-Presentation*
	    (remove-presentation *Query-Stream* *Parse-Error-Presentation* T))
	  ;; if we rescan and the rescan doesn't work we end up in an infinite loop.
	  ;; therefore we need to avoid rescanning when we know it can't possible
	  ;; get better.
	  ;; i.e. if the next character is a self-inserting character we don't have
	  ;; a chance
	  ;; just sit idling while reading characters until we get an automatic
	  ;; throw back to the rescanner.
	  (do ()(())
	    (editor stream)))))))

(defun query-from-string (type string &REST args
			   &KEY index (start 0) end &ALLOW-OTHER-KEYS)
  (declare (ignore args))			;4/30/90 (SLN)
  ;; 6/27/90 (sln) We need to refer to INDEX to suppress the compiler
  ;; warning about its value never being used.  WITH-INPUT-FROM-STRING
  ;; merely SETFs INDEX, but doesn't explicitly refer to it.
  index
  (with-input-from-string (stream string :INDEX index :START start :END end)
    (query type :STREAM stream :PROMPT NIL :PROMPTS-IN-LINE T
	   :provide-default NIL)))

(defun compare-char (char-from-query comparandum)
  (when (and (consp char-from-query) (member (first char-from-query)
					      '(:ACTIVATION :QUERY)))
    (setq char-from-query (second char-from-query)))
  (when (and (consp comparandum) (member (first comparandum)
					      '(:ACTIVATION :QUERY)))
    (setq comparandum (second comparandum)))
  (equal char-from-query comparandum))


#+ignore
(defun test2 (&AUX (*query-io* a))
  (let ((value 0))
    (querying-values (*query-io*)
      (setq value (query 'integer :stream *query-io* :default value
			  :Prompt "Enter Your Age: ")))
    (list value value)))

(defun query-values-into-list (descriptions &REST args &KEY (prompt NIL) (near-mode '(:MOUSE))
				(stream *Query-Io*) (own-window NIL)
				(temporary-p own-window)
				(initially-select-query-identifier NIL))
  (declare (ignore args))			;4/30/90 (sln)
  (let ((values NIL))
    (querying-values (stream :own-window own-window :label prompt
			      :temporary-p temporary-p :near-mode near-mode
			      :initially-select-query-identifier
			      initially-select-query-identifier)
      (setq values
	    (mapcar #'(lambda (description)
			(apply 'query (first description)
			       :STREAM stream (cdr description)))
		    descriptions)))
    values))

(defun query-values (descriptions &REST args &KEY (prompt NIL) (near-mode '(:MOUSE))
		      (stream *Query-Io*) (own-window NIL)
		      (temporary-p own-window) (initially-select-query-identifier NIL))
  (declare (ignore prompt near-mode stream
		   temporary-p initially-select-query-identifier))
  (values-list (apply #'query-values-into-list descriptions args)))



(defstruct query-values-value
  object
  query-identifier
  type
  stream
  presentation)

(defmacro standard-query-values-displayer ((stream object query-identifier provide-default)
					    &BODY body)
  (declare (ignore object))
  `(flet ((present-editable-choice (object type &OPTIONAL dont-present dont-highlight)
	    (let ((value-object
		    (make-query-values-value
		      :OBJECT object :STREAM *Querying-Values-Stream*
		      :TYPE type
		      :QUERY-IDENTIFIER ,query-identifier)))
	      (let ((presentation
		      (display-as (:OBJECT value-object
				   :SINGLE-BOX T
				   :TYPE 'QUERY-VALUES-VALUE-DISPLAY
				   :STREAM ,stream)
			(cond ((and ,provide-default (not dont-present)
				    (or (not (eq ,provide-default 'unless-default-is-nil))
					object))
			       (if dont-highlight
				   (display object type :stream ,stream)
				   (with-character-face (:BOLD stream)
				     (display object type :stream ,stream))))
			      (t (with-character-face (:ITALIC stream)
				   (describe-type type ,stream)))))))
		;; keep track of the presentation -- even if this moves
		;; because it is part of a table it should have the right coordinates
		;; in the presentation to figure out how to edit this value.
		(setf (query-values-value-presentation value-object) presentation)))))
     . ,body)) 



(defun default-standard-query-values-displayer (stream object query-identifier
						 type
						 original-type provide-default)
  (declare (ignore original-type))
  (standard-query-values-displayer (stream object query-identifier provide-default)
    (present-editable-choice object type)))


(defstruct query-values-choices
  query-identifier
  sequence
  select-action
  old-value
  stream)
  

(defstruct query-values-choice
  choices
  value
  presentation)

(defvar *Radio-Button-Circle-Radius* 6)
(defvar *Check-Box-Height* 12)
(proclaim '(fixnum *Radio-Button-Circle-Radius* *Check-Box-Height*))

(defun always-false (&rest args)
  (declare (ignore args))
  NIL)

(defun query-values-choose-from-sequence-display-item (item stream value type
						       printer
						       button-type key
						       highlighting-test
						       highlighting-function
						       single-box choices)
  (declare (ignore highlighting-function))	;4/30/90 (SLN)
  (declare (optimize (safety 3)(speed 0)))
  (let ((object (funcall key item)))
    ;; object is one element of the possible values
    ;; value is the current value or set of values.
    (multiple-value-bind (start-x start-y)
       (get-menu-item-position item)
       (when start-x
	     (set-cursorpos stream
			    (if *Query-X* (+ *Query-X* start-x) start-x)
			    (if *Query-Y* (+ *Query-Y* start-y) start-y))))
    (let ((choice-object (make-query-values-choice
			  :CHOICES choices
			  :VALUE (if *Running-Menu-Definer-P*
				     item
				   object))))
      (setf (query-values-choice-presentation choice-object)
	    (display-as (:STREAM stream :SINGLE-BOX single-box
				 :TYPE 'QUERY-VALUES-CHOICE
				 :OBJECT choice-object)
  	      (memoize (:STREAM stream
			:VALUE (list object (funcall highlighting-test object value))
			:VALUE-TEST (if *Running-Menu-Definer-P* #'always-false
				      #'EQUAL))
		       (funcall (get button-type :alist-item-choice-printer
				     'display-default-alist-item-choice)
				stream object value printer type
				highlighting-test)))))))

(defun query-values-choose-from-sequence (stream sequence value query-identifier
					   &KEY (type 'T) highlighted-type printer
					   (button-type :radio-button)
					   (key #'identity) (highlighting-test #'EQ)
					   highlighting-function
					   (select-action #'(lambda (new ignore)
							      new))
					   fill-p multiple-choices (single-box T))
;  (declare (ignore fill-p highlighting-function multiple-choices)
  (declare (ignore fill-p multiple-choices)
	   (optimize (safety 3)(speed 0) #+lucid (compilation-speed 3)))
  (unless highlighted-type (setq highlighted-type type))
  (let ((choices (make-query-values-choices :QUERY-IDENTIFIER query-identifier
					    :SEQUENCE sequence
					    :OLD-VALUE value
					    :SELECT-ACTION select-action
					    :STREAM *Querying-Values-Stream*)))
    (memoize (:STREAM stream :ID query-identifier)
      (when sequence
	(query-values-choose-from-sequence-display-item 
	 (first sequence) stream value type printer
	 button-type key highlighting-test highlighting-function single-box
	 choices)
	(dolist (item (cdr sequence))
	   (simple-princ " " stream)
	   (query-values-choose-from-sequence-display-item 
	    item stream value type printer
	    button-type key highlighting-test highlighting-function single-box
	    choices))))))



(setf (get :OVAL :alist-item-choice-printer)
      'display-oval-alist-item-choice)
(defun display-oval-alist-item-choice (stream object value printer type
				       highlighting-test)
  (with-border (stream :SHAPE :OVAL :MOVE-CURSOR NIL)
    (if (funcall highlighting-test object value)
	(with-character-face (:BOLD stream)
	  (if printer
	      (funcall printer object stream)
	      (display object type :STREAM stream)))
	(if printer
	    (funcall printer object stream)
	    (display object type :STREAM stream)))))


(setf (get :RADIO-BUTTON :alist-item-choice-printer)
      'display-radio-button-alist-item-choice)
(defun display-radio-button-alist-item-choice (stream object value printer type
					       highlighting-test)
  (multiple-value-bind (x y)
      (read-cursorpos stream)
    (let ((selected-state (funcall highlighting-test object value)))
      (memoize (:STREAM stream :VALUE selected-state)
	(draw-icon (if selected-state
		       selected-radio-button
		       non-selected-radio-button)
		   x y :STREAM stream)))
    (set-cursorpos stream (%+ x 2 (%* 2 *Radio-Button-Circle-Radius*))
		   y)
    (memoize (:STREAM stream :VALUE T)
      (if printer
	  (funcall printer object stream)
	  (display object type :STREAM stream)))))


(setf (get :CHECK-BOX :alist-item-choice-printer)
      'display-check-box-alist-item-choice)
(defun display-check-box-alist-item-choice (stream object value printer type
					    highlighting-test)
  (multiple-value-bind (x y)
      (read-cursorpos stream)
    (when (funcall highlighting-test object value)
      (draw-line (%+ 2 x) (%+ 2 y)
		 (%+ x -2 *Check-Box-Height*)
		 (%+ y -2 *Check-Box-Height*)
		 :STREAM stream)
      (draw-line (%+ 2 x) (%+ y -2 *Check-Box-Height*)
		 (%+ x -2 *Check-Box-Height*) (%+ 2 y)
		 :STREAM stream))
    (set-cursorpos stream
		   (%+ x 2 *Check-Box-Height*)
		   y)
    (memoize (:STREAM stream :VALUE T)
      (draw-rectangle (%1+ x) (%1+ y)
		      (%+ x 1 *Check-Box-Height*)
		      (%+ y 1 *Check-Box-Height*)
		      :stream stream :thickness 2 :filled NIL)
      (if printer
	  (funcall printer object stream)
	  (display object type :STREAM stream)))))


(defun display-default-alist-item-choice (stream object value printer type
					  highlighting-test)
  (if (funcall highlighting-test object value)
      (with-character-face (:BOLD stream)
	(if printer
	    (funcall printer object stream)
	    (display object type :STREAM stream)))
      (if printer
	  (funcall printer object stream)
	  (display object type :STREAM stream))))

(defun prompt-and-query (type-or-args &OPTIONAL format-string &REST format-args)
  (if (consp type-or-args)
      (apply #'query (getf type-or-args :type)
	     :prompt (apply #'format NIL format-string format-args)
	     type-or-args)
      (query type-or-args
	     :prompt (apply #'format NIL format-string format-args))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
