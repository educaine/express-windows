;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************



(cl:in-package 'ew)


#||
alist-member
and
boolean
character
character-face-or-style
character-style
class
command
directory-pathname
dynamic-window
expression
flavor-name
font
form
function-spec
host
instance
integer
inverted-boolean
keyword
member
member-sequence
network
no-type
not
null
null-or-type
number
or
out-of-band-character
package
pathname
printer
raw-text
satisfies
sequence
sequence-enumerated
string
subset
symbol
symbol-name
time-interval
token-or-type
type-or-string
user
wildcard-pathname
window

||#


(define-type character-family ()
  :PRINTER ((family stream) (simple-princ family stream))
  :PARSER ((stream)
	   (let ((families (xfonts::get-defined-character-families)))
	     (query `(member . ,families) :PROMPT NIL :stream stream
		     :default NIL :provide-default NIL))))

(define-type character-style ()
  :PRINTER ((style stream)
	    (display (first style) 'CHARACTER-FAMILY :STREAM stream)
	    (simple-princ "." stream)
	    (simple-princ (second style) stream)
	    (simple-princ "." stream)
	    (simple-princ (third style) stream))
  :PARSER ((stream)
	   (let (family face size)
	     (with-token-delimiters (#\. :override T)
	       (setq family (query 'character-family :stream stream :PROMPT NIL
				    :Default NIL :provide-default NIL))
	       (let ((delimiter (read-char-for-query stream)))
		 (unless (compare-char delimiter #\.)
		   (parse-error "Incorrect Delimiter ~A for Character Style."
				delimiter)))
	       ;; only query faces and sizes the are valid for family.
	       (setq face (query
			    `(member . ,(xfonts::get-defined-character-faces-for-family
					  family))
			    :stream stream :PROMPT NIL
			    :default NIL :provide-default NIL))
	       (let ((delimiter (read-char-for-query stream)))
		 (unless (compare-char delimiter #\.)
		   (parse-error "Incorrect Delimiter ~A for Character Style."
				delimiter))))
	     ;; now read a valid size.
	     (setq size (query
			  `(member . ,(xfonts::get-defined-character-sizes-for-family-face
					family face))
			  :stream stream :PROMPT NIL
			  :default NIL :provide-default NIL))
	     (list family face size))))



#+pcl
(define-type class (())
  :PRINTER ((class stream)
	    (simple-princ (and #+pcl-victoria (lisp:typep class 'pcl::iwmc-class)
			       #+pcl-may (pcl::standard-class-p class)	;5/1/90 (sln)
			       (make-pretty-name (pcl:class-name class))) stream))
  :PARSER ((stream &KEY type)
	   (let ((classes NIL))
	     (maphash #'(lambda (class-name class)
			  (push (list (make-pretty-name class-name) class)
				classes))
		      pcl::*Find-Class*)
	     (complete-input stream
			     #'(lambda (input-string operation)
				 (default-complete-function input-string operation
				   classes '(#\Space #\-)))
			     :TYPE type
			     :PARTIAL-COMPLETERS
			     *Standard-Completion-Delimiters*)))
  :DESCRIPTION "a class")


(define-type package (())
  :NO-DEFTYPE T
  :PARSER ((stream &KEY type)
	   (let ((packages (mapcan #'(lambda (package)
				       (let ((name (package-name package))
					     (nicknames (package-nicknames package)))
					 (cons (list name package)
					       (mapcar #'(lambda (nick)
							   (list nick package))
						       nicknames))))
				   (list-all-packages))))
	     (complete-input stream
			     #'(lambda (input-string operation)
				 (default-complete-function input-string operation
				   packages '(#\Space #\-)))
			     :TYPE type
			     :PARTIAL-COMPLETERS
			     *Standard-Completion-Delimiters*)))
  :DESCRIPTION "a package")

(define-type symbol (() &KEY (package *Package*))
  :NO-DEFTYPE T
  :PARSER ((stream)
	   (let ((symbol-name (read-standard-token stream)))
	     (let ((*Package* package))
	       (let ((symbol (read-from-string symbol-name NIL stream)))
		 (if (or (eq symbol stream) (not (symbolp symbol)))
		     (parse-error symbol-name 'SYMBOL)
		     symbol)))))
  :DESCRIPTION "a symbol")


(define-type string ((&OPTIONAL size min max) &KEY (case :unchanged) activation-chars)
  :NO-DEFTYPE T
  :PARSER ((stream)
	   (with-activation-chars (activation-chars)
	     (let ((string (read-standard-token stream)))
	       (declare (string string))
	       ;; verify size of string.
	       (when (and (integerp size) (not (= (length string) size)))
		 (parse-error size 'size-violation
			      "String ~S is not the correct size of ~D." string size))
	       (when (and min (not (< (if (integerp min) min (%1- (first min)))
				      (length string))))
		 (if (integerp min)
		     (parse-error size 'size-violation
				  "The Length of ~S is not greater than ~D."
				  string min)
		     (parse-error size 'size-violation
				  "The Length of ~S is not greater than or equal to ~D."
				  string min)))
	       (when (and max (not (> (if (integerp max) max (%1+ (first max)))
				      (length string))))
		 (if (integerp max)
		     (parse-error size 'size-violation
				  "The Length of ~S is not less than ~D."
				  string max)
		     (parse-error size 'size-violation
				  "The Length of ~S is not less than or equal to ~D."
				  string max)))
	       (case case
		 (:UNCHANGED string)
		 ((:lower-case :lowercase :lower :down :down-case :downcase)
		  (string-downcase string))
		 ((:upper-case :uppercase :upper :up :up-case :upcase)
		  (string-upcase string))
		 ((:capitalize :capitilize)
		  (string-capitalize string))
		 (T string)))))
  :DESCRIPTION "a string")

(define-type font (())
  :PRINTER ((font stream)
	    (simple-princ (and (xfonts::fontp font)
			       (make-pretty-name (xfonts::font-name font))) stream))
  :PARSER ((stream &KEY type)
	   (let ((fonts NIL))
	     (do-symbols (font-name (find-package "XFONTS"))
	       (when (and (boundp font-name) (xfonts::fontp (symbol-value font-name)))
		 (push (list (make-pretty-name font-name NIL) (symbol-value font-name))
		       fonts)))
	     (complete-input stream
			     #'(lambda (input-string operation)
				 (default-complete-function input-string operation
				   fonts '(#\Space #\-)))
			     :TYPE type
			     :PARTIAL-COMPLETERS
			     *Standard-Completion-Delimiters*)))
  :DESCRIPTION "a font")

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
