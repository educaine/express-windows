;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */



(cl:in-package 'ew)

#+meter
(defvar *Meter-Time* 0)

#+meter
(defvar *Meter-Symbols* NIL)

;(push :meter *Features*)

(defmacro meter (symbol &BODY body)
  #+meter
  (let ((start-time (gensym "Start-Time"))
	(end-time (gensym "End-Time"))
	(values (gensym "VALUES")))
  (pushnew symbol *Meter-Symbols*)
  `(let ((,Start-time (get-internal-run-time))
	 (,values NIL)
	 (,end-time NIL))
     (let ((*meter-time* ,start-time))
       (setq ,values (multiple-value-list (progn . ,body)))
       (setq ,end-time (get-internal-run-time))
       (setf (get ',symbol :time)
	     (+ (- ,end-time *Meter-Time*) (or (get ',symbol :time) 0))))
     (incf *Meter-Time* (- ,end-time ,start-time))
     (values-list ,values)))
  #-meter
  (declare (ignore symbol))
  #-meter
  `(progn . ,body))

#+meter
(defun show-meter-results ()
  (dolist (symbol *Meter-Symbols*)
    (format T "~%~A    ~D" symbol (get symbol :time))))

#+meter
(defun clear-meter ()
  (setf *meter-time* 0)
  (dolist (symbol *Meter-Symbols*)
    (setf (get symbol :time) 0)))

#+ignore
(defun test-meter-1 (lower upper)
  (meter test-1
	 (test-meter-2 lower)
  (dotimes (y upper)
    (foo y))))

#+ignore
(defun test-meter-2 (x)
  (meter test-2
  (dotimes (y x)
    (foo y))))

#+ignore
(defun foo (s) (sqrt s))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
