;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)


(defvar *Transform-Coordinates* T)


(defvar *Input-Context* NIL)


(defun offset-presentation-tree (presentation x-offset y-offset)
  (declare (fixnum x-offset y-offset))
  (when (presentation-p presentation)
    (dolist (p (presentation-records presentation))
      (offset-presentation-tree p x-offset y-offset)))
  (offset-presentation presentation x-offset y-offset))

(defun offset-presentation (presentation x-offset y-offset)
  (declare (fixnum x-offset y-offset))
  (incf (boxed-presentation-left presentation) x-offset)
  (incf (boxed-presentation-top presentation) y-offset)
  (incf (boxed-presentation-right presentation) x-offset)
  (incf (boxed-presentation-bottom presentation) y-offset)
  (unless (presentation-p presentation)
    (typecase presentation ;; important that this use typecase to get inheritance automatically.
      ;; put this here since we expect there to be a lot of string presentations
      ;; and we don't want to waste time checking to see if this is an ellipse.
      (string-presentation NIL)
      (triangle-presentation
	(incf (triangle-presentation-x1 presentation) x-offset)
	(incf (triangle-presentation-y1 presentation) y-offset)
	(incf (triangle-presentation-x2 presentation) x-offset)
	(incf (triangle-presentation-y2 presentation) y-offset)
	(incf (triangle-presentation-x3 presentation) x-offset)
	(incf (triangle-presentation-y3 presentation) y-offset))
      (polygon-presentation
	(let ((points (copy-list (polygon-presentation-points presentation))))
	  (setf (polygon-presentation-points presentation) points)
	  (do ((p points (cddr p)))
	      ((null p))
	    (incf (first p) x-offset)
	    (incf (second p) y-offset))))
      (arrow-presentation
	(incf (arrow-presentation-x1 presentation) x-offset)
	(incf (arrow-presentation-y1 presentation) y-offset)
	(incf (arrow-presentation-x2 presentation) x-offset)
	(incf (arrow-presentation-y2 presentation) y-offset))
      (line-presentation
	(incf (line-presentation-start-x presentation) x-offset)
	(incf (line-presentation-start-y presentation) y-offset)
	(incf (line-presentation-end-x presentation) x-offset)
	(incf (line-presentation-end-y presentation) y-offset))
      (circle-presentation
	(incf (circle-presentation-center-x presentation) x-offset)
	(incf (circle-presentation-center-y presentation) y-offset))
      (ellipse-presentation
	(incf (ellipse-presentation-center-x presentation) x-offset)
	(incf (ellipse-presentation-center-y presentation) y-offset)))))

(defun scale-presentation (presentation x-scale y-scale)
  (declare (optimize (safety 0) (speed 3)))
  (if (> x-scale 0)
      (setf (boxed-presentation-left presentation)
	    (* (boxed-presentation-left presentation) x-scale)
	    (boxed-presentation-right presentation)
	    (* (boxed-presentation-right presentation) x-scale))
      (psetf (boxed-presentation-right presentation)
	     (* (boxed-presentation-left presentation) x-scale)
	     (boxed-presentation-left presentation)
	     (* (boxed-presentation-right presentation) x-scale)))
  (if (> y-scale 0)
      (setf (boxed-presentation-top presentation)
	    (* (boxed-presentation-top presentation) y-scale)
	    (boxed-presentation-bottom presentation)
	    (* (boxed-presentation-bottom presentation) y-scale))
      (psetf (boxed-presentation-bottom presentation)
	     (* (boxed-presentation-top presentation) y-scale)
	     (boxed-presentation-top presentation)
	     (* (boxed-presentation-bottom presentation) y-scale)))
  (typecase presentation
    (triangle-presentation
      (setf (triangle-presentation-x1 presentation)
	    (* (triangle-presentation-x1 presentation) x-scale))
      (setf (triangle-presentation-y1 presentation)
	    (* (triangle-presentation-y1 presentation) y-scale))
      (setf (triangle-presentation-x2 presentation)
	    (* (triangle-presentation-x2 presentation) x-scale))
      (setf (triangle-presentation-y2 presentation)
	    (* (triangle-presentation-y2 presentation) y-scale))
      (setf (triangle-presentation-x3 presentation)
	    (* (triangle-presentation-x3 presentation) x-scale))
      (setf (triangle-presentation-y3 presentation)
	    (* (triangle-presentation-y3 presentation) y-scale)))
    (polygon-presentation
      (let ((points (copy-list (polygon-presentation-points presentation))))
	(setf (polygon-presentation-points presentation) points)
	(do ((p points (cddr p)))
	    ((null p))
	  (setf (first p) (* (first p) x-scale)
		(second p) (* (second p) y-scale)))))
    (arrow-presentation
      (setf (arrow-presentation-x1 presentation)
	    (* (arrow-presentation-x1 presentation) x-scale))
      (setf (arrow-presentation-y1 presentation)
	    (* (arrow-presentation-y1 presentation) y-scale))
      (setf (arrow-presentation-x2 presentation)
	    (* (arrow-presentation-x2 presentation) x-scale))
      (setf (arrow-presentation-y2 presentation)
	    (* (arrow-presentation-y2 presentation) y-scale)))
    (line-presentation
      (setf (line-presentation-start-x presentation)
	    (* (line-presentation-start-x presentation) x-scale))
      (setf (line-presentation-start-y presentation)
	    (* (line-presentation-start-y presentation) y-scale))
      (setf (line-presentation-end-x presentation)
	    (* (line-presentation-end-x presentation) x-scale))
      (setf (line-presentation-end-y presentation)
	    (* (line-presentation-end-y presentation) y-scale)))
    (circle-presentation
      (setf (circle-presentation-center-x presentation)
	    (* (circle-presentation-center-x presentation) x-scale))
      (setf (circle-presentation-center-y presentation)
	    (* (circle-presentation-center-y presentation) y-scale)))))  

(defun insert-presentations-with-offset (window presentations x-offset y-offset
					 &OPTIONAL (redraw-p NIL) (recursive-p NIL))
  (dolist (presentation presentations)
    (when (presentation-p presentation)
      (insert-presentations-with-offset window (presentation-records presentation)
					x-offset y-offset redraw-p T))
    (offset-presentation presentation x-offset y-offset))
  (unless recursive-p
    (dolist (p presentations)
      (record-presentation-top-level window p)))
  (unless (fake-window-p window)
    (when redraw-p
      (dolist (p presentations)
	(redraw-presentation p window NIL NIL NIL NIL)))))

(defun insert-presentations-for-room-for-graphics (window presentations height
						   &OPTIONAL (recursive-p NIL) (y-offset NIL))
  (unless y-offset (setq y-offset (%+ height (window-scroll-y-offset window))))
  (dolist (presentation presentations)
    (when (presentation-p presentation)
      (insert-presentations-for-room-for-graphics window (presentation-records presentation)
						  height T y-offset))
    (scale-presentation presentation 1 -1)
    (offset-presentation presentation 0 y-offset))
  (unless recursive-p
    (dolist (p presentations)
      (record-presentation-top-level window p)))
  (unless (fake-window-p window)
    (dolist (p presentations)
      (redraw-presentation p window NIL NIL NIL NIL))))

#+obsolete
(defun insert-presentations (window presentations &OPTIONAL (redraw-p NIL) (recursive-p NIL))
  (dolist (presentation presentations)
    (when (presentation-p presentation)
      (insert-presentations window (presentation-records presentation) redraw-p T)))
  (unless recursive-p
    (dolist (p presentations)
      (record-presentation-top-level window p)))
  (unless (fake-window-p window)
    (when redraw-p
      (dolist (p presentations)
	(unless (presentation-p p)
	  (if *Defer-Redraw-Presentations*
	      (progn
		#+debugging-memo
		(dformat "~&Deferring IPI ~D" p)
		(push p *Deferred-Redraw-Presentations*))
	      (redraw-presentation p window NIL NIL NIL NIL NIL)))))))


(defun insert-presentations (window presentations &OPTIONAL (redraw-p NIL))
  (dolist (presentation presentations)
    (when (presentation-p presentation)
      (unless (eq 'memo (presentation-type presentation))
	(insert-presentations-internal window (presentation-records presentation) redraw-p))))
  (dolist (p presentations)
    (record-presentation-top-level window p))
  (unless (fake-window-p window)
    (when redraw-p
      (dolist (p presentations)
	(unless (presentation-p p)
	  (if *Defer-Redraw-Presentations*
	      (progn
		#+debugging-memo
		(dformat "~&Deferring IPI ~D" p)
		(push p *Deferred-Redraw-Presentations*))
	      (redraw-presentation p window NIL NIL NIL NIL NIL)))))))

(defun insert-presentations-internal (window presentations &OPTIONAL (redraw-p NIL))
  (dolist (presentation presentations)
    (when (presentation-p presentation)
      (unless (eq 'memo (presentation-type presentation))
	(insert-presentations-internal window (presentation-records presentation) redraw-p)))
    (unless (fake-window-p window)
      (when redraw-p
	(dolist (p presentations)
	  (unless (presentation-p p)
	    (if *Defer-Redraw-Presentations*
		(progn
		  #+debugging-memo
		  (dformat "~&Deferring IPI ~D" p)
		  (push p *Deferred-Redraw-Presentations*))
		(redraw-presentation p window NIL NIL NIL NIL NIL))))))))

(defun insert-memo-presentation (window memo-presentation)
  #.(fast)
  (let ((*Defer-Redraw-Presentations* NIL))
    (record-presentation-top-level window memo-presentation))
  ;; also make all drawable subordinate presentations go on deferred list.
;  (when (or *Redisplaying-Memo-P* (not (fake-window-p window)))
    (defer-memo-presentations (presentation-records memo-presentation)));)

(defun defer-memo-presentations (presentations)
  #.(fast)
  (dolist (p presentations)
    (if (presentation-p p)
	(if (eq 'MEMO (presentation-type p))
	    NIL
	    (defer-memo-presentations (presentation-records p)))
	(progn
	  #+debugging-memo
	  (dformat "~&Deferring drp ~D" p)
	  (push p *Deferred-Redraw-Presentations*)))))

(defun redraw-deferred-presentations (window)
  #.(fast)
  (unless (fake-window-p window)
    (dolist (p *Deferred-Redraw-Presentations*)
      (redraw-presentation p window NIL NIL NIL NIL))))

(defvar *Erase-p* NIL)

(defun erase-displayed-presentation (presentation window
				     &OPTIONAL recursive as-single-box clear-inferiors)
  (declare (ignore as-single-box clear-inferiors))
  (remove-presentation window presentation recursive)

  ;;  7/25/90 - I added the following form in order to force the erased presentation to
  ;; disappear from the screen - LSB

  (redraw-window window (presentation-left presentation) (presentation-top presentation)
		 (presentation-right presentation) (presentation-bottom presentation))
  )

(defun delete-presentations (window presentations &AUX (*Erase-p* T))
  (dolist (p presentations)
    (redraw-presentation p window NIL NIL NIL NIL)
    (setf (presentation-window-presentations window)
	  (delete p (presentation-window-presentations window) :count 1))
    (delete-object-from-quad-table window p)))

(defun delete-presentation (window presentation remove-from-history-p &AUX (*Erase-p* T))
  (redraw-presentation presentation window NIL NIL NIL NIL NIL)
  (when remove-from-history-p
    (setf (presentation-window-presentations window)
	  (delete presentation (presentation-window-presentations window) :COUNT 1))
    (delete-object-from-quad-table window presentation)))


(defun remove-presentation (window presentation recursive-erase-p &AUX (*Erase-P* T))
  (redraw-presentation presentation window NIL NIL NIL NIL recursive-erase-p)
  (let ((superior (presentation-superior presentation)))
    (if (presentation-p superior)
	(setf (presentation-records superior)
	      (delete presentation (presentation-records superior)))
	(delete-object-from-quad-table window presentation))))


;;; ****************************************************************************************
;;; PRESENTATION STUFF
;;; ****************************************************************************************


(defun get-presentations-boundaries (presentations)
  #.(fast)
  (declare (values left top right bottom))
  (if (null presentations) 
      (values 0 0 0 0)
      (let ((first-presentation (first presentations)))
	(let ((x1 (boxed-presentation-left first-presentation))
	      (y1 (boxed-presentation-top first-presentation))
	      (x2 (boxed-presentation-right first-presentation))
	      (y2 (boxed-presentation-bottom first-presentation)))
	  (declare (fixnum x1 y1 x2 y2))
	  (dolist (record (cdr presentations))
	    (minimize x1 (boxed-presentation-left record))
	    (minimize y1 (boxed-presentation-top record))
	    (maximize x2 (boxed-presentation-right record))
	    (maximize y2 (boxed-presentation-bottom record)))
	  (values x1 y1 x2 y2)))))

(defun calculate-presentation-boundaries-of-tree (presentation)
  (when (presentation-p presentation)
    (mapc #'calculate-presentation-boundaries-of-tree (presentation-records presentation))
    (calculate-presentation-boundaries presentation)))

(defun calculate-presentation-boundaries (presentation)
  #.(fast)
  (let ((records (presentation-records presentation)))
    (if records
	(let ((first-presentation (first records)))
	  (let ((x1 (boxed-presentation-left first-presentation))
		(y1 (boxed-presentation-top first-presentation))
		(x2 (boxed-presentation-right first-presentation))
		(y2 (boxed-presentation-bottom first-presentation)))
	    (declare (fixnum x1 y1 x2 y2))
	    (dolist (record (cdr records))
	      (minimize x1 (boxed-presentation-left record))
	      (minimize y1 (boxed-presentation-top record))
	      (maximize x2 (boxed-presentation-right record))
	      (maximize y2 (boxed-presentation-bottom record)))
	    (setf (boxed-presentation-left presentation) x1
		  (boxed-presentation-top presentation) y1
		  (boxed-presentation-right presentation) x2
		  (boxed-presentation-bottom presentation) y2)))
	(setf (boxed-presentation-left presentation) 0
	      (boxed-presentation-top presentation) 0
	      (boxed-presentation-right presentation) 0
	      (boxed-presentation-bottom presentation) 0))))

(defvar *Default-Highlighting-Filled-Rectangle-P* T)

(defun highlight-presentation (presentation stream &AUX (*Record-Presentations-P* NIL))
  (unless (eq presentation *No-Type*)
    (if (presentation-p presentation)
	(with-type-decoded (presentation-name) (presentation-type presentation)
	  (cond ((get presentation-name :TYPE-HIGHLIGHTING-BOX-FUNCTION)
		 (with-presentations-disabled
		   (funcall (get presentation-name :TYPE-HIGHLIGHTING-BOX-FUNCTION)
			    stream (presentation-type presentation)
			    (presentation-type presentation)
			    presentation
			    (presentation-left presentation)
			    (presentation-top presentation))))
		((presentation-single-box presentation)
		 (fast-draw-rectangle (%1- (boxed-presentation-left presentation))
				      (%1- (boxed-presentation-top presentation))
				      (%1+ (boxed-presentation-right presentation))
				      (boxed-presentation-bottom presentation)
				      stream
				      *Default-Highlighting-Filled-Rectangle-P*
				      *Flip-Alu*))
		(T (dolist (record (presentation-records presentation))
		     (highlight-presentation record stream)))))
	(fast-draw-rectangle
	  (%1- (boxed-presentation-left presentation))
	  (%1- (boxed-presentation-top presentation))
	  (%1+ (boxed-presentation-right presentation))
	  (%1+ (boxed-presentation-bottom presentation))
	  stream *Default-Highlighting-Filled-Rectangle-P* *Flip-Alu*))))



(defun underline-presentation (stream line-height p start-x start-y end-x end-y)
  (declare (fixnum line-height start-x start-y end-x end-y))
  (flet ((underline-entire-presentation ()
	   (underline-presentation-internal p stream)
	   NIL)
	 (underline-presentation-from-left (right y)
	   (let ((index (find-text-index-for-position
			  (string-presentation-font p)
			  (string-presentation-string p)
			  (presentation-left p)
			  (presentation-top p)
			  right y line-height)))
	     (underline-presentation-internal p stream 0 index))
	   p)
	 (underline-presentation-from-right (left y)
	   (let ((index (find-text-index-for-position
			  (string-presentation-font p)
			  (string-presentation-string p)
			  (presentation-left p)
			  (presentation-top p)
			  left y line-height)))
	     (underline-presentation-internal p stream index))
	   p)
	 (underline-part-of-presentation (left right y)
	   (let ((index1 (find-text-index-for-position
			   (string-presentation-font p)
			   (string-presentation-string p)
			   (presentation-left p)
			   (presentation-top p)
			   left y line-height))
		 (index2 (find-text-index-for-position
			   (string-presentation-font p)
			   (string-presentation-string p)
			   (presentation-left p)
			   (presentation-top p)
			   right y line-height)))
	     (underline-presentation-internal p stream index1 index2)
	     p)))
  (if (<= (presentation-top p) start-y (presentation-bottom p))
      (if (<= (presentation-top p) end-y (presentation-bottom p))
	  ;; Case I - both y coordinates are inside presentation level.
	  ;; we need to recheck to make sure we have left and right
	  (progn
	    (when (> start-x end-x) (psetq start-x end-x end-x start-x))
	    ;; now check coordinates to see how much of presentation is covered.
	    (if (<= (presentation-left p) start-x (presentation-right p))
		(if (<= (presentation-left p) end-x (presentation-right p))
		    ;; CASE I A - both x coordinates are within presentation
		    ;; so we have to indices to worry about.
		    (underline-part-of-presentation start-x end-x start-y)
		    ;; CASE I B left side is inside presentation only.
		    (underline-presentation-from-right start-x start-y))
		(if (<= (presentation-left p) end-x (presentation-right p))
		    ;; CASE I C - right side is inside presentation only.
		    (underline-presentation-from-left end-x end-y)
		    ;; CASE I D - neither side is inside presentation.
		    (progn
		      ;; check for possible errors
		      (unless (and (< start-x (presentation-left p))
				   (> end-x (presentation-right p)))
			(error "Shouldn't Be Underlining this presentation ~D." p))
		      (underline-entire-presentation)))))
	  ;; CASE II start coordinates are inside presentation only.
	  ;; need only compare to start for position.
	  (cond ((<= start-x (presentation-left p))
		 ;; CASE II A entire presentation to be underlined.
		 (underline-entire-presentation))
		((<= start-x (presentation-right p))
		 ;; CASE II B Start underlining at position of start-x
		 (underline-presentation-from-right start-x start-y))
		(T (error "Shouldn't be Underlining this presentation ~D." p))))
      (if (<= (presentation-top p) end-y (presentation-bottom p))
	  ;; CASE III end coordinates are inside presentation only.
	  (cond ((<= (presentation-right p) end-x)
		 ;; CASE III A entire presentation to be underlined.
		 (underline-entire-presentation))
		((<= (presentation-left p) start-x)
		 ;; CASE II B End underlining at position of end-x
		 (underline-presentation-from-left end-x end-y))
		(T (error "Shouldn't be Underlining this presentation ~D." p)))
	  ;; CASE IV. all presentation should be underlined.
	  (underline-entire-presentation)))))

(defun underline-presentation-internal (presentation stream &OPTIONAL (start 0) end)
  (with-presentations-disabled
    (cond ((and (zerop start) (not end))
	   (draw-line
	     (boxed-presentation-left presentation)
	     (boxed-presentation-bottom presentation)
	     (boxed-presentation-right presentation)
	     (boxed-presentation-bottom presentation)
	     :FILLED NIL :STREAM stream :ALU *Flip-Alu*))
	  ;; hard case, figure out coordinate.
	  ((zerop start)
	   ;; draw a line from start to somewhere.
	   (multiple-value-bind (x)
	       (compute-cursor-position-in-string
		 (string-presentation-font presentation)
		 (string-presentation-string presentation)
		 end
		 (presentation-left presentation)
		 (presentation-top presentation)
		 (window-line-height stream)
		 most-positive-fixnum)
	     (draw-line
	       (boxed-presentation-left presentation)
	       (boxed-presentation-bottom presentation)
	       x (boxed-presentation-bottom presentation)
	       :FILLED NIL :STREAM stream :ALU *Flip-Alu*)))
	  ((and (not (zerop start)) end)
	   ;; draw a line from start to somewhere.
	   (multiple-value-bind (x1)
	       (compute-cursor-position-in-string
		 (string-presentation-font presentation)
		 (string-presentation-string presentation)
		 start
		 (presentation-left presentation)
		 (presentation-top presentation)
		 (window-line-height stream)
		 most-positive-fixnum)
	     (multiple-value-bind (x2)
		 (compute-cursor-position-in-string
		   (string-presentation-font presentation)
		   (string-presentation-string presentation)
		   end
		   (presentation-left presentation)
		   (presentation-top presentation)
		   (window-line-height stream)
		   most-positive-fixnum)
	       (draw-line
		 x1 (boxed-presentation-bottom presentation)
		 x2
		 (boxed-presentation-bottom presentation)
		 :FILLED NIL :STREAM stream :ALU *Flip-Alu*))))
	  ((not (zerop start))
	   ;; draw a line from start to somewhere.
	   (multiple-value-bind (x)
	       (compute-cursor-position-in-string
		 (string-presentation-font presentation)
		 (string-presentation-string presentation)
		 start
		 (presentation-left presentation)
		 (presentation-top presentation)
		 (window-line-height stream)
		 most-positive-fixnum)
	     (draw-line
	       x (boxed-presentation-bottom presentation)
	       (boxed-presentation-right presentation)
	       (boxed-presentation-bottom presentation)
	       :FILLED NIL :STREAM stream :ALU *Flip-Alu*)))
	  (T (error "Shouldn't Get here.")))))

(defmethod redraw-window ((window window) &OPTIONAL left top right bottom)
  (let ((*Record-Presentations-P* NIL)
	(*Transform-Coordinates* NIL))
    (when *Highlighted-Presentation*
      (highlight-presentation *Highlighted-Presentation*
			      *Highlighted-Presentation-Window*))
    (prepare-window (window)
      (multiple-value-bind (x y)
	  (read-cursorpos window)
	(if (or left top right bottom)
	    (clear-window-internal window
				   (%+ (window-inside-left window)
				       (%- left (window-scroll-x-offset window)))
				   (%+ (window-inside-top window)
				       (%- top (window-scroll-y-offset window)))
				   (%- right left) (%- bottom top) NIL)
	    (clear-window-internal window 0 0 NIL NIL NIL))
	(multiple-value-bind (width height)
	    (window-inside-size window)
	  (unless left (setq left (window-scroll-x-offset window)))
	  (unless top (setq top (window-scroll-y-offset window)))
	  (unless right (setq right (%+ left width)))
	  (unless bottom (setq bottom (%+ top height)))
	  (let* ((bottom-quad-number (the (values fixnum number)
					  (floor bottom *Quad-Height*)))
		 (quad-table (presentation-window-quad-table window))
		 node
		 (start-index (the (values fixnum number)
				   (floor top *Quad-Height*))))
	    (declare (fixnum bottom-quad-number))
	    (do* ((index start-index (%1+ index))
		  (start-y-pos (%* index *Quad-Height*) (%+ start-y-pos *Quad-Height*)))
		 ((%> index bottom-quad-number))
	      (declare (fixnum index))
	      (when (setq node (get-quad-node quad-table index))
		(dolist (presentation (quad-node-objects node))
		  ;; we need to make sure we only redisplay the object once.
		  ;; only display it when its top is within this box, that should do it.
		  (when (or (%>= (presentation-top presentation) start-y-pos)
			    ;; if the object stretches into this region and are not
			    ;; redisplaying its region I guess we should display it here as
			    ;; well
			    (%= start-index index))
		    ;; we have open coded the function REDRAW-PRESENTATION
		    ;; (redraw-presentation presentation window left top right bottom)
		    (when (not (or (%> (boxed-presentation-left presentation) right)
				   (%> (boxed-presentation-top presentation) bottom)
				   (%< (boxed-presentation-right presentation) left)
				   (%< (boxed-presentation-bottom presentation) top)))
		      (let ((type (structure-type presentation)))
			(case type
			  (PRESENTATION
			    (dolist (record (presentation-records presentation))
			      ;; figure out if we can go fast yet and avoid the checking.
			      (if (and (%>= (the fixnum right)
					    (boxed-presentation-left record))
				       (%>= (the fixnum bottom)
					    (boxed-presentation-top record))
				       (%<= (the fixnum left)
					    (boxed-presentation-right record))
				       (%<= (the fixnum top)
					    (boxed-presentation-bottom record)))
				  (fast-redraw-presentation record window)
				  (medium-fast-redraw-presentation
				    record window left top right bottom))))
			  #+ignore(TEXT-PRESENTATION
			    (redraw-text-presentation presentation window))
			  (T (funcall type; (get type :PRESENTATION-REDRAW-FUNCTION)
				      presentation window)))))))))))
	(set-cursorpos window x y)))
    (when *Highlighted-Presentation*
      (highlight-presentation *Highlighted-Presentation*
			      *Highlighted-Presentation-Window*))
    (or *Inhibit-Scroll-Bar-P*
	(draw-margins window))))




#+ignore
(defmacro display-as ((&KEY object type
			    (stream '*Standard-Output*) (single-box NIL)
			    (allow-sensitive-inferiors T))
		      &BODY body)
  (let ((records (gensym))
	(recording-string-p (gensym "RECORDING-STRING-P"))
	(object-value (gensym "OBJECT-VALUE")))
    `(let ((,object-value ,object)
	   ,records
	   (,recording-string-p
	    (and (lisp:typep ,stream 'presentation-recording-string)
		 (fill-pointer (presentation-recording-string-string ,stream)))))
       (let ((*Record-Inferior-Presentations-P*
	       (and ,allow-sensitive-inferiors *Record-Inferior-Presentations-P*))
	     (*Immediately-Inside-Woap* T)
	     (*Presentation-Records* NIL)
	     (*Presentation-Records-Last-Cons* NIL)
	     (*Inside-Record-Presentations-P* T))
	 (progn ,@body)
	 (setq ,records *Presentation-Records*))
       (when (or ,records ,recording-string-p)
	 (record-presentation ,stream ,records ,object-value
			      ,type ,single-box ,recording-string-p)))))

(defmacro display-as ((&KEY object type
			    (stream '*Standard-Output*) (single-box NIL)
			    (allow-sensitive-inferiors T))
		      &BODY body)
  `(display-as-internal ,object ,type
			,stream ,single-box
			,allow-sensitive-inferiors
			#'(lambda (,stream) (progn . ,body))))



(defun display-as-internal (object type stream single-box allow-sensitive-inferiors body)
  (let ((records NIL)
	(recording-string-p
	  (and (presentation-recording-string-p stream)
	       (fill-pointer (presentation-recording-string-string stream)))))
    (let ((*Record-Inferior-Presentations-P*
	    (and allow-sensitive-inferiors *Record-Inferior-Presentations-P*))
	  (*Immediately-Inside-Woap* T)
	  (*Presentation-Records* NIL)
	  (*Presentation-Records-Last-Cons* NIL)
	  (*Inside-Record-Presentations-P* T))
      (funcall body stream)
      (setq records *Presentation-Records*))
    (when (or records recording-string-p)
      (record-presentation stream records object
			   type single-box recording-string-p))))

(defun make-top-level-presentation (stream records object type)
  (when records
    (record-presentation stream records object type T NIL)))

;;; Called from graphics commands to record the presentation they just created.
;; This will probably have to check whether it is within a redisplaying memo
;; or not.
;;; This has four cases.
;;; CASE I. - We are at top level (not inside a display-as or within
;;;		a memo.  Therefore Record it directly on the stream.
;;; CASE II. - Directly inside a display-as (even though it may be indirectly
;;;		inside a memo.  Push the presentation on the special variable
;;;		*Presentation-Records*.		
;;; CASE III. - Inside a memo running the first time.  This should be just like CASE I.
;;; CASE IV.  - Directly inside a redisplaying memo.  This needs to push the
;;;		presentation somewhere, but I haven't figured out where yet.



(defun record-presentation-top-level (stream presentation)
  (when (or (and *Defer-Redraw-Presentations* (not (fake-window-p stream)))
	    *Redisplaying-Memo-P*)
    #+debugging-memo (dformat  "~&Deferring rptl ~D" presentation)
    (unless (presentation-p presentation)
      (push presentation *Deferred-Redraw-Presentations*)))
  (cond (*Immediately-Inside-Woap*
	 #+debugging-memo (dformat "~%Case II")
	 ;; CASE II.
	 (add-to-end-of-list presentation
			     *Presentation-Records* *Presentation-Records-Last-Cons*))
	(*Redisplaying-Memo-P*
	 #+debugging-memo (dformat "~%Case IV")
	 ;; CASE IV.
	 (setf (presentation-superior presentation) *Memo-Presentation*)
	 ;; this is a hairy case since we are trying to be efficient about remembering
	 ;; the previous presentation list conses.
	 #+debugging-memo
	 (dformat "~%M-P-Records ~D" *Memo-Presentation-Records*)
	 #+debugging-memo
	 (dformat "~%M-P-Records-Previous-Cons ~D" *Memo-Presentation-Records-Previous-Cons*)
	 #+debugging-memo
	 (dformat "~%Memo Presentation ~D Records ~D."
		 *Memo-Presentation*
		 (presentation-records *Memo-Presentation*))
	 (if (eq *Memo-Presentation-Records*
		 *Memo-Presentation-Records-Previous-Cons*)
	     ;; we are at the beginning of the list of presentations for this memo
	     ;; so we need to push this on the list inside the presentation.
	     (let ((records (presentation-records *Memo-Presentation*)))
	       #+debugging-memo (dformat "~%Case IV a.")
	       (push presentation records)
	       (setf (presentation-records *Memo-Presentation*) records)
	       (setq *Memo-Presentation-Records-Previous-Cons* records
		     *Memo-Presentation-Records* (cdr records)))
	     ;; we are not at beginning of records
	     (let ((records (cons presentation *Memo-Presentation-Records*)))
	       #+debugging-memo (dformat "~%Case IV b.")
	       (setf (cdr *Memo-Presentation-Records-Previous-Cons*)
		     records)
	       (setf *Memo-Presentation-Records-Previous-Cons*
		     (cdr *Memo-Presentation-Records-Previous-Cons*))))
	 #+debugging-memo
	 (dformat "~&Memo Presentations Previous Cons ~D."
		  *Memo-Presentation-Records-Previous-Cons*)
	 )
	(*Inferior-Memo-P*
	 #+debugging-memo (dformat "Case III")
	 ;; CASE III.
	 (add-to-end-of-list presentation
			     *Presentation-Records* *Presentation-Records-Last-Cons*))
	(T ;; CASE I.
	 #+debugging-memo (dformat "Case I")
	 (if *Inside-Record-Presentations-P*
	     (add-to-end-of-list presentation
				 *Presentation-Records* *Presentation-Records-Last-Cons*)
	     (progn
	       (setf (presentation-superior presentation) stream)
	       (if (fake-window-p stream) ;; check to make sure type-of returns
		   (push presentation (presentation-window-presentations stream))
		   (insert-object-into-quad-table stream presentation)))))))

(defun record-presentation (stream records object type single-box recording-string-p)
  (cond (recording-string-p
	 #+debugging-memo (dformat "Case I")
	 (let ((current-length (fill-pointer (presentation-recording-string-string stream))))
	   (unless (%= current-length recording-string-p)
	     ;; record presentation inside recording string
	     (setf (presentation-recording-string-presentations stream)
		   (nconc (presentation-recording-string-presentations stream)
			  (list (make-recording-string-presentation
				  :START-INDEX recording-string-p
				  :END-INDEX current-length
				  :OBJECT object :TYPE type)))))))
	#+ignore
	((not *Record-Inferior-Presentations-P*)
	 #+debugging-memo (dformat "Case II")
	 ;; make these records go one level up.
	 (if *Inside-Record-Presentations-P*
	     (add-items-to-end-of-list records
				       *Presentation-Records* *Presentation-Records-Last-Cons*)
	     (unless (fake-window-p stream)
	       (dolist (p records)
		 (insert-object-into-quad-table stream p)))))
	(T
	 #+debugging-memo (dformat "Case III")
	 ;; record them here with new surrounding presentation
	 (let ((presentation (make-presentation :TYPE type #+ignore
						(if *Record-Inferior-Presentations-P*
							  type
							  'ignore-presentation)
						:SINGLE-BOX single-box
						:RECORDS records
						:OBJECT object)))
	   ;; open coding of calculate-presentation-boundaries for minor efficiencies
	   ;; saves function call, slot-access, and extra cdring down list of records
	   ;; for superior pointers
	   (if records
	       (let ((first-presentation (first records)))
		 (let ((x1 (boxed-presentation-left first-presentation))
		       (y1 (boxed-presentation-top first-presentation))
		       (x2 (boxed-presentation-right first-presentation))
		       (y2 (boxed-presentation-bottom first-presentation)))
		   (declare (fixnum x1 y1 x2 y2))
		   (setf (superior first-presentation) presentation)
		   (dolist (record (cdr records))
		     (minimize x1 (boxed-presentation-left record))
		     (minimize y1 (boxed-presentation-top record))
		     (maximize x2 (boxed-presentation-right record))
		     (maximize y2 (boxed-presentation-bottom record))
		     (setf (superior record) presentation))
		   (setf (boxed-presentation-left presentation) x1
			 (boxed-presentation-top presentation) y1
			 (boxed-presentation-right presentation) x2
			 (boxed-presentation-bottom presentation) y2)))
	       (setf (boxed-presentation-left presentation) 0
		     (boxed-presentation-top presentation) 0
		     (boxed-presentation-right presentation) 0
		     (boxed-presentation-bottom presentation) 0))
	   (record-presentation-top-level stream presentation)
	   presentation))))






;; tried optimizing in LUCID by getting rid of LET's but it didn't seem to help any.
;; so I left it this way.  I did get rid of the ABS though which means it is possible to
;; get a negative distance if some how a presentation get's its left and right mixed up.
;; Hopefully not - That is a bug anyway.  Also makes the compiled function A lot smaller.

(defmacro distance-from-presentation (presentation x y)
  `(let ((x1 (boxed-presentation-left ,presentation))
	 (y1 (boxed-presentation-top ,presentation)))
     (declare (fixnum x1 y1))
     (or (and (<= x1 ,x)
	      (<= y1 ,y)
	      (let ((x2 (boxed-presentation-right ,presentation))
		    (y2 (boxed-presentation-bottom ,presentation)))
		(declare (fixnum x2 y2))
		(and (<= ,x x2)
		     (<= ,y y2)
		     (the fixnum
			  (min (%- ,x x1)	;(the fixnum (abs (- ,x x1)))
			       (%- x2 ,x)	;(the fixnum (abs (- x2 ,x)))
			       (%- ,y y1)	;(the fixnum (abs (- ,y y1)))
			       (%- y2 ,y))))))	;(the fixnum (abs (- y2 ,y)))
	 most-positive-fixnum)))



(defun find-greatest-y-coordinate (stream)
  (let ((max 0)
	(presentations (find-greatest-y-coordinate-presentations stream)))
    (declare (fixnum max))
    (dolist (presentation presentations)
      (maximize max (boxed-presentation-bottom presentation)))
    max))

(defun clear-window (stream)
  (let ((y (find-greatest-y-coordinate stream))
	(line-height (window-line-height stream)))
    (set-viewport-position
      stream
      0
      ;; set the position to an intergral number of lines.
      (%* line-height (%1+ (the (values fixnum number) (floor y line-height)))))
    (clear-window-internal stream)))

(defun clear-fake-window (stream)
  (setf (presentation-window-presentations stream) NIL
	(presentation-window-max-x-position stream) 0
	(presentation-window-max-y-position stream) 0
	(window-scroll-x-offset stream) 0
	(window-scroll-y-offset stream) 0
	(window-x-pos stream) (window-left-margin-size stream)
	(window-y-pos stream) (window-top-margin-size stream)))

(defun clear-history (&OPTIONAL (stream *Standard-Output*) (reset-home-p T))
  (setf (presentation-window-presentations stream) NIL)
  (let* ((table (presentation-window-quad-table stream))
	 (array (quad-table-array table))
	 (hash-table (quad-table-table table)))
    (declare (simple-vector array))
    (clrhash hash-table)
    (dotimes (x *Quad-Table-Array-Size*)
      (setf (aref array x) NIL)))
  (setf (presentation-window-max-x-position stream) 0
	(presentation-window-max-y-position stream) 0)
  (when reset-home-p
    (setf (window-scroll-x-offset stream) 0
	  (window-scroll-y-offset stream) 0))
  (clear-window-internal stream)
  (draw-scroll-bars stream))

;;; ****************************************************************************************
;;; PRESENTATION HANDLER STUFF
;;; ****************************************************************************************


(defun add-new-handler (handler)
  (if (eq (handler-from-type handler) 'no-type)
      (progn
	(setq *no-type-handlers* (delete (handler-name handler)
					 *No-Type-Handlers*
					 :KEY #'handler-name
					 :COUNT 1))
	(dolist (entry *No-Type-Handlers-Table*)
	  (setf (cdr entry)
		(delete (handler-name handler)
			(cdr entry)
			:KEY #'handler-name)))
	(dolist (gesture (handler-gestures handler))
	  (let ((mouse-bits (mouse-char-bits gesture)))
	    (let ((entry (assoc mouse-bits *No-Type-Handlers-Table*)))
	      (if (not entry)
		  (setq *No-Type-Handlers-Table*
			(nconc *No-Type-Handlers-Table* (list (list mouse-bits handler))))
		  (push handler (cdr entry))))))
	(push handler *No-Type-handlers*))
      (progn
	(setq *Handlers* (delete (handler-name handler)
				 *Handlers*
				 :KEY #'handler-name
				 :COUNT 1))
	(dolist (entry *Handlers-Table*)
	  (setf (cdr entry)
		(delete (handler-name handler)
			(cdr entry)
			:KEY #'handler-name :COUNT 1)))
	(dolist (gesture (handler-gestures handler))
	  (let ((mouse-bits (mouse-char-bits gesture)))
	    (let ((entry (assoc mouse-bits *Handlers-Table*)))
	      (if (not entry)
		  (setq *Handlers-Table*
			(nconc *Handlers-Table* (list (list mouse-bits handler))))
		  (push handler (cdr entry))))))
	(push handler *Handlers*))))


(defmacro get-handlers-by-mouse-char (mouse-bits
				      &OPTIONAL
				      (table '*Handlers-Table*))
  `(cdr (assoc ,mouse-bits ,table)))



(eval-when (compile eval load)
(defun add-extra-args-to-arglist (arglist)
  (declare (list arglist))
  (cond ((and (member '&REST arglist) (member '&KEY arglist)
	      (member '&ALLOW-OTHER-KEYS arglist))
	 arglist)
	((and (member '&REST arglist) (member '&KEY arglist))
	 (append arglist '(&ALLOW-OTHER-KEYS)))
	((member '&KEY arglist)
	 (let ((position (position '&KEY arglist))
	       (length (length arglist)))
	   (append (butlast arglist (%- length position))
		   '(&REST ignore) (nthcdr position arglist) '(&ALLOW-OTHER-KEYS))))
	((member '&REST arglist)
	 (append arglist '(&KEY &ALLOW-OTHER-KEYS)))
	(T
	 (append arglist '(&REST ignore &KEY &ALLOW-OTHER-KEYS))))))





;;; An alist of menu names with a list of the appropriate handlers.

(defvar *Handler-Menus* NIL)

(defstruct (handler-menu (:TYPE LIST))
  name
  handler
  handlers)


(defun add-handler (name from-type to-type
		    function-name test-name gesture
		    documentation suppress-highlighting menu context-independent
		    blank-area priority exclude-other-handlers
		    &KEY include-body-in-test defines-menu)
  (declare (ignore context-independent))
  (let ((handler
	  (make-handler
	    :NAME name
	    :FROM-TYPE from-type
	    :TO-TYPE to-type
	    :TEST test-name
	    :GESTURES (translate-gesture-to-mouse-char gesture)
	    :DOCUMENTATION documentation
	    :BLANK-AREA blank-area
	    :PRIORITY priority
	    :EXCLUDE-OTHER-HANDLERS exclude-other-handlers
	    :INCLUDE-BODY-IN-TEST include-body-in-test
	    :SUPPRESS-HIGHLIGHTING suppress-highlighting
	    :BODY function-name )))
    (add-new-handler handler)
    (when defines-menu
      (let ((previous-handler-menu
	      (find defines-menu *Handler-Menus* :KEY #'handler-menu-name)))
	(if previous-handler-menu
	    (setf (handler-menu-handler previous-handler-menu) handler)
	    (push (make-handler-menu :NAME defines-menu
				     :HANDLER handler)
		  *Handler-Menus*))))
    (unless (eq from-type 'no-type)
      (dolist (handler-menu *Handler-Menus*)
	(setf (handler-menu-handlers handler-menu)
	      (delete handler (handler-menu-handlers handler-menu)
		      :TEST #'(lambda (h1 h2)
				(equalp (handler-name h1)
					(handler-name h2)))))
	(when (equalp (handler-menu-name handler-menu) menu)
	  (push handler (handler-menu-handlers handler-menu)))))
    handler))

(defmacro define-type-transform (name
				 (from-type
				   to-type
				   &KEY test (gesture :SELECT)
				   documentation
				   suppress-highlighting
				   (menu T)
				   (context-independent NIL)
				   priority
				   exclude-other-handlers
				   blank-area
				   (include-body-in-test T))
				 arglist &BODY body)
  (warn-unimplemented-args exclude-other-handlers NIL
			   context-independent NIL)
  ;; 7/9/90 [sln] Don't use the package of the symbol, since it may be a "locked"
  ;; package.  Instead, just default to *package*.  See comment in definition of
  ;; DEFINE-MOUSE-COMMAND.
  (let ((function-name (intern (lisp-format NIL "~a-~A" :translator name)))
	(test-name (intern (lisp-format NIL "~a-~A-~a" :translator name :test))))
    `(progn
       (defun ,function-name ,(add-extra-args-to-arglist arglist)
	 . ,(if (and (consp body)
		     (consp (first body))
		     (eq 'DECLARE (caar body)))
		`(,(car body)
		  (block ,name . ,(cdr body)))
		`((block ,name . ,body))))
       ,(if test
	    (let ((arglist (first test))
		  (body (cdr test)))
	      `(defun ,test-name ,(add-extra-args-to-arglist arglist)
		 . ,body))
	    NIL)
       (if (let ((gesture (translate-gesture-to-mouse-char ,gesture)))
	     (or (numberp gesture)
		 (null ,gesture)
		 (and (consp gesture)
		      (every #'numberp gesture))))
	   (add-handler
	     ',name ',from-type ',to-type
	     ',function-name
	     ',(and test test-name)
	     ,gesture ,documentation
	     ,suppress-highlighting
	     ,menu ,context-independent ,blank-area (or ',priority 0)
	     ',exclude-other-handlers
	     :INCLUDE-BODY-IN-TEST ,include-body-in-test)
	   (format T "~%Cannot Add Translator since gesture ~A is undefined." ,gesture)))))


(defmacro define-mouse-action (name
			       (from-type
				 to-type
				 &KEY test (gesture :SELECT)
				 documentation
				 suppress-highlighting
				 (menu T)
				 (context-independent NIL)
				 priority
				 exclude-other-handlers
				 blank-area defines-menu)
			       arglist &BODY body)
  (warn-unimplemented-args exclude-other-handlers NIL context-independent NIL)
  (let ((function-name (intern (lisp:format NIL "translator-~A" name) (symbol-package name)))
	(test-name (intern (lisp:format NIL "translator-~A-test" name) (symbol-package name))))
    `(progn
       (defun ,function-name ,(add-extra-args-to-arglist arglist)
	 (block ,name
	   (progn
	     ;; 4/30/90 (sln) More Bit-O-Magic to suppress compiler warnings about unused variables.
	     ,@(remove t (add-extra-args-to-arglist arglist)
		       :test #'(lambda (a1 a2) (declare (ignore a1)) (member a2 lambda-list-keywords)))
	     . ,body)
		NIL))
       ,(if test
	    (let ((arglist (first test))
		  (body (cdr test)))
	      `(defun ,test-name ,(add-extra-args-to-arglist arglist)
		 . ,body))
	    NIL)
       (if (let ((gesture (translate-gesture-to-mouse-char ,gesture)))
	     (or (numberp gesture)
		 (and (consp gesture)
		      (every #'numberp gesture))))
	   (add-handler
	     ',name ',from-type ',to-type
	     ',function-name
	     ',(and test test-name)
	     ,gesture ,documentation
	     ,suppress-highlighting
	     ,menu ,context-independent ,blank-area (or ',priority 0)
	     ',exclude-other-handlers
	     :INCLUDE-BODY-IN-TEST NIL
	     :DEFINES-MENU ,defines-menu)
	   (format T "~%Cannot Add Mouse Action since gesture ~A is undefined." ,gesture)))))


(defmacro define-mouse-command (name
				(type
				  &key test (gesture :SELECT)
				  documentation
				  suppress-highlighting
				  (menu T)
				  priority
				  (include-body-in-test T)
				  blank-area)
				arglist &BODY body)
    `(define-type-transform ,name
			    (,type
			     command
			     :test ,test
			     :gesture ,gesture
			     :documentation ,documentation
			     :suppress-highlighting ,suppress-highlighting
			     :menu ,menu
			     :priority ,priority
			     :include-body-in-test ,include-body-in-test
			     :blank-area ,blank-area)
			    ,arglist
       . ,body))

(setq *No-Type* (make-presentation :TYPE 'NO-TYPE))

;;; the top level loop should look for each translator, if there is an object
;;; show it.

(defun find-closest-presentation-with-valid-handler (stream x y)
  #.(fast)
  (meter find-closest-top
  (when (presentation-window-p stream)
    (let ((*Command-Menu-Test-Phase* T))
      (let (closest-object (closest-distance most-positive-fixnum)
	    handler
	    (possible-handlers  ;;(get-handlers-by-mouse-char (mouse-chord-shifts))
	      (get-handlers-for-context)
	      ))
	#+ignore
	(when *Debug*
	  (format tv:initial-lisp-listener
		  "~%Mouse Shifts ~D, Handlers ~D." (mouse-chord-shifts) possible-handlers))
	(dolist (presentation (find-objects-near-coords x y stream))
	  (when (presentation-p presentation)
	    (multiple-value-setq (closest-distance closest-object handler)
	      (find-closest-presentation-with-valid-handler-internal
		stream
		presentation x y closest-distance closest-object handler possible-handlers))))
	;; we need to check for no-type handlers even if we already found a valid handler
	;; since one of them might have a higher priority
	(multiple-value-setq (closest-object handler)
	  (find-valid-no-type-handler
	    stream (get-handlers-by-mouse-char (mouse-chord-shifts)
					       *No-Type-Handlers-Table*)
	    x y closest-object handler))
	(values closest-object handler))))))



(defun find-valid-handler (stream presentation possible-handlers mouse-x mouse-y)
  #.(fast)
  (meter find-valid-handler
  (let ((presentation-type (presentation-type presentation))
	(object (presentation-object presentation))
	(highest-priority-handler NIL))
    (do ((contexts *Input-Context* (cdr contexts)))
	((null contexts) NIL)
      (dolist (handler possible-handlers NIL)
	(when (and (or (not highest-priority-handler)
		       (> (handler-priority handler)
			  (handler-priority highest-priority-handler)))
		   (not (handler-suppress-highlighting handler))
		   (match-type-to-context
		     (first contexts) handler presentation-type presentation)
		   (or (null (handler-test handler))
		       (funcall (handler-test handler)
				object
				:MOUSE-X mouse-x :MOUSE-Y mouse-y
				:PRESENTATION presentation
				:GESTURE (handler-gestures handler)
				:WINDOW stream))
		   (or (not (handler-include-body-in-test handler))
		       (let ((body-value
			       (funcall (handler-body handler)
					object
					:MOUSE-X mouse-x :MOUSE-Y mouse-y
					:PRESENTATION-TYPE presentation-type
					:GESTURE
					(handler-gestures handler)
					:WINDOW stream)))
			 (and body-value
			      ;; need to check the body value
			      ;; if it is a command translator
			      (or (not (eq (handler-to-type handler)
					   'COMMAND))
				  ;; it is a command, better be a
				  ;; valid command in the current table
				  (and *Command-Table*
				       (command-in-command-table-p
					 (first body-value)
					 *Command-Table* NIL)))))))
	  (setq highest-priority-handler handler))))
    highest-priority-handler)))

(defun find-valid-no-type-handler (stream possible-handlers mouse-x mouse-y
				   closest-object closest-handler)
  #.(fast)
  (do ((contexts *Input-Context* (cdr contexts)))
      ((null contexts) NIL)
    (dolist (handler possible-handlers NIL)
      (when (and (or (not closest-handler)
		     (> (handler-priority handler)
			(handler-priority closest-handler)))
		 (not (handler-suppress-highlighting handler))
		 (match-type-to-no-type-context (first contexts) handler)
		 (or (null (handler-test handler))
		     (funcall (handler-test handler)
			      NIL
			      :MOUSE-X mouse-x :MOUSE-Y mouse-y
			      :GESTURE (handler-gestures handler)
			      :WINDOW stream))
		 (or (not (handler-include-body-in-test handler))
		     (let ((body-value
			     (funcall (handler-body handler)
				      NIL
				      :MOUSE-X mouse-x :MOUSE-Y mouse-y
				      :PRESENTATION-TYPE (first contexts)
				      :GESTURE
				      (handler-gestures handler)
				      :WINDOW stream)))
		       (and body-value
			    ;; need to check the body value
			    ;; if it is a command translator
			    (or (not (eq (handler-to-type handler)
					 'COMMAND))
				;; it is a command, better be a
				;; valid command in the current table
				(and *Command-Table*
				     (command-in-command-table-p
				       (first body-value)
				       *Command-Table* NIL)))))))
	(setq closest-handler handler
	      closest-object *No-Type*))))
  (values closest-object closest-handler))



(defun find-valid-handlers (stream presentation possible-handlers mouse-x mouse-y)
  #.(fast)
  (let ((*Command-Menu-Test-Phase* T)
	(presentation-type (presentation-type presentation))
	(object (presentation-object presentation))
	(handlers NIL))
    (do ((contexts *Input-Context* (cdr contexts)))
	((null contexts) NIL)
      (dolist (handler possible-handlers NIL)
	#+ignore(when *DEbug*
	  (format tv:initial-lisp-listener "~%Testing Handler ~D."
		  (handler-name handler)))
	(when (and (match-type-to-context
		     (first contexts) handler presentation-type presentation)
		   (or (null (handler-test handler))
		       (funcall (handler-test handler)
				object
				:MOUSE-X mouse-x :MOUSE-Y mouse-y
				:PRESENTATION presentation
				:GESTURE (handler-gestures handler)
				:WINDOW stream))
		   (or (not (handler-include-body-in-test handler))
		       (let ((body-value
			       (funcall (handler-body handler)
					object
					:MOUSE-X mouse-x :MOUSE-Y mouse-y
					:PRESENTATION-TYPE presentation-type
					:GESTURE (handler-gestures handler)
					:WINDOW stream)))
			 (and body-value
			      ;; need to check the body value
			      ;; if it is a command translator
			      (or (not (eq (handler-to-type handler)
					   'COMMAND))
				  ;; it is a command, better be a
				  ;; valid command in the current table
				  (and *Command-Table*
				       (command-in-command-table-p
					 (first body-value)
					 *Command-Table* NIL)))))))
	  (pushnew handler handlers))))
    handlers))


;;; I'm not sure if this makes sense or not, an experiment, to unroll this function once.
;;; it gets rid of a level of multiple value return stuff as well as several arguments
;;; that don't need to passed down (x, y, closest-distance, closest-object,
;;; closest-handler possible-handlers).  Good Luck!

(defun find-closest-presentation-with-valid-handler-internal (stream presentation x y
							      closest-distance closest-object
							      closest-handler
							      possible-handlers
							      &AUX (distance 0) handler)
  #.(fast)
  (declare (fixnum x y closest-distance distance))
  (if (= most-positive-fixnum (setq distance (distance-from-presentation presentation x y)))
      (values closest-distance closest-object closest-handler)
      (progn
	(let ((distance 0))
	  (declare (fixnum distance))
	  (dolist (presentation-inferior-1 (presentation-records presentation))
	    (when (presentation-p presentation-inferior-1)
	      (if (= most-positive-fixnum
		     (setq distance (distance-from-presentation presentation-inferior-1 x y)))
		  NIL
		  (progn
		    (dolist (presentation-inferior-2
			      (presentation-records presentation-inferior-1))
		      (when (presentation-p presentation-inferior-2)
			(multiple-value-setq
			  (closest-distance closest-object closest-handler)
			  (find-closest-presentation-with-valid-handler-internal
			    stream presentation-inferior-2 x y closest-distance
			    closest-object closest-handler
			    possible-handlers))))
		    (if (and (not (eq 'memo (presentation-type presentation-inferior-1)))
			     (setq handler
				   (find-valid-handler stream presentation-inferior-1
						       possible-handlers x y))
			     (or (= closest-distance most-positive-fixnum)
				 (< distance closest-distance)
				 ;; well both handlers are good,
				 ;; now make a choice which has higher priority
				 (> (handler-priority handler)
				    (handler-priority closest-handler))))
			(setq closest-distance distance
			      closest-object presentation-inferior-1
			      closest-handler handler)))))))
	(if (and (not (eq 'memo (presentation-type presentation)))
		 (setq handler (find-valid-handler stream presentation possible-handlers x y))
		 (or (= closest-distance most-positive-fixnum)
		     (< distance closest-distance)
		     ;; well both handlers are good,
		     ;; now make a choice which has higher priority
		     (> (handler-priority handler)
			(handler-priority closest-handler))))
	    (values distance presentation handler)
	    (values closest-distance closest-object closest-handler)))))



#+ignore
(defun find-closest-presentation-with-valid-handler-internal (stream presentation x y
							      closest-distance closest-object
							      closest-handler
							      possible-handlers
							      &AUX (distance 0) handler)
  #.(fast)
  (declare (fixnum x y closest-distance distance))
  (if (= most-positive-fixnum (setq distance (distance-from-presentation presentation x y)))
      (values closest-distance closest-object closest-handler)
      (progn
	(dolist (presentation-inferior (presentation-records presentation))
	  (when (presentation-p presentation-inferior)
	    (multiple-value-setq (closest-distance closest-object closest-handler)
	      (find-closest-presentation-with-valid-handler-internal
		stream
		presentation-inferior x y closest-distance closest-object closest-handler
		possible-handlers))))
	(if (and (setq handler (find-valid-handler stream presentation possible-handlers x y))
		 (or (= closest-distance most-positive-fixnum)
		     (< distance closest-distance)
		     ;; well both handlers are good,
		     ;; now make a choice which has higher priority
		     (> (handler-priority handler)
			(handler-priority closest-handler))))
	    (values distance presentation handler)
	    (values closest-distance closest-object closest-handler)))))

(defun find-selected-closest-presentation-with-valid-handler (stream presentation gesture
							      mouse-x mouse-y)
  #.(fast)
  (let ((*Command-Menu-Test-Phase* T))
    (if (eq presentation *No-Type*)
	(find-selected-closest-presentation-with-valid-handler-for-no-type-presentation
	  stream gesture mouse-x mouse-y)
	(find-selected-closest-presentation-with-valid-handler-for-regular-presentation
	  stream presentation gesture mouse-x mouse-y))))

(defun find-selected-closest-presentation-with-valid-handler-for-no-type-presentation
       (stream gesture mouse-x mouse-y)
  #.(fast)
  (declare (values handler context))
  (let ((handlers (get-handlers-by-mouse-char (mouse-chord-shifts)
					      *No-Type-Handlers-Table*))
	(highest-priority-handler NIL)
	(highest-priority-handler-context NIL))
    (do ((contexts *Input-Context* (cdr contexts)))
	((null contexts))
      (dolist (handler handlers)
	#+ignore (dformat "~%Testing Handler ~D." (handler-name handler))
	(when (and (or (not highest-priority-handler)
		       (> (handler-priority handler)
			  (handler-priority highest-priority-handler)))
		   (member gesture (handler-gestures handler))
		   (match-type-to-no-type-context (first contexts) handler)
		   (or (not (handler-test handler))
		       (funcall (handler-test handler)
				NIL
				:MOUSE-X mouse-x :MOUSE-Y mouse-y
				:PRESENTATION-TYPE (first contexts)
				:GESTURE (handler-gestures handler)
				:WINDOW stream))
		   (or (not (handler-include-body-in-test handler))
		       (let ((body-value
			       (funcall (handler-body handler)
					NIL
					:MOUSE-X mouse-x :MOUSE-Y mouse-y
					:PRESENTATION-TYPE (first contexts)
					:GESTURE
					(handler-gestures handler)
					:WINDOW stream)))
			 (and body-value
			      ;; need to check the body value if it is a
			      ;; command translator
			      (or (not (eq (handler-to-type handler)
					   'COMMAND))
				  ;; it is a command, better be a valid command in
				  ;;the current table
				  (and *Command-Table*
				       (command-in-command-table-p
					 (first body-value)
					 *Command-Table* NIL)))))))
	  (setq highest-priority-handler handler
		highest-priority-handler-context contexts))))
    (values highest-priority-handler highest-priority-handler-context)))


(defun find-selected-closest-presentation-with-valid-handler-for-regular-presentation
       (stream presentation gesture mouse-x mouse-y)
  #.(fast)
  (declare (values handler context))
  (let ((presentation-type (presentation-type presentation))
	(object (presentation-object presentation))
	(handlers ;;(get-handlers-by-mouse-char (mouse-chord-shifts))
	 (get-handlers-for-context)
		  )
	(highest-priority-handler NIL)
	(highest-priority-handler-context NIL))
    (do ((contexts *Input-Context* (cdr contexts)))
	((null contexts))
      (dolist (handler handlers)
	#+ignore (dformat "~%Testing Handler ~D." (handler-name handler))
	(when (and (or (not highest-priority-handler)
		       (> (handler-priority handler)
			  (handler-priority highest-priority-handler)))
		   (and (or (eq (handler-name handler) 'EQUALITY-TRANSLATOR)
			    (member gesture (handler-gestures handler)))
			(match-type-to-context (first contexts) handler presentation-type
					       presentation))
		   (or (not (handler-test handler))
		       (funcall (handler-test handler)
				object
				:MOUSE-X mouse-x :MOUSE-Y mouse-y
				:PRESENTATION-TYPE presentation-type
				:PRESENTATION presentation
				:GESTURE (handler-gestures handler)
				:WINDOW stream))
		   (or (not (handler-include-body-in-test handler))
		       (let ((body-value
			       (funcall (handler-body handler)
					object
					:MOUSE-X mouse-x :MOUSE-Y mouse-y
					:PRESENTATION-TYPE presentation-type
					:PRESENTATION presentation
					:GESTURE (handler-gestures handler)
					:WINDOW stream)))
			 (and body-value
			      ;; need to check the body value if it is a
			      ;; command translator
			      (or (not (eq (handler-to-type handler)
					   'COMMAND))
				  ;; it is a command, better be a valid command in
				  ;;the current table
				  (and *Command-Table*
				       (command-in-command-table-p (first body-value)
								   *Command-Table* NIL)))))))
	  (setq highest-priority-handler handler
		highest-priority-handler-context contexts))))
    (values highest-priority-handler highest-priority-handler-context)))



(defvar *Mouse-Gestures*
	(append '((:select . #.*Mouse-Left*)
		  (:describe . #.*mouse-middle*)
		  (:menu . #.*mouse-right*)
		  (:alternate-select . #.*shift-mouse-left*)
		  (:select-and-activate . #.*shift-mouse-left*)
		  (:inspect . #.*Shift-mouse-middle*)
		  (:delete . #.*Shift-mouse-middle*)
		  (:remove . #.*Shift-mouse-middle*)
		  (:system-menu . #.*Shift-mouse-right*)
		  (:edit-definition . #.*meta-mouse-left*)
		  (:edit-function . #.*meta-mouse-left*)
		  (:evaluate-form . #.*meta-mouse-middle*)
		  (:disassemble . #.*meta-mouse-middle*))
	      (mapcar #'(lambda (x)
			  (cons (cdr x) (symbol-value (car x))))
		      *Mouse-Character-Names*)))

(defun mouse-char-gestures (mouse-char)
  (mapcan #'(lambda (item)
	      (when (equal mouse-char (cdr item))
		(list (first item))))
	  *Mouse-Gestures*))

(defun mouse-char-gesture (mouse-char)
  (some #'(lambda (item)
	      (when (equal mouse-char (cdr item))
		(first item)))
	  *Mouse-Gestures*))

(defun mouse-char-for-gesture (gesture)
  #.(fast)
  (cdr (assoc gesture *Mouse-Gestures*)))

(defun set-mouse-char-for-gesture (gesture mouse-char)
  (when (symbolp mouse-char)
    (setq mouse-char (mouse-char-for-gesture mouse-char)))
  (let ((item (assoc gesture *Mouse-Gestures*)))
    (if item (setf (cdr item) mouse-char)
	(push (cons gesture mouse-char) *Mouse-Gestures*))))

(defsetf mouse-char-for-gesture set-mouse-char-for-gesture)

(defun translate-gesture-to-mouse-char (gesture)
  (cond ((eq gesture T)
	 '(#.*mouse-left* #.*mouse-Middle* #.*mouse-Right*))
	((numberp gesture) (list gesture))
	((consp gesture)
	 (mapcar #'(lambda (x) (cdr (assoc x *mouse-gestures*))) gesture))
	((cdr (assoc gesture *mouse-gestures*))
	 (list (cdr (assoc gesture *mouse-gestures*))))))



(defun dformat (ctrl-string &rest args)
  (when *Debug*
    (apply #'lisp-format #+symbolics tv:selected-window
	   #-symbolics *Debug-Io* ctrl-string args)))

(defun show-documentation-for-handlers (presentation stream mouse-x mouse-y)
  #.(fast)
  (meter show-documentation
  (cond ((lisp:typep stream 'scroll-bar)
	 (show-scroll-bar-documentation stream mouse-x mouse-y))
	((not presentation)
	 (set-mouse-documentation-string ""))
	(T
	 (let (left middle right
	       (*Command-Menu-Test-Phase* :DOCUMENTATION))
	   (if (eq presentation *No-Type*)
	       (let ((keyboard-bits (mouse-chord-shifts)))
		 ;;(dformat "~%Keyboard Bits ~D" keyboard-bits)
		 (dolist (handler (get-handlers-by-mouse-char
				    (mouse-chord-shifts)
				    *No-Type-Handlers-Table*))
		   (dolist (mouse-char (handler-gestures handler))
		     (let ((bits (mouse-char-bits mouse-char)))
		       #+ignore
		       (dformat "~% Name ~A Bits ~D Mouse Char ~D"
				(handler-name handler) bits mouse-char)
		       (when (and (dolist (context *Input-Context* NIL)
				    (when (match-type-to-no-type-context context handler)
				      (return T)))
				  ;;(or (dformat "~&    Matched Context.") T)
				  (eql keyboard-bits bits)
				  ;;(or (dformat "~&    Matched Bits.") T)
				  (or (null (handler-test handler))
				      (funcall (handler-test handler)
					       NIL
					       :MOUSE-X mouse-x :MOUSE-Y mouse-y
					       :GESTURE (handler-gestures handler)
					       :WINDOW stream))
				  ;;(or (dformat "~&    Matched Test.") T)
				  (or (not (handler-include-body-in-test handler))
				      (let ((body-value
					      (funcall (handler-body handler)
						       NIL
						       :MOUSE-X mouse-x :MOUSE-Y mouse-y
						       :GESTURE (handler-gestures handler)
						       :WINDOW stream)))
					(and body-value
					     ;; need to check the body value if it is a command translator
					     (or (not (eq (handler-to-type handler)
							  'COMMAND))
						 ;; it is a command,
						 ;; better be a valid command in the current
						 ;; table
						 (and *Command-Table*
						      (command-in-command-table-p
							(first body-value)
							*Command-Table* NIL))))))
				  ;;(or (dformat "~&    Matched Compose.") T)
				  )
			 ;;(dformat "~&    Mouse Button ~D" (mouse-char-button mouse-char))
			 (case (mouse-char-button mouse-char)
			   (0 (when (or (not left)
					(> (handler-priority handler)
					   (handler-priority left)))
				(setq left handler)))
			   (1 (when (or (not middle)
					(> (handler-priority handler)
					   (handler-priority middle)))
				(setq middle handler)))
			   (2 ;;(dformat "~&Maybe Setting Right to ~A" handler)
			     (when (or (not right)
				       (> (handler-priority handler)
					  (handler-priority right)))
			       (setq right handler)))))))))
	       (let ((presentation-type (presentation-type presentation))
		     (object (presentation-object presentation))
		     (keyboard-bits (mouse-chord-shifts)))
		 ;;(dformat "~%Keyboard Bits ~D" keyboard-bits)
		 (dolist (handler (get-handlers-by-mouse-char (mouse-chord-shifts)))
		   (dolist (mouse-char (handler-gestures handler))
		     (let ((bits (mouse-char-bits mouse-char)))
		       #+ignore
		       (dformat "~% Name ~A Bits ~D Mouse Char ~D"
				(handler-name handler) bits mouse-char)
		       (when (and (dolist (context *Input-Context* NIL)
				    (when (match-type-to-context context handler presentation-type
								 presentation)
				      (return T)))
				  ;;(or (dformat "~&    Matched Context.") T)
				  (eql keyboard-bits bits)
				  ;;(or (dformat "~&    Matched Bits.") T)
				  (or (null (handler-test handler))
				      (funcall (handler-test handler)
					       object
					       :MOUSE-X mouse-x :MOUSE-Y mouse-y
					       :PRESENTATION presentation
					       :GESTURE (handler-gestures handler)
					       :WINDOW stream
					       :PRESENTATION-TYPE presentation-type))
				  ;;(or (dformat "~&    Matched Test.") T)
				  (or (not (handler-include-body-in-test handler))
				      (let ((body-value
					      (funcall (handler-body handler)
						       object
						       :MOUSE-X mouse-x :MOUSE-Y mouse-y
						       :PRESENTATION presentation
						       :GESTURE (handler-gestures handler)
						       :WINDOW stream
						       :PRESENTATION-TYPE presentation-type)))
					(and body-value
					     ;; need to check the body value if it is a command translator
					     (or (not (eq (handler-to-type handler)
							  'COMMAND))
						 ;; it is a command,
						 ;; better be a valid command in the current
						 ;; table
						 (and *Command-Table*
						      (command-in-command-table-p
							(first body-value)
							*Command-Table* NIL))))))
				  ;;(or (dformat "~&    Matched Compose.") T)
				  )
			 (case (mouse-char-button mouse-char)
			   (0 (when (or (not left)
					(> (handler-priority handler)
					   (handler-priority left)))
				(setq left handler)))
			   (1 (when (or (not middle)
					(> (handler-priority handler)
					   (handler-priority middle)))
				(setq middle handler)))
			   (2 (when (or (not right)
					(> (handler-priority handler)
					   (handler-priority right)))
				(setq right handler))))))))))
	   ;; no we have the appropriate left, middle, and right handlers regardless of
	   ;; where they came from above.
	   (labels ((documentation (handler)
		      (or (if (eq 'EQUALITY-TRANSLATOR (handler-name handler))
			      (with-presentations-disabled
				(make-printed-representation-of-presentation-blip
				  (presentation-type presentation)
				  (presentation-object presentation)))
			      (handler-documentation handler))
			  (and (handler-include-body-in-test handler)
			       (let ((body-value
				       (funcall (handler-body handler)
						(presentation-object presentation)
						:MOUSE-X mouse-x :MOUSE-Y mouse-y
						:PRESENTATION presentation
						:GESTURE (handler-gestures handler)
						:WINDOW stream
						:PRESENTATION-TYPE
						(presentation-type presentation))))
				 ;; use the body-value to get the documentation string.
				 ;;(print *input-context* tv:initial-lisp-listener)
				 (make-printed-representation-of-presentation-blip
				   (dolist (context *Input-Context* NIL)
				     (when (match-type-to-context
					     context handler (presentation-type presentation)
					     presentation)
				       (return context)))
				  body-value)))
			  (if (symbolp (handler-name handler))
			      (symbol-name (handler-name handler))
			      (format NIL "~A" (handler-name handler))))))
	     ;;(dformat "Left ~S Middle ~S Right ~S" left middle right)
	     (when left (setq left (documentation left)))
	     (when middle (setq middle (documentation middle)))
	     (when right (setq right (documentation right)))
	     (let ((documentation-string
		     (if left
			 (if middle
			     (if right
				 (concatenate 'string
					      "L: " left "  M: " middle "  R: " right)
				 (concatenate 'string "L: " left "  M: " middle))
			     (if right
				 (concatenate 'string
					      "L: " left "  R: " right)
				 (concatenate 'string "L: " left)))
			 (if middle
			     (if right
				 (concatenate 'string "  M: " middle "  R: " right)
				 (concatenate 'string "  M: " middle))
			     (if right
				 (concatenate 'string "  R: " right)
				 "")))
		     #+ignore
		     (format NIL "~:[~*~;L: ~A~]~:[~*~;    M: ~A~]~:[~*~;   R: ~A~]"
			     left left middle middle right right)))
	       (set-mouse-documentation-string documentation-string))))))))













(defun write-char-to-recording-string (output-stream char)
  #.(fast)
  (let ((string (presentation-recording-string-string output-stream)))
    (vector-push-extend char string 30)))

(defun write-string-to-recording-string (output-stream string)
  (declare (string string))
  (let* ((recording-string (presentation-recording-string-string output-stream))
	 (recording-string-length (array-dimension recording-string 0))
	 (recording-string-active-length (length recording-string))
	 (string-length (length string)))
    (declare (string recording-string))
    (when (%> (%+ recording-string-active-length string-length) recording-string-length)
      ;; extend the array
      (setf (presentation-recording-string-string output-stream)
	    (setq recording-string
		  (adjust-array recording-string
				(%+ 30 recording-string-active-length string-length)))))
    ;; set the fill pointer and then copy characters over
    (setf (fill-pointer recording-string) (+ recording-string-active-length string-length))
    (dotimes (x string-length)
      (setf (aref recording-string (%+ x recording-string-active-length))
	    (aref string x))))
  output-stream)






(defun call-presentation-menu (menu-type &KEY presentation original-presentation
			       window label x y &ALLOW-OTHER-KEYS)
  (declare (ignore original-presentation))
  (let ((menu-handler (find menu-type *Handler-Menus* :KEY #'FIRST)))
    (when menu-handler
      (let ((active-handlers
	      (find-valid-handlers window presentation
				   (handler-menu-handlers menu-handler)
				   x y)))
	(let ((choice (menu-choose active-handlers
				   :PRINTER
				   #'(lambda (handler stream type)
				       (declare (ignore type))
				       (princ
					 (if (lisp:typep handler 'HANDLER)
					     (or (handler-documentation handler)
						 (make-pretty-name (handler-name handler)))
					     handler)
					 stream))
				   :PROMPT
				   (or label
				       (with-output-to-string (stream)
					 (simple-princ "Choose Operation on " stream)
					 (display (presentation-object presentation)
						  (presentation-type presentation)
						  :STREAM stream))))))
	  (when choice
	    (let ((body-value
		    (funcall (handler-body choice)
			     (presentation-object presentation)
			     :MOUSE-X x :MOUSE-Y y
			     :PRESENTATION-TYPE (presentation-type presentation)
			     :GESTURE (handler-gestures choice)
			     :WINDOW window)))
	      body-value)))))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
