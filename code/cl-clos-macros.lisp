;;; -*- Mode: Lisp; Syntax: Common-lisp; Package: EXPRESS-WINDOWS; Base: 10. -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'express-windows)


;; 6/14/90 (sln) allow for non-pcl CLOSes.
#+EW-CLOS
(import (mapcar #'(lambda (sym) (intern (string sym) (or (find-package :clos) #+pcl :pcl)))
		'(:defclass :defmethod))
	'express-windows)

#-EW-CLOS
(defmacro defclass (name includes slots &REST options)
  (let ((defstruct-slots (mapcar #'(lambda (slot)
				     (cond ((symbolp slot) slot)
					   ((consp slot)
					    `(,(first slot)
					      ,(getf (cdr slot) :INITFORM)
					      ,@(and (getf (cdr slot) :TYPE)
						     `(:TYPE ,(getf (cdr slot) :TYPE)))))
					   (T (error "Not A Valid Slot"))))
				 slots)))
    (when (> (length includes) 1) (error "Can only include One Superior in a defstruct."))
    `(defstruct (,name
		 ,@(and includes `((:include . ,includes)))
		 (:PRINT-FUNCTION ,(intern (lisp:format NIL "PRINT-~A" name) (symbol-package name)))
		 (:CONC-NAME ,(or ;(getf options :ACCESSOR-PREFIX)
				  (intern (lisp:format NIL "~A-" name) (symbol-package name))))
		 ,@(and (assoc :constructor options) `(,(assoc :constructor options))))
       . ,defstruct-slots)))

#-EW-CLOS
(defmacro defmethod (name specialized-lambda-list &BODY body)
  ;; this is only designed to work on defmethods specialized on first arg.
  ;; also does not provide any with-slots method.
  `(defun ,name ,(cons (caar specialized-lambda-list)
		       (cdr specialized-lambda-list))
     . ,body))


;;set-cursorpos-and-size-from-char draw-cursor

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
