;;; -*- Mode: LISP; Syntax: Common-Lisp; Base: 10; Package: Express-Windows -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */

(cl:in-package 'ew)

;;;****************************************************************
;;;****************************************************************
;;;************************* DEMOS ********************************
;;;****************************************************************
;;;****************************************************************

(defvar *Demos* NIL)
(proclaim '(list *Demos*))

(defvar *Last-Demo-Run* NIL)

(defmacro define-demo (name arglist &BODY body)
  (let ((defun `(defun ,name ,arglist . ,body)))
    `(progn
       (pushnew ',name *Demos*)
       (setf (get ',name :CODE-STRING)
	     ,(with-output-to-string (stream)
		(write defun :stream stream :pretty :code)))
       ,defun)))


		     
(defun wait-next-step ()
  (query 'character :PROMPT "Type a Char for the Next Step" :PROMPT-MODE :RAW
	 :PROVIDE-DEFAULT NIL)
  (fresh-line *Query-IO*))

(define-demo demo-lines ()
  (clear-history)
  (format T "~&Demoing Memoized Graphics.")
  (terpri *Standard-output*)
  (let* ((list (copy-tree '((100 100 200 100)
			    (200 100 300 100)
			    (100 300 200 300)
			    (200 300 300 300)
			    (300 100 300 300)
			    (100 300 100 100))))
	 (d (memo ()
	      (dolist (args list)
		(memoize (:VALUE args :COPY-VALUE T :VALUE-TEST #'EQUAL)
		  (apply 'draw-line args))))))
    (run-memo d)
    (wait-next-step)
    (dotimes (x 20)
      #+symbolics (declare (ignore x))			;6/27/90 (sln)
      (incf (fourth (first list)) 5)
      (incf (second (second list)) 5)
      (decf (fourth (third list)) 5)
      (decf (second (fourth list)) 5)
      (run-memo d))
    (dotimes (x 20)
      #+symbolics (declare (ignore x))			;6/27/90 (sln)
      (decf (fourth (first list)) 5)
      (decf (second (second list)) 5)
      (run-memo d))
    (dotimes (x 20)
      #+symbolics (declare (ignore x))			;6/27/90 (sln)
      (incf (fourth (third list)) 5)
      (incf (second (fourth list)) 5)
      (run-memo d))))


(define-demo demo-list-of-numbers ()
  (clear-history)
  (format T "Demoing a simple Memoed list of numbers.")
  (let* ((list (copy-list '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15)))
	 (d (memo ()
	      (dolist (x list)
		(memoize (:value x)
		  (print x))))))
    (run-memo d)
    (wait-next-step)
    (do ((elements list (cdr elements)))
	((null elements))
      (when (zerop (rem (first elements) 5))
	(incf (first elements))))
    (with-character-style ('(NIL :italic NIL))
      (run-memo d))
;    (wait-next-step)
;    (run-memo d)
    ))

(define-demo demo-list-of-numbers-2 (&AUX (fast-p
					    (menu-choose '(("Fast" . T) ("Slow"))
							 :PROMPT "Choose Fast or Slow")))
  (clear-history)
  (format T "Demoing a simple Memoed list of numbers.~%")
  (let* ((list (copy-list '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15)))
	 (d (memo ()
	      (dolist (x list)
		(memoize (:value x)
		  (princ x)
		  (terpri))))))
    (run-memo d)
    (wait-next-step)
    (dotimes (x 15)
      (when (> x 0)
	(setf (nth (1- x) list) (1- x)))
      (setf (nth x list) 'X)
      (run-memo d)
      (unless fast-p
	(and (listen-any T 1) (read-any T))))
    (dotimes (x 15)
      (let ((x (- 14 x)))
	(when (< x 14)
	  (setf (nth (1+ x) list) (1+ x)))
	(setf (nth x list) 'X)
	(run-memo d)
	(unless fast-p
	  (and (listen-any T 1) (read-any T)))))
    (setf (first list) 0)
    (run-memo d)))

#+ignore
(defun demo-list-of-numbers-4 ()
  (clear-history)
  (format T "~%Demoing a simple Memoed list of numbers.~%")
  (let* ((list (copy-list '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)))
	 (d (memo ()
	      (dolist (x list)
		(memoize (:value x)
		  (princ x)
		  (terpri))))))
    (run-memo d)
    ;;(wait-next-step)
    (time
      (progn
	(dotimes (x 20)
	  (when (> x 0)
	    (setf (nth (1- x) list) (1- x)))
	  (setf (nth x list) 'X)
	  (run-memo d))
	(dotimes (x 20)
	  (let ((x (- 19 x)))
	    (when (< x 19)
	      (setf (nth (1+ x) list) (1+ x)))
	    (setf (nth x list) 'X)
	    (run-memo d)))
	(setf (first list) 0)
	(run-memo d)))))

#+ignore
(defun demo-list-of-numbers-3 (&AUX (stream *standard-output*))
  (scl:send stream :CLEAR-HISTORY)
  (format T "~%Demoing a simple Memoed list of numbers.~%")
  (let* ((list (copy-list '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)))
	 (d (dw:redisplayer ()
	      (dolist (x list)
		(dw:with-redisplayable-output (:cache-value x)
		  (princ x)
		  (terpri))))))
    (dw:do-redisplay d)
    ;(wait-next-step)
    (time
      (progn
	(dotimes (x 20)
	  (when (> x 0)
	    (setf (nth (1- x) list) (1- x)))
	  (setf (nth x list) 'X)
	  (dw:do-redisplay d))
	(dotimes (x 20)
	  (let ((x (- 19 x)))
	    (when (< x 19)
	      (setf (nth (1+ x) list) (1+ x)))
	    (setf (nth x list) 'X)
	    (dw:do-redisplay d)))
	(setf (first list) 0)
	(dw:do-redisplay d)))))

(define-demo demo-make-table-multiply (&OPTIONAL (x 5) (y 5))
  (clear-history)
  (format T "~&Demoing Multiplication Table~%")
  ;; create array with data.
  (let ((array (make-array (list x y))))
    (dotimes (i x)
      (dotimes (j y)
	(setf (aref array i j) (* i j))))
    ;; display array information.
    (make-table-from-sequence
      array :ROW-SPACING 10 :COLUMN-SPACING 20 :RETURN-AT-END NIL
      :ADDITIONAL-INDENTATION NIL
      :PRINTER
      #'(lambda (item *Standard-Output* x y)
	  (cond ((zerop x) (princ y))
		((zerop y) (princ x))
		(T (display-as (:TYPE 'integer :OBJECT item)
		     (make-table ()
		       (dotimes (z 4)
			 #+symbolics (declare (ignore z))			;6/27/90 (sln)
			 (when (plusp item)
			   (row ()
			     (dotimes (zz 4)
			       #+symbolics (declare (ignore zz))			;6/27/90 (sln)
			       (when (plusp item)
				 (entry ()
				   (draw-circle 10 10 10 :FILLED T)
				   (decf item)))))))))))))))




(define-demo demo-make-table-simple-multiply ()
  (clear-history)
  (format T "~&Demoing Simple Multiplication Table~%")
  (make-table (() :row-spacing 10 :column-spacing 10)
    (dotimes (x 6)
      (row ()
	(dotimes (y 6)
	  (entry (() :align T)
	    (cond ((zerop x) y)
		  ((zerop y) x)
		  (T (let ((value (* x y)))
		       (display-as (:TYPE 'integer :object value)
			 (draw-circle (* 2 value) (* 2 value) value :filled NIL)))))))))))


(define-demo demo-memoized-make-table-row-changes ()
  (clear-history *Standard-Output*)
  (format T "~&Demoing Memoized Table with Row Changes~%")
  (set-cursorpos *Standard-Output* 300. 100.)
  (let* ((list (copy-tree '(("AB" "CD") ("ABC" "CDC") ("XX" "YY"))))
	 (d (memo ()
	      (make-table ()
		(dolist (item list)
		  (row (NIL :id item)
		    (dolist (i item)
		      (entry (T :VALUE i)))))))))
    (run-memo d)
    (wait-next-step)
    (setf (cdr list) (cddr list))
    (run-memo d)
    (wait-next-step)
    (setf (cdr list) (cons (copy-list '("Foo" "Bar")) (cdr list)))
    (run-memo d)))


#+ignore ;; SYMBOLICS 7.2 DW
(dw:redisplayer ()
  (formatting-table ()
    (dolist (item list)
      (dw:with-redisplayable-output (:UNIQUE-ID item)
	(formatting-row ()
	  (dolist (i item)
	    (formatting-cell ()
	      (dw:with-redisplayable-output (:cache-value i)
		(princ i)))))))))

#+ignore  ;; EXPRESS WINDOWS.
(memo ()
  (make-table ()
    (dolist (item list)
      (row (NIL :id item)
	(dolist (i item)
	  (entry (T :VALUE i)))))))

(define-demo demo-menu-choose (&OPTIONAL (style '(:FIX :ROMAN :HUGE)))
  (menu-choose '(("First Choice" :value :first)
		 ("Second Choice with Bold Style" :value :second :style
		  (NIL :bold NIL))
		 ("Third Choice with Italic Style" :value :second :style
		  (NIL :italic NIL))
		 ("Unselectable Text" :no-select T)
		 ("Fourth Choice" :value :third))
	       :character-style style
	       :prompt "Sample Choose Menu"))

(define-demo demo-menu-choose-with-graphics ()
  (menu-choose '(5 10 15 20)
	       :PRINTER #'(lambda (number stream &REST args)
			    (declare (ignore args))
			    (draw-circle number number number :STREAM stream))
	       :PROMPT "Graphics Menu"))

(define-demo demo-memoized-make-table-font-changes ()
  (clear-history)
  (format T "~&Demoing Memoized Table with Font Changes~%")
  (set-cursorpos *Standard-Output* 300. 150.)
  (let* ((list (copy-tree '(("AB" "CD") ("ABC" "CDC") ("XX" "YY"))))
	 (fonts (copy-list '((:fix :roman :very-large)
			     (:fix :roman :very-large) (:fix :roman :very-large))))
	 (d (memo ()
	      (make-table ()
		(do ((items list (cdr items))
		     (f fonts (cdr f)))
		    ((null items))
		  (let ((item (first items))
			(style (first f)))
		    (with-character-style (style)
		      (row (() :id item)
			(dolist (i item)
			  (entry (T :VALUE (list style i)
				    :VALUE-TEST #'equal)
			    i))))))))))
    (run-memo d)
    (wait-next-step)
    (setf (second fonts) '(:fix :bold :huge))
    (run-memo d)
    (wait-next-step)
    (setf (first fonts) '(:fix :italic :huge))
    (run-memo d)
    (wait-next-step)
    (setf (third fonts) '(:fix :italic :huge))
    (run-memo d)))

(define-demo demo-simple-table-with-borders ()
  (clear-history)
  (format T "~%Demoing Table with borders around elements.")
  (set-cursorpos *Standard-Output* 300 100)
  (let ((list1 (list 10 20 30))
	(list2 (list 1 2 3)))
    (let ((d (memo ()
	       (make-table (() :column-spacing 16 :row-spacing 10)
		 (dolist (x list1)
		   (row ()
		     (dolist (y list2)
		       (entry (() :VALUE (* x y) :align :center)
			 (with-border (NIL :shape :oval)
			   (princ (* x y)))))))))))
      (run-memo d)
      (wait-next-step)
      (setf (second list1) 4)
      (run-memo d))))

(define-demo demo-make-table-after-table ()
  (clear-history)
  (format T "~&Demoing Table After Table~%")
  (set-cursorpos *Standard-Output* 300. 150.)
  (let* ((list (copy-tree '(("AB" "CD") ("ABC" "CDC") ("XX" "YY"))))
	 (d (memo ()
	      (make-table ()
		(dolist (item list)
		  (row (T :ID item)
		    (dolist (i item)
		      (entry (T :value i))))))
	      ;;(fresh-line)
	      (increment-cursorpos *Standard-Output* 0 20.)
	      (make-table ()
		(dolist (item list)
		  (row (T :ID item)
		    (dolist (i item)
		      (entry (T :value i)))))))))
    (run-memo d)
    (wait-next-step)
    (setf (cdr list) (cddr list))
    (run-memo d)
    (wait-next-step)
    (setf (cdr list) (cons (copy-list '("Foo" "Bar")) (cdr list)))
    (run-memo d)
    (wait-next-step)
    (setf (caar list) "ZZ")
    (run-memo d)))



#+ignore
(define-demo demo-make-table-5 ()
  (declare (special *l*))
  (fresh-line *Standard-Output*)
  (clear-history *Standard-Output*)
  (setq *l* (copy-list '("Old Top" "Old Middle" "Old Bottom")))
  (let ((displayer (memo ()
		     (dolist (thing *l*)
		       (memoize (:id thing :value thing)
			 (display-as (:TYPE 'STRING :OBJECT thing)
			   (princ thing)))
		       (terpri)))))
    (run-memo displayer)
    (wait-next-step)
    (push "New Top" *l*)
    (run-memo displayer)
    (wait-next-step)
    (pop (cddr *l*))
    (run-memo displayer)))

(define-demo demo-double-table (&AUX (x1 (copy-list '(1 2 3)))
				     (y1 (copy-list '(1 2 3)))
				     (x2 (copy-list '(1 2 3)))
				     (y2 (copy-list '(1 2 3))))
  (clear-history)
  (format T "~&Demoing Recursive Memoized Tables.~%")
  (set-cursorpos *Standard-Output* 300. 100.)
  (let ((d (memo ()
	     (make-table (() :row-spacing 10)
	       (dolist (x x1)
		 (row ()
		   (dolist (y y1)
		     (entry ()
		       (make-table ()
			 (dolist (x x2)
			   (row ()
			     (dolist (y y2)
			       (entry (() :VALUE (list x y) :VALUE-TEST #'EQUAL)
				 (princ (* x y)))))))))))))))
    (run-memo d)
    (wait-next-step)
    (setf (second x2) 3)
    (run-memo d)
    (wait-next-step)
    (setf (second x2) 2)
    (run-memo d)
    (wait-next-step)
    (setf (second x2) 3)
    (run-memo d)))





(define-demo demo-displays-and-queries ()
  (clear-history)
  (format T "~&Demoing Simple Displays and Queries~%")
  (make-table-from-sequence '(1 2 3 4 5 6 7 8 9) :TYPE 'INTEGER)
  (format T "~2&Demoing Query of (integer 3 6)~%")
  (query '(integer 3 6) :prompt "Enter Integer: ")
  (query '(integer 3 6) :prompt "Enter Integer: ")
  (clear-history)
  (dotimes (x 5)
    (display-as (:type 'INTEGER :object (1+ x))
      (draw-circle (+ 10 (* 50 x)) (+ 10 (* 50 x)) (* 10 (1+ x)))))
  (query 'integer :PROMPT "Enter Integer, (Note circles are sensitive): ")
  (clear-history)
  (format T "~&Demoing Type Transform \"String-to-integer\".~%")
  (with-character-style ('(NIL NIL :smaller))
    (print "(define-type-transform string-to-integer
		(string
		  integer
		  :test
		  ((data) (and (stringp data)
			       (> (length data) 0)
			       (every #'digit-char-p data))))
		(object)
  (let ((value (read-from-string object)))
    value))")
    (format T "~2%Displaying the following as strings:~%")
    (make-table-from-sequence '("abcde" "1234" "why-me" "Are-you-enjoying-the-demo" "4325")
			      :TYPE 'STRING))
  (format T "~%Now querying for an Integer.~%Please Select an integer with the mouse.")
  (query 'integer))

(define-type-transform string-to-integer
		       (string
			 integer
			 :test
			 ((data) (and (stringp data)
				      (> (length (the string data)) 0)
				      (every #'digit-char-p data))))
		       (object)
  (let ((value (read-from-string object)))
    value))

#+ignore
(defun demo-make-table-4 (&OPTIONAL (initial 3))
  (let ((number initial))
    (dotimes (x 2)
      (clear-history *Standard-Output*)
      (make-table (NIL ;;:equalize-column-widths T	;:inter-row-spacing 20
			  :multiple-columns T
			  :column-spacing 10	;:extend-height T
			  )
	(row () "First" "Second" "Third")
	(dotimes (x number)
	  (row ()
	    (entry (T)		; :align-x :center
	      (display-as (:TYPE 'integer :OBJECT x)
		(format T "First ~D" x)))
	    (entry ()
	      (display-as (:type 'integer :single-box T :object (+ x 2))
		(format T "Second ~D" x)
		(multiple-value-bind (x y)
		    (read-cursorpos *Standard-Input*)
		  (set-cursorpos *Standard-Output* (+ x 30) y ))
		(draw-circle 100 100 40 :filled NIL)))
	    (entry (T)
	      (format T "Third ~D" x)))))
      (let ((integer (query '(integer 1 10))))
	(when (plusp integer)
	  (setq number (+ integer 2)))))))




;;; 6/27/90 (sln) Because of the following warning, I'm removing the
;;; binding &SETQing of VALUE2 & VALUE-3.  I'm keeping, however, the forms
;;; to which the value of VALUE2 & VALUE-3 is SET since it appears to have
;;; the intentional side-effect of querying the user.

;;; While compiling DEMO-QUERYING-VALUES
;;; Warning: Variable VALUE2 is bound but not referenced
;;; Warning: Variable VALUE-3 is bound but not referenced
(define-demo demo-querying-values (&optional (own-window T))
  (let ((value1 NIL) (value2 NIL) (foo 0) (value-3 4))
    (declare (ignore value2 value-3))
    (querying-values (*Query-IO* :OWN-WINDOW own-window)
      (setq value1 (query '((alist-member :ALIST (("Fine" . T)("Not so good" . NIL))))
			   :PROMPT "How are you" :DEFAULT T))
      (command-button (*Query-IO*) "Click Here to Change Menu"
	(incf foo))
      (terpri *Query-IO*)
      (memo-write-string "This is an example of text that stays still." *Query-Io*)
      (terpri *Query-IO*)
      (query '((alist-member :ALIST (("Fine" . T)("Not so good" . NIL))))
	     :prompt "How am I" :default T)
;      (setq value2 (query '((alist-member :ALIST (("Fine" . T)("Not so good" . NIL))))
;			   :prompt "How am I" :default T))
      (memoize (:stream *Query-IO* :value T)
	(format *Query-IO* "Hello "))
      (memoize (:stream *Query-IO* :value value1)
	(format *Query-IO* "~A" (if value1 "That's Great" "Sorry to Hear that"))
	(terpri *Query-IO*))
      (when (> foo 2)
	(query 'integer :prompt "Give me another number " :default 4)
;	(setq value-3 (query 'integer :prompt "Give me another number " :default 4))
	(memoize (:stream *Query-IO* :value foo)
	  (format *Query-Io* "~&The value of Foo is ~D.~%" foo))))))

(define-demo demo-tracking-mouse ()
  (clear-history)
  (with-output-filling ()
    (format T "~&Demoing Tracking Mouse.  Press the left button and drag the mouse to draw.")
    (format T "~%Press the Right Button to Quit."))
  (tracking-mouse (())
    (:WHO-LINE-DOCUMENTATION-STRING () "Left: Draw Point, Right: Quit")
    (:MOUSE-CLICK (button x y)
     (case button
       (#.*Mouse-Left*
	(draw-rectangle x y (+ x 2) (+ y 2)))
       (#.*Mouse-Middle*
	(setq *Consume-Extra-Mouse-Motion-Events-P*
	      (not *Consume-Extra-Mouse-Motion-Events-P*)))
       (#.*Mouse-Right* (return NIL))))
    (:MOUSE-MOTION-HOLD (x y)
     (draw-rectangle x y (+ x 2) (+ y 2)))))



(defun run-demos (&OPTIONAL (set-font T) &AUX (demos *Demos*))
  (when set-font
    (set-window-style *standard-output* '(:fix :roman :very-large)))
  (loop
    (unless demos (setq demos *Demos*))
    (let ((demo (first demos)))
      (funcall demo)
      ;;(clear-history *Standard-Output*)
      (format T "~%Next Demo, N, P, S, Q")
      (case (wait-next-step)
	((#\n #\N) (pop demos))
	((#\p #\P) (setq demos (nthcdr (max 0 (- (length *Demos*) (length demos) 1))
			      *Demos*)))
	((#\Q #\q) (return T))))))



(eval-when (compile load eval)
(define-program-framework demo
  :SELECT-KEY
  #\D
  :COMMAND-DEFINER
  T
  :COMMAND-TABLE
  (:INHERIT-FROM '("colon full command" "standard arguments" "standard scrolling")
   :KBD-ACCELERATOR-P NIL)
  :TOP-LEVEL (demo-top-level)
  :STATE-VARIABLES ()
  :PANES
  ((TITLE :TITLE
          :REDISPLAY-STRING "Express Windows Demos by Liszt Programming Inc."
	  :style (:FIX :ITALIC :HUGE)
          :HEIGHT-IN-LINES 1
          :REDISPLAY-AFTER-COMMANDS NIL)
   (MENU :COMMAND-MENU :MENU-LEVEL :TOP-LEVEL)
   (DISPLAY :DISPLAY :STYLE (:fix :roman :huge)
	    :MARGIN-COMPONENTS
	    '((margin-scroll-bar :margin :LEFT :visibility :if-needed)))
   (code-DISPLAY :DISPLAY
		 :STYLE (:fix :roman :large)
		 :LABEL "Demonstration Code"
		 :MARGIN-COMPONENTS
		 '((margin-scroll-bar :margin :LEFT :visibility :if-needed)
		   (margin-scroll-bar :margin :BOTTOM :visibility :if-needed)))
   (demo-name-DISPLAY :DISPLAY
		      :STYLE (:fix :roman :very-large)
		      :LABEL "List of Demonstrations"
		      :MARGIN-COMPONENTS
		      '((margin-scroll-bar :margin :LEFT :visibility :if-needed)))
   (INTERACTOR :INTERACTOR :MARGIN-COMPONENTS
	       '((margin-scroll-bar :margin :LEFT :visibility :if-needed))))
  :CONFIGURATIONS
  '(#+ignore
    (main
      (:LAYOUT (main :COLUMN TITLE MENU DISPLAYS INTERACTOR)
       (DISPLAYS :ROW DISPLAY rest-of-display)
       (rest-of-display :COLUMN demo-name-display code-display))
      (:SIZES
	(MAIN (TITLE 1 :LINES) (MENU :ASK-WINDOW) (INTERACTOR 5 :LINES)
	      :THEN (displays :EVEN))
	(DISPLAYS (display .6) :THEN (rest-of-display :EVEN))
	(rest-of-display (demo-name-display :EVEN) (code-display :EVEN)))
      )
    (main
      (:LAYOUT (main :COLUMN TITLE MENU DISPLAY STUFF INTERACTOR)
       (STUFF :ROW demo-name-display code-display))
      (:SIZES
	(MAIN (TITLE 1 :LINES) (MENU :ASK-WINDOW) (INTERACTOR 5 :LINES)
	      (display .5)
	      :THEN (stuff :EVEN))
	(stuff (code-display .65) :THEN (demo-name-display :EVEN)))
      ))))


(defun demo-top-level (program)
  (com-write-code-names program)
  (default-command-top-level program :prompt "-> "))


(define-type demo (())
  :PARSER ((stream)
	   (query `(member . ,*Demos*) :STREAM stream :PROMPT NIL)))

(defun com-write-code-names (program)
  (let* ((program-frame (program-frame program))
	 (*Standard-Output* (get-pane program-frame 'DEMO-NAME-DISPLAY)))
    (clear-history)
    (with-output-truncation ()
      (dolist (demo *Demos*)
	(display-as (:TYPE 'DEMO :OBJECT demo)
	  (let ((name (make-pretty-name demo)))
	    (princ (if (and (> (length name) 4)
			    (string-equal "Demo " name :END1 5 :END2 5))
		       (subseq name 5)
		       name))))
	(terpri)))))

(define-mouse-command run-demo-command
		      (DEMO
			:GESTURE '(:LEFT :MIDDLE :RIGHT)
			:DOCUMENTATION "Run Demo")
		      (demo-name)
  `(com-run-demo ,demo-name))

(define-demo-command (com-run-demo) ((demo 'DEMO))
  (if (not (fboundp demo))
      (format T "Demo ~A not implemented." demo)
      (let ((*Standard-Output* (get-program-pane 'DISPLAY))
	    (code-pane (get-program-pane 'CODE-DISPLAY)))
	(unless (eq *Last-Demo-Run* demo)
	  (clear-history code-pane)
	  (with-output-truncation (code-pane)
	    (when (get demo :CODE-STRING)
	      (princ (get demo :CODE-STRING) code-pane)))
	  (setq *Last-Demo-Run* demo))
	(funcall demo))))


(define-demo-command (com-rerun-last-demo :menu-accelerator T) ()
  (when *Last-Demo-Run*
    (com-run-demo *Last-Demo-Run*)))

(define-demo-command (com-exit-demos :menu-accelerator "Exit") ()
  (exit-program))



(define-demo spread-sheet (&OPTIONAL (size 10))
  (clear-history)
  (let ((rows NIL))
    (dotimes (x size)
      (push (make-list size :initial-element x) rows))
    (setq rows (nreverse rows))
    (querying-values (*Standard-Output* :Label "Fill In values and look at results"
					:character-style '(:fix :roman :huge))
      (make-table ()
	(let ((column-sums (make-list size :initial-element 0))
	      (row-number 0))
	  (dolist (row rows)
	    (let ((row-sum 0))
	      (row ()
		(do ((cells row (cdr cells))
		     (csums column-sums (cdr csums))
		     (cell-number 0 (1+ cell-number)))
		    ((null cells))
		  (entry (()) ; :VALUE (first cells)
		    (setf (first cells)
			  (query 'INTEGER :DEFAULT (first cells)
				 :STREAM *Standard-Output*
				 :PROMPT NIL
				 :QUERY-IDENTIFIER
				 (format NIL "~D-~D" row-number cell-number))))
		  (incf row-sum (first cells))
		  (incf (first csums) (first cells)))
		(entry (() :VALUE row-sum))))
	    (incf row-number))
	  (row ()
	    (let ((total 0))
	      (dolist (sum column-sums)
		(incf total sum)
		(entry (() :VALUE sum)))
	      (entry (() :VALUE total)))))))))


(defvar *Slide-Show-Window* NIL)

(defvar *Slides*
	'(("Express Windows"
	   "by"
	   "Liszt Programming Inc")
	  ("Portable High Level Window Toolkit"
	   "* Implemented in Common Lisp"
	   "* Drives X11 Windows with CLX"
	   "* Available on Standard Hardware")
	  ("Express Windows"
	   "Presentation System"
	   "Command Processor"
	   "Table Generation"
	   "Menu Generation"
	   "Automatic Display Management")))

(defvar *Font-for-this* '(:fix :BOLD-ITALIC :huge))

(define-demo slide-show ()
;;  (unless *Slide-Show-Window*
    (setq *Slide-Show-Window*
	  (make-window 'WINDOW :CHARACTER-STYLE *Font-for-this*
		       :BLINKER-P NIL
		       :LABEL "Slide Show"));)
  (unwind-protect
      (let ((slides NIL))
	(expose-window *Slide-Show-Window*)
	(flet ((next-slide ()
		 (setq slides (cdr slides))
		 (if (not slides) (setq slides *Slides*))
		 (first slides))
	       (previous-slide ()
		 (if (or (not slides) (eq *Slides* slides))
		     (setq slides (last *Slides*))
		     (do ((s *Slides* (cdr s)))
			 ((null s) (error "No Slide"))
		       (when (eq (cdr s) slides)
			 (setq slides s))))
		 (first slides)))
	  (do ((current-slide (next-slide)))
	      (())
	    (display-slide current-slide *Slide-Show-Window*)
	    (case (query 'character)
	      ((#\Q #\q #\Control-C #\ESC) (return NIL))
	      ((#\P #\p #\Rubout) (setq current-slide (previous-slide)))
	      ((#\Y #\y #\N #\n #\Space #\Newline) (setq current-slide (next-slide)))))))
    (deexpose-window *Slide-Show-Window*)))

(defun display-slide (slide window)
  ;; for now a slide is a list of text strings.
  (clear-history window)
  (flet ((ter (n window) (dotimes (x n)
			   #+symbolics (declare (ignore x))	;6/27/90 (sln)
			   (terpri window))))
    (ter 2 window)
    (dolist (string slide)
      (with-centered-display (window)
	(simple-princ string window)
	(ter 2 window)))))



(define-demo demo-order-ice-cream-sundae (&optional (own-window T))
  (let ((flavors (list 'vanilla)) (mixins NIL) (container 'cup))
    (querying-values (*Query-IO* :OWN-WINDOW own-window
				 :NEAR-MODE '(:point 400 200)
				 :LABEL "Pick your Ice Cream Sundae Options")
      (with-character-style ('(:fix :bold-italic :large) *Query-Io*)
	(setq flavors
	      (query '(alist-subset
			(("Vanilla" :value vanilla :X 100 :Y 25)
			 ("Chocolate" :value chocolate :X 175 :Y 45)
			 ("Strawberry" :value strawberry :X 250 :Y 25)
			 ("Orange" :value orange :X 325 :Y 45)
			 ("Coffee" :value coffee :X 400 :Y 25))
			1)
		     :PROMPT "Pick at least one flavor"
		     :DEFAULT flavors))
	(set-cursorpos *Query-Io* 0 90)
	(setq mixins
	      (query '(alist-subset (mixed-nuts raisins chips jimmies) NIL 2)
		     :PROMPT "Pick up to Two Mixins"
		     :DEFAULT mixins))
	(terpri *Query-Io*)
	(setq container
	      (query '(alist-member (cup cone hand))
		     :PROMPT "Which would you prefer"
		     :DEFAULT container))
	(case container
	  (cone
	    (memo-write-string "Take Plenty of Napkins for your messy cone!"
			       *Query-Io*))
	  (hand
	    (memo-write-string "Take Plenty of Napkins for your messy hands!"
			       *Query-Io*)
	    (terpri *Query-Io*)
	    (memo-write-string "Take Plenty of Napkins for your messy hands!"
			       *Query-Io*)))))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
