;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 90 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)


(defvar *Program-Frameworks* NIL)

(defvar *Program-Frame* NIL)
(defvar *Program* NIL)

(defvar *Dispatch-Mode* :FORM-PREFERRED)


#+(and symbolics (not x))
(defvar *IO-Buffer*)

(setf (get :command-menu 'make-window) 'make-command-menu)
(defun make-command-menu (framework framework-instance program pane-name
			  &REST other-keys
			  &KEY (menu-level '(:top-level))
			  (columns NIL columns-p)
			  (rows NIL rows-p)
			  &ALLOW-OTHER-KEYS)
  (declare (ignore program framework framework-instance))
  ;; when making a command menu we get our menu items from two source.
  ;; first an option to the pane, second from menu-accelerators in the command table.
  ;; first use the pane option, because this will give us some fixed ideas of ordering the
  ;; items in the menu.  Then add onto end any menu-accelerators.
  ;; if the :COLUMS is a list of list of strings.  Then each sublist represents a different
  ;; column.  Be clever.  Of course, presenting a string, will not make the menu mouse
  ;; sensitive unless there is an matching command menu accelerator.
  (let ((window (apply #'make-window 'PROGRAM-COMMAND-MENU :BLINKER-P NIL
		       :LABEL (getf other-keys :LABEL)
		       :MARGIN-COMPONENTS (getf other-keys :MARGIN-COMPONENTS)
		       other-keys)))
    (setf (window-name window) pane-name
	  (program-command-menu-orientation window)
	  (cond (columns (list :columns columns))
		(rows (list :rows rows))
		(rows-p :default-rows)
		(columns-p :default-columns))
	  (program-command-menu-level window) menu-level)
    window))

(setf (get :title 'make-window) 'make-title-pane)
(defun make-title-pane (framework framework-instance program pane-name
			&REST other-keys
			&KEY character-style redisplay-string redisplay-function
			&ALLOW-OTHER-KEYS)
  (declare (ignore framework-instance program pane-name))
  (let ((window (apply #'MAKE-WINDOW 'PROGRAM-TITLE-PANE :BLINKER-P NIL
		       :LABEL (getf other-keys :LABEL)
		       :MARGIN-COMPONENTS (getf other-keys :MARGIN-COMPONENTS)
		       other-keys)))
    (when (and redisplay-string redisplay-function)
      (format T "Error in pane ~A, using both redisplay string and function options."))
    (when (and (not redisplay-string) (not redisplay-function))
      (setq redisplay-string (program-framework-pretty-name framework)))
    (setf (program-pane-redisplay-string window) redisplay-string
	  (program-pane-redisplay-function window) redisplay-function)
    (unless (or character-style (getf other-keys :style)
		(getf other-keys :default-style)
		(getf other-keys :default-character-style))
      (set-window-style window '(:FIX :ITALIC :VERY-LARGE)))
    window))

(setf (get :display 'make-window) 'make-display-pane)
(defun make-display-pane (framework framework-instance program pane-name
			  &REST other-keys
			  &KEY redisplay-string redisplay-function incremental-redisplay
			  &ALLOW-OTHER-KEYS)
  (declare (ignore framework pane-name framework-instance))
  (when (and redisplay-string redisplay-function)
      (format T "Error in pane ~A, using both redisplay string and function options."))
  (let ((program-pane
	  (apply #'make-window (or (getf other-keys :flavor) 'PROGRAM-DISPLAY-PANE)
		 :BLINKER-P NIL
		 :LABEL (getf other-keys :LABEL)
		 :MARGIN-COMPONENTS (getf other-keys :MARGIN-COMPONENTS)
		 other-keys)))
    (setf (program-pane-redisplay-string program-pane) redisplay-string)
    (if redisplay-function
	(if incremental-redisplay
	    (setf (program-pane-incremental-redisplayer program-pane)
		  (memo (stream) (funcall redisplay-function program stream)))
	    (setf (program-pane-redisplay-function program-pane) redisplay-function)))
    program-pane))


(setf (get :query-values 'make-window) 'make-query-values-pane)
(setf (get :accept-values 'make-window) 'make-query-values-pane)
(defun make-query-values-pane (framework framework-instance program pane-name
			       &REST other-keys
			       &KEY query-values-function
			       &ALLOW-OTHER-KEYS)
  (declare (ignore program framework pane-name framework-instance))
  (when (getf other-keys :ACCEPT-VALUES-FUNCTION)
    (setq query-values-function (getf other-keys :ACCEPT-VALUES-FUNCTION)))
  (setq query-values-function query-values-function)
  (let ((program-pane
	  (apply #'make-window 'PROGRAM-QUERY-VALUES-PANE :BLINKER-P NIL
		 :LABEL (getf other-keys :LABEL)
		 :MARGIN-COMPONENTS (getf other-keys :MARGIN-COMPONENTS)
		 other-keys)))
    (setf (program-query-values-pane-query-values-function program-pane)
	  query-values-function)
    program-pane))

(setf (get :interactor 'make-window) 'make-interactor-pane)
(setf (get :listener 'make-window) 'make-interactor-pane)
(defun make-interactor-pane (framework framework-instance program pane-name
			     &REST other-keys
			     &KEY character-style
			     &ALLOW-OTHER-KEYS)
  (declare (ignore framework framework-instance program pane-name))
  (unless (or character-style (getf other-keys :default-style)
	      (getf other-keys :style)
	      (getf other-keys :default-character-style))
    (setq character-style '(:FIX :ITALIC :LARGE)))
  (apply #'make-window 'PROGRAM-PANE :DEFAULT-STYLE character-style
	 :MARGIN-COMPONENTS (getf other-keys :MARGIN-COMPONENTS)
	 :LABEL (getf other-keys :LABEL)
	 other-keys))


(defun random-make-other-pane (framework framework-instance program pane-name
			       &REST other-keys &KEY character-style &ALLOW-OTHER-KEYS)
  (declare (ignore framework framework-instance program pane-name character-style))
  (apply #'make-window 'PROGRAM-PANE :BLINKER-P NIL
	 :LABEL (getf other-keys :LABEL)
	 :MARGIN-COMPONENTS (getf other-keys :MARGIN-COMPONENTS)
	 other-keys))

(defun make-framework-pane (pane framework framework-instance program superior)
  (let* ((window (apply (or (get (second pane) 'make-window) 'random-make-other-pane)
			framework framework-instance program (first pane)
			:SUPERIOR superior (cddr pane)))
	 (character-style (window-character-style window)))
    (set-window-style window character-style)
    window))





(defvar *Pane-Keywords-to-be-evaled*
	'(:REDISPLAY-FUNCTION
	   :REDISPLAY-STRING :MARGIN-COMPONENTS
	   :ROWS :COLUMNS
	   :QUERY-VALUES-FUNCTION :ACCEPT-VALUES-FUNCTION
	   ))

(defun process-panes-for-proper-eval (panes)
  (cons 'list
	(mapcar #'(lambda (pane)
		    ;; make sure things in the pane that are supposed to be eval'd
		    ;; get that way.
		    (if (not (consp pane))
			`',pane
			;; what keywords should be evaled
			(do ((args (cddr pane) (cddr args))
			     (result NIL)
			     (any-evals NIL))
			    ((null args)
			     (if any-evals
				 (list* 'list `',(first pane) (second pane) (nreverse result))
				 `',pane))
			  (push (first args) result)
			  (if (member (first args) *Pane-Keywords-to-be-evaled*)
			      (progn (setq any-evals T)
				     (push (second args) result))
			      (push `',(second args) result)))))
		panes)))

(defmacro define-program-framework (name &KEY pretty-name command-definer command-table
				    (top-level '(default-command-top-level))
				    command-evaluator (panes '(main listener))
				    selected-pane query-io-pane terminal-io-pane label-pane
				    configurations state-variables
				    (selectable t) select-key system-menu size-from-pane
				    (inherit-from 'basic-frame-program)
				    other-defflavor-options)
  (warn-unimplemented-args system-menu NIL size-from-pane NIL
			   other-defflavor-options NIL selectable T)
  `(progn 'compile
	  (define-program-framework-internal
	    ',name ,select-key
	    ',(or pretty-name (make-pretty-name name))
	    (list . ,command-table)
	    ',top-level
	    ',command-evaluator
	    ,(process-panes-for-proper-eval panes)
	    ,configurations ',state-variables
	    ',selected-pane ',query-io-pane ',terminal-io-pane ',label-pane)
	  #+EW-CLOS
	  (defclass
	    ,name
	    (,inherit-from)
	    ,(mapcar #'(lambda (variable)
			 (if (consp variable)
			     `(,(first variable)
			       :accessor ,(intern (lisp:format NIL "~A-~A" name (first variable))
						  (symbol-package name))
			       :initarg ,(intern (string (first variable)) :keyword)
			       :INITFORM ,(second variable))
			     variable))
		     state-variables))
	  #+EW-CLOS
; 4/30/90 (SLN)
;	  (defmethod print-object ((self ,name) stream &REST ignore)
	  (defmethod print-object ((self ,name) stream)
;	    (declare (ignore ignore))
	    (format stream "#<Program ~A>" ',name))
	  #-EW-CLOS
	  (defstruct (,name (:INCLUDE ,inherit-from)
		      (:PRINT-FUNCTION ,(intern (format NIL "PRINT-~A" name)
						(symbol-package name))))
	    ,(mapcar #'(lambda (variable)
			 (if (consp variable)
			     `(,(first variable) ,(second variable))
			     variable))
		     state-variables))
	  #-EW-CLOS
	  (defun ,(intern (format NIL "PRINT-~A" name)
			  (symbol-package name))
		 (self stream &REST ignore)
	    (declare (ignore ignore))
	    (format stream "#<Program ~A>" ',name))
	  ,(if command-definer
	       `(defmacro ,(if (eq T command-definer)
			       (intern (lisp-format NIL "DEFINE-~A-COMMAND" name)
				       (symbol-package name))
			       command-definer)
			  ((name &REST options &KEY keyboard-accelerator
				 (echo t) menu-accelerator (menu-level '(:top-level))
				 (menu-documentation t)
				 menu-documentation-include-defaults
				 provide-output-destination-keyword &ALLOW-OTHER-KEYS)
			   arglist &BODY body)
		  #+excl options #-excl (declare (ignore options)) 
		  ;;6/27/90 (sln) Franz complains about OPTIONS being used.
		  ;; Must be in the macroexpansion of DEFMACRO.
		  (list* 'define-program-command
			 (list name ',name
			       :KEYBOARD-ACCELERATOR keyboard-accelerator
			       :ECHO echo :menu-accelerator menu-accelerator
			       :menu-level menu-level
			       :menu-documentation menu-documentation
			       :menu-documentation-include-defaults
			       menu-documentation-include-defaults
			       :provide-output-destination-keyword
			       provide-output-destination-keyword)
			 arglist body)))))

(defun find-program-framework (name)
  (find name *Program-Frameworks* :KEY #'program-framework-name :TEST #'EQUALP))

(defun program-name (program)
  (and program
       (program-framework-name
	 (program-framework-instance-program-framework (program-frame program)))))

(defun define-program-framework-internal (name select-key pretty-name
					  command-table top-level command-evaluator
					  panes configurations state-variables
					  selected-pane query-io-pane
					  terminal-io-pane label-pane)
  (let ((program-framework (find-program-framework name)))
    (unless program-framework
      ;; figure out if we need to add a query-values command table here.
      (when (and (listp command-table)
		 (find-if #'(lambda (pane-info)
			      (and (consp pane-info) (member (second pane-info)
							     '(:query-values :accept-values))))
			  panes))
	;; make sure we have "Query-Values"
	(if (getf command-table :INHERIT-FROM)
	    (setf (getf command-table :INHERIT-FROM)
		  (pushnew "QUERY-VALUES" (getf command-table :INHERIT-FROM) :TEST #'EQUALP))
	    (setq command-table (list* :INHERIT-FROM "QUERY-VALUES" command-table))))
      (let ((command-table (apply #'make-command-table
				  (string name) :IF-EXISTS NIL command-table)))
	(push (setq program-framework (make-instance 'program-framework
						     :NAME name
						     :COMMAND-TABLE command-table))
	      *Program-Frameworks*)))
    (setf (program-framework-select-key program-framework) select-key
	  (program-framework-top-level program-framework) top-level
	  (program-framework-command-evaluator program-framework) command-evaluator
	  (program-framework-state-variables program-framework) state-variables
	  (program-framework-pretty-name program-framework) pretty-name
	  (program-framework-panes program-framework) panes
	  (program-framework-configurations program-framework) configurations
	  (program-framework-selected-pane program-framework) selected-pane
	  (program-framework-query-io-pane program-framework) query-io-pane
	  (program-framework-terminal-io-pane program-framework) terminal-io-pane
	  (program-framework-label-pane program-framework) label-pane)
    #+(and symbolics (not x))
    (tv:add-select-key select-key `(select-program-framework ',name) pretty-name NIL NIL)
    #+ignore ;; to be implemented later.
    (dolist (instance (program-framework-program-instances program-framework))
      (if (y-or-n-p "Update Program Framework Instance?")
	  (update-program-instance instance)))))

(defun get-program-streams (program)
  (declare (values *Standard-Input* *Standard-Output* *Query-Io* *Terminal-Io*
		   *trace-output* *error-output*))
  ;; look in panes
  (let* ((*Program-Frame* (program-frame program))
	 (program-framework (program-framework-instance-program-framework *Program-Frame*))
	 (panes (program-framework-panes program-framework)))
    (labels ((find-pane-name (types)
	       (dolist (pane-type types)
		 (let ((pane (find pane-type panes :KEY #'second)))
		   (and pane (return (first pane))))))
	     (find-pane (default-pane types)
	       (unless default-pane (setq default-pane (find-pane-name types)))
	       (or (get-program-pane default-pane)
		   (error "Couldn't find pane of type ~A." types))))
      ;; order for standard input search is listener interactor display
      ;;  - based on selected-pane
      ;; order for standard output search is display listener interactor
      ;; order for query-io search is listener interactor display
      ;; order for terminal-io is listener display interactor
      (let ((selected-pane (find-pane (program-framework-selected-pane program-framework)
				      '(:listener :interactor :display)))
	    (input-pane (find-pane (program-framework-selected-pane program-framework)
				   '(:listener :interactor :display)))
	    (query-io-pane (find-pane (program-framework-query-io-pane program-framework)
				      '(:listener :interactor :display)))
	    (terminal-io-pane (find-pane (program-framework-terminal-io-pane program-framework)
					 '(:listener :interactor :display))))
	(values input-pane selected-pane query-io-pane terminal-io-pane
		selected-pane selected-pane)))))


(defun make-program-instance (program-framework)
  (let* ((frame (make-instance 'program-framework-instance
			       :PROGRAM-FRAMEWORK program-framework))
	 (object (make-instance (program-framework-name program-framework)
				:FRAME frame)))
    (values object frame)))

(defun create-program-framework-instance (program-framework)
  (multiple-value-bind (instance frame) (make-program-instance program-framework)
    (setq *Program* instance)
    (let (#+(and symbolics (not x)) (*Io-Buffer* (tv:make-io-buffer 100)))
      (let ((superior-window
	      (make-window 'PROGRAM-FRAME :BORDERS 0
			   :LABEL NIL
			   :BLINKER-P NIL :MORE-P NIL)))
	(setf (program-frame-program-framework-instance superior-window)
	      instance)
	(setf (program-framework-instance-root-window frame) superior-window)
	(dolist (pane (program-framework-panes program-framework))
	  (push (cons pane (make-framework-pane pane program-framework frame instance
						superior-window))
		(program-framework-instance-windows frame)))
	(multiple-value-bind (left top right bottom)
	    (window-edges superior-window)
	  (setup-configuration
	    frame (caar (program-framework-configurations program-framework))
	    0 0 (%- right left) (%- bottom top))))
      (push instance (program-framework-program-instances program-framework)))))

(defun find-debug-pane (instance)
  (let ((panes (program-framework-instance-current-panes (program-frame instance))))
    ;; basically find the window with the biggest area.
    (let ((area-so-far 0)
	  (biggest-pane NIL))
      (dolist (pane panes)
	(multiple-value-bind (width height)
	    (window-size pane)
	  (declare (fixnum width height))
	  (let ((area (* width height)))
	    (when (> area area-so-far)
	      (setq area-so-far area
		    biggest-pane pane)))))
      biggest-pane)))

(defun deexpose-program-framework-instance (instance)
  (deexpose-window (program-framework-instance-root-window instance)))

(defun expose-program-framework-instance (instance)
  (let ((panes (program-framework-instance-current-panes instance)))
    (expose-window (first panes))
    (dolist (pane (reverse panes))
      ;;(push pane *Windows-Exposure-Ignores*)
      (expose-window pane))
    (dolist (pair (program-framework-instance-windows instance))
      (unless (member (cdr pair) panes)
	(deexpose-window (cdr pair))))
    ;(expose-window (program-framework-instance-root-window instance))
    #+(and symbolics (not x))
    (scl:send (program-framework-instance-root-window instance) :EXPOSE)
    #+CLX
    (xlib::set-window-priority
      :ABOVE (window-real-window (program-framework-instance-root-window instance)) NIL)
    #+X
    (expose-window (program-framework-instance-root-window instance))
    (select-window (first panes))
    (dolist (pane (reverse panes))
      (typecase pane
	(PROGRAM-COMMAND-MENU
	  (if (empty-window-p pane)
	      (display-program-command-menu pane)
	      (redraw-window pane)))
	(PROGRAM-TITLE-PANE
	  (display-title-pane pane T))
	(program-display-pane
	  (display-display-pane pane T))
	(program-query-values-pane
	  (display-query-values-pane pane T))
	(PROGRAM-PANE (redraw-window pane))))
    (values (window-real-window (first panes)) (first panes))))

(defun program-redisplay (program-instance)
  (let ((panes (program-framework-instance-current-panes (program-frame program-instance))))
    (dolist (pane (reverse panes))
      (typecase pane
	(PROGRAM-COMMAND-MENU
	  (when (empty-window-p pane)
	    (display-program-command-menu pane)))
	(PROGRAM-TITLE-PANE
	  (display-title-pane pane NIL))
	(program-query-values-pane
	  (display-query-values-pane pane NIL))
	(program-display-pane
	  (display-display-pane pane NIL))))))

(defun program-refresh (program-instance)
  (let ((panes (program-framework-instance-current-panes (program-frame program-instance))))
    (dolist (pane (cdr panes))
      (redraw-window pane))))

(defun select-program (program)
  (unless (lisp:typep program 'BASIC-FRAME-PROGRAM)
    (error "~A is not a valid program to select." program))
  (setq *Program* program)
  (setq *Program-Frame* (program-frame program))
  (expose-program-framework-instance (program-frame program)))

(defun run-program (framework-name)
  (unless *X-Display* (initialize-window-system))
  (with-safe-windows
    (let ((framework (find-program-framework framework-name)))
      (if (not framework)
	  (format T "~&Unable to find program ~A." framework-name)
	  (progn
	    (let ((instances (program-framework-program-instances framework)))
	      (unless instances
		(create-program-framework-instance framework)))
	    (let ((*Command-Table* (program-framework-command-table framework))
		  (program (first (program-framework-program-instances framework))))
	      (unless (lisp:typep program 'BASIC-FRAME-PROGRAM)
		(error "Not Able to find Program in framework."))
	      (let ((*Program* program)
		    (*Program-Frame* (program-frame program)))
		(multiple-value-bind
		  (*Standard-Input*		; *Standard-Input*
		   *Standard-Output*		; *Standard-Output*
		   *Query-Io*			; *Query-Io*
		   ignore			; *Terminal-Io*
		   *Trace-Output*		; *Trace-Output*
		   ignore1)			; *Error-Output*
		    (get-program-streams program)
		  (declare (ignore ignore1))
		  (apply (first (program-framework-top-level framework)) program
			 (cdr (program-framework-top-level framework)))))))))))

(defun find-program-window (program-name &REST make-window-options &KEY (create-p T)
			    (activate-p t) (selected-ok T) reuse-test console
			    superior program-state-variables &ALLOW-OTHER-KEYS)
  (declare (ignore program-state-variables superior console reuse-test
		   selected-ok activate-p make-window-options)) ;4/30/90 (SLN)
  (let ((framework-definition (find-program-framework program-name)))
    (unless framework-definition
      (error "Framework ~A Does not Exist." program-name))
    (let ((instances (program-framework-program-instances framework-definition)))
      (cond (instances
	     (program-frame (first instances)))
	    (create-p
	     (program-frame (create-program-framework-instance framework-definition)))
	    (T NIL)))))

(defun find-program-command-table (program)
  (unless (lisp:typep program 'BASIC-FRAME-PROGRAM)
    (error "~A is Not a valid program."))
  (let* ((program-frame (program-frame program))
	 (program-framework (program-framework-instance-program-framework program-frame)))
    (program-framework-command-table program-framework)))

(defun get-pane (frame pane-name)
  (cdr (find pane-name (program-framework-instance-windows frame)
	     :KEY #'caar)))

(defun get-program-pane (pane-name)
  (and *Program-Frame*
       (cdr (find pane-name (program-framework-instance-windows *Program-Frame*)
		  :KEY #'caar))))

(defun program-command-evaluator (program)
  (program-framework-command-evaluator
    (program-framework-instance-program-framework (program-frame program))))

(defstruct constraint-pane
  constraints)

(defstruct (real-pane (:include constraint-pane) (:print-function print-real-pane))
  name
  pane)

(defun print-real-pane (pane stream &REST ignore)
  (declare (ignore ignore))
  (format stream "#<Real Pane ~A>" (real-pane-name pane)))

(defstruct (fictious-pane (:include constraint-pane) (:print-function print-fictious-pane))
  name
  superior
  inferiors
  orientation)

(defun print-fictious-pane (pane stream &REST ignore)
  (declare (ignore ignore))
  (format stream "#<Fictious Pane ~A>" (fictious-pane-name pane)))

(defun setup-configuration (instance configuration-name left top right bottom)
  (setf (program-framework-instance-current-panes instance) NIL)
  (let* ((program-framework (program-framework-instance-program-framework instance))
	 (configuration (cdr (assoc configuration-name
				    (program-framework-configurations program-framework))))
	 (real-pane-names
	   (mapcar #'FIRST (program-framework-panes program-framework)))
	 (width (%- right left))
	 (height (%- bottom top)))
    (unless configuration
      (error "No Configuration ~A exists for program framework ~A"
	     configuration-name (program-framework-pretty-name program-framework)))
    ;; no interpret configuration and set sizes of windows.
    (let ((layout (cdr (assoc :LAYOUT configuration)))
	  (sizes (cdr (assoc :SIZES configuration)))
	  (fictious-panes NIL)
	  (real-panes NIL))
      (flet ((get-fictious-pane (name)
	       (find name fictious-panes :KEY #'fictious-pane-name))
	     (get-real-pane (name)
	       (cdr (find name (program-framework-instance-windows instance)
			  :KEY #'caar))))
	;; first create fictious panes - the ones mentioned in the beginning of a layout list.
	(dolist (layout-item layout)
	  (let ((pane-name (first layout-item)))
	    (cond ((member pane-name real-pane-names)
		   (error "Real Pane ~A mentioned as definition pane in layout ~A."
			  pane-name layout-item))
		  ((get-fictious-pane pane-name)
		   (error "Fictious Pane ~A mentioned twice in layout definition ~A."
			pane-name layout-item))
		(T (push (make-fictious-pane :name pane-name) fictious-panes)))))
      ;; now go through and create hierarchy of fictious panes
      (dolist (layout-item layout)
	(let ((fictious-pane (get-fictious-pane (first layout-item))))
	  (unless (member (second layout-item) '(:ROW :COLUMN))
	    (error "Unknown Orientation ~A in layout ~A, should have been one of ~
			:Row or :Column"
		   (second layout-item) layout-item))
	  (setf (fictious-pane-orientation fictious-pane) (second layout-item))
	  ;; make sure each pane is real
	  (setf (fictious-pane-inferiors fictious-pane)
		(mapcar #'(lambda (pane-name)
			    (cond ((get-real-pane pane-name)
				   (let ((real-pane
					   (make-real-pane :NAME pane-name
							   :pane (get-real-pane pane-name))))
				     (push (real-pane-pane real-pane)
					   (program-framework-instance-current-panes instance))
				     (push real-pane real-panes)
				     real-pane))
				  ((get-fictious-pane pane-name)
				   (setf (fictious-pane-superior (get-fictious-pane pane-name))
					 fictious-pane)
				   (get-fictious-pane pane-name))
				  (T (error "Pane ~A in layout ~A is not mentioned in either ~
						the fictious panes or the real panes."
					    pane-name layout-item))))
			(cddr layout-item)))))
      ;; verify there is a fictious pane with the same name as the configuration
      (let ((top-pane (get-fictious-pane configuration-name)))
	(unless top-pane
	  (error
	    "You must include a layout with the same name as the configuration name of ~A."
	    configuration-name))
	;(setq tt top-pane)
	;; verify that all fictious panes have a superior except for the top pane
	(verify-fictious-pane-superiors fictious-panes top-pane)
	;; make sure hierarchy of rows and columns is right.
	(verify-row-column-layout-of-configuration top-pane)
	;; well We've beaten to death the configuration stuff, now the size stuff.
	;; for a row, all sizes have something to do with the width
	;; for a column, all sizes have something to do with the height.
	(configure-pane-sizes top-pane sizes width height fictious-panes real-panes)
	;; now give them real edges.
	(set-pane-sizes top-pane left top)))))
  (setf (program-framework-instance-configuration instance) configuration-name))

(defun set-constraints-in-pane (constraint pane &OPTIONAL (recursive-p NIL))
  (push constraint (constraint-pane-constraints pane))
  (when (and recursive-p (lisp:typep pane 'FICTIOUS-PANE))
    (dolist (p (fictious-pane-inferiors pane))
      (set-constraints-in-pane constraint p))))

(defun verify-evens-in-groups (groups)
  (flet ((even-item (item) (and (consp item) (eq (second item) :EVEN))))
    (dolist (group groups)
      (when (some #'even-item group)
	;; better be all even and be the last one.
	(unless (eq group (first (last groups)))
	  (error "Even specifications can only be in the last group of specs ~A."
		 groups))
	(unless (every #'even-item group)
	  (error "All or none of the items in a group must be :even specifications ~A."
		 groups))))))

(defun find-pane-sizes (pane sizes)
  (or (cdr (assoc (fictious-pane-name pane) sizes))
      (error "Pane ~A does not have a size definition." (fictious-pane-name pane))))

(defun configure-pane-sizes (top-pane sizes width height fictious-panes real-panes)
  (setf (constraint-pane-constraints top-pane) `((:WIDTH ,width)(:HEIGHT ,height)))
  (configure-pane top-pane sizes fictious-panes real-panes))


(defun ask-window (pane orientation &REST args)
  (unless (lisp:typep pane 'real-pane)
    (error "Cannot handle :ASK-WINDOW on a virtual pane. ~A ~A" pane args))
  (let ((real-pane (real-pane-pane pane)))
    (typecase real-pane
      (PROGRAM-COMMAND-MENU
	;; go ask the menu how big it wants to be.
	(apply #'ask-menu-window-size real-pane orientation args))
      (T (error "Cannot handle :ASK-WINDOW on this window ~A." real-pane)))))

(defun configure-pane (pane sizes fictious-panes real-panes)
   ;; something doesn't compile right in this function in lucid production-mode.
  (declare (optimize (compilation-speed 3) (speed 0) (safety 3)))
  (flet ((get-pane (name)
	   (or (find name fictious-panes :KEY #'fictious-pane-name)
	       (find name real-panes :KEY #'real-pane-name))))
    (let* ((constraints (constraint-pane-constraints pane))
	   (width (second (assoc :WIDTH constraints)))
	   (height (second (assoc :HEIGHT constraints)))
	   (inferiors (fictious-pane-inferiors pane))
	   (orientation (fictious-pane-orientation pane))
	   (inferior-sizes (find-pane-sizes pane sizes)))
      (declare (fixnum width height))
      ;; separate sizes into groups
      (let ((groups NIL))
	(let ((group NIL))
	  (dolist (item inferior-sizes
			(progn
			  (if (not group) (error "Where is the Next group?" inferior-sizes))
			  (setq groups (nconc groups (list group)))))
	    (if (eq item :THEN)
		(progn
		  (if (not group) (error "Doesn't make sense to have a then \"~A\" without something before it." inferior-sizes))
		  (setq groups (nconc groups (list group))
			group NIL))
		(setq group (nconc group (list (cons (get-pane (first item)) (cdr item))))))))
	;; check that any :EVENS are all together
	(verify-evens-in-groups groups)
	;; now process each group one at a time.
	;; Each group gets what remains of the size after the previous group takes some.
	;; this is important when using :EVEN constraints and proportional constraints.
	;; for convience split here on columns vs rows, although this may turn out to be kludgy
	(ecase orientation
	  (:COLUMN (setq height (configure-pane-column groups height orientation)))
	  (:ROW (setq width (configure-pane-rows groups width orientation))))
	(dolist (group groups)
	  (dolist (item group)
	    (ecase orientation
	      (:ROW ;; propagate down height constraint
		(set-constraints-in-pane `(:HEIGHT ,height) (first item)))
	      (:COLUMN ;; propagate down width constraint
		(set-constraints-in-pane `(:WIDTH  ,width) (first item)))))))
      ;; now recurse down and configure sizes of below panes.
      (dolist (inferior inferiors)
	(when (lisp:typep inferior 'fictious-pane)
	  (configure-pane inferior sizes fictious-panes real-panes))))))


(defun configure-pane-rows (groups width orientation)
  (declare (optimize (speed 0) (safety 3) (space 3)))
  (dolist (group groups)
    (declare (list group))
    (let ((width-used 0))
      (declare (fixnum width-used))
      ;; check to see if we hit a final group of evens.
      (if (eq :EVEN (second (first group)))
	  ;; evenly split up remaining space.
	  (let ((number-of-items (length group)))
	    (declare (fixnum number-of-items))
	    (multiple-value-bind (pixels-per-item remainder)
		(floor width number-of-items)
	      (dolist (item group)
		(if (eq item (first (last group)))
		    ;; handle specially to use up all pixels.
		    (set-constraints-in-pane
		      `(:width ,(%+ pixels-per-item remainder)) (first item) T)
		    (set-constraints-in-pane
		      `(:width ,pixels-per-item) (first item) T)))))
	  (dolist (item group)
	    (let ((pane (first item)))
	      (cond ((eq (second item) :ASK-WINDOW)
		     (multiple-value-bind (width-needed height-needed)
			 (ask-window pane orientation (cddr item))
		       (declare (ignore height-needed))
		       (incf width-used width-needed)
		       (push `(:WIDTH ,width-needed)
			     (constraint-pane-constraints pane))))
		    ((and (null (cddr item)) (integerp (second item))
			  (> (second item) 1))
		     ;; use up width based on pixels of width available.
		     (let ((width-needed (second item)))
		       (declare (fixnum width-needed))
		       (incf width-used width-needed)
		       (set-constraints-in-pane `(:width ,width-needed) pane T)))
		    ((and (null (cddr item)) (numberp (second item))
			  (<= (second item) 1.0))
		     ;; use up width based on proportional of width available.
		     (let ((width-needed (floor (%* (second item) width))))
		       (declare (fixnum width-needed))
		       (incf width-used width-needed)
		       (set-constraints-in-pane `(:width ,width-needed) pane T)))
		    (T (error "~A not implemented yet" item))))))
      ;; subtract width used from total width.
      (decf width width-used)
      (if (< width 0)
	  ;; you are in trouble
	  (error "Used up too much width for constraint."))))
  width)

(defun configure-pane-column (groups height orientation)
  (declare (optimize (speed 0) (safety 3) (space 3)))
  (dolist (group groups)
    (declare (list group))
    (let ((height-used 0))
      (declare (fixnum height-used))
      ;; check to see if we hit a final group of evens.
      (if (eq :EVEN (second (first group)))
	  ;; evenly split up remaining space.
	  (let ((number-of-items (length group)))
	    (declare (fixnum number-of-items))
	    (multiple-value-bind (pixels-per-item remainder)
		(floor height number-of-items)
	      (dolist (item group)
		(if (eq item (first (last group)))
		    ;; handle specially to use up all pixels.
		    (set-constraints-in-pane
		      `(:height ,(%+ pixels-per-item remainder)) (first item) T)
		    (set-constraints-in-pane
		      `(:height ,pixels-per-item) (first item) T)))))
	  (dolist (item group)
	    (let ((pane (first item)))
	      (cond ((eq (third item) :LINES)
		     ;; use up lines based on character styles
		     (if (lisp:typep pane 'fictious-pane)
			 (error "Can't get line heights from a fictious pane ~A"
				item))
		     (let* ((line-height
			      (window-line-height (real-pane-pane pane)))
			    (height-needed
			      (the (values fixnum number)
				   (floor (%* (second item) (%1+ line-height))))))
		       (declare (fixnum line-height height-needed))
		       (incf height-used height-needed)
		       (push `(:height ,height-needed)
			     (constraint-pane-constraints pane))))
		    ((eq (second item) :ASK-WINDOW)
		     (multiple-value-bind (width-needed height-needed)
			 (ask-window pane orientation (cddr item))
		       (declare (ignore width-needed))
		       (incf height-used height-needed)
		       (push `(:height ,height-needed)
			     (constraint-pane-constraints pane))))
		    ((and (null (cddr item)) (integerp (second item))
			  (> (second item) 2))
		     ;; use up height based on proportional of width available.
		     (let ((height-needed (second item)))
		       (declare (fixnum height-needed))
		       (incf height-used height-needed)
		       (set-constraints-in-pane `(:height ,height-needed) pane T)))
		    ((and (null (cddr item)) (numberp (second item))
			  (<= (second item) 1.0))
		     ;; use up height based on proportional of width available.
		     (let ((height-needed
			     (the (values fixnum number) (floor (%* (second item) height)))))
		       (declare (fixnum height-needed))
		       (incf height-used height-needed)
		       (set-constraints-in-pane `(:height ,height-needed) pane T)))
		    (T (error "Constraint ~A Not Implemented yet" item))))))
      ;; subtract height used from total height.
      (decf height height-used)
      (if (%< height 0)
	  ;; you are in trouble
	  (error "Used up too much height for constraint.")))))

(defun verify-fictious-pane-superiors (fictious-panes top-pane)
  #.(fast)
  (dolist (pane fictious-panes)
    (if (eq pane top-pane)
	(if (fictious-pane-superior pane)
	    (error "The Pane ~A should have no superior fictious panes."))
	(if (not (fictious-pane-superior pane))
	    (error "The Pane ~A should have a superior fictious panes.")))))

(defun verify-row-column-layout-of-configuration (top-pane)
  #.(fast)
  (labels ((opposite (orientation)
	     (if (eq orientation :row) :column :row))
	   (internal (pane next-orientation)
	     (when (lisp:typep pane 'FICTIOUS-PANE)
	       (unless (eq next-orientation (fictious-pane-orientation pane))
		 (error "Incorrect Orientation for fictious pane ~A."
			(fictious-pane-name pane)))
	       (let ((orientation (opposite next-orientation)))
		 (dolist (inferior (fictious-pane-inferiors pane))
		   (internal inferior orientation))))))
    (let ((orientation (opposite (fictious-pane-orientation top-pane))))
      (dolist (inferior (fictious-pane-inferiors top-pane))
	(internal inferior orientation)))))

(defun set-pane-sizes (pane left top)
  (let ((inferiors (fictious-pane-inferiors pane))
	(orientation (fictious-pane-orientation pane)))
    (ecase orientation
      (:COLUMN
	(dolist (inferior inferiors)
	  (let ((width (second (assoc :WIDTH (constraint-pane-constraints inferior))))
		(height (second (assoc :HEIGHT (constraint-pane-constraints inferior)))))
	    (typecase inferior
	      (FICTIOUS-PANE
		(set-pane-sizes inferior left top))
	      (REAL-PANE
		(set-window-edges (real-pane-pane inferior)
				    left
				    top
				    (%+ left width)
				    (%+ top height))))
	    (incf top height))))
      (:ROW
	(dolist (inferior inferiors)
	  (let ((width (second (assoc :WIDTH (constraint-pane-constraints inferior))))
		(height (second (assoc :HEIGHT (constraint-pane-constraints inferior)))))
	    (typecase inferior
	      (FICTIOUS-PANE
		(set-pane-sizes inferior left top))
	      (REAL-PANE
		(set-window-edges (real-pane-pane inferior)
				    left
				    top
				    (%+ left width)
				    (%+ top height))))
	    (incf left width)))))))


#-pcl
(defun get-argument-list (args)
  (let ((args (mapcar #'(lambda (arg)
			  (if (symbolp arg) arg
			      (list (first arg) (getf (cddr arg) :DEFAULT))))
		      args)))
    (when args
      (cons '&OPTIONAL args))))

#+pcl
(defun get-argument-list (args)
  (let ((args (mapcar #'(lambda (arg)
			  (if (symbolp arg) arg
			      (list (first arg) (getf (cddr arg) :DEFAULT))))
		      args)))
    ;; 5/3/90 (sln) Okay.  The problem all started when calls to
    ;; define-lisp-command with keyword args broke lucid3.0 (">>Trap:
    ;; Bus error").  This is a PCL problem.  What I've done here is to
    ;; add an "&rest ignore" to the arglist if it has &key in it.  I
    ;; can't take credit for this one.  Phil Race
    ;; <philr%fs.icl.stc.co.uk@NSFnet-Relay.AC.UK> sent me a message
    ;; saying he had run into similar problems in clue7.1.  He fixed it
    ;; by adding the &rest arg.  Everything from the "(let
    ;; ((key-position" down to (but not including) "(when args" is my
    ;; added code.  Basically, it just looks for &key and splices in
    ;; "&rest ignore" if it finds it there.  While it is only a PCL
    ;; problem, it shouldn't hurt any other CLOS's to do this (I hope).
    (let ((key-position (position '&key args)))
      (when key-position
	(cond ((zerop key-position) (setq args (append '(&rest ignore) args)))
	      (t (let* ((list (append '(&rest ignore) (nthcdr key-position args))))
		   (setq args (rplacd (nthcdr (%1- key-position) args) list))))))
      (when args
	(cons '&OPTIONAL args)))))

(defun get-argument-calling-list (args)
  (let ((keywords-p NIL))
    (mapcan #'(lambda (arg)
		(case arg
		  (&KEY (setq keywords-p T) NIL)
		  (T (if keywords-p
			 (list (intern (symbol-name (if (symbolp arg) arg (first arg)))
				       (find-package "KEYWORD"))
			       (if (symbolp arg) arg (first arg)))
			 (list (if (symbolp arg) arg (first arg)))))))
	    args)))

(defmacro define-program-command ((name program-name &REST options &KEY keyboard-accelerator
					(echo t) menu-accelerator (menu-level '(:top-level))
					(menu-documentation t)
					menu-documentation-include-defaults
					provide-output-destination-keyword &ALLOW-OTHER-KEYS)
				  arglist &BODY body)
  (declare (ignore menu-documentation-include-defaults #-excl options
		   provide-output-destination-keyword echo))
  (let* ((internal-function-name (intern (format NIL "~A-internal" name)))
	 (program-framework (find-program-framework program-name))
	 (state-variables (program-framework-state-variables program-framework))
	 (state-variable-names
	   (mapcar #'(lambda (variable) (if (symbolp variable) variable (car variable)))
		   state-variables))
	 (command-table-name (symbol-name program-name)))
    `(progn 'compile
	    (define-command (,name :command-table ,command-table-name)
				   ,arglist
	      (funcall ',internal-function-name
		       *Program* . ,(get-argument-calling-list arglist)))
	    ,@(if keyboard-accelerator
		  `((define-command-accelerator
		      ,(intern (format NIL "accelerator-~A" name) (symbol-package name))
		      ,command-table-name ',keyboard-accelerator
		      NIL ,arglist
		      (list ',name))))
	    (defmethod ,internal-function-name ((self ,program-name)
						. ,(get-argument-list arglist))
	      ;; 6/29/90 (sln) I know this is kindof bogus to do this, but
	      ;; I can't figure out a way for a user to specify
	      ;; declarations in his code and have those declaration appear
	      ;; in the correct place.  I suppose I could go through the
	      ;; body code, looking for "(declare ...)".  Oh, well, such is
	      ;; life.
	      ,@(mapcar #'(lambda (a1) (if (consp a1) (car a1) a1))
			(remove t (get-argument-list arglist) :test #'(lambda (ignore a2) (member a2 lambda-list-keywords))))
	      ;; 6/27/90 (sln) Genera 8.0 CLOS spits out a warning message
	      ;; about a gensymed variable not being used if SLOT-ENTRIES
	      ;; arg to WITH-SLOTS is NIL, so let's test for nil first and
	      ;; emit the proper code.
	      ,@(if state-variable-names
		    `((with-slots ,state-variable-names self
			;; 6/28/90 (sln) Let's ref all the state-variables in case they aren't used.
			,@state-variable-names nil
			. ,body))
		    `(,@body)))
	    ,(when menu-accelerator
	       (let ((command-menu-name (if (eq T menu-accelerator)
					    (make-pretty-name name)
					    menu-accelerator)))
		 `(define-command-menu-handler (,command-menu-name
						,command-table-name ,menu-level
						:GESTURE (:LEFT :RIGHT)
						:DOCUMENTATION ,menu-documentation)
					       (&REST args)
		    (apply #'standard-command-menu-handler ',name
			   :command-table ',program-name args)))))))



(defun display-query-values-pane (pane force-p)
  (declare (ignore force-p))
  (when (program-query-values-pane-query-values-function pane)
    (if (program-query-values-pane-program-memo pane)
	(let ((*Querying-Values-Stream* pane)
	      (*Query-Entry-Table* (program-query-values-pane-query-entry-table pane)))
	  (draw-querying-values-menu pane NIL
				     (program-query-values-pane-program-memo pane)
				     NIL T))
	(let ((*Querying-Values-Stream* pane)
	      (*Query-Entry-Table* (make-hash-table :TEST 'equal)))
	  (setf (program-query-values-pane-query-entry-table pane)
		*Query-Entry-Table*)
	  (multiple-value-bind (values program-memo)
	      (initial-draw-querying-values-menu
		pane NIL NIL NIL
		#'(lambda (stream)
		    (funcall (program-query-values-pane-query-values-function pane)
			     *Program* stream))
		NIL NIL)
	    (declare (ignore values))
	    (setf (program-query-values-pane-program-memo pane)
		  program-memo))))))

(defun display-display-pane (pane force-p)
  (when (or force-p (program-pane-incremental-redisplayer pane)
	    (program-pane-redisplay-after-commands pane))
    (cond ((program-pane-incremental-redisplayer pane)
	   (run-memo (program-pane-incremental-redisplayer pane) pane))
	  ((program-pane-redisplay-string pane)
	   (with-centered-display (pane)
	     (simple-princ (program-pane-redisplay-string pane) pane)))
	  ((program-pane-redisplay-function pane)
	   (funcall (program-pane-redisplay-function pane) *Program* pane)))))


(defun display-title-pane (pane force-p)
  (when (or force-p (program-pane-redisplay-after-commands pane))
    (let ((string (program-pane-redisplay-string pane))
	  (function (program-pane-redisplay-function pane)))
      (clear-history pane)
      (cond (string
	     (with-centered-display (pane)
	       (simple-princ string pane)))
	    (function
	     (funcall function *Program* pane))))))

(defun ask-menu-window-size (menu orientation &REST args)
  (declare (ignore args))
  (when (null (program-command-menu-orientation menu))
    (setf (program-command-menu-orientation menu)
	  (if (eq orientation :ROW) :default-columns :default-rows)))
  (multiple-value-bind (width height)
      (display-program-command-menu menu NIL)
    (values width height)))

  
(defvar *Inter-Menu-Item-Spacing* 16.)


;; make a bold assumption for starters.
;; 1. if we are given an orientation with commands, then use those only
;;    and ignore menu accelerators.
;; 2. if not, then find all menu accelerators, and make them a single list for
;;    row or column
(defun display-program-command-menu (pane &OPTIONAL (display-p T) &AUX width height)
  (multiple-value-setq (width height) (window-size pane))
  (unless (program-command-menu-items pane)
    ;; if we have a printer than we need to make a fake window and pass it along
    ;; to each item and check its size.
    (let ((fake-stream (allocate-fake-window (and display-p pane) NIL (not display-p)))
	  (command-table  (find-program-command-table *Program*)))
      (with-character-style ((window-character-style pane) fake-stream)
      (labels ((printer (item stream)
		 (get-menu-item-string item)
		 (simple-princ item stream))
	       (draw-menu-item (item)
		 (clear-history fake-stream)
		 (if (not (get-menu-item-selectable-p item))
		     (printer item fake-stream)
		     (display-as
		       (:OBJECT (if (consp item) item (list item))
			:TYPE `(COMMAND-MENU-ITEM :COMMAND-TABLE ,command-table)
			:STREAM fake-stream)
		       (printer item fake-stream)))
		 (presentation-window-presentations fake-stream)))
	(cond ((consp (program-command-menu-orientation pane))
	       ;; then use exactly what we find
	       (display-program-command-menu-with-given-commands
		 pane #'draw-menu-item))
	      (T ;; find item list
	       (let ((new-item-list
		       (sort (mapcar #'(lambda (item)
					 (cons (draw-menu-item item) item))
				     (find-menu-items-for-pane pane))
			     #'string-lessp :KEY #'cdr)))
		 (case (program-command-menu-orientation pane)
		   ((:ROWS :DEFAULT-ROWS)
		    (let ((needed-height 0)
			  (max-height 0)
			  (width-so-far 5)
			  (row NIL) (rows NIL))
		      (declare (fixnum needed-height max-height width-so-far))
		      (dolist (item new-item-list
				    (setq rows (nconc rows (list row))
					  row (list item)
					  needed-height (%+ needed-height max-height)))
			(multiple-value-bind (left top right bottom)
			    (get-presentations-boundaries (first item))
			  (declare (ignore top left) (fixnum right bottom))
			  (maximize max-height bottom)
			  (setq width-so-far (%+ width-so-far right *Inter-Menu-Item-Spacing*))
			  (if (%> width-so-far width)
			      (setq rows (nconc rows (list row))
				    row (list item)
				    width-so-far (+ 5 right)
				    needed-height (+ needed-height max-height))
			      (setq row (nconc row (list item))))))
		      ;; for now, make the menu two columns
		      (clear-history pane)
		      (let ((needed-height 0)
			    (max-height 0)
			    (width-so-far 5))
			(dolist (row rows)
			  (dolist (item row)
			    (multiple-value-bind (left top right bottom)
				(get-presentations-boundaries (first item))
			      (declare (ignore top left) (fixnum right bottom))
			      (insert-presentations-with-offset
				pane (car item) width-so-far needed-height)
			      (maximize max-height bottom)
			      (setq width-so-far (%+ width-so-far right
						     *Inter-Menu-Item-Spacing*))))
			  (setq needed-height (%+ needed-height max-height)
				width-so-far 0
				max-height 0)))))
		   (T ;; use columns
		     (dformat "~&Using Columns.")
		     (let ((needed-width 0)
			   (max-width 0)
			   (height-so-far 5)
			   (column NIL) (columns NIL))
		       (declare (fixnum needed-width max-width height-so-far))
		       (dolist (item new-item-list
				     (setq columns (nconc columns (list column))
					   column (list item)
					   needed-width (%+ needed-width max-width)))
			 (multiple-value-bind (left top right bottom)
			     (get-presentations-boundaries (first item))
			   (declare (ignore top left) (fixnum right bottom))
			   (maximize max-width right)
			   (setq height-so-far (%+ height-so-far bottom 10))
			   (if (%> height-so-far height)
			       (setq columns (nconc columns (list column))
				     column (list item)
				     height-so-far (%+ 5 bottom)
				     needed-width
				     (%+ needed-width *Inter-Menu-Item-Spacing* max-width))
			       (setq column (nconc column (list item))))))
		       ;; for now, make the menu two columns
		       (clear-history pane)
		       (let ((needed-width 0)
			     (max-width 0)
			     (height-so-far 5))
			 (dolist (column columns)
			   (dolist (item column)
			     (multiple-value-bind (left top right bottom)
				 (get-presentations-boundaries (first item))
			       (declare (ignore top left) (fixnum right bottom))
			       (insert-presentations-with-offset
				 pane (car item) needed-width height-so-far)
			       (maximize max-width right)
			       (setq height-so-far (%+ height-so-far bottom 10))))
			   (setq needed-width
				 (%+ needed-width *Inter-Menu-Item-Spacing* max-width)
				 height-so-far 0
				 max-width 0)))))))))
	(when display-p (redraw-window pane))))
      (deallocate-fake-window fake-stream)))
  (when (not display-p)
    ;; we called this to get the height and width
    (values (%+ 10 (presentation-window-max-x-position pane))
	    (%+ 4 (presentation-window-max-y-position pane)))))

(defun display-program-command-menu-with-given-commands (pane draw-menu-item-function)
  (let* ((items (second (program-command-menu-orientation pane)))
	 (new-items (mapcar #'(lambda (sub-items)
				(mapcar #'(lambda (item)
					    (cons (funcall draw-menu-item-function item) item))
					sub-items))
			    items)))
    ;;(print items tv:selected-window)
    ;;(print new-items tv:selected-window)
    (ecase (first (program-command-menu-orientation pane))
      (:ROWS
	(clear-history pane)
	(let ((needed-height 0)
	      (max-height 0)
	      (width-so-far 5))
	  (declare (fixnum max-height width-so-far needed-height))
	  (dolist (row new-items)
	    (dolist (item row)
	      (multiple-value-bind (left top right bottom)
		  (get-presentations-boundaries (first item))
		(declare (ignore top left) (fixnum right bottom))
		(insert-presentations-with-offset
		  pane (car item) width-so-far needed-height)
		(maximize max-height bottom)
		(setq width-so-far (%+ width-so-far right 10))))
	    (setq needed-height (+ needed-height max-height)
		  width-so-far 0
		  max-height 0))))
      (:COLUMNS
	(clear-history pane)
	(let ((needed-width 0)
	      (max-width 0)
	      (height-so-far 5))
	  (declare (fixnum max-width height-so-far needed-width))
	  (dolist (column new-items)
	    (dolist (item column)
	      (multiple-value-bind (left top right bottom)
		  (get-presentations-boundaries (first item))
		(declare (ignore top left) (fixnum right bottom))
		(insert-presentations-with-offset
		  pane (car item) needed-width height-so-far)
		(maximize max-width right)
		(setq height-so-far (%+ height-so-far bottom 10))))
	    (setq needed-width (+ needed-width max-width)
		  height-so-far 0
		  max-width 0)))))))





(defun find-menu-items-for-pane (pane)
  (let ((command-table (find-program-command-table *Program*))
	(orientation (program-command-menu-orientation pane))
	(level (program-command-menu-level pane))
	(menu-items NIL))
    (if (consp orientation) (setq menu-items (second orientation)))
    (dolist (menu-handler (command-table-menu-accelerator-table command-table))
      (let ((levels (command-menu-handler-menu-levels menu-handler)))
	(when (if (consp levels) (member level levels) (eq level levels))
	  ;; this handler is for this table.
	  (push (command-menu-handler-command-name menu-handler) menu-items))))
    menu-items))

(defun configuration (frame)
  (unless (lisp:typep frame 'program-framework-instance)
    (error "Frame ~A must be a program-framework-instance" frame))
  (program-framework-instance-configuration frame))


(defun set-program-frame-configuration (configuration-name &OPTIONAL (frame *Program-Frame*))
  (set-configuration frame configuration-name))

(defun set-configuration (frame configuration-name &OPTIONAL (force-p NIL))
  (when (or force-p
	    (not (eq configuration-name (program-framework-instance-configuration frame))))
    (multiple-value-bind (left top right bottom)
	#+(and symbolics (not x))
	(scl:send (program-framework-instance-root-window frame) :inside-edges)
	#+X
	(window-edges (program-framework-instance-root-window frame))
	(setup-configuration frame configuration-name left top right bottom))
    (expose-program-framework-instance frame)))

(defsetf configuration set-configuration)

(defun set-program-frame-edges (frame left top right bottom)
  (set-window-edges (program-framework-instance-root-window frame)
		       left top right bottom)
  (set-configuration frame (program-framework-instance-configuration frame) T)
  (let ((*Disable-Caching* T))
    (expose-program-framework-instance frame)))

;(setf (program-framework-program-instances (first *program-frameworks*)) NIL)


(defvar *command-loop-eval-function* 'command-loop-eval-function)
(defvar *command-loop-print-function* 'command-loop-print-function)


(defun default-command-evaluator (command args)
  (when command
    (apply command args)))



(defvar *Bind-Debug-Io* NIL)

(defun default-command-top-level (program &REST options
				  &KEY (window-wakeup 'default-window-wakeup-handler)
				  abort-exits
				  (command-evaluator
				    (program-command-evaluator program))
				  (form-evaluator *command-loop-eval-function*)
				  environment
				  (form-values-print-function *command-loop-print-function*)
				  (initial-redisplay t)
				  (prompt *Full-Command-Prompt*)
				  (unknown-accelerator-is-command NIL)
				  &ALLOW-OTHER-KEYS
				  &AUX
				  (*Full-Command-Prompt* prompt)
				  #+symbolics
				  (dbg:*debug-io-override* tv:initial-lisp-listener))
  (declare (ignore options))			;4/30/90 (SLN)
  (declare (ignore form-evaluator form-values-print-function window-wakeup
		   initial-redisplay abort-exits environment))
  (select-program program)
  (unless command-evaluator
    (setq command-evaluator #'default-command-evaluator))
  (unwind-protect
      (catch 'quit-top-level-command-loop
	(program-redisplay program)
	(let (+ ++ +++ * ** ***
	      #-symbolics
	      (*debug-io* (if *Bind-Debug-Io*
			      (or (find-debug-pane program) *Query-Io*)
			      *debug-io*))
	      command-query
	      command-query-dispatch-mode
	      command-query-command-table
	      (count 0))
	  (declare (special + ++ +++ * ** ***) (fixnum count))
	  (do () (())
	    (when (zerop (rem count 10)) (clrhash *Subtypep-Cache-Table*))
	    (unless (and (eq command-query-dispatch-mode *Dispatch-Mode*)
			 (eq command-query-command-table *Command-Table*))
	      (setq command-query-dispatch-mode *Dispatch-Mode*
		    command-query-command-table *Command-Table*)
	      (setq command-query
		    (ecase *Dispatch-Mode*
		      (:COMMAND-ONLY
			'command)
		      ((:FORM-PREFERRED :COMMAND-PREFERRED)
		       `(command-or-form ,*Command-Table*
					 :DISPATCH-MODE ,*Dispatch-Mode*))
		      (:FORM-ONLY 'COMMAND-FORM))))
	    (catch #+lucid 'system:top-level #-lucid 'ignored-tag
		   (catch 'top-level-command-loop
		     (fresh-line *Query-Io*)
		     (multiple-value-bind (command-and-args type)
			 (read-program-command program :PROMPT prompt
					       :COMMAND-QUERY command-query
					       :UNKNOWN-ACCELERATOR-IS-COMMAND
					       unknown-accelerator-is-command)
		       (if (eq type 'COMMAND-FORM)
			   (progn
			     (setq +++ ++ ++ + + command-and-args)
			     ;;(fresh-line *Query-Io*)
			     (let ((values (multiple-value-list (eval command-and-args))))
			       (setq *** ** ** * * (first values))
			       (dolist (value values)
				 (fresh-line *Query-Io*)
				 (prin1 value *Query-Io*))))
			   (funcall command-evaluator (first command-and-args)
				    (cdr command-and-args)))))
		   (program-redisplay program))
	    (incf count))
	  *query-io*))
    ;; deexpose the panes
    (deexpose-program-framework-instance (program-frame program))
    ))

#+ignore
(defun test (*Query-Io*)
  (let (;(window tv:selected-window)
	)
    (unwind-protect
	(let ((*standard-output* *query-io*))
	  ;(expose-window *Query-Io*)
	  (clear-history *Query-Io*)
	  ;(scl:send (window-real-window *Query-Io*) :SELECT)
	  (do () (())
	    (catch 'top-level-command-loop
	      (fresh-line *Query-Io*)
	      (let ((command-and-args (query 'command :PROMPT-MODE :RAW
					      :STREAM *Query-Io*
					      :PROMPT "-> ")))
		(funcall #'(lambda (command args)
			     (when command
			       (apply command args)))
			 (first command-and-args)
			 (cdr command-and-args))))))
      ;(scl:send window :SELECT)
      )))

(defun command-loop-eval-function (form &OPTIONAL environment)
  (declare (ignore form environment))
  )

(defun command-loop-print-function (values)
  (declare (ignore values))
  )


(defun default-window-wakeup-handler (blip)
  (declare (ignore blip))
  )


(defun read-program-command (program &REST options &KEY (stream *Query-Io*)
			     (command-query NIL) (unknown-accelerator-is-command NIL)
			     prompt (dispatch-mode *Dispatch-Mode*) keyboard-accelerators
			     environment window-wakeup input-wait-handler &ALLOW-OTHER-KEYS)
  (declare (ignore program keyboard-accelerators environment window-wakeup
		   input-wait-handler))
  ;; decide if we are reading accelerators or not.
  (if (and (not (getf options :command-only))
	   (command-table-kbd-accelerator-table *Command-Table*))
      ;; accelerators are preference.
      (read-accelerated-command :STREAM stream
				:PROMPT prompt
				:UNKNOWN-ACCELERATOR-IS-COMMAND unknown-accelerator-is-command)
      (query (or command-query
		 (ecase dispatch-mode
		   (:COMMAND-ONLY
		     'command)
		   ((:FORM-PREFERRED :COMMAND-PREFERRED)
		    `(command-or-form ,*Command-Table*
				      :DISPATCH-MODE ,dispatch-mode))
		   (:FORM-ONLY 'COMMAND-FORM)))
	     :STREAM stream :PROMPT-MODE :RAW
	     :PROMPT prompt)))


(defvar *Standard-Arguments-Command-Table* (make-command-table "Standard Arguments"))

(defun read-accelerated-command (&KEY (command-table *command-table*)
				 (stream *query-io*)
				 (help-stream stream) (echo-stream stream) whostate prompt
				 (command-prompt *full-command-prompt*)
				 full-command-full-rubout special-blip-handler timeout
				 input-wait input-wait-handler form-p handle-clear-input
				 (catch-accelerator-errors t)
				 unknown-accelerator-is-command
				 unknown-accelerator-tester
				 unknown-accelerator-reader
				 (blank-line-mode *default-blank-line-mode*)
				 unknown-accelerator-reader-prompt
				 process-unknown-accelerator abort-chars suspend-chars status
				 intercept-function window-wakeup (notification t))
  (warn-unimplemented-args notification T window-wakeup NIL intercept-function NIL status NIL
			   suspend-chars NIL abort-chars NIL process-unknown-accelerator NIL
			   unknown-accelerator-tester NIL
			   unknown-accelerator-reader NIL
			   blank-line-mode *default-blank-line-mode*
			   help-stream stream echo-stream stream whostate NIL prompt NIL
			   command-prompt *full-command-prompt*
			   full-command-full-rubout NIL special-blip-handler NIL timeout NIL
			   input-wait NIL input-wait-handler NIL form-p NIL
			   handle-clear-input NIL unknown-accelerator-reader-prompt NIL
			   catch-accelerator-errors t)
  (let ((character
	  (multiple-value-list
	    (with-presentation-input-context
	      (`(command ,command-table) :INHERIT NIL) (blip)
	      (read-char-for-query-internal stream)
	      (T (let ((command-and-args (presentation-blip-object blip)))
		   (apply (first command-and-args)
			  (cdr command-and-args))))))))
    (if (and (consp character)
	     (characterp (first character)))
	;; if we return a character here, hopefully it is an accelerated command.
	(let ((accelerator-table (command-table-kbd-accelerator-table command-table)))
	  (setq character (first character))
	  (if accelerator-table
	      (let ((entry (assoc character
				  (keyboard-command-table-command-alist accelerator-table)
				  :TEST #'EQUALP)))
		(if entry
		    (let ((command (funcall (second entry))))
		      (apply (first command) (cdr command)))
		    (if unknown-accelerator-is-command
			;;
			(read-program-command :STREAM stream :PROMPT prompt
					      :COMMAND-ONLY T)	      
			(beep))))
	      (beep)))
	(values-list character))))



(defun command-error (format-string &REST format-args)
  (throw 'top-level-command-loop
    (apply #'format *Query-Io* format-string format-args)))

(defun exit-program ()
  (when *Program-Frame*
    (xlib::set-window-priority
      :BELOW (window-real-window
	       (program-framework-instance-root-window *Program-Frame*)) NIL))
  (throw 'quit-top-level-command-loop NIL))

(defun exit ()
  (throw 'quit-top-level-command-loop NIL))

(defun find-valid-command-menu-handler (command-table command-name gestures)
  (dolist (handler (command-table-menu-accelerator-table command-table))
    (when (and (equalp command-name (command-menu-handler-command-name handler))
	       (if (consp gestures)
		   (dolist (gesture gestures NIL)
		     (when (member gesture
				   (translate-gesture-to-mouse-char
				     (command-menu-handler-gestures handler)))
		       (return T)))
		   (member gestures
			   (translate-gesture-to-mouse-char
			     (command-menu-handler-gestures handler))))
	       (or (null (command-menu-handler-test handler))
		   (funcall (command-menu-handler-test handler))))
      (return handler))))



(defun standard-command-menu-handler (command-name &rest args)
  ;(cons command-name (copy-list args))
  (declare (ignore args))
  (list command-name))

(defmacro define-command-menu-handler ((command-name command-table menu-levels
						     &KEY (gesture :LEFT)
						     (documentation T)
						     test)
				       arglist &BODY command-form)
  (let* ((body-function-name
	   (intern (format NIL "~A-~A-~A-~A-menu"
			   (if (symbolp gesture) gesture (first gesture))
			   command-name command-table
			   (if (symbolp menu-levels) menu-levels (first menu-levels)))))
	 (test-function-name
	   (and (consp test)
		(intern (format NIL "~A-~A-~A-~A-test"
				(if (symbolp gesture) gesture (first gesture))
			   command-name command-table
			   (if (symbolp menu-levels) menu-levels (first menu-levels))))))
	 (documentation-function-name
	   (and (consp documentation)
		(intern (format NIL "~A-~A-~A-~A-documentation"
				(if (symbolp gesture) gesture (first gesture))
			   command-name command-table
			   (if (symbolp menu-levels) menu-levels (first menu-levels)))))))
    `(progn (defun ,body-function-name
		   ,(if (or (member '&allow-other-keys arglist)
			    (not (member '&KEY arglist)))
			(if (not arglist) '(&Rest ignore) arglist) ;; needs more work someday.
			(let ((position (position '&AUX arglist)))
			  (if (not position)
			      (append arglist (list '&allow-other-keys))
			      (cond ((zerop position)
				     (cons '&allow-other-keys arglist ))
				    (T (setq arglist (copy-list arglist))
				       #+ignore ;; lucid can't handle setf of nthcdr.
				       (setf (nthcdr position arglist)
					     (cons '&allow-other-keys
						   (nthcdr position arglist)))
				       (let* ((list (cons '&allow-other-keys
							  (nthcdr position arglist))))
					 (if (zerop position)
					     (setq arglist list)
					     (rplacd (nthcdr (%1- position) arglist)
						     list)))
				       arglist)))))
	      . ,command-form)
	    ,@(and (consp test)
		   `((defun ,test-function-name . ,test)))
	    ,@(and (consp documentation)
		   `((defun ,documentation-function-name . ,documentation)))
	    (define-command-menu-handler-internal
	      ',command-name ',command-table ',menu-levels
	      ',gesture ',body-function-name
	      ,(if (consp documentation) documentation-function-name documentation)
	      ,(if (consp test) test-function-name test)))))


(defun define-command-menu-handler-internal (command-name command-table menu-levels gesture
					     body-function-name documentation test)
  (setq gesture (if (symbolp gesture) (list gesture) gesture))
  (setq command-table (find-command-table command-table))
  ;; now install it in the right places.
  ;; ha, ha you thought you were done already.
  ;; find any previous menu-handlers in the command table and delete them.
  ;; also have to worry about telling windows sometime that they exist.
  ;; I guess the window can get it from the command table.
  (let ((existing-handler
	  (find command-name (command-table-menu-accelerator-table command-table)
		:TEST #'EQUALP :KEY #'command-menu-handler-command-name)))
    (if existing-handler
	(setf (command-menu-handler-menu-levels existing-handler) menu-levels
	      (command-menu-handler-function existing-handler) body-function-name
	      (command-menu-handler-gestures existing-handler) gesture
	      (command-menu-handler-documentation existing-handler) documentation
	      (command-menu-handler-test existing-handler) test)
	(push
	  (make-command-menu-handler :command-name command-name
				     :FUNCTION body-function-name
				     :menu-levels menu-levels
				     :gestures gesture
				     :DOCUMENTATION documentation
				     :TEST test)
	  (command-table-menu-accelerator-table command-table)))))


(defun clear-all ()
  (setq *Program* NIL
	*Query-Menu-Windows* NIL
	*mouse-window* NIL
	*Allocated-Windows* NIL
	*Fake-Windows* NIL
	*Windows* NIL
	*Program-Frame* NIL
	*Menu-Choose-Resource* NIL)
  (dolist (i *Program-Frameworks*)
    (setf (program-framework-program-instances i) NIL))
  (initialize-window-system))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
