;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************

(cl:in-package 'ew)

#||
LUCID::EDIT, Function (&KEY ((:WINDOWS EDITOR::WINDOWS-P) NIL EDITOR::WINDOWS-P-SUPPLIED) (EDITOR::FRAME NIL EDITOR::FRAME-SUPPLIED-P) EDITOR::FRAME-SPEC EDITOR::WIDTH EDITOR::HEIGHT EDITOR::X EDITOR::Y EDITOR::ENTRY-FUNCTION EDITOR::FIND-DEFINITION EDITOR::FIND-FILE (EDITOR::TOP-LEVEL-FUNCTION (QUOTE EDITOR::%COMMAND-LOOP)) EDITOR::EXIT-FUNCTION (EDITOR::WAIT T) EDITOR::EDITOR-OBJECT EDITOR::TEMPORARY-P EDITOR::USE-EXISTING-EDITOR (EDITOR::LOAD-INIT-FILE-P EDITOR::*INIT-FILE-NEEDS-LOADING*))
EDITOR::INSERT-STRING-AT-POINT, Function (STRING)

EDITOR::CURRENT-MARK, Function
EDITOR:CURRENT-POINT, Function
EDITOR:MAKE-BUFFER, Function (EDITOR::NAME &OPTIONAL (EDITOR::MODES (EDITOR:VALUE EDITOR:DEFAULT-MODES)))
EDITOR::BUFFER-STREAM, Function (EDITOR:BUFFER)
EDITOR::ALL-BUFFERS-LIST, Function (EDITOR::SPEC)
EDITOR::*CURRENT-BUFFER*, Bound
EDITOR:*BUFFER-NAMES*, Bound
EDITOR:*BUFFER-LIST*, Bound

EDITOR::PATHNAME-TO-BUFFER-NAME, Function (PATHNAME)
EDITOR::MODIFYING-BUFFER, Macro
EDITOR:BUFFER-POINT, Function (LUCID::X)
EDITOR::BUFFER-%NAME, Function (LUCID::X)
EDITOR::BUFFER-LOCK, Function (LUCID::X)
EDITOR::MAKE-TOP-LEVEL-BUFFER-STREAM, Function (EDITOR:BUFFER)
EDITOR:BUFFER-NAME, Function (EDITOR:BUFFER)
EDITOR::INTERNAL-MAKE-BUFFER, Function (&KEY (EDITOR::%NAME NIL) (EDITOR::%REGION NIL) (EDITOR::%PATHNAME NIL) (EDITOR::MODES NIL) (EDITOR::MODE-OBJECTS NIL) (EDITOR::BINDINGS NIL) (EDITOR::POINT NIL) (EDITOR::WRITABLE T) (EDITOR::MODIFIED-TICK -2) (EDITOR::UNMODIFIED-TICK -1) (EDITOR::WINDOWS NIL) (EDITOR::VAR-VALUES NIL) (EDITOR::VARIABLES NIL) (EDITOR::WRITE-DATE NIL) (EDITOR::BACKED-UP (QUOTE NIL)) (EDITOR::LOCK (QUOTE NIL)))
ED, Function (&OPTIONAL EDITOR::X)
||#

(define-type editor-buffer (() &KEY (any-input NIL))
  :PRINTER ((buffer stream)
	    (simple-princ (if (editor::bufferp buffer) (editor::buffer-name buffer)
			      buffer)
			  stream))
  :PARSER ((stream &KEY type)
	   (let ((buffers (mapcar #'(lambda (buffer)
				      (list (editor::buffer-name buffer)
					    buffer))
				  editor:*buffer-List*)))
	     (let ((buffer
		     (complete-input stream
				     #'(lambda (input-string operation)
					 (default-complete-function input-string operation
					   buffers
					   '(#\Newline #\Space #\TAB)))
				     :ALLOW-ANY-INPUT any-input
				     :TYPE type
				     :partial-completers
				     *Standard-Completion-Delimiters*)))
	       (if (stringp buffer)
		   ;; must be a new editor buffer
		   buffer
		   buffer)))))



(defun write-string-to-buffer (string &OPTIONAL buffer)
  (cond ((stringp buffer)
	 (setq buffer (find buffer editor::*Buffer-List* :KEY #'editor::buffer-name
			    :TEST #'EQUALP)))
	((editor::bufferp buffer)
	 NIL)
	(T (setq buffer (query 'editor-buffer
			       :PROMPT "Choose Editor Buffer for the Output"))))
  (when buffer
    (when (stringp buffer)
      ;; need a new buffer
      (setq buffer (editor::make-buffer buffer)))
    (let (stream)
      (unwind-protect
	  (progn
	    (setq stream (editor::make-top-level-buffer-stream buffer))
	    (princ string stream))
	(and stream (close stream))))))
