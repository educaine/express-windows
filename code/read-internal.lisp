;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-Windows; Base: 10; Patch-File:T -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)



(defvar *Sample-String* "clear output history")
(defvar *String-Pointer* 0)

(defvar *Times-Read* 0)
(defvar *Max-Times-Read* 0)

(defvar *Reading-chars* NIL)

(defun read-char-loop (read-function &aux character-result)
  (let ((character
	 (if (= *String-Pointer* (length *Sample-string*))
	     (progn (incf *Times-Read*)
		    (if (= *times-read* *max-times-read*)
			(progn (setq *Reading-chars* NIL
				     *times-read* 0)
			       NIL)
		      (progn (setq *string-pointer* 0)
			     #\Meta-K)))
	   (prog1 (aref *Sample-string* *string-pointer*)
	     (incf *string-pointer*)))))
    (case read-function
	  (LISTEN (setq *unread-char* character)
		  (setq character-result T)
		  T)
	  (PEEK  (setq *unread-char* character)
		 (setq character-result character)
		 T)
	  (READ (setq character-result character)
		T))
    character-result))



#+clx
(defun read-internal (read-function timeout
		      &AUX (pointer-motion-p NIL) character-result)
  #.(fast)
  ;;(sync)
  (if *reading-chars* 
      (read-char-loop read-function)
  (progn(xlib:event-case (*X-Display* :TIMEOUT (and (eq read-function 'LISTEN) timeout)
 				:FORCE-OUTPUT-P T :DISCARD-P T)
    (:MOTION-NOTIFY (window) ;;  x y root-x root-y
		    ;;(dformat "Motion")
     (multiple-value-bind (x y same-screen-p child mask root-x root-y root)
	 (xlib:query-pointer window)
       (declare (ignore root mask child same-screen-p))	; 4/30/90 (SLN)
       (setq window (if (eq window *Last-Found-Window-Real-Window*)
			*Last-Found-Window* ;; get this loop fast for tracking mouse.
			;; open code of first part of find-window.
			(find-window window)))
       (setq #.*Mouse-X* x #.*Mouse-Y* y
	     *Global-Mouse-X* root-x *Global-Mouse-Y* root-y)
       ;;(setq pointer-motion-p T)
       (if (eq read-function 'LISTEN)
	   (progn (setq *Mouse-Window* (or window *Mouse-Window*))
	     T)
	   NIL)))
    (:KEY-PRESS (code state)
     (let ((character (xlib:keycode->character *X-Display* code state)))
       (cond ((not character) NIL)
	     (#+visual-640 (and (characterp character)
				(not (member character '(#\Ctrl-Function #\Function))))
	      #-visual-640 (characterp character)
	      (when (not (zerop *Key-Meta-State*))
		(if (upper-case-p character)
		    (setq character (char-downcase character))
		    (setq character (char-upcase character)))
		(setq character (set-char-bit character :meta T)))
	      (case read-function
		(LISTEN (setq *unread-char* character)
			(setq character-result T)
			T)
		(PEEK  (setq *unread-char* character)
		       (setq character-result character)
		       T)
		(READ (setq character-result character)
		      T)))
	     (T (case character
		  ((:LEFT-SHIFT :RIGHT-SHIFT)
		   (setq *Chord-Shifts* (%+ 16. *Key-Meta-State* *Key-Control-State*))
		   (setq *Key-Shift-State* 16))
		  #+visual-640
		  ((#\Ctrl-Function #\Function)
		   (setq *Chord-Shifts* (%+ 2 *Key-Shift-State* *Key-Control-State*))
		   (setq *Key-Meta-State* 2))
		  ((:LEFT-CONTROL :RIGHT-CONTROL)
		   (setq *Chord-Shifts* (%+ 1. *Key-Shift-State* *Key-Meta-State*))
		   (setq *Key-Control-State* 1)))
		(eq read-function 'listen)))))
    (:KEY-RELEASE (code state)
     (let ((character (xlib:keycode->character *X-Display* code state)))
       (cond ((not character) NIL)
	     (#+visual-640 (and (characterp character)
				(not (member character '(#\Ctrl-Function #\Function))))
	      #-visual-640 (characterp character)
	      NIL)
	     (T (case character
		  ((:LEFT-SHIFT :RIGHT-SHIFT)
		   (setq *Chord-Shifts* (%+ *Key-Meta-State* *Key-Control-State*))
		   (setq *Key-Shift-State* 0))
		  #+visual-640
		  ((#\Ctrl-Function #\Function)
		   (setq *Chord-Shifts* (%+ *Key-Shift-State* *Key-Control-State*))
		   (setq *Key-Meta-State* 0))
		  ((:LEFT-CONTROL :RIGHT-CONTROL)
		   (setq *Chord-Shifts* (%+ *Key-Shift-State* *Key-Meta-State*))
		   (setq *Key-Control-State* 0)))
		(eq read-function 'listen)))))
    (:BUTTON-PRESS (code window x y root-x root-y)
     (setq window (find-window window))
     (setq *Global-Mouse-X* root-x
	   *Global-Mouse-Y* root-y
	   #.*Mouse-X* x
	   #.*Mouse-Y* y)
     (setq #.*Mouse-buttons*
	   (the fixnum (dpb 1
			    #+excl (byte 1 (%1- code))
			    #-excl (the fixnum (byte 1 (%1- code)))
			    (the fixnum #.*Mouse-buttons*))))
     #+debugging-memo (dformat "~%New Mouse Buttons ~D." #.*Mouse-Buttons*)
     (let ((character (list :MOUSE-CLICK
			    (the fixnum
				 (dpb (mouse-chord-shifts)
				      #+excl '#.(byte 5 3)
				      #-excl #.(byte 5 3)
				      (the fixnum (if (%= code 3) 4 code)))) ;; change code to 1,2,4 instead of 1,2,3
			    window
			    x y)))
       (case read-function
	 (LISTEN (setq *unread-char* character) T)
	 (PEEK (setq *unread-char* character)
	       (setq character-result character)
	       T)
	 (READ (setq character-result character)
	       T))))
    (:BUTTON-RELEASE (code x y root-x root-y)
     (setq *Global-Mouse-X* root-x
	   *Global-Mouse-Y* root-y
	   #.*Mouse-X* x
	   #.*Mouse-Y* y)
     (setq #.*Mouse-Buttons* (the fixnum (dpb 0
					      #-excl (the fixnum (byte 1 (%1- code)))
					      #+excl (byte 1 (%1- code))
					      (the fixnum #.*Mouse-Buttons*))))
     (dformat "~%New Mouse Buttons ~D." #.*Mouse-Buttons*)
     (eq read-function 'listen))
    (:EXPOSURE (window x y width height)
     (setq window (find-window window))
     (if (member window *Windows-Exposure-Ignores*)
	 (setq *Windows-Exposure-Ignores* (delete window *Windows-Exposure-Ignores*))
	 (when (presentation-window-p window)
	   ;; probably should take care of scroll bars too, but not for now.
	   (multiple-value-bind (x y)
	       (convert-screen-coords-to-window-coords window x y)
	     (redraw-window window x y (%+ x width) (%+ y  height)))))))
  (when (and pointer-motion-p *Consume-Extra-Mouse-Motion-Events-P*)
    ;; read off more events.
    (loop (unless (xlib:event-case (*X-Display* :TIMEOUT 0)
		    (:motion-notify (window x y root-x root-y)
		     (setq window (if (eq window *Last-Found-Window-Real-Window*)
				      ;; get this loop fast for tracking mouse.
				      ;; open code of first part of find-window.
				      *Last-Found-Window*
				      (find-window window)))
		     (setq #.*Mouse-X* x #.*Mouse-Y* y
			   *Global-Mouse-X* root-x *Global-Mouse-Y* root-y)
		     (when (eq read-function 'listen)
		       (setq *Mouse-Window* (or window *Mouse-Window*)))
		     T))
	    (return NIL))))
  character-result)))


;; macro expand following to see what we get.
#+ignore
(xlib:event-case (*X-Display* :TIMEOUT 0)
  (:motion-notify (window x y root-x root-y)
   (setq window (if (eq real-window *Last-Found-Window-Real-Window*)
		    ;; get this loop fast for tracking mouse.
		    ;; open code of first part of find-window.
		    *Last-Found-Window*
		    (find-window window)))
   (setq #.*Mouse-X* x #.*Mouse-Y* y
	 *Global-Mouse-X* root-x *Global-Mouse-Y* root-y)
   (when (eq read-function 'listen)
     (setq *Mouse-Window* (or window *Mouse-Window*)))
   T))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
