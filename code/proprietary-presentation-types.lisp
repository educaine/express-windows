;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************



(cl:in-package 'ew)

#||
alist-member
and
boolean
character
character-face-or-style
character-style
class
command
directory-pathname
dynamic-window
expression
flavor-name
font
form
function-spec
host
instance
integer
inverted-boolean
keyword
member
member-sequence
network
no-type
not
null
null-or-type
number
or
out-of-band-character
package
pathname
printer
raw-text
satisfies
sequence
sequence-enumerated
string
subset
symbol
symbol-name
time-interval
token-or-type
type-or-string
user
wildcard-pathname
window

||#

(define-type expression ())

(define-type-transform equality-translator
		       (NIL NIL :MENU NIL)
		       (object &KEY presentation)
  (values object (and presentation (presentation-type presentation))))

(define-type table ())

(define-type exit-menu-choice ())

(define-type command-button ())

(define-type popup-menu-choice ())


(define-type ignore-presentation (())
  )


(define-type alist-member ((alist &REST others) &KEY convert-spaces-to-dashes
			   (button-type :radio-button))
  :PRINTER ((object stream)
	    ;; the task here is to find which element of alist
	    ;; is the right value and display the associated string.
	    (if (eq alist :ALIST) (setq alist (first others)))
	    (let ((element (find object alist :KEY #'GET-MENU-ITEM-VALUE)))
	      (simple-princ (get-menu-item-string element) stream)))
  :PARSER ((stream)
	   (if (eq alist :ALIST) (setq alist (first others)))
	   (let ((alist (mapcar #'(lambda (item)
				    (list (coerce-to-string (get-menu-item-string item)
							    convert-spaces-to-dashes)
					  (get-menu-item-value item)))
				alist)))
	     (complete-input stream
			     #'(lambda (input-string operation)
				 (default-complete-function input-string operation
				   alist '(#\Space #\-)))
			     :TYPE type
			     :partial-completers
			     *Standard-Completion-Delimiters*)))
  :QUERY-VALUES-DISPLAYER
  ((stream object query-identifier &KEY type)
   (if (eq alist :ALIST) (setq alist (first others)))
   (query-values-choose-from-sequence
     stream alist object query-identifier :TYPE type
     :KEY #'get-menu-item-value
     :button-type button-type)))


(define-type alist-subset ((alist &OPTIONAL min-elements max-elements)
			   &KEY convert-spaces-to-dashes
			   (button-type :radio-button))
  :PRINTER ((objects stream)
	    ;; the task here is to find which element of alist
	    ;; is the right value and display the associated string.
	    (when (eq alist :ALIST)
	      (setq alist min-elements)
	      (setq min-elements NIL))
	    (format-list
	      objects
	      #'(lambda (object)
		  (let ((element (find object alist :KEY #'GET-MENU-ITEM-VALUE)))
		    (simple-princ (get-menu-item-string element) stream)))
	      :stream stream))
  :PARSER ((stream)
	   (when (eq alist :ALIST) (psetq alist min-elements min-elements NIL))
	   (let ((value-strings
		   (query `(sequence
			     (member . ,(mapcar #'(lambda (item)
						    (coerce-to-string
						      (get-menu-item-string item)
						      convert-spaces-to-dashes))
						alist))
			     ,min-elements ,max-elements)
			  :STREAM stream :PROMPT NIL :PROVIDE-DEFAULT NIL)))
	     (mapcar #'(lambda (string)
			 (let ((item (find string alist
					   :KEY #'get-menu-item-string
					   :TEST #'equalp)))
			   (when item
			     (get-menu-item-value item))))
		     value-strings)))
  :QUERY-VALUES-DISPLAYER
  ((stream object query-identifier &KEY type)
   (when (eq alist :ALIST) (psetq alist min-elements min-elements NIL))
   ;; check object to make sure it is winning to start with - i.e. fits in bounds.
;;   (unless (compare-range object min-elements max-elements))
     ;; oh  now we lose.
     ;; add or subtract some elements to fit.
;;     (if min-elements)
   (query-values-choose-from-sequence
     stream alist object query-identifier
     :HIGHLIGHTING-TEST #'(lambda (item values) (member item values :TEST #'EQUALP))
     :PRINTER #'(lambda (object stream)
		  (let ((element (find object alist :KEY #'GET-MENU-ITEM-VALUE)))
		    (simple-princ (get-menu-item-string element) stream)))
     :TYPE type
     :KEY #'GET-MENU-ITEM-VALUE
     :SELECT-ACTION #'(lambda (new old)
			(if (member new old :TEST #'EQUALP)
			    ;; subtract from list as long as it still meets
			    ;; the length requirements.
			    (if (compare-range (1- (length old)) min-elements max-elements NIL)
				(remove new old)
				(progn (beep)
				       old))
			    ;; add to list as long as it still meets length
			    (if (compare-range (1+ (length old)) min-elements max-elements NIL)
				(cons new old)
				(progn (beep)
				       old))))
     :BUTTON-TYPE button-type)))

#+ignore
(defun test (&OPTIONAL (own-window T))
  (querying-values (*Query-Io* :OWN-WINDOW own-window)
    (values
      (query '((alist-member (("First" :first) ("Second" :second)))
	       :button-type :radio-button)
	     :default :first
	     :prompt "Hello")
      (query '((alist-member (("First" :first) ("Second" :second)))
	       :button-type :check-box)
	     :default :second
	     :prompt "Hello Again")
      (query '((alist-member (("First" :first) ("Second" :second)))
	       :button-type :bold-text)
	     :default :second
	     :prompt "Hello for the third"))))

(deftype boolean () '(or null (not null)))

(define-type boolean (() &KEY (response-type :yes-or-no) (button-type :radio-button))
  :no-deftype T
  :PRINTER ((boolean stream) (simple-princ (if boolean
					(ecase response-type
					  (:yes-or-no "Yes")
					  (:true-or-false "True")
					  (:no-or-off "On")
					  (:1-or-0 "1"))
					(ecase response-type
					  (:yes-or-no "No")
					  (:true-or-false "False")
					  (:no-or-off "Off")
					  (:1-or-0 "0")))
				    stream))
  :PARSER ((stream)
	   (query (ecase response-type
		    (:yes-or-no '(alist-member (("Yes" . T) ("No" . NIL))))
		    (:true-or-false '(alist-member (("True" . T) ("False" . NIL))))
		    (:on-or-off '(alist-member (("On" . T) ("Off" . NIL))))
		    (:1-or-0 '(alist-member (("1" . T) ("0" . NIL)))))
		  :PROMPT NIL :PROVIDE-DEFAULT NIL
		  :STREAM stream))
  :QUERY-VALUES-DISPLAYER
  ((stream object query-identifier &KEY type)
   (query-values-choose-from-sequence
     stream
     (ecase response-type
		    (:yes-or-no '(("Yes" . T) ("No" . NIL)))
		    (:true-or-false '(("True" . T) ("False" . NIL)))
		    (:on-or-off '(("On" . T) ("Off" . NIL)))
		    (:1-or-0 '(("1" . T) ("0" . NIL))))
     object query-identifier :TYPE type
     :KEY #'get-menu-item-value
     :BUTTON-TYPE button-type)))

(define-type inverted-boolean (() &KEY (response-type :yes-or-no) (button-type :radio-button))
  :PRINTER ((boolean stream) (simple-princ (if boolean
					(ecase response-type
					  (:yes-or-no "No")
					  (:true-or-false "False")
					  (:no-or-off "Off")
					  (:1-or-0 "0"))
					(ecase response-type
					  (:yes-or-no "Yes")
					  (:true-or-false "True")
					  (:no-or-off "On")
					  (:1-or-0 "1")))
				    stream))
  :PARSER ((stream)
	   (query (ecase response-type
		    (:yes-or-no '(alist-member (("Yes" . NIL) ("No" . T))))
		    (:true-or-false '(alist-member (("True" . NIL) ("False" . T))))
		    (:on-or-off '(alist-member (("On" . NIL) ("Off" . T))))
		    (:1-or-0 '(alist-member (("1" . NIL) ("0" . T)))))
		  :PROMPT NIL :PROVIDE-DEFAULT NIL :STREAM stream))
  :QUERY-VALUES-DISPLAYER
  ((stream object query-identifier &KEY type)
   (query-values-choose-from-sequence
     stream (ecase response-type
		    (:yes-or-no '(("Yes" . NIL) ("No" . T)))
		    (:true-or-false '(("True" . NIL) ("False" . T)))
		    (:on-or-off '(("On" . NIL) ("Off" . T)))
		    (:1-or-0 '(("1" . NIL) ("0" . T))))
     object query-identifier :TYPE type
     :KEY #'get-menu-item-value
     :BUTTON-TYPE button-type)))


(define-type NULL (())
  :NO-DEFTYPE T
  :PARSER ((stream)
	   (let ((char (read-char-for-query stream)))
	     (if (and (consp char)
		      (eq :ACTIVATION (first char)))
		 NIL
		 (parse-error "Not A Valid Null entry" 'not-null-type
			      "Not a Valid Null Entry"))))
  :PRINTER ((object stream)
	    (simple-princ "None" stream)))

(defun number-describer (stream type number-string low high)
  (cond ((consp low)
	 (cond ((consp high)
		(simple-princ number-string stream)
		(simple-princ " greater than " stream)
		(display (first low) type :STREAM stream)
		(simple-princ " and less than " stream)
		(display (first high) type :STREAM stream))
	       (high
		(simple-princ number-string stream)
		(simple-princ " greater than " stream)
		(display (first low) type :STREAM stream)
		(simple-princ " and less than or equal to " stream)
		(display high type :STREAM stream))
	       (T
		(simple-princ number-string stream)
		(simple-princ " greater than " stream)
		(display (first low) type :STREAM stream))))
	(low
	 (cond ((consp high)
		(simple-princ number-string stream)
		(simple-princ " greater than or equal to " stream)
		(display low type :STREAM stream)
		(simple-princ " and less than " stream)
		(display (first high) type :STREAM stream))
	       (high
		(simple-princ number-string stream)
		(simple-princ " greater than or equal to " stream)
		(display low type :STREAM stream)
		(simple-princ " and less than or equal to " stream)
		(display high type :STREAM stream))
	       (T
		(simple-princ number-string stream)
		(simple-princ " greater than or equal to  " stream)
		(display low type :STREAM stream))))
	(T (simple-princ number-string stream))))


(defun compare-range (value low high error-string)
  (cond ((and (numberp low) (> low value))
	 (and error-string
	      (parse-error value 'integer
			   "~A ~D is not greater than ~D"
			   error-string value low)))
	((and (consp low) (numberp (first low)) (>= (first low) value))
	 (and error-string
	      (parse-error value 'integer
			   "~A ~D is not greater than or equal to ~D"
			   error-string value (first low))))
	((and (numberp high) (< high value))
	 (and error-string
	      (parse-error value 'integer
			   "~A ~D is not less than or equal to ~D"
			   error-string value high)))
	((and (consp high) (numberp (first high)) (<= (first high) value))
	 (and error-string
	      (parse-error value 'integer
			   "~A ~D is not less than ~D"
			   error-string value (first high))))
	(T T)))

(defun my-parse-integer (number-string low high base)
  (declare (string number-string))
  (multiple-value-bind (number length)
      (parse-integer number-string :radix base :JUNK-ALLOWED T)
    (declare (fixnum length) (number number))
    (cond ((or (not number)
	       (not (= length (the fixnum (length number-string)))))
	   (parse-error number-string 'integer "~A is not an integer" number-string))
	  (T (compare-range number low high "The Integer")
	     number))))

(defun my-parse-number (number-string low high)
  (declare (string number-string))
  (multiple-value-bind (number length)
      (read-from-string number-string NIL NIL)
    (declare (fixnum length) (number number))
    (cond ((or (not number)
	       (not (= length (the fixnum (length number-string)))))
	   (parse-error number-string 'number "~A is not a number" number-string))
	  (T (compare-range number low high "The Number")
	     number))))

(define-type integer ((&OPTIONAL low high) &KEY (base 10.))
  :NO-DEFTYPE T
  :PRINTER ((integer stream)
	    (let ((*Print-Base* base))
	      (simple-princ integer stream)))
  :PARSER ((stream)
	   (let* ((number-string (read-standard-token stream)))
	     (my-parse-integer number-string low high base)))
  :DESCRIBER ((stream &KEY plural-count)
	      (let ((base-description
		      (if plural-count
			  (cond ((or (null base) (= base 10.)) "integers")
				((= base 8) "octal integers")
				((= base 16) "hexadecimal integers")
				(T (format NIL "base ~D integers" base)))
			  (cond ((or (null base) (= base 10.)) "an integer")
				((= base 8) "an octal integer")
				((= base 16) "a hexadecimal integer")
				(T (format NIL "a base ~D integer" base))))))
		(number-describer stream 'integer base-description low high))))

(define-type number ((&OPTIONAL low high) &KEY (base 10.))
  :NO-DEFTYPE T
  :PARSER ((stream)
	   (let* ((number-string (read-standard-token stream)))
	     (my-parse-number number-string low high)))
  :DESCRIBER ((stream)
	      (number-describer stream 'number "a number" low high)))



(define-type character ()
  :NO-DEFTYPE T
  :PARSER ((stream)
	   (read-char-for-query stream)))


(define-type command-menu-item ((&KEY command-table menu-level))
  :PRINTER ((object stream)
	    (display object `(command ,command-table) :stream stream))
  :PARSER ((stream)
	   ()))

(define-mouse-command command-menu-item
		      (command-menu-item :DOCUMENTATION "Select")
		      (command-name &KEY gesture presentation-type window)
  ;; look for a handler in the current command table.
  (with-type-decoded (name data-args)
		     presentation-type
    (declare (ignore name))
    (when (consp command-name) (setq command-name (first command-name)))
    (let ((command-table (getf data-args :COMMAND-TABLE)))
      (unless command-table (error "I should have a command table here."))
      ;; look for a valid handler in this table.
      (let ((handler (find-valid-command-menu-handler
		       command-table command-name gesture)))
	;; now take the handler and build a command from it.
	(and handler
	     (let ((command
		     (funcall (command-menu-handler-function handler)
			      :MENU-LEVEL
			      (command-menu-handler-menu-levels handler)
			      :GESTURE gesture
			      :WINDOW window)))
	       (if *Command-Menu-Test-Phase*
		   command
		   ;; otherwise we need to prompt for more arguments.
		   (catch 'ABORT-MENU-CHOOSE
		     (cons (first command)
			   (command-menu-choose-arguments
			     (first command) :INITIAL-ARGUMENTS (cdr command)
			     :GESTURE gesture
			     :COMMAND-TABLE command-table
			     :OWN-WINDOW NIL
			     :MODE :KEYBOARD
			     :FULL-RUBOUT T))))))))))


(define-type keyword-argument ((keywords))
;  :PRINTER ((keyword stream) (declare (ignore keywords)) (prin1 keyword stream))
  :PRINTER ((keyword stream) (prin1 keyword stream))	;5/1/90 (sln)
  :PARSER ((stream &KEY type)
	   (let ((alist (mapcar #'(lambda (keyword)
				    (if (symbolp keyword)
					(list (symbol-name keyword) keyword)
					keyword))
				keywords)))
	     (complete-input stream
			     #'(lambda (input-string operation)
				 (default-complete-function input-string operation
				   alist '(#\Space #\-)))
			     :ALLOW-EMPTY-INPUT T
			     :TYPE type
			     :partial-completers
			     *Standard-Completion-Delimiters*)))
  :DESCRIBER ((stream)
	      (simple-princ (if (%> (length (the list keywords)) 1) "keyword" "keywords")
			    stream)))


(define-type command-or-form ((&OPTIONAL (command-table *Command-Table*)
					 &KEY dispatch-mode))
;  :PRINTER ((command stream) (declare (ignore command-table))
  :PRINTER ((command stream)			;5/1/90 (sln)
	    ;; first try to figure out if this is a command.
	    (let* ((command-name (first command))
		   (args (cdr command))
		   (command-object (gethash command-name
					    (command-table-commands-hash-table
					      command-table))))
	      (declare (ignore args))		;5/1/90 (sln)
	      (if command-object
		  ;; use the COMMAND printer instead
		  (display command 'COMMAND :STREAM stream)
		  (display command 'COMMAND-FORM :STREAM stream))))
  :PARSER ((stream)
	   ;; first read the name of the command using completion of course.
	   (when command-table
	     (let ((first-char
		     ;; this hair is necessary to get previously entered commands to be
		     ;; mouse sensitive before the first char is typed.
		     (with-presentation-input-context
		       (`(command ,command-table) :INHERIT T) (blip)
		       (peek-char-for-query stream T)
		       (T (throw (cdr *input-context*) blip)))))
	       (cond ((compare-char #\: first-char)
		      (query `(command ,command-table) :STREAM stream
			     :PROMPT NIL))
		     ((or (compare-char #\( first-char)
			  (compare-char #\" first-char)
			  (compare-char #\, first-char))
		      (query 'COMMAND-FORM :STREAM stream :PROMPT NIL))
		     (T
		      (query `(command ,command-table) :STREAM stream
			     :PROMPT NIL)))))))


(defvar *Override-Command-Form-Activation-Chars* T)
(define-type form ()
  :PRINTER ((object stream) (prin1 object stream))
  :PARSER ((stream)
	   (with-activation-chars ('(#\Space #\CR) :override T)
	     (let ((*Override-Command-Form-Activation-Chars* NIL))
	       (multiple-value-bind (object type)
		   (query 'command-form :stream stream :PROMPT NIL :DEFAULT NIL
			   :PROVIDE-DEFAULT NIL)
		 (if (eq type 'command-form)
		     (values object 'FORM)
		     (values object type)))))))


(defvar *End-of-Form-Chars* '(#\Space #\) #\" #\CR #\Newline #\; #\TAB))
(defvar *End-of-Form-Chars-White-Space* '(#\Space #\CR #\Newline #\; #\TAB))

(define-type command-form ()
  :PRINTER ((object stream) (prin1 object stream))
  :PARSER ((stream)
	   ;; read a lisp expression
	   ;; the idea is to keep reading tokens until we win with an expression
	   (with-activation-chars (NIL :OVERRIDE T)
	     (with-token-delimiters (NIL :OVERRIDE T)
	       (let ((initial-location (read-location stream))
		     (state (window-input-editor stream)))
		 ;; read tokens until we win.
		 (do (char)
		     (())
		   (do ()
		       (())
		     (setq char (read-char-for-query stream))
		     #+debugging-ie
		     (dformat "~%In FORM Cursorpos ~D Length string ~D Scan Pointer ~D."
			      (input-editor-state-cursorpos state)
			      (length (input-editor-state-string state))
			      (input-editor-state-scan-pointer state))
		     (when (and (characterp char)
				(member char *End-of-Form-Chars*)
				(%= (input-editor-state-cursorpos state)
				    (if *Rescanning*
					(input-editor-state-scan-pointer state)
					(length (input-editor-state-string state)))))
		       (return NIL)))
		   (let* ((total-string (input-editor-state-string state))
			  (current-location (read-location stream))
			  (string (subseq total-string initial-location current-location)))
		     (if (or (null char)
			     (member char *End-of-Form-Chars*))
			 (let ((eof (gensym)))
			   (let ((form (my-read-from-string
					 string NIL eof
					 :START
					 (if (char-equal #\, (aref string 0))
					     1 0))))
			     (unless (eq form eof)
			       (when (member char *End-of-Form-Chars-White-Space*)
				 (unread-char-for-query char stream))
			       (return (values form 'command-form)))))
			 (error "What is this delimiter ~A." char)))))))))

(define-type command ((&OPTIONAL (command-table *Command-Table*)))
  :PRINTER ((command stream)
	    ;; look for the command object in the current table
	    (let* ((command-name (first command))
		   (args (cdr command))
		   (command-object (gethash command-name
					    (command-table-commands-hash-table
					      command-table)))
		   (pretty-name (and command-object
				     (cp-command-name command-object))))
	      (simple-princ pretty-name stream)
	      (when (and command-object args)
		(let ((command-args (cp-command-arguments command-object)))
		  ;; process args one at a time, check for keyword args
		  ;; specially.
		  (do ((sub-list command-args (cdr sub-list))
		       (given-args args (cdr given-args)))
		      ((or (null sub-list) (null given-args)))
		    (if (eq (first sub-list) '&KEY)
			(return
			  ;; process keyword args
			  (do ((given-keyword-args given-args (cddr given-keyword-args)))
			      ((null given-keyword-args))
			    (let ((keyword (first given-keyword-args))
				  (value (second given-keyword-args)))
			      (let ((arg-description (assoc keyword (cdr sub-list)
							    :TEST #'(lambda (x y)
								      (equalp (symbol-name x)
									      (symbol-name y)
									      )))))
				(let ((type (eval (second arg-description))))
				  (simple-princ " " stream)
				  (display value type :STREAM stream))))))
			(let ((arg-description (first sub-list))
			      (arg-value (first given-args)))
			  (let ((type (eval (second arg-description))))
			    (simple-princ " " stream)
			    (display arg-value type :STREAM stream)))))))))
  :PARSER ((stream)
	   ;; first read the name of the command using completion of course.
	   (when command-table
	     (let ((command-name (query `((command-name ,command-table))
					 :STREAM stream :PROVIDE-DEFAULT NIL :PROMPT NIL)))
	       (when command-name
		 (let ((command (gethash command-name
					 (command-table-commands-hash-table
					   command-table))))
		   ;; now that we have the command
		   ;; query for its args.
		   ;; now go through args.

		   ;;5/1/90 (sln) but command isn't reference below, so...
		   (declare (ignore command))
		   (let ((argument-function (get command-name :COMMAND-ARGUMENT-PARSER)))
		     (with-token-delimiters (#\Space)
		       (let ((values
			       (multiple-value-list (let ((arguments (funcall argument-function stream)))
						      ;; now return the function and arguments
						      (cons command-name arguments)))))
			 (let ((char (peek-char-for-query stream)))
			   (if (or (not char)
				   (and (consp char)
					;;(eq (first char) :ACTIVATION) ;; make it work for other list chars as well.
					))
			       NIL
			       (progn
				 (setq char (read-char-for-query stream))
				 #+ignore ;;3/25/89
				 (insert-delimiter-into-stream
				   stream
				   (if (consp char) (second char) char))
				 (setq char (peek-char-for-query stream))
				 (unless (and (consp char)
					      (eq (first char) :ACTIVATION))
				   (parse-error NIL NIL "No More Args to Command")))))
			 (values-list values))))))))))

(define-type-transform command-name-to-command
		       (command-name command)
		       (command-name)
  (values `(,command-name) 'command :activate NIL))

(define-type-transform command-name-to-command-or-form
		       (command-name command-or-form)
		       (command-name)
  (values `(,command-name) 'command :activate NIL))

(define-type command-name ((&OPTIONAL (command-table *Command-Table*)))
  :PRINTER ((command-name stream)
	    (if command-table
		(let ((entry (find command-name
				   (command-table-command-aarray command-table)
				   :KEY #'SECOND)))
		  (simple-princ (or (first entry) command-name) stream))
		(format stream "Couldn't find Command Table for ~A." command-name)))
  :PARSER ((stream &KEY type)
	   (let* ((possible-commands
		    (command-table-command-aarray command-table))
		  (command
		    (complete-input stream
				    #'(lambda (input-string operation)
					(default-complete-function input-string operation
					  possible-commands
					  (command-table-delims command-table)))
				    :ALLOW-ANY-INPUT NIL
				    :TYPE type
				    :partial-completers
				    *Standard-Completion-Delimiters*)))
	     command)))


(defun coerce-to-string (thing &OPTIONAL (convert-spaces-to-dashes NIL)
			 (capitalize-p NIL))
  (let ((string (cond ((stringp thing)
		       (if (or convert-spaces-to-dashes capitalize-p)
			   (setq thing (copy-seq thing))
			   thing))
		      ((symbolp thing)
		       (if (or convert-spaces-to-dashes capitalize-p)
			   (setq thing (copy-seq (symbol-name thing)))
			   (symbol-name thing)))
		      (T (format NIL "~D" thing)))))
    (when convert-spaces-to-dashes
      (nsubstitute #\- #\Space string))
    (when capitalize-p
      (nstring-capitalize string))
    string))

(define-type member ((&REST sequence) &KEY (button-type :radio-button))
  :NO-DEFTYPE T
  :PRINTER ((member stream) (simple-princ member stream))
  :PARSER ((stream &KEY type)
	   (query `(member-sequence ,sequence)
		   :STREAM stream :PROMPT NIL :PROVIDE-DEFAULT NIL))
  :describer ((stream &key type)
	      (describe-sequence sequence type stream))
  :QUERY-VALUES-DISPLAYER
  ((stream object query-identifier &KEY type)
   (query-values-choose-from-sequence
     stream sequence object query-identifier :TYPE type
     :KEY #'get-menu-item-value
     :button-type button-type)))

(define-type member-sequence ((sequence) &KEY (button-type :radio-button))
  :PRINTER ((member stream) (simple-princ member stream))
  :PARSER ((stream &KEY type)
	   (let ((possible-commands
		   (mapcar #'(lambda (element)
			       (list (typecase element
				       (SYMBOL (symbol-name element))
				       (STRING element)
				       (T (format NIL "~A" element)))
				     element))
			   sequence)))
	     (complete-input stream #'(lambda (input-string operation)
					(default-complete-function input-string operation
					  possible-commands '(#\Space #\-)))
			     :TYPE type
			     :partial-completers
			     *Standard-Completion-Delimiters*)))
  :DESCRIBER ((stream &KEY type)
	      (describe-sequence sequence type stream))
  :QUERY-VALUES-DISPLAYER
  ((stream object query-identifier &KEY type)
   (query-values-choose-from-sequence
     stream sequence object query-identifier :TYPE type
     :KEY #'get-menu-item-value
     :button-type button-type)))

(defun describe-sequence (sequence type stream)
  (cond ((null sequence) "")
	((null (cdr sequence))
	 (display (coerce-to-string (first sequence) NIL T) type :STREAM stream))
	((null (cddr sequence))
	 (display (coerce-to-string (first sequence) NIL T) type :STREAM stream)
	 (simple-princ " or " stream)
	 (display (coerce-to-string (second sequence) NIL T) type :STREAM stream))
	(T (do ((seq sequence (cdr seq)))
	       ((null seq))
	     (cond ((null (cdr seq))
		    (simple-princ "or " stream)
		    (display (coerce-to-string (first seq) NIL T) type :STREAM stream))
		   (T (display (coerce-to-string (first seq) NIL T) type :STREAM stream)
		      (simple-princ ", " stream)))))))

(define-type sequence ((type &OPTIONAL min-elements max-elements)
		       &KEY (sequence-delimiter #\,) (echo-space T))
  :NO-DEFTYPE T
  :DESCRIBER ((stream)
	      (describe-type type stream T NIL))
  :PRINTER ((sequence stream)
	    (when (consp sequence-delimiter)
	      (setq sequence-delimiter (first sequence-delimiter)))
	    (format-list sequence
			 #'(lambda (item stream)
			     (display item type :STREAM stream))
			 :STREAM stream
			 :SEPARATOR (if echo-space
					(concatenate 'string
						     (string sequence-delimiter)
						     " ")
					(string sequence-delimiter))))
  :PARSER ((stream)
	   (let ((result NIL))
	     (with-token-delimiters (sequence-delimiter)
	       (do ()
		   (())
		 (let ((partial-result (query type :STREAM stream :PROMPT NIL
					       :PROVIDE-DEFAULT NIL)))
		   (setq result (nconc result (list partial-result)))
		   (let ((delimiter (peek-char-for-query stream)))
		     (when delimiter
		       ;; no delimiter implies that we had a mouse blip
		       (unless (compare-char sequence-delimiter delimiter)
			 (compare-range (length result) min-elements max-elements
					"The Number of Entries")
			 (return NIL)))
		     (when delimiter (read-char-for-query stream))
		     #+ignore ;;3/25/89 01:08:36
		     (insert-delimiter-into-stream
		       stream (if delimiter (if (consp delimiter)
						(second delimiter) delimiter)
				  (if (consp sequence-delimiter)
				      (first sequence-delimiter) sequence-delimiter)))
		     (when (and echo-space
				(not *Rescanning*))
		       (replace-input-editor-string stream (read-location stream)
						   " "))))))
	     result)))


(define-type sequence-enumerated ((&REST types) &KEY (sequence-delimiter #\,) (echo-space T))
  :PRINTER ((sequence stream)
	    (when (consp sequence-delimiter)
	      (setq sequence-delimiter (first sequence-delimiter)))
	    (formatting-list (stream :SEPARATOR
				     (if echo-space
					 (concatenate 'string
						      (string sequence-delimiter)
						      " ")
					 (string sequence-delimiter)))
	      (do ((items sequence (cdr items))
		   (type-list types (cdr type-list)))
		  ((or (null items) (null type-list)))
		(formatting-list-element (stream)
		  (display (first items) (first type-list) :stream stream)))))
  :PARSER ((stream)
	   (let ((result NIL))
	     (with-token-delimiters (sequence-delimiter)
	       (do ((type-list types (cdr type-list)))
		   ((null type-list))
		 (let ((partial-result (query (first type-list)
					       :STREAM stream :PROMPT NIL
					       :PROVIDE-DEFAULT NIL)))
		   (setq result (nconc result (list partial-result)))
		   (let ((delimiter (peek-char-for-query stream)))
		     (when delimiter (read-char-for-query stream))
		     #+ignore ;;3/25/89
		     (insert-delimiter-into-stream
		       stream (if delimiter (if (consp delimiter)
						(second delimiter) delimiter)
				  (if (consp sequence-delimiter)
				      (first sequence-delimiter) sequence-delimiter)))
		     (when (and echo-space
				(not *Rescanning*))
		       (replace-input-editor-string stream (read-location stream)
						   " "))))))
	     result)))

(define-type pathname (() &KEY (default-version :newest) (default-type NIL)
		       (default-name NIL) dont-merge-default (direction :READ)
		       (format :NORMAL))
  :NO-DEFTYPE T
  :PRINTER ((pathname stream)
	    (unless (pathnamep pathname)
	      (setq pathname (pathname pathname)))
	    (simple-princ
	      (ecase format
		(:NORMAL (namestring pathname))
		(:DIRECTORY (directory-namestring pathname)))
	      stream))
  :PARSER ((stream &KEY type default)
	   (let ((pathname
		   (complete-input stream
				   #'(lambda (input-string operation)
				       (default-complete-pathname input-string operation
					 NIL NIL	;'(#\. #\-)
					 ))
				   :ALLOW-ANY-INPUT (not (eq direction :READ))
				   :TYPE type
				   :partial-completers NIL)))
	     (merge-pathnames (if (or dont-merge-default (not (pathnamep default)))
				  pathname
				  (merge-pathnames pathname default default-version))
			      (make-pathname :TYPE default-type :NAME default-name)))) 
  :DESCRIPTION "a pathname")

(define-type host (())
  :DESCRIPTION "a host")


(define-type keyword (())
  :NO-DEFTYPE T
  :PARSER ((stream)
	   (let ((keyword-name (read-standard-token stream)))
	     (if (equal keyword-name "")
		 (parse-error keyword-name 'keyword "the input is not a keyword")
		 (let ((keyword (intern (string-upcase
					  (string-trim '(#\Space #\TAB) keyword-name))
					(find-package "KEYWORD"))))
		   keyword))))
  :DESCRIPTION "a keyword")

(define-type token-or-type ((special-tokens otherwise-type))
  :PARSER ((stream)
	   (query `(or (alist-member :ALIST ,special-tokens)
			,otherwise-type)
		   :STREAM stream :PROMPT NIL :PROVIDE-DEFAULT NIL))
  :PRINTER ((object stream)
	    (display object
		     (if (find object special-tokens :KEY #'CDR)
			 `(alist-member :ALIST . ,special-tokens)
			 otherwise-type)
		     :STREAM stream)))

(define-type or ((&REST expressions))
  :NO-DEFTYPE T
  :PARSER ((stream)
	   (let ((success-p NIL)
		 (success-type NIL)
		 (value NIL)
		 (initial-location (read-location stream))
		 (state (window-input-editor stream)))
	     (dolist (exp expressions)
	       (catch 'OR-PARSER
		 (let ((*Parsing-Or-Type-P* T))
		   (setq value (query exp :STREAM stream :PROMPT NIL)))
		 (return (setq success-p T success-type exp)))
	       ;; we must have failed, so setup for rescan of given input for OR.
	       (setf (input-editor-state-scan-pointer state) initial-location
		     (input-editor-state-cursorpos state) initial-location)
	       (setq *Rescanning* T))
	     (if (not success-p)
		 (progn (setq *Rescanning* NIL)
			(parse-error "Not a valid input for ~A." expressions))
		 (values value success-type))))
  :DESCRIBER ((stream)
	      (format-list expressions #'(lambda (exp stream)
					   (describe-type exp stream))
			   :STREAM stream
			   :IF-TWO " or "
			   :CONJUNCTION " or ")))





(define-type and ((&REST expressions))
  :NO-DEFTYPE T
  :PARSER ((stream)
	   (let ((value (query (first expressions) :STREAM stream
				:PROMPT NIL :PROVIDE-DEFAULT NIL)))
	     (unless (dolist (exp (cdr expressions) T)
		       (unless (typep value exp)
			 (return NIL)))
	       (parse-error "Not a valid input for ~A." expressions))
	     value))
  :DESCRIBER ((stream)
	      (format-list expressions #'(lambda (exp stream)
					   (describe-type exp stream))
			   :STREAM stream
			   :IF-TWO " and "
			   :CONJUNCTION " and ")))







(define-mouse-action system-menu (T NIL
				    :DOCUMENTATION
				    "System Menu"
				    :GESTURE :RIGHT
				    :DEFINES-MENU T
				    :MENU NIL
				    :SUPPRESS-HIGHLIGHTING T
						;:CONTEXT-INDEPENDENT T
				    :BLANK-AREA T)
		     (object &REST args)
  (return-from system-menu
    (apply #'CALL-PRESENTATION-MENU T args)))



(define-type subset ((&rest keywords))
  :PARSER ((stream)
	   (query `(sequence (member . ,keywords)) :STREAM stream))
  :PRINTER ((object stream)
	    (display object `(sequence (member . ,keywords))
		     :STREAM stream)))


(define-type function-spec (())
  :PARSER ((stream)
	   (query '(and symbol (satisfies fboundp))
		  :STREAM stream)))


(define-type window (())
  :NO-DEFTYPE T
  :PRINTER ((window stream)
	    (princ (window-name window) stream))
  :PARSER ((stream &KEY type)
	   (let ((windows NIL))
	     (dolist (window *Windows*)
	       (push (list (window-name window) window)
		     windows))
	     (complete-input stream
			     #'(lambda (input-string operation)
				 (default-complete-function input-string operation
				   windows '(#\Space #\-)))
			     :TYPE type
			     :PARTIAL-COMPLETERS
			     *Standard-Completion-Delimiters*))))






(define-mouse-command execute-command-button
		      (command-button
			:gesture :select
			:documentation "Execute Command Button"
			:include-body-in-test NIL
			)
		      (conditional-forms)
  `(funcall ,conditional-forms))


(define-mouse-command popup-menu-choice
		      (popup-MENU-CHOICE
			:gesture T
			:Documentation "Select")
		      (object &KEY window gesture)
  `(com-popup-menu-choice ,object ,window ,gesture))

(define-command (com-popup-menu-choice :command-table *Query-Command-Table*)
		       ((object 'POPUP-MENU-CHOICE)
			(menu 'WINDOW)
			(gesture 'form))
  (let ((value (get-menu-item-value object menu)))
    (throw 'done (values value object gesture))))

;(define-type query-values-choice (()))
(define-type query-values-value-display (()))

(define-mouse-command select-query-values-menu-choice
		      (query-values-choice
			:gesture :select
			:documentation "Select This Choice"
			)
		      (object)
  `(com-select-query-values-menu-choice ,object))

(define-command (com-select-query-values-menu-choice
		  :COMMAND-TABLE *Query-Command-Table*)
		((object 'ALIST-VALUES-CHOICE))
  (let ((value (query-values-choice-value object))
	(choices (query-values-choice-choices object)))
    (let ((old-value (query-values-choices-old-value choices))
	  (query-identifier (query-values-choices-query-identifier choices))
	  (select-action (query-values-choices-select-action choices))
	  (stream (query-values-choices-stream choices)))
      (let ((new-value (funcall select-action value old-value))
	    (table (if (lisp:typep stream 'PROGRAM-QUERY-VALUES-PANE)
		       (program-query-values-pane-query-entry-table stream)
		       *Query-Entry-Table*)))
	(setf (gethash query-identifier table) (list new-value))
	(setf (query-values-choices-old-value choices) new-value)))))



(define-mouse-command exit-menu-choice
		      (EXIT-MENU-CHOICE
			:gesture :select
			:Documentation "Exit")
		      (object)
  `(com-exit-query-values-menu ,object))

(define-command (com-exit-query-values-menu
			 :COMMAND-TABLE *Query-Command-Table*)
		       ((object 'symbol))
  (if (eq 'abort object)
      (throw 'top-level-command-loop NIL)
      (throw 'done :exit-menu)))

(define-mouse-command edit-query-values-value-1
		      (query-values-value-display
			:GESTURE :LEFT
			:documentation "Replace this Field")
		      (object &KEY presentation)
  `(com-edit-query-values-value ,object T ,presentation))

(define-mouse-command edit-query-values-value-2
		      (query-values-value-display
			:GESTURE :MIDDLE
			:documentation "Edit this Field")
		      (object &KEY presentation)
  `(com-edit-query-values-value ,object NIL ,presentation))


(define-command (com-edit-query-values-value
			 :COMMAND-TABLE *Query-Command-Table*)
		       ((avv-object 'ALIST-VALUES-CHOICE)
			(replace-value-p 'BOOLEAN)
			(presentation T))
  (let ((object (query-values-value-object avv-object))
	(query-identifier (query-values-value-query-identifier avv-object))
	(start-presentation (query-values-value-presentation avv-object))
	(type (query-values-value-type avv-object))
	(stream (query-values-value-stream avv-object)))
    (declare (ignore object))
    (unless start-presentation
      (error "Could not Find Presentation from previous display of Edit Values - System Bug."))
    (when (and (presentation-superior start-presentation)
	       (memo-p (presentation-object (presentation-superior start-presentation))))
      (setf (memo-cache-value (presentation-object (presentation-superior start-presentation)))
	    'force-redisplay))
    (let ((start-y (presentation-top start-presentation))
	  (start-x (presentation-left start-presentation))
	  (table (if (lisp:typep stream 'PROGRAM-QUERY-VALUES-PANE)
		     (program-query-values-pane-query-entry-table stream)
		     *Query-Entry-Table*)))
      (remove-presentation stream presentation T)
      (set-cursorpos stream start-x start-y)
      (let ((*Inside-Querying-Window* NIL)
	    (*Input-Context* NIL)) ;; make sure outer accepts are not valid.
	(let* ((value (car (gethash query-identifier table)))
	       (new-value
		 (if (not replace-value-p)
		     ;; need to start with an initial input.
		     (let ((initial-string (with-output-to-string (stream)
					     (display value type :STREAM stream))))
		       (with-input-editing-options
			 (((:initial-input :override) initial-string))
			 (query type :STREAM stream
				:DEFAULT NIL :PROMPT NIL :PROMPTS-IN-LINE T)))
		     (query type :STREAM stream
			    :DEFAULT NIL :PROMPT NIL :PROMPTS-IN-LINE T))))
	  (setf (gethash query-identifier table)
		(list new-value)))))))


;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
