;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************

(cl:in-package 'ew)

(defvar *Functions-Shadowed*
	'(;; lisp:clear-input
	  lisp:format
	  #+symbolics format::format-internal
	  lisp:fresh-line
	  lisp:listen
	  lisp:peek-char
	  ;;lisp:pi
	  lisp:prin1
	  lisp:princ
	  lisp:print
	  lisp:read lisp:read-char
	  lisp:read-char-no-hang
	  lisp:read-line
	  lisp:read-preserving-whitespace
	  lisp:terpri
	  lisp:unread-char
	  lisp:write
	  lisp:write-char
	  lisp:write-line
	  lisp:write-string
	  lisp:y-or-n-p
	  lisp:yes-or-no-p
	  lisp:streamp
	  lisp:input-stream-p
	  lisp:output-stream-p
	  lisp:stream-element-type
	  lisp:close
	  lisp:force-output
	  #+lucid lucid::stream-full-duplex-p
	  #+excl excl::fast-write-string
	  ;; 6/14/90 (sln) only pcl needs this.
	  #+pcl lisp:type-of
	  lisp:typep))

(defun format(destination control-string &rest args)
  (declare (string control-string) (list args))
  (when (eq destination T) (setq destination *Standard-Output*))
  (cond ((presentation-window-p destination)
	 (let ((string (apply #'lisp-format NIL control-string args)))
	   ;; check for fresh-lines at beginning of control-string
	   (when (and (stringp control-string)
		      (%> (length control-string) 1)
		      (eql #\~ (aref control-string 0))
		      ;; search for a number of digits followed by either : or @ and finally
		      ;; by an &.
		      (do ((digit-char-p T)
			   (end (length control-string))
			   (index 1 (%1+ index)))
			  ((= index end) NIL)
			(declare (fixnum end index))
			(let ((char (aref control-string index)))
			  (cond ((eql char #\&) (return T))
				((digit-char-p char)
				 (unless digit-char-p
				   (return NIL)))
				((member char '(#\@ #\:)))
				(T (return NIL))))))
	     (fresh-line destination))
	   (display-as (:OBJECT string :TYPE 'string :STREAM destination)
	     (presentation-print-object string 'PRINC destination)))
	 NIL)
	((presentation-recording-string-p destination)
	 (let ((object (apply #'lisp-format NIL control-string args)))
	   (display-as (:STREAM destination :OBJECT object :TYPE 'expression)
	     (write-string-to-recording-string destination object))))
	(T (apply #'lisp-format destination control-string args))))

#+symbolics
(defun format-internal (STREAM CTL-STRING &REST ARGS)
  (apply (if (window-p (if (eq stream T) *Standard-Output* stream))
	     #'format #'lisp-format-internal)
	 stream ctl-string args))

(defmacro setup-input-stream (stream)
  `(cond ((eq ,stream T) (setq ,stream *terminal-io*))
	 ((not ,stream) (setq ,stream *Standard-Input*))))

(defmacro setup-output-stream (stream)
  `(cond ((eq ,stream T) (setq ,stream *terminal-io*))
	 ((not ,stream) (setq ,stream *Standard-Output*))))

(defun fresh-line (&OPTIONAL output-stream)
  #.(fast)
  (setup-output-stream output-stream)
  (cond ((presentation-window-p output-stream)
	 (multiple-value-bind (x-pos y-pos)
	     (read-cursorpos output-stream)
	   (declare (fixnum x-pos) (ignore y-pos))
	   (unless (eql (or (first (window-indentation output-stream)) 0) x-pos)
	     (indent-terpri output-stream))))
	((presentation-recording-string-p output-stream)
	 (unless (zerop (the fixnum
			     (length (presentation-recording-string-string output-stream))))
	   (write-char-to-recording-string output-stream #\CR)))
	(T (lisp-fresh-line output-stream))))

(defun listen (&OPTIONAL input-stream)
  #.(fast)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (listen-any input-stream 0)
      (lisp-listen input-stream)))

(defun peek-char (&OPTIONAL peek-type input-stream (eof-errorp t) eof-value recursive-p)
  #.(fast)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (let ((char (read-char input-stream)))
	(unread-char char input-stream)
	char)
      (lisp-peek-char peek-type input-stream eof-errorp eof-value recursive-p)))

(defun prin1 (object &OPTIONAL output-stream)
  (setup-output-stream output-stream)
  (cond ((presentation-window-p output-stream)
	 (presentation-print-object object 'PRIN1 output-stream)
	 object)
	((presentation-recording-string-p output-stream)
	 (display-as (:STREAM output-stream :OBJECT object :TYPE 'expression)
	   (write-string-to-recording-string
	     output-stream (format NIL "~S" object))))
	(T (lisp-prin1 object output-stream))))

(defun princ (object &OPTIONAL output-stream)
  #.(fast)
  (setup-output-stream output-stream)
  (cond ((presentation-window-p output-stream)
	 (presentation-print-object object 'PRINC output-stream)
	 object)
	((presentation-recording-string-p output-stream)
	 (let ((string-to-write (cond ((stringp object) object)
				      ((symbolp object) (symbol-name object))
				      ((presentation-recording-string-p object)
				       (presentation-recording-string-string object))
				      (T (format NIL "~A" object)))))
	   (if *Disable-Expression-Presentation-P*
	       (write-string-to-recording-string output-stream string-to-write)
	       (display-as (:STREAM output-stream :OBJECT object :TYPE 'expression)
		 (write-string-to-recording-string output-stream string-to-write))))
	 object)
	(T (lisp-princ object output-stream))))

(defun simple-princ (object &OPTIONAL output-stream)
  #.(fast)
  (setup-output-stream output-stream)
  (let ((*Disable-Expression-Presentation-P* T))
    (princ object output-stream)))

(defun print (object &OPTIONAL output-stream)
  #.(fast)
  (setup-output-stream output-stream)
  (cond ((presentation-window-p output-stream)
	 (indent-terpri output-stream)
	 (presentation-print-object object 'PRIN1 output-stream)
	 (presentation-print-function " " 'PRINC output-stream)
	 object)
	((presentation-recording-string-p output-stream)
	 (write-char-to-recording-string output-stream #\CR)
	 (display-as (:STREAM output-stream :OBJECT object :TYPE 'expression)
	   (write-string-to-recording-string
	     output-stream (format NIL "~S" object)))
	 (write-char-to-recording-string output-stream #\Space)
	 object)
	(T (lisp-print object output-stream))))

(defun read (&OPTIONAL input-stream (eof-errorp T) eof-value recursive-p)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (let ((form (query 'form :PROMPT NIL :PROVIDE-DEFAULT NIL)))
	form)
      (lisp-read input-stream eof-errorp eof-value recursive-p)))

(defun read-preserving-whitespace (&OPTIONAL input-stream (eof-errorp T) eof-value recursive-p)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (with-input-editing-options ((:PRESERVE-WHITESPACE T))
	(let ((form (query 'form :PROMPT NIL :PROVIDE-DEFAULT NIL)))
	  form))
      (lisp-read-preserving-whitespace input-stream eof-errorp eof-value recursive-p)))

(defun read-char (&OPTIONAL input-stream (eof-errorp t) eof-value recursive-p)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (do (char)(())
	(setq char (read-any input-stream))
	(when (characterp char)
	  (return char)))
      (lisp-read-char input-stream eof-errorp eof-value recursive-p)))

(defun read-char-no-hang (&OPTIONAL input-stream (eof-errorp t) eof-value recursive-p)
  #.(fast)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (do (char) (())
	(if (listen-any input-stream 0)
	    (progn (setq char (read-any input-stream))
		   (when (characterp char)
		     (return char)))
	    (return NIL)))
      (lisp-read-char-no-hang input-stream eof-errorp eof-value recursive-p)))

(defun read-line (&OPTIONAL input-stream (eof-errorp T) eof-value recursive-p)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (with-activation-chars ('(#\NEWLINE) :OVERRIDE T)
	(let ((line (query 'STRING :PROMPT NIL :PROVIDE-DEFAULT NIL)))
	  line))
      (lisp-read-line input-stream eof-errorp eof-value recursive-p)))

(defun terpri (&OPTIONAL output-stream)
  #.(fast)
  (setup-output-stream output-stream)
  (cond ((presentation-window-p output-stream)
	 (indent-terpri output-stream))
	((presentation-recording-string-p output-stream)
	 (write-char-to-recording-string output-stream #\CR))
	(T (lisp-terpri output-stream))))

(defun unread-char (character &OPTIONAL input-stream)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (unread-any input-stream character)
      (lisp-unread-char character input-stream)))

; 4/30/90 (SLN)
(defun write (object &REST other-args &KEY stream escape radix base circle pretty level length
	      case gensym array integer-length array-length string-length
	      bit-vector-length abbreviate-quote readably structure-contents
	      exact-float-value)
  (declare (ignore escape radix circle pretty level length case gensym array integer-length array-length string-length
		   bit-vector-length abbreviate-quote readably structure-contents exact-float-value))	; 4/30/90 (SLN)
  (setup-output-stream stream)
  (if (window-p stream)
      (let ((*Print-Base* (or base 10.)))
	(prin1 object stream))
      (apply #'lisp-write object other-args)))

(defun write-char (character &OPTIONAL output-stream)
  #.(fast)
  (setup-output-stream output-stream)
  (if (window-p output-stream)
      (if (eql #\Newline character)
	  (terpri output-stream)
	  (with-presentations-disabled
	    (presentation-print-string (string character) output-stream)))
      (lisp-write-char character output-stream)))

(defun write-line (string &OPTIONAL output-stream &KEY (start 0) end)
  (setup-output-stream output-stream)
  (if (window-p output-stream)
      (progn
	(presentation-print-string string output-stream start end)
	(terpri output-stream))
      (lisp-write-line string output-stream
		       :start start
		       :end
		       #+excl (or end (length string))
		       #-excl end)))

(defun write-string (string &OPTIONAL output-stream &KEY (start 0) end)
  (setup-output-stream output-stream)
  (if (window-p output-stream)
      (presentation-print-string string output-stream start end)
      (lisp-write-string string output-stream :start start
			 :end
			 #+excl (or end (length string))
			 #-excl end)))

(defun y-or-n-p (&OPTIONAL format-string &REST args)
  (if (window-p *Query-Io*)
      (progn
	(when format-string
	  (fresh-line *Query-Io*)
	  (apply #'format *Query-Io* format-string args))
	(let ((input (read-char *Query-Io*)))
	  (case input
	    ((#\Y #\y #\Space) T)
	    ((#\N #\n #\Rubout) NIL)
	    (T (beep)
	       (apply #'y-or-n-p format-string args)))))
      (apply #'lisp-y-or-n-p format-string args)))

(defun yes-or-no-p (format-string &REST args)
  (if (window-p *Query-Io*)
      (query 'BOOLEAN :PROMPT (if format-string (apply #'format NIL format-string args))
	      :PROVIDE-DEFAULT NIL)
      (apply #'lisp-yes-or-no-p format-string args)))

(defun streamp (object)
  (or (lisp-streamp object)
      (lisp:typep object 'window)))

(defun input-stream-p (stream)
  (or (lisp:typep stream 'window)
      (lisp-input-stream-p stream)))

(defun output-stream-p (stream)
  (or (lisp:typep stream 'window)
      (lisp-output-stream-p stream)))

(defun stream-element-type (stream)
  (if (lisp:typep stream 'window)
      'character
      (lisp-stream-element-type stream)))

(defun close (stream &KEY abort)
  (if (lisp:typep stream 'WINDOW)
      NIL
      (lisp-close stream :ABORT abort)))

(defun force-output (&OPTIONAL output-stream)
  (setup-output-stream output-stream)
  (if (lisp:typep output-stream 'WINDOW)
      (progn (sync) NIL)
      (lisp-force-output output-stream)))

(defun read-any-char (&OPTIONAL input-stream (eof-errorp t) eof-value recursive-p)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (read-any input-stream)
      (lisp-read-char input-stream eof-errorp eof-value recursive-p)))

(defun unread-any-char (character &OPTIONAL input-stream)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (unread-any input-stream character)
      (lisp-unread-char character input-stream)))

(defun read-any-char-no-hang (&OPTIONAL input-stream (eof-errorp t) eof-value recursive-p)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (and (listen-any input-stream 0)
	   (read-any input-stream))
      (lisp-read-char input-stream eof-errorp eof-value recursive-p)))

(defun peek-any-char (&OPTIONAL input-stream (eof-errorp t) eof-value recursive-p)
  (setup-input-stream input-stream)
  (if (window-p input-stream)
      (peek-any input-stream)
      (lisp-peek-char input-stream eof-errorp eof-value recursive-p)))

#+lucid
(defun stream-full-duplex-p (&rest args)
  (if (lisp:typep (first args) 'window)
      NIL
      (apply #'lucid::stream-full-duplex-p args)))

;;; 7/2/90 [sln]- Franz sometimes shortcuts through EXCL::FAST-WRITE-STRING. This
;;; should fix it so that EW windows aren't barfed all over.
#+excl
(defun fast-write-string (&rest args)
  (let ((output-stream (second args)))
    (setup-output-stream output-stream)
    (apply (if (window-p output-stream)
	       #'presentation-print-string
	       #'lisp-fast-write-string)
	   args)))

(defun setup-lisp-functions (&OPTIONAL (functions *Functions-Shadowed*))
  (dolist (function functions)
    (let ((local-name (intern (concatenate 'STRING "LISP-" (symbol-name function)) (find-package "EW")))
	  (new-function (intern (symbol-name function) (find-package "EW")))
	  (cl-function function))
;      (lisp:format t "~&~s ~s ~s" local-name new-function cl-function)
      (unless (fboundp local-name)
	(setf (symbol-function local-name) (symbol-function cl-function)))
      (when (fboundp new-function)
	(setf (symbol-function cl-function) (symbol-function new-function))))))

(setup-lisp-functions)

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
