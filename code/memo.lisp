;;; -*- Mode: LISP; Syntax: Common-Lisp; Base: 10; Package: Express-windows -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************

(cl:in-package 'ew)

;; (push :DEBUGGING-MEMO *Features*)
;; (setq *Features* (delete :DEBUGGING-MEMO *Features*))


(defvar *List-Conses* 0)
(defvar *Structures-Consed* 0)

(defvar *Top-Memo* NIL)



(defvar *Real-Stream*)

#+ignore (defvar *Return-Old-Cache-Results-P* NIL)


(defun memoizing-internal (continuation)
  #.(fast)
  (make-memo :continuation continuation))




(defun make-memo-of-type (piece-flavor &KEY unique-id id-test)
  #.(fast)
  (case piece-flavor
    (:TABLE (make-table-memo :UNIQUE-ID unique-id :ID-TEST id-test))
    (:ROW (make-row-memo :UNIQUE-ID unique-id :ID-TEST id-test))
    (:CELL (make-cell-memo :UNIQUE-ID unique-id :ID-TEST id-test))
    (otherwise (make-memo :UNIQUE-ID unique-id :ID-TEST id-test))))

(defun find-matching-memo (unique-id id-test piece-flavor
				  &OPTIONAL (create-memo-p T))
  #.(fast)
  (declare (values memo old-memo-p))
  ;; first look for it on the list.
  ;; if it is on the list, than assume that any memos on list before it are obsolete.
  (when *Memo*
    (let ((memos (member unique-id *Memos* :key #'memo-unique-id
				:TEST id-test)))
      (if (or (not create-memo-p) #+ignore *Return-Old-Cache-Results-P*)
	  ;; don't worry about changing anything
	  (car memos)
	  (if memos
	      (let ((memo (first memos)))
		#+ignore
		(dformat "~&Found OLD Memo.")
		#+debugging-memo
		(dformat "~%Before Deleting Memos - old Inferiors *Memo* ~D"
			 (memo-inferiors *Memo*))
		(unless (eq *Memos* memos)	; no sense going through all the mess of
						; deleting memos if there aren't any to
						; actually delete.
		  ;; delete any memos found before this one.
		  (do* ((memos *Memos* (cdr memos))
			(r (car memos) (car memos)))
		       ((or (null memos) (eq r memo)) NIL)
		    ;; if we have the last-memos around we can delete the r directly without
		    ;; calling delete on the whole list.
		    ;; this deletes the innards of the memo, the memo itself will be deleted
		    ;; right below.
		    (delete-memo r))
		  ;; this is where we delete the memo itself.
		  (let ((superior-inferiors (memo-inferiors *Memo*)))
		    (if (eq *Memos* superior-inferiors)
			;; in this case we are deleting right from the start of the
			;; list so we can just do a simple setf.
			(setf (memo-inferiors *Memo*) memos)
			;; first find the last cons cell in the first part of the list
			;; we shall keep.
			(do ((inferiors superior-inferiors (cdr inferiors)))
			    ((null inferiors) (error "Should have found it man."))
			  (when (eq (cdr inferiors) *Memos*)
			    ;; we have found that magic cons to tack onto.
			    (return (setf (cdr inferiors) memos)))))))
		#+debugging-memo
		(dformat "~%After Deleting Memos - old Inferiors *Memo* ~D"
			 (memo-inferiors *Memo*))
		;; side effect the memo list for next time.
		(setq *Memos* (cdr memos))
		(values memo T))
	      ;; not found, so assume it is a new one.
	      ;; insert it in the list, for now do it inefficiently.
	      (let ((new-memo (make-memo-of-type
				       piece-flavor :UNIQUE-ID unique-id :ID-TEST id-test))
		    (inferiors (memo-inferiors *Memo*))
		    (first-memo (first *Memos*)))
		#+ignore
		(dformat "~&Created NEW Memo.")
		(cond ((null inferiors)
		       (setf (memo-inferiors *Memo*)
			     (list new-memo)))
		      ((null first-memo)
		       (setf (memo-inferiors *Memo*)
			     (nconc (memo-inferiors *Memo*)
				    (list new-memo))))
		      ((eq first-memo (first inferiors))
		       (push new-memo (memo-inferiors *Memo*)))
		      (T (do ((rs inferiors (cdr rs)))
			     ((null rs) (error "How could you not find it, system bug."))
			   (when (eq first-memo (second rs))
			     (return (setf (cdr rs)
					   (cons new-memo (cdr rs))))))))
		(values new-memo NIL)))))))



;; like find-matching-memo, but only looks for the first element for comparison so that
;; it is fast for the common case.
;; this is actually just here for document - it is open coded
;; at the beginning of memoizing-internal
#+ignore
(defun fast-find-matching-memo (unique-id id-test piece-flavor
				&OPTIONAL (create-memo-p T))
  ;; first look for it on the list.
  ;; if it is on the list, than assume that any memos on list before it are obsolete.
  (and *Memo*
       *Memos*
       (funcall id-test unique-id (memo-unique-id (first *Memos*)))
       ;; side effect the memo list for next time.
       (values (pop *Memos*) T)))


;;; to delete a memo we need to only delete the presentations directly associated
;;; with it and not delete any presentations from the screen for presentations from
;;; inferior memos.
;;; The redisplay will take care of any lower level memos that must be deleted.
;;; Remember of course, that a presentation from the memo itself does not get deleted
;;; from the screen.


(defun delete-memo (memo &AUX (*Erase-P* T))
  #.(fast)
  #+debugging-memo
  (progn
    (dformat "~%Before Deleting in Delete-Memo.")
    (dformat "~%*Memo-Presentation* ~D." *Memo-Presentation*)
    (dformat "~%*Memo-Presentation-Records* ~D." *Memo-Presentation-Records*)
    (dformat "~%*Memo-Presentation-Records-Previous-Cons* ~D."
	     *Memo-Presentation-Records-Previous-Cons*)
    (dformat "~%(presentation-records *Memo-Presentation*) ~D."
	     (presentation-records *Memo-Presentation*))
    (dformat "~%------------"))
  ;; need to also take care of the memos presentation
  (let ((presentation (memo-presentation memo)))
    (if *Redisplaying-Memo-P*
	(if (eq (memo-presentation memo) (first *Memo-Presentation-Records*))
	    ;; do nothing same as before.
	    (progn
	      (if (eq *Memo-Presentation-Records*
		      *Memo-Presentation-Records-Previous-Cons*)
		  ;; remove presentation from superior memo presentation itself
		  ;; since we are still at beginning of list.
		  (let ((records (presentation-records *Memo-Presentation*)))
		    #+debugging-memo
		    (dformat "~%Deleting First in Delete-Memo. Total Records ~D" records)
		    #+debugging-memo
		    (dformat "~%Memo Presentation ~D" *Memo-Presentation*)
		    (setq records (cdr records))
		    (setf (presentation-records *Memo-Presentation*) records)
		    (setq *Memo-Presentation-Records-Previous-Cons* records
			  *Memo-Presentation-Records* records))
		  ;; splice out presentation from list.
		  (progn
		    #+debugging-memo
		    (dformat "~%Deleting NOT First in Delete-Memo.")
		    (setf (cdr *Memo-Presentation-Records-Previous-Cons*)
			  (cddr *Memo-Presentation-Records-Previous-Cons*))
		    (setq *Memo-Presentation-Records*
			  (cdr *Memo-Presentation-Records-Previous-Cons*))))
	      #+debugging-memo
	      (dformat "~&Memo Presentations Previous Cons ~D."
		       *Memo-Presentation-Records-Previous-Cons*))
	    ;; different, better look for a correct one.
	    (progn
	      #+debugging-memo
	      (dformat "~%Deleting NOT First in Delete-Memo.")
	      (let ((previous-memo
		      (member (memo-presentation memo)
			      *Memo-Presentation-Records*)))
		(if (not previous-memo)
		    (error "Cannot find previous memo")
		    (error "Found previous memo")))))
	(error "Should be have and old memo unless we are redisplaying."))
    #+debugging-memo
    (progn
      (dformat "~%After Deleting in Delete-Memo.")
      (dformat "~%*Memo-Presentation* ~D." *Memo-Presentation*)
      (dformat "~%*Memo-Presentation-Records* ~D." *Memo-Presentation-Records*)
      (dformat "~%*Memo-Presentation-Records-Previous-Cons* ~D."
	       *Memo-Presentation-Records-Previous-Cons*)
      (dformat "~%(presentation-records *Memo-Presentation*) ~D."
	       (presentation-records *Memo-Presentation*))
      (dformat "~%------------"))

    (redraw-presentation presentation *Real-Stream*
			 NIL NIL NIL NIL T)))

;;; delete any presentations that are not part of another memo.

(defun delete-memo-presentations (memo stream &AUX (*Erase-P* T))
  #.(fast)
  (let* ((memo-presentation (memo-presentation memo))
	 (inferior-presentations (presentation-records memo-presentation)))
    (do ((presentations inferior-presentations (cdr presentations))
	 (previous-cons NIL))
	((null presentations))
      (if (and (presentation-p (first presentations))
	       (eq 'MEMO (presentation-type (first presentations))))
	  (setq previous-cons presentations)
	  (progn
	    ;; we have a presentation that is not the top presentation of a memo.
	    ;; it will be recreated when the body is run, so delete it.
	    ;; remove presentation
	    (if previous-cons
		(progn
		  (setf (cdr previous-cons) (cddr previous-cons))
		  ;; bogus I think.(setq previous-cons (cdr previous-cons))
		  )
		;; we must be at top of list. remove from the top level presentation
		(setf (presentation-records memo-presentation)
		      (cdr presentations)))
	    ;; based on strategy 2 below. we need to keep track of the current
	    ;; level of previous-conses
	    (setq previous-cons
		  (delete-memo-presentation-internal memo previous-cons
						     (first presentations) stream)))))))

(defun delete-memo-presentation-internal (memo previous-cons presentation stream)
  #.(fast)
  ;; Presentation has been removed from appropriate list by above.
  ;; This still needs to take care of erasing the presentation from the stream
  ;; It then recursively needs to delete inferior presentations until it hits another
  ;; memos presentation.
  (if (not (presentation-p presentation))
      (redraw-presentation presentation *Real-Stream* NIL NIL NIL NIL NIL)
      (dolist (p (presentation-records presentation))
	(if (and (presentation-p p)
		 (eq 'MEMO (presentation-type p)))
	    ;; since this is another MEMO presentation inside of a non-memo-presentation
	    ;; we will erase it for now from the superior's inferior's list.
	    ;; this is inefficient since we are deleting a memo that may not really have to
	    ;; be redisplayed.  We need to eventually figure out how to preserve this
	    ;; memo so that it can be reused within a new presentation object.
	    ;; Strategy 0.
;	  (redraw-presentation p *Real-Stream* NIL NIL NIL NIL T)
;	  (setf (memo-inferiors memo)
;		(delete (presentation-object p) (memo-inferiors memo) :COUNT 1))
;
	    ;; Strategy 1. is to have ordinary presentations also be clever about there
	    ;; inferiors when redisplaying and keep themselves around until they know
	    ;; if they have stayed around.  This gets really complicated since it
	    ;; is conceivable that the same memo might be around at a different level
	    ;; of presentations.
	    ;; Strategy 2. is to push this memo up higher in the presentation tree.
	    ;; then use it over as necessary.
	    (progn
;	    (dformat "~%Deleting Memo Presentation ~D ~D" p (presentation-object p))
;	    (dformat "~%Superiors Inferiors are ~D." (memo-inferiors memo))
;	    (dformat "~%Found P ~D."
;		     (not (null (member (presentation-object p) (memo-inferiors memo)))))
	      ;; Strategy 2.
	      #+debugging-memo
	      (dformat "~%Changing superior of memo presentation ~D" p)
	      #+debugging-memo
	      (dformat "~%Superior memo ~D records ~D" memo
		       (presentation-records (memo-presentation memo))
		       (memo-presentation memo))
	      #+debugging-memo
	      (dformat "~%Previous Cons ~D" previous-cons)
	      (if previous-cons
		  (progn
		    (setf (cdr previous-cons) (cons p (cdr previous-cons)))
		    (setq previous-cons (cdr previous-cons))
		    )
		  ;; we must be at top of list. remove from the top level presentation
		  (progn (setq previous-cons
			       (cons p (presentation-records (memo-presentation memo))))
			 (setf (presentation-records (memo-presentation memo))
			       previous-cons)))
	      #+debugging-memo
	      (dformat "~%After Change Superior memo ~D records ~D" memo
		       (presentation-records (memo-presentation memo))
		       (memo-presentation memo)))
	    (setq previous-cons
		  (delete-memo-presentation-internal memo previous-cons p stream)))))
  previous-cons)

(defvar *Disable-Caching* NIL)

(defvar *Incremental-Memo-Redo* NIL)

;;; Incremental Redisplay has three cases to take care of.
;; CASE I. - First Call
;	On the first call to a memo We must do the following:
;		1. Create a Memo
;		2. Run the Body of Code.
;		3. Create a Presentation for the memo that will be the superior
;		   of any presentations created by the body.
;		4. Remember the starting and ending locations of the cursorpos.

;; CASE II. - Second Call - Cache Value is The Same.
;	1. We shouldn't have to much of anything.  If we are inside of a new presentation
;	   being created we need to put the memos presentation on the list of
;	   new inferiors.  Otherwise, we should just be able to check that the memo's
;	   presentation is on the list of the previous superior.

;; CASE III. - Second Call - Cache Value is Different.
;	We want to save as much consing structure as possible.
;	We should not recreate the memo object or the memo's presentation object.
;	There is a high likihood that the inferior presentations are going to be unchanged.
; 	1. Put on special list the inferiors of the memo's presentation.
;	2. Bind a special variable to specify we are in this mode.  That tells any
;	   recursive inferiors to handle their presentation specially by checking
;	   for its membership on this list rather than creating a new list of inferiors.

(defun memoize-internal (stream piece-flavor cache-value unique-id
			 copy-cache-value id-test cache-test
			 continuation)
  (if (not *Memo*)
      (funcall continuation stream)
      ;; get starting location.
      (multiple-value-bind (x y)
	  (read-cursorpos stream)
	;; look for a previous memo object if any.  Create one if none around.
	(multiple-value-bind (memo old-p)
	    (if (and *Memo* ;; open code of simple case for find-matching-memo.
		     *Memos*
		     (funcall id-test unique-id (memo-unique-id (first *Memos*))))
		;; side effect the memo list for next time.
		(values (pop *Memos*) T)
		(find-matching-memo unique-id id-test piece-flavor))
	  (cond #+ignore
		(*Return-Old-Cache-Results-P*
		 (when memo (insert-memo-presentation stream
						      (memo-presentation memo))))
		;; CASE: Previous memo is good and needs no change.
		#+debugging-memo
		((and *Debug*
		      old-p
		      (dformat "~&Old Cache Value ~D New Cache Value ~D Old-X ~D Old-Y ~D New-X ~D New-Y ~D"
			       (memo-cache-value memo)
			       cache-value
			       (memo-start-cursor-x memo) (memo-start-cursor-y memo)
			       x y
			       )
		      NIL))
		((and old-p ;; True if previous memo exists.
		      (not *Disable-Caching*) ;; Debugging Flag - normally *disable-caching* is NIL
		      ;; There was a user specified cache-value
		      (not (eq cache-value 'NO-CACHE-VALUE))
		      (not (eq (memo-cache-value memo) 'force-redisplay))
		      ;; Check to see if new cache value matches old cache value.
		      (funcall cache-test
			       cache-value (memo-cache-value memo))
		      ;; check to see the old starting location is same as starting location.
		      (%= x (memo-start-cursor-x memo))
		      (or (%= y (memo-start-cursor-y memo))
			  *Optimizing-Memo*)
		      )
		 (if (or (%= y (memo-start-cursor-y memo))
			 (not *Optimizing-Memo*))
		     (memoize-internal-unchanged stream cache-value unique-id
						 copy-cache-value cache-test
						 continuation
						 x y memo)
		     (memoize-internal-optimize-unchanged stream cache-value unique-id
							  copy-cache-value cache-test
							  continuation
							  x y (memo-start-cursor-y memo) memo)))
		;; do the display for a new or changed memo.
		((not old-p) ;; CASE I. New Memo
		 (memoize-internal-new-memo stream cache-value unique-id
					    copy-cache-value cache-test
					    continuation
					    x y memo))
		((not *Immediately-Inside-Woap*)
		 (memoize-internal-changed-not-woap stream cache-value unique-id
						    copy-cache-value cache-test
						    continuation
						    x y memo))
		(T
		 (memoize-internal-changed-woap stream cache-value unique-id
						copy-cache-value cache-test
						continuation
						x y memo)))
	  memo))))


(defun memoize-internal-changed-not-woap (stream cache-value unique-id
					  copy-cache-value cache-test
					  continuation
					  x y memo)
  (declare (ignore unique-id cache-test))	; 4/30/90 (SLN)
  ;; we are inside of a memo, but not directly inside of
  ;; a display-as.
  #+debugging-memo
  (dformat "~%REDISPLAY NECESSARY not inside woap ~A ~A ~A"
	   stream unique-id cache-value)
  #+debugging-memo
  (when *Debug*
    (cond ((eq cache-value 'NO-CACHE-VALUE)
	   (dformat "~&Redisplay was necessary because there was no cache value."))
	  ((not (funcall cache-test
			 cache-value (memo-cache-value memo)))
	   (dformat "~&Redisplay was necessary because the cache was different."))
	  ((not (and (%= x (memo-start-cursor-x memo))
		     (%= y (memo-start-cursor-y memo))))
	   (dformat "~&Redisplay was necessary because the location was changed. Old ~D,~D  New ~D,~D"
		    (memo-start-cursor-x memo)
		    (memo-start-cursor-y memo)
		    x y))
	  (T (error "Don't no why redisplay."))))
  ;; delete any presentations from this memo which are not from inferior
  ;; memos.  This will remove them from the memos' presentations
  ;; list of inferior presentations.
  (delete-memo-presentations memo stream)
  ;; check to see if this presentation as before if we are inside
  ;; anothers presentation.
  (if *Redisplaying-Memo-P*
      (if (eq (memo-presentation memo)
	      (first *Memo-Presentation-Records*))
	  ;; do nothing same as before.
	  (if (eq *Memo-Presentation-Records*
		  *Memo-Presentation-Records-Previous-Cons*)
	      (progn
		#+debugging-memo
		(dformat "~&Found First One previous memo")
		(setq *Memo-Presentation-Records*
		      (cdr *Memo-Presentation-Records*)))
	      (progn
		#+debugging-memo
		(dformat "~&Found previous memo")
		(setq *Memo-Presentation-Records*
		      (cdr *Memo-Presentation-Records*)
		      *Memo-Presentation-Records-Previous-Cons*
		      (cdr *Memo-Presentation-Records-Previous-Cons*))))
	  ;; different, better look for a correct one.
	  (progn
	    #+debugging-memo
	    (dformat "~&Did not previous memo")
	    (let ((previous-memo
		    (member (memo-presentation memo)
			    *Memo-Presentation-Records*)))
	      (if (not previous-memo)
		  (error "Cannot find previous memo")
		  (error "Found previous memo")))))
      (error "Should be have and old memo unless we are redisplaying."))
  #+debugging-memo
  (dformat "~&Memo Presentations Previous Cons ~D."
	   *Memo-Presentation-Records-Previous-Cons*)
  ;; reinitialize the memo's cursorpos
  (setf (memo-start-cursor-x memo) x
	(memo-start-cursor-y memo) y
	(memo-offset-p memo) NIL)
  (let ((fake-window (allocate-fake-window stream T)))
    (unwind-protect
	(progn
	  (let* ((*Memo* memo)
		 (*Memos* (memo-inferiors memo))
		 (*Inferior-Memo-P* T)
		 (*Redisplaying-Memo-P* T)
		 (*Memo-Presentation*
		   (memo-presentation memo))
		 (*Memo-Presentation-Records*
		   (presentation-records *Memo-Presentation*))
		 (*Memo-Presentation-Records-Previous-Cons*
		   *Memo-Presentation-Records*))
	    (funcall continuation fake-window)
	    (calculate-presentation-boundaries *Memo-Presentation*)
	    (delete-tail-end-of-displayers
	      *Memo* *Memos*))
	  (multiple-value-bind (x y)
	      (read-cursorpos fake-window)
	    (set-cursorpos stream x y)
	    (setf (memo-end-cursor-x memo) x
		  (memo-end-cursor-y memo) y)))
      (deallocate-fake-window fake-window)))
  (setf (memo-cache-value memo)
	(if (and copy-cache-value (lisp:typep cache-value 'sequence))
	    (copy-seq cache-value) cache-value)))



(defun memoize-internal-changed-woap (stream cache-value unique-id
				      copy-cache-value cache-test
				      continuation
				      x y memo)
  (declare (ignore unique-id cache-test))	; 4/30/90 (SLN)
  ;; we are inside of a memo, but not directly inside of
  ;; a display-as.
  #+debugging-memo
  (dformat "~%REDISPLAY NECESSARY inside woap ~A ~A ~A"
	   stream unique-id cache-value)
  #+debugging-memo
  (when *Debug*
    (cond ((eq cache-value 'NO-CACHE-VALUE)
	   (dformat "~&Redisplay was necessary because there was no cache value."))
	  ((not (funcall cache-test
			 cache-value (memo-cache-value memo)))
	   (dformat "~&Redisplay was necessary because the cache was different."))
	  ((not (and (%= x (memo-start-cursor-x memo))
		     (%= y (memo-start-cursor-y memo))))
	   (dformat "~&Redisplay was necessary because the location was changed. Old ~D,~D  New ~D,~D"
		    (memo-start-cursor-x memo)
		    (memo-start-cursor-y memo)
		    x y))
	  (T (error "Don't no why redisplay."))))
  ;; delete any presentations from this memo which are not from inferior
  ;; memos.  This will remove them from the memos' presentations
  ;; list of inferior presentations.
  (delete-memo-presentations memo stream)
  ;; check to see if this presentation as before if we are inside
  ;; anothers presentation.
  (if (not (eq (memo-presentation memo)
	       (first *Memo-Presentation-Records*)))
      (error "Couldn't find presentation to remove from memo superior.")
      (progn
	#+debugging-memo
	(dformat "~&Before Changing Superior - Memo Presentations Previous Cons ~D."
		 *Memo-Presentation-Records-Previous-Cons*)

	;; need to remove this from the superiors list of presentations.
	(if (eq *Memo-Presentation-Records*
		*Memo-Presentation-Records-Previous-Cons*)
	    (progn
	      #+debugging-memo
	      (dformat "~&Removing First Memo Presentation for old")
	      (setf (presentation-records *Memo-Presentation*)
		    (cdr *Memo-Presentation-Records*)
		    *Memo-Presentation-Records*
		    (cdr *Memo-Presentation-Records*))
	      (setf *Memo-Presentation-Records-Previous-Cons*
		    *Memo-Presentation-Records*))
	    (progn
	      #+debugging-memo
	      (dformat "~&Removing Non-First Memo Presentation for old")
	      (setf (cdr *Memo-Presentation-Records-Previous-Cons*)
		    (cddr *Memo-Presentation-Records-Previous-Cons*))
	      (setf *Memo-Presentation-Records*
		    (cdr *Memo-Presentation-Records-Previous-Cons*))))
	#+debugging-memo
	(dformat "~&Memo Presentations Previous Cons ~D."
		 *Memo-Presentation-Records-Previous-Cons*)
	;; now we need to add it to its new superiors presentations.
	(add-to-end-of-list (memo-presentation memo)
			    *Presentation-Records*
			    *Presentation-Records-Last-Cons*)
	#+debugging-memo
	(dformat "~&Presentations Previous Cons ~D."
		 *Presentation-Records-Last-Cons*)))
  #+ignore
  (if *Redisplaying-Memo-P*
      (if (eq (memo-presentation memo)
	      (first *Memo-Presentation-Records*))
	  ;; do nothing same as before.
	  (if (eq *Memo-Presentation-Records*
		  *Memo-Presentation-Records-Previous-Cons*)
	      (progn
		#+debugging-memo
		(dformat "~&Found First One previous memo")
		(setq *Memo-Presentation-Records*
		      (cdr *Memo-Presentation-Records*)))
	      (progn
		#+debugging-memo
		(dformat "~&Found previous memo")
		(setq *Memo-Presentation-Records*
		      (cdr *Memo-Presentation-Records*)
		      *Memo-Presentation-Records-Previous-Cons*
		      (cdr *Memo-Presentation-Records-Previous-Cons*))))
	  ;; different, better look for a correct one.
	  (progn
	    #+debugging-memo
	    (dformat "~&Did not previous memo")
	    (let ((previous-memo
		    (member (memo-presentation memo)
			    *Memo-Presentation-Records*)))
	      (if (not previous-memo)
		  (error "Cannot find previous memo")
		  (error "Found previous memo")))))
      (error "Should be have and old memo unless we are redisplaying."))
  #+debugging-memo
  (dformat "~&Memo Presentations Previous Cons ~D."
	   *Memo-Presentation-Records-Previous-Cons*)
  ;; reinitialize the memo's cursorpos
  (setf (memo-start-cursor-x memo) x
	(memo-start-cursor-y memo) y
	(memo-offset-p memo) NIL)
  #+debugging-memo
  (dformat "~%Before Redisplay of Memo.")
  #+debugging-memo
  (dformat "~%Memo ~D   Memo Presentation ~D" memo (memo-presentation memo))
  #+debugging-memo
  (dformat "~%Memo Presentation inferiors ~D~%"
	   (presentation-records (memo-presentation memo)))
  (let ((fake-window (allocate-fake-window stream T)))
    (unwind-protect
	(progn
	  (let* ((*Memo* memo)
		 (*Memos* (memo-inferiors memo))
		 (*Inferior-Memo-P* T)
		 (*Redisplaying-Memo-P* T)
		 (*Memo-Presentation*
		   (memo-presentation memo))
		 (*Memo-Presentation-Records*
		   (presentation-records *Memo-Presentation*))
		 (*Memo-Presentation-Records-Previous-Cons*
		   *Memo-Presentation-Records*)
		 (*Immediately-Inside-Woap* NIL))
	    (funcall continuation fake-window)
	    #+debugging-memo
	    (dformat "~%*Memo-Presentation-Records* ~D" *Memo-Presentation-Records*)
	    #+debugging-memo
	    (dformat "~%*Memo-Presentation-Records-previous-cons* ~D"
		     *Memo-Presentation-Records-previous-cons*)
	    #+debugging-memo
	    (dformat "~%*Memos* ~D" *Memos*)
	    (calculate-presentation-boundaries *Memo-Presentation*)
	    (delete-tail-end-of-displayers
	      *Memo* *Memos*))
	  (multiple-value-bind (x y)
	      (read-cursorpos fake-window)
	    (set-cursorpos stream x y)
	    (setf (memo-end-cursor-x memo) x
		  (memo-end-cursor-y memo) y)))
      (deallocate-fake-window fake-window)))
  #+debugging-memo
  (dformat "~%After Redisplay of Memo.")
  #+debugging-memo
  (dformat "~%Memo ~D   Memo Presentation ~D" memo (memo-presentation memo))
  #+debugging-memo
  (dformat "~%Memo Presentation inferiors ~D~%"
	   (presentation-records (memo-presentation memo)))
  (setf (memo-cache-value memo)
	(if (and copy-cache-value (lisp:typep cache-value 'sequence))
	    (copy-seq cache-value) cache-value)))

(defun memoize-internal-new-memo (stream cache-value unique-id
				  copy-cache-value cache-test
				  continuation
				  x y memo)
  (declare (ignore cache-test))
  (declare (ignore unique-id))	; 4/30/90 (SLN)
  #+debugging-memo
  (dformat "~%NEW MEMO ~A ~A ~A"
	   stream unique-id cache-value)
  ;; initialize the memo with starting cursorpos
  (setf (memo-start-cursor-x memo) x
	(memo-start-cursor-y memo) y
	(memo-offset-p memo) NIL)
  (let ((fake-window (allocate-fake-window stream T)))
    ;; setup fake window to handle cursorpos movements and font changes
    ;; however we will setup our own variable for recording presentations.
    (unwind-protect
	(progn
	  (let ((records NIL))
	    (let ((*Memo* memo)
		  (*Memos* (memo-inferiors memo))
		  (*Redisplaying-Memo-P* NIL)
		  (*Inferior-Memo-P* T)
		  (*Presentation-Records* NIL)
		  (*Presentation-Records-Last-Cons* NIL)
		  (*Inside-Record-Presentations-P* T)
		  (*Immediately-Inside-Woap* NIL))
	      (funcall continuation fake-window)
	      (setq records *Presentation-Records*))
	    #+debugging-memo
	    (dformat "~&Records ~S" records)
	    (let ((memo-presentation
		    (make-presentation :TYPE 'MEMO
				       :SINGLE-BOX T
				       :RECORDS records :OBJECT memo)))
	      (calculate-presentation-boundaries memo-presentation)
	      (dolist (record records)
		(setf (superior record) memo-presentation))
	      (setf (memo-presentation memo)
		    memo-presentation)
	      (insert-memo-presentation stream memo-presentation)))
	  ;; remember the final cursorpos
	  (multiple-value-bind (x y)
	      (read-cursorpos fake-window)
	    (set-cursorpos stream x y)
	    (setf (memo-end-cursor-x memo) x
		  (memo-end-cursor-y memo) y)))
      (deallocate-fake-window fake-window)))
  (setf (memo-cache-value memo)
	(if (and copy-cache-value (lisp:typep cache-value 'sequence))
	    (copy-seq cache-value) cache-value)))

(defun memoize-internal-unchanged (stream cache-value unique-id
				   copy-cache-value cache-test
				   continuation
				   x y memo)
  (declare (ignore x y cache-test copy-cache-value continuation))
  (declare (ignore unique-id cache-value))	; 4/30/90 (SLN)
  ;; CASE is true.
  #+debugging-memo
  (dformat "~%No Redisplay Necessary ~A ~A ~A"
	   stream unique-id cache-value)
	     ;; set cursorpos to correct value which was previous ending location.
	     ;; and ignore any details of its display or its inferior's display.
  (set-cursorpos stream
		 (memo-end-cursor-x memo)
		 (memo-end-cursor-y memo))
  ;; check to see if we need to place this presentation at another level
  ;; inside a display-as presentation
  (if *Immediately-Inside-Woap*
      ;; even though the memo will not need to be rerun. its positioning in the
      ;; hierarchy of presentations must be changed.
      (if (not (eq (memo-presentation memo)
		   (first *Memo-Presentation-Records*)))
	  (error "Couldn't find presentation to remove from memo superior.")
	  (progn
	    #+debugging-memo
	    (dformat "~&Before Changing Superior - Memo Presentations Previous Cons ~D."
		     *Memo-Presentation-Records-Previous-Cons*)

	    ;; need to remove this from the superiors list of presentations.
	    (if (eq *Memo-Presentation-Records*
		    *Memo-Presentation-Records-Previous-Cons*)
		(progn
		  #+debugging-memo
		  (dformat "~&Removing First Memo Presentation for old")
		  (setf (presentation-records *Memo-Presentation*)
			(cdr *Memo-Presentation-Records*)
			*Memo-Presentation-Records*
			(cdr *Memo-Presentation-Records*))
		  (setf *Memo-Presentation-Records-Previous-Cons*
			*Memo-Presentation-Records*))
		(progn
		  #+debugging-memo
		  (dformat "~&Removing Non-First Memo Presentation for old")
		  (setf (cdr *Memo-Presentation-Records-Previous-Cons*)
			(cddr *Memo-Presentation-Records-Previous-Cons*))
		  (setf *Memo-Presentation-Records*
			(cdr *Memo-Presentation-Records-Previous-Cons*))))
	    #+debugging-memo
	    (dformat "~&Memo Presentations Previous Cons ~D."
		     *Memo-Presentation-Records-Previous-Cons*)
	    ;; now we need to add it to its new superiors presentations.
	    (add-to-end-of-list (memo-presentation memo)
				*Presentation-Records*
				*Presentation-Records-Last-Cons*)
	    #+debugging-memo
	    (dformat "~&Presentations Previous Cons ~D."
		     *Presentation-Records-Last-Cons*)))
      ;; check to see if this presentation as before if we are inside
      ;; anothers presentation.
      (if *Redisplaying-Memo-P*
	  (if (eq (memo-presentation memo)
		  (first *Memo-Presentation-Records*))
	      ;; do nothing same as before.
	      (if (eq *Memo-Presentation-Records*
		      *Memo-Presentation-Records-Previous-Cons*)
		  (progn
		    #+debugging-memo
		    (dformat "~&Found First One previous memo")
		    (setq *Memo-Presentation-Records*
			  (cdr *Memo-Presentation-Records*)))
		  (progn
		    #+debugging-memo
		    (dformat "~&Found previous memo")
		    (setq *Memo-Presentation-Records*
			  (cdr *Memo-Presentation-Records*)
			  *Memo-Presentation-Records-Previous-Cons*
			  (cdr *Memo-Presentation-Records-Previous-Cons*))))
	      ;; different, better look for a correct one.
	      (progn
		#+debugging-memo
		(dformat "~&Did not previous memo")
		(let ((previous-memo
			(member (memo-presentation memo)
				*Memo-Presentation-Records*)))
		  (if (not previous-memo)
		      (error "Cannot find previous memo")
		      (error "Found previous memo")))))
	  (error "Should not have an old memo unless we are redisplaying."))))



;; The goal here is that we have a memo that has changed in Y direction only.
;; If we are lucky we can just shift this memo down by BITBLT as well as all other
;; later memos and win big on redisplay.
;; we must find the maximum x,y coordinate on the display for the bitblt
;; and then go through all remaining (memos and their presentations)
;; and update their y coordinates.


(defun memoize-internal-optimize-unchanged (stream cache-value unique-id
					    copy-cache-value cache-test
					    continuation
					    x y old-y memo)
  (let* ((top-presentation (memo-presentation *Top-Memo*))
	 (left (presentation-left top-presentation))
	 (top (presentation-top top-presentation))
	 (right (presentation-right top-presentation))
	 (bottom (presentation-bottom top-presentation))
	 (delta (- y old-y)))
    ;; start from where we are and shift down any remaining stuff on the screen.
    (if (inside-window-p *Real-Stream* left top right bottom)
	;; some part of this is on the screen.
	;; find it and shift it with a bitblt operation.
	;; of course be careful because we may actually be shifting up.
	(let ((screen-y (max 0 (convert-window-y-coord-to-screen-coord *Real-Stream* y)))
	      (screen-old-y (max 0 (convert-window-y-coord-to-screen-coord *Real-Stream* old-y)))
	      (screen-bottom (window-inside-height *Real-Stream*)))
	  ;; redraw the screen.
	  (prepare-window (*Real-Stream*)
	    (if (plusp delta)
		;; shifting down. everything from point old-y must shift down by delta.
		(progn
		  (bitblt (draw-alu :magic)
			  ;; 10 is a magic number, I think there is a bug in ALU for bitblt
			  (window-inside-width *Real-Stream*)
			  (max 0 (- screen-bottom screen-y))
			  *Real-Stream* 0 screen-old-y
			  *Real-Stream* 0 screen-y)
		  (clear-window-internal *Real-Stream*
					 0 screen-old-y
					 (window-width *Real-Stream*)
					 (min delta
					      (- screen-bottom screen-old-y))
					 NIL)
		  ;; now fix up the memos and presentations.
		  (fix-up-memos-with-delta-starting-at memo *Top-Memo* delta)
		  )
		(progn
		  ;; first need to clear out area we are copying to.
		  (clear-window-internal *Real-Stream*
					 0 screen-y
					 (window-width *Real-Stream*)
					 (min (- delta) screen-bottom)
					 NIL)
		  ;; only need to shift up bits if the old position was on the screen.
		  (when (< screen-old-y screen-bottom)
		    (bitblt (draw-alu :magic)
			    ;; 10 is a magic number, I think there is a bug in ALU for bitblt
			    (window-inside-width *Real-Stream*)
			    (max 0 (- screen-bottom screen-old-y))
			    *Real-Stream* 0 screen-old-y
			    *Real-Stream* 0 screen-y)
		    ;; if bits are shifted we also need to clear the place
		    ;; right underneath the shiftings.
		    (clear-window-internal *Real-Stream*
					   0 (- screen-bottom (min (- delta) screen-old-y))
					   (window-width *Real-Stream*)
					   (min (- delta) screen-old-y)
					   NIL))
		  ;; now fix up the memos and presentations.
		  (fix-up-memos-with-delta-starting-at memo *Top-Memo* delta)
		  (if (< screen-old-y screen-bottom)
		      ;; if we able to save some bits we only need to redraw the part of the
		      ;; screen we cleared above when shifting bits.
		      (multiple-value-bind (left top)
			  (convert-screen-coords-to-window-coords
			    *Real-Stream*
			    0
			    (- screen-bottom (min (- delta) screen-old-y)))
			(multiple-value-bind (right bottom)
			  (convert-screen-coords-to-window-coords
			    *Real-Stream*
			    (window-width *Real-Stream*)
			    screen-bottom)
			  (redraw-window *Real-Stream* left top right bottom)))
		      ;; if we didn't shift any bits we need to redraw from the new y position
		      ;; to the bottom
		      (multiple-value-bind (left top)
			  (convert-screen-coords-to-window-coords
			    *Real-Stream*
			    0 (min screen-bottom screen-y))
			(multiple-value-bind (right bottom)
			  (convert-screen-coords-to-window-coords
			    *Real-Stream*
			    (window-width *Real-Stream*)
			    screen-bottom)
			  (redraw-window *Real-Stream* left top right bottom))))))))
	(fix-up-memos-with-delta-starting-at memo *Top-Memo* delta))
    ;; go ahead and do the old stuff as if we didn't do any of the optimizing above.
    (memoize-internal-unchanged stream cache-value unique-id
				copy-cache-value cache-test
				continuation
				x y memo)))


(defun fix-up-memos-with-delta-starting-at (memo-to-start-with current-memo delta
					    &OPTIONAL (found-it NIL))
  ;; when found-it is T then we can actually set values of things.
  (dolist (inferior (memo-inferiors current-memo))
    (if (not found-it)
	(if (eq inferior memo-to-start-with)
	    ;; great we have found paydirt.  Now every memo we find we shall tell
	    ;; its subordinates to offset everything by delta.
	    (progn
	      (setq found-it T)
	      (offset-memo-for-optimize inferior delta))
	    ;; if we haven't found it yet try the children.
	    (setq found-it
		  (fix-up-memos-with-delta-starting-at memo-to-start-with inferior delta)))
	;; tell the memo to offset everything.
	(offset-memo-for-optimize inferior delta)))
  found-it)

(defun offset-memo-for-optimize (memo delta)
  (let ((presentation (memo-presentation memo)))
    (offset-presentation-tree-until-memo presentation delta)
    (incf (memo-start-cursor-y memo) delta)
    (incf (memo-end-cursor-y memo) delta)
    ;; now tell all inferior memos to change.
    (dolist (inferior (memo-inferiors memo))
      (offset-memo-for-optimize-internal inferior delta))))

(defun offset-memo-for-optimize-internal (memo delta)
  (let ((presentation (memo-presentation memo)))
    (offset-presentation-tree-until-memo presentation delta))
  (incf (memo-start-cursor-y memo) delta)
  (incf (memo-end-cursor-y memo) delta)
  ;; now tell all inferior memos to change.
  (dolist (inferior (memo-inferiors memo))
    (offset-memo-for-optimize-internal inferior delta)))

(defun offset-presentation-tree-until-memo (presentation delta
					    &OPTIONAL (recursive-p NIL))
  (if (presentation-p presentation)
      (if (and (eq 'MEMO (presentation-type presentation))
	       recursive-p)
	  NIL ;; noop.
	  (progn
	    ;;(offset-presentation presentation 0 delta)
	    ;; open code of offset-presentation for this case.
	    (incf (boxed-presentation-top presentation) delta)
	    (incf (boxed-presentation-bottom presentation) delta)
	    (dolist (inferior (presentation-records presentation))
	      (offset-presentation-tree-until-memo inferior delta T))))
      (offset-presentation presentation 0 delta)))

(defun delete-tail-end-of-displayers (memo memos)
  #.(fast)
  (when memos
    (dolist (r memos)
      (delete-memo r))
    ;; now remove them from the memos-inferiors-list.
    (let ((inferiors (memo-inferiors memo)))
      (if (eq inferiors memos)
	  (setf (memo-inferiors memo) NIL)
       (do ((sub-list (cdr inferiors) (cdr sub-list))
	    (previous-cons inferiors (cdr previous-cons)))
	   ((null sub-list) (error "Couldn't find memos to remove from list."))
	 (when (eq memos sub-list)
	   (return (setf (cdr previous-cons) NIL))))))))
  
;  
;    ;; if we have the last-memos around we can delete the r directly without
;    ;; calling delete on the whole list.
;    (if previous-memos-cons
;	(setf (cdr previous-memos-cons) (cddr previous-memos-cons))
;	(setf (memo-inferiors memo)
;	      (delete r (memo-inferiors memo) :COUNT 1)))
;    (setq previous-memos-cons sub-memos)))

(defun run-memo (memo &OPTIONAL (stream *Standard-Output*)
		     &KEY fill-set-cursorpos truncate-p once-only
		     save-cursor-position limit-to-viewport)
  (warn-unimplemented-args limit-to-viewport NIL fill-set-cursorpos NIL once-only NIL)
  (let ((returning-values NIL))
    (if *Memo*
	;; if we are already inside of a running memo, then just call the body
	(error "Cannot Handle a Run memo inside of a Run Memo.")
	(prepare-window (stream)
	  (with-output-truncation (stream :HORIZONTAL truncate-p
					  :VERTICAL truncate-p)
	    (if (not (memo-start-cursor-x memo))
		(multiple-value-bind (x y)
		    (read-cursorpos stream)
		  (setf (memo-start-cursor-x memo) x
			(memo-start-cursor-y memo) y))
		(set-cursorpos
		  stream
		  (memo-start-cursor-x memo)
		  (memo-start-cursor-y memo)))
	    (let ((*Real-Stream* stream)
		  (continuation (memo-continuation memo)))
	      (if (memo-presentation memo)
		  ;; redisplaying
		  (progn
		    (delete-memo-presentations memo stream)
		    (let* ((*Memo* memo)
			   (*Top-Memo* memo)
			   (*Memos* (memo-inferiors memo))
			   (*Defer-Redraw-Presentations* T)
			   (*Deferred-Redraw-Presentations* NIL)
			   (*Redisplaying-Memo-P* T)
			   (*Memo-Presentation*
			     (memo-presentation memo))
			   (*Memo-Presentation-Records*
			     (presentation-records *Memo-Presentation*))
			   (*Memo-Presentation-Records-Previous-Cons*
			     *Memo-Presentation-Records*))
		      (setq returning-values
			    (multiple-value-list 
			      (funcall continuation stream)))
		      ;; delete tail end of memos since they don't exist anymore.
		      (delete-tail-end-of-displayers *Memo* *Memos*)
		      (with-repositioning-object-in-quad-table (stream *Memo-Presentation*)
			(calculate-presentation-boundaries *Memo-Presentation*))
		      (redraw-deferred-presentations stream)
		      NIL))
		  ;; first time
		  (let ((*Defer-Redraw-Presentations* T)
			(*Deferred-Redraw-Presentations* NIL))
		    (let ((records NIL))
		      (let* ((*Memo* memo)
			     (*Memos* (memo-inferiors memo))
			     (*Inferior-Memo-P* T)
			     (*Presentation-Records* NIL)
			     (*Presentation-Records-Last-Cons* NIL)
			     (*Inside-Record-Presentations-P* T)
			     (*Immediately-Inside-Woap* NIL))
			(setq returning-values
			      (multiple-value-list 
				(funcall continuation stream)))
			(setq records *Presentation-Records*))
		      (let ((memo-presentation
			      (make-presentation :TYPE 'MEMO
						 :SINGLE-BOX T
						 :RECORDS records :OBJECT memo)))
			(calculate-presentation-boundaries memo-presentation)
			(dolist (record records)
			  (setf (superior record) memo-presentation))
			(setf (memo-presentation memo) memo-presentation)
			(insert-memo-presentation stream memo-presentation)
			(redraw-deferred-presentations stream))
		      NIL)))
	      (when save-cursor-position
		(set-cursorpos stream
			       (memo-start-cursor-x memo)
			       (memo-start-cursor-y memo)))))))
    (values-list returning-values)))

;(defun clear-all ()
;  (setq *windows* ()
;	*program-frameworks* NIL))



(defun memo-format (stream format-string &REST format-args)
  (declare (list format-args))
  (when (eq stream T) (setq stream *Standard-Output*))
  (memoize (:STREAM stream :ID format-string
		    :VALUE
		    (case (length format-args)
		      (0 T)
		      (1 (first format-args))
		      (T `(list . ,(copy-list format-args))))
		    :VALUE-TEST #'equal)
    (apply #'FORMAT stream format-string format-args)))

(defun memo-display (object &OPTIONAL (type (type-of object))
		     &REST options &KEY (stream *standard-output*)
		     id &ALLOW-OTHER-KEYS)
  (memoize (:STREAM stream :ID id :VALUE object)
    (apply #'DISPLAY object type :stream stream options)))


(defun memo-write-string (string &OPTIONAL (stream *Standard-Output*))
  (memoize (:STREAM stream :VALUE string)
    (princ string stream)))




#+ignore
(defvar *sample-list*
;	'(1 2 3)
	;#+ignore
	'(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22
			      23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38)
  ;;'(1 2 3)
  )

#+ignore
(defun sample-optimize ()
  (clear-history)
  (let* ((list (copy-list *sample-list*))
	 (d (memo ()
	      (dolist (number list)
		(memoize (:VALUE number :ID number)
		  (simple-princ number)
		  (terpri))))))
    (run-memo d)
;    (read-char)
;    (setf (cdr list) (cons 95 (cdr list)))
;    (run-memo d)
    (read-char)
    (setf (cdr list) (cddr list))
    (run-memo d)))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
