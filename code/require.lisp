;;; -*- Mode: Lisp; Base: 10; Package: CL-USER; Syntax: Common-Lisp; -*-

;;; -----------------------------------------------------------------
;;;
;;;	Look for lines beginning with ";;; USER CUSTOMIZATION ---" for
;;;	hints on places to modify to reflect the setup at your site.
;;;
;;; -----------------------------------------------------------------

;;; ------------------------------
;;;	IN-PACKAGE
;;; ------------------------------

(cl:in-package :cl-user)

(eval-when (load eval)

;;; -----------------------------------------------------------------
;;;
;;;	LOOP	LOOP	LOOP	LOOP	LOOP	LOOP	LOOP	LOOP
;;;
;;; -----------------------------------------------------------------

  ;; Make sure the LOOP macro is loaded.  Lispms and Lucid already have it.
  #-lispm
  (unless (member :loop *features*)
    (require :loop #+excl (merge-pathnames "loop" EXCL::*LIBRARY-CODE-FASL-PATHNAME*)))

;;; -----------------------------------------------------------------
;;;
;;;	CLX	CLX	CLX	CLX	CLX	CLX	CLX	CLX
;;;
;;; -----------------------------------------------------------------

  ;; This is where you do whatever is necessary to insure that CLX is
  ;; loaded.

  ;; Note: On Symbolics, this isn't necessary as it is specified in the
  ;; DEFSYSTEM for EW.

  ;; Note: for Franz Allegro Common Lisp, to load the version of CLX
  ;; provided with Allegro you may substitute this entire form with
  ;; "(require :clx)"

  #-lispm
  (let ((*CLX-DIRECTORY* "~pub-lisp/clx/clx4.3/"))
;;; USER CUSTOMIZATION --^^^^^^^^^^^^^^^^^^^^^^^
    (declare (special *CLX-DIRECTORY*))
    (unless (member :xlib *features*)
      (require :clx (merge-pathnames "defsystem.lisp" *CLX-DIRECTORY*)))
    (unless (member :xlib *features*)
      #-excl (funcall (intern "LOAD-CLX" :user) *CLX-DIRECTORY* :macrosp t)
      #+excl (let ((dir (excl:current-directory))
		   (sys:*source-file-types* '("lisp" "cl" nil)))
	       (excl:chdir *CLX-DIRECTORY*)
	       (load-system :clx)
	       (excl:chdir dir)))
    (pushnew :clx *features*))

;;; -----------------------------------------------------------------
;;;
;;;	CLOS	CLOS	CLOS	CLOS	CLOS	CLOS	CLOS	CLOS
;;;
;;; -----------------------------------------------------------------

  ;; This is where you do whatever is necessary to insure a CLOS
  ;; implementation is loaded.  

  ;; Note: on Symbolics, if release 8.0 or greater this assumes the
  ;; existence of CLOS, if pre release 8.0, it assumes that PCL is the CLOS
  ;; implementation of choice.

  #+excl (require :pcl)
  (let ((*CLOS-DIRECTORY* #-lispm "~pub-lisp/pcl/may/" #+lispm "PCL:PCL;MAY;"))
;;; USER CUSTOMIZATION ---^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    (declare (special *CLOS-DIRECTORY*))
    (cond #+symbolics
	  ((>= 8 (sct:get-release-version))
	   ;; if we're on Genera 8.0 let's use the native CLOS by just
	   ;; adding "EW-CLOS" as a nickname for the CLOS package.
	   (rename-package :clos :clos (ADJOIN "EW-CLOS" (package-nicknames (find-package :clos)) :test #'string=)))
	  ;; Let's look for the only real alternative, PCL.
	  ((not (member :pcl *features*))
	   ;; if PCL is not loaded, try to load it.
	   #+lispm (unless (fs:get-logical-pathname-host "pcl" t) (fs:make-logical-pathname-host "pcl"))
	   (load (merge-pathnames "defsys" *CLOS-DIRECTORY*))
	   (funcall (intern "LOAD-PCL" :pcl))
	   (rename-package :pcl :pcl (ADJOIN "EW-CLOS" (package-nicknames (find-package :pcl)) :test #'string=)))
	  ((and (member :pcl *features*)
		(find-package :pcl)
		(not (find-package :ew-clos)))
	   ;; If PCL is around, let's add "EW-CLOS" as a nickname for the PCL package.
	   (rename-package :pcl :pcl (ADJOIN "EW-CLOS" (package-nicknames (find-package :pcl)) :test #'string=))))
    ;; If PCL is loaded, let's update the *FEATURES* list so the it
    ;; reflects the version of PCL we are using (e.g., :pcl-aaai,
    ;; :pcl-victoria, :pcl-rainy, :pcl-may).
    (when (member :pcl *FEATURES*)
      (pushnew (intern (format nil "~a-~s" :pcl
			       (read-from-string
				 (symbol-value (intern "*PCL-SYSTEM-DATE*" :pcl)) nil nil
				 :start (nth 1 (multiple-value-list
						 (read-from-string (symbol-value (intern "*PCL-SYSTEM-DATE*" :pcl)))))))
		       :keyword)
	       *features*))
    ;; Let's put :EW-CLOS on the *FEATURES* list.
    (when (find-package ':ew-clos) (pushnew ':ew-clos *FEATURES*)))

;;; -----------------------------------------------------------------
;;;
;;;	DEFPACKAGE	DEFPACKAGE	DEFPACKAGE	DEFPACKAGE
;;;
;;; -----------------------------------------------------------------

  ;; This loads a portable implementation of DEFPACKAGE.  This form should
  ;; require no user customization in order to compile/load.

  ;; Note: Symbolics doesn't need to load this here as it is contained in
  ;; the defsystem form.
  #-lispm
  (unless (member "DEFPACKAGE" *MODULES* :test #'string=)
    (let ((source (merge-pathnames "define-defpackage"  *EW-SOURCE-CODE-DIRECTORY*))
	  (binary (merge-pathnames *EW-BINARY-CODE-DIRECTORY*
				   (make-pathname :name "define-defpackage" :type *Binary-File-Type*))))
      (compile-file-maybe source :output-file binary)
      (load binary)))
			     
;;; -----------------------------------------------------------------
;;;
;;;	MISCELLANEOUS	MISCELLANEOUS	MISCELLANEOUS	MISCELLANEOUS
;;;
;;; -----------------------------------------------------------------

  ;; 8/1/90 (sln) I don't particularly know why we have :X, :CLX, & :XLIB
  ;; features reference throughout the code, instead of just one of 'em, do
  ;; you?
  (pushnew :X *Features*)

  ;; 8/1/90 (sln) Not sure why this is here, either.
  #+symbolics
  (pushnew :VISUAL-640 *Features*)
  )  ;; end of (eval-when (load eval) ...)
       
;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
