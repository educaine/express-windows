;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************

(cl:in-package 'ew)


;; A quad node has a boundary which describes what area it covers for objects inside of it.



(defconstant *Quad-Height* 500.)

(defun bounding-box (presentation)
  (values (boxed-presentation-left presentation)
	  (boxed-presentation-top presentation)
	  (boxed-presentation-right presentation)
	  (boxed-presentation-bottom presentation)))

(defmacro get-quad-node (table index)
  `(if (< -1 (the fixnum ,index) *Quad-Table-Array-Size*)
       (aref (the simple-vector (quad-table-array ,table)) ,index)
       (gethash ,index (quad-table-table ,table))))

(defmacro set-quad-node (table index node)
  `(if (< -1 (the fixnum ,index) *Quad-Table-Array-Size*)
       (setf (aref (the simple-vector (quad-table-array ,table)) ,index) ,node)
       (setf (gethash ,index (quad-table-table ,table)) ,node)))

(defun insert-object-into-quad-table (stream object)
  (let ((table (presentation-window-quad-table stream))
	(right (boxed-presentation-right object))
	(bottom (boxed-presentation-bottom object)))
    (declare (fixnum right bottom))
    ;; check for scroll-bar update as well
    (when (> bottom (presentation-window-max-y-position stream))
      (update-vertical-scroll-bars stream bottom))
    (when (> right (presentation-window-max-x-position stream))
      (update-horizontal-scroll-bars stream right))
    (let ((bottom-quad-number (the (values fixnum number)  (floor bottom *Quad-Height*))))
      (declare (fixnum bottom-quad-number))
      (do ((index (the (values fixnum number)
		       (floor (boxed-presentation-top object) *Quad-Height*))
		  (%1+ index)))
	  ((> index bottom-quad-number))
	(declare (fixnum index))
	(let ((quad (get-quad-node table index))
	      (cell (list object)))
	  (unless quad
	    (setq quad (make-quad-node :TOP (%* index *Quad-Height*)
				       :BOTTOM (%+ (%* index *Quad-Height*)
						   *Quad-Height*)))
	    (set-quad-node table index quad))
	  (if (quad-node-last-cons quad)
	      (progn
		(setf (cdr (quad-node-last-cons quad)) cell)
		(setf (quad-node-last-cons quad) (cdr (quad-node-last-cons quad))))
	      ;; no last-cons means quad is empty.
	      (setf (quad-node-last-cons quad) cell
		    (quad-node-objects quad) cell)))))))

(defun delete-object-from-quad-table (stream object)
  (unless (fake-window-p stream)
    (let ((table (presentation-window-quad-table stream))
	  (top-quad-number (the (values fixnum number)
				(floor (boxed-presentation-top object) *Quad-Height*)))
	  (bottom-quad-number (the (values fixnum number)
				   (floor (boxed-presentation-bottom object)
					  *Quad-Height*))))
      (declare (fixnum bottom-quad-number))
      (do ((index top-quad-number (%+ index *Quad-Height*)))
	  ((> index bottom-quad-number))
	(declare (fixnum index))
	(let ((quad (get-quad-node table top-quad-number)))
	  (if (not quad)
	      (error "~&How can I be removing Objects from a quad that doesn't exist.")
	      (progn
		(setf (quad-node-objects quad)
		      (delete object (quad-node-objects quad) :COUNT 1))
		;; make sure that it isn't the last cons object.
		(when (eq object (first (quad-node-last-cons quad)))
		  (setf (quad-node-last-cons quad)
			(last (quad-node-objects quad)))))))))))


(defmacro with-repositioning-object-in-quad-table ((stream object) &BODY body)
  (let ((old-top (gensym "OLD-TOP"))
	(old-bottom (gensym "OLD-BOTTOM")))
    `(let ((,old-top most-positive-fixnum)
	   (,old-bottom 0))
       (declare (fixnum ,old-top ,old-bottom))
       (when ,object
	 (setq ,old-top (boxed-presentation-top ,object))
	 (setq ,old-bottom (boxed-presentation-bottom ,object)))
       ,@body
       (reposition-objects-in-quad-table ,stream ,old-top ,old-bottom ,object))))

(defun reposition-objects-in-quad-table (stream old-top old-bottom &REST objects)
  (declare (fixnum old-top old-bottom) (list objects))
  (unless (fake-window-p stream)
    (let ((table (presentation-window-quad-table stream)))
      (dolist (object objects)
	(let ((right (boxed-presentation-right object))
	      (top (boxed-presentation-top object))
	      (bottom (boxed-presentation-bottom object)))
	  (declare (fixnum right top bottom))
	  ;; check for scroll-bar update as well
	  (when (> bottom (presentation-window-max-y-position stream))
	    (update-vertical-scroll-bars stream bottom))
	  (when (> right (presentation-window-max-x-position stream))
	    (update-horizontal-scroll-bars stream right))
	  (let ((top-quad-number (the (values fixnum number)  (floor top *Quad-Height*)))
		(bottom-quad-number (the (values fixnum number) (floor bottom *Quad-Height*)))
		(old-top-quad-number (the (values fixnum number)
					  (floor old-top *Quad-Height*)))
		(old-bottom-quad-number (the (values fixnum number)
					     (floor old-bottom *Quad-Height*))))
	    (declare (fixnum top-quad-number bottom-quad-number
			     old-top-quad-number old-bottom-quad-number))
	    (do ((index (min top-quad-number old-top-quad-number) (%1+ index))
		 (top (%* (min top-quad-number old-top-quad-number) *Quad-Height*)
		      (%+ top *Quad-Height*)))
		((%> index (the fixnum (max bottom-quad-number old-bottom-quad-number))))
	      (declare (fixnum index top))
	      (let ((quad (get-quad-node table index)))
		(cond ((and (%<= old-top-quad-number index old-bottom-quad-number)
			    (%<= top-quad-number index bottom-quad-number))
		       ;; nothing changes, it was already in both places.
		       NIL)
		      ((%<= old-top-quad-number index old-bottom-quad-number)
		       ;; must be removed.
		       (if (not quad)
			   (error
			     "~&How can I be removing Objects from a quad that doesn't exist.")
			   (progn
			     (setf (quad-node-objects quad)
				   (delete object (quad-node-objects quad) :COUNT 1))
			     (when (eq object (first (quad-node-last-cons quad)))
			       (setf (quad-node-last-cons quad)
				     (last (quad-node-objects quad)))))))
		      ((%<= top-quad-number index bottom-quad-number)
		       ;; must be added.
		       (unless quad
			 (setq quad (make-quad-node :TOP top :BOTTOM (%+ top *Quad-Height*)))
			 (set-quad-node table index quad))
		       (let ((cell (list object)))
			 (if (quad-node-last-cons quad)
			     (progn
			       (setf (cdr (quad-node-last-cons quad)) cell)
			       (setf (quad-node-last-cons quad)
				     (cdr (quad-node-last-cons quad))))
			     ;; no last-cons means quad is empty.
			     (setf (quad-node-last-cons quad) cell
				   (quad-node-objects quad) cell))))
		      (T (error "How can this index not be found already.")))))))))))

(defun find-objects-near-coords (x y stream)
  (declare (fixnum x y))
  (declare (ignore x))
  (setq y (the (values fixnum number) (floor y *Quad-Height*)))
  (and (>= y 0)
       (let ((quad (get-quad-node (presentation-window-quad-table stream) y)))
	 (and quad
	      (quad-node-objects quad)))))



(defun find-greatest-y-coordinate-presentations (stream)
  (let ((maximum 0) (max-quad NIL))
    (declare (fixnum maximum))
    (maphash #'(lambda (key value)
		 (declare (fixnum key))
		 (when (> key maximum)
		   (setq max-quad value
			 maximum key)))
	     (quad-table-table (presentation-window-quad-table stream)))
    (if max-quad
	(quad-node-objects max-quad)
	(let ((array (quad-table-array (presentation-window-quad-table stream))))
	  (declare (simple-vector array))
	  (do ((x (%1- *Quad-Table-Array-Size*) (%1- x)))
	      ((< x 0) NIL)
	    (declare (fixnum x))
	    (when (aref array x)
	      (return (quad-node-objects (aref array x)))))))))


(defun empty-window-p (stream)
  (let ((table (presentation-window-quad-table stream)))
    (and (every #'not (quad-table-array table))
	 (zerop (hash-table-count (quad-table-table table))))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
