;;; -*- Mode: LISP; Syntax: Common-lisp; Package: File-Manager; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'file-manager)

(defvar *Show-Creation-Date-P* T)
(defvar *Show-Reference-Date-P* NIL)
(defvar *Show-Permissions-P* NIL)

(defstruct node
  pathname
  superior
  permissions
  link-to)

(defstruct (directory-node (:print-function print-directory-node)
			   (:include node)
			   (:conc-name dn-))
  list
  cached
  open-p)

(defun print-directory-node (node stream &REST args)
  (declare (ignore args))
  (format stream "#<D Node ~S>" (node-pathname node)))

(defstruct (file-node (:conc-name fn-)
		      (:print-function print-file-node)
		      (:include node))
  byte-size
  author
  length-in-bytes
  length-in-blocks
  modification-date
  reference-date
  creation-date
  )

(defun print-file-node (node stream &REST args)
  (declare (ignore args))
  (format stream "#<FN ~A>" (node-pathname node)))

(defvar *Directory-Depth* 0)

(define-type directory-node (())
  :no-deftype T
  :PRINTER ((node stream)
	    (format stream "~D"
		    (node-pathname node))))

(define-type file-node (())
  :no-deftype T
  :PRINTER ((node stream)
	    (if (ew::window-p stream)
		(progn
		  (princ (file-namestring (node-pathname node))
				stream)
		  (if (node-link-to node)
		      (format stream "-> ~A" (node-link-to node))
		      (progn
			(ew:tab-cursorpos stream 24. 5 :CHARACTER)
			(when (fn-length-in-blocks node)
			  (princ (fn-length-in-blocks node) stream))
			(ew:tab-cursorpos stream 40. 5 :CHARACTER)
			(format stream "~D" ;;; "~D(~D)" (fn-byte-size node)
				(fn-length-in-bytes node) )))
		  (ew:tab-cursorpos stream 55. 5 :CHARACTER)
		  (when (and *Show-Creation-Date-P*
			     (integerp (fn-creation-date node)))
		    (display (fn-creation-date node)
			     '((ew:universal-time) :brief T)
			     :STREAM stream))
		  (when (and *Show-Reference-Date-P*
			     (integerp (fn-reference-date node)))
		    (ew::simple-princ "  (" stream)
		    (display (fn-reference-date node)
			     '((ew:universal-time) :brief T) :STREAM stream)
		    (ew::simple-princ ")" stream))
		  (format stream "  ~A" (fn-author node)))
		(princ (file-namestring (node-pathname node))
		       stream))))

(eval-when (compile load eval)
(ew:define-program-framework file-manager
  :COMMAND-DEFINER
  T
  :COMMAND-TABLE
  (:INHERIT-FROM '("colon full command" "standard arguments" "standard scrolling")
   :KBD-ACCELERATOR-P NIL)
  :STATE-VARIABLES ((root-directory NIL))
  :PANES
  ((TITLE :TITLE
          :REDISPLAY-STRING "File Manager"
          :HEIGHT-IN-LINES 1
          :REDISPLAY-AFTER-COMMANDS NIL)
   (MENU :COMMAND-MENU :MENU-LEVEL :TOP-LEVEL)
   (DISPLAY :DISPLAY :REDISPLAY-FUNCTION 'REDISPLAY-FILE-MANAGER
	    :INCREMENTAL-REDISPLAY T
	    :MARGIN-COMPONENTS
	    '((ew::margin-scroll-bar :margin :LEFT :visibility :if-needed)))
   (INTERACTOR :INTERACTOR :MARGIN-COMPONENTS
	       '((ew::margin-scroll-bar :margin :LEFT :visibility :if-needed))))
  :CONFIGURATIONS
  '((main
      (:LAYOUT (main :COLUMN TITLE MENU DISPLAY INTERACTOR))
      (:SIZES
	(MAIN (TITLE 1 :LINES) (MENU :ASK-WINDOW) (INTERACTOR 5 :LINES)
	      :THEN (display :even)))))))

#|
#+ignore
((NIL :BLOCK-SIZE 36352 :SETTABLE-PROPERTIES
  (:GENERATION-RETENTION-COUNT
    :MODIFICATION-DATE :REFERENCE-DATE :CREATION-DATE
    :AUTHOR :DELETED :DONT-REAP :DONT-DELETE
    :DONT-DELETE-REASON :AUTO-EXPUNGE-INTERVAL :DEFAULT-GENERATION-RETENTION-COUNT
    :DEFAULT-LINK-TRANSPARENCIES
   :LINK-TRANSPARENCIES)
  :DISK-SPACE-DESCRIPTION
  "2226 free, 26744/28970 used (92%, 3 partitions) (LMFS records, 1 = 4544. 8-bit bytes)")
 
 ;; example of a linked file entry
 (#P"E:>mjk>lispm-init.bin.1" :LINK-TRANSPARENCIES (:WRITE :READ)
  :LINK-TO ">ALR>lispm-init.bin" :LENGTH-IN-BLOCKS 1
  :MODIFICATION-DATE 2793368912 :CREATION-DATE 2793368912 :AUTHOR "mjk")

 ;; example of a file entry.
 (#P"E:>scl>carry-tape.lisp.5" :LENGTH-IN-BLOCKS 1 :REFERENCE-DATE 2812488040
  :MODIFICATION-DATE 2811618093 :CREATION-DATE
  2811618093 :AUTHOR "alr" :BYTE-SIZE 8 :LENGTH-IN-BYTES 931)


 ;; example of a directory entry.
 (#P"E:>scl>clos.directory.1" :DEFAULT-LINK-TRANSPARENCIES (:WRITE :READ)
  :DEFAULT-GENERATION-RETENTION-COUNT NIL
  :DATE-LAST-EXPUNGED 2801312485 :AUTO-EXPUNGE-INTERVAL NIL
  :DIRECTORY T :LENGTH-IN-BLOCKS 1 :NOT-BACKED-UP T :CREATION-DATE
  2801312485 :AUTHOR "alr"))
|#

(defun find-directory-contents (node)
  #.(ew::fast)
  (let ((dlist #+symbolics
	       (fs:directory-list (make-pathname :NAME :WILD :TYPE :WILD :VERSION :WILD
						 :DEFAULTS (node-pathname node))
				  :SORTED)
	       #-symbolics
	       (get-ls-listing
		(make-pathname :NAME :WILD :TYPE :WILD :VERSION :WILD
			       :DEFAULTS (node-pathname node)))))
    #-symbolics
    (progn
      (mapc #'(lambda (n)
		(setf (node-superior n) node))
	    dlist)
      dlist)
    #+symbolics
    (mapcar #'(lambda (item)
		(if (getf (cdr item) :DIRECTORY)
		    (make-directory-node
		      :SUPERIOR node
		      :PATHNAME
		      (make-pathname :HOST (pathname-host (first item))
				     :DIRECTORY
				     #+symbolics
				     (if (eq :ROOT (pathname-directory (first item)))
					 (list (pathname-name (first item)))
					 (append (pathname-directory (first item))
						 (list (pathname-name (first item)))))
				     #-symbolics
				     (append (pathname-directory (first item))
					     (list (pathname-name (first item))))))
		    (make-file-node :PATHNAME (first item)
				    :SUPERIOR node
				    :LINK-TO (getf (cdr item) :LINK-TO)
				    :byte-size (getf (cdr item) :byte-size)
				    :author (getf (cdr item) :author)
				    :length-in-bytes (getf (cdr item) :length-in-bytes)
				    :modification-date (getf (cdr item) :modification-date)
				    :reference-date (getf (cdr item) :reference-date)
				    :creation-date (getf (cdr item) :creation-date)
				    :length-in-blocks (getf (cdr item) :length-in-blocks))))
	    (cdr dlist))))



(define-file-manager-command (exit :menu-accelerator T) ()
  (ew:exit-program))

(define-file-manager-command (edit-root :menu-accelerator T) ()
  (let ((root-pathname (query '((pathname) :DIRECTION :WRITE) :PROMPT "Enter Root Pathname")))
    (when (pathnamep root-pathname)
      (setq root-directory
	    (make-directory-node
	      :PATHNAME (make-pathname :HOST (pathname-host root-pathname)
				       :DIRECTORY
				       #+symbolics :ROOT
				       #+lucid (list :root)
				       #+excl (list :absolute :root)))))))

(define-file-manager-command (edit-directory :menu-accelerator T) ()
  (let ((root-pathname (query '((pathname) :DIRECTION :WRITE) :PROMPT "Enter Directory")))
    (when (pathnamep root-pathname)
      (setq root-directory
	    (make-directory-node
	      :PATHNAME (make-pathname :HOST (pathname-host root-pathname)
				       :DIRECTORY
				       (pathname-directory root-pathname)))))))

(define-file-manager-command (edit-homedir :menu-accelerator T) ()
  (let ((current-directory
	  #+excl (excl::current-directory)
	  #+lucid (user::pwd)
	  #+symbolics NIL
	  #-(or excl lucid symbolics)
	  (error "Not Supported for this system.")))
    #+excl (excl:chdir)
    #+lucid (user::cd)
    (let ((user-directory
	    #+excl (excl::current-directory)
	    #+lucid (user::pwd)
	    #+symbolics NIL
	    #-(or excl lucid symbolics)
	    (error "Not Supported for this system.")))
      (when (pathnamep user-directory)
	(setq root-directory
	      (make-directory-node
		:PATHNAME user-directory))))
    (when current-directory
      #+excl (excl:chdir current-directory)
      #+lucid (user::cd current-directory))))
      

(define-file-manager-command (set-options :menu-accelerator T) ()
  (let ((creation-date-p *Show-Creation-Date-P*)
	(reference-date-p *Show-Reference-Date-P*)
	(show-permissions-p *Show-Permissions-P*))
    (ew:querying-values
      (*Query-IO* :own-window T :LABEL "Choose Options for File Manager")
      (setq creation-date-p (query 'ew::boolean :stream *Query-Io*
				    :prompt "Show Creation Date"
				    :default creation-date-p)
	    reference-date-p (query 'ew::boolean :stream *Query-Io*
				     :prompt "Show Reference Date"
				     :default reference-date-p)
	    show-permissions-p (query 'ew::boolean :stream *Query-Io*
				       :prompt "Show Permissions"
				       :default show-permissions-p)))
    (setq *Show-Creation-Date-P* creation-date-p
	  *Show-Reference-Date-P* reference-date-p
	  *Show-Permissions-P* show-permissions-p)))



(defmethod redisplay-file-manager ((self file-manager) *Standard-Output*)
  (with-slots (root-directory) self
    (unless root-directory
      (setq root-directory (make-directory-node
			     :PATHNAME (make-pathname :DIRECTORY
						      #+symbolics :ROOT
						      #+lucid (list :ROOT)
						      #+excl (list :absolute :root)))))
    (ew:with-output-truncation ()
      (ew:prepare-window (*Standard-Output*)
	(display-directory root-directory *Standard-Output*)))))

(defun display-directory (directory stream)
  (fresh-line stream)
  (ew:increment-cursorpos stream *Directory-Depth* 0)
  (ew:memo-display directory 'DIRECTORY-NODE :ID directory
				:stream stream)
  (when (dn-open-p directory)
    (let ((*Directory-Depth* (ew::%+ 16. *Directory-Depth*)))
      (unless (dn-list directory)
	(setf (dn-list directory)
	      (find-directory-contents directory)))
      (dolist (file (dn-list directory))
	(fresh-line stream)
	(typecase file
	  (DIRECTORY-NODE
	    (display-directory file stream))))
      (dolist (file (dn-list directory))
	(typecase file
	  (FILE-NODE
	    (fresh-line stream)
	    (display-file file stream)))))))

(defun display-file (file stream)
  (ew:increment-cursorpos stream *Directory-Depth* 0)
  (ew:memo-display file 'FILE-NODE :ID file
				:STREAM stream :SINGLE-BOX T))

(define-mouse-command open/close-directory-trans
   (directory-node
     :documentation "Open/Close Directory"
     )
   (node)
  `(com-open/close-directory ,node))

(define-file-manager-command (com-open/close-directory)
			     ((node 'DIRECTORY-NODE))
  (setf (dn-open-p node) (not (dn-open-p node)))
  (when (and (dn-open-p node)
	     (not (dn-list node)))
    (setf (dn-list node) (find-directory-contents node))
    (unless (dn-list node)
      (setf (dn-open-p node) NIL)
      (ew:beep)
      (format *Query-Io* "~&Couldn't open Directory because it is empty."))))

(define-mouse-command close-superior-directory-translator
   (file-node
     :documentation "Close Directory"
     :TEST ((node) (node-superior node)))
   (node)
  `(com-open/close-directory ,(node-superior node)))


(define-mouse-command rename-file-translator
   (file-node :gesture NIL :documentation "Rename File")
   (node)
  `(com-rename-file ,node))

(define-file-manager-command (com-rename-file)
			     ((node 'FILE-NODE))
  (let ((new-name (query '((PATHNAME) :DIRECTION :WRITE)
			  :PROMPT
			  (format NIL "New name for file ~A" (node-pathname node)))))
    (multiple-value-bind (new-name old-truename new-truename)
	(rename-file (node-pathname node) new-name #+symbolics NIL)
      (declare (ignore new-name old-truename))
      (if (not new-truename)
	  (format T "~&Rename was not successful.")
	  (progn
	    (com-decache-directory (node-superior node))
	    (format T "~&~A renamed to ~A." (node-pathname node)
		    new-truename))))))


(define-mouse-command com-decache-directory-translator
					      (directory-node
						:gesture :meta-left
						:documentation "Decache Directory List")
					      (node)
  `(com-decache-directory ,node))

(define-file-manager-command (com-decache-directory)
			     ((node 'DIRECTORY-NODE))
  (and node
       (setf (dn-list node) NIL)))


(define-mouse-command delete-file-translator
   (file-node :gesture NIL :documentation "Delete File")
   (node)
  `(com-delete-file ,node))

(define-file-manager-command (com-delete-file)
			     ((node 'FILE-NODE))
  (let ((delete-p (query 'ew:BOOLEAN
			  :PROMPT
			  (format NIL "Delete file ~A" (node-pathname node)))))
    (when delete-p
      (delete-file (node-pathname node) #+symbolics NIL)
      (com-decache-directory (node-superior node)))))




(define-mouse-command load-file-translator
   (file-node :gesture NIL :documentation "Load File")
   (node)
  `(com-load-file ,node))

(define-file-manager-command (com-load-file)
			     ((node 'FILE-NODE))
  (load (node-pathname node)))


(define-mouse-command edit-file-translator
   (file-node :gesture NIL :documentation "Edit File")
   (node)
  `(com-edit-file ,node))

(define-file-manager-command (com-edit-file)
			     ((node 'FILE-NODE))
  (user::ed (node-pathname node)))

#+unix
(defvar *Tar-Option-Verbose-P* T)
#+unix
(defvar *Tar-Option-Default-Tape-Drive-Device* "/dev/rst0")
#+unix
(defvar *Tar-Option-Default-Tape-Drive-Type* :CARTRIDGE)
#+unix
(defvar *Tar-Option-Read-Write* :READ)
#+unix
(defvar *Tar-Option-Pathnames* NIL)

#+unix
(defun query-tar-options ()
  (querying-values (*Query-Io* :own-window T :LABEL "Options For TAR")
    (setq *Tar-Option-Read-Write*
	  (query '(alist-member :alist (("Write" . :write) ("Read" . :read)
					    ("List" . :LIST)))
		  :PROMPT "Read or Write Tape" :DEFAULT *Tar-Option-Read-Write*))
    (setq *Tar-Option-Verbose-P*
	  (query 'boolean :PROMPT "Verbose" :DEFAULT *Tar-Option-Verbose-P*))
    (setq *Tar-Option-Default-Tape-Drive-Type*
	  (query '(alist-member :alist (("Cartridge" . :CARTRIDGE)
					    ("Reel" . :REEL)))
		  :PROMPT "Tape Drive Type"
		  :DEFAULT *Tar-Option-Default-Tape-Drive-Type*))
    (setq *Tar-Option-Default-Tape-Drive-Device*
	  (query 'STRING
		  :PROMPT "Tape Drive Device"
		  :DEFAULT *Tar-Option-Default-Tape-Drive-Device*))
    (setq *Tar-Option-Pathnames*
	  (query '(sequence pathname)
		  :PROMPT (ecase *Tar-Option-Read-Write*
			    (:write "Files to Write")
			    (:read "Files to Read")
			    (:list "Files to List"))
		  :DEFAULT *Tar-Option-Pathnames*))))

#+unix
(define-file-manager-command (com-tape :menu-accelerator T) ()
  (query-tar-options))

#+unix
(defun build-tar-command-args ()
  (let ((arguments
	  (list (concatenate 'string "-"
			     (case *Tar-Option-Read-Write*
			       (:Write "c") (:read "x") (:list "t"))
			     (if *Tar-Option-Verbose-P* "v" "")
			     (if (not (equal *Tar-Option-Default-Tape-Drive-Device*
					     "/dev/rmt0"))
				 "f" "")))))
    (when (equal arguments "-")
      (setq arguments NIL))
    (setq arguments (nconc arguments
			   (if (not (equal *Tar-Option-Default-Tape-Drive-Device*
					   "rmt0"))
			       (list *Tar-Option-Default-Tape-Drive-Device*)
			       ())))
    (setq arguments (nconc arguments
			   (apply #'concatenate 'string
				  (mapcar #'(lambda (p)
					      (namestring p))
					  *Tar-Option-Pathnames*))))))

#-symbolics
(defvar *LS-Listing-args* "-Al1s")

#-symbolics
(defun get-ls-listing (pathname &AUX directory-list)
  (with-open-stream (input
		     #+lucid
		     (user::run-program
		      "ls"
		      :ARGUMENTS
		      `(,*LS-Listing-args*
			,(if (stringp pathname) pathname
			     (namestring (make-pathname :NAME NIL :TYPE NIL
							:VERSION NIL :DEFAULTS pathname))))
		      :OUTPUT :STREAM :WAIT NIL)
		     #+excl
		     (excl:run-shell-command
		       (concatenate 'string
				    "ls "
				    *LS-Listing-args*
				    " "
				    (if (stringp pathname) pathname
					(namestring
					  (make-pathname
					    :NAME NIL :TYPE NIL
					    :VERSION NIL
					    :DIRECTORY (pathname-directory pathname)
					    :device (pathname-device pathname)
					    :host (pathname-host pathname)))))
		       :OUTPUT :STREAM :WAIT NIL))
    (read-line input NIL 'eof)
    (do ()(())
      (multiple-value-bind (line eof)
	 (read-line input NIL 'eof)
	 (if (or (null line) (eq line 'eof) eof) (return NIL))
	 (push (parse-directory-listing-line line pathname)
	       directory-list)))
    (nreverse directory-list)))

#-symbolics
(defun parse-directory-listing-line (line directory)
  (declare (string line))
  (flet ((m-path (string) (make-pathname :NAME string
					 :host (pathname-host directory)
					 :device (pathname-device directory)
					 :directory (pathname-directory directory))))
    (let ((length (length line)))
      (declare (fixnum length))
      (declare (ignore length))			;5/1/90 (sln)
      ;; the first numeric characters represent the block size.
      (multiple-value-bind (block-size index)
	  (read-from-string line)
	(declare (ignore block-size))		;5/1/90 (sln)
	;;5/1/90 (sln) link-p pipe-p and file-p are not referenced, so I'm commenting them out.
	(let ((type-char (aref line index))
	      directory-p
;	      link-p pipe-p file-p
	      )
	  (case type-char
	    ((#\d #\D) (setq directory-p T))
;	    ((#\L #\l) (setq link-p T))
;	    ((#\P #\p) (setq pipe-p T))
;	    (#\- (setq file-p T))
	    )
	  (flet ((read-permission (start-index)
		   (and *Show-Permissions-P*
			(subseq line start-index (ew::%+ start-index 3)))))
	    (let ((owner-permission (read-permission (ew::%+ 1 index)))
		  (group-permission (read-permission (ew::%+ 4 index)))
		  (other-permission (read-permission (ew::%+ 7 index))))
	      (multiple-value-bind (number index)
		  (read-from-string line NIL NIL :START (ew::%+ index 10.))
		(declare (ignore number))	;5/1/90 (sln)
		(multiple-value-bind (author index)
		    (read-from-string line NIL NIL :START index)
		  (declare (ignore author))	;5/1/90 (sln)
		  (multiple-value-bind (length-in-bytes end)
		      (read-from-string line NIL NIL :START index)
		    (let ((date (subseq line end (ew::%+ end 12)))
			  (file-name (string-trim " " (subseq line (ew::%+ end 12)))))
		      ;; cheat for the date
		      (let ((long-date (concatenate 'string
						    (subseq date 0 6)
						    ", 1989 "
						    (subseq date 6))))
			(setq date (ew::parse-universal-time long-date)))
		      ;; now if last char of file-name is a slash
		      ;; than it is a directory
		      (let ((node
			      (if directory-p
				  (make-directory-node
				    ;;:SUPERIOR node
				    :PATHNAME
				    (let ((p (m-path file-name)))
				      (make-pathname :HOST (pathname-host p)
						     :DIRECTORY
						     #+lucid
						     (append (pathname-directory p)
							     (list (pathname-name p)))
						     #+excl
						     (if (equal (pathname-directory p)
								'(:absolute :root))
							 (list :absolute
							       (pathname-name p))
							 (append (pathname-directory p)
								 (list (pathname-name p)))))))
				  (make-file-node :PATHNAME (m-path file-name)
						  ;;:SUPERIOR node
						  :LINK-TO NIL
						  ;;:byte-size byte-size
						  :author NIL
						  :length-in-bytes length-in-bytes
						  :modification-date NIL
						  :reference-date NIL
						  :creation-date date
						  :length-in-blocks NIL))))
			(when *Show-Permissions-P*
			  (setf (node-permissions node)
				(list owner-permission group-permission other-permission)))
			node))))))))))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
