;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10; Patch-File:T -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */


;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************


(cl:in-package 'ew)



		

;;; ****************************************************************************************
;;; GRAPHICS PRIMITIVES
;;; ****************************************************************************************


;;; Standard Graphic Arguments
;      ;; standard graphic arguments
;      (ALU :DRAW) PATTERN STIPPLE TILE
;      COLOR (GRAY-LEVEL 1) (OPAQUE T)
;      MASK (MASK-X 0) (MASK-Y 0)
;      (FILLED T) (THICKNESS 0) (SCALE-THICKNESS T)
;      (LINE-END-SHAPE :BUTT) (LINE-JOINT-SHAPE :MITER)
;      DASHED (DASH-PATTERN) (INITIAL-DASH-PHASE 0) (DRAW-PARTIAL-DASHES T) SCALE-DASHES
;      (STREAM *STANDARD-OUTPUT*) RETURN-PRESENTATION
;      (ROTATION 0) (SCALE 1) (SCALE-X 1) (SCALE-Y 1) TRANSLATION TRANSFORM)



;rectangle (LEFT TOP RIGHT BOTTOM

;polygon (POINTS &KEY POINTS-ARE-CONVEX-P

;line (START-X START-Y END-X END-Y &KEY
;does not take standard graphic arguments FILLED

;circle (CENTER-X CENTER-Y RADIUS &KEY (INNER-RADIUS 0) (START-ANGLE 0) (END-ANGLE GRAPHICS2PI)
; CLOCKWISE JOIN-TO-PATH
;; note does not take standard graphic arguments STIPPLE, TILE, COLOR, GRAY-LEVEL,
;; RETURN-PRESENTATION

;point (X Y
;; does not take FILLED THICKNESS SCALE-THICKNESS LINE-END-SHAPE LINE-JOINT-SHAPE DASH-ARGS

;string
;(STRING X Y	&KEY (ATTACHMENT-Y BASELINE) (ATTACHMENT-X LEFT)
;	(TOWARD-X (1+ GRAPHICSSTART-X)) (TOWARD-Y GRAPHICSSTART-Y) STRETCH-P
;	CHARACTER-STYLE
;	RECORD-AS-TEXT
;
;	;; standard arguments not used. - FILLED THICKNESS SCALE-THICKNESS
;	;; LINE-END-SHAPE LINE-JOINT-SHAPE DASH-ARGUMENTS
;
;
;string-image (STRING X Y
;	D,#TD1PsT[Begin using 006 escapes](1 0 (NIL 0) (:FIX :ITALIC :NORMAL) "CPTFONTI")&KEY0 (ATTACHMENT-Y BASELINE) (ATTACHMENT-X LEFT)
;	CHARACTER-STYLE CHARACTER-SIZE STRING-WIDTH (SCALE-DOWN-ALLOWED T)

;glyph (INDEX FONT X Y &KEY


(defun draw-rectangle (left top right bottom &REST args
		       &KEY
		       (alu :DRAW) pattern stipple tile
		       color (gray-level 1) (opaque T)
		       ;; mask (mask-x 0) (mask-y 0)
		       (filled t) (thickness 0) (scale-thickness T)
		       (line-end-shape :BUTT) (line-joint-shape :MITER)
		       dashed (dash-pattern '(10 10)) (initial-dash-phase 0)
		       (draw-partial-dashes T) scale-dashes
		       (stream *Standard-Output*) return-presentation
		       (rotation 0) (scale 1) (scale-x 1) (scale-y 1) translation transform
		       &ALLOW-OTHER-KEYS
		       &AUX (stream-transform (window-transform stream))
		       (gcontext NIL))
  (declare (number left top right bottom))
  (declare (ignore args))			; 4/30/90 (SLN)
  (warn-unimplemented-args return-presentation NIL color NIL
			   ;;mask NIL mask-x 0 mask-y 0
			   )
  (unless (or (numberp alu) (symbolp alu)) (setq gcontext alu))
  (when (eq line-joint-shape :NONE) (setq line-joint-shape NIL))
  (setq line-end-shape (cdr (assoc line-end-shape '((:BUTT . :BUTT)
						    (:NO-END-POINT . :NOT-LAST)
						    (:round . :ROUND)
						    (:square . :projecting)))))
  (when (and scale-thickness (not (= scale 1))) (setq thickness (* scale thickness)))
  (setq dashed (if (and dashed (not (eq dashed :SOLID))) :DASH :SOLID))
  (when (and (eq dashed :DASH) scale-dashes (not (= scale 1)))
    (setq dash-pattern (mapcar #'(lambda (x) (* x scale)) dash-pattern)))
  (if *Erase-p* (setq alu :ERASE))
  (when (eq T pattern) (setq pattern NIL))
  (setq transform (merge-transform-arguments
		    (* scale scale-x)
		    (* scale scale-y)
		    rotation translation (or transform stream-transform)))
  (if (non-rotating-transform-p transform)
      (progn
	(multiple-value-setq (left top) (transform-coordinates transform left top))
	(multiple-value-setq (right bottom) (transform-coordinates transform right bottom))
	(make-fixnum left top right bottom)
	(when *Record-Presentations-P*
	  (let ((half-thickness (the (values fixnum number) (floor thickness 2))))
	    (record-presentation-top-level
	      stream
	      (if (and (null STIPPLE) (null TILE)
		       (null COLOR) (%= 1 GRAY-LEVEL) (eq T OPAQUE)
		       ;;(null MASK) (zerop MASK-X) (zerop MASK-Y)
		       (eq LINE-END-SHAPE :BUTT) (eq LINE-JOINT-SHAPE :MITER)
		       (eq DASHED :SOLID))
		  (make-rectangle-presentation
		    :ALU alu
		    :LEFT (%- left half-thickness)
		    :TOP (%- top half-thickness)
		    :RIGHT (%+ right half-thickness)
		    :BOTTOM (%+ bottom half-thickness)
		    :PATTERN pattern :FILLED filled :THICKNESS thickness)
		  (make-complex-rectangle-presentation
		    :ALU alu
		    :LEFT (%- left half-thickness)
		    :TOP (%- top half-thickness)
		    :RIGHT (%+ right half-thickness)
		    :BOTTOM (%+ bottom half-thickness)
		    :PATTERN pattern :FILLED filled :THICKNESS thickness
		    :STIPPLE stipple :TILE tile
		    :COLOR color :GRAY-LEVEL gray-level :OPAQUE opaque
		    ;;:MASK mask :MASK-X mask-x :MASK-Y mask-y
		    :LINE-END-SHAPE line-end-shape :LINE-JOINT-SHAPE line-joint-shape
		    :DASHED dashed :DASH-PATTERN dash-pattern
		    :INITIAL-DASH-PHASE initial-dash-phase
		    :DRAW-PARTIAL-DASHES draw-partial-dashes)))))
	(unless (fake-window-p stream)
	  (convert-window-coords-to-screen-coords-macro stream left top)
	  (convert-window-coords-to-screen-coords-macro stream right bottom)
	  (when (inside-window-p stream left top right bottom)
	    #+CLX
	    (prepare-window (stream)
	      (let ((width (%- right left))
		    (height (%- bottom top)))
		(if gcontext
		    (xlib:draw-rectangle (window-real-window stream) gcontext
					 left top width height filled)
		    (let ((gcontext (window-gcontext stream)))
		      (xlib:with-gcontext (gcontext :FUNCTION (draw-alu alu)
						    :LINE-WIDTH thickness
						    :CAP-STYLE line-end-shape
						    :JOIN-STYLE line-joint-shape
						    :LINE-STYLE dashed
						    :DASHES dash-pattern
						    :DASH-OFFSET initial-dash-phase)
			(if pattern
			    (ecase (lisp:type-of pattern)
			      (stipple-pattern
				;;(print "Printing Stipple Pattern" tv:selected-window)
				(xlib:with-gcontext (gcontext :fill-style :stippled
							      :stipple
							      (get-pattern-pixmap pattern))
				  (xlib:draw-rectangle (window-real-window stream) gcontext
						       left top width height filled))))
			    (xlib:draw-rectangle (window-real-window stream) gcontext
						 left top width height filled))))))))))
      (multiple-value-bind (x1 y1) (transform-coordinates transform left top)
	(multiple-value-bind (x2 y2) (transform-coordinates transform right top)
	  (multiple-value-bind (x3 y3) (transform-coordinates transform right bottom)
	    (multiple-value-bind (x4 y4) (transform-coordinates transform left bottom)
	      (let ((*Transform-Coordinates* NIL))
		(draw-polygon (list x1 y1 x2 y2 x3 y3 x4 y4 x1 y1)
			      :STREAM stream
			      :ALU alu :PATTERN pattern :STIPPLE stipple :TILE tile
			      :COLOR color :GRAY-LEVEL gray-level :OPAQUE opaque
			      ;; :mask mask :mask-x mask-x :mask-y mask-y
			      :FILLED filled
			      :THICKNESS thickness))))))))



(defun fast-draw-rectangle (left top right bottom stream filled-p alu)
  (convert-window-coords-to-screen-coords-macro stream left top)
  (convert-window-coords-to-screen-coords-macro stream right bottom)
  (when (inside-window-p stream left top right bottom)
    #+CLX
    (prepare-window (stream)
      (let ((width (%- right left))
	    (height (%- bottom top)))
	(let ((gcontext (window-gcontext stream)))
	  (xlib:with-gcontext (gcontext :FUNCTION (draw-alu alu))
	    (xlib:draw-rectangle (window-real-window stream) gcontext
				 left top width height filled-p)))))))



(defun draw-polygon (points
		     &KEY (points-are-convex-p nil)
		     (alu :DRAW) pattern stipple tile
		     color (gray-level 1) (opaque T)
		     ;; mask (mask-x 0) (mask-y 0)
		     (filled t) (thickness 0) (scale-thickness T)
		     (line-end-shape :BUTT) (line-joint-shape :MITER)
		     dashed (dash-pattern '(10 10)) (initial-dash-phase 0)
		     (draw-partial-dashes T) scale-dashes
		     (stream *Standard-Output*) return-presentation
		     (rotation 0) (scale 1) (scale-x 1) (scale-y 1) translation transform
		     &ALLOW-OTHER-KEYS
		     &AUX (stream-transform (window-transform stream))
		     (gcontext NIL)
		     (min-x most-positive-fixnum) (min-y most-positive-fixnum)
		     (max-x 0) (max-y 0) (number-points 0))
  (declare (ignore points-are-convex-p) (fixnum number-points min-x min-y max-x max-y))
  (warn-unimplemented-args return-presentation NIL color NIL
			   ;;mask NIL mask-x 0 mask-y 0
			   )
  (unless (or (numberp alu) (symbolp alu)) (setq gcontext alu))
  (when (eq line-joint-shape :NONE) (setq line-joint-shape NIL))
  (setq line-end-shape (cdr (assoc line-end-shape '((:BUTT . :BUTT)
						    (:NO-END-POINT . :NOT-LAST)
						    (:round . :ROUND)
						    (:square . :projecting)))))
  (when (and scale-thickness (not (= scale 1))) (setq thickness (* scale thickness)))
  (setq dashed (if (and dashed (not (eq dashed :SOLID))) :DASH :SOLID))
  (when (and (eq dashed :DASH) scale-dashes (not (= scale 1)))
    (setq dash-pattern (mapcar #'(lambda (x) (* x scale)) dash-pattern)))
  (if *Erase-p* (setq alu :ERASE))
  (when (eq T pattern) (setq pattern NIL))
  (setq transform (merge-transform-arguments
		    (* scale scale-x)
		    (* scale scale-y)
		    rotation translation (or transform stream-transform)))
  (setq points (copy-list points))
  (do ((p points (cddr p)))
      ((null p))
    (let ((x (first p))
	  (y (second p)))
      (when (and *Transform-Coordinates* transform)
	(multiple-value-setq (x y) (transform-coordinates transform x y)))
      (make-fixnum x y)
      (when (< (the fixnum x) min-x) (setq min-x x))
      (when (> (the fixnum x) max-x) (setq max-x x))
      (when (< (the fixnum y) min-y) (setq min-y y))
      (when (> (the fixnum y) max-y) (setq max-y y))
      (setf (first p) x
	    (second p) y))
    (incf number-points))
  (when (> number-points 2)
    (unless (and (%= (first points) (nth (%- number-points 2) points))
		 (%= (second points) (nth (%1- number-points) points)))
      (setq points (nconc points (list (first points) (second points))))))
  (setq thickness (floor thickness))
  (when *Record-Presentations-P*
    (record-presentation-top-level
      stream
      (if (and (null STIPPLE) (null TILE)
	       (null COLOR) (%= 1 GRAY-LEVEL) (eq T OPAQUE)
	       ;; (null MASK) (zerop MASK-X) (zerop MASK-Y)
	       (eq LINE-END-SHAPE :BUTT) (eq LINE-JOINT-SHAPE :MITER)
	       (eq DASHED :SOLID))
	  (make-polygon-presentation
	    :ALU alu
	    :POINTS points
	    :LEFT min-x :TOP min-y :RIGHT max-x :BOTTOM max-y
	    :PATTERN pattern :FILLED filled :THICKNESS thickness)
	  (make-complex-polygon-presentation
	    :ALU alu
	    :POINTS points
	    :LEFT min-x :TOP min-y :RIGHT max-x :BOTTOM max-y
	    :PATTERN pattern :FILLED filled :THICKNESS thickness
	    :STIPPLE stipple :TILE tile
	    :COLOR color :GRAY-LEVEL gray-level :OPAQUE opaque
	    ;; :MASK mask :MASK-X mask-x :MASK-Y mask-y
	    :LINE-END-SHAPE line-end-shape :LINE-JOINT-SHAPE line-joint-shape
	    :DASHED dashed :DASH-PATTERN dash-pattern
	    :INITIAL-DASH-PHASE initial-dash-phase
	    :DRAW-PARTIAL-DASHES draw-partial-dashes))))
  (unless (fake-window-p stream)
    (setq points (copy-list points))
    (do ((p points (cddr p)))
	((null p))
      (convert-window-coords-to-screen-coords-macro stream (first p) (second p)))
    (when (progn
	    (convert-window-coords-to-screen-coords-macro stream min-x min-y)
	    (convert-window-coords-to-screen-coords-macro stream max-x max-y)
	    (inside-window-p stream min-x min-y max-x max-y))
      #+CLX
      (prepare-window (stream)
	(if gcontext
	    (xlib:draw-lines (window-real-window stream)
			     gcontext points
			     :FILL-P filled)
	    (let ((gcontext (window-gcontext stream)))
	      (xlib:with-gcontext (gcontext :FUNCTION (draw-alu alu)
					    :LINE-WIDTH thickness
					    :CAP-STYLE line-end-shape
					    :JOIN-STYLE line-joint-shape
					    :LINE-STYLE dashed
					    :DASHES dash-pattern
					    :DASH-OFFSET initial-dash-phase)
		(if pattern
		    (ecase (lisp:type-of pattern)
		      (stipple-pattern
			(xlib:with-gcontext (gcontext :fill-style :stippled
						      :stipple (get-pattern-pixmap pattern))
			  (xlib:draw-lines (window-real-window stream)
					   gcontext points
					   :FILL-P filled))))
		    (xlib:draw-lines (window-real-window stream)
				     gcontext points
				     :FILL-P filled)))))))))


;(defun draw-regular-polygon (start-x start-y end-x end-y number-of-sides
;			     &KEY (handedness :LEFT)
;			     (alu :DRAW) pattern stipple tile
;			     color (gray-level 1) (opaque T)
;			     ;; mask (mask-x 0) (mask-y 0)
;			     (filled t) (thickness 0) (scale-thickness T)
;			     (line-end-shape :BUTT) (line-joint-shape :MITER)
;			     dashed (dash-pattern '(10 10)) (initial-dash-phase 0)
;			     (draw-partial-dashes T) scale-dashes
;			     (stream *Standard-Output*) return-presentation
;			     (rotation 0) (scale 1) (scale-x 1) (scale-y 1) translation transform
;			     &ALLOW-OTHER-KEYS
;			     &AUX (stream-transform (window-transform stream))
;			     (gcontext NIL)
;			     (min-x most-positive-fixnum) (min-y most-positive-fixnum)
;			     (max-x 0) (max-y 0) (number-points 0))
;  (declare (fixnum number-of-sides min-x min-y max-x max-y))
;  (warn-unimplemented-args return-presentation NIL color NIL
;			   ;;mask NIL mask-x 0 mask-y 0
;			   )
;  ;; we need to calculate the points for the polygon.
;  ;; the number of sides tells us how many degrees each side is going to get.
;  ;; a simple way is to start with the first pair of points and walk around the polygon
;  ;; some day we should probably get a more efficient algorithm
;
;  ;; Calculate the inner center of the polygon.
;  ;; (c**2 = a**2 + b**2 - 2a*b*cos(theta).
;
;  (let ((degrees-per-side (/ (* 2 pi) number-of-sides))
;	(first-angle
;  FIGURE it out and then call draw-polygon.

(defun draw-point (x y &KEY
		   (alu :DRAW) pattern stipple tile
		   color (gray-level 1) (opaque T)
		   ;; mask (mask-x 0) (mask-y 0)
		   (stream *Standard-Output*) return-presentation
		   (rotation 0) (scale 1) (scale-x 1) (scale-y 1) translation transform
		   &ALLOW-OTHER-KEYS
		   &AUX (gcontext NIL) (stream-transform (window-transform stream)))
  (warn-unimplemented-args return-presentation NIL color NIL
			   ;; mask NIL mask-x 0 mask-y 0
			   )
  (unless (or (numberp alu) (symbolp alu)) (setq gcontext alu))
  (if *Erase-p* (setq alu :ERASE))
  (when (eq T pattern) (setq pattern NIL))
  (setq transform (merge-transform-arguments
		    (* scale scale-x)
		    (* scale scale-y)
		    rotation translation (or transform stream-transform)))
  (multiple-value-setq (x y) (transform-coordinates transform x y))
  (make-fixnum x y)
  (when *Record-Presentations-P*
    (record-presentation-top-level
      stream
      (if (and (null STIPPLE) (null TILE)
	       (null COLOR) (%= 1 GRAY-LEVEL) (eq T OPAQUE)
	       ;;(null MASK) (zerop MASK-X) (zerop MASK-Y)
	       )
	  (make-point-presentation
	    :ALU alu
	    :LEFT x
	    :TOP y
	    :RIGHT (%1+ x)
	    :BOTTOM (%1+ y)
	    :PATTERN pattern)
	  (make-complex-point-presentation
	    :ALU alu
	    :LEFT x
	    :TOP y
	    :RIGHT (%1+ x)
	    :BOTTOM (%1+ y)
	    :PATTERN pattern
	    :STIPPLE stipple :TILE tile
	    :COLOR color :GRAY-LEVEL gray-level :OPAQUE opaque
	    ;; :MASK mask :MASK-X mask-x :MASK-Y mask-y
	    ))))
  (unless (fake-window-p stream)
    (convert-window-coords-to-screen-coords-macro stream x y)
    (when (inside-window-p stream (the fixnum x) (the fixnum y))
      #+CLX
      (prepare-window (stream)
	(if gcontext
	    (xlib:draw-point (window-real-window stream) gcontext
			     x y)
	    (let ((gcontext (window-gcontext stream)))
	      (xlib:with-gcontext (gcontext :FUNCTION (draw-alu alu))
		(if pattern
		    (ecase (lisp:type-of pattern)
		      (stipple-pattern
			;;(print "Printing Stipple Pattern" tv:selected-window)
			(xlib:with-gcontext (gcontext :fill-style :stippled
						      :stipple (get-pattern-pixmap pattern))
			  (xlib:draw-point (window-real-window stream) gcontext
					   x y))))
		    (xlib:draw-point (window-real-window stream) gcontext
				     x y)))))))))

(defun draw-glyph (index font x y
		   &KEY
		   (alu :DRAW) pattern stipple tile
		   color (gray-level 1) (opaque T)
		   ;; mask (mask-x 0) (mask-y 0)
		   ;; (filled t) (thickness 0) (scale-thickness T) - NOT used in draw-string
		   ;; (line-end-shape :BUTT)(line-joint-shape :MITER) not used in draw-string
		   ;; dashed (dash-pattern) (initial-dash-phase 0) - NOT used in draw-string
		   ;; (draw-partial-dashes t) scale-dashes - NOT used in draw-string
		   (stream *Standard-Output*) return-presentation
		   (rotation 0) (scale 1) (scale-x 1) (scale-y 1) translation transform
		   &ALLOW-OTHER-KEYS
		   &AUX (gcontext NIL) (stream-transform (window-transform stream)))
  (warn-unimplemented-args return-presentation NIL)
  (unless (or (numberp alu) (symbolp alu)) (setq gcontext alu))
  (setq transform (merge-transform-arguments
		    (* scale scale-x)
		    (* scale scale-y)
		    rotation translation (or transform stream-transform)))
  (multiple-value-setq (x y) (transform-coordinates transform x y))
  (make-fixnum x y)
  (if *Erase-p* (setq alu :ERASE))
  (multiple-value-bind (width height)
      (char-size-from-font index font)
    (declare (fixnum width height))
    (when *Record-Presentations-P*
      (record-presentation-top-level
	stream
	(if (and (null STIPPLE) (null TILE)
		 (null COLOR) (%= 1 GRAY-LEVEL) (eq T OPAQUE)
		 ;; (null MASK) (zerop MASK-X) (zerop MASK-Y)
		 )
	    (make-glyph-presentation
	      :ALU alu
	      :INDEX index :FONT font
	      :LEFT x :TOP (%- y (font-ascent font))
	      :RIGHT (%+ x width) :BOTTOM (%+ y (font-descent font))
	      :PATTERN pattern)
	    (make-complex-glyph-presentation
	      :ALU alu
	      :INDEX index :FONT font
	      :LEFT x :TOP (%- y (font-ascent font))
	      :RIGHT (%+ x width) :BOTTOM (%+ y (font-descent font))
	      :PATTERN pattern :STIPPLE stipple :TILE tile
	      :COLOR color :GRAY-LEVEL gray-level :OPAQUE opaque
	      ;; :MASK mask :MASK-X mask-x :MASK-Y mask-y
	      ))))
    (unless (fake-window-p stream)
      (convert-window-coords-to-screen-coords-macro stream x y)
      (when (inside-window-p stream (the fixnum x) (the fixnum y) (%+ x width) (%+ y height))
	#+CLX
	(prepare-window (stream)
	  (if gcontext
	      (xlib:draw-glyph (window-real-window stream) gcontext
			       x y index)
	      (xlib:with-gcontext ((window-gcontext stream)
				   :FONT font :FUNCTION (draw-alu alu))
		(xlib:draw-glyph (window-real-window stream) (window-gcontext stream)
				 x y index))))))))

(defun draw-triangle (x1 y1 x2 y2 x3 y3 &REST args
		       &KEY (stream *Standard-Output*)
		       (filled T) (alu :DRAW)
		       (thickness 1) (pattern NIL)
		       (rotation 0) (scale 1) (scale-x 1) (scale-y 1)
		       translation transform
		       &ALLOW-OTHER-KEYS
		       &AUX min-x max-x min-y max-y
		       (gcontext NIL)
		       (stream-transform (window-transform stream)))
  (declare (ignore args))
  (unless (or (numberp alu) (symbolp alu)) (setq gcontext alu))
  (setq transform (merge-transform-arguments
		    (* scale scale-x)
		    (* scale scale-y)
		    rotation translation (or transform stream-transform)))
  (multiple-value-setq (x1 y1) (transform-coordinates transform x1 y1))
  (multiple-value-setq (x2 y2) (transform-coordinates transform x2 y2))
  (multiple-value-setq (x3 y3) (transform-coordinates transform x3 y3))
  (make-fixnum x1 y1 x2 y2 x3 y3)
  (setq min-x (min (the fixnum x1) (the fixnum x2) (the fixnum x3))
	max-x (max (the fixnum x1) (the fixnum x2) (the fixnum x3))
	min-y (min (the fixnum y1) (the fixnum y2) (the fixnum y3))
	max-y (max (the fixnum y1) (the fixnum y2) (the fixnum y3)))
  (if *Erase-p* (setq alu :ERASE))
  (when *Record-Presentations-P*
    (record-presentation-top-level
      stream
      (make-triangle-presentation
	:ALU alu
	:x1 x1 :y1 y1 :x2 x2 :y2 y2 :x3 x3 :y3 y3
	:FILLED filled
	:PATTERN pattern
	:THICKNESS thickness
	:LEFT min-x
	:TOP min-y
	:RIGHT max-x
	:BOTTOM max-y)))
  (convert-window-coords-to-screen-coords-macro stream x1 y1)
  (convert-window-coords-to-screen-coords-macro stream x2 y2)
  (convert-window-coords-to-screen-coords-macro stream x3 y3)
  (unless (fake-window-p stream)
    (convert-window-coords-to-screen-coords-macro stream min-x min-y)
    (convert-window-coords-to-screen-coords-macro stream max-x max-y)
    (when (inside-window-p stream (the fixnum min-x) (the fixnum min-y)
			   (the fixnum max-x) (the fixnum max-y))
      #+X
      (prepare-window (stream)
	(if gcontext
	    (xlib:draw-lines (window-real-window stream) gcontext
			     (list x1 y1 x2 y2 x3 y3 x1 y1) :FILL-P filled)
	    (let ((gcontext (window-gcontext stream)))
	      (xlib:with-gcontext (gcontext :FUNCTION (draw-alu alu)
					    :LINE-WIDTH thickness)
		(xlib:draw-lines (window-real-window stream) gcontext
				 (list x1 y1 x2 y2 x3 y3 x1 y1) :FILL-P filled))))))))

(defvar *Default-Arrow-Width* 5)
(defvar *Default-Arrow-Length* 10)

(defun draw-arrow (x1 y1 x2 y2 &KEY (stream *Standard-Output*)
		   (alu *Draw-Alu*)
		   (draw-shaft T) (thickness 1)
		   (rotation 0) (scale 1) (scale-x 1) (scale-y 1)
		   translation transform
		   (arrow-head-length *Default-Arrow-Length*)
		   (arrow-head-width 10)
		   (arrow-base-width *Default-Arrow-Width*)
		   &ALLOW-OTHER-KEYS
		   &AUX min-x max-x min-y max-y
		   (gcontext NIL)
		   (stream-transform (window-transform stream)))
  (declare (ignore gcontext))			; 4/30/90 (SLN)
;  (unless (or (numberp alu) (symbolp alu)) (setq gcontext alu)) ;4/30/90 (SLN) since gcontext is not used
  (setq arrow-head-width arrow-base-width)
  (if *Erase-p* (setq alu :ERASE))
  (let ((*Record-Presentations-P* NIL))
    (when draw-shaft (draw-line x1 y1 x2 y2 :STREAM stream :THICKNESS thickness
				:ALU alu :ROTATION rotation))
    (let* ((dy (- y2 y1))
	   (dx (- x2 x1))
	   (length (sqrt (+ (* dx dx) (* dy dy)))))
      (if (zerop length)
	  NIL
	  (let* ((cos (/ dx length))
		 (sin (/ dy length))
		 (arrow-start-x (- x2 (* arrow-head-length cos)))
		 (arrow-start-y (- y2 (* arrow-head-length sin)))
		 (delta-x (* (/ arrow-head-width 2) sin))
		 (delta-y (* (/ arrow-head-width 2) cos)))
	    (draw-triangle x2 y2 (floor (+ arrow-start-x delta-x))
			   (floor (- arrow-start-y delta-y))
			   (floor (- arrow-start-x delta-x))
			   (floor (+ arrow-start-y delta-y))
			   :STREAM stream :ALU alu)))))
  (setq transform (merge-transform-arguments
		    (* scale scale-x)
		    (* scale scale-y)
		    rotation translation (or transform stream-transform)))
  (multiple-value-setq (x1 y1) (transform-coordinates transform x1 y1))
  (multiple-value-setq (x2 y2) (transform-coordinates transform x2 y2))
  (make-fixnum x1 y1 x2 y2)
  (setq min-x (min (the fixnum x1) (the fixnum x2))
	max-x (max (the fixnum x1) (the fixnum x2))
	min-y (min (the fixnum y1) (the fixnum y2))
	max-y (max (the fixnum y1) (the fixnum y2)))
  (when *Record-Presentations-P*
    (record-presentation-top-level stream
				   (make-arrow-presentation
				     :ALU alu
				     :x1 x1 :y1 y1 :x2 x2 :y2 y2
				     :THICKNESS thickness
				     :DRAW-SHAFT draw-shaft :HEAD-LENGTH arrow-head-length
				     :HEAD-WIDTH arrow-head-width
				     :LEFT min-x
				     :TOP min-y
				     :RIGHT max-x
				     :BOTTOM max-y))))

(defun draw-circle (center-x center-y radius
		    &KEY (inner-radius 0)
		    (start-angle 0) (end-angle *360-degrees*) clockwise
		    join-to-path
		    (alu :DRAW) pattern
		    ;; STIPPLE TILE - not used in circle drawing.
		    ;; COLOR (GRAY-LEVEL 1) - not used in circle drawing.
		    (opaque T)
		    ;; mask (mask-x 0) (mask-y 0)
		    (filled t) (thickness 0) (scale-thickness T)
		    (line-end-shape :BUTT) (line-joint-shape :MITER)
		    dashed (dash-pattern '(10 10)) (initial-dash-phase 0)
		    (draw-partial-dashes T) scale-dashes
		    (stream *Standard-Output*)
		    return-presentation
		    (rotation 0) (scale 1) (scale-x 1) (scale-y 1) translation transform
		    &ALLOW-OTHER-KEYS
		    &AUX (gcontext NIL)
		    (stream-transform (window-transform stream)))
  (warn-unimplemented-args join-to-path NIL return-presentation NIL)
  (unless (or (numberp alu) (symbolp alu)) (setq gcontext alu))
  (setq transform (merge-transform-arguments
		    (* scale scale-x)
		    (* scale scale-y)
		    rotation translation (or transform stream-transform)))
  (multiple-value-setq (center-x center-y)
    (transform-coordinates transform center-x center-y))
  (when (and scale-thickness (not (= scale 1))) (setq thickness (* scale thickness)))
  (setq radius (scale-circle-radius radius transform))
  (setq dashed (if (and dashed (not (eq dashed :SOLID))) :DASH :SOLID))
  (when (and (eq dashed :DASH) scale-dashes (not (= scale 1)))
    (setq dash-pattern (mapcar #'(lambda (x) (* x scale)) dash-pattern)))
  (if *Erase-p* (setq alu :ERASE))
  (when clockwise
    (psetq start-angle end-angle
	   end-angle (+ start-angle (* 2 pi))))
  (make-fixnum center-x center-y radius)
  (when *Record-Presentations-P*
    (record-presentation-top-level
      stream
      (if (and ;; (null STIPPLE) (null TILE) not used
	       ;; (null COLOR) (%= 1 GRAY-LEVEL)
	       (eq T OPAQUE)
	       ;; (null MASK) (zerop MASK-X) (zerop MASK-Y)
	       (eq LINE-END-SHAPE :BUTT) (eq LINE-JOINT-SHAPE :MITER)
	       (eq DASHED :SOLID))
	  (make-circle-presentation
	    :ALU alu
	    :CENTER-X center-x :CENTER-Y center-y :RADIUS radius
	    :LEFT (%- center-x radius)
	    :TOP (%- center-y radius)
	    :RIGHT (%+ center-x radius)
	    :BOTTOM (%+ center-y radius)
	    :INNER-RADIUS inner-radius :START-ANGLE start-angle :END-ANGLE end-angle
	    :PATTERN pattern :FILLED filled :THICKNESS thickness)
	  (make-complex-circle-presentation
	    :ALU alu
	    :CENTER-X center-x :CENTER-Y center-y :RADIUS radius
	    :LEFT (%- center-x radius)
	    :TOP (%- center-y radius)
	    :RIGHT (%+ center-x radius)
	    :BOTTOM (%+ center-y radius)
	    :INNER-RADIUS inner-radius :START-ANGLE start-angle :END-ANGLE end-angle
	    :PATTERN pattern :FILLED filled :THICKNESS thickness
	    ;; :STIPPLE stipple :TILE tile
	    ;;:COLOR color :GRAY-LEVEL gray-level
	    :OPAQUE opaque
	    ;; :MASK mask :MASK-X mask-x :MASK-Y mask-y
	    :LINE-END-SHAPE line-end-shape :LINE-JOINT-SHAPE line-joint-shape
	    :DASHED dashed :DASH-PATTERN dash-pattern
	    :INITIAL-DASH-PHASE initial-dash-phase
	    :DRAW-PARTIAL-DASHES draw-partial-dashes))))
  (unless (fake-window-p stream)
    (convert-window-coords-to-screen-coords-macro stream center-x center-y)
    (when (inside-window-p stream (%- center-x radius) (%- center-y radius)
			   (%+ center-x radius) (%+ center-y radius))
      #+X
      (setq end-angle
	    (if (> end-angle start-angle) (- end-angle start-angle)
		(+ #.(* 2 pi) (- end-angle start-angle))))
      (setq center-x (%- center-x (the (values fixnum number) (floor (* scale-x radius))))
	    center-y (%- center-y (the (values fixnum number) (floor (* scale-y radius)))))
      #+CLX
      (prepare-window (stream)
	(if gcontext
	    (xlib:draw-arc (window-real-window stream) gcontext
			   center-x center-y
			   (%* 2 (the (values fixnum number) (floor (* scale-x radius))))
			   (%* 2 (the (values fixnum number) (floor (* scale-y radius))))
			   start-angle end-angle filled)
	    (let ((gcontext (window-gcontext stream)))
	      (xlib:with-gcontext (gcontext :FUNCTION (draw-alu alu)
					    :LINE-WIDTH thickness
					    :CAP-STYLE line-end-shape
					    :JOIN-STYLE line-joint-shape
					    :LINE-STYLE dashed
					    :DASHES dash-pattern
					    :DASH-OFFSET initial-dash-phase)
		(if pattern
		    (ecase (lisp:type-of pattern)
		      (stipple-pattern
			(xlib:with-gcontext (gcontext :fill-style :stippled
						      :stipple (get-pattern-pixmap pattern))
			  (xlib:draw-arc (window-real-window stream) gcontext
					 center-x center-y
					 (%* 2 (the (values fixnum number)
						    (floor (* scale-x radius))))
					 (%* 2 (the (values fixnum number)
						    (floor (* scale-y radius))))
					 start-angle end-angle filled))))
		    (xlib:draw-arc (window-real-window stream) gcontext
				   center-x center-y
				   (%* 2 (the (values fixnum number)
					      (floor (* scale-x radius))))
				   (%* 2 (the (values fixnum number)
					      (floor (* scale-y radius))))
				   start-angle end-angle filled)))))))))


(defun draw-ellipse (center-x center-y x-radius y-radius
		    &KEY (inner-x-radius 0)
		    (inner-y-radius (/ (* inner-x-radius y-radius) x-radius))
		    (start-angle 0) (end-angle *360-degrees*)
		    clockwise join-to-path
		    (alu :DRAW) pattern
		    ;; STIPPLE TILE - not used in circle drawing.
		    ;; COLOR (GRAY-LEVEL 1) - not used in circle drawing.
		    (opaque T)
		    ;; mask (mask-x 0) (mask-y 0)
		    (filled t) (thickness 0) (scale-thickness T)
		    (line-end-shape :BUTT) (line-joint-shape :MITER)
		    dashed (dash-pattern '(10 10)) (initial-dash-phase 0)
		    (draw-partial-dashes T) scale-dashes
		    (stream *Standard-Output*)
		    return-presentation
		    (rotation 0) (scale 1) (scale-x 1) (scale-y 1) translation transform
		    &ALLOW-OTHER-KEYS
		    &AUX (stream-transform (window-transform stream))
		    (gcontext NIL))
  (warn-unimplemented-args join-to-path NIL return-presentation NIL)
  (unless (or (numberp alu) (symbolp alu)) (setq gcontext alu))
  (setq transform (merge-transform-arguments
		    (* scale scale-x)
		    (* scale scale-y)
		    rotation translation (or transform stream-transform)))
  (multiple-value-setq (center-x center-y)
    (transform-coordinates transform center-x center-y))
  (when (and scale-thickness (not (= scale 1))) (setq thickness (* scale thickness)))
  (setq x-radius (scale-circle-radius x-radius transform))
  (setq y-radius (scale-circle-radius y-radius transform))
  (setq dashed (if (and dashed (not (eq dashed :SOLID))) :DASH :SOLID))
  (when (and (eq dashed :DASH) scale-dashes (not (= scale 1)))
    (setq dash-pattern (mapcar #'(lambda (x) (* x scale)) dash-pattern)))
  (if *Erase-p* (setq alu :ERASE))
  (when clockwise
    (psetq start-angle end-angle end-angle start-angle))
  (when *Record-Presentations-P*
    (record-presentation-top-level
      stream
      (if (and ;; (null STIPPLE) (null TILE) not used
	       ;; (null COLOR) (= 1 GRAY-LEVEL)
	       (eq T OPAQUE)
	       ;; (null MASK) (zerop MASK-X) (zerop MASK-Y)
	       (eq LINE-END-SHAPE :BUTT) (eq LINE-JOINT-SHAPE :MITER)
	       (eq DASHED :SOLID))
	  (make-ellipse-presentation
	    :ALU alu
	    :CENTER-X center-x :CENTER-Y center-y
	    :X-RADIUS x-radius
	    :Y-RADIUS y-radius
	    :LEFT (- center-x x-radius)
	    :TOP (- center-y y-radius)
	    :RIGHT (+ center-x x-radius)
	    :BOTTOM (+ center-y y-radius)
	    :INNER-X-RADIUS inner-x-radius
	    :INNER-Y-RADIUS inner-y-radius
	    :START-ANGLE start-angle :END-ANGLE end-angle
	    :PATTERN pattern :FILLED filled :THICKNESS thickness)
	  (make-complex-ellipse-presentation
	    :ALU alu
	    :CENTER-X center-x :CENTER-Y center-y
	    :X-RADIUS x-radius
	    :Y-RADIUS y-radius
	    :LEFT (- center-x x-radius)
	    :TOP (- center-y y-radius)
	    :RIGHT (+ center-x x-radius)
	    :BOTTOM (+ center-y y-radius)
	    :INNER-X-RADIUS inner-x-radius
	    :INNER-Y-RADIUS inner-y-radius
	    :START-ANGLE start-angle :END-ANGLE end-angle
	    :PATTERN pattern :FILLED filled :THICKNESS thickness
	    ;; :STIPPLE stipple :TILE tile
	    ;;:COLOR color :GRAY-LEVEL gray-level
	    :OPAQUE opaque
	    ;; :MASK mask :MASK-X mask-x :MASK-Y mask-y
	    :LINE-END-SHAPE line-end-shape :LINE-JOINT-SHAPE line-joint-shape
	    :DASHED dashed :DASH-PATTERN dash-pattern
	    :INITIAL-DASH-PHASE initial-dash-phase
	    :DRAW-PARTIAL-DASHES draw-partial-dashes))))
  (unless (fake-window-p stream)
    (convert-window-coords-to-screen-coords-macro stream center-x center-y)
    (when (inside-window-p stream (- center-x x-radius) (- center-y y-radius)
			   (+ center-x x-radius) (+ center-y y-radius))
      #+X
      (setq end-angle
	    (if (> end-angle start-angle) (- end-angle start-angle)
		(+ #.(* 2 pi) (- end-angle start-angle))))
      (setq center-x (the (values fixnum number) (floor (- center-x (* scale-x x-radius))))
	    center-y (the (values fixnum number) (floor (- center-y (* scale-y y-radius)))))
      #+CLX
      (prepare-window (stream)
	(if gcontext
	    (xlib:draw-arc (window-real-window stream) gcontext
			   center-x center-y
			   (* scale-x 2 x-radius) (* scale-y 2 y-radius)
			   start-angle end-angle)
	    (let ((gcontext (window-gcontext stream)))
	      (xlib:with-gcontext (gcontext :FUNCTION (draw-alu alu)
					    :LINE-WIDTH thickness
					    :CAP-STYLE line-end-shape
					    :JOIN-STYLE line-joint-shape
					    :LINE-STYLE dashed
					    :DASHES dash-pattern
					    :DASH-OFFSET initial-dash-phase)
		(if pattern
		    (ecase (lisp:type-of pattern)
		      (stipple-pattern
			(xlib:with-gcontext (gcontext :fill-style :stippled
						      :stipple (get-pattern-pixmap pattern))
			  (xlib:draw-arc (window-real-window stream) gcontext
					 center-x center-y
					 (* scale-x 2 x-radius) (* scale-y 2 y-radius)
					 start-angle end-angle))))
		    (xlib:draw-arc (window-real-window stream) gcontext
				   center-x center-y
				   (* scale-x 2 x-radius) (* scale-y 2 y-radius)
				   start-angle end-angle)))))))))

;; For Symbolics
;; Draw-string specifies the x,y location of the left bottom of the first character.
;; By bottom we mean below the normal part of the character but above the descenders.

;; For X  The draw-glyphs function seems to be the same.


(defun draw-string (string x y &KEY (attachment-y :BASELINE) (attachment-x :LEFT)
		    (toward-x x) (toward-y y)
		    stretch-p character-style
		    record-as-text
		    (alu :DRAW) pattern stipple tile
		    color (gray-level 1) (opaque T)
		    ;; mask (mask-x 0) (mask-y 0)
		    ;; (filled t) (thickness 0) (scale-thickness T) - NOT used in draw-string
		    ;; (line-end-shape :BUTT)(line-joint-shape :MITER) not used in draw-string
		    ;; dashed (dash-pattern) (initial-dash-phase 0) - NOT used in draw-string
		    ;; (draw-partial-dashes t) scale-dashes - NOT used in draw-string
		    (stream *Standard-Output*) return-presentation
		    (rotation 0) (scale 1) (scale-x 1) (scale-y 1) translation transform
		    &ALLOW-OTHER-KEYS
		    &AUX (stream-transform (window-transform stream))
		    (gcontext NIL))
  (declare (string string) (number x y))
  (warn-unimplemented-args return-presentation NIL record-as-text NIL stretch-p NIL
			   toward-x x toward-y y attachment-y :BASELINE)
  (unless (or (numberp alu) (symbolp alu)) (setq gcontext alu))
  (setq transform (merge-transform-arguments
		    (* scale scale-x)
		    (* scale scale-y)
		    rotation translation (or transform stream-transform)))
  (multiple-value-setq (x y) (transform-coordinates transform x y))
  (make-fixnum x y)
  (unless character-style (setq character-style (window-character-style stream)))
  (let* ((font (get-font-from-style character-style))
	 (width (graphics-textlength font string 0 0)))
    (declare (fixnum width))
    (when (not (eq attachment-x :LEFT))
      ;; need to refigure coordinates for drawing text.
      (case attachment-x
	(:CENTER (setq x (%- x (the (values fixnum number) (floor width 2)))))
	(:RIGHT (setq x (%- x width)))))
    (if *Erase-p* (setq alu :ERASE))
    (when *Record-Presentations-P*
      (record-presentation-top-level
	stream
	(if (and (null pattern) (null STIPPLE) (null TILE)
		 (null COLOR) (%= 1 GRAY-LEVEL) (eq T OPAQUE)
		 ;; (null MASK) (zerop MASK-X) (zerop MASK-Y)
		 )
	    (make-string-presentation
	      :ALU alu
	      :STRING string :FONT font
	      :LEFT x :TOP (%- y (font-ascent font))
	      :RIGHT (%+ x width)
	      :BOTTOM (%+ y (font-descent font)))
	    (make-complex-string-presentation
	      :ALU alu
	      :STRING string :FONT font
	      :LEFT x :TOP (%- y (font-ascent font))
	      :RIGHT (%+ x width)
	      :BOTTOM (%+ y (font-descent font))
	      :PATTERN pattern :STIPPLE stipple :TILE tile
	      :COLOR color :GRAY-LEVEL gray-level :OPAQUE opaque
	      ;; :MASK mask :MASK-X mask-x :MASK-Y mask-y
	      ))))
    (unless (fake-window-p stream)
      (when (inside-window-p stream x y (%+ x width) (%+ y (font-descent font)))
	#+X
	(convert-window-coords-to-screen-coords-macro stream x y)
	(prepare-window (stream)
	  (if gcontext
	      (;;#+lucid xlib::fast-draw-glyphs8
	       ;;#-lucid
	       xlib::draw-glyphs8
	       (window-real-window stream)
	       gcontext
	       x y string
	       0 (length string)
	       ;;#-lucid
	       #'xlib::translate-default
	       ;;#-lucid
	       NIL)
	      (xlib:with-gcontext ((window-gcontext stream) :FONT font :FUNCTION (draw-alu alu))
		#+ignore
		(xlib:draw-glyphs (window-real-window stream) (window-gcontext stream)
				  x y string)
		(;;#+lucid xlib::fast-draw-glyphs8
		 ;;#-lucid
		 xlib::draw-glyphs8
		 (window-real-window stream)
		 (window-gcontext stream)
		 x y string
		 0 (length string)
		 ;;#-lucid
		 #'xlib::translate-default
		 ;;#-lucid
		 NIL))))))))


(defun draw-line (start-x start-y end-x end-y
		  &KEY
		  (alu :DRAW) pattern stipple tile
		  color (gray-level 1) (opaque T)
		  ;; mask (mask-x 0) (mask-y 0)
		  ;; (filled t) not used in line drawing.
		  (thickness 0) (scale-thickness T)
		  (line-end-shape :BUTT) (line-joint-shape :MITER)
		  dashed (dash-pattern '(10 10)) (initial-dash-phase 0)
		  (draw-partial-dashes T) scale-dashes
		  (stream *Standard-Output*) return-presentation
		  (rotation 0) (scale 1) (scale-x 1) (scale-y 1) translation transform
		  &ALLOW-OTHER-KEYS
		  &AUX (gcontext NIL) (min-x 0) (max-x 0) (min-y 0) (max-y 0)
		  (stream-transform (window-transform stream)))
  (declare (fixnum min-x min-y max-x max-y))
  (warn-unimplemented-args return-presentation NIL color NIL
			   ;; mask NIL mask-x 0 mask-y 0
			   )
  (unless (or (numberp alu) (symbolp alu)) (setq gcontext alu))
  (if (eq line-joint-shape :NONE) (setq line-joint-shape NIL))
  (setq line-end-shape (cdr (assoc line-end-shape '((:BUTT . :BUTT)
						    (:NO-END-POINT . :NOT-LAST)
						    (:round . :ROUND)
						    (:square . :projecting)))))
  (setq transform (merge-transform-arguments
		    (* scale scale-x)
		    (* scale scale-y)
		    rotation translation (or transform stream-transform)))
  (multiple-value-setq (start-x start-y) (transform-coordinates transform start-x start-y))
  (multiple-value-setq (end-x end-y) (transform-coordinates transform end-x end-y))
  (when (and scale-thickness (not (= scale 1))) (setq thickness (* scale thickness)))
  (setq dashed (if (and dashed (not (eq dashed :SOLID))) :DASH :SOLID))
  (when (and dashed scale-dashes (not (= scale 1)))
    (setq dash-pattern (mapcar #'(lambda (x) (* x scale)) dash-pattern)))
  (if *Erase-p* (setq alu :ERASE))
  (make-fixnum start-x start-y end-x end-y)
  (setq min-x (min (the fixnum start-x) (the fixnum end-x))
	max-x (max (the fixnum start-x) (the fixnum end-x))
	min-y (min (the fixnum start-y) (the fixnum end-y))
	max-y (max (the fixnum start-y) (the fixnum end-y)))
  (when *Record-Presentations-P*
    (record-presentation-top-level
      stream
      ;;; now figure out whether we are creating a complex one or not.
      (if (and (null STIPPLE) (null TILE)
	       (null COLOR) (= 1 GRAY-LEVEL) (eq T OPAQUE)
	       ;; (null MASK) (zerop MASK-X) (zerop MASK-Y)
	       (eq LINE-END-SHAPE :BUTT) (eq LINE-JOINT-SHAPE :MITER)
	       (eq DASHED :SOLID))
	  (make-line-presentation
	    :ALU alu
	    :START-X start-x :START-Y start-y
	    :END-X end-x :END-Y end-y
	    :LEFT min-x :TOP min-y
	    :RIGHT max-x :BOTTOM max-y
	    :PATTERN pattern			;; :FILLED filled not in lines.
	    :THICKNESS thickness)
	  (make-complex-line-presentation
	    :ALU alu
	    :START-X start-x :START-Y start-y
	    :END-X end-x :END-Y end-y
	    :LEFT min-x :TOP min-y
	    :RIGHT max-x :BOTTOM max-y
	    :PATTERN pattern			;; :FILLED filled not in lines.
	    :THICKNESS thickness
	    :STIPPLE stipple :TILE tile
	    :COLOR color :GRAY-LEVEL gray-level :OPAQUE opaque
	    ;; :MASK mask :MASK-X mask-x :MASK-Y mask-y
	    :LINE-END-SHAPE line-end-shape :LINE-JOINT-SHAPE line-joint-shape
	    :DASHED dashed :DASH-PATTERN dash-pattern
	    :INITIAL-DASH-PHASE initial-dash-phase
	    :DRAW-PARTIAL-DASHES draw-partial-dashes))))
  (unless (fake-window-p stream)
    (convert-window-coords-to-screen-coords-macro stream start-x start-y)
    (convert-window-coords-to-screen-coords-macro stream end-x end-y)
    (convert-window-coords-to-screen-coords-macro stream min-x min-y)
    (convert-window-coords-to-screen-coords-macro stream max-x max-y)
    (when (inside-window-p stream min-x min-y max-x max-y)
      #+CLX
      (prepare-window (stream)
	(if gcontext
	    (xlib:draw-line (window-real-window stream) gcontext start-x start-y
			    end-x end-y)
	    (xlib:with-gcontext ((window-gcontext stream) :LINE-WIDTH thickness
				 :CAP-STYLE line-end-shape
				 :JOIN-STYLE line-joint-shape
				 :FUNCTION (draw-alu alu)
				 :LINE-STYLE dashed
				 :DASHES dash-pattern
				 :DASH-OFFSET initial-dash-phase)
	      (if pattern
		  (typecase pattern
		    (stipple-pattern
		      (xlib:with-gcontext ((window-gcontext stream) :fill-style :stippled
					   :stipple (get-pattern-pixmap pattern))
			(xlib:draw-line (window-real-window stream) (window-gcontext stream)
					start-x start-y
					end-x end-y))))
		  (xlib:draw-line (window-real-window stream) (window-gcontext stream)
				  start-x start-y
				  end-x end-y))))))))


(defun draw-icon (icon-pattern left top &KEY (stream *Standard-Output*)
		  (alu :DRAW)
		  color (gray-level 1) (opaque T)
		  (rotation 0) (scale 1) (scale-x 1) (scale-y 1) translation transform
		  &ALLOW-OTHER-KEYS
		  &AUX (gcontext NIL)
		  (stream-transform (window-transform stream)))
  (unless (or (numberp alu) (symbolp alu)) (setq gcontext alu))
  (setq transform (merge-transform-arguments
		    (* scale scale-x) (* scale scale-y)
		    rotation translation (or transform stream-transform)))
  (multiple-value-setq (left top) (transform-coordinates transform left top))
  (if *Erase-p* (setq alu :ERASE))
  (make-fixnum left top)
  (let* ((width (stipple-pattern-width icon-pattern))
	 (height (stipple-pattern-height icon-pattern))
	 (right (%+ width left))
	 (bottom (%+ height top)))
    (when *Record-Presentations-P*
      (record-presentation-top-level
	stream
      ;;; now figure out whether we are creating a complex one or not.
	(if (and (null COLOR) (= 1 GRAY-LEVEL) (eq T OPAQUE))
	    (make-icon-presentation
	      :ALU alu
	      :LEFT left :TOP top
	      :RIGHT right :BOTTOM bottom
	      :ICON-PATTERN icon-pattern)
	    (make-complex-icon-presentation
	      :ALU alu
	      :LEFT left :TOP top
	      :RIGHT right :BOTTOM bottom
	      :ICON-PATTERN icon-pattern
	      :COLOR color :GRAY-LEVEL gray-level :OPAQUE opaque))))
    (unless (fake-window-p stream)
      (convert-window-coords-to-screen-coords-macro stream left top)
      (convert-window-coords-to-screen-coords-macro stream right bottom)
      (when (inside-window-p stream left top right bottom)
	#+CLX
	(prepare-window (stream)
	  (if gcontext
	      (xlib:put-image (window-real-window stream) gcontext
			      (stipple-pattern-black-image icon-pattern)
			      :X left :Y top
			      :WIDTH width :HEIGHT height :BITMAP-P T)
	      (xlib:with-gcontext ((window-gcontext stream) :FUNCTION (draw-alu alu))
		(xlib:put-image (window-real-window stream) (window-gcontext stream)
				(stipple-pattern-black-image icon-pattern)
				:X left :Y top
				:WIDTH width :HEIGHT height :BITMAP-P T))))))))




(defun presentation-print-function (object print-function
				    &OPTIONAL (stream *Standard-Output*)
				    (start 0) end)
  #.(fast)
  (multiple-value-bind (x y)
      (read-cursorpos stream)
    (declare (fixnum x y))
    (let* ((string (cond ((stringp object)
			  (if (eq print-function 'prin1)
			      (concatenate 'string "\"" object "\"")
			      object))
			 (T (with-output-to-string (stream)
			      (if (eq print-function 'prin1)
				  (prin1 object stream)
				  (princ object stream)))))))
      (multiple-value-bind (width height)
	  (window-size stream)
	(presentation-print-function-internal string stream x y start end T
					      (window-font stream)
					      width height))))
  object)

(defun presentation-print-function-internal (string stream x y start end
					     set-cursorpos-p font window-width window-height
					     &AUX final-x-pos final-y-pos)
  #.(fast)
  (declare (string string) (fixnum x y start window-width window-height))
  (let ((length (or end (length string)))
	(ascent (font-ascent font))
	(font-height (font-height font))
	(line-height (window-line-height stream))
	(x-before-tab NIL)
	(index-before-tab NIL)
	(gcontext (window-gcontext stream))
	(processing-tab-p NIL)) ;; set to T to delay creating presentation during tab
    (declare (fixnum ascent font-height length line-height))
    (prepare-window (stream)
      (do ()
	  (())
	(multiple-value-bind (final-x end-index tabs-found-p cr-found-p)
	    (compute-text-size-to-print stream string start x length font)
	  (declare (fixnum end-index final-x))
	  ;; make a separate presentation for each substring.
	  ;; eventually will have to get more clever for list structures.
	  (when (and set-cursorpos-p cr-found-p)
	    (set-cursorpos-with-checks x y stream)
	    (check-end-of-page-mode stream))
	  (when (and tabs-found-p (not processing-tab-p))
	    (setq x-before-tab x
		  index-before-tab start
		  processing-tab-p T))
	  (when (not tabs-found-p)
	    (when *Record-Presentations-P*
	      (unless (or (equal string "") (equal string #.(string #\CR)))
		(record-presentation-top-level
		  stream
		  (make-text-presentation
		    :ALU *Draw-Alu*
		    ;; :PRINTED-OBJECT object
		    :STRING (let ((start (if processing-tab-p
					     index-before-tab
					     start)))
			      (declare (fixnum start))
			      (cond (cr-found-p
				     (subseq string start (%1- end-index)))
				    ((and (zerop start) (%= end-index length))
				     string)
				    (T (subseq string start end-index))))
		    :FONT font
		    :LEFT (if processing-tab-p x-before-tab x)
		    :TOP y
		    :RIGHT final-x :BOTTOM (%+ y font-height))))))
	  (when (> (%- end-index start) (the fixnum (if cr-found-p 1 0)))
	    (let ((screen-x (%+ (%- x (window-scroll-x-offset stream))
				(window-inside-left stream)))
		  (screen-y (%+ (%- y (window-scroll-y-offset stream))
				(window-inside-top stream))))
	      ;; open coding of (convert-window-coords-to-screen-coords stream x y)
	      (declare (fixnum screen-x screen-y))
	      (unless (or (%> screen-x window-width)
			  (%< (%+ screen-x (%- final-x x)) 0)
			  (%> screen-y window-height))
		(unless (fake-window-p stream)
		  #+CLX
		  (if *Erase-p*
		      (xlib:with-gcontext (gcontext :FUNCTION (draw-alu *Erase-Alu*))
			 (xlib:draw-rectangle (window-real-window stream) gcontext
			  screen-x screen-y
			  (%- (convert-window-x-coord-to-screen-coord stream final-x)
			      screen-x)
			  font-height T))
		    (if (eq font (xlib:gcontext-font gcontext))
			(;;#+lucid xlib::fast-draw-glyphs8
				 ;;#-lucid
				 xlib::draw-glyphs8 (window-real-window stream)
				 gcontext
				 screen-x (%+ screen-y ascent)
				 string
				 start
				 (or (if cr-found-p
					 (%1- end-index) end-index)
				     end)
				 ;;#-lucid
				 #'xlib::translate-default
				 ;;#-lucid
				 NIL)
		      (xlib:with-gcontext (gcontext :FONT font)
			  (;;#+lucid xlib::fast-draw-glyphs8
				   ;;#-lucid
				   xlib::draw-glyphs8 (window-real-window stream)
				   gcontext
				   screen-x (%+ screen-y ascent)
				   string
				   start
				   (or (if cr-found-p
					   (%1- end-index) end-index)
				       end)
				   ;;#-lucid
				   #'xlib::translate-default
				   ;;#-lucid
				   NIL))))))))
	  (when (%= end-index length)
	    ;; done printing, "taking care of business". you're supposed to sing here.
	    (when cr-found-p
	      (setq final-x 0 x 0)
	      (incf y line-height))
	    (when set-cursorpos-p (set-cursorpos stream final-x y))
	    (setq final-x-pos final-x final-y-pos y)
	    (return NIL))
	  (when (not tabs-found-p) (setq processing-tab-p NIL))
	  (if (not tabs-found-p)
	      (progn
		(setq start end-index)
		;; if we have not returned yet, there must be more to print still
		;; and if we did not find a tab, then we must have found a CR
		;; or run into the end of the line.
		(unless cr-found-p
		  (setq x 0)
		  (incf y line-height)))
	      (progn
		;; process change in X due to tabs.
		;; tabs are a pain in the neck.
		;; calculate their width aways assuming they go up to the next column
		;; of 8 spaces width.
		(setq x (get-next-tab-position final-x font))
		(unless (eql #\tab (aref string end-index))
		  (error "Should have found a tab here."))
		;; anyway found how many other tabs there are here.
		#+ignore
		(let ((next-non-tab-position (position #\TAB string :TEST-NOT #'EQL
						       :START (%1+ end-index))))
		  (if next-non-tab-position
		      (progn (incf x (%* 8 (char-width-from-font #\Space font)
					 (%- next-non-tab-position (%1+ end-index))))
			     (setq start next-non-tab-position))
		      (setq start length)))
		;; hopefully faster version of above.
		(do ((i (%1+ end-index) (%1+ i)))
		    ((%= i length) (setq start length))
		  (declare (fixnum i))
		  (unless (eql #\TAB (aref string i))
		    (incf x (%* 8 (char-width-from-font #\Space font)
				(%- i (%1+ end-index))))
		    (setq start i)
		    (return NIL)))
		))
	  (when cr-found-p
	    (setq x 0)
	    (setq y (%+ y line-height))
	    (when set-cursorpos-p (set-cursorpos stream x y)))))))
  (values final-x-pos final-y-pos))

(defun presentation-print-object (object print-function &OPTIONAL (stream *Standard-Output*))
  #.(fast)
  (when (presentation-recording-string-p object)
    (setq object (presentation-recording-string-string object)))
  (if *Disable-Expression-Presentation-P*
      (let ((*Disable-Expression-Presentation-P* NIL))
	(if (not (consp object))
	    (presentation-print-function object print-function stream)
	    (progn
	      (presentation-print-function "(" 'PRINC stream)
	      (presentation-print-object (first object) print-function stream)
	      (do ((sub-list (cdr object) (cdr sub-list)))
		  ((null sub-list))
		(if (consp sub-list)
		    (progn
		      (presentation-print-function " " 'PRINC stream)
		      (presentation-print-object (first sub-list) print-function stream))
		    (progn
		      (presentation-print-function " . " 'PRINC stream)
		      (return (presentation-print-object sub-list print-function stream)))))
	      (presentation-print-function ")" 'PRINC stream))))
      (display-as (:OBJECT object :TYPE 'EXPRESSION :STREAM stream)
	(if (not (consp object))
	    (presentation-print-function object print-function stream)
	    (progn
	      (presentation-print-function "(" 'PRINC stream)
	      (presentation-print-object (first object) print-function stream)
	      (do ((sub-list (cdr object) (cdr sub-list)))
		  ((null sub-list))
		(if (consp sub-list)
		    (progn
		      (presentation-print-function " " 'PRINC stream)
		      (presentation-print-object (first sub-list) print-function stream))
		    (progn
		      (presentation-print-function " . " 'PRINC stream)
		      (return (presentation-print-object sub-list print-function stream)))))
	      (presentation-print-function ")" 'PRINC stream))))))



(defun presentation-print-string (object &OPTIONAL (stream *Standard-Output*) (start 0) end)
  #.(fast)
  (declare (fixnum start))
  (when (presentation-recording-string-p object)
    (setq object (presentation-recording-string-string object)))
  (when (or (not (zerop start))
	    end)
    (setq object (subseq object start end)))
  (if *Disable-Expression-Presentation-P*
      (let ((*Disable-Expression-Presentation-P* NIL))
	(presentation-print-function object 'princ stream))
      (display-as (:OBJECT object :TYPE 'EXPRESSION :STREAM stream)
	(presentation-print-function object 'princ stream))))







;; this returns three values FINAL-X FINAL-Y and LIST-OF-STRINGS


(defun window-compute-motion (window string &OPTIONAL (start 0) end x y cr-at-end-p (stop-x 0)
			      stop-y)
  (declare (string string) (fixnum start stop-x))
  (let ((length (length string))
	(number-tabs 0)
	(line-height (window-line-height window))
	(font (window-font window)))
    (declare (fixnum number-tabs length line-height))
    (unless end (setq end length))
    (unless stop-y (setq stop-y (window-inside-bottom window)))
    (when (or (not y) (not x))
      (multiple-value-bind (cx cy)
	  (read-cursorpos window)
	(when (not y) (setq y cy))
	(when (not x) (setq x cx))))
    ;; first find any leading TABS
    (dotimes (i length)
      (if (eql #\TAB (aref string (%+ start i)))
	  (incf number-tabs)
	  (return NIL)))
    ;; need to increase x-pos by appropriate amount.
    ;; typically make tabs go over 8 spaces apiece except for first which goes to next
    ;; column
    (unless (zerop number-tabs)
      (let* ((width (char-width-from-font #\Space font))
	     (number-of-chars-so-far (the (values fixnum number) (floor (the fixnum x) width)))
	     (x-char-pos (%* 8 (%1+ (the (values fixnum number)
					 (floor number-of-chars-so-far 8))))))
	(declare (fixnum width number-of-chars-so-far x-char-pos))
	(setq x (%* x-char-pos width)))
      (incf start number-tabs))
    ;; the main body
    (do ((i start (%1+ i)))
	((%= end i)
	 ;; ending condition
	 (if (not cr-at-end-p)
	     (values x y NIL)
	     (progn (setq x 0)
		    (setq y (%+ y line-height))
		    (values x y (%>= y stop-y)))))
      (declare (fixnum i))
      (let ((char (aref string i)))
	(case char
	  (#\Newline
	   (setq x 0
		 y (%+ y line-height))
	   (when (and (%>= x stop-x)
		      (%>= y stop-y))
	     (return (values x y i))))
	  ;; (#\TAB)
	  (T (let ((char-width (xlib:char-width
				 font
				 (if (numberp char) char (char-code char)))))
	       (setq x (%+ x char-width))
	       (when (and (%>= x stop-x)
			  (%>= y stop-y))
		 (return (values x y i))))))))))

;; Iterates down characters in STRING starting at the character numbered INDEX.
;; Starts with a displayed X-POSITION and increments that by the width of each
;; character until either a #\Newline is encountered or X-POSITION reach MAX-X.
;; It returns two values.  The current X-POSITION and the current INDEX.  The
;; purpose is to tell the caller how much of the string could be displayed on the
;; current line of the display, and exactly how far it got displaying.  Note, just
;; because you ran out of room to display, does not mean you should return the
;; right side of the window.  You should return the right side of the last
;; character that would fit.


 ;; X version
#+ignore
(defun textlength (font string index x-position max-x &AUX (length (length string)))
  (declare (values final-x-position final-index))
  (do ((x index (1+ x)))
      ((= length x) (values x-position length))
    (declare (fixnum x))
    (let ((char (aref string x)))
      (case char
	(#\Newline
	  (return (values x-position x)))
	;(#\TAB)
	(T (let ((char-width (char-width-from-font char font)))
	     (incf x-position char-width)
	     (when (> x-position max-x)
	       (return (values (- x-position char-width) x)))))))))


#+lucid
(defmacro optimize-string-access ((string start-index length) &BODY body)
  (declare (ignore length start-index))
  (let ((offset (gensym "OFFSET")))
    `(multiple-value-bind (,string ,offset)
	 (sys:underlying-simple-vector ,string)
       (declare (simple-string ,string) (fixnum ,offset))
       (incf start-index ,offset)
       (incf length ,offset)
       . ,body)))

#-lucid
(defmacro optimize-string-access ((string start-index length) &BODY body)
  (declare (ignore string start-index length))
  `(progn . ,body))


;; the purpose of this function is to tell the caller how much of the text can be printed
;; on the current line taking into account the width of the window, the font, and
;; any truncation going on. and any filling.
;; This will return the index of the char after the last one used.
;; It is up to the caller to make sure it strips off CRs if it is building presentation
;; objects.
;; for efficiencies of other programs it returns a third value which tells if any tabs have
;; been found in the string.

;; to do filling we shall compute text and if we return because we run into the end
;; we shall first back up until we hit a fill-character and return that as the value.

(defun compute-text-size-to-print (window string start-index x length font)
  #.(fast)
  (declare (values final-x-position end-index tabs-found-p cr-found-p)
	   (fixnum start-index x length)
	   (string string))
  (optimize-string-access (string start-index length)
    (let ((min-char-width (xlib:min-char-width font))
	  (dont-truncate (not (eq :truncate (window-end-of-line-mode window))))
	  (fill (window-fill window))
	  (max-x (window-inside-width window)))
      (declare (fixnum min-char-width max-x))
      (if (= min-char-width (the fixnum (xlib:max-char-width font)))
	  ;; use faster methods for computing text size.
	  (do ((i start-index (%1+ i)))
	      ((= length i) (values x length NIL))
	    (declare (fixnum i))
	    (let ((char (aref string i)))
	      (case char
		(#\Newline
		 (return (values x (%1+ i) NIL T)))
		(#\TAB
		 (return (values x i T NIL)))
		(T (incf x min-char-width)
		   (when fill
		     (multiple-value-bind (filled-p new-x new-index)
			 (handle-fill fill window x i string start-index
				      font min-char-width max-x)
		       (when filled-p
			 (return (values new-x new-index NIL)))))
		   (when (and (> x max-x) dont-truncate)
		     (return (values (%- x min-char-width) i NIL)))))))
	  ;; slower method that needs to access every character for its width.
	  (do ((i start-index (%1+ i)))
	      ((= length i) (values x length NIL))
	    (declare (fixnum i))
	    (let ((char (aref string i)))
	      (case char
		(#\Newline
		 (return (values x (%1+ i) NIL)))
		(#\TAB
		 (return (values x i T NIL)))
		(T (let ((char-width (xlib:char-width font (char-code char))))
		     (declare (fixnum char-width))
		     (incf x char-width)
		     (when fill
		       (multiple-value-bind (filled-p new-x new-index)
			   (handle-fill fill window x i string start-index
					font min-char-width max-x)
			 (when filled-p
			   (return (values new-x new-index NIL)))))
		     (when (and dont-truncate
				(> x max-x))
		       (return (values (%- x char-width) i NIL))))))))))))



(defun handle-fill (fill window x i string start-index font min-char-width max-x)
  (declare (values filled-p new-x new-index))
  (let ((fill-column (fill-object-column fill)))
    ;; check fill
    (when ;; when the condition is true process fill
      (cond ((integerp fill-column)
	     (when (< fill-column 10)
	       (setq fill-column
		     (* fill-column (default-char-width window))))
	     (> x fill-column))
	    ((consp fill-column)
	     (setq fill-column
		   (if (eq :PIXEL (second fill-column))
		       (first fill-column)
		       (* (first fill-column) (default-char-width window))))
	     (> x fill-column))
	    ((> x max-x)))
      (dformat "~%Processing Fill")
      ;; we have reached maximum fill.  Do it bud.
      ;; search backwards until we find a fill-character
      ;; do it silly slow way for now.
      (if (%= min-char-width (the fixnum (xlib:max-char-width font)))
	  (do ((index i (1- index)))
	      ((= index start-index)
	       ;; yow we went all the way back to the beginning.
	       ;; check to see if this is the beginning of a line.
	       ;; if so we want to forget this filling stuff.
	       (if (zerop x)
		   NIL
		   (values T 0 start-index)))
	    ;; hopefully tabs are already taken care of and we don't need to worry about
	    ;; filling them.
	    (let ((char (aref string index)))
	      ;; is this a fill-character
	      (when (member char (fill-object-characters fill))
		;; we have found gold
		(return (values T x (%1+ index))))
	      (decf x min-char-width)))
	  (do ((index i (1- index)))
	      ((= index start-index)
	       ;; yow we went all the way back to the beginning.
	       ;; check to see if this is the beginning of a line.
	       ;; if so we want to forget this filling stuff.
	       (if (zerop x)
		   NIL
		   (values T 0 start-index)))
	    ;; hopefully tabs are already taken care of and we don't need to worry about
	    ;; filling them.
	    (let* ((char (aref string index))
		   (char-width (xlib:char-width font (char-code char))))
	      (declare (fixnum char-width))
	      ;; is this a fill-character
	      (when (member char (fill-object-characters fill))
		;; we have found gold
		(return (values T x (%1+ index))))
	      (decf x char-width)))))))

#+ignore
(defun sample (&AUX (stream *Standard-output*))
;  (terpri)
  (with-output-filling (stream :FILL-COLUMN 8)
    (format stream "foo bar bletch wozoo")))

#+ignore
(defun zsample (&AUX (stream *Standard-output*))
  (terpri)
  (dw:filling-output (stream :FILL-COLUMN 8)
    (format stream "foo bar bletch wozoo")))

(defun window-string-length (window string &OPTIONAL (start 0) (end NIL) (stop-x NIL)
			     character-style (start-x 0) (max-x 0)
			     &AUX (max-x-position 0) (x-position start-x))
  (declare (values x-position final-x-position final-index max-x-position)
	   (fixnum max-x-position x-position start-x)
	   (string string) (ignore max-x))
  (if (not end) (setq end (length string)))
  (let ((font (if character-style
		  (get-font-from-style character-style)
		  (window-font window))))
    (do ((x start (%1+ x)))
	((%= end x) (values x-position x (max x-position max-x-position)))
      (declare (fixnum x))
      (let ((char (aref string x)))
	(case char
	  (#\Newline
	   (maximize max-x-position start-x)
	   (setq start-x 0))
	  ;(#\TAB)
	  (T (let ((char-width (xlib:char-width font
						(if (numberp char) char (char-code char)))))
	       (declare (fixnum char-width))
	       (incf x-position char-width)
	       (if (numberp stop-x)
		   (when (> x-position (the fixnum stop-x))
		     (return (values (%- x-position char-width) x
				     (max x-position max-x-position))))))))))))


#+X
(defun find-text-index-for-position (font string start-x start-y mouse-x mouse-y line-height
				     &AUX (length (length (the string string))))
				     
  (declare (values index) (fixnum mouse-x mouse-y start-x start-y line-height length)
	   (string string))
  (do ((x 0 (1+ x)))
      ((= length x) NIL)
    (declare (fixnum x))
    (let ((char (aref string x)))
      (case char
	(#\Newline
	 (progn (incf start-y line-height)
		(setq start-x 0)))
	(#\TAB
	 (let ((new-x-position (get-next-tab-position start-x font)))
	   (if (and (<= start-x mouse-x new-x-position)
		    (<= start-y mouse-y (%+ start-y line-height)))
	       (return x)
	       (maximize start-x new-x-position))))
	(T (let ((char-width (char-size-from-font char font)))
	     (if (and (<= start-x mouse-x (%+ start-x char-width))
		      (<= start-y mouse-y (%+ start-y line-height)))
		 (return x)
		 (incf start-x char-width))))))))

#+clx
(defun graphics-textlength (font string index x-position
			    &AUX (length (length (the string string))))
  #.(fast)
  (declare (fixnum index length x-position) (string string) (values x-position length))
  (do ((start index (%1+ end))
       (end 0))
      (())
    (declare (fixnum start end))
    (setq end (position #\newline string :START start))
    (incf x-position (xlib:text-width font string :START start :END end))
    (unless end
      (return (values x-position length)))))



#+symbolics
(setq TV:*NOTIFICATION-POP-UP-DELAY* 5)



(defun make-graphics-transform (&KEY r11 r12 r21 r22 tx ty)
  (list r11 r12 r21 r22 tx ty))

(defun make-identity-transform ()
  (list 1 0 0 1 0 0))

(defun transform-coordinates (transform x y)
  #.(fast)
  (if (and transform *Transform-Coordinates*)
      (let (a b c d e f)
	(setq a (pop transform))
	(setq b (pop transform))
	(setq c (pop transform))
	(setq d (pop transform))
	(setq e (pop transform))
	(setq f (pop transform))
	(values (values (floor (+ (* a x) (* c y) e)))
		(values (floor (+ (* b x) (* d y) f)))))
      (values x y)))

(defun transform-distance (x y transform)
  #.(fast)
  (if (and transform *Transform-Coordinates*)
      (let (a b c d)
	(setq a (pop transform))
	(setq b (pop transform))
	(setq c (pop transform))
	(setq d (pop transform))
	(values (values (floor (+ (* a x) (* c y))))
		(values (floor (+ (* b x) (* d y))))))
      (values x y)))

(defun transform-point (x y transform)
  #.(fast)
  (transform-coordinates transform x y))

(defun fast-sin (theta)
  #.(fast)
  (declare (number theta))
  (cond ((zerop theta) 0)
	((= theta pi) 0)
	((= theta #.(/ pi 2)) 1)
	((= theta #.(* 3 (/ pi 2))) -1)
	(T (sin theta))))

(defun fast-cos (theta)
  #.(fast)
  (declare (number theta))
  (cond ((zerop theta) 1)
	((= theta pi) -1)
	((= theta #.(/ pi 2)) 0)
	((= theta #.(* 3 (/ pi 2))) 0)
	(T (cos theta))))

(defun build-graphics-transform (&KEY (rotation 0) (scale 1) (scale-x 1) (scale-y 1)
				 translation transform)
  (merge-transform-arguments (* scale scale-x) (* scale scale-y)
			     rotation translation transform))

(defun merge-transform-arguments (scale-x scale-y rotation translate transform)
  #.(fast)
  (let ((translate-x (or (first translate) 0))
	(translate-y (or (second translate) 0)))
    (unless scale-x (setq scale-x 1))(unless scale-y (setq scale-y 1))
    (unless rotation (setq rotation 0))
    (let ((sin (fast-sin rotation))
	  (cos (fast-cos rotation)))
      (if (and (zerop translate-x) (zerop translate-y)
	       (= 1 scale-x) (= 1 scale-y)
	       (zerop rotation))
	  transform
	  (merge-transforms (* scale-x cos) (* scale-x sin)
			    (* -1 scale-y sin) (* scale-y cos)
			    translate-x translate-y transform)))))

(defun scale-circle-radius (radius transform)
  #.(fast)
  (let ((scale (pop transform)))
    (if scale (values (floor (* scale radius))) radius)))

(defun merge-transforms (a b c d e f transform)
  #.(fast)
  (if transform
      (let (old-a old-b old-c old-d old-e old-f)
	(setq old-a (pop transform))
	(setq old-b (pop transform))
	(setq old-c (pop transform))
	(setq old-d (pop transform))
	(setq old-e (pop transform))
	(setq old-f (pop transform))
	(list (+ (* a old-a) (* b old-c)) (+ (* a old-b) (* b old-d))
	      (+ (* c old-a) (* d old-c)) (+ (* c old-b) (* d old-d))
	      (+ (* e old-a) (* f old-c) old-e) (+ (* e old-b) (* f old-d) old-f)))
      (list a b c d e f)))

(defmacro with-identity-transform ((&OPTIONAL stream) &BODY body)
  (let ((old-transform (gensym "OLD-TRANSFORM")))
    `(let ((,old-transform (window-transform ,stream)))
       (unwind-protect
	   (progn
	     (unless (not ,old-transform)
	       (setf (window-transform ,stream) (make-identity-transform)))
	     . ,body)
	 (setf (window-transform ,stream) ,old-transform)))))


(defmacro with-transform ((stream a b c d e f) &BODY body)
  (let ((old-transform (gensym "OLD-TRANSFORM")))
    `(let ((,old-transform (window-transform ,stream)))
       (unwind-protect
	   (progn
	     (setf (window-transform ,stream)
		   (merge-transforms ,a ,b ,c ,d ,e ,f ,old-transform))
	     . ,body)
	 (setf (window-transform ,stream) ,old-transform)))))

(defmacro with-translation ((stream delta-x delta-y) &BODY body)
  (when (eq stream T) (setq stream '*Standard-Output*))
  `(with-transform (,stream 1 0 0 1 ,delta-x ,delta-y)
     . ,body))

(defmacro with-scaling ((stream scale &OPTIONAL (y-scale scale)) &BODY body)
  (when (eq stream T) (setq stream '*Standard-Output*))
  `(with-transform (,stream ,scale 0 0 ,y-scale 0 0)
     . ,body))

(defmacro with-rotation ((stream theta) &BODY body)
  (when (eq stream T) (setq stream '*Standard-Output*))
  (let ((cos-var (gensym "COS"))
	(sin-var (gensym "SIN")))
    `(let ((,cos-var (fast-cos ,theta))
	   (,sin-var (fast-sin ,theta)))
       (with-transform (,stream ,cos-var ,sin-var (- ,sin-var) ,cos-var 0 0)
	 . ,body))))

#+ignore ;;; needs to be implemented.
(defun decompose-transform (transform)
  (declare (values rotation x-scale y-scale x-offset y-offset x-skew))
  (if (not transform) (values 0 1 1 0 0 0)
      ))


#+ignore
(defun invert-transform (transform &OPTIONAL (into-transform (make-graphics-transform)))
  )

(defun compose-transforms (transform additional-transform)
  (let (old-a old-b old-c old-d old-e old-f (trans transform))
    (setq old-a (pop trans)
	  old-b (pop trans)
	  old-c (pop trans)
	  old-d (pop trans)
	  old-e (pop trans)
	  old-f (pop trans))
    (let (other-a other-b other-c other-d other-e other-f)
      (setq other-a (pop additional-transform)
	    other-b (pop additional-transform)
	    other-c (pop additional-transform)
	    other-d (pop additional-transform)
	    other-e (pop additional-transform)
	    other-f (pop additional-transform))
      (setf (first transform) (+ (* old-a other-a) (* old-c other-b))
	    (second transform) (+ (* old-b other-a) (* old-d other-b))
	    (third transform) (+ (* old-c other-d) (* old-a other-c))
	    (fourth transform) (+ (* old-d other-d) (* old-b other-c))
	    (fifth transform) (+ old-e (* old-a other-e) (* old-c other-f))
	    (sixth transform) (+ old-f (* old-d other-f) (* old-b other-e)))))
  transform)


#+ignore
(defun angle-between-angles-p (theta theta-1 theta-2))





(defun defstipple-internal (name height width pretty-name gray-level tv-gray patterns)
  (declare (list patterns) (fixnum height width))
  (warn-unimplemented-args gray-level tv-gray NIL)
  (when (and (consp patterns) (eq 'quote (first patterns))) (setq patterns (second patterns)))
  (unless (= height (length patterns))
    (error "Height ~D should match number of patterns." height))
  ;; translate patterns (integers) to bit vectors
  (let ((black-patterns NIL)
	(white-patterns NIL))
    (mapc #'(lambda (pattern)
	      (declare (fixnum pattern))
	      (let ((vector (make-array width :ELEMENT-TYPE 'bit)))
		(declare (simple-bit-vector vector))
		(dotimes (x width)
		  (setf (aref vector x)
			(if (logbitp x pattern) 1 0)))
		(push vector black-patterns))
	      (let ((vector (make-array width :ELEMENT-TYPE 'bit)))
		(declare (simple-bit-vector vector))
		(dotimes (x width)
		  (setf (aref vector x)
			(if (logbitp x pattern) 0 1)))
		(push vector white-patterns)))
	  patterns)
    (setq black-patterns (nreverse black-patterns)
	  white-patterns (nreverse white-patterns))
    (let ((black-image (apply #'xlib:bitmap-image NIL black-patterns))
	  (white-image (apply #'xlib:bitmap-image NIL white-patterns))
	  (pattern-name (or pretty-name name)))
      (setq *stipple-arrays* (delete pattern-name *stipple-arrays*
				     :KEY #'stipple-pattern-name :TEST #'EQUALP))
      (let ((stipple-pattern (make-stipple-pattern :name pattern-name
						   :height height :width width
						   :black-image black-image
						   :white-image white-image)))
	(push stipple-pattern *stipple-arrays*)
	stipple-pattern))))

(defun get-pattern-pixmap (pattern)
  (or (second (assoc (xlib:screen-root *screen*) (stipple-pattern-pixmaps pattern)))
      (let ((pixmap (xlib:image-pixmap (xlib:screen-root *screen*)
				       (if (not (= 0 (xlib:screen-black-pixel *Screen*)))
					   (stipple-pattern-black-image pattern)
					   (stipple-pattern-white-image pattern)))))
	(push (list (xlib:screen-root *screen*) pixmap)
	      (stipple-pattern-pixmaps pattern))
	pixmap)))

(defstipple 51%-gray (2 2) (:tv-gray t :gray-level t)
	    (#b1 #b0))

(defstipple 25%-gray (2 2) (:tv-gray t :gray-level t)
	    (#b10 #b00))

(defstipple 5.5%-gray (4 5) ()
	    (#b10000
	     #b00000
	     #b00000
	     #b00000))

(defstipple vertical-lines (4 4) ()
	    (#b1000 #b1000 #b1000 #b1000))

(defstipple horizontal-lines (4 4) ()
	    (#b1111 #b0000 #b0000 #b0000))

(defstipple vertical-dashes (6 6) ()
	    (#b000100 #b000100 #b000100 #b100000 #b100000 #b100000))

(defstipple filled-diamonds (9 9) ()
	    (#b000010000
	     #b000111000
	     #b001111100
	     #b011111110
	     #b111111111
	     #b011111110
	     #b001111100
	     #b000111000
	     #b000010000
	     ))

(defstipple brick (16 16) ()
	    (#b1111111111111111
	     #b1000000000000000
	     #b1000000000000000
	     #b1000000000000000
	     #b1000000000000000
	     #b1000000000000000
	     #b1000000000000000
	     #b1000000000000000
	     #b1111111111111111
	     #b0000000010000000
	     #b0000000010000000
	     #b0000000010000000
	     #b0000000010000000
	     #b0000000010000000
	     #b0000000010000000
	     #b0000000010000000))



(defstipple non-selected-radio-button (13 13) ()
	    (#b0000111110000
	     #b0011000001100
	     #b0110000000110
	     #b0100000000010
	     #b1000000000001
	     #b1000000000001
	     #b1000000000001
	     #b1000000000001
	     #b1000000000001
	     #b0100000000010
	     #b0110000000110
	     #b0011000001100
	     #b0000111110000))

(defstipple selected-radio-button (13 13) ()
	    (#b0000111110000
	     #b0011000001100
	     #b0110000000110
	     #b0100111110010
	     #b1001111111001
	     #b1001111111001
	     #b1001111111001
	     #b1001111111001
	     #b1001111111001
	     #b0100111110010
	     #b0110000000110
	     #b0011000001100
	     #b0000111110000))




;       large-diamonds            small-diamonds
;       filled-diamonds           weave8b
;       weave8                    parquet
;       diagonals                 small-diamonds
;       hearts                    tiles
;       double-bricks             half-bricks
;       bricks                    vertical-lines
;       horizontal-lines          vertical-dashes
;       horizontal-dashes         tracks
;       alt-rain                  southwest-rain
;       southeast-rain            southwest-thick-hatch
;       southeast-thick-hatch     southwest-thin-hatch
;       southeast-thin-hatch      southwest-dense-hatch
;       southeast-dense-hatch
;    5.5%-gray       6%-gray                 7%-gray       8%-gray                 9%-gray
;       10%-gray                12%-gray       hes-gray                 33%-gray
;       75%-gray                25%-gray       50%-gray





(defmacro def-mouse-cursor (name pixmap mask &OPTIONAL (x 0)
			    (y 0))
  `(progn
     (set ',name
	   (xlib:create-cursor :SOURCE ,pixmap :X ,x :Y ,y :mask ,mask
			       :FOREGROUND
			       (xlib:make-color)
			       :BACKGROUND
			       (xlib:make-color :red 0.0
						:green 0.0
						:blue 0.0))
	   #+ignore
	   (xlib:create-cursor :SOURCE ,pixmap :X ,x :Y ,y :mask ,mask
			       :FOREGROUND
			       (if (= 1 (xlib:screen-black-pixel *Screen*))
				   (xlib:make-color)
				   (xlib:make-color :red 0.0
							    :green 0.0
							    :blue 0.0))
			       :BACKGROUND
			       (if (= 1 (xlib:screen-black-pixel *Screen*))
				   (xlib:make-color :red 0.0
							    :green 0.0
							    :blue 0.0)
				   (xlib:make-color))))))





(defstipple mouse-nw-arrow-pattern (16. 14.) ()
	    (#b00000000000111
	     #b00000000001111
	     #b00000000011111
	     #b00000000111111
	     #b00000001111111
	     #b00000011111111
	     #b00000111111111
	     #b00000001111111
	     #b00000001111111
	     #b00000011111010
	     #b00000011111000
	     #b00000111110000
	     #b00000111110000
	     #b00001111100000
	     #b00001111100000
	     #b00000111000000
	     ))

(defstipple mouse-nw-arrow-pattern-inverse (16. 14.) ()
	    (#b11111111111101
	     #b11111111111001
	     #b11111111110001
	     #b11111111100001
	     #b11111111000001
	     #b11111110000001
	     #b11111100000001
	     #b11111111000001
	     #b11111111001101
	     #b11111110001111
	     #b11111110001111
	     #b11111100011111
	     #b11111100011111
	     #b11111000111111
	     #b11111000111111
	     #b11111111111111
	     ))

(defstipple mouse-horizontal-double-arrow-pattern (8 16) ()
	    (#b0001100000011000
	     #b0011000000001100
	     #b0110000000000110
	     #b1111111111111111
	     #b1111111111111111
	     #b0110000000000110
	     #b0011000000001100
	     #b0001100000011000
	     ))

(defstipple mouse-vertical-double-arrow-pattern (16 8) ()
	    (#b00011000
	     #b00111100
	     #b01111110
	     #b11011011
	     #b10011001
	     #b00011000
	     #b00011000
	     #b00011000
	     #b00011000
	     #b00011000
	     #b00011000
	     #b10011001
	     #b11011011
	     #b01111110
	     #b00111100
	     #b00011000
	     ))

(defstipple mouse-horizontal-double-arrow-pattern-inverse (8 16) ()
	    (#b1110011111100111
	     #b1100111111110011
	     #b1001111111111001
	     #b0000000000000000
	     #b0000000000000000
	     #b1001111111111001
	     #b1100111111110011
	     #b1110011111100111
	     ))

(defstipple mouse-vertical-double-arrow-pattern-inverse (16 8) ()
	    (#b11100111
	     #b11000011
	     #b10000001
	     #b00100100
	     #b01100110
	     #b11100111
	     #b11100111
	     #b11100111
	     #b11100111
	     #b11100111
	     #b11100111
	     #b01100110
	     #b00100100
	     #b10000001
	     #b11000011
	     #b11100111
	     ))

(defun init-mouse-cursors ()
  (def-mouse-cursor
      mouse-nw-arrow

      (get-pattern-pixmap mouse-nw-arrow-pattern-inverse)
      (get-pattern-pixmap mouse-nw-arrow-pattern))
  (def-mouse-cursor
      mouse-horizontal-double-arrow
      (get-pattern-pixmap mouse-horizontal-double-arrow-pattern-inverse)
      (get-pattern-pixmap mouse-horizontal-double-arrow-pattern)
    4 8)
  (def-mouse-cursor
      mouse-vertical-double-arrow
      (get-pattern-pixmap mouse-vertical-double-arrow-pattern-inverse)
      (get-pattern-pixmap mouse-vertical-double-arrow-pattern)
    8 4))




(defmacro drawing-path ((&OPTIONAL stream &REST draw-path-args) &BODY body)
  (declare (ignore stream draw-path-args))
  `(progn . ,body))

(defun draw-line-to (&rest args)
  args)





;;; This spends a lot of time in typep checking.
;;; If we want to bum this to use non-common-lisp
;;; structures we could speed it up.

;; If you change this function, check redraw-window too.
(defun redraw-presentation (presentation stream left top right bottom
			    &OPTIONAL (redraw-inferiors T)
			    &AUX
			    (*Record-Presentations-P* NIL)
			    (*Transform-Coordinates* NIL))
  #.(fast)
  (when (or (null left)
	    (not (or (%> (boxed-presentation-left presentation) (the fixnum right))
		     (%> (boxed-presentation-top presentation) (the fixnum bottom))
		     (%< (boxed-presentation-right presentation) (the fixnum left))
		     (%< (boxed-presentation-bottom presentation) (the fixnum top)))))
    (let ((type (structure-type presentation)))
      (case type
	(PRESENTATION
	  (when redraw-inferiors
	    (dolist (record (presentation-records presentation))
	      (medium-fast-redraw-presentation record stream left top right bottom))))
	#+ignore
	(TEXT-PRESENTATION
	  (redraw-text-presentation presentation stream))
	(T (funcall type ; (get type :PRESENTATION-REDRAW-FUNCTION)
		    presentation stream))))))


;;; Call this as an internal primitive when you have coordinates to check
;;; and so far the presentation is bigger than the window size.
(defun medium-fast-redraw-presentation (presentation stream left top right bottom)
  #.(fast)
  (let ((type (structure-type presentation)))
    (case type
      (PRESENTATION
	(dolist (record (presentation-records presentation))
	  (if left
	      ;; figure out if we can go fast yet and avoid the checking.
	      (if (and (%>= (the fixnum right) (boxed-presentation-left record))
		       (%>= (the fixnum bottom) (boxed-presentation-top record))
		       (%<= (the fixnum left) (boxed-presentation-right record))
		       (%<= (the fixnum top) (boxed-presentation-bottom record)))
		  (fast-redraw-presentation record stream)
		  (medium-fast-redraw-presentation record stream left top right bottom))
	      (fast-redraw-presentation record stream))))
      #+ignore
      (TEXT-PRESENTATION
	(redraw-text-presentation presentation stream))
      (T (funcall type ;(get type :PRESENTATION-REDRAW-FUNCTION)
		  presentation stream)))))


;;; Call this when you are sure the presentation is on the window 
;;; It will still work if you call it on something off the window, but it is intended to be
;;; used for speed so don't waste it's time with presentations that are not on the screen.
(defun fast-redraw-presentation (presentation stream)
  #.(fast)
  (let ((type (structure-type presentation)))
    (case type
      (PRESENTATION
	(dolist (record (presentation-records presentation))
	  (fast-redraw-presentation record stream)))
      #+IGNORE
      (TEXT-PRESENTATION
	(redraw-text-presentation presentation stream))
      (T (funcall type ; (get type :PRESENTATION-REDRAW-FUNCTION)
		  presentation stream)))))


#+ignore
(setf (get 'COMPLEX-STRING-PRESENTATION :presentation-redraw-function)
      'REDRAW-COMPLEX-STRING-PRESENTATION)

(defun complex-string-presentation (presentation stream)
  #+X
  (prepare-window (stream)
    (if *erase-p*
	(multiple-value-bind (left top)
	    (convert-window-coords-to-screen-coords
	      stream
	      (boxed-presentation-left presentation)
	      (boxed-presentation-top presentation))
	  (multiple-value-bind (right bottom)
	      (convert-window-coords-to-screen-coords
		stream
		(boxed-presentation-right presentation)
		(boxed-presentation-bottom presentation))
	    (xlib:with-gcontext ((window-gcontext stream)
				 :FUNCTION (draw-alu :ERASE))
	      (xlib:draw-rectangle
		(window-real-window stream) (window-gcontext stream)
		left top (%- right left) (%- bottom top)
		T))))
	(multiple-value-bind (x y)
	    (convert-window-coords-to-screen-coords
	      stream
	      (boxed-presentation-left presentation) (boxed-presentation-top presentation))
	  (let ((font (string-presentation-font presentation))
		(alu (graphics-presentation-alu presentation)))
	    (if (or (symbolp alu) (integerp alu))
		(xlib:with-gcontext ((window-gcontext stream) :FONT font
				     :FUNCTION (draw-alu alu))
		  (xlib:draw-glyphs (window-real-window stream) (window-gcontext stream)
				    x (%+ y (font-ascent font))
				    (string-presentation-string presentation)))
		(xlib:draw-glyphs (window-real-window stream) alu
				  x (%+ y (font-ascent font))
				  (string-presentation-string presentation))))))))

#+ignore
(setf (get 'STRING-PRESENTATION :presentation-redraw-function)
      'REDRAW-STRING-PRESENTATION)
(defun string-presentation (presentation stream)
  #+X
  (prepare-window (stream)
    (if *erase-p*
	(multiple-value-bind (left top)
	    (convert-window-coords-to-screen-coords
	      stream
	      (boxed-presentation-left presentation)
	      (boxed-presentation-top presentation))
	  (multiple-value-bind (right bottom)
	      (convert-window-coords-to-screen-coords
		stream
		(boxed-presentation-right presentation)
		(boxed-presentation-bottom presentation))
	    (xlib:with-gcontext ((window-gcontext stream)
				 :FUNCTION (draw-alu :ERASE))
	      (xlib:draw-rectangle
		(window-real-window stream) (window-gcontext stream)
		left top (%- right left) (%- bottom top) T))))
	(multiple-value-bind (x y)
	    (convert-window-coords-to-screen-coords
	      stream
	      (boxed-presentation-left presentation) (boxed-presentation-top presentation))
	  (let ((font (string-presentation-font presentation))
		(alu (graphics-presentation-alu presentation)))
	    (if (or (symbolp alu) (integerp alu))
		(xlib:with-gcontext ((window-gcontext stream) :FONT font
				     :FUNCTION (draw-alu alu))
		  (xlib:draw-glyphs (window-real-window stream) (window-gcontext stream)
				    x (%+ y (font-ascent font))
				    (string-presentation-string presentation)))
		(xlib:draw-glyphs (window-real-window stream) alu
				  x (%+ y (font-ascent font))
				  (string-presentation-string presentation))))))))


#+ignore
(setf (get 'TEXT-PRESENTATION :presentation-redraw-function)
      'REDRAW-TEXT-PRESENTATION)
(defun text-presentation (presentation stream)
  #.(fast)
  (with-presentations-disabled
    (multiple-value-bind (width height)
	(window-size stream)
      (with-output-truncation (stream)
	(presentation-print-function-internal
	  (text-presentation-string presentation)
	  stream
	  (boxed-presentation-left presentation)
	  (boxed-presentation-top presentation)
	  0 NIL NIL
	  (text-presentation-font presentation)
	  width height)))))

#+ignore
(setf (get 'COMPLEX-RECTANGLE-PRESENTATION :presentation-redraw-function)
      'REDRAW-COMPLEX-RECTANGLE-PRESENTATION)

(defun complex-rectangle-presentation (presentation stream)
  (let ((half-thickness
	  (the (values fixnum number)
	       (floor (rectangle-presentation-thickness presentation) 2))))
    (declare (fixnum half-thickness))
    (draw-rectangle
      (%+ (boxed-presentation-left presentation) half-thickness)
      (%+ (boxed-presentation-top presentation) half-thickness)
      (%+ (boxed-presentation-right presentation) (%- (%+ half-thickness half-thickness)))
      (%+ (boxed-presentation-bottom presentation) (%- (%+ half-thickness half-thickness)))
      :ALU (graphics-presentation-alu presentation)
      :STREAM stream
      :PATTERN (rectangle-presentation-pattern presentation)
      :FILLED (rectangle-presentation-filled presentation)
      :THICKNESS (rectangle-presentation-thickness presentation)

      :STIPPLE (complex-rectangle-presentation-stipple presentation)
      :TILE (complex-rectangle-presentation-tile presentation)
      :COLOR (complex-rectangle-presentation-color presentation)
      :GRAY-LEVEL (complex-rectangle-presentation-gray-level presentation)
      :OPAQUE (complex-rectangle-presentation-opaque presentation)

;;      :MASK (complex-rectangle-presentation-mask presentation)
;;      :MASK-X (complex-rectangle-presentation-mask-x presentation)
;;      :MASK-Y (complex-rectangle-presentation-mask-y presentation)

      :LINE-END-SHAPE (complex-rectangle-presentation-line-end-shape presentation)
      :LINE-JOINT-SHAPE (complex-rectangle-presentation-line-joint-shape presentation)

      :DASHED (complex-rectangle-presentation-dashed presentation)
      :DASH-PATTERN (complex-rectangle-presentation-dash-pattern presentation)
      :INITIAL-DASH-PHASE
      (complex-rectangle-presentation-initial-dash-phase presentation)
      :DRAW-PARTIAL-DASHES
      (complex-rectangle-presentation-draw-partial-dashes presentation))))

#+ignore
(setf (get 'RECTANGLE-PRESENTATION :presentation-redraw-function)
      'REDRAW-RECTANGLE-PRESENTATION)

(defun rectangle-presentation (presentation stream)
  (let ((half-thickness
	  (the (values fixnum number)
	       (floor (rectangle-presentation-thickness presentation) 2))))
    (declare (fixnum half-thickness))
    (draw-rectangle
      (%+ (boxed-presentation-left presentation) half-thickness)
      (%+ (boxed-presentation-top presentation) half-thickness)
      (%+ (boxed-presentation-right presentation) (%- (%+ half-thickness half-thickness)))
      (%+ (boxed-presentation-bottom presentation) (%- (%+ half-thickness half-thickness)))
      :ALU (graphics-presentation-alu presentation)
      :STREAM stream
      :PATTERN (rectangle-presentation-pattern presentation)
      :FILLED (rectangle-presentation-filled presentation)
      :THICKNESS (rectangle-presentation-thickness presentation))))


#+ignore
(setf (get 'COMPLEX-POINT-PRESENTATION :presentation-redraw-function)
      'REDRAW-COMPLEX-POINT-PRESENTATION)

(defun COMPLEX-POINT-PRESENTATION (presentation stream)
  (draw-point
    (boxed-presentation-left presentation)
    (boxed-presentation-top presentation)
    :ALU (graphics-presentation-alu presentation)
    :STREAM stream
    :PATTERN (point-presentation-pattern presentation)
    :STIPPLE (complex-point-presentation-stipple presentation)
    :TILE (complex-point-presentation-tile presentation)
    :COLOR (complex-point-presentation-color presentation)
    :GRAY-LEVEL (complex-point-presentation-gray-level presentation)
    :OPAQUE (complex-point-presentation-opaque presentation)

;;    :MASK (complex-point-presentation-mask presentation)
;;    :MASK-X (complex-point-presentation-mask-x presentation)
;;    :MASK-Y (complex-point-presentation-mask-y presentation)
    ))

#+ignore
(setf (get 'POINT-PRESENTATION :presentation-redraw-function)
      'REDRAW-POINT-PRESENTATION)

(defun POINT-PRESENTATION (presentation stream)
  (draw-point
    (boxed-presentation-left presentation)
    (boxed-presentation-top presentation)
    :ALU (graphics-presentation-alu presentation)
    :STREAM stream
    :PATTERN (point-presentation-pattern presentation)))


#+ignore
(setf (get 'COMPLEX-POLYGON-PRESENTATION :presentation-redraw-function)
      'REDRAW-COMPLEX-POLYGON-PRESENTATION)

(defun COMPLEX-POLYGON-PRESENTATION (presentation stream)
  (draw-polygon
    (polygon-presentation-points presentation)
    :ALU (graphics-presentation-alu presentation)
    :STREAM stream
    :PATTERN (polygon-presentation-pattern presentation)
    :FILLED (polygon-presentation-filled presentation)
    :THICKNESS (polygon-presentation-thickness presentation)

    :STIPPLE (complex-polygon-presentation-stipple presentation)
    :TILE (complex-polygon-presentation-tile presentation)
    :COLOR (complex-polygon-presentation-color presentation)
    :GRAY-LEVEL (complex-polygon-presentation-gray-level presentation)
    :OPAQUE (complex-polygon-presentation-opaque presentation)

;;    :MASK (complex-polygon-presentation-mask presentation)
;;    :MASK-X (complex-polygon-presentation-mask-x presentation)
;;    :MASK-Y (complex-polygon-presentation-mask-y presentation)

    :LINE-END-SHAPE (complex-polygon-presentation-line-end-shape presentation)
    :LINE-JOINT-SHAPE (complex-polygon-presentation-line-joint-shape presentation)

    :DASHED (complex-polygon-presentation-dashed presentation)
    :DASH-PATTERN (complex-polygon-presentation-dash-pattern presentation)
    :INITIAL-DASH-PHASE
    (complex-polygon-presentation-initial-dash-phase presentation)
    :DRAW-PARTIAL-DASHES
    (complex-polygon-presentation-draw-partial-dashes presentation)))

#+ignore
(setf (get 'POLYGON-PRESENTATION :presentation-redraw-function)
      'REDRAW-POLYGON-PRESENTATION)

(defun POLYGON-PRESENTATION (presentation stream)
  (draw-polygon
    (polygon-presentation-points presentation)
    :ALU (graphics-presentation-alu presentation)
    :STREAM stream
    :PATTERN (polygon-presentation-pattern presentation)
    :FILLED (polygon-presentation-filled presentation)
    :THICKNESS (polygon-presentation-thickness presentation)))

#+ignore
(setf (get 'COMPLEX-glyph-PRESENTATION :presentation-redraw-function)
      'REDRAW-COMPLEX-glyph-PRESENTATION)

(defun COMPLEX-GLYPH-PRESENTATION (presentation stream)
  (draw-glyph (glyph-presentation-index presentation)
	      (glyph-presentation-font presentation)
	      (glyph-presentation-left presentation)
	      (glyph-presentation-top presentation)
	      :ALU (graphics-presentation-alu presentation)
	      :STREAM stream
	      :PATTERN (glyph-presentation-pattern presentation)
	      :STIPPLE (complex-glyph-presentation-stipple presentation)
	      :TILE (complex-glyph-presentation-tile presentation)
	      :COLOR (complex-glyph-presentation-color presentation)
	      :GRAY-LEVEL (complex-glyph-presentation-gray-level presentation)
	      :OPAQUE (complex-glyph-presentation-opaque presentation)
;;	      :MASK (complex-glyph-presentation-mask presentation)
;;	      :MASK-X (complex-glyph-presentation-mask-x presentation)
;;	      :MASK-Y (complex-glyph-presentation-mask-y presentation)
	      ))

#+ignore
(setf (get 'glyph-PRESENTATION :presentation-redraw-function)
      'REDRAW-glyph-PRESENTATION)

(defun GLYPH-PRESENTATION (presentation stream)
  (draw-glyph (glyph-presentation-index presentation)
	      (glyph-presentation-font presentation)
	      (glyph-presentation-left presentation)
	      (%+ (glyph-presentation-top presentation)
		  (font-ascent (glyph-presentation-font presentation)))
	      :ALU (graphics-presentation-alu presentation)
	      :STREAM stream
	      :PATTERN (glyph-presentation-pattern presentation)))


#+ignore
(setf (get 'COMPLEX-circle-PRESENTATION :presentation-redraw-function)
      'REDRAW-COMPLEX-circle-PRESENTATION)

(defun COMPLEX-CIRCLE-PRESENTATION (presentation stream)
  (draw-circle
    (circle-presentation-center-x presentation)
    (circle-presentation-center-y presentation)
    (circle-presentation-radius presentation)
    :ALU (graphics-presentation-alu presentation)
    :STREAM stream
    :INNER-RADIUS (circle-presentation-inner-radius presentation)
    :START-ANGLE (circle-presentation-start-angle presentation)
    :END-ANGLE (circle-presentation-end-angle presentation)

    :PATTERN (circle-presentation-pattern presentation)
    :FILLED (circle-presentation-filled presentation)
    :THICKNESS (circle-presentation-thickness presentation)
    :OPAQUE (complex-circle-presentation-opaque presentation)
;;    :MASK (complex-circle-presentation-mask presentation)
;;    :MASK-X (complex-circle-presentation-mask-x presentation)
;;    :MASK-Y (complex-circle-presentation-mask-y presentation)
    :LINE-END-SHAPE (complex-circle-presentation-line-end-shape presentation)
    :LINE-JOINT-SHAPE (complex-circle-presentation-line-joint-shape presentation)
    :DASHED (complex-circle-presentation-dashed presentation)
    :DASH-PATTERN (complex-circle-presentation-dash-pattern presentation)
    :INITIAL-DASH-PHASE
    (complex-circle-presentation-initial-dash-phase presentation)
    :DRAW-PARTIAL-DASHES
    (complex-circle-presentation-draw-partial-dashes presentation)))


#+ignore
(setf (get 'circle-PRESENTATION :presentation-redraw-function)
      'REDRAW-circle-PRESENTATION)

(defun CIRCLE-PRESENTATION (presentation stream)
  (draw-circle
    (circle-presentation-center-x presentation)
    (circle-presentation-center-y presentation)
    (circle-presentation-radius presentation)
    :ALU (graphics-presentation-alu presentation)
    :STREAM stream
    :INNER-RADIUS (circle-presentation-inner-radius presentation)
    :START-ANGLE (circle-presentation-start-angle presentation)
    :END-ANGLE (circle-presentation-end-angle presentation)
    :PATTERN (circle-presentation-pattern presentation)
    :FILLED (circle-presentation-filled presentation)
    :THICKNESS (circle-presentation-thickness presentation)))


#+ignore
(setf (get 'COMPLEX-icon-PRESENTATION :presentation-redraw-function)
      'REDRAW-COMPLEX-icon-PRESENTATION)

(defun COMPLEX-ICON-PRESENTATION (presentation stream)
  (draw-icon
    (icon-presentation-icon-pattern presentation)
    (icon-presentation-left presentation)
    (icon-presentation-right presentation)
    :ALU (graphics-presentation-alu presentation)
    :STREAM stream
    :OPAQUE (complex-icon-presentation-opaque presentation)
;;    :MASK (complex-icon-presentation-mask presentation)
;;    :MASK-X (complex-icon-presentation-mask-x presentation)
;;    :MASK-Y (complex-icon-presentation-mask-y presentation)
    ))

#+ignore
(setf (get 'icon-PRESENTATION :presentation-redraw-function)
      'REDRAW-icon-PRESENTATION)

(defun ICON-PRESENTATION (presentation stream)
  (draw-icon
    (icon-presentation-icon-pattern presentation)
    (icon-presentation-left presentation)
    (icon-presentation-top presentation)
    :ALU (graphics-presentation-alu presentation)
    :STREAM stream))

#+ignore
(setf (get 'COMPLEX-line-PRESENTATION :presentation-redraw-function)
      'REDRAW-COMPLEX-line-presentation)

(defun COMPLEX-LINE-PRESENTATION (presentation stream)
  (draw-line (line-presentation-start-x presentation)
	     (line-presentation-start-y presentation)
	     (line-presentation-end-x presentation)
	     (line-presentation-end-y presentation)
	     :ALU (graphics-presentation-alu presentation)
	     :STREAM stream
	     :PATTERN (line-presentation-pattern presentation)
	     :THICKNESS (line-presentation-thickness presentation)

	     :STIPPLE (complex-line-presentation-stipple presentation)
	     :TILE (complex-line-presentation-tile presentation)
	     :COLOR (complex-line-presentation-color presentation)
	     :GRAY-LEVEL (complex-line-presentation-gray-level presentation)
	     :OPAQUE (complex-line-presentation-opaque presentation)

;;	     :MASK (complex-line-presentation-mask presentation)
;;	     :MASK-X (complex-line-presentation-mask-x presentation)
;;	     :MASK-Y (complex-line-presentation-mask-y presentation)

	     :LINE-END-SHAPE (complex-line-presentation-line-end-shape presentation)
	     :LINE-JOINT-SHAPE (complex-line-presentation-line-joint-shape presentation)

	     :DASHED (complex-line-presentation-dashed presentation)
	     :DASH-PATTERN (complex-line-presentation-dash-pattern presentation)
	     :INITIAL-DASH-PHASE
	     (complex-line-presentation-initial-dash-phase presentation)
	     :DRAW-PARTIAL-DASHES
	     (complex-line-presentation-draw-partial-dashes presentation)))

#+ignore
(setf (get 'line-PRESENTATION :presentation-redraw-function)
      'REDRAW-line-presentation)
(defun LINE-PRESENTATION (presentation stream)
  (draw-line (line-presentation-start-x presentation)
	     (line-presentation-start-y presentation)
	     (line-presentation-end-x presentation)
	     (line-presentation-end-y presentation)
	     :ALU (graphics-presentation-alu presentation)
	     :STREAM stream
	     :PATTERN (line-presentation-pattern presentation)
	     :THICKNESS (line-presentation-thickness presentation)))

#+ignore
(setf (get 'arrow-PRESENTATION :presentation-redraw-function)
      'REDRAW-arrow-presentation)
(defun ARROW-PRESENTATION (presentation stream)
  (draw-arrow (arrow-presentation-x1 presentation)
	      (arrow-presentation-y1 presentation)
	      (arrow-presentation-x2 presentation)
	      (arrow-presentation-y2 presentation)
	      :ALU (graphics-presentation-alu presentation)
	      :DRAW-SHAFT (arrow-presentation-draw-shaft presentation)
	      :ARROW-HEAD-LENGTH (arrow-presentation-head-length presentation)
	      :ARROW-HEAD-WIDTH (arrow-presentation-head-width presentation)
	      :THICKNESS (arrow-presentation-thickness presentation)
	      :STREAM stream))

#+ignore
(setf (get 'triangle-PRESENTATION :presentation-redraw-function)
      'REDRAW-triangle-presentation)
(defun TRIANGLE-PRESENTATION (presentation stream)
  (draw-triangle (triangle-presentation-x1 presentation)
		 (triangle-presentation-y1 presentation)
		 (triangle-presentation-x2 presentation)
		 (triangle-presentation-y2 presentation)
		 (triangle-presentation-x3 presentation)
		 (triangle-presentation-y3 presentation)
		 :ALU (graphics-presentation-alu presentation)
		 :THICKNESS (triangle-presentation-thickness presentation)
		 :FILLED (triangle-presentation-filled presentation)
		 :STREAM stream))

#+ignore
(setf (get 'COMPLEX-ellipse-PRESENTATION :presentation-redraw-function)
      'REDRAW-COMPLEX-ellipse-presentation)
(defun COMPLEX-ELLIPSE-PRESENTATION (presentation stream)
  (draw-ellipse
    (ellipse-presentation-center-x presentation)
    (ellipse-presentation-center-y presentation)
    (ellipse-presentation-x-radius presentation)
    (ellipse-presentation-y-radius presentation)
    :ALU (graphics-presentation-alu presentation)
    :STREAM stream
    :INNER-X-RADIUS (ellipse-presentation-inner-x-radius presentation)
    :INNER-Y-RADIUS (ellipse-presentation-inner-y-radius presentation)
    :START-ANGLE (ellipse-presentation-start-angle presentation)
    :END-ANGLE (ellipse-presentation-end-angle presentation)

    :PATTERN (ellipse-presentation-pattern presentation)
    :FILLED (ellipse-presentation-filled presentation)
    :THICKNESS (ellipse-presentation-thickness presentation)
    :OPAQUE (complex-ellipse-presentation-opaque presentation)
;;    :MASK (complex-ellipse-presentation-mask presentation)
;;    :MASK-X (complex-ellipse-presentation-mask-x presentation)
;;    :MASK-Y (complex-ellipse-presentation-mask-y presentation)
    :LINE-END-SHAPE (complex-ellipse-presentation-line-end-shape presentation)
    :LINE-JOINT-SHAPE (complex-ellipse-presentation-line-joint-shape presentation)
    :DASHED (complex-ellipse-presentation-dashed presentation)
    :DASH-PATTERN (complex-ellipse-presentation-dash-pattern presentation)
    :INITIAL-DASH-PHASE
    (complex-ellipse-presentation-initial-dash-phase presentation)
    :DRAW-PARTIAL-DASHES
    (complex-ellipse-presentation-draw-partial-dashes presentation)))
      

#+ignore
(setf (get 'ellipse-PRESENTATION :presentation-redraw-function)
      'REDRAW-ellipse-presentation)

(defun ELLIPSE-PRESENTATION (presentation stream)
  (draw-ellipse
    (ellipse-presentation-center-x presentation)
    (ellipse-presentation-center-y presentation)
    (ellipse-presentation-x-radius presentation)
    (ellipse-presentation-y-radius presentation)
    :ALU (graphics-presentation-alu presentation)
    :STREAM stream
    :INNER-X-RADIUS (ellipse-presentation-inner-x-radius presentation)
    :INNER-Y-RADIUS (ellipse-presentation-inner-y-radius presentation)
    :START-ANGLE (ellipse-presentation-start-angle presentation)
    :END-ANGLE (ellipse-presentation-end-angle presentation)
    :PATTERN (ellipse-presentation-pattern presentation)
    :FILLED (ellipse-presentation-filled presentation)
    :THICKNESS (ellipse-presentation-thickness presentation)))


;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
