;;; -*- Mode: LISP; Syntax: Common-lisp; Package: Express-windows; Base: 10 -*-

;;; This file is part of Express Windows.

;;; Express Windows is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY.  No author or distributor
;;; accepts responsibility to anyone for the consequences of using it
;;; or for whether it serves any particular purpose or works at all,
;;; unless he says so in writing.  Refer to the Express Windows General Public
;;; License for full details.

;;; Everyone is granted permission to copy, modify and redistribute
;;; Express Windows, but only under the conditions described in the
;;; Express Windows General Public License.   A copy of this license is
;;; supposed to have been given to you along with Express Windows so you
;;; can know your rights and responsibilities.  It should be in a
;;; file named COPYING.  Among other things, the copyright notice
;;; and this notice must be preserved on all copies.  */

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; ********** (c) Copyright 1988, 1989, 1990 Liszt Programming Inc. All Rights Reserved *********
;;; ****************************************************************************************
;;; ****************************************************************************************

;;; ****************************************************************************************
;;; ****************************************************************************************
;;; **************** Written by Dr. Andrew L. Ressler **************************************
;;; ****************************************************************************************
;;; ****************************************************************************************

(cl:in-package 'ew)



(defstruct (command-table (:constructor make-command-table-internal)
			  (:print-function print-command-table))
  name
  (commands-hash-table (make-hash-table))
  inherit-from
  delims
  kbd-accelerator-table
  menu-accelerator-table
  command-aarray)

(defstruct keyboard-command-table
  command-alist)

(defun print-command-table (command-table stream ignore)
  (format stream "#<Command Table ~A>" (command-table-name command-table)))

(defun find-command-table (name &KEY (if-does-not-exist :error))
  (if (lisp:typep name 'COMMAND-TABLE) name
      (progn
	(setq name (string name))
	(or (find name *Command-Tables* :KEY #'command-table-name :TEST #'EQUALP)
	    (ecase if-does-not-exist
	      (:ERROR (error "There is no command table named ~A." name))
	      (:CREATE (let ((table (make-command-table :NAME name)))
			 (push table *Command-Tables*)
			 table))
	      ((NIL) NIL))))))

(defun delete-command-table (command-table-or-name)
  (unless (lisp:typep command-table-or-name 'command-table)
    (setq command-table-or-name (find-command-table command-table-or-name)))
  (if (member command-table-or-name *Command-Tables*)
      (setq *Command-Tables* (delete command-table-or-name *Command-Tables*))
      (format T "~&Command table ~A does not exist to delete."
	      (command-table-name command-table-or-name))))


(defun make-command-table (name &REST init-options &KEY (if-exists :error)
			      &ALLOW-OTHER-KEYS)
  (let ((table (find-command-table name :if-does-not-exist NIL)))
    (if (and table
	     (eq if-exists :ERROR))
	(error "Command Table ~A already exists." name)
	(progn
	  (unless table
	    (setq table (make-command-table-internal :NAME name))
	    (push table *Command-Tables*))
	  (setf (command-table-delims table)
		(or (getf init-options :COMMAND-TABLE-DELIMS)
		    (list #\CR #\Space #\Tab)))
	  (setf (command-table-inherit-from table)
		(getf init-options :inherit-from))
	  (if (or (getf init-options :accelerator-p)
		  (getf init-options :kbd-accelerator-p))
	      (unless (command-table-kbd-accelerator-table table)
		(setf (command-table-kbd-accelerator-table table)
		      (make-keyboard-command-table)))
	      (setf (command-table-kbd-accelerator-table table) NIL))))
    table))


(defmacro define-command-accelerator (name command-table characters options arglist &BODY body)
  (declare (ignore options))
  `(progn
     (install-accelerator-command ',name ',command-table ',characters)
     (defun ,name ,arglist . ,body)))

(defun install-accelerator-command (name command-table characters)
  (setq command-table (find-command-table command-table))
  (let ((keyboard-table (command-table-kbd-accelerator-table command-table)))
    (unless keyboard-table (error "Cannot add a keyboard accelerator unless command table was defined to handle them."))
    (dolist (char (if (listp characters) characters (list characters)))
      (let ((entry (assoc char (keyboard-command-table-command-alist keyboard-table)
			  :TEST #'EQUALP)))
	(if entry
	    (setf (second entry) name)
	    (push (list char name) (keyboard-command-table-command-alist keyboard-table)))))))



(defmacro define-command (name-and-options arguments &BODY body)
  (let (command-name name command-table values provide-output-destination-keyword
	explicit-arglist)
    (if (consp name-and-options)
	(setq command-name (first name-and-options)
	      name (or (getf (cdr name-and-options) :NAME)
		       (make-pretty-name command-name))
	      command-table (getf (cdr name-and-options) :command-table)
	      values (getf (cdr name-and-options) :values)
	      provide-output-destination-keyword
	      (getf (cdr name-and-options) :provide-output-destination-keyword)
	      explicit-arglist
	      (getf (cdr name-and-options) :explicit-arglist))
	(setq name (make-pretty-name name-and-options)
	      command-name name-and-options))
    (let ((function-arguments (get-argument-list arguments))
	  (parser-function-name (intern (lisp-format NIL "~A-parser" command-name)
					(symbol-package command-name))))
      `(progn 'compile
	      (defun ,command-name ,function-arguments
		(setq *Last-Command-Values* (multiple-value-list (progn . ,body)))
		,(if values
		     '(values-list *Last-Command-Values*)
		     NIL))
	      (define-command-internal ',command-name ,name ,command-table ',values
				       ',provide-output-destination-keyword ',explicit-arglist
				       ',arguments)
	      (setf (get ',command-name :COMMAND-ARGUMENT-PARSER) ',parser-function-name)
	      ,(compile-command-parser parser-function-name command-name arguments)))))

(defun undefine-command (command-name)
  (fmakunbound command-name)
  (dolist (command-table *Command-Tables*)
    (remhash command-name (command-table-commands-hash-table command-table))
    (setf (command-table-command-aarray command-table)
	  (delete command-name
		  (command-table-command-aarray command-table)
		  :KEY #'SECOND))))

(defstruct cp-command
  command-name
  name
  arguments)



(defun define-command-internal (command-name name command-table
				values provide-output-destination-keyword
				explicit-arglist arguments)
  (declare (ignore values provide-output-destination-keyword
				explicit-arglist))
  (let ((command (make-cp-command :NAME name :COMMAND-NAME command-name :ARGUMENTS arguments)))
    (when (stringp command-table)
      (setq command-table (find-command-table command-table)))
    (setf (get command-name :COMMMAND-PROCESSOR-COMMAND) command)
    (when command-table
      (install-command command-table command-name name))))


(defun install-command (command-table command-symbol &OPTIONAL command-name)
  (let ((command (get command-symbol :COMMMAND-PROCESSOR-COMMAND)))
    (unless command
      (error "Command ~A is not a command." command-symbol))
    (unless (lisp:typep command-table 'command-table)
      (setq command-table (find-command-table command-table :IF-DOES-NOT-EXIST :CREATE)))
    (setf (gethash command-symbol (command-table-commands-hash-table command-table))
	  command)
    (pushnew (list (or command-name
		       (make-pretty-name command-symbol))
		   command-symbol)
	     (command-table-command-aarray command-table)
	     :TEST #'EQUAL)))


(defun command-in-command-table-p (command-symbol command-table &OPTIONAL (need-name T))
  (let ((data (gethash command-symbol (command-table-commands-hash-table command-table))))
    (if data
	(values T )
	;; if couldn't find it there, then search up the inherit tables.
	(dolist (table-name (command-table-inherit-from command-table))
	  (let ((table (find-command-table table-name :if-does-not-exist NIL)))
	    (when table
	      (let ((found-it (command-in-command-table-p command-symbol table need-name)))
		(when found-it (return found-it)))))))))


(defun find-command (command-symbol command-table &OPTIONAL (need-name T))
  (let ((data (gethash command-symbol (command-table-commands-hash-table command-table))))
    (if data
	(values data)
	;; if couldn't find it there, then search up the inherit tables.
	(dolist (table-name (command-table-inherit-from command-table))
	  (let ((table (find-command-table table-name :if-does-not-exist NIL)))
	    (when table
	      (let ((found-it (command-in-command-table-p command-symbol table need-name)))
		(when found-it (return found-it)))))))))

(defun build-command (command-name &REST args)
  (list* command-name (copy-list args)))

(defun execute-command (command-name &REST command-arguments)
  (apply command-name command-arguments))


(defvar *Global-Table* (make-command-table "Global"))
(setq *Command-Table* *Global-Table*)



;; the basics of writing a command argument parser are the following:
;; 1. Each argument must get set with a value so that latter args can use the value.
;; 2. Required Arguments are asked first.
;; 3. All required arguments must be given.
;; 4. Optional Arguments are next.  These can be ignored by an activation command.
;; 5. If the command is to be activated, all the args not given, must be given
;;    defaults.

#+ignore
(cp:define-command (com-sample-file :command-table "Global")
		((file1 'scl:pathname :prompt "File1")
		 (file2 'scl:pathname :prompt "File2"))
  (lisp:format T "~D ~D" file1 file2))



#+ignore
(defun get-query-arguments-for-command (options arg-name arg-type)
  (let ((prompt (getf options :PROMPT))
	(prompt-mode (getf options :PROMPT-MODE))
	(default (getf options :DEFAULT))
	(provide-default (getf options :PROVIDE-DEFAULT))
	(default-type (getf options :DEFAULT-TYPE))
	(display-default (getf options :DISPLAY-DEFAULT))
	(confirm (getf options :CONFIRM))
	(query-arguments NIL))
    (when prompt (setq query-arguments (list* :PROMPT prompt query-arguments)))
    (when prompt-mode
      (setq query-arguments (list* :PROMPT-MODE prompt-mode query-arguments)))
    (when default
      (if (eq arg-type :KEYWORD)
	  (setq query-arguments (list* :DEFAULT arg-name query-arguments))
	  (setq query-arguments (list* :DEFAULT default query-arguments))))
    (when display-default
      (setq query-arguments (list* :DISPLAY-DEFAULT display-default query-arguments)))
    (when default-type
      (setq query-arguments (list* :DEFAULT-TYPE default-type query-arguments)))
    (when provide-default
      (setq query-arguments (list* :PROVIDE-DEFAULT provide-default query-arguments)))
    (when confirm
      (setq query-arguments (list* :CONFIRM confirm query-arguments)))
    query-arguments))

#+ignore
(defun compile-required-argument-parse (argument)
  (let ((argument-name (first argument))
	(type (second argument))
	(options (cddr argument)))
    (let (;;(mentioned-default (getf options :MENTIONED-DEFAULT)) keyword args only
	  (documentation (getf options :DOCUMENTATION))
	  (when (getf options :WHEN))
	  ;;(name (getf options :NAME)) keyword args only
	  (query-arguments (get-query-arguments-for-command options argument-name :REQUIRED)))
      (warn-unimplemented-args documentation NIL)
      (let ((body
	      `(setq ,argument-name (read-command-argument ,type stream
							   . ,query-arguments))))
	(if when
	    `(when ,when ,body)
	    body)))))

#+ignore
(defun compile-keyword-argument-parse (argument)
  (let ((argument-name (first argument))
	(type (second argument))
	(options (cddr argument)))
    (let ((keyword (intern (symbol-name argument-name) (find-package "KEYWORD")))
	  (default (getf options :DEFAULT))
	  (mentioned-default (getf options :MENTIONED-DEFAULT))
	  (documentation (getf options :DOCUMENTATION))
	  (when (getf options :WHEN))
	  (name (getf options :NAME))
	  (query-arguments (get-query-arguments-for-command options argument-name :KEYWORD)))
      (warn-unimplemented-args documentation NIL name NIL)
      (list
	(let ((body `(progn
		       (setq ,argument-name (query ,type
						    :DEFAULT ,mentioned-default
						    :STREAM stream . ,query-arguments))
		       (push ,keyword keywords-entered))))
	  body)
	`(,argument-name ,default)
	when))))



#+ignore
(defun compile-command-parser (command-name arguments)
  (multiple-value-bind (required optional keywords)
      (decode-arglist arguments)
    (when optional
      (error "&Optional arguments are not allowed in command definitions.~%~
  		Use a required argument instead with :CONFIRM NIL"))
    (let* ((required-code (mapcar #'compile-required-argument-parse required))
	   (keywords-code (mapcar #'compile-keyword-argument-parse keywords))
	   (keyword-names (mapcar #'FIRST keywords))
	   (keyword-alist (mapcar #'(lambda (keyword-name)
				      (list (concatenate
					      'STRING ":"
					      (string-capitalize
						(symbol-name keyword-name)))
					    (intern (symbol-name keyword-name)
						    (find-package "KEYWORD"))))
				  keyword-names))
	   (return-code
	     `(list ,@(mapcar #'FIRST required)
		    ,@(mapcan #'(lambda (arg-name)
				  (list (intern (symbol-name arg-name)
						(find-package "KEYWORD"))
					arg-name))
			      keyword-names))))
      (declare (list keyword-alist keyword-names required-code keywords-code))
      ;; combine required-code
      (let (required-body keyword-body)
	(setq required-body (and required-code required-code))
	(when keywords
	  ;; combine keyword body
	  ;; trick here is to realize we shall create a completion context around this
	  ;; and dispatch once we know which keyword we are reading.
	  (setq keyword-body
		`(case keyword-input
		   ,@(mapcar #'(lambda (keyword-code name)
				 (let ((keyword (intern (symbol-name name)
							(find-package "KEYWORD"))))
				   `(,keyword ,(first keyword-code))))
			     keywords-code keyword-names)
		   (OTHERWISE ;; well we got an query that didn't fit any of the keywords
		     ;; what should we do?
		     (parse-error keyword-input () "Not a Valid Keyword."))))
	  (setq keyword-body
		`(do ((keyword-alist keyword-alist)
		      (first-keyword T NIL))
		     ((= (length keywords-entered) (length keyword-alist)))
		   (declare (list keyword-alist))
		   (read-delimiter-char stream)
		   (let ((keyword-input
			   (query `((keyword-argument
				       ,(let ((result NIL))
					  (dolist (pair keyword-alist)
					    (unless (member (second pair) keywords-entered)
					      (push pair result)))
					  result)))
				   :STREAM stream :PROMPT-AFTER NIL :PROMPT (and first-keyword "keywords")
				   :PROVIDE-DEFAULT NIL)))
		     (read-delimiter-char stream)
		     ,keyword-body))))
	`(defun ,command-name (stream)
	   ,(unless (or required optional keywords)
	      `(declare (ignore stream)))
	   (let ((*Parse-Type* :NORMAL))
	     (let ,(mapcar #'first required)
	       (let ,(if keywords '(keywords-entered) NIL)
		 ,@(if keywords '((declare (list keywords-entered))) NIL)
		 (progn
		   ,(cons 'progn required-body)
		   (let ,(if keywords
			     (cons `(keyword-alist ',keyword-alist)
				   (mapcar #'second keywords-code)) NIL)
		     ;; for all whens in keywords remove them if when is not true now.
		     (progn
		       . ,(mapcan
			    #'(lambda (triple name)
				(let ((when-predicate (third triple)))
				  (when when-predicate
				    (list `(unless ,when-predicate
					     (setq keyword-alist
						   (remove
						     ,(intern (symbol-name name)
							      (find-package "KEYWORD"))
						     keyword-alist
						     :KEY #'SECOND)))))))
			    keywords-code keyword-names))
		     ,keyword-body
		     ,return-code))))))))))






(defun read-delimiter-char (stream &OPTIONAL (hang-p T))
  #.(fast)
  (let ((char (if hang-p (read-char-for-query stream)
		  (and (peek-char-for-query stream NIL)
		       (read-char-for-query stream)))))
    ;; character should be either an :ACTIVATION char or an :QUERY
    (if (not char)
	;; I think I should have this automatically add a delimiter so that a rescan
	;; won't lose
	(if (not *Rescanning*)
	    (insert-delimiter-into-stream stream #\Space))
	(progn
	  (when (consp char)
	    (ecase (first char)
	      (:QUERY
		#+ignore (insert-delimiter-into-stream stream (second char)) ;;3/25/89
		)
	      (:ACTIVATION (unread-char-for-query char stream)
	       #+ignore (insert-delimiter-into-stream stream #\Space) ;;3/25/89
	       (setq *Parse-Type* :DEFAULT-UNCONFIRMED-ARGS))))
	  char))))

;; The idea is that the previous value read will leave its delimiter in the stream
;; Then this will read it out.  It will then setup for the next call.

(defun read-command-argument (type stream &REST keyword-args
			      &KEY default (provide-default t) mentioned-default
			      (class :normal) documentation confirm query-identifier
			      (prompt :ENTER-TYPE)
			      &ALLOW-OTHER-KEYS)
  (declare (ignore mentioned-default class documentation))
  (declare (ignore keyword-args))		; 4/30/90 (SLN)
  (read-delimiter-char stream NIL)
  (query type :STREAM stream :DEFAULT default :QUERY-IDENTIFIER query-identifier
	 :PROVIDE-DEFAULT provide-default :PROMPT prompt
	 :CONFIRM confirm))




;; When reading a command we have to worry about required arguments with :CONFIRM T
;; Really normally a required argument is not required unless it has a :CONFIRM
;; keyword.  Just calling the command without args will work unless a :CONFIRM.
;; The strategy is to read args until an Activation.  Then look to see if any
;; required args not yet read have :CONFIRM T.  If so continue reading args
;; and prompting with a clue for the user that the argument is required.
;; For now I am ignoring :CONFIRM on keyword args.

(defvar *Unbound-Argument* '|.unbound-cp-arg.|)

(defun read-command-arguments (command-name &KEY initial-arguments
			       (command-table *Command-Table*) (stream *Standard-Input*)
			       (prompt NIL))
  (declare (ignore prompt initial-arguments))	; 4/30/90 (SLN)
  ;; first find all the arguments for the command.
  (let ((command (find-command command-name command-table)))
    (unless command
      (error "Could not Find command ~A in table ~A to read." command-name command-table))
    (let ((arguments (cp-command-arguments command))
	  (keywords-entered NIL)) ;; build up the list of keywords used.
      (declare (list keywords-entered))
      (multiple-value-bind (required-args optional-args keyword-args aux-args)
	  (decode-arglist arguments)
	(declare (ignore optional-args aux-args) (list required-args keyword-args))
	(let* ((argument-names
		 (append (mapcar #'FIRST required-args)
			 (mapcar #'FIRST keyword-args)))
	       (completion-alist NIL)
	       (keyword-argument-alist NIL)
	       (number-of-keywords (length keyword-args))
	       (first-keyword T))
	  (declare (list argument-names) (fixnum number-of-keywords))
	  ;; setup data structures for later quick processing.
	  (dolist (keyword-arg keyword-args)
	    (let* ((keyword-name (first keyword-arg))
		   (actual-keyword (intern (symbol-name keyword-name)
					   (find-package "KEYWORD"))))
	      (push (list (concatenate 'STRING ":"
				       (string-capitalize
					 (symbol-name keyword-name)))
			  actual-keyword)
		    completion-alist)
	      (push (list actual-keyword keyword-arg) keyword-argument-alist)))
	  ;; now begin querying for the keyword arguments.
	  (with-input-editing (stream NIL)
	    ;; bind all the arguments so they can be used for argument defaulting.
	    (progv argument-names (make-list (length argument-names)
					     :INITIAL-ELEMENT *Unbound-Argument*)
	      ;; we now have special bindings for all the variables.
	      ;; hope it works.
	      (catch 'FINISHED-READING-ARGS
		(let ((*Parse-Type* :NORMAL))
		  ;; Note: this is expecting that there is a delimiter around.
		  (do ((args required-args (cdr args)))
		      ((null args))
		    ;; First look for the character that finished off the last arg.
		    ;; It could even find an activation before a single arg is read.
		    (let ((next-char (peek-char-for-query stream NIL)))
		      (when (and (consp next-char) (eq (first next-char) :ACTIVATION))
			;; the user is trying to activate the command.  Decide
			;; if we will let her/him.
			;; look for any args not read yet for a :CONFIRM
			(if (dolist (arg args NIL)
			      (when (getf (cddr arg) :CONFIRM)
				(return T)))
			    (progn
			      ;; yes we have a confirmed arg somewhere, keep reading.
			      ;; we need to throw away the character.  Just unreading it
			      ;; won't quite work. since it will put it back in the queue.
			      (delete-char-for-query (read-char-for-query stream) stream)
			      ;; Substitute a space instead.
			      (insert-delimiter-into-stream stream #\Space)
			      ;; and keep on reading.
			      NIL)
			    ;; no confirmed args and we have a valid activation char.
			    (throw 'finished-reading-args NIL))))
		    (let ((arg (first args)))
		      (setf (symbol-value (first arg))
			    (read-command-argument
			      (eval (second arg))
			      stream
			      :DEFAULT (eval (getf (cddr arg) :DEFAULT))
			      :PROMPT (getf (cddr arg) :PROMPT)))))
		  ;; now read the keywords.
		  (do ()
		      ;; read until all the kewords are used or something inside returns from
		      ;; an activation
		      ((%= (length keywords-entered) number-of-keywords))
		    (let ((next-char (read-delimiter-char stream NIL)))
		      ;; look for a possible activation char here.
		      (when (and (consp next-char) (eq (first next-char) :ACTIVATION))
			(throw 'finished-reading-args NIL)))
		    ;; query based on the keywords that aren't used yet.
		    (let ((keyword-input
			    (query `((keyword-argument
				       ,(let ((result NIL))
					  (dolist (pair completion-alist)
					    (unless (member (second pair) keywords-entered)
					      (push pair result)))
					  result)))
				   :STREAM stream
				   :PROMPT-AFTER NIL
				   :PROMPT (and first-keyword "keywords")
				   :PROVIDE-DEFAULT NIL)))
		      (when keyword-input
			(read-delimiter-char stream NIL)
			(let ((keyword-arg (assoc keyword-input keyword-argument-alist)))
			  (dformat "~%Keyword-Input ~D~%Key-arg-list ~D. Keyword Arg ~D"
				   keyword-input keyword-argument-alist keyword-arg)
			  (if keyword-arg
			      (let* ((original-keyword-arg (second keyword-arg))
				     (argument-name (first original-keyword-arg))
				     (type (second original-keyword-arg))
				     (other-query-args (cddr original-keyword-arg)))
				(setf (symbol-value argument-name)
				      (query (eval type)
					     :DEFAULT (getf other-query-args :DEFAULT)
					     :STREAM stream
					     :PROMPT (getf other-query-args :PROMPT)))
				(push keyword-input keywords-entered))
			      (parse-error keyword-input NIL "Not a Valid Keyword.")))))
		    (setq first-keyword NIL))))
	      ;; go through all arg and make sure they have a default value.
	      (flet ((default-args (args)
		       (dolist (arg args)
			 (when (and (eq (symbol-value (first arg)) *Unbound-Argument*)
				    (getf arg :DEFAULT))
			   (setf (symbol-value (first arg)) (eval (getf arg :DEFAULT)))))))
		(default-args required-args)
		(default-args keyword-args))
	      ;; finished reading all argument.  Now return them in a manner such that
	      ;; the function can be applied to them.
	      (append (mapcar #'(lambda (req)
				  (symbol-value (first req)))
			      required-args)
		      (mapcan #'(lambda (key)
				  (list (first key) (symbol-value (first (second key)))))
			      keyword-argument-alist)))))))))



(defun compile-command-parser (command-function-name command-name arguments)
  (declare (ignore arguments))			; 4/30/90 (SLN)
  `(defun ,command-function-name (stream)
     (read-command-arguments ',command-name :stream stream)))



(defun command-menu-choose-arguments (command-name &KEY initial-arguments
				      gesture command-table own-window mode full-rubout)
  (declare (ignore full-rubout own-window mode gesture))
  ;; make it look like it is at top level, not inside a rubout handler.
  (if (not (unread-confirmed-args-p command-name command-table initial-arguments))
      initial-arguments
      (let ((*Inside-Input-Editor-P* NIL)
	    (*Query-Active* NIL))
	(let ((arguments
		(with-input-editing-options ((:full-rubout 'ABORT-MENU-CHOOSE))
		  (with-input-editing ()
		    (read-command-arguments command-name :INITIAL-ARGUMENTS initial-arguments
					    :COMMAND-TABLE command-table)))))
	  ;; clean-up input editor from this mess.
	  (initialize-input-editor *Standard-Input*
				   (window-input-editor *Standard-Input*))
	  arguments))))

(defun unread-confirmed-args-p (command-name command-table initial-arguments)
  (declare (list initial-arguments))
  (let ((command (find-command command-name command-table)))
    (unless command
      (error "Could not Find command ~A in table ~A to read." command-name command-table))
    (let ((arguments (cp-command-arguments command)))
      (let ((required-args (decode-arglist arguments)))
	;; find index of last required argument that has a confirm of T.
	(let ((count (position-if #'(lambda (arg)
				      (getf (cddr arg) :CONFIRM))
				  required-args :FROM-END T)))
	  ;; we need to read arguments if there are not enough initial arguments.
	  (and (numberp count)
	       (< (length initial-arguments) (1+ count))))))))

;;; -----------------------------------------------------------------
;;;	END OF FILE
;;; -----------------------------------------------------------------
