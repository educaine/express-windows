
				MESSAGES

The information contained herein is proprietary to and comprises valuable
trade secrets of Liszt Programming Inc.  It is provided in confidence
pursuant to a written product test license agreement and it is intended for
internal use only at the test site during the period of the product test.
It may not be duplicated in whole or in part without prior written consent
of Liszt Programming Inc.  All copies of this information remain the
property of Liszt Programming and are to be returned to Liszt Programming
upon termination of the product test.  Copyright 1989 Liszt Programming
Inc.  All rights reserved.

****
send	 								[Macro]

(object message &rest args)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****

The following are compatability associations between lisp-machine-lisp
window system messages and EW functions.  The Macro SEND has been defined
such that it will automatically expand these messages into the
corresponding EW function.

:ANY-TYI			read-any
:ANY-TYI-NO-HANG		read-any-char-no-hang
:BASELINE			window-font-ascent
:BOTTOM-MARGIN-SIZE		window-bottom-margin-size
:CENTER-AROUND			center-window-around
:CHAR-WIDTH			default-char-width
:CLEAR-HISTORY			clear-history
:CLEAR-REST-OF-LINE		clear-rest-of-line
:CLEAR-REST-OF-WINDOW		clear-rest-of-window
:CLEAR-WINDOW			clear-window
:COLOR-STREAM-P			window-color-p
:COMPUTE-MOTION			window-compute-motion
:CONFIGURATION			configuration
:CURRENT-FONT			window-font
:CURRENT-LINE-HEIGHT		window-line-height
:CURRENT-STYLE			window-character-style
:DEEXPOSE			deexpose-window
:EDGES				window-edges
:EXPOSE				expose-window
:EXPOSE-NEAR			expose-window-near
:FRESH-LINE			fresh-line
:GET-PANE			get-pane
:HEIGHT				window-height
:HOME-CURSOR			home-cursor
:HOME-DOWN			home-down
:INCREMENT-CURSORPOS		increment-cursorpos
:INFERIORS			window-inferiors
:INSIDE-EDGES			window-inside-edges
:INSIDE-HEIGHT			window-inside-height
:INSIDE-SIZE			window-inside-size
:INSIDE-WIDTH			window-inside-width
:KILL				window-kill
:LEFT-MARGIN-SIZE		window-left-margin-size
:LINE-HEIGHT			window-line-height
:MARGIN-COMPONENTS		presentation-window-margins
:MAXIMUM-X-POSITION		presentation-window-max-x-position
:MAXIMUM-Y-POSITION		presentation-window-max-y-position
:MORE-P				window-more-p
:NAME				window-name
:OPERATION-HANDLED-P		window-operation-handled-p
:POSITION			window-position
:READ-CURSORPOS			read-cursorpos
:READ-LOCATION			read-location
:RIGHT-MARGIN-SIZE		window-right-margin-size
:SET-CONFIGURATION		set-configuration
:SET-CURSORPOS			set-cursorpos
:SET-EDGES			set-window-edges
:SET-INSIDE-SIZE		set-window-inside-size
:SET-LABEL			set-window-label
:SET-MAXIMUM-X-POSITION		(setf presentation-window-max-x-position)
:SET-MAXIMUM-Y-POSITION		(setf presentation-window-max-y-position)
:SET-MORE-P			(setf window-more-p)
:SET-POSITION			set-window-position
:SET-SIZE			set-window-size
:SET-SUPERIOR			(setf window-superior)
:SET-VIEWPORT-POSITION		set-viewport-position
:SET-VSP			set-window-line-spacing
:SIZE				window-size
:STRING-LENGTH			window-string-length
:STRING-OUT			window-string-out
:SUPERIOR			window-superior
:TOP-MARGIN-SIZE		window-top-margin-size
:TYI				read-char
:TYI-NO-HANG			read-char-no-hang
:TYIPEEK			peek-char
:TYO				(write-char arg window arg)
:UNTYI				unread-char
:VIEWPORT-POSITION		viewport-position
:VISIBLE-CURSORPOS-LIMITS	visible-cursorpos-limits
:VSP				window-line-spacing
:WIDTH				window-width
:WHICH-OPERATIONS		which-operations
:X-SCROLL-POSITION		x-scroll-position
:X-SCROLL-TO			x-scroll-to
:Y-SCROLL-POSITION		y-scroll-position
:Y-SCROLL-TO			y-scroll-to
