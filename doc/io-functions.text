
				IO FUNCTIONS

The information contained herein is proprietary to and comprises valuable
trade secrets of Liszt Programming Inc.  It is provided in confidence
pursuant to a written product test license agreement and it is intended for
internal use only at the test site during the period of the product test.
It may not be duplicated in whole or in part without prior written consent
of Liszt Programming Inc.  All copies of this information remain the
property of Liszt Programming and are to be returned to Liszt Programming
upon termination of the product test.  Copyright 1989 Liszt Programming
Inc.  All rights reserved.


Most Common Lisp IO Functions have been redefined to interface properly
with EW windows and to automatically take advantage of the presentation
system.  Access to the original Common Lisp definitions is still available
through an internal symbol of the EW package.  For instance, the original
Common Lisp "format" function is avaiable as "ew::lisp-format".  All other
redefined Common Lisp functions are similarly named "ew:lisp-name", where
"name" is the original Common Lisp function name.

Functions and their arguments which are extensions to pure Common Lisp are
documented accordingly.  For detailed information about the functions and the various
arguments not explained here, see the appropriate Common Lisp documentation.


				Output Functions


****
format	 								[Function]

(destination control-string &rest args)

****
fresh-line 								[Function]

(&OPTIONAL output-stream)

The cursor position in the new line can be affected by a enclosing
fresh-line in a call to the macro with-indenting-output.

****
prin1 									[Function]

(object &OPTIONAL output-stream)

****
princ 									[Function]

(object &OPTIONAL output-stream)

****
print	 								[Function]

(object &OPTIONAL output-stream)

****
terpri 									[Function]

(&OPTIONAL output-stream)

****
write 									[Function]

(object &REST other-args &KEY stream escape radix base circle pretty level length
	      case gensym array integer-length array-length string-length
	      bit-vector-length abbreviate-quote readably structure-contents

****
write-char 								[Function]

(character &OPTIONAL output-stream)

****
write-line 								[Function]

(string &OPTIONAL output-stream &KEY (start 0) end)

****
write-string 								[Function]

(string &OPTIONAL output-stream &KEY (start 0) end)

****



				Input Functions



****
listen	 								[Function]

(&OPTIONAL input-stream)

****
peek-char 								[Function]

(&OPTIONAL peek-type input-stream (eof-errorp t) eof-value recursive-p)

****
read	 								[Function]

(&OPTIONAL input-stream (eof-errorp T) eof-value recursive-p)

****
read-char 								[Function]

(&OPTIONAL input-stream (eof-errorp t) eof-value recursive-p)

****
read-char-no-hang 							[Function]

(&OPTIONAL input-stream (eof-errorp t) eof-value recursive-p)

****
read-line 								[Function]

(&OPTIONAL input-stream (eof-errorp T) eof-value recursive-p)

****
read-preserving-whitespace						[Function]

(&OPTIONAL input-stream (eof-errorp T) eof-value recursive-p)

****
unread-char 								[Function]

(character &OPTIONAL input-stream)

****
y-or-n-p 								[Function]

(&OPTIONAL format-string &REST args)

****
yes-or-no-p 								[Function]

(format-string &REST args)

****



				Extensions to Common Lisp


****
peek-any-char 								[Function]

(&OPTIONAL input-stream (eof-errorp t) eof-value recursive-p)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****
prompt-and-read								[Function]

(type &optional format-string &rest format-args)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****
read-any-char 								[Function]

(&OPTIONAL input-stream (eof-errorp t) eof-value recursive-p)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****
read-any-char-no-hang							[Function]

(&OPTIONAL input-stream (eof-errorp t) eof-value recursive-p)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****
unread-any-char								[Function]

(character &OPTIONAL input-stream)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****
with-input-editing	 						[Macro]

((stream keyword) &BODY body)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****
with-input-editing-options	 					[Macro]

(options &BODY body)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****
