
				DISPLAY MACROS

The information contained herein is proprietary to and comprises valuable
trade secrets of Liszt Programming Inc.  It is provided in confidence
pursuant to a written product test license agreement and it is intended for
internal use only at the test site during the period of the product test.
It may not be duplicated in whole or in part without prior written consent
of Liszt Programming Inc.  All copies of this information remain the
property of Liszt Programming and are to be returned to Liszt Programming
upon termination of the product test.  Copyright 1989 Liszt Programming
Inc.  All rights reserved.

****
entry 									[Macro]

((&OPTIONAL (stream '*Standard-Output*)
			    &KEY align-x align-y align
			    value (value-test '#'EQL)
		 &BODY body)

For use in conjuction with the make-table and row macros.  Entry determines
the appearance of individual elements of a table.  It is equivalent to the
macro table-entry.

Optional arguments:

stream - The stream (window) on which the table entry is displayed.

Keyword options:

:align-x -  Determines the horizontal alignment of the entry in its column.
Valid options are :LEFT, :RIGHT, and :CENTER.  If not specified, the
alignment defaults to :LEFT.

:align-y - Determines the vertical alignment of the entry in its row.
Valid options are :TOP, :CENTER, and :BOTTOM.  If not specified, the
alignment defaults to :TOP.

:align -  Equivalent to :align-x.  If both :align and :align-x are
specified, the value of :align-x is used.

:value -  

:value-test -

Usage:

> (let ((*standard-output* *my-window*))
    (clear-history *my-window*)
    (make-table ()
		(row ()
		     (entry (T :align-x :center :align-y :center) 1)
		     (with-character-style ('(:fix :roman :huge))
					   (entry () 245))
		     (entry () 3))
		(row ()
		     (entry () 'abcdef)
		     (entry (T :align :right) 'b)
		     (entry () 'c)))
    (sync))
NIL

****
format-list	 							[Function]

(sequence function
		    &KEY (separator ",") finally if-two
		    filled after-line-break conjunction (stream *Standard-Output*))

Prints a formatted version of the sequence to the stream via calls to
formatting-list and formatting-list-element.  The appearance of individual
sequence elements is controlled by the function argument.

Required arguments:

sequence - A sequence of elements to be printed to the stream.
 
function - A function of two arguments which controls the printed
appearance of the individual sequence elements.  The first argument is an
element of the sequence; the second argument is the output stream.

Keyword options:

:separator - An expression, which is evaluated once to produce characters
which appear between the elements of the sequence in the printed output.
Defaults to a comma ",".

:finally -  A special separator to be used between the last two elements of
the printed sequence.

:if-two - A special separator to be used if the sequence contains only two elements.
		    
:filled - {Not used}

:after-line-break - {Not used}

:conjunction - A special separator used between the last two elements of a
sequence.  The conjunction is printed with a leading and trailing space.
This argument has lower precedence than arguments supplied to :finally and :if-two.

:stream - The stream to which the formatted output is directed.

Usage:

> (progn
    (format-list '(rain shine)
		 #'(lambda (element stream) (format stream "~S" element))
		 :stream *my-window*  :conjunction "or")
    (sync))
NIL
> (let ((sequence (list 10 9 8 7)))
    (format-list sequence
		 #'(lambda (element stream) (format stream "~S" element))
		 :stream *my-window*  :separator "; " :finally " and ")
    (sync))
NIL

****
formatting-list 							[Macro]

((&OPTIONAL stream
				      &KEY (separator ", ") finally if-two
				      filled after-line-break conjunction
			   &BODY body)

Used in conjunction with formatting-list-element to print formatted output
to stream (or to *Standard-Output*).  See format-list for description of arguments. 

Usage:

> (let ((*Standard-Output* *my-window*)
	(sequence (list '1 2 3 4)))
    (formatting-list ()
		     (dolist (element sequence)
		       (formatting-list-element ()
						(princ element))))
    (sync))
NIL

****
formatting-list-element		 					[Macro]

((&OPTIONAL stream) &BODY body)

Used in conjunction with formatting-list to print formatted output to
stream.  See format-list for description of arguments.  See formatting-list
for sample usage.

****
make-table 								[Macro]

 ((&OPTIONAL (stream '*Standard-Output*)
				 &KEY equalize-column-widths extend-width
				 extend-height (row-spacing 0)
				 column-spacing
				 multiple-columns
				 (multiple-column-column-spacing)
				 equalize-multiple-column-widths
				 output-multiple-columns-row-wise
				 &ALLOW-OTHER-KEYS)
		      &BODY body)

Used in conjunction with row and entry to print formatted tabular output.

Optional arguments:

stream - Window on which table will appear.

Keyword options:

:equalize-column-widths - Boolean option.  When NIL, table column widths
vary in size according to the size of elements in each column.

:extend-width - An integral number of pixels specifying the horizontal
space which the resulting table should be expected to fill.  Each column
width is extended proportionally to fill the space specified by this
argument.
				 
:extend-height - An integral number of pixels specifying the vertical
space which the resulting table should be expected to fill.  Each row
height is extended proportionally to fill the space specified by this
argument.

:row-spacing - An integral number of pixels specifying the spacing
between table rows.
				 
:column-spacing - An integral number of pixels specifying the spacing
between table columns.
				 
:multiple-columns - Boolean option.
				 
:multiple-column-column-spacing -
				 
:equalize-multiple-column-widths -
				 
:output-multiple-columns-row-wise -

Usage:

> (let ((*standard-output* *my-window*))
    (make-table ()
		(row ()
		     (entry () 1)
		     (entry () 2)
		     (entry () 3))
		(row ()
		     (entry () 'abcdef)
		     (entry () 'b)
		     (entry () 'c)))
    (sync))
NIL

****
row 									[Macro]

((&OPTIONAL (stream '*Standard-Output*)
			  &KEY single-column id (id-test #'eql) (once-only NIL)
			  &ALLOW-OTHER-KEYS)


Optional arguments:

stream -

Keyword options:

:single-column  -

:id  -

:id-test - 

:once-only -

Usage:

****
table-entry 								[Macro]

((&OPTIONAL (stream '*Standard-Output*)
				  &KEY align-x align-y align
				  value (value-test '#'EQL)
		       &BODY body)


Table-entry is equivalent to the macro entry.  See entry for details.

****
table-row 								[Macro]

((&OPTIONAL (stream '*Standard-Output*)
				&KEY single-column id (id-test #'eql) (once-only NIL)
				&ALLOW-OTHER-KEYS)

Table-row is equivalent to the macro row.  See row for details.

****
table-column-headings 							[Macro]

((&OPTIONAL stream
					    &KEY underline-p
					    (id ''headings)
					    &ALLOW-OTHER-KEYS)
				 &BODY body)

Used in conjunction with the macro make-table to print headings for a
table's columns.  The headings are printed over the table's columns in left
to right order until the supply of headings is exhausted.  If more headings
are provided than actual columns in the table, the extra headings appear
over blank columns.  If fewer headings are provided, then some column(s) at
the right side of the table will have no headings.

Optional arguments:

stream - Stream (window) to which output is directed.

Keyword options:

:underline-p - Boolean option.  When true, underlines each of the headings.
{Not implemented}
					    
:id -

Usage:

> (let ((*standard-output* *my-window*))
    (clear-history *my-window*)
    (make-table ()
		(table-column-headings () "first" "second" "third" "fourth")
		(row ()
		     (entry () 1)
		     (entry () 2)
		     (entry () 3))
		(row ()
		     (entry () 'abcdef)
		     (entry () 'b)
		     (entry () 'c)))
    (sync))
NIL

****
with-indenting-output 						[Macro]

((stream indentation) &BODY body)

When used in conjunction with the output formatting functions fresh-line,
indent-terpri, indent-by, and remember-indentation, the macro
with-indenting-output causes printed output in the body to be indented by
the number of pixels specified by the indentation argument.

Required arguments:

stream - The stream (window) to be indented.

indentation - The number of pixels by which output should be indented.

Usage:

> (let ((*standard-output* *my-window*))
    (clear-history *my-window*)
    (princ "No Indentation")
    (with-indenting-output (T 20)
			   (fresh-line)
			   (princ "Indented by 20")
			   (fresh-line)
			   (indent-by 20)
			   (princ "Indented by 40"))
    (fresh-line)
    (princ "Back to Left Edge")
    (sync))
NIL

****
with-border 							[Macro]

((&OPTIONAL
			 stream
			 &KEY
			 (shape :rectangle)
			 (thickness 1) (margins 1) (pattern t)
			 (gray-level 1) opaque filled
			 (alu :DRAW) (move-cursor T)
			 width height label (label-position :bottom)
			 label-separator-line
			 (label-separator-line-thickness 1)
			 (label-alignment :left))
		       &BODY body)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****
with-centered-display 						[Macro]

((&OPTIONAL (stream '*Standard-Output*)
					    &KEY (center-x-p T) (center-y-p NIL)
					    width height)

Used to center text in either the horizontal or vertical direction in the
output stream (window).

Optional arguments:

stream - The stream (window) for output.  Defaults to *Standard-Output*.

Keyword options:

center-x-p - Defaults to T.

center-y-p -  Defaults to NIL.

width -  

height -

Usage:

> (let ((*Standard-Output* *my-window*))
    (clear-history *my-window*)
    (with-centered-display (*my-window* :center-y-p T)
			   (princ "Centered Text"))
    (sync))
NIL

****
with-room-for-graphics 						[Macro]

((&optional STREAM Height
					     &key (fresh-line t) (move-cursor t))
				  &body body)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****
with-output-truncation 						[Macro]

((&OPTIONAL stream &REST options) &BODY body)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****
with-output-filling 						[Macro]

((&OPTIONAL stream &KEY fill-column (fill-characters ''(#\Space))
					 after-line-break initially-too
					 &ALLOW-OTHER-KEYS)

Required arguments:

Optional arguments:

Keyword options:

Usage:

****
with-underlining 						[Macro]

((&OPTIONAL stream &KEY (underline-whitespace T)) &BODY body)


{Not implemented}

Optional arguments:

stream -

Keyword options:

:underline-whitespace -

Usage:


****
